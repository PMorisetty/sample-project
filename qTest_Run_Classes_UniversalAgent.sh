testNGXMLfilePath=$1
executionEnvironment=$2
echo $testNGXMLfilePath
echo $executionEnvironment
java -Xms1024M -Xmx1048M -cp "./lib/*:./target/classes:./target/test-classes" -Dregion=$executionEnvironment org.testng.TestNG $testNGXMLfilePath
###### For insufficient memory issue - memory loss :->
###### Xms1024M -Xmx1048M can be increased to 2048 provided there's sufficient allocated memory at execution m/c.