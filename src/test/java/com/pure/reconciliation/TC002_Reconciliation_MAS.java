package com.pure.reconciliation;

import java.lang.reflect.Method;
import java.util.Hashtable;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.utilities.TestUtil;
import com.pure.reconciliation.MASReconciliationLib;

public class TC002_Reconciliation_MAS extends ActionEngine {

	/*
	 * Reset file for new data
	 */
	boolean status = MASReconciliationLib.resetDataSheet();
	@BeforeMethod
	public void beforeMethod(Method m){
		String value = m.getName().substring(6);
		value = value.substring(0, value.lastIndexOf("_"));
		reporter.createHeader(value);
	}
	@DataProvider
	private Object[][] getTestData() {
		return TestUtil.getData("TC002_Reconciliation_MAS", TestDataReconciliation,
				"MASReconciliation");
	}

	@Test(dataProvider = "getTestData", priority = 1)
	public void TC002_Reconciliation_MAS_Script(Hashtable<String, String> data)
			throws Throwable {
		MASReconciliationLib masReconciliationLib = new MASReconciliationLib(driver, reporter);
		openApplication("DW");
		masReconciliationLib.selectSource1(data.get("Source1"));
		masReconciliationLib.selectSource2(data.get("Source2"));
		masReconciliationLib.selectCompareField(data.get("CompareField"));
		masReconciliationLib.enterDiffBtwnSystems(data.get("DiffBtwnSystems"));
		masReconciliationLib.clickCompareSources();
		String rowCount = masReconciliationLib.getRowCount();
		masReconciliationLib.getTableData("MAS Reconciliation", rowCount, data.get("DiffBtwnSystems"), data.get("Source1") + " - " + data.get("Source2"));
	}

	@Test(priority = 2)
	public void TC002_Reconciliation_STATS_To_Billing_Script() throws Throwable {
		MASReconciliationLib masReconciliationLib = new MASReconciliationLib(driver, reporter);
		openApplication("SB");
		String rowCount1 = masReconciliationLib.getRowCount();
		masReconciliationLib.getTableData("STATS to Billing Tables Reconciliation", rowCount1, "NA", "STATS to Billing Tables Reconciliation");
	}
	
	@Test(priority = 3)
	public void TC002_Reconciliation_Transaction_Inconsistencies_Script() throws Throwable {
		MASReconciliationLib masReconciliationLib = new MASReconciliationLib(driver, reporter);
		openApplication("TI");
		String rowCount2 = masReconciliationLib.getRowCount();
		masReconciliationLib.getTableData("Transaction Inconsistencies page", rowCount2, "NA", "Transaction Inconsistencies");
	}
	
	@AfterClass
	public void afterAllTests() throws Exception{
		MASReconciliationLib.createEmailSheet();
	}
}

