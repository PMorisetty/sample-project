package com.pure.reconciliation;

import org.openqa.selenium.By;

public class MASReconciliationPage {
	static By Source1DrpDwn;
	static By Source2DrpDwn;
	static By CompareFieldDrpDwn;
	static By DiffBtwnSystems;
	static By compareSourcesBtn;
	static By getRowCountText;
	static By CompareSTATStoBILLINGBtn;
	static By updateBtn;
	static{
		Source1DrpDwn = By.xpath("//select[@name='Source1']");
		Source2DrpDwn = By.xpath("//select[@name='Source2']");
		CompareFieldDrpDwn = By.xpath("//select[@name='CompareField']");
		DiffBtwnSystems = By.xpath("//input[@name='Diff']");
		compareSourcesBtn = By.xpath("//input[@value='Compare Sources']");
		getRowCountText = By.xpath("//td[contains(text(), 'Row Count:')]");
		CompareSTATStoBILLINGBtn = By.xpath("//input[@value='Compare STATS to BILLING']");
		updateBtn = By.xpath("//input[@value='Update']");

	}
}
