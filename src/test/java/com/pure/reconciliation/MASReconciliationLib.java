package com.pure.reconciliation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.velocity.runtime.directive.Foreach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.utilities.TestUtil;
import com.pure.utilities.Xls_Reader;

public class MASReconciliationLib extends ActionEngine {

	DateFormat dateFormat = new SimpleDateFormat("MMddyyyyHHmmss");
	Date date = new Date();
	String timestamp = dateFormat.format(date);
	String resultFilePath = "ReconciliationExcel_New.xlsx";
	String resultFilePath_Old = "ReconciliationExcel_Old.xlsx";
	Row row0, row1, row2;

	FileInputStream inputStream;
	XSSFWorkbook book;
	Sheet sheet;
	FileOutputStream outputStream;

	static boolean fileAvailable = true;

	public MASReconciliationLib(EventFiringWebDriver driver, CReporter reporter) {
		this.driver = driver;
		this.reporter = reporter;
	}

	/*Function to select source1 in PURE Dragon/Access/DW/MAS Reconciliation*/
	public void selectSource1(String source1) throws Throwable{
		selectByVisibleText(MASReconciliationPage.Source1DrpDwn, source1, "Source1");
	}

	/*Function to select source2 in PURE Dragon/Access/DW/MAS Reconciliation*/
	public void selectSource2(String source2) throws Throwable{
		selectByVisibleText(MASReconciliationPage.Source2DrpDwn, source2, "Source2");
	}

	/*Function to select compare field in PURE Dragon/Access/DW/MAS Reconciliation*/
	public void selectCompareField(String compareField) throws Throwable{
		selectByVisibleText(MASReconciliationPage.CompareFieldDrpDwn, compareField, "Compare Field");
	}

	/*Function to enter difference between systems in PURE Dragon/Access/DW/MAS Reconciliation*/
	public void enterDiffBtwnSystems(String diffBtwnSystems) throws Throwable{
		type(MASReconciliationPage.DiffBtwnSystems, diffBtwnSystems, "Difference Between Systems");
	}

	/*Function to click compare sources in PURE Dragon/Access/DW/MAS Reconciliation*/
	public void clickCompareSources() throws Throwable{
		click(MASReconciliationPage.compareSourcesBtn, "Compare Sources");
	}

	public void verifyWebPageIsOpened(String URLName) throws Throwable{
		if(URLName.equalsIgnoreCase("DW"))
		{
			isVisible(MASReconciliationPage.compareSourcesBtn, "Compare Sources Button");
			System.out.println("PURE Dragon/Access/DW/MAS Reconciliation page is opened successfully");
		}
		if(URLName.equalsIgnoreCase("SB"))
		{
			isVisible(MASReconciliationPage.CompareSTATStoBILLINGBtn, "Compare STATS To BILLING Button");
			System.out.println("PURE Dragon STATS to Billing Tables Reconciliation is opened successfully");
		}
		if(URLName.equalsIgnoreCase("TI"))
		{
			isVisible(MASReconciliationPage.updateBtn, "Update Button");
			System.out.println("PURE Dragon Transaction Inconsistencies page is opened successfully");
		}
	}

	public String getRowCount() throws Throwable{
		String[] rowCountText = getText(MASReconciliationPage.getRowCountText, "Row Count Text").split(": ");
		System.out.println("Row Count is :"+rowCountText[1]);
		//int rowCount = Integer.parseInt(rowCountText[1]);
		return rowCountText[1];
	}

	public static synchronized boolean resetDataSheet(){
		try {
			File srcFile = new File("ReconciliationExcel_New.xlsx");
			File destFile = new File("ReconciliationExcel_Old.xlsx");
			if(srcFile.exists()){
				FileUtils.copyFile(srcFile, destFile);
				srcFile.delete();
				System.out.println("<<<File deleted>>>");
				fileAvailable = true;
			}else{
				System.out.println("<<<File not available>>>");
				fileAvailable = false;
			}
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public void getTableData(String AppName, String rowCount, String diffBtwnSystems, String sheetName) throws Throwable{

		DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date date2 = new Date();
		String dateStr = dateFormat2.format(date2);
		InputStream ExcelFileToRead = null;
		try {
			ExcelFileToRead = new FileInputStream(resultFilePath);
			book = new XSSFWorkbook(ExcelFileToRead);
		} catch (Exception e1) {
			book =new XSSFWorkbook();
		}
		System.out.println("<<<" + sheetName.trim().toUpperCase() + ">>>");
		sheet = book.createSheet(sheetName.trim().toUpperCase());
		int lastRowNum = sheet.getLastRowNum();
		row0 = sheet.createRow(0);
//		row0.createCell(0).setCellValue(AppName); //Changed to sheetname 
		row0.createCell(0).setCellValue(sheetName.trim().toUpperCase());
		row0.createCell(1).setCellValue("Row Count:"+rowCount);
		row1 = sheet.createRow(1);
		row1.createCell(0).setCellValue("DATE & TIME");
		row1.createCell(1).setCellValue("Diff Between Systems");

		String headinglocator = "//tr[td[@class='Heading2']]/td";
		String rowLocator = "//tr[td[not(@class)] and td[not(contains(text(), 'Row Count'))]]";

		List<Map<String, String>> allRowsList = new ArrayList<Map<String,String>>();

		List<WebElement> headingElementsList = driver.findElements(By.xpath(headinglocator));
		List<String> headingsList = new ArrayList<String>();

		for(WebElement element : headingElementsList) {
			headingsList.add(element.getText().trim());
		}
		for(int k=2, s=0;s<headingsList.size();k++, s++)
		{
			row1.createCell(k).setCellValue(headingsList.get(s));
		}
		Xls_Reader xls;
		String name;
		Object[][] obj = null;
		String requiredKey = null;
		int col = 0;
		if(fileAvailable){
			xls = new Xls_Reader("ReconciliationExcel_Old.xlsx");
			name = (sheetName.trim().length() > 31)? sheetName.trim().toUpperCase().substring(0, 31): sheetName.trim().toUpperCase();
			obj = TestUtil.getData(sheetName.trim().toUpperCase(), xls , name);
			switch(name.toUpperCase()){
			case "DRAGON_POLICY_TRX - DRAGON_TRAN":
				requiredKey = "PolicyNum";
				col = 0;break;
			case "DRAGON_TRANSACTION_STATS - DW":
				requiredKey = "PolicyNum";
				col = 0;break;
			case "DRAGON_TRANSACTION_STATS - BILL":
				requiredKey = "PolicyNum";
				col = 0;break;
			case "DRAGON_TRANSACTION_STATS - MAS":
				requiredKey = "PolicyNum";
				col = 0;break;
			case "TRANSACTION INCONSISTENCIES":
				requiredKey = "Policy_Trx_ID";
				col = 1;break;
			case "STATS TO BILLING TABLES RECONCI":
				requiredKey = "POLICY_TRX_ID";
				col = 2;break;
			}
		}
		if(Integer.valueOf(rowCount) > 0){
			List<WebElement> rowElementsList = driver.findElements(By.xpath(rowLocator));
			int currentRowCount = 1;
			for(int i=1;i<=rowElementsList.size();i++)
			{
				List<WebElement> cellsOfEachRow = driver.findElements(By.xpath(rowLocator+"["+i+"]/td"));
				List<String> eachRowCellsText = new ArrayList<String>();
				for(WebElement element1 : cellsOfEachRow) {
					eachRowCellsText.add(element1.getText().trim());
				}
				boolean status = true;

				boolean validDate = false;
				if(AppName.equalsIgnoreCase("MAS Reconciliation")){
					String date = (!eachRowCellsText.get(3).equalsIgnoreCase(""))?eachRowCellsText.get(3):eachRowCellsText.get(8);
					Date effectiveDate = new SimpleDateFormat("MM/dd/yyyy").parse(date);
					if(effectiveDate.compareTo(new Date("01/01/2015")) > 0){
						validDate = true;
					}
				}
				if(AppName.equalsIgnoreCase("Transaction Inconsistencies page")){
					String date = eachRowCellsText.get(4);
					Date effectiveDate = new SimpleDateFormat("MM/dd/yyyy").parse(date);
					if(effectiveDate.compareTo(new Date("01/01/2015")) > 0){
						validDate = true;
					}
				}
				if(fileAvailable){
					loop:
						for(int j = 0; j < obj.length; j++){
							Hashtable<String, String> table = (Hashtable<String, String>) obj[j][0];
							String value = table.get(requiredKey);
							if(value.equalsIgnoreCase(eachRowCellsText.get(col))){
								status = false;
								break loop;
							}
						}
				}

				if((AppName.equalsIgnoreCase("MAS Reconciliation") && validDate) || 
						(AppName.equalsIgnoreCase("Transaction Inconsistencies page") && validDate)||
						AppName.equalsIgnoreCase("STATS to Billing Tables Reconciliation")){
					row2 = sheet.createRow(lastRowNum+1+currentRowCount);
					row2.createCell(0).setCellValue(dateStr);
					row2.createCell(1).setCellValue(diffBtwnSystems);
					for(int l=2, m=0;m<eachRowCellsText.size();l++, m++)
					{
						row2.createCell(l).setCellValue(eachRowCellsText.get(m));
						if(fileAvailable && status){
							CellStyle cellStyle = book.createCellStyle();
							cellStyle = book.createCellStyle();
							cellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
							cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
							row2.getCell(0).setCellStyle(cellStyle);
							row2.getCell(1).setCellStyle(cellStyle);
							row2.getCell(l).setCellStyle(cellStyle);
						}
					}	
					currentRowCount++;
					validDate = false;
				}
			}
		}
		try {
			if(ExcelFileToRead != null) ExcelFileToRead.close();
			outputStream = new FileOutputStream(resultFilePath);
			book.write(outputStream);
			outputStream.flush();
			outputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.err.println("Result file: "+ resultFilePath + " is not found, please check once again");
			e.printStackTrace();
		}


	}

	@SuppressWarnings({ "deprecation", "resource" })
	public static void createEmailSheet() throws Exception{
//		int requiredDelta = Integer.valueOf(ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "requiredDelta"));
		XSSFWorkbook wb = new XSSFWorkbook("ReconciliationExcel_New.xlsx");
		FileOutputStream oStream;
//		boolean status = false;
		boolean iteration = true;
		int noOfSheets = wb.getNumberOfSheets();
//		System.out.println("Number of sheets initially:::" + noOfSheets);
//		int currentSheetCounter = 0;
//		System.out.println("Number of sheets in iteration:::" + wb.getNumberOfSheets());
//		if(currentSheetCounter < wb.getNumberOfSheets()){
		for(int currentSheetCounter = 0;currentSheetCounter<wb.getNumberOfSheets(); currentSheetCounter++){
			/*if(wb.getNumberOfSheets() == 0){
				System.out.println("Inside break");
				Sheet tempSheet = wb.createSheet("Default sheet");
				tempSheet.createRow(0).createCell(0).setCellValue("");
				break;
			}*/
			Sheet sheet = wb.getSheetAt(currentSheetCounter);
			int rowCount = 0;
			int totalRows = sheet.getLastRowNum();

			for(int rowCounter = 2; rowCounter<=totalRows; rowCounter++){
				Row row = sheet.getRow(rowCounter);
				Cell cell = row.getCell(0);
				CellStyle style = cell.getCellStyle();
				if(style.getFillForegroundColor() == 13) rowCount++;
			}
			/*if(rowCount >= requiredDelta) {
				status = true;
				currentSheetCounter++;
			}else{
				wb.removeSheetAt(currentSheetCounter);
			}*/
			if(rowCount == 0) {
				Row row0 = sheet.getRow(0);
				row0.createCell(2).setCellValue("No Changes in current sheet");
				CellStyle cellStyle = wb.createCellStyle();
				XSSFFont font = wb.createFont();
		        font.setColor(HSSFColor.RED.index);
		        font.setBold(true);
		        font.setFontHeightInPoints((short) 12);
		        cellStyle.setFont(font);
				row0.getCell(2).setCellStyle(cellStyle);
			}
		}
		oStream = new FileOutputStream(new File("ReconciliationExcel_Email.xlsx"));
		wb.write(oStream);
		oStream.flush();
		oStream.close();
//		if(!status) throw new Exception("Non sheet has delta greater than counter#" + requiredDelta);
	}

}
