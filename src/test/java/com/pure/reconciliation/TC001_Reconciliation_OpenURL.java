package com.pure.reconciliation;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.lang.reflect.Method;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;

public class TC001_Reconciliation_OpenURL extends ActionEngine {
	boolean statusDW = false, statusSB = false, statusTI = false;
	File file = new File("EmailNotification.html");
	PrintStream fileStream;
	int counter = 1;
	@BeforeClass
	public void beforeClass(){
		try {
			if(file.exists()) file.delete();
			file.createNewFile();
			fileStream = new PrintStream(file);
			fileStream.println("<html>");
			fileStream.println("<body>");
			fileStream.println("<style>");
			fileStream.println("p  {");
			fileStream.println("    font-family: system-ui;");
			fileStream.println("}");
			fileStream.println("</style>");
			fileStream.println("<p>Hello everyone,<br><br>");
			fileStream.println("Below is result:</p><br>");
			fileStream.println("<table>");
			fileStream.println("<style>");
			fileStream.println("table {");
			fileStream.println("    border-collapse: collapse;");
			fileStream.println("}");
			fileStream.println("table, td, th {");
			fileStream.println("border: 1px solid black;");
			fileStream.println("font-family: system-ui;");
			fileStream.println("}");
			fileStream.println("th, td {");
			fileStream.println("text-align: left");
			fileStream.println("}");
			fileStream.println("</style>");
			fileStream.println("<tbody>");
			fileStream.println("<tr>");
			fileStream.println("<th style='width: 10%;'>S.No.</th>");
			fileStream.println("<th style='width: 70%;'>Url</th>");
			fileStream.println("<th style='width: 20%;'>Status</th>");
			fileStream.println("</tr>");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public void afterClass() throws Exception{
		fileStream.println("</tbody>");
		fileStream.println("</table>");
		fileStream.println("<br><br>");
		fileStream.println("*********************************<br>");
		fileStream.println("This is auto-generated email notification<br>");
		fileStream.println("*********************************<br>");
		fileStream.println("</body>");
		fileStream.println("</html>");
		fileStream.close();
		if(statusDW && statusSB && statusTI){
			// Email notification via jenkins is triggered on Success
			throw new Exception("None of weblink is down, throwing intentional exception to fail the build");
		}
	}
	@BeforeMethod
	public void beforeTest(Method method) {
		switch(method.getName().toUpperCase()){
		case "MAS_RECONCILIATION": 
			reporter.createHeader("MAS_RECONCILIATION");
			break;
		case "STATS_BILLING_RECONCILIATION": 
			reporter.createHeader("STATS_BILLING_RECONCILIATION");
			break;
		case "TRANSACTION_INCONSISTENCIES_RECONCILIATION": 
			reporter.createHeader("TRANSACTION_INCONSISTENCIES_RECONCILIATION");
			break;
		}
	}
	@Test(priority=1)
	public void MAS_Reconciliation()
			throws Throwable {
		// this.reporter.initTestCaseDescription(data.get("TestCase_Description"));	
		MASReconciliationLib masReconciliationLib = new MASReconciliationLib(driver, reporter);
		openApplication("DW");
		masReconciliationLib.verifyWebPageIsOpened("DW");
		statusDW = true;
	}
	@Test(priority=2)
	public void Stats_Billing_Reconciliation()
			throws Throwable {
		// this.reporter.initTestCaseDescription(data.get("TestCase_Description"));	
		MASReconciliationLib masReconciliationLib = new MASReconciliationLib(driver, reporter);
		openApplication("SB");
		masReconciliationLib.verifyWebPageIsOpened("SB");	
		statusSB = true;
	}
	@Test(priority=3)
	public void Transaction_Inconsistencies_Reconciliation()
			throws Throwable {
		// this.reporter.initTestCaseDescription(data.get("TestCase_Description"));
		MASReconciliationLib masReconciliationLib = new MASReconciliationLib(driver, reporter);
		openApplication("TI");
		masReconciliationLib.verifyWebPageIsOpened("TI");
		statusTI = true;
	}
	@AfterMethod
	public void afterMethod(ITestResult result){
		LOG.info("After method executing...");
		String status="";
		String url = "";
		switch (result.getMethod().getMethodName()) {
		case "MAS_Reconciliation":
			url = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "dragonDWUrl");
			break;
		case "Transaction_Inconsistencies_Reconciliation":
			url = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "dragonSBUrl");
			break;
		case "Stats_Billing_Reconciliation":
			url = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "dragonTIUrl");
			break;
		}
		
		switch (result.getStatus()) {
		case ITestResult.SUCCESS:
			status = "SUCCESS";
			break;
		case ITestResult.FAILURE:
			status = "FAILURE";
			break;
		}
		fileStream.println("<tr>");
		fileStream.println("<td>" + counter++ + "</td>");
		fileStream.println("<td>" + url + "</td>");
		if(status.equalsIgnoreCase("FAILURE")){
			fileStream.println("<td style='color: red;'><b>" + status + "<b></td>");
		}else{
		fileStream.println("<td style='color: green;'>" + status + "</td>");
		}
		fileStream.println("</tr>");
	}
}

