package com.pure.claim.unverifiedPolicy;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.EnumLOBType;
import com.pure.claim.NewContactPageLib;
import com.pure.claim.NewVehiclePageLib;
import com.pure.claim.SearchAddressBookPageLib;
import com.pure.claim.lifeCycle.ExposuresPage;
import com.pure.report.CReporter;

public class UnverifiedPolicyMainPageLib extends ActionEngine {

	public UnverifiedPolicyMainPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void setLOBType(EnumLOBType lobType) throws Throwable {
		switch (lobType) {
		case Collections:
			type(UnverifiedPolicyMainPage.lobTypeCombo, "Collections",
					"policyType");
			type(UnverifiedPolicyMainPage.lobTypeCombo, Keys.TAB, "Tab key");
			break;
		case ExcessLiability:
			type(UnverifiedPolicyMainPage.lobTypeCombo, "Excess liability",
					"policyType");
			type(UnverifiedPolicyMainPage.lobTypeCombo, Keys.TAB, "Tab key");
			break;
		case HomeConstruction:
			type(UnverifiedPolicyMainPage.lobTypeCombo, "Home Construction",
					"policyType");
			type(UnverifiedPolicyMainPage.lobTypeCombo, Keys.TAB, "Tab key");
			break;
		case HomeOwners:
			type(UnverifiedPolicyMainPage.lobTypeCombo, "HomeOwner's",
					"policyType");
			type(UnverifiedPolicyMainPage.lobTypeCombo, Keys.TAB, "Tab key");
			break;
		case MemberAdvocateServiceRequest:
			type(UnverifiedPolicyMainPage.lobTypeCombo,
					"Member Advocate Service Request", "policyType");
			type(UnverifiedPolicyMainPage.lobTypeCombo, Keys.TAB, "Tab key");
			break;
		case PersonalAuto:
			type(UnverifiedPolicyMainPage.lobTypeCombo, "Personal auto",
					"policyType");
			type(UnverifiedPolicyMainPage.lobTypeCombo,Keys.TAB, "Tab key");
			break;
		case SurplusLinesHomeOwners:
			type(UnverifiedPolicyMainPage.lobTypeCombo,
					"Surplus Lines HomeOwner's", "policyType");
			type(UnverifiedPolicyMainPage.lobTypeCombo, Keys.TAB, "Tab key");
			break;
		case Watercraft:
			type(UnverifiedPolicyMainPage.lobTypeCombo, "Watercraft",
					"policyType");
			type(UnverifiedPolicyMainPage.lobTypeCombo, Keys.TAB, "Tab key");
			break;
		case WorkersComp:
			type(UnverifiedPolicyMainPage.lobTypeCombo, "Workers' comp",
					"policyType");
			type(UnverifiedPolicyMainPage.lobTypeCombo, Keys.TAB, "Tab key");
			break;
		}
	}
	public String getLOBType(){
		String lobTypeSelected = driver.findElement(UnverifiedPolicyMainPage.lobTypeCombo).getText();
		return lobTypeSelected;
	}

	public void setPolicyNumber(String policyNumber) throws Throwable{
		type(UnverifiedPolicyMainPage.policyNum, policyNumber, "policyNum");
		type(UnverifiedPolicyMainPage.policyNum, Keys.TAB, "Tab key");
		waitForVisibilityOfElement(UnverifiedPolicyMainPage.effectiveDate, "effectiveDate");
	}

	public String getPolicyNumber() {
		String lobTypeSelected = driver.findElement(
				UnverifiedPolicyMainPage.policyNum).getText();
		return lobTypeSelected;
	}
	
	public void setMemberID(String memberID) throws Throwable{
		type(UnverifiedPolicyMainPage.memberID, memberID, "Member ID");
		type(UnverifiedPolicyMainPage.memberID, Keys.TAB, "Tab key");
		waitForVisibilityOfElement(UnverifiedPolicyMainPage.insuredNameListIcon, "Insured NameList Icon");
	}
	
	public void setTypeOfClaim(String claimType) throws Throwable{
		switch(claimType.trim()){
		case "Auto - Auto First and Final":
			click(UnverifiedPolicyMainPage.autoFirstAndFinalClaimType, "Claim type - Auto First and Final");
			break;
		case "Auto - Quick Claim Auto":
			click(UnverifiedPolicyMainPage.autoQuickClaimAutoClaimType, "Claim type - Auto Quick Claim");
			break;
	}
	}

	public String getDateOfLoss() {
		String dateOfLoss = driver.findElement(
				UnverifiedPolicyMainPage.dateOfLoss).getText();
		return dateOfLoss;
	}

	public void setDateOfLoss(String dateOfLoss) throws Throwable {
		type(UnverifiedPolicyMainPage.dateOfLoss, dateOfLoss, "DateOfLoss");
		type(UnverifiedPolicyMainPage.dateOfLoss, Keys.TAB, "Tab key");
	}
	
	public void setDateOfRequest(String dateOfRequest) throws Throwable {
		type(UnverifiedPolicyMainPage.dateOfRequest, dateOfRequest, "DateOfRequest");
		type(UnverifiedPolicyMainPage.dateOfRequest, Keys.TAB, "Tab key");
	}

	public String getEffectiveDate(){
		String expirationDate = driver.findElement(UnverifiedPolicyMainPage.effectiveDate).getText();
		return expirationDate;
	}
	public void setEffectiveDate(String effectiveDate) throws Throwable{
		type(UnverifiedPolicyMainPage.effectiveDate,effectiveDate , "EffectiveDate");
	}	

	public String getExpirationDate(){
		String expirationDate = driver.findElement(UnverifiedPolicyMainPage.expirationDate).getText();
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) throws Throwable {
		type(UnverifiedPolicyMainPage.expirationDate, expirationDate,
				"ExpirationDate");
	}
	
	public void setProjectType(String projectType) throws Throwable {
		type(UnverifiedPolicyMainPage.projectType, projectType,
				"projectType");
		type(UnverifiedPolicyMainPage.projectType, Keys.TAB,
				"Tab keys");
	}

	public void selectPropertyRadio(String propertyClaimRadio) throws Throwable {
		click(UnverifiedPolicyMainPage.propertyClaimRadio,
				"Property ClaimRadio");
	}

	public void clickAddEndoresement(String addEndorsement) throws Throwable {
		click(UnverifiedPolicyMainPage.addEndorsementBtn,
				"Add Endorsement Button");
	}

	public void clickRemoveEndorsement(String endorseRemoveBtn)
			throws Throwable {
		click(UnverifiedPolicyMainPage.endorseRemoveBtn,
				"Remove Endorsement Button");
	}

	public void clickAddLocationBtn(String addLocationBtn) throws Throwable {
		click(UnverifiedPolicyMainPage.addLocationBtn, "Add LocationBtn");
	}

	public void clickRemoveLocationBtn(String removeLocationBtn)
			throws Throwable {
		click(UnverifiedPolicyMainPage.removeLocationBtn, "Remove LocationBtn");
	}

	public String getInsuredName() {
		String insuredName = driver.findElement(
				UnverifiedPolicyMainPage.insuredName).getText();
		return insuredName;
	}
	public void fillInsuredDetails(Hashtable<String, String> data) throws Throwable{
		click(UnverifiedPolicyMainPage.insuredNameListIcon, "insuredNameListIcon");
		click(UnverifiedPolicyMainPage.newPersonFromInsuredNameList, "New Person");
		new NewContactPageLib(driver, reporter).fillContactDetails(data);
	}
	public void setMemberOrProspect(String memberOrProspect) throws Throwable{
		type(UnverifiedPolicyMainPage.memberOrProspect, memberOrProspect, "Member Or Prospect");
		type(UnverifiedPolicyMainPage.memberOrProspect, Keys.TAB, "Tab key");
	}
	public void fillGeneralConractorDetails(Hashtable<String, String> data) throws Throwable{
		click(UnverifiedPolicyMainPage.generalContractorListIcon, "generalContractorListIcon");
		click(UnverifiedPolicyMainPage.searchFromGeneralContractorList, "Search");
		new SearchAddressBookPageLib(driver, reporter).searchNSelectFromAddressBook(data);
		if(data.get("gcWaived")!=null){
			if(data.get("gcWaived").equalsIgnoreCase("yes")){
				click(UnverifiedPolicyMainPage.subroGCWaivedYes, "Subro GC waived Yes");
			}else{
				click(UnverifiedPolicyMainPage.subroGCWaivedNo, "Subro GC waived No");
			}
		}
	}
	public String getEndorseFormName() throws Throwable {
		return getText(UnverifiedPolicyMainPage.endorseFormNumber, "Form Name");
	}

	public void setEndorseFormName(String endorseFormNumber) throws Throwable {
		type(UnverifiedPolicyMainPage.endorseFormNumber, endorseFormNumber,
				"Form Number");
	}

	public String getEndorseComment() throws Throwable {
		return getText(UnverifiedPolicyMainPage.endorseComment,
				"Endorse Comment");
	}

	public void setEndorseComment(String endorseComment) throws Throwable {
		type(UnverifiedPolicyMainPage.endorseComment, endorseComment,
				"Endore Comment");
	}

	public String getEndorsementEffectiveDate() throws Throwable {
		return getText(UnverifiedPolicyMainPage.endorseEffectiveDate,
				"Endoresement Effective date");
	}

	public void setEndorsementEffectiveDate(String endorseEffectiveDate)
			throws Throwable {
		type(UnverifiedPolicyMainPage.endorseEffectiveDate,
				endorseEffectiveDate, "Endoresement Effective date");
	}

	public String getEndorsementExpiryDate() throws Throwable {
		return getText(UnverifiedPolicyMainPage.endorseExpiryDate,
				"Endoresement Expiry date");
	}

	public void setEndorsementExpiryDate(String endorseExpiryDate)
			throws Throwable {
		type(UnverifiedPolicyMainPage.endorseExpiryDate, endorseExpiryDate,
				"Endoresement Expiry date");
	}

	public String getEndorsementDescription() throws Throwable {
		return getText(UnverifiedPolicyMainPage.endorseDescription,
				"Endoresement Description");
	}

	public void setEndorsementDescription(String endorseDescription)
			throws Throwable {
		type(UnverifiedPolicyMainPage.endorseDescription, endorseDescription,
				"Endoresement Description");
	}

	public String getLocationNumber() throws Throwable {
		return getText(UnverifiedPolicyMainPage.addLocationNumber,
				"Location Number");
	}

	public void setLocationNumber(String addLocationNumber) throws Throwable {
		type(UnverifiedPolicyMainPage.addLocationNumber, addLocationNumber,
				"Location Number");
	}

	public String getLocationAddress1() throws Throwable {
		return getText(UnverifiedPolicyMainPage.addLocationAddress1,
				"Location Address1");
	}

	public void setLocationAddress1(String addLocationAddress1)
			throws Throwable {
		type(UnverifiedPolicyMainPage.addLocationAddress1, addLocationAddress1,
				"Location Address1");
	}

	public String getLocationAddress2() throws Throwable {
		return getText(UnverifiedPolicyMainPage.addLocationAddress2,
				"Location Address2");
	}

	public void setLocationAddress2(String addLocationAddress2)
			throws Throwable {
		type(UnverifiedPolicyMainPage.addLocationAddress2, addLocationAddress2,
				"Location Address2");
	}

	public String getLocationDescription() throws Throwable {
		return getText(UnverifiedPolicyMainPage.addLocationDescription,
				"Location Description");
	}

	public void setLocationDescription(String addLocationDescription)
			throws Throwable {
		type(UnverifiedPolicyMainPage.addLocationDescription,
				addLocationDescription, "Location Description");
	}

	public String getLocationCity() throws Throwable {
		return getText(UnverifiedPolicyMainPage.addLocationCity,
				"Location City");
	}

	public void setLocationCity(String addLocationCity) throws Throwable {
		type(UnverifiedPolicyMainPage.addLocationCity, addLocationCity,
				"Location City");
	}

	public String getLocationZip() throws Throwable {
		return getText(UnverifiedPolicyMainPage.addLocationZip, "Location Zip");
	}

	public void setLocationZip(String addLocationZip) throws Throwable {
		type(UnverifiedPolicyMainPage.addLocationZip, addLocationZip,
				"Location Zip");
	}

	public String getLocationCountry() throws Throwable {
		return getText(UnverifiedPolicyMainPage.addLocationCountry,
				"Location Country");
	}

	public void setLocationCountry(String addLocationCountry) throws Throwable {
		type(UnverifiedPolicyMainPage.addLocationCountry, addLocationCountry,
				"Location Country");
	}

	public void setInsuredName(String insuredName) throws Throwable {
		if (driver.findElement(UnverifiedPolicyMainPage.insuredName).getText()
				.equalsIgnoreCase("none")) {
			click(UnverifiedPolicyMainPage.insuredNameListIcon,
					"insuredNameListIcon");
			selectByValue(
					UnverifiedPolicyMainPage.newPersonFromInsuredNameList,
					"New Person", "newPersonFromInsuredNameList");
			NewContactPageLib NewContactPageLib = new NewContactPageLib(driver, reporter);
			NewContactPageLib.setLastName(insuredName);
			NewContactPageLib.clickOK();

		}

		type(UnverifiedPolicyMainPage.insuredName, insuredName, "Insured Name");
	}

	
	/*
	 * This method is to select the Home owners type from the the un-verified
	 * policy flow
	 */
	public void selectHomeOwnersLob(EnumLOBType lobType, String policyNumber,
			String propertyClaimRadio, String dateOfLoss) throws Throwable {
		setLOBType(lobType);
		setPolicyNumber(policyNumber);
		selectPropertyRadio(propertyClaimRadio);
		setDateOfLoss(dateOfLoss);
	}

	/*
	 * This method is to set the un-verified policy's Endorsement parameters
	 */
	public void addEndorsement(String endorseFormNumber,
			String endorseDescription, String getEndorsementDescription,
			String endorseEffectiveDate, String endorseExpiryDate,
			String endorseComment) throws Throwable {
		/* clickAddEndoresement(addEndorsementBtn); */
		WebDriverWait wait = new WebDriverWait(driver, 30);
		LOG.info("Waiting for element");
		LOG.info("Locator is Visible :: "
				+ UnverifiedPolicyMainPage.endorseFormNumber);
		wait.until(ExpectedConditions.elementToBeClickable(driver
				.findElement(UnverifiedPolicyMainPage.endorseFormNumber)));
		LOG.info("Clicked on the Locator");
		setEndorseFormName(endorseFormNumber);
		setEndorsementDescription(endorseDescription);
		setEndorsementEffectiveDate(endorseEffectiveDate);
		setEndorsementExpiryDate(endorseExpiryDate);
		setEndorseComment(endorseComment);

	}

	/*
	 * This method is to set the un-verified policy's Location parameters
	 */
	public void addLocation(String addLocationNumber,
			String addLocationAddress1, String addLocationAddress2,
			String addLocationCity, String addLocationZip,
			String addLocationCountry, String addLocationDescription)
			throws Throwable {
		/* clickAddLocationBtn(addLocationBtn); */
		WebDriverWait wait = new WebDriverWait(driver, 40);
		LOG.info("Waiting for element");
		LOG.info("Locator is Visible :: "
				+ UnverifiedPolicyMainPage.addLocationNumber);
		wait.until(ExpectedConditions.elementToBeClickable(driver
				.findElement(UnverifiedPolicyMainPage.addLocationNumber)));
		LOG.info("Clicked on the Locator");
		setLocationNumber(addLocationNumber);
		setLocationAddress1(addLocationAddress1);
		setLocationAddress2(addLocationAddress2);
		setLocationCity(addLocationCity);
		setLocationCountry(addLocationCountry);
		setLocationZip(addLocationZip);
		setLocationDescription(addLocationDescription);

	}

	public void addPolicyLevelCoverages(Hashtable<String, String> data)throws Throwable{
		String coveragetype,deductible,exposurelimit,incidentlimit,policyCoverageNote;
		coveragetype 	= data.get("coveragetype");
		deductible		= data.get("deductible");
		exposurelimit	= data.get("exposurelimit");
		incidentlimit	= data.get("incidentlimit");
		policyCoverageNote = data.get("policyCoverageNote");
		
		click(UnverifiedPolicyMainPage.addBtnPolicyCoverage,"addBtnPolicyCoverage");
		waitForMask();
		setPolicyLevelCoverageType(coveragetype);
		setPolicyLevelCoverageDeductible(deductible);
		setPolicyLevelCoverageExposureLimit(exposurelimit);
		setPolicyLevelCoverageIncidentLimit(incidentlimit);
		setPolicyLevelCoverageNote(policyCoverageNote);
	}
	
	public void clickAddVessels() throws Throwable{
		click(UnverifiedPolicyMainPage.addBtnVessels,"addBtnVessels");
		waitForMask();
	}
	
	//Will return column w.r.t. last row in table
		private List<WebElement> getColumns() throws Throwable{
			waitForVisibilityOfElement(UnverifiedPolicyMainPage.policyLevelCoverageDiv, "Policy level coverage div");
			List<WebElement> tableRows = null;
			List<WebElement> tableColumns = null;
			boolean flag = false;
			try{
				tableRows = driver.findElement(UnverifiedPolicyMainPage.policyLevelCoverageDiv).findElement(By.tagName("table")).findElements(By.tagName("tr"));
				tableColumns = tableRows.get(tableRows.size() - 1).findElements(By.tagName("td"));
				flag = true;
			}catch(Exception e){
				flag = false;
			}
			finally{
				if(!flag){
					reporter.failureReport("Find division tag for the element - "+ExposuresPage.exposuresDiv, "unable to find the Division tag for "+ExposuresPage.exposuresDiv, driver);
				}
			}
			return tableColumns;
		}
		
		public void setPolicyLevelCoverageType(String type) throws Throwable{
			WebElement elem = getColumns().get(1); 
			Actions actions = new Actions(driver);
			actions.moveToElement(elem).doubleClick(elem).perform();
			type(UnverifiedPolicyMainPage.policyLevelCoverageType,type,"Coverage type");
			actions.sendKeys(Keys.TAB).perform();
			waitForMask();
			reporter.SuccessReport("Enter coverage type", "Entered '"+type +"' in coverage type."  );
		}
		
		public void setPolicyLevelCoverageDeductible(String deductible) throws Throwable{
			WebElement elem = getColumns().get(2); 
			new Actions(driver).doubleClick(elem).sendKeys(deductible)
			.build().perform();
			reporter.SuccessReport("Enter coverage deductible", "Entered '"+deductible +"' in coverage deductible."  );
		}
		
		public void setPolicyLevelCoverageExposureLimit(String exposureLimit) throws Throwable{
			WebElement elem = getColumns().get(3); 
			new Actions(driver).doubleClick(elem).sendKeys(exposureLimit)
			.build().perform();
			reporter.SuccessReport("Enter coverage exposure limit", "Entered '"+exposureLimit +"' in coverage exposure limit."  );
		}
		
		public void setPolicyLevelCoverageIncidentLimit(String incidentLimit) throws Throwable{
			WebElement elem = getColumns().get(4); 
			new Actions(driver).doubleClick(elem).sendKeys(incidentLimit)
			.build().perform();
			reporter.SuccessReport("Enter coverage incident limit", "Entered '"+incidentLimit +"' in coverage incident limit."  );
		}
		
		public void setPolicyLevelCoverageNote(String note) throws Throwable{
			WebElement elem = getColumns().get(5); 
			Actions actions = new Actions(driver);
			actions.doubleClick(elem).sendKeys(note)
			.build().perform();
			actions.sendKeys(Keys.TAB).perform();
			waitForMask();
			reporter.SuccessReport("Enter coverage additional note", "Entered '"+note +"' in coverage additional note."  );
		}
		
		public void addVehicle(Hashtable<String, String> data) throws Throwable{
			click(UnverifiedPolicyMainPage.addVehiclesBtn, "Add Vehicle button");
			NewVehiclePageLib  newVehiclePageLib = new NewVehiclePageLib(driver, reporter);
			newVehiclePageLib.setMake(data.get("vehicleMake"));
			newVehiclePageLib.setModel(data.get("vehicleModel"));
			newVehiclePageLib.setYear(data.get("vehicleYear"));
			newVehiclePageLib.clickOk();
		}
}