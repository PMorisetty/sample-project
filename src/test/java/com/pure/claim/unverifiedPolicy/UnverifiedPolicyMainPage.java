package com.pure.claim.unverifiedPolicy;

import org.openqa.selenium.By;

public class UnverifiedPolicyMainPage {
	static By lobTypeCombo, policyNum, memberID, dateOfLoss, dateOfRequest;
	static By lossTime, effectiveDate, expirationDate;
	static By insuredName, insuredNameListIcon;
	static By generalContractorListIcon;
	static By searchFromGeneralContractorList;
	static By newPersonFromInsuredNameList;/*
											 * Element available only when
											 * insuredListIcon is clicked
											 */
	static By basicInformationText,propertyClaimRadio,addEndorsementBtn,
	endorseFormNumber,
	endorseDescription,endorseEffectiveDate,
	endorseExpiryDate,endorseComment,endorseRemoveBtn,
	addLocationsBtn,addLocationNumber,addLocationAddress1,addLocationAddress2,
	addLocationZip,
	addLocationCity,addLocationDescription,
	addLocationCountry,addLocationBtn,removeLocationBtn,
	typeOfClaimRadiobtn,policyLevelCoverageDiv,addBtnPolicyCoverage,policyCoverageType,
	policyLevelCoverageType,policyCoverageDeductible,policyCoverageExposureLimit,policyCoverageIncidentLimit,
	policyCoverageNotes, coverageTypeInputBox, projectType,
	subroGCWaivedYes, subroGCWaivedNo, addVehiclesBtn, memberOrProspect,
	addBtnVessels;
	//--------------------claim type - auto-----------------------
	static By autoFirstAndFinalClaimType;
	static By autoQuickClaimAutoClaimType;
		
	static {
		lobTypeCombo = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:Type-inputEl");
		policyNum = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:PolicyNumber-inputEl");
		memberID = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:MemberID-inputEl");
		dateOfLoss = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:Claim_LossDate-inputEl");
		dateOfRequest = By
				.xpath("//input[@id='FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:Claim_Requestdate-inputEl']");
								 
		lossTime = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:Claim_lossTime-inputEl");
		effectiveDate = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:NewClaimPolicyGeneralDV:EffectiveDate-inputEl");
		expirationDate = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:NewClaimPolicyGeneralDV:ExpirationDate-inputEl");
		insuredName = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:NewClaimPolicyGeneralDV:Insured_Name-inputEl");
		insuredNameListIcon = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:NewClaimPolicyGeneralDV:Insured_Name:Insured_NameMenuIcon");
		generalContractorListIcon = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:NewClaimPolicyGeneralDV:generalContractorInput:generalContractorInputMenuIcon");
		newPersonFromInsuredNameList = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:NewClaimPolicyGeneralDV:Insured_Name:ClaimNewPersonOnlyPickerMenuItemSet:ClaimNewPersonOnlyPickerMenuItemSet_NewPersonMenuItem-textEl");
		searchFromGeneralContractorList = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:NewClaimPolicyGeneralDV:generalContractorInput:MenuItem_Search-textEl");
		basicInformationText = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:NewClaimPolicyGeneralDV:0");
		propertyClaimRadio = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:ClaimLossType_option0-inputEl");
		addEndorsementBtn = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimEndorsementsLV_tb:Add-btnInnerEl");
		endorseFormNumber = By
				.id("NewClaimWizard_NewEndorsementPopup:NewClaimWizard_NewEndorsementDetailScreen:EndorsementDetailDV:FormNumber-inputEl");
		endorseDescription = By
				.id("NewClaimWizard_NewEndorsementPopup:NewClaimWizard_NewEndorsementDetailScreen:EndorsementDetailDV:Description-inputEl");
		endorseEffectiveDate = By
				.id("NewClaimWizard_NewEndorsementPopup:NewClaimWizard_NewEndorsementDetailScreen:EndorsementDetailDV:EffectiveDate-inputEl");
		endorseExpiryDate = By
				.id("NewClaimWizard_NewEndorsementPopup:NewClaimWizard_NewEndorsementDetailScreen:EndorsementDetailDV:ExpirationDate-inputEl");
		endorseComment = By
				.id("NewClaimWizard_NewEndorsementPopup:NewClaimWizard_NewEndorsementDetailScreen:EndorsementDetailDV:Comments-inputEl");
		endorseRemoveBtn = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimEndorsementsLV_tb:Remove-btnInnerEl");
		addLocationsBtn = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:PolicyLocationLDV_tb:Add-btnInnerEl");
		addLocationNumber = By
				.id("PolicyLocationPopup:PolicyLocationScreen:LocationNumber");
		addLocationAddress1 = By
				.id("PolicyLocationPopup:PolicyLocationScreen:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:AddressLine1-inputEl");
		addLocationAddress2 = By
				.id("PolicyLocationPopup:PolicyLocationScreen:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:AddressLine2-inputEl");
		addLocationZip = By
				.id("PolicyLocationPopup:PolicyLocationScreen:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:PostalCode-inputEl");
		addLocationCity = By
				.id("PolicyLocationPopup:PolicyLocationScreen:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:City-inputEl");
		addLocationDescription = By
				.id("PolicyLocationPopup:PolicyLocationScreen:CCAddressInputSet:globalAddressContainer:Address_Description-inputEl");
		addLocationCountry = By
				.id("PolicyLocationPopup:PolicyLocationScreen:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:Country-inputEl");
		addLocationBtn = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:PolicyLocationLDV_tb:Add-btnInnerEl");
		removeLocationBtn = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:PolicyLocationLDV_tb:Remove-btnInnerEl");
		typeOfClaimRadiobtn = By
				.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:ClaimLossType_option0-inputEl");
	
		addBtnPolicyCoverage = By.xpath("//span[contains(@id,'PolicyCoveragesLV_tb:Add-btnEl')]"); 
//				By.cssSelector("input[id*=\"PolicyCoveragesLV_tb:Add-btnEl\"]");
	
		policyCoverageType = By
				.xpath("//div[contains(@id,'PolicyCoveragesLV-body')]//td[2]//div");
		coverageTypeInputBox = By.cssSelector("input[name*=\"CoverageType\"]");
		
		policyCoverageDeductible = By
				.xpath("//div[contains(@id,'PolicyCoveragesLV-body')]//td[3]//div");
		
		policyCoverageExposureLimit = By
				.xpath("//div[contains(@id,'PolicyCoveragesLV-body')]//td[4]//div");
	
		policyCoverageIncidentLimit = By
				.xpath("//div[contains(@id,'PolicyCoveragesLV-body')]//td[5]//div");
	
		policyCoverageNotes = By
				.xpath("//div[contains(@id,'PolicyCoveragesLV-body')]//td[6]//div");
		
		projectType = By.cssSelector("input[id*=\"projectTypeInput-inputEl\"]");
		
		subroGCWaivedYes = By.cssSelector("input[id*=\"waivedSubroRadioInput_true-inputEl\"]");
		
		subroGCWaivedNo = By.cssSelector("input[id*=\"waivedSubroRadioInput_false-inputEl\"]");
		
		policyLevelCoverageDiv = By.cssSelector("div[id*=\"PolicyCoveragesLV-body\"]");
		
		policyLevelCoverageType = By.xpath("//input[@name='CoverageType']");
		
		addVehiclesBtn = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimVehiclesLV_tb:Add-btnInnerEl");
		
		memberOrProspect = By.xpath("//input[@id='FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:NewClaimPolicyGeneralDV:InsuredType-inputEl']");
		
		addBtnVessels = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimWatercraftLV_tb:Add-btnInnerEl");
		
		//--------------------claim type - auto-----------------------
		autoFirstAndFinalClaimType = By.xpath("//label[text()='Auto - Auto First and Final']/../input");
		autoQuickClaimAutoClaimType = By.xpath("//label[text()='Auto - Quick Claim Auto']/../input");
	
	}

}
