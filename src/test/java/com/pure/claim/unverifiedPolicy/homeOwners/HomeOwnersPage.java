package com.pure.claim.unverifiedPolicy.homeOwners;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.*;
import com.pure.report.CReporter;

public class HomeOwnersPage extends ActionEngine{

	boolean lobSelected;
	static By policyType;
	NewClaimPage NewClaimPage;
	
	static{
		policyType = By.xpath("//input[contains(@id,'Type')]");
	}
	public HomeOwnersPage(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {
		this.driver = webDriver;
		this.reporter = reporter;
		NewClaimPage = new NewClaimPage(driver, reporter);
		if(getText(policyType, "policyType").equalsIgnoreCase("HomeOwner's")){
			lobSelected = true;
		}
	}
	
	String getPolicyType() throws Throwable{
		return getText(policyType, "policyType");
	}
}
