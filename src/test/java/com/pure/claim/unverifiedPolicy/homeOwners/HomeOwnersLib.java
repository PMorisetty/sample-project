package com.pure.claim.unverifiedPolicy.homeOwners;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.NewClaimPage;
import com.pure.report.CReporter;

public class HomeOwnersLib extends ActionEngine{
	NewClaimPage NewClaimPage;
	
	public HomeOwnersLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {
		this.driver = webDriver;
		this.reporter = reporter;
		NewClaimPage = new NewClaimPage(driver, reporter);
	}
	

}
