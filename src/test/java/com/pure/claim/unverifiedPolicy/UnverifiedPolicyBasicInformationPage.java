package com.pure.claim.unverifiedPolicy;

import org.openqa.selenium.By;

import com.pure.accelerators.ActionEngine;

public class UnverifiedPolicyBasicInformationPage extends ActionEngine{
	static By howReported;
	static By reporterName;
	static By relationToInsured;
	static By iconReportedBy;
	static By dateOfNotice;
	static By newPersonFromReporterNameList;/*Element available only when insuredListIcon is clicked*/
	static By sameAsReporterOption;
	static By differentPersonOption;
	static By mainContactName;
	static By mainContactType;
	static By addPolicyCoverageButton;
	static By deletePolicyCoverageButton;
	static By addCoverageButton;
	static By deleteCoverageButton;
	static By classificationCoverageTerm;
	static By financialCoverageTerm;
	static By numericCoverageTerm;
	static By addLocationsButton;
	static By deleteLocationsButton;
	static By addEndorsementsButton;
	static By deleteEndorsementsButton;
	static By policyLevelCoverageDiv;
	static By injuredName;
	static By serviceType;
	static By location;
		
	
	static{
		howReported = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_BasicInfoScreen:PanelRow:BasicInfoDetailViewPanelDV:HowReported-inputEl");
//		reporterName = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_BasicInfoScreen:PanelRow:BasicInfoDetailViewPanelDV:ReportedBy_Name-inputEl");
		reporterName = By.cssSelector("[id*=\"ReportedBy_Name-inputEl\"]");
		relationToInsured = By.cssSelector("[id*=\"Claim_ReportedByType-inputEl\"]");
		//EnumRelationToInsured relationToInsured = EnumRelationToInsured.valueOf(driver.findElement(By.id("FNOLWizard:GeneralPropertyWizardStepSet:NewClaimWizard_MainContactsScreen:NewClaimPeopleDV:Claim_ReportedByType-inputEl")).getText());
		iconReportedBy = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_BasicInfoScreen:PanelRow:BasicInfoDetailViewPanelDV:ReportedBy_Name:ReportedBy_NameMenuIcon");
		newPersonFromReporterNameList = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_BasicInfoScreen:PanelRow:BasicInfoDetailViewPanelDV:ReportedBy_Name:ClaimNewPersonOnlyPickerMenuItemSet:ClaimNewPersonOnlyPickerMenuItemSet_NewPersonMenuItem-textEl");
		sameAsReporterOption = By.cssSelector("[id*=\"MainContactChoice_true-inputEl\"]");
		differentPersonOption = By.cssSelector("[id*=\"MainContactChoice_false-inputEl\"]");
		mainContactName = By.cssSelector("[id*=\"MainContact_Name-inputEl\"]");
		mainContactType = By.cssSelector("[id*=\"MainContact_Type-inputEl\"]");
		addPolicyCoverageButton = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:PolicyCoverageListDetail:EditablePropertyPolicyCoveragesLV_tb:Add-btnInnerEl");
		deletePolicyCoverageButton = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:PolicyCoverageListDetail:EditablePropertyPolicyCoveragesLV_tb:Remove-btnInnerEl");
		addCoverageButton = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:PolicyCoverageListDetail:ClaimPolicyCovTermsCV:ClaimPolicyCovTermsLV_tb:AddCovTerm-btnInnerEl");
		deleteCoverageButton = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:PolicyCoverageListDetail:ClaimPolicyCovTermsCV:ClaimPolicyCovTermsLV_tb:Remove-btnInnerEl");
		classificationCoverageTerm = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:PolicyCoverageListDetail:ClaimPolicyCovTermsCV:ClaimPolicyCovTermsLV_tb:AddCovTerm:0:CovTermMenuItem-textEl");
		financialCoverageTerm = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:PolicyCoverageListDetail:ClaimPolicyCovTermsCV:ClaimPolicyCovTermsLV_tb:AddCovTerm:1:CovTermMenuItem-textEl");
		numericCoverageTerm = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:PolicyCoverageListDetail:ClaimPolicyCovTermsCV:ClaimPolicyCovTermsLV_tb:AddCovTerm:2:CovTermMenuItem-textEl");
		addLocationsButton = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:PolicyLocationLDV_tb:Add-btnInnerEl");
		deleteLocationsButton = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:PolicyLocationLDV_tb:Remove-btnInnerEl");
		addEndorsementsButton = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimEndorsementsLV_tb:Add-btnInnerEl");
		deleteEndorsementsButton = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimEndorsementsLV_tb:Remove-btnInnerEl");
		policyLevelCoverageDiv = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:PolicyCoverageListDetail:EditablePropertyPolicyCoveragesLV-body");
		injuredName = By.cssSelector("[id*=\"Claimant_Name-inputEl\"]");
		serviceType = By.xpath("//input[@id='FNOLWizard:MAWizardStepSet:FNOLWizard_BasicInfoScreen:ClaimServiceType-inputEl']");
		location = By.xpath("//input[@id='FNOLWizard:MAWizardStepSet:FNOLWizard_BasicInfoScreen:CCAddressInputSet:globalAddressContainer:Address_Picker-inputEl']");
	}
}
