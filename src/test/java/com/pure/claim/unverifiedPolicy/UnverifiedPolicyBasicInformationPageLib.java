package com.pure.claim.unverifiedPolicy;

import java.util.List;

import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.NewContactPageLib;
import com.pure.report.CReporter;

public class UnverifiedPolicyBasicInformationPageLib extends ActionEngine {

	public UnverifiedPolicyBasicInformationPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void setHowReported(String howReported) throws Throwable{
		type(UnverifiedPolicyBasicInformationPage.howReported, howReported, "How Reported");
		type(UnverifiedPolicyBasicInformationPage.howReported, Keys.TAB, "TAB key");
	}
	
	String getReportedBy() throws Throwable{
		return getText(UnverifiedPolicyBasicInformationPage.reporterName, "reporterName");
	}
	
	public void setReportedBy(String reporterName) throws Throwable{
		type(UnverifiedPolicyBasicInformationPage.reporterName, reporterName, "Reporter Name");
		type(UnverifiedPolicyBasicInformationPage.reporterName, Keys.TAB, "TAB key");
	}

	public void setReportedBy(JSONObject reporterDetails) throws Throwable{
		click(UnverifiedPolicyBasicInformationPage.iconReportedBy, "Reported by icon");
		click(UnverifiedPolicyBasicInformationPage.newPersonFromReporterNameList, "New Person");
		new NewContactPageLib(driver, reporter).fillContactDetails(reporterDetails);
	}

	public void setRelationToInsured(String relationToInsured) throws Throwable{
		type(UnverifiedPolicyBasicInformationPage.relationToInsured, relationToInsured.toString(), "Relation To Insured");
		type(UnverifiedPolicyBasicInformationPage.relationToInsured, Keys.TAB, "TAB key");
	}
	
	public void selectReporter(String reporter, String reporterName) throws Throwable{
		if(reporter.equalsIgnoreCase("SameAsReporter")){
			selectSameAsReporter();
		}else{
			selectDifferentPerson(reporterName);
		}
	}
	
	public void selectSameAsReporter() throws Throwable{
		click(UnverifiedPolicyBasicInformationPage.sameAsReporterOption, "Same As Reporter Option");
	}

	public void selectDifferentPerson(String personName) throws Throwable{
		click(UnverifiedPolicyBasicInformationPage.differentPersonOption, "Different Person Option");
		type(UnverifiedPolicyBasicInformationPage.mainContactName, personName, "Main Contact Name");
	}
	
	public void setServiceType(String serviceType) throws Throwable{
		type(UnverifiedPolicyBasicInformationPage.serviceType, serviceType, "Service Type");
		type(UnverifiedPolicyBasicInformationPage.serviceType, Keys.TAB, "TAB key");
	}
	
	public void setServiceLocation(String location) throws Throwable{
		type(UnverifiedPolicyBasicInformationPage.location, location, "location");
		type(UnverifiedPolicyBasicInformationPage.location, Keys.TAB, "TAB key");
	}
	
	public void setMainContact(String personName) throws Throwable{
		type(UnverifiedPolicyBasicInformationPage.mainContactName, personName, "Main Contact Name");
		type(UnverifiedPolicyBasicInformationPage.mainContactName, Keys.TAB, "TAB key");
	}
	
	public void setMainContactType(String type) throws Throwable{
		type(UnverifiedPolicyBasicInformationPage.mainContactType, type, "Main Contact Type");
		type(UnverifiedPolicyBasicInformationPage.mainContactType, Keys.TAB, "TAB key");
	}
	
	public void selectDifferentPerson(JSONObject personDetails) throws Throwable{
		click(UnverifiedPolicyBasicInformationPage.iconReportedBy, "Reported by icon");
		click(UnverifiedPolicyBasicInformationPage.newPersonFromReporterNameList, "New Person");
		new NewContactPageLib(driver, reporter).fillContactDetails(personDetails);
	}
	
	public void clickAddPolicyLevelCoverage() throws Throwable{
		click(UnverifiedPolicyBasicInformationPage.addPolicyCoverageButton, "Policy Level Coverage Add button");
	}
	
	public void clickDeletePolicyLevelCoverage() throws Throwable{
		click(UnverifiedPolicyBasicInformationPage.deletePolicyCoverageButton, "Policy Level Coverage Delete button");
	}
	//Will return column w.r.t. last row in table
	private List<WebElement> getColumns(){
		List<WebElement> tableRows = driver.findElement(UnverifiedPolicyBasicInformationPage.policyLevelCoverageDiv).findElement(By.tagName("table")).findElements(By.tagName("tr"));
		List<WebElement> tableColumns = tableRows.get(tableRows.size() - 1).findElements(By.tagName("td"));
		return tableColumns;
	}
	
	public void setPolicyLevelCoverageType(String type) throws Throwable{
		WebElement elem = getColumns().get(1); 
		new Actions(driver).doubleClick(elem).sendKeys(type)
		.build().perform();
		reporter.SuccessReport("Enter coverage type", "Entered '"+type +"' in coverage type."  );
	}
	
	public void setPolicyLevelCoverageDeductible(String deductible) throws Throwable{
		WebElement elem = getColumns().get(2); 
		new Actions(driver).doubleClick(elem).sendKeys(deductible)
		.build().perform();
		reporter.SuccessReport("Enter coverage deductible", "Entered '"+deductible +"' in coverage deductible."  );
	}
	
	public void setPolicyLevelCoverageExposureLimit(String exposureLimit) throws Throwable{
		WebElement elem = getColumns().get(3); 
		new Actions(driver).doubleClick(elem).sendKeys(exposureLimit)
		.build().perform();
		reporter.SuccessReport("Enter coverage exposure limit", "Entered '"+exposureLimit +"' in coverage exposure limit."  );
	}
	
	public void setPolicyLevelCoverageIncidentLimit(String incidentLimit) throws Throwable{
		WebElement elem = getColumns().get(4); 
		new Actions(driver).doubleClick(elem).sendKeys(incidentLimit)
		.build().perform();
		reporter.SuccessReport("Enter coverage incident limit", "Entered '"+incidentLimit +"' in coverage incident limit."  );
	}
	
	public void setPolicyLevelCoverageNote(String note) throws Throwable{
		WebElement elem = getColumns().get(4); 
		new Actions(driver).doubleClick(elem).sendKeys(note)
		.build().perform();
		reporter.SuccessReport("Enter coverage additional note", "Entered '"+note +"' in coverage additional note."  );
	}
	
	public void clickAddClassificationCoverageTerm() throws Throwable{
		click(UnverifiedPolicyBasicInformationPage.addCoverageButton, "Add Coverage Button");
		click(UnverifiedPolicyBasicInformationPage.classificationCoverageTerm, "Classification Coverage Term");
	}
	
	public void clickAddFinancialCoverageTerm() throws Throwable{
		click(UnverifiedPolicyBasicInformationPage.addCoverageButton, "Add Coverage Button");
		click(UnverifiedPolicyBasicInformationPage.financialCoverageTerm, "Financial Coverage Term");
	}
	
	public void clickAddNumericCoverageTerm() throws Throwable{
		click(UnverifiedPolicyBasicInformationPage.addCoverageButton, "Add Coverage Button");
		click(UnverifiedPolicyBasicInformationPage.numericCoverageTerm, "Numeric Coverage Term");
	}
	
	public void setInjuredWorkerName(String injuredName) throws Throwable{
		type(UnverifiedPolicyBasicInformationPage.injuredName, injuredName, "Injured Name");
		type(UnverifiedPolicyBasicInformationPage.injuredName, Keys.TAB, "Tab keys");
	}
	
	public void selectInvolvedVessel(String vesselName) throws Throwable{
		By vesselCheckBox = By.xpath("//div[contains(text(),'"+vesselName+"')]/..//input"); 
		if(isElementPresent(vesselCheckBox, "vesselName")){
			click(vesselCheckBox,vesselName );
		}
	}

}
