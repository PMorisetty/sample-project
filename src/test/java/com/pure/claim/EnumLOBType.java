package com.pure.claim;

public enum EnumLOBType {
	Collections,
	ExcessLiability,
	HomeConstruction,
	HomeOwners,
	MemberAdvocateServiceRequest,
	PersonalAuto,
	SurplusLinesHomeOwners,
	Watercraft,
	WorkersComp
}
