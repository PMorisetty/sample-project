package com.pure.claim;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.lifeCycle.NotesPage;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoPage;
import com.pure.report.CReporter;

public class ServicesPageLib extends ActionEngine{
	
	public ServicesPageLib(EventFiringWebDriver driver, CReporter reporter) {
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void addService(String serviceType, String vendor) throws Throwable{
		switch (serviceType){
			case "Emergency Management Services":
				click(ServicesPage.emergencyMagementService, "emergencyMagementService");
				waitForMask();
				type(ServicesPage.emergencyMagementServiceVendor, vendor, "Vendor");
				type(ServicesPage.emergencyMagementServiceVendor, Keys.TAB, "Vendor");
				break;
			case "Debris Removal":
				click(ServicesPage.debrisRemoval, "debrisRemoval");
				waitForMask();
				type(ServicesPage.debrisRemovalVendor, vendor, "Vendor");
				type(ServicesPage.debrisRemovalVendor, Keys.TAB, "Vendor");
				break;
		}
	}
	
	public void clickEditBtn() throws Throwable{
		waitUntilServiceProcessDone();
		waitForVisibilityOfElement(ServicesPage.editBtn, "edit button");
		click(ServicesPage.editBtn, "edit button");
	}
	
	public void clickCancelBtn() throws Throwable{
		waitUntilServiceProcessDone();
		waitForVisibilityOfElement(ServicesPage.cancelBtn, "cancel button");
		click(ServicesPage.cancelBtn, "cancel button");
	}
	
	public void clickReassignBtn() throws Throwable{
		waitUntilServiceProcessDone();
		waitForVisibilityOfElement(ServicesPage.reassignBtn, "reassign button");
		click(ServicesPage.reassignBtn, "reassign button");
	}
	
	public void clickAssignBtn() throws Throwable{
		waitUntilServiceProcessDone();
		waitForVisibilityOfElement(ServicesPage.assignBtn, "assign button");
		click(ServicesPage.assignBtn, "assign button");
	}
	
	public void verifyAllTheTabsAreDisplayed() throws Throwable{
		waitUntilServiceProcessDone();
		verifyElementPresent(ServicesPage.detailsTab, "Details tab", true);
		verifyElementPresent(ServicesPage.historyTab, "History tab", true);
		verifyElementPresent(ServicesPage.activitiesTab, "Activities tab", true);
		verifyElementPresent(ServicesPage.documentsTab, "Documents tab", true);
		verifyElementPresent(ServicesPage.notesTab, "Notes tab", true);
//		verifyElementPresent(ServicesPage.invoicesTab, "Invoices tab", true);
	}
	
	public void verifyBtnsInDetailsTab() throws Throwable{
		waitUntilServiceProcessDone();
		verifyElementPresent(ServicesPage.editBtn, "Edit btn", true);
		verifyElementPresent(ServicesPage.reassignBtn, "Reassign btn", true);
		verifyElementPresent(ServicesPage.cancelBtn, "Cancel btn", true);
	}

	public void verifyProgressStatusRequested() throws Throwable{
		String expectedProgressStatus = "Requested";
		assertText(ServicesPage.progress, expectedProgressStatus);
	}
	
	public void verifyProgressStatusCancelled() throws Throwable{
		String expectedProgressStatus = "Canceled";
		assertText(ServicesPage.progress, expectedProgressStatus);
	}
	
	public void verifyInstToVendor(String expectedInstToVendor) throws Throwable{
		assertText(ServicesPage.instructionsToVendor, expectedInstToVendor);
	}
	
	/* this method will verify the address fields by splitting each line and 
	 * verifying its corresponding value from test data
	 * public void verifyServiceAddress(Hashtable<String, String> data) throws Throwable{
		String serviceAddress[] = getText(ServicesPage.serviceAddress, "Service Address").split("<br>");
		String address[] = serviceAddress[0].split("\n");
		assertTextStringMatching(address[0], data.get("vehicleLocationAddress1"));
		assertTextStringMatching(address[1], data.get("vehicleLocationAddress2"));
		String state = data.get("vehicleCity") + ", " + data.get("vehicleStateCode") + " " + data.get("vehicleLocationZip");
		assertTextStringMatching(address[2], state);
	}*/
	
	public void verifyServiceAddress(Hashtable<String, String> data) throws Throwable{
		String actualAddress = getText(ServicesPage.serviceAddress, "Service Address");
		String expAddressLine1 = data.get("vehicleLocationAddress1");
		assertTextStringContains(actualAddress, expAddressLine1);
		String expAddressLine2 = data.get("vehicleLocationAddress2");
		assertTextStringContains(actualAddress, expAddressLine2);
		String expCity = data.get("vehicleCity");
		assertTextStringContains(actualAddress, expCity);
		String expZip = data.get("vehicleLocationZip");
		assertTextStringContains(actualAddress, expZip);
	}
	
	public void verifyVendorName(String expectedVendorName) throws Throwable{
		assertText(ServicesPage.vendorName, expectedVendorName);
	}
	
	public void verifyCoverage(String expectedCoverage) throws Throwable{
		assertText(ServicesPage.coverage, expectedCoverage);
	}
	
	public void verifyServiceType(String expectedServiceType) throws Throwable{
		assertText(ServicesPage.serviceType, expectedServiceType);
	}
	
	public void waitUntilServiceProcessDone() throws Throwable{
		clickUntilElementNotVisible(ServicesPage.serviceNoLink, ServicesPage.actionRequiredMsg, "Service number link");
	}
	
	public void clickHistoryTab() throws Throwable{
		click(ServicesPage.historyTab, "History tab");
	}
	
	public void verifyLogsInHistory(String expectedMsg) throws Throwable{
		try{
			By actualMsgsBy = By.xpath("//div[@id='ClaimServiceRequests:ServiceRequestHistoryLV-body']//tbody//td[3]/div");
			List<WebElement> actualMsgs = driver.findElements(actualMsgsBy);
			List<String> actualMsgsText = new LinkedList<>();
			for(WebElement ele:actualMsgs ){
				String actualMsg = ele.getText();
				actualMsgsText.add(actualMsg);
			}
			if(actualMsgsText.contains(expectedMsg)){
				reporter.SuccessReport("Verify Element in list", expectedMsg + " is present in the list");
			}else{
				reporter.failureReport("Verify Element in list", expectedMsg + " is not present in the list", driver);
			}
		}catch(Exception e){
			LOG.info(e.getMessage());
			reporter.failureReport("Verify Element in list", expectedMsg + " is not present in the list", driver);
		}
	}
	
	public void verifyReassignedInLogs() throws Throwable{
		clickHistoryTab();
		String reassignMsg = "Service record reassigned";
		verifyLogsInHistory(reassignMsg);
	}
	
	public void verifyCancelMsgInLogs() throws Throwable{
		clickHistoryTab();
		String reassignMsg = "Service record cancelled";
		verifyLogsInHistory(reassignMsg);
	}
	
	public void verifyCreateMsgInLogs() throws Throwable{
		clickHistoryTab();
		String reassignMsg = "Auto created by CCC Service Request";
		verifyLogsInHistory(reassignMsg);
	}
	
	public void clickDetailsTab() throws Throwable{
		click(ServicesPage.detailsTab, "Details tab");
	}
	
	public void clickActivitiesTab() throws Throwable{
		click(ServicesPage.activitiesTab, "Activities tab");
	}
	
	public void setClaimServiceActivity(String activityname) throws Throwable {
		type(ServicesPage.claimServiceRequest, activityname, "LastName");
		type(ServicesPage.claimServiceRequest,Keys.TAB,"keys tab");
	}
	
	public void clickEstimateReceivedLink() throws Throwable {
		click(ServicesPage.estimateReceivableLink, "Estimate Received - Vehicle Deemed Repairable");
	}
	
	public void clickEstimateReceivedLinkS01() throws Throwable {
		click(ServicesPage.estimateReceivableLink_S01, "Estimate Received - Vehicle Deemed Repairable");
	}
	
	public void clickReviewForPayLink() throws Throwable {
		click(ServicesPage.reviewForpayLink, "review for pay- E01");
		waitForVisibilityOfElement(ServicesPage.paymentAmount, "Payment Amount");
	}
	
	public void clickReviewForPayLink_S1() throws Throwable {
		click(ServicesPage.reviewForpayLink_S1, "review for pay- S01");
		waitForVisibilityOfElement(ServicesPage.paymentAmount, "Payment Amount");
	}
	
	public void clickConformRepairablesBtn() throws Throwable{
		click(ServicesPage.confirmRepairableBtn, "confirm Repairable");
	}
	
	public void clickClearBtn_ValidationResults() throws Throwable{
		click(ServicesPage.clearBtn, "clear btn");
	}
	
	
	public void enterValuesInApproval() throws Throwable{
		type(ServicesPage.paymentAmount, "100", "Payment Amount");
		//type(ServicesPage.reserveLine,"(1) 1st Party Vehicle - Ace Ventura; Claim Cost/Salvage","Reserve Line Value");
		type(ServicesPage.shareOnMemberPortal,"Never","Share On Member Portal Value");
		if(isElementPresent(ServicesPage.deliveryMethod, "Delivery Method")){
			type(ServicesPage.deliveryMethod,"Standard Mail","Delivery Method");
		}		
		type(ServicesPage.invoiceNumber, "invoice number:007", "Invoice Number");
		click(ServicesPage.check_yes_radiobtn, "yes for check");
		if(isElementPresent(ServicesPage.deductableDecision_applied_radiobtn, "Applied radio button")){
			click(ServicesPage.deductableDecision_applied_radiobtn,"Applied radio button");
		}
		click(ServicesPage.approveBtn,"Approve Button");
		
	}
	
	
	
	public void clickSubmitSupplementButton() throws Throwable {
		click(ServicesPage.submitSupplementBtn, "Submit Supplement");
	}
	
	public void verifyAndClickSubmitSupplement() throws Throwable{
		
		if(isElementPresent(ServicesPage.submitSupplementBtn, "Submit Supplement")){
			//waitForInVisibilityOfElement(ServicesPage.submitSupplementBtn, "Submit Supplement");
			click(ServicesPage.cancelSubmitSupplementBtn, "Cancel Button next to SubmitSupplement");
			clickChekcBoxForEstimateReceived();
			clickAssignButton();
			new SearchAssignerPageLib(driver, reporter).findUserAndAssign_AssignActivities("Smoke Test 1 (Claims)");
		}
	}
	
	public void clickChekcBoxForEstimateReceived() throws Throwable{
		click(ServicesPage.EstimateReceivedChkbox,"checkbox");
	}
	
	public void clickAssignButton() throws Throwable {
		isEnabled(ServicesPage.assign_Btn, "Assign Button");
		click(ServicesPage.assign_Btn, "Assign Button");
	}
	
	public String getServiceNumber() throws Throwable{
		waitForVisibilityOfElement(ServicesPage.serviceNoLink, "ServiceId ");
		String num = driver.findElement(ServicesPage.serviceNoLink).getText();
		
		return num;
	}
	
	public void setPaymentMethodAsEFT() throws Throwable{
		
		click(ServicesPage.payment_EFT, "Electronic Transfer Funds");
	}
	
	public void enterBankDetails(String accountName, String bankName, String accountType, String accountNumber, String routingNumber) throws Throwable{
		
		type(ServicesPage.accountName, accountName, "Account Name");
		type(ServicesPage.bankName, bankName, "Bank Name");
		click(ServicesPage.accountType, "Account Type");
		type(ServicesPage.accountNumber, accountNumber, "Account Number");
		type(ServicesPage.routingNumber, routingNumber, "Routing Number");
		
	}
	
	public void clickRejectButton() throws Throwable {
		click(ServicesPage.rejectButton, "Reject Button");
	}
	
	public void clickOkButton_MessageButton() throws Throwable {
		click(ServicesPage.okBtn, "OK Button");
	}
	
	public void clickNotesTab() throws Throwable{
		click(ServicesPage.notesTab, "Notes tab");
	}
	
	public void verifyLogsInNotes(String expectedMsg) throws Throwable{
		
		waitForVisibilityOfElement(ServicesPage.notes_msg, "Notes message");
		String actualMsg = driver.findElement(ServicesPage.notes_msg).getText();

		if(actualMsg.contains(expectedMsg)){
			reporter.SuccessReport("Verify message in Notes", expectedMsg + " is present in the Notes Tab");
		}else{
			reporter.failureReport("Verify message in Notes", expectedMsg + " is not present in the Notes Tab", driver);
		}
	
	}
	
	public void clickConformTotlaLossBtn() throws Throwable{
		click(ServicesPage.confirmTotalLoss, "confirm Total Loss");
	}
	
}
