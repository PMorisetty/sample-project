package com.pure.claim;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ActionsPageLib extends ActionEngine{
	
	public ActionsPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void clickAndHoverOnActions() throws Throwable{
		//Actions menu items locators are different for claims creation & claim lifecycle.
		// Depending on which element is present, the mode [claim creation or claim lifecycle] is determined
		// and click on Actions menu
		try{
			driver.findElement(ActionsPage.fnolActionsMenu);
			((JavascriptExecutor)driver).executeScript("arguments[0].click(function(){$(this).trigger(\"mouseover\");});", driver.findElement(ActionsPage.fnolActionsMenu));			
		}catch(NoSuchElementException e){
			((JavascriptExecutor)driver).executeScript("arguments[0].click(function(){$(this).trigger(\"mouseover\");});", driver.findElement(ActionsPage.ActionsMenu));
		} 
	}
	
	public void clickOn(String itemToClick) throws Throwable{
		//clickAndHoverOnActions();	 This should be added at script level henceforth
		reporter.SuccessReport("Actions -->: ", "Clicking '"+itemToClick +"' in Actions menu."  );
		switch (itemToClick) {
		case "Note": 				moveToItem(ActionsPage.Note, itemToClick);click(ActionsPage.Note, itemToClick); break;
		case "Email": 				moveToItem(ActionsPage.Email, itemToClick);click(ActionsPage.Email, itemToClick);	break;
		case "Litigation":  		moveToItem(ActionsPage.Litigation, itemToClick);click(ActionsPage.Litigation, itemToClick);	break;
		case "Negotiation":			moveToItem(ActionsPage.Negotiation, itemToClick);click(ActionsPage.Negotiation, itemToClick);break;
		case "Rental": 		        moveToItem(ActionsPage.Rental, itemToClick);click(ActionsPage.Rental, itemToClick); break;
		case "Service": 		    moveToItem(ActionsPage.Service, itemToClick);click(ActionsPage.Service, itemToClick); break;
		case "NewTransaction": 		moveToItem(ActionsPage.NewTransaction, itemToClick);click(ActionsPage.NewTransaction, itemToClick);	break;
		case "Reserve":			    moveToItem(ActionsPage.Reserve, itemToClick);click(ActionsPage.Reserve, itemToClick); break;
		case "Check": 		        moveToItem(ActionsPage.Check, itemToClick);click(ActionsPage.Check, itemToClick); break;
		case "Other": 				moveToItem(ActionsPage.Other, itemToClick);click(ActionsPage.Other, itemToClick);	break;
		case "NewDocument":			moveToItem(ActionsPage.NewDocument, itemToClick);click(ActionsPage.NewDocument, itemToClick); break;
		case "CreateFromTemplate":  moveToItem(ActionsPage.CreateFromTemplate, itemToClick);click(ActionsPage.CreateFromTemplate, itemToClick);	break;
		case "AttachAnExistingDocument": moveToItem(ActionsPage.AttachAnExistingDocument, itemToClick);click(ActionsPage.AttachAnExistingDocument, itemToClick); break;
		case "IndicateExistenceOfDocument":	moveToItem(ActionsPage.IndicateExistenceOfDocument, itemToClick);click(ActionsPage.IndicateExistenceOfDocument, itemToClick); break;
		case "NewActivity":			moveToItem(ActionsPage.NewActivity, itemToClick);click(ActionsPage.NewActivity, itemToClick); break;
		case "correspondence":      moveToItem(ActionsPage.correspondence, itemToClick);click(ActionsPage.correspondence, itemToClick);	break;
		case "General":			    moveToItem(ActionsPage.General, itemToClick);click(ActionsPage.General, itemToClick); break;
		case "Interview": 		    moveToItem(ActionsPage.Interview, itemToClick);click(ActionsPage.Interview, itemToClick);	break;
		case "NewMail":				moveToItem(ActionsPage.NewMail, itemToClick);click(ActionsPage.NewMail, itemToClick); break;
		case "Reminder": 			moveToItem(ActionsPage.Reminder, itemToClick);click(ActionsPage.Reminder, itemToClick); break;
		case "Request":				moveToItem(ActionsPage.Request, itemToClick);click(ActionsPage.Request, itemToClick); break;
		case "Warning":				moveToItem(ActionsPage.Warning, itemToClick);click(ActionsPage.Warning, itemToClick); break;
		case "NewExposure":			moveToItem(ActionsPage.NewExposure, itemToClick);click(ActionsPage.NewExposure, itemToClick); break;
		case "ChooseByCoverage": 	moveToItem(ActionsPage.ChooseByCoverage, itemToClick);click(ActionsPage.ChooseByCoverage, itemToClick); break;
		case "ClaimActions": 		moveToItem(ActionsPage.ClaimActions, itemToClick);click(ActionsPage.ClaimActions, itemToClick); break;
		case "AssignClaim":			moveToItem(ActionsPage.AssignClaim, itemToClick);click(ActionsPage.AssignClaim, itemToClick);	break;
		case "CloseClaim": 			moveToItem(ActionsPage.CloseClaim, itemToClick);click(ActionsPage.CloseClaim, itemToClick);	break;
		case "PrintClaim":			moveToItem(ActionsPage.PrintClaim, itemToClick);click(ActionsPage.PrintClaim, itemToClick);	break;
		case "SyncStatus":			moveToItem(ActionsPage.SyncStatus, itemToClick);click(ActionsPage.SyncStatus, itemToClick);	break;
		case "ValidateClaimOnly": 	moveToItem(ActionsPage.ValidateClaimOnly, itemToClick);click(ActionsPage.ValidateClaimOnly, itemToClick);break;
		case "ValidateClaimAndExposure": moveToItem(ActionsPage.ValidateClaimAndExposure, itemToClick);click(ActionsPage.ValidateClaimAndExposure, itemToClick); break;
		case "ValidatePolicy":		moveToItem(ActionsPage.ValidatePolicy, itemToClick);click(ActionsPage.ValidatePolicy, itemToClick);	break;
		case "ValidForISO":			moveToItem(ActionsPage.validForISO, itemToClick);click(ActionsPage.validForISO, itemToClick);	break;
		case "AbilityToPay":		moveToItem(ActionsPage.abilityToPay, itemToClick);click(ActionsPage.abilityToPay, itemToClick);	break;		
		case "AbilityToPayClaimsOnly":		moveToItem(ActionsPage.abilityToPay_claims, itemToClick);click(ActionsPage.abilityToPay_claims, itemToClick);	break;
		case "SendClaimAcknowledgementLetter":		moveToItem(ActionsPage.sendClaimAcknowledgementLetter, itemToClick);click(ActionsPage.sendClaimAcknowledgementLetter, itemToClick);	break;
		
		//=============fnol================================================================
		case "fnolNewExposure":		moveToItem(ActionsPage.fnolNewExposure, itemToClick);click(ActionsPage.fnolNewExposure, itemToClick); break;
		case "fnolChooseByCoverage":	moveToItem(ActionsPage.fnolChooseByCoverage, itemToClick);click(ActionsPage.fnolChooseByCoverage, itemToClick); break;
		case "fnolMiscValuablesScheduled":	moveToItem(ActionsPage.fnolMiscValuablesScheduled, itemToClick);click(ActionsPage.fnolMiscValuablesScheduled, itemToClick); break;
		case "fnolPersonalExcessCoverageType":	moveToItem(ActionsPage.fnolPersonalExcessCoverageType, itemToClick);click(ActionsPage.fnolPersonalExcessCoverageType, itemToClick); break;
		case "fnolPersonalLiabilityPhysicalDamage":	moveToItem(ActionsPage.fnolPersonalLiabilityPhysicalDamage, itemToClick);click(ActionsPage.fnolPersonalLiabilityPhysicalDamage, itemToClick); break;

		//======================= New Exposure elements ================
		case "policyLevelCoverage":		moveToItem(ActionsPage.policyLevelCoverage, itemToClick);click(ActionsPage.policyLevelCoverage, itemToClick); break;		
		case "liability":		moveToItem(ActionsPage.liability, itemToClick);click(ActionsPage.liability, itemToClick); break;
		case "liabilityPropertyDamage":	moveToItem(ActionsPage.liabilityPropertyDamage, itemToClick);click(ActionsPage.liabilityPropertyDamage, itemToClick); break;
		case "liabilityVehicleDamage":	moveToItem(ActionsPage.liabilityVehicleDamage, itemToClick);click(ActionsPage.liabilityVehicleDamage, itemToClick); break;
		case "liabilityMedicalPayments":	moveToItem(ActionsPage.liabilityMedicalPayments, itemToClick);click(ActionsPage.liabilityMedicalPayments, itemToClick); break;
		case "liability_Property_damage": moveToItem(ActionsPage.liability_Property_damage, itemToClick);click(ActionsPage.liability_Property_damage, itemToClick); break;
		case "liability_Property_damage_property":	moveToItem(ActionsPage.liability_Property_damage_property, itemToClick);click(ActionsPage.liability_Property_damage_property, itemToClick); break;
		case "liability_Property_damage_vehicle":	moveToItem(ActionsPage.liability_Property_damage_vehicle, itemToClick);click(ActionsPage.liability_Property_damage_vehicle, itemToClick); break;
		case "liabilityIncidentalBusinessDamage":	moveToItem(ActionsPage.liabilityIncidentalBusinessDamage, itemToClick);click(ActionsPage.liabilityIncidentalBusinessDamage, itemToClick); break;
		case "liabilityLossOfUseDamage":	moveToItem(ActionsPage.liabilityLossOfUseDamage, itemToClick);click(ActionsPage.liabilityLossOfUseDamage, itemToClick); break;
		case "collisionVehicleDamage": 	moveToItem(ActionsPage.collisionVehicleDamage, itemToClick);click(ActionsPage.collisionVehicleDamage, itemToClick); break;
		case "comprehensiveVehicleDamage": 	moveToItem(ActionsPage.comprehensiveVehicleDamage, itemToClick);click(ActionsPage.comprehensiveVehicleDamage, itemToClick); break;
		case "liabilityCombinedSingleLimit": 	moveToItem(ActionsPage.liabilityCombinedSingleLimit, itemToClick);click(ActionsPage.liabilityCombinedSingleLimit, itemToClick); break;
		case "liabilityAutoBodilyInjury": 	moveToItem(ActionsPage.liabilityAutoBodilyInjury, itemToClick);click(ActionsPage.liabilityAutoBodilyInjury, itemToClick); break;
		case "medicalPayments":	moveToItem(ActionsPage.medicalPayments, itemToClick);click(ActionsPage.medicalPayments, itemToClick); break;
		case "towingAndLabor":	moveToItem(ActionsPage.towingAndLabor, itemToClick);click(ActionsPage.towingAndLabor, itemToClick); break;
		case "firstPartyRental": moveToItem(ActionsPage.firstPartyRental, itemToClick);click(ActionsPage.firstPartyRental, itemToClick); break;
		case "underInsuredMotorist": moveToItem(ActionsPage.underInsuredMotorist, itemToClick);click(ActionsPage.underInsuredMotorist, itemToClick); break;
		case "underInsuredMotoristBodilyInjury": moveToItem(ActionsPage.underInsuredMotoristBodilyInjury, itemToClick);click(ActionsPage.underInsuredMotoristBodilyInjury, itemToClick); break;
		case "liabilityBodilyInjuryDamage": moveToItem(ActionsPage.liabilityBodilyInjuryDamage, itemToClick);click(ActionsPage.liabilityBodilyInjuryDamage, itemToClick); break;
		case "liabilityPersonalInjuryDamage": moveToItem(ActionsPage.liabilityPersonalInjuryDamage, itemToClick);click(ActionsPage.liabilityPersonalInjuryDamage, itemToClick); break;
		case "uninsuredMotoristBI":	moveToItem(ActionsPage.uninsuredMotoristBI, itemToClick);click(ActionsPage.uninsuredMotoristBI, itemToClick); break;
		case "autoPropertyDamageProperty": moveToItem(ActionsPage.autoPropertyDamageProperty, itemToClick);click(ActionsPage.autoPropertyDamageProperty, itemToClick); break;
		case "fraudAndCyber": moveToItem(ActionsPage.fruadAndCyber, itemToClick);click(ActionsPage.fruadAndCyber, itemToClick); break;
		case "cyberAttack": moveToItem(ActionsPage.cyberAttack, itemToClick);click(ActionsPage.cyberAttack, itemToClick); break;
		case "cyberExtortion": moveToItem(ActionsPage.cyberExtortion, itemToClick);click(ActionsPage.cyberExtortion, itemToClick); break;
		case "fraud": moveToItem(ActionsPage.fraud, itemToClick);click(ActionsPage.fraud, itemToClick); break;
		case "building": moveToItem(ActionsPage.building, itemToClick);click(ActionsPage.building, itemToClick); break;
		case "rebuildingToCode": moveToItem(ActionsPage.rebuildingToCode, itemToClick);click(ActionsPage.rebuildingToCode, itemToClick); break;
		case "buildingDebrisRemoval": moveToItem(ActionsPage.buildingDebrisRemoval, itemToClick);click(ActionsPage.buildingDebrisRemoval, itemToClick); break;
		case "buildingDemandSurge": moveToItem(ActionsPage.buildingDemandSurge, itemToClick);click(ActionsPage.buildingDemandSurge, itemToClick); break;
		case "buildingEarthquake": moveToItem(ActionsPage.buildingEarthquake, itemToClick);click(ActionsPage.buildingEarthquake, itemToClick); break;
		case "buildingExcessFlood": moveToItem(ActionsPage.buildingExcessFlood, itemToClick);click(ActionsPage.buildingExcessFlood, itemToClick); break;
		case "buildingExtendedRebuildingProvision": moveToItem(ActionsPage.buildingExtendedRebuildingProvision, itemToClick);click(ActionsPage.buildingExtendedRebuildingProvision, itemToClick); break;
		case "buildingLandStabilization": moveToItem(ActionsPage.buildingLandStabilization, itemToClick);click(ActionsPage.buildingLandStabilization, itemToClick); break;
		case "buildingLandscaping": moveToItem(ActionsPage.buildingLandscaping, itemToClick);click(ActionsPage.buildingLandscaping, itemToClick); break;
		case "buildingLossAssessment": moveToItem(ActionsPage.buildingLossAssessment, itemToClick);click(ActionsPage.buildingLossAssessment, itemToClick); break;
		case "buildingLossMitigationMeasures": moveToItem(ActionsPage.buildingLossMitigationMeasures, itemToClick);click(ActionsPage.buildingLossMitigationMeasures, itemToClick); break;
		case "buildingMoldRemediation": moveToItem(ActionsPage.buildingMoldRemediation, itemToClick);click(ActionsPage.buildingMoldRemediation, itemToClick); break;
		case "buildingPropertyDamage": moveToItem(ActionsPage.buildingPropertyDamage, itemToClick);click(ActionsPage.buildingPropertyDamage, itemToClick); break;
		case "otherStructures": moveToItem(ActionsPage.otherStructures, itemToClick);click(ActionsPage.otherStructures, itemToClick); break;
		case "otherStructuresEarthQuake": moveToItem(ActionsPage.otherStructuresEarthQuake, itemToClick);click(ActionsPage.otherStructuresEarthQuake, itemToClick); break;
		case "otherStructuresExcessFlood": moveToItem(ActionsPage.otherStructuresExcessFlood, itemToClick);click(ActionsPage.otherStructuresExcessFlood, itemToClick); break;
		case "otherStructuresPropertyDamage": moveToItem(ActionsPage.otherStructuresPropertyDamage, itemToClick);click(ActionsPage.otherStructuresPropertyDamage, itemToClick); break;
		case "contents": moveToItem(ActionsPage.contents, itemToClick);click(ActionsPage.contents, itemToClick); break;
		case "contentsAtAListedResidence": moveToItem(ActionsPage.contentsAtAListedResidence, itemToClick);click(ActionsPage.contentsAtAListedResidence, itemToClick); break;
		case "contentsAtAnUnlistedResidence": moveToItem(ActionsPage.contentsAtAnUnlistedResidence, itemToClick);click(ActionsPage.contentsAtAnUnlistedResidence, itemToClick); break;
		case "contentsAwayFromAResidence": moveToItem(ActionsPage.contentsAwayFromAResidence, itemToClick);click(ActionsPage.contentsAwayFromAResidence, itemToClick); break;
		case "contentsEarthQuake": moveToItem(ActionsPage.contentsEarthQuake, itemToClick);click(ActionsPage.contentsEarthQuake, itemToClick); break;
		case "contentsExcessFlood": moveToItem(ActionsPage.contentsExcessFlood, itemToClick);click(ActionsPage.contentsExcessFlood, itemToClick); break;
		case "contentsMoldRemediation": moveToItem(ActionsPage.contentsMoldRemediation, itemToClick);click(ActionsPage.contentsMoldRemediation, itemToClick); break;
		case "identityFraudProtection": moveToItem(ActionsPage.identityFraudProtection, itemToClick);click(ActionsPage.identityFraudProtection, itemToClick); break;
		case "identityFraudCreditCard": moveToItem(ActionsPage.identityFraudCreditCard, itemToClick);click(ActionsPage.identityFraudCreditCard, itemToClick); break;
		case "identityFraudIdentityFraud": moveToItem(ActionsPage.identityFraudIdentityFraud, itemToClick);click(ActionsPage.identityFraudIdentityFraud, itemToClick); break;
		case "additionalLivingExpenses": moveToItem(ActionsPage.additionalLivingExpenses, itemToClick);click(ActionsPage.additionalLivingExpenses, itemToClick); break;
		case "additionalLivingExpensesEarthquake": moveToItem(ActionsPage.additionalLivingExpensesEarthquake, itemToClick);click(ActionsPage.additionalLivingExpensesEarthquake, itemToClick); break;
		case "additionalLivingExpensesExcessFlood": moveToItem(ActionsPage.additionalLivingExpensesExcessFlood, itemToClick);click(ActionsPage.additionalLivingExpensesExcessFlood, itemToClick); break;
		case "additionalLivingExpensesLossOfUseDamage": moveToItem(ActionsPage.additionalLivingExpensesLossOfUseDamage, itemToClick);click(ActionsPage.additionalLivingExpensesLossOfUseDamage, itemToClick); break;
		case "yachtHullDamage": moveToItem(ActionsPage.yachtHullDamage, itemToClick);click(ActionsPage.yachtHullDamage, itemToClick); break;
		case "yachtMachineryDamage": moveToItem(ActionsPage.yachtMachineryDamage, itemToClick);click(ActionsPage.yachtMachineryDamage, itemToClick); break;
		case "yachtTenderDamage": moveToItem(ActionsPage.yachtTenderDamage, itemToClick);click(ActionsPage.yachtTenderDamage, itemToClick); break;
		case "yachtRental": moveToItem(ActionsPage.yachtRental, itemToClick);click(ActionsPage.yachtRental, itemToClick); break;
		case "yachtInspectionAndPrecautionary": moveToItem(ActionsPage.yachtInspectionAndPrecautionary, itemToClick);click(ActionsPage.yachtInspectionAndPrecautionary, itemToClick); break;
		case "emergencyExpenses": moveToItem(ActionsPage.emergencyExpenses, itemToClick);click(ActionsPage.emergencyExpenses, itemToClick); break;
		case "fineArtContents": moveToItem(ActionsPage.fineArtContents, itemToClick);click(ActionsPage.fineArtContents, itemToClick); break;
		case "personalEffectsPersonalProperty": moveToItem(ActionsPage.personalEffectsPersonalProperty, itemToClick);click(ActionsPage.personalEffectsPersonalProperty, itemToClick); break;
		case "jonesAct": moveToItem(ActionsPage.jonesAct, itemToClick);click(ActionsPage.jonesAct, itemToClick); break;
		case "liabilityBIDamageWithSpaces": moveToItem(ActionsPage.liabilityBIDamageWithSpaces, itemToClick);click(ActionsPage.liabilityBIDamageWithSpaces, itemToClick); break;
		case "liabilityPropertyDamageWithSpaces": moveToItem(ActionsPage.liabilityPropertyDamageWithSpaces, itemToClick);click(ActionsPage.liabilityPropertyDamageWithSpaces, itemToClick); break;
		case "liabilityWatercraftDamage": moveToItem(ActionsPage.liabilityWatercraftDamage, itemToClick);click(ActionsPage.liabilityWatercraftDamage, itemToClick); break;
		case "trailerDamage": moveToItem(ActionsPage.trailerDamage, itemToClick);click(ActionsPage.trailerDamage, itemToClick); break;
		case "uninsuredBoaterBIDamage": moveToItem(ActionsPage.uninsuredBoaterBIDamage, itemToClick);click(ActionsPage.uninsuredBoaterBIDamage, itemToClick); break;
		case "additionalLivingExpensesLossOfUse": moveToItem(ActionsPage.additionalLivingExpensesLossOfUse, itemToClick);click(ActionsPage.additionalLivingExpensesLossOfUse, itemToClick); break;
		case "equipmentBreakdownHomeSystems": moveToItem(ActionsPage.equipmentBreakdownHomeSystems, itemToClick);click(ActionsPage.equipmentBreakdownHomeSystems, itemToClick); break;
		
		//======================== Batch process info page=================
		case "returnToClaimCenter": moveToItem(ActionsPage.returnToClaimCenter, itemToClick);click(ActionsPage.returnToClaimCenter, itemToClick); break;
		
		case "recoveryReserve": moveToItem(ActionsPage.recoveryReserve, itemToClick);click(ActionsPage.recoveryReserve, itemToClick); break;
		case "recovery": moveToItem(ActionsPage.recovery, itemToClick);click(ActionsPage.recovery, itemToClick); break;
		case "reopenClaim": moveToItem(ActionsPage.reopenClaim, itemToClick);click(ActionsPage.reopenClaim, itemToClick);
		case "closeClaim": moveToItem(ActionsPage.closeClaim, itemToClick);click(ActionsPage.closeClaim, itemToClick);
		default:
			break;
		}
		
	}
	
	public void clickOnVehicleInvolved(String vehicleInvolved) throws Throwable{
		By vehicalInvolvedXpath = By.xpath("//span[text()='"+vehicleInvolved+"']");
		moveToItem(vehicalInvolvedXpath, vehicleInvolved);
		click(vehicalInvolvedXpath, "vehicalInvolved:"+vehicleInvolved);
	}
	
	public void clickOnLocationCoverage(String locationCoverage) throws Throwable{
		By locationCoverageXpath = By.xpath("//span[contains(text(),'"+locationCoverage+"')]");
		moveToItem(locationCoverageXpath, locationCoverage);
		click(locationCoverageXpath, "locationCoverage:"+locationCoverage);
	}
	
	public void moveToItem(By item, String itemToClick) throws Throwable{
		waitForMask();
		waitForVisibilityOfElement(item, itemToClick);
		new Actions(driver).moveToElement(driver.findElement(item)).perform();
	}
	
	public void clearTheErrors() throws Throwable{
		//Thread.sleep(1000);
		if(isElementPresent(By.id("WebMessageWorksheet:WebMessageWorksheetScreen:WebMessageWorksheet_ClearButton-btnInnerEl"),"clear button")){
			click(By.id("WebMessageWorksheet:WebMessageWorksheetScreen:WebMessageWorksheet_ClearButton-btnInnerEl"),"clear errors btn");
		}
	}

	public void verifyExposureIsNotDisplayed(String exposureName) throws Throwable{
		By exposureXpath = By.xpath("//span[contains(text(),'"+exposureName+"')]");
		isElementNotPresent(exposureXpath, exposureName);
	}
	
}
