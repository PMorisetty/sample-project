package com.pure.claim;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.lifeCycle.VehicleIncidentPage;
import com.pure.report.CReporter;

public class WatercraftIncidentPageLib extends ActionEngine{
	
	public WatercraftIncidentPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void enterDamageDescription(String damageDescription) throws Throwable{
		type(WatercraftIncidentPage.damageDescription,damageDescription,"damageDescription");
		type(WatercraftIncidentPage.damageDescription, Keys.TAB,"tab key");
	}
	public void setDriverName(String driverName) throws Throwable{
		type(WatercraftIncidentPage.driverName,driverName,"driver name");
		type(WatercraftIncidentPage.driverName, Keys.TAB,"tab key");
	}
	public void setLossOccured(String lossoccured) throws Throwable{
		type(WatercraftIncidentPage.lossOccured,lossoccured,"lossoccured");
		type(WatercraftIncidentPage.lossOccured, Keys.TAB,"tab key");
	}
	public void enterDetails(String damageDescription,String driverName, String lossoccured) throws Throwable{
		enterDamageDescription(damageDescription);
		setDriverName(driverName);
		setLossOccured(lossoccured);	
		click(VehicleIncidentPage.okBtn, "OkBtn");
	}
	
	public void setInvolvedVehicle(String watercraft, String lossParty) throws Throwable{
		type(WatercraftIncidentPage.selectWatercraft, watercraft, "Select vehicle");
		type(WatercraftIncidentPage.selectWatercraft, Keys.TAB, "Tab key");
		waitForMask();
		type(WatercraftIncidentPage.lossParty, lossParty, "Loss Party");
		type(WatercraftIncidentPage.lossParty, Keys.TAB, "tab key");
	}

}
