package com.pure.claim;

import org.openqa.selenium.By;

public class ClaimCreationSummaryPage {

	static By claimNumberLabel, newClaimSaved, serviceRequestNumberLabel;

	static {
		claimNumberLabel = By.id("NewClaimSaved:NewClaimSavedScreen:NewClaimSavedDV:Header");
		newClaimSaved = By.id("NewClaimSaved:NewClaimSavedScreen:NewClaimSavedDV:GoToClaim-bodyEl");
		serviceRequestNumberLabel = By.id("NewClaimSaved:NewClaimSavedScreen:NewClaimSavedDV:0:ServiceRequestNumber");
	}
}
