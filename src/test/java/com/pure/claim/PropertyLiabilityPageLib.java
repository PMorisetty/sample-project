package com.pure.claim;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;


public class PropertyLiabilityPageLib extends ActionEngine {
	protected int numOLoops = 0;
	
	public PropertyLiabilityPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void setZip(String zip) throws Throwable {
		type(PropertyLiabilityPage.zip, zip, "ZIP text box");
		click(PropertyLiabilityPage.zipIcon,"ZipIcon");
		Thread.sleep(2000);//Inevitable: Auto data fill is happening in the background
	}

	public void setPropertyName(String propertyname, String... addressNzip) throws Throwable{
		new Actions(driver).doubleClick(driver.findElement(PropertyLiabilityPage.propertyName));
		//By propertyNameToSelect = By.xpath("//li[contains(text(),'"+propertyname+"')]");

		type(PropertyLiabilityPage.propertyName,propertyname," in propertyname");
		type(PropertyLiabilityPage.propertyName,Keys.BACK_SPACE,"Backspace");
		type(PropertyLiabilityPage.propertyName,Keys.ENTER,"Enter key");
		if(propertyname.equalsIgnoreCase("New...")){
			setZip(addressNzip[0]);
			type(PropertyLiabilityPage.AddressLine1,addressNzip[1]," in AddressLine1");
			type(PropertyLiabilityPage.AddressLine1,Keys.TAB,"tab");
			if(addressNzip[2]!=null){
				type(PropertyLiabilityPage.AddressLine2,addressNzip[2]," in AddressLine2");
				type(PropertyLiabilityPage.AddressLine2,Keys.TAB,"tab");
			}
		}
	}


	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		
		String propertydescription = data.get("propertydescription");
		String damagedescription = data.get("damagedescription");
		String propertyname = data.get("propertyname");
		String zip = data.get("propertyincidentzip");
		String adress1 = data.get("propertyincidentadress1");
		String adress2 = data.get("propertyincidentadress2");
		String lossestimate = data.get("lossestimate");
		
		type(PropertyLiabilityPage.propertyDescription,propertydescription," in propertydescription");
		type(PropertyLiabilityPage.propertyDescription,Keys.TAB,"tab");
		type(PropertyLiabilityPage.damageDescription,damagedescription," in damagedescription");
		type(PropertyLiabilityPage.damageDescription,Keys.TAB,"tab");
		type(PropertyLiabilityPage.lossEstimate,lossestimate," in damagedescription");
		type(PropertyLiabilityPage.lossEstimate,Keys.TAB,"tab");
		
		if(zip!=null && adress1!=null){
			setPropertyName(propertyname,zip,adress1,adress2);
		}else{
			setPropertyName(propertyname);
		}
		
		//=============>
		//== The whole page is going blank --- need to find solution to this...
		//=============>

		click(PropertyLiabilityPage.okBtn,"Ok button");
		reporter.SuccessReport("", "Successfully added property incident");
	}


}
