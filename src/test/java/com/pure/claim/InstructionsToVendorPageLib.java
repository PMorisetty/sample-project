package com.pure.claim;


import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class InstructionsToVendorPageLib extends ActionEngine{
	Hashtable<String, String> data;
	public InstructionsToVendorPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable{
		this.driver = driver;
		this.reporter = reporter;
		printPageTitle(InstructionsToVendorPage.title,"Page title");
	}
	
	private void setCoverage(String coverage) throws Throwable{
		type(InstructionsToVendorPage.coverage, coverage, "coverage");
		type(InstructionsToVendorPage.coverage, Keys.TAB, "Tab key");
		waitForMask();
	}
	
	private void setInstructionsToVendor(String instructionsToVendor) throws Throwable{
		type(InstructionsToVendorPage.instructionsToVendor, instructionsToVendor, "instructionsToVendor");
		type(InstructionsToVendorPage.instructionsToVendor, Keys.TAB, "Tab key");
		waitForMask();
	}
	
	private void setLocationContact(String locationContact) throws Throwable{
		type(InstructionsToVendorPage.locationContact, locationContact, "locationContact");
		type(InstructionsToVendorPage.locationContact, Keys.TAB, "Tab key");
		waitForMask();
	}
	
	private void setSnapSheetContactNo(String contactNo) throws Throwable{
		type(InstructionsToVendorPage.snapSheetContactNo, contactNo, "contactNo");
	}
	
	public void setAdditionalInstructions(String additionalInst) throws Throwable{
		type(InstructionsToVendorPage.additionalInst, additionalInst, "additional Instructions");
	}
	
	public void clickUpdate() throws Throwable{
		click(InstructionsToVendorPage.updateBtn, "updateBtn");
		waitForMask();
	}
	
	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		this.data = data;
		String vendorCoverage = data.get("vendorCoverage");
		String vendorInstructions = data.get("vendorInstructions");
		String vendorLocationContact = data.get("vendorLocationContact"); 
		
		setCoverage(vendorCoverage);
		setInstructionsToVendor(vendorInstructions);
//		setLocationContact(vendorLocationContact);
		setParty(vendorLocationContact);
		
		if(isElementPresent(InstructionsToVendorPage.snapSheetContactNo, "snapSheet Contact No.")){
			if(data.get("vendorSnapsheetContactNo") != null){
				setSnapSheetContactNo(data.get("vendorSnapsheetContactNo"));
			}
		}
		
		if(data.get("vendorAdditionalInst") != null){
			setAdditionalInstructions(data.get("vendorAdditionalInst"));
		}
		
	}
	
	public void verifyPageTitle() throws Throwable{
		assertText(InstructionsToVendorPage.title, "Instructions to Vendor");
	}
	
	public void setVendorCoverageAndInstructions(String vendorCoverage, String vendorInst, String vendorLocation) throws Throwable{
		setCoverage(vendorCoverage);
		setInstructionsToVendor(vendorInst);
		setLocationContact(vendorLocation);
	}
	
	public void verifyComSearchDocMessage(String expectedDocMsg) throws Throwable{
		assertText(InstructionsToVendorPage.comSearchDocMsg, expectedDocMsg);
	}
	
	public void clickAttachBtn() throws Throwable{
		click(InstructionsToVendorPage.attachBtn, "Attach button");
	}
	
	public void verifySelectedDocIsDisplayed(String expectedDocName) throws Throwable{
		String docToVerifyXpath = "//div[contains(text(),'" + expectedDocName + "')]";
		By docToVerifyBy = By.xpath(docToVerifyXpath);
		waitForVisibilityOfElement(docToVerifyBy, "document to verify - "+expectedDocName);
	}
	
	public void setParty(String party) throws Throwable{
		type(InstructionsToVendorPage.party, party, "party");
		type(InstructionsToVendorPage.party, Keys.TAB, "Tab key");
		waitForMask();
	}
	
}
