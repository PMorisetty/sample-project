package com.pure.claim;

import org.openqa.selenium.By;

public class NewContactPage {
	static By lastName, insuredNameListIcon, itemNewPersonFromInsuredNameList;
	static By prefix, firstName, middleName, suffix;
	static By address1, address2, zip, city, county, state, country; 
	static By zipAutoFill;
	
	static By btnOK;
	static{
		prefix = By.xpath("//label[text()='Prefix']/../..//input");
		firstName = By.xpath("//label[text()='First name']/../..//input");
		middleName = By.xpath("//label[text()='Middle name']/../..//input");
		lastName = By.xpath("//label[text()='Last name']/../..//input");
		suffix = By.xpath("//label[text()='Suffix']/../..//input");
		address1 = By.xpath("//label[text()='Address 1']/../..//input");
		address2 = By.xpath("//label[text()='Address 2']/../..//input");
		zip = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//input");
		zipAutoFill = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//a");
		city = By.xpath("//label[text()='City']/../..//input");
		county = By.xpath("//label[text()='County']/../..//input");
		state = By.id("NewContactPopup:ContactDetailScreen:ContactBasicsDV:PrimaryAddressInputSet:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:State-inputEl");
		country = By.xpath("//label[text()='Country']/../..//input");
		btnOK = By.id("NewContactPopup:ContactDetailScreen:ContactBasicsDV_tb:ContactDetailToolbarButtonSet:Update-btnInnerEl");
		insuredNameListIcon = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:NewClaimPolicyGeneralDV:Insured_Name:Insured_NameMenuIcon");		
		itemNewPersonFromInsuredNameList = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:NewClaimPolicyGeneralPanelSet:NewClaimPolicyGeneralDV:Insured_Name:ClaimNewPersonOnlyPickerMenuItemSet:ClaimNewPersonOnlyPickerMenuItemSet_NewPersonMenuItem-textEl");
	}
}
