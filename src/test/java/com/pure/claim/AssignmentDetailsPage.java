package com.pure.claim;

import org.openqa.selenium.By;

public class AssignmentDetailsPage {

	static By relatesTo, addBtn, addServiceBtn, sendTo, updateBtn, estimator;

	static {
		relatesTo = By.xpath("//input[@id='PURE_NewAssignment:NewAssignmentScreen:PURE_NewAssignmentDV:Property_Incident-inputEl']");
		addBtn = By.xpath("//span[@id='PURE_NewAssignment:NewAssignmentScreen:PURE_NewAssignmentDV:InstructionServicesLV_tb:AddMultipleServices-btnInnerEl']");
		sendTo = By.xpath("//input[@id='PURE_NewAssignment:NewAssignmentScreen:PURE_NewAssignmentDV:Specialist-inputEl']");
		estimator = By.xpath("//input[@id='PURE_NewAssignment:NewAssignmentScreen:PURE_NewAssignmentDV:PURE_NewAssignementVendorInputSet:GAUsers-inputEl']");
		updateBtn = By.xpath("//span[@id='PURE_NewAssignment:NewAssignmentScreen:PURE_NewAssignmentDV_tb:SubmitButton-btnInnerEl']");
		
		addServiceBtn = By.xpath("//span[@id='SelectServicesPopup:UpdateServiceRequestServices-btnInnerEl']");
	}

}
