package com.pure.claim;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewTrailerIncidentPageLib extends ActionEngine{
	public NewTrailerIncidentPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable{
		this.driver = driver;
		this.reporter = reporter;
		waitForMask();
		printPageTitle(NewTrailerIncidentPage.title,"Page title");
	}
	
	public void selectTrailer(String trailer) throws Throwable{
		type(NewTrailerIncidentPage.trailer, trailer, "trailer");
		type(NewTrailerIncidentPage.trailer, Keys.TAB, "Tab key");
	}
	
	public void setLength(String length) throws Throwable{
		type(NewTrailerIncidentPage.length, length, "length");
		type(NewTrailerIncidentPage.length, Keys.TAB, "Tab key");
	}
	
	public void setManufacturer(String manufacturer) throws Throwable{
		type(NewTrailerIncidentPage.manufacturer, manufacturer, "manufacturer");
		type(NewTrailerIncidentPage.manufacturer, Keys.TAB, "Tab key");
	}
	
	public void setSerialNo(String serialNo) throws Throwable{
		type(NewTrailerIncidentPage.serialNo, serialNo, "serialNo");
		type(NewTrailerIncidentPage.serialNo, Keys.TAB, "Tab key");
	}
	
	public void setLossOccurred(String lossOccurred) throws Throwable{
		type(NewTrailerIncidentPage.lossOccurred, lossOccurred, "lossOccurred");
		type(NewTrailerIncidentPage.lossOccurred, Keys.TAB, "Tab key");
	}
	
	public void clickOk() throws Throwable{
		click(NewTrailerIncidentPage.okBtn, "ok btn");
	}
	
	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		String trailer = data.get("trailer");
		String trailerLength = data.get("trailerLength");
		String trailerManufacturer = data.get("trailerManufacturer");
		String trailerSerialNo = data.get("trailerSerialNo");
		String lossoccured = data.get("lossoccured");
		
		selectTrailer(trailer);
		setLength(trailerLength);
		setManufacturer(trailerManufacturer);
		setSerialNo(trailerSerialNo);
		setLossOccurred(lossoccured);
		clickOk();
	}
}
