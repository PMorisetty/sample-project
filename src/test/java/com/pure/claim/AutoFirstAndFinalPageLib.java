package com.pure.claim;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class AutoFirstAndFinalPageLib extends ActionEngine{
	
	public AutoFirstAndFinalPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	private void selectAutoBodyVendorReportedClaim() throws Throwable {
		click(AutoFirstAndFinalPage.autoBodyVendorReporter,
				"Radio button Auto Body Vendor");
	}

	private void selectInsuredReportedClaim() throws Throwable {
		click(AutoFirstAndFinalPage.insuredVendorReporter,
				"Radio button Insured");
	}
	
	private void selectCoverageInQuestionYes() throws Throwable {
		click(AutoFirstAndFinalPage.coverageInQuestionYes,
				"Radio button Coverage In Question YES");
	}

	private void selectCoverageInQuestionNo() throws Throwable {
		click(AutoFirstAndFinalPage.coverageInQuestionNo,
				"Radio button Coverage In Question NO");
	}
	
	private void selectVehicle(String vehicle) throws Throwable{
		type(AutoFirstAndFinalPage.vehicle, vehicle, "Vehicle");
		type(AutoFirstAndFinalPage.vehicle, Keys.TAB, "Tab");
		waitForMask();
	}
	
	private void setLossDescription(String description) throws Throwable{
		type(AutoFirstAndFinalPage.lossDescription, description, "Loss Description");
	}
	
	private void setClaimantName(String name) throws Throwable{
		type(AutoFirstAndFinalPage.claimantName, name, "Claimant Name");
		type(AutoFirstAndFinalPage.claimantName, Keys.TAB, "Tab");
	}
	
	private void setClaimantType(String type) throws Throwable{
		type(AutoFirstAndFinalPage.claimantType, type, "Claimant Type");
		type(AutoFirstAndFinalPage.claimantType, Keys.TAB, "Tab");
	}
	
	private void setClaimantDetails(Hashtable<String, String> data) throws Throwable{
		setClaimantName(data.get("claimantname"));
		setClaimantType(data.get("claimanttype"));
	}
	
	private void setFinancialName(String name) throws Throwable{
		type(AutoFirstAndFinalPage.financialsName, name, "Financials Name");
		type(AutoFirstAndFinalPage.financialsName, Keys.TAB, "Tab");
	}

	private void setFinancialPayeeType(String type) throws Throwable{
		type(AutoFirstAndFinalPage.financialsPayeeType, type, "Financials Payee Type");
		type(AutoFirstAndFinalPage.financialsPayeeType, Keys.TAB, "Tab");
	}

	private void setAmount(String amount) throws Throwable{
		type(AutoFirstAndFinalPage.amount, amount, "Financials Amount");
	}
	
	private void setFinancialsDetails(Hashtable<String, String> data) throws Throwable{
		setFinancialName(data.get("financialname"));
		setFinancialPayeeType(data.get("payeetype"));
		setAmount(data.get("amount"));
	}
	
	private void setLocation(String location) throws Throwable{
		type(AutoFirstAndFinalPage.location, location, "Location");
		type(AutoFirstAndFinalPage.location, Keys.TAB, "Tab");
		waitForMask();
	}
	
	public void clickFinish() throws Throwable{
		click(AutoFirstAndFinalPage.finishButton,"Finish button");
		waitForMask();
		boolean flag = false;
		try{
			flag = driver.findElement(AutoFirstAndFinalPage.duplicateClaimTab).isDisplayed();
		}catch(Exception ex){
			//Do nothing, as element is not expected to be present always
		}
		if(flag){
			isElementPresent(AutoFirstAndFinalPage.closeButton, "Close button");
			click(AutoFirstAndFinalPage.closeButton, "Close duplicate claims pop up");
			waitForInVisibilityOfElement(AutoFirstAndFinalPage.duplicateClaimTab,"Duplicate claim tab");
			click(AutoFirstAndFinalPage.finishButton,"Finish button");
			waitForMask();
		}
	}
	
	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		waitForMask();
		printPageTitle(AutoFirstAndFinalPage.title,"Page title");
		if(data.get("reportedClaim") != null){
			if(data.get("reportedClaim").toString().equalsIgnoreCase("Auto Body Vendor")){
				selectAutoBodyVendorReportedClaim();
			}else if(data.get("reportedClaim").toString().equalsIgnoreCase("Insured")){
				selectInsuredReportedClaim();
			}
		}
		if(data.get("coverageInQuestion") != null){
			if(data.get("coverageInQuestion").toString().equalsIgnoreCase("Yes")){
				selectCoverageInQuestionYes();
			}else{
				selectCoverageInQuestionNo();
			}
		}
		setLocation(data.get("location"));
		setClaimantDetails(data);
		setFinancialsDetails(data);
		selectVehicle(data.get("damageVehicle"));
		setLossDescription(data.get("lossDescription"));
		clickFinish();
	}
	
	public void searchVendor() throws Throwable{
		waitForMask();
		printPageTitle(AutoFirstAndFinalPage.title,"Page title");
		click(AutoFirstAndFinalPage.vendorMenuIcon, "Vendor menu icon");
		click(AutoFirstAndFinalPage.vendorSearchButton, "Vendor menu Search");
		waitForMask();
	}
}