package com.pure.claim.search;
import org.openqa.selenium.By;

public class SimpleSearchPage {
	public static By claimNumber;
	public static By policyNumber;
	public static By searchFor;
	public static By firstName;
	public static By lastName;
	public static By orgName;
	public static By taxID;
	public static By searchButton;
	public static By resetButton;
	public static By assignButton;
	public static By printExportButton;
	public static By resultTable;
	
	static {
		claimNumber = By.id("SimpleClaimSearch:SimpleClaimSearchScreen:SimpleClaimSearchDV:ClaimNumber-inputEl");
		policyNumber = By.id("SimpleClaimSearch:SimpleClaimSearchScreen:SimpleClaimSearchDV:PolicyNumber-inputEl");
		searchFor = By.id("SimpleClaimSearch:SimpleClaimSearchScreen:SimpleClaimSearchDV:SearchFor-inputEl");
		firstName = By.id("SimpleClaimSearch:SimpleClaimSearchScreen:SimpleClaimSearchDV:GlobalPersonNameInputSet:FirstName-inputEl");
		lastName = By.id("SimpleClaimSearch:SimpleClaimSearchScreen:SimpleClaimSearchDV:GlobalPersonNameInputSet:LastName-inputEl");
		orgName = By.id("SimpleClaimSearch:SimpleClaimSearchScreen:SimpleClaimSearchDV:GlobalContactNameInputSet:Name-inputEl");
		taxID = By.id("SimpleClaimSearch:SimpleClaimSearchScreen:SimpleClaimSearchDV:TaxID-inputEl");
		searchButton = By.id("SimpleClaimSearch:SimpleClaimSearchScreen:SimpleClaimSearchDV:ClaimSearchAndResetInputSet:Search");
		resetButton = By.id("SimpleClaimSearch:SimpleClaimSearchScreen:SimpleClaimSearchDV:ClaimSearchAndResetInputSet:Reset");
		assignButton = By.id("SimpleClaimSearch:SimpleClaimSearchScreen:SimpleClaimSearchResultsLV_tb:ClaimSearchScreen_AssignButton-btnInnerEl");
		printExportButton = By.id("SimpleClaimSearch:SimpleClaimSearchScreen:SimpleClaimSearchResultsLV_tb:SimpleClaimSearchScreen_PrintButton-btnInnerEl");
		resultTable = By.cssSelector("table[id^=\"gridview\"]");
	}
}
;