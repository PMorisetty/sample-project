package com.pure.claim.search;
import java.util.Arrays;
import java.util.List;

import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.pure.accelerators.ActionEngine;

@SuppressWarnings("unchecked" )
public class AdvancedSearchLib extends ActionEngine{
	private JSONObject searchData;
	public String getClaimNumber() throws Throwable {
		return getText(AdvancedSearchPage.claimNumber, "Claim Number");
	}
	
	public void setClaimNumber(String claimNumber) throws Throwable{
		type(AdvancedSearchPage.claimNumber, claimNumber, "Claim Number text box");
		searchData.put("claimNumber", claimNumber);
	}
	
	public String getPolicyNumber() throws Throwable {
		return getText(AdvancedSearchPage.policyNumber, "Policy Number");
	}
	
	public void setPolicyNumber(String policyNumber) throws Throwable{
		type(AdvancedSearchPage.policyNumber, policyNumber, "Policy Number text box");
		searchData.put("policyNumber", policyNumber);
	}
	
	public void selectSearchFor(String searchFor)throws Throwable{
		type(AdvancedSearchPage.searchFor, searchFor, "Search For");
		if(!searchFor.equalsIgnoreCase("<none>")){
			searchData.put("searchFor", searchFor);
		}
	}
	
	public String getFirstName() throws Throwable {
		return getText(AdvancedSearchPage.firstName, "First Name");
	}
	
	public void setFirstName(String firstName) throws Throwable{
		type(AdvancedSearchPage.firstName, firstName, "First Name text box");
		searchData.put("firstName", firstName);
	}
	
	public String getLastName() throws Throwable {
		return getText(AdvancedSearchPage.lastName, "Last Name");
	}
	
	public void setLastName(String lastName) throws Throwable {
		type(AdvancedSearchPage.lastName, lastName, "Last Name text box");
		searchData.put("lastName", lastName);
	}
	
	public String getOrgName() throws Throwable {
		return getText(AdvancedSearchPage.orgName, "Org Name");
	}
	
	public void setOrgName(String orgName) throws Throwable{
		type(AdvancedSearchPage.orgName, orgName, "Org Name text box");
		searchData.put("orgName", orgName);
	}
	
	public String getTaxID() throws Throwable {
		return getText(AdvancedSearchPage.taxID, "Tax ID");
	}
	
	public void setTaxID(String taxID) throws Throwable {
		type(AdvancedSearchPage.taxID, taxID, "Tax ID text box");
		searchData.put("taxID", taxID);
	}
	
	public void selectAssignedToGroup(String assignedToGroup)throws Throwable{
		type(AdvancedSearchPage.assignedToGroup, assignedToGroup, "Assigned To Group");
		if(!assignedToGroup.equalsIgnoreCase("<none>")){
			searchData.put("assignedToGroup", assignedToGroup);
		}
	}
	
	public void setIncludeChildGroups(boolean value) throws Throwable{
		if(value){
			click(AdvancedSearchPage.includeChildGroupYes, "Include Child Group Yes");
		}else{
			click(AdvancedSearchPage.includeChildGroupNo, "Include Child Group No");
		}
	}
	
	public void selectAssignedToUser(String assignedToUser)throws Throwable{
		type(AdvancedSearchPage.assignedToUser, assignedToUser, "Assigned To User");
		if(!assignedToUser.equalsIgnoreCase("<none>")){
			searchData.put("assignedToUser", assignedToUser);
		}
	}
	
	public void selectCreatedBy(String createdBy)throws Throwable{
		type(AdvancedSearchPage.createdBy, createdBy, "Created By");
		if(!createdBy.equalsIgnoreCase("<none>")){
			searchData.put("createdBy", createdBy);
		}
	}
	
	public String getCatNumber() throws Throwable {
		return getText(AdvancedSearchPage.catNumber, "CAT#");
	}
	
	public void setCatNumber(String catNumber) throws Throwable {
		type(AdvancedSearchPage.catNumber, catNumber, "CAT# text box");
		searchData.put("catNumber", catNumber);
	}
	
	public String getVinNumber() throws Throwable {
		return getText(AdvancedSearchPage.vinNumber, "VIN Number");
	}
	
	public void setVinNumber(String vinNumber) throws Throwable {
		type(AdvancedSearchPage.vinNumber, vinNumber, "VIN Number text box");
		searchData.put("vinNumber", vinNumber);
	}
	
	public String getLicenseNumber() throws Throwable {
		return getText(AdvancedSearchPage.licenseNumber, "License Number");
	}
	
	public void setLicenseNumber(String licenseNumber) throws Throwable {
		type(AdvancedSearchPage.licenseNumber, licenseNumber, "License Number text box");
		searchData.put("licenseNumber", licenseNumber);
	}
	
	public void selectJurisdiction(String jurisdiction)throws Throwable{
		type(AdvancedSearchPage.jurisdiction, jurisdiction, "Jurisdiction");
		if(!jurisdiction.equalsIgnoreCase("<none>")){
			searchData.put("jurisdiction", jurisdiction);
		}
	}
	
	public void selectClaimStatus(String claimStatus)throws Throwable{
		type(AdvancedSearchPage.claimStatus, claimStatus, "Claim Status");
		if(!claimStatus.equalsIgnoreCase("<none>")){
			searchData.put("claimStatus", claimStatus);
		}
	}
	
	public void selectLineOfBusiness(String lineOfBusiness)throws Throwable{
		type(AdvancedSearchPage.lineOfBusiness, lineOfBusiness, "Line Of Business");
		if(!lineOfBusiness.equalsIgnoreCase("<none>")){
			searchData.put("lineOfBusiness", lineOfBusiness);
		}
	}
	
	public void selectLossType(String lossType)throws Throwable{
		type(AdvancedSearchPage.lossType, lossType, "Loss Type");
		if(!lossType.equalsIgnoreCase("<none>")){
			searchData.put("lossType", lossType);
		}
	}
	
	public void selectPolicyType(String policyType)throws Throwable{
		type(AdvancedSearchPage.policyType, policyType, "Policy Type");
		if(!policyType.equalsIgnoreCase("<none>")){
			searchData.put("policyType", policyType);
		}
	}
	
	public void selectPendingAssignment(String pendingAssignment)throws Throwable{
		type(AdvancedSearchPage.pendingAssignment, pendingAssignment, "Pending Assignment");
		if(!pendingAssignment.equalsIgnoreCase("<none>")){
			searchData.put("pendingAssignment", pendingAssignment);
		}
	}
	
	public void selectIncidentReport(String incidentReport)throws Throwable{
		type(AdvancedSearchPage.incidentReport, incidentReport, "IncidentReport");
		if(!incidentReport.equalsIgnoreCase("<none>")){
			searchData.put("incidentReport", incidentReport);
		}
	}
	
	public void selectReinsuranceReportable(String reinsuranceReportable)throws Throwable{
		type(AdvancedSearchPage.reinsuranceReportable, reinsuranceReportable, "Reinsurance Reportable");
		if(!reinsuranceReportable.equalsIgnoreCase("<none>")){
			searchData.put("jurisdiction", reinsuranceReportable);
		}
	}
	
	public void selectCoverageInQuestion(String coverageInQuestion)throws Throwable{
		type(AdvancedSearchPage.coverageInQuestion, coverageInQuestion, "Coverage In Question");
		if(!coverageInQuestion.equalsIgnoreCase("<none>")){
			searchData.put("coverageInQuestion", coverageInQuestion);
		}
	}
	
	public void selectFlaggedType(String flaggedType)throws Throwable{
		type(AdvancedSearchPage.jurisdiction, flaggedType, "Flagged Type");
		if(!flaggedType.equalsIgnoreCase("<none>")){
			searchData.put("flaggedType", flaggedType);
		}
	}
	
	public void selectLitigationStatus(String litigationStatus)throws Throwable{
		type(AdvancedSearchPage.litigationStatus, litigationStatus, "Litigation Status");
		if(!litigationStatus.equalsIgnoreCase("<none>")){
			searchData.put("litigationStatus", litigationStatus);
		}
	}
	
	public void selectSubrogationStatus(String subrogationStatus)throws Throwable{
		type(AdvancedSearchPage.subrogationStatus, subrogationStatus, "Subrogation Status");
		if(!subrogationStatus.equalsIgnoreCase("<none>")){
			searchData.put("subrogationStatus", subrogationStatus);
		}
	}
	
	public void selectSearchForDate(String searchForDate)throws Throwable{
		type(AdvancedSearchPage.searchForDate, searchForDate, "Search For Date");
		if(!searchForDate.equalsIgnoreCase("<none>")){
			searchData.put("searchForDate", searchForDate);
		}
	}
	
	public void selectSearchForDateSinceRadio()throws Throwable{
		click(AdvancedSearchPage.searchForDateSinceRadio, "Search For Date Since Radio Button");
	}
	
	public void selectSearchForDateFromRadio()throws Throwable{
		click(AdvancedSearchPage.searchForDateFromRadio, "Search For Date From Radio Button");
	}
	
	public void setSearchForDateSinceText(String searchForDateSinceText) throws Throwable{
		type(AdvancedSearchPage.searchForDateSinceText, searchForDateSinceText, "Search For Date Since Text Box");
		searchData.put("searchForDateSinceText", searchForDateSinceText);
	}
	
	public void setSearchForDateText(String searchForFromDateText) throws Throwable{
		type(AdvancedSearchPage.searchForFromDateText, searchForFromDateText, "Search For - From Date Text Box");
		searchData.put("searchForFromDateText", searchForFromDateText);
	}
	public void setSearchToDateText(String searchForToDateText) throws Throwable{
		type(AdvancedSearchPage.searchForToDateText, searchForToDateText, "Search For - To Date Text Box");
		searchData.put("searchForToDateText", searchForToDateText);
	}
	
	public void selectFinancialType(String financialType) throws Throwable{
		type(AdvancedSearchPage.financialType, financialType, "Financial Type Text Box");
		searchData.put("financialType", financialType);
	}
	
	public void setFinancialFrom(String financialFrom) throws Throwable{
		type(AdvancedSearchPage.financialFrom, financialFrom, "Financial From Text Box");
	}
	
	public String getFinancialFrom() throws Throwable{
		return getText(AdvancedSearchPage.financialFrom, "Financial From Text Box");
	}
	
	public void setFinancialTo(String financialTo) throws Throwable{
		type(AdvancedSearchPage.financialTo, financialTo, "Financial To Text Box");
	}
	
	public String getFinancialTo() throws Throwable{
		return getText(AdvancedSearchPage.financialTo, "Financial To Text Box");
	}
	
	public void clickSearch() throws Throwable{
		click(AdvancedSearchPage.searchButton, "Search Button");
	}
	
	public void clickReset() throws Throwable{
		click(AdvancedSearchPage.resetButton, "Reset Button");
		searchData.clear();
	}
	
	public void clickAssign() throws Throwable{
		click(AdvancedSearchPage.assignButton, "Assign Button");
	}
	
	public void clickPrintExport() throws Throwable{
		click(AdvancedSearchPage.printExportButton, "Print/Export Button");
	}
	
	private int columnNumber(String columnName){
		int colNum = 0;
		switch (columnName) {
		case "claimNumber":
			colNum = 3;
			break;
		case "policyNumber":
			colNum = 5;
			break;
		case "Claimant":
			colNum = 6;
			break;
		case "Insured":
			colNum = 4;
			break;
		case "claimStatus":
			colNum = 9;
			break;
		case "Loss date":
			colNum = 7;
			break;
		case "assignedToUser":
			colNum = 8;
			break;
		case "Remaining Reserves":
			colNum = 10;
			break;
		case "Future Payments":
			colNum = 11;
			break;
		case "Total Paid":
			colNum = 12;
			break;
			
		}
		return colNum;
	}
	
	public void verifySearch() throws Throwable{
		WebElement tbody = driver.findElement(SimpleSearchPage.resultTable).findElement(By.cssSelector("tbody[id^=\"gridview\"]"));
		List<WebElement> rows = tbody.findElements(By.cssSelector("tr"));
		if(!rows.isEmpty()){
			for(int i=0; i<rows.size(); i++){
				for (Object key : searchData.keySet()) {
					String keyStr = key.toString();
					String keyValue = searchData.get(keyStr).toString();
					if(Arrays.asList(new String[]{"1","2","3"}).contains(keyStr)){
					String columnValue;
					int columnNumber;
					String[] values = {"searchFor", "financialType"};
					columnValue = (Arrays.asList(values).contains(keyStr))?keyValue:keyStr;
					columnNumber = columnNumber(columnValue);
					WebElement columnEl = rows.get(i).findElements(By.cssSelector("td")).get(columnNumber);
					String columnText = columnEl.getAttribute("innerText").trim();
					
					if(keyStr.equalsIgnoreCase("financialType")){
						assertTrue((Integer.parseInt(getFinancialFrom())>= Integer.parseInt(columnText.substring(1)) && (Integer.parseInt(getFinancialTo())<= Integer.parseInt(columnText.substring(1)))), columnValue + " is in Range");
					}else if(keyStr.equalsIgnoreCase("searchFor")){
						contains(columnText, getFirstName());
						contains(columnText, getLastName());
					}else{
						contains(columnText, columnValue);
					}
					}
				}
			}
		}		
	}
}
