package com.pure.claim.search;
import java.util.List;

import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.pure.accelerators.ActionEngine;

@SuppressWarnings( "unchecked" )
public class SimpleSearchLib extends ActionEngine{
	private JSONObject searchData;
	
	public String getClaimNumber() throws Throwable {
		return getText(SimpleSearchPage.claimNumber, "Claim Number");
	}
	
	public void setClaimNumber(String claimNumber) throws Throwable{
		type(SimpleSearchPage.claimNumber, claimNumber, "Claim Number text box");
		searchData.put("claimNumber", claimNumber);
	}
	
	public String getPolicyNumber() throws Throwable {
		return getText(SimpleSearchPage.policyNumber, "Policy Number");
	}
	
	public void setPolicyNumber(String policyNumber) throws Throwable {
		type(SimpleSearchPage.policyNumber, policyNumber, "Policy Number text box");
		searchData.put("policyNumber", policyNumber);
	}
	
	public void selectSearchFor(String searchFor)throws Throwable{
		type(SimpleSearchPage.searchFor, searchFor, "Search For");
		if(!searchFor.equalsIgnoreCase("<none>")){
			searchData.put("searchFor", searchFor);
		}
	}
	
	public String getFirstName() throws Throwable {
		return getText(SimpleSearchPage.firstName, "First Name");
	}
	
	public void setFirstName(String firstName) throws Throwable{
		type(SimpleSearchPage.firstName, firstName, "First Name text box");
		searchData.put("firstName", firstName);
	}
	
	public String getLastName() throws Throwable {
		return getText(SimpleSearchPage.lastName, "Last Name");
	}
	
	public void setLastName(String lastName) throws Throwable {
		type(SimpleSearchPage.lastName, lastName, "Last Name text box");
		searchData.put("lastName", lastName);
	}
	
	public String getOrgName() throws Throwable {
		return getText(SimpleSearchPage.orgName, "Org Name");
	}
	
	public void setOrgName(String orgName) throws Throwable{
		type(SimpleSearchPage.orgName, orgName, "Org Name text box");
		searchData.put("orgName", orgName);
	}
	
	public String getTaxID() throws Throwable {
		return getText(SimpleSearchPage.taxID, "Tax ID");
	}
	
	public void setTaxID(String taxID) throws Throwable {
		type(SimpleSearchPage.taxID, taxID, "Tax ID text box");
		searchData.put("taxID", taxID);
	}
	
	public void clickSearch() throws Throwable{
		click(SimpleSearchPage.searchButton, "Search Button");
	}
	
	public void clickReset() throws Throwable{
		click(SimpleSearchPage.resetButton, "Reset Button");
		searchData.clear();
	}
	
	public void clickAssign() throws Throwable{
		click(SimpleSearchPage.assignButton, "Assign Button");
	}
	
	public void clickPrintExport() throws Throwable{
		click(SimpleSearchPage.printExportButton, "Print/Export Button");
	}
	
	private int columnNumber(String columnName){
		int colNum = 0;
		switch (columnName) {
		case "claimNumber":
			colNum = 3;
			break;
		case "policyNumber":
			colNum = 5;
			break;
		case "Claimant":
			colNum = 6;
			break;
		case "Insured":
			colNum = 4;
			break;
		}
		return colNum;
	}
	
	public void verifySearch() throws Throwable{
		WebElement tbody = driver.findElement(SimpleSearchPage.resultTable).findElement(By.cssSelector("tbody[id^=\"gridview\"]"));
		List<WebElement> rows = tbody.findElements(By.cssSelector("tr"));
		if(!rows.isEmpty()){
			for(int i=0; i<rows.size(); i++){
				for (Object key : searchData.keySet()) {
					String keyStr = key.toString();
					String keyValue = searchData.get(keyStr).toString();
					String columnValue;
					int columnNumber;
					columnValue = (!keyStr.equalsIgnoreCase("searchFor"))? keyValue:keyStr;
					columnNumber = columnNumber(columnValue);
					WebElement columnEl = rows.get(i).findElements(By.cssSelector("td")).get(columnNumber);
					String columnText = columnEl.getAttribute("innerText").trim();
					if(!keyStr.equalsIgnoreCase("searchFor")){
						contains(columnText, columnValue);
					}else{
						contains(columnText, getFirstName());
						contains(columnText, getLastName());
					}
				}
			}
		}		
	}
}
