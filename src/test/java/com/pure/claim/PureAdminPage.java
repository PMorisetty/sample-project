package com.pure.claim;

import org.openqa.selenium.By;

public class PureAdminPage {
	
	static By monitoringLink;
	public static By messageQueuesLink;
	static{
		monitoringLink = By.xpath("//span[text()='Monitoring']");
		messageQueuesLink = By.xpath("//span[text()='Message Queues']");
	}
}
