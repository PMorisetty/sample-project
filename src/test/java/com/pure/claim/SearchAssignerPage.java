package com.pure.claim;

import org.openqa.selenium.By;

public class SearchAssignerPage {

	static By username, searchBtn,assignBtnForSecondaryApprover, title, selecFromList, assignBtn;
	static{
		username = By.id("AssigneePickerPopup:AssigneePickerScreen:AssignmentSearchDV:Username-inputEl");
		searchBtn = By.id("AssigneePickerPopup:AssigneePickerScreen:AssignmentSearchDV:SearchAndResetInputSet:SearchLinksInputSet:Search");
		assignBtnForSecondaryApprover = By.xpath("//span[text()='Secondary Approval']/../../..//a[text()='Assign']");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		selecFromList = By.id("AssignActivitiesPopup:AssignmentPopupScreen:AssignmentPopupDV:SelectFromList-inputEl");
		assignBtn = By.id("AssignActivitiesPopup:AssignmentPopupScreen:AssignmentPopupDV:cAssignmentPopupScreen_ButtonButton-btnInnerEl");
	}
}
