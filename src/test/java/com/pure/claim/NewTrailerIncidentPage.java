package com.pure.claim;

import org.openqa.selenium.By;

public class NewTrailerIncidentPage {
	public static By trailer, length, manufacturer, serialNo, lossOccurred, title, okBtn;
	
	static{
		title = By.xpath("//span[@class='g-title']");
		trailer = By.xpath("//input[contains(@id,'PURE_TrailerIncidentDV:TrailerPicker-inputEl')]");
		length = By.xpath("//input[contains(@id,'PURE_TrailerIncidentDV:Vessel_Length-inputEl')]");
		manufacturer = By.xpath("//input[contains(@id,'PURE_TrailerIncidentDV:Vessel_Manufacturer-inputEl')]");
		serialNo = By.xpath("//input[contains(@id,'PURE_TrailerIncidentDV:Vessle_HullID-inputEl')]");
		lossOccurred = By.xpath("//input[contains(@id,'PURE_TrailerIncidentDV:LossOccured-inputEl')]");
		okBtn = By.id("PURE_NewTrailerIncidentPopup:NewWatercraftIncidentScreen:Update-btnInnerEl");
	}
}
