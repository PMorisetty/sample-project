package com.pure.claim;

import org.openqa.selenium.By;

public class ActionsPage {

	public static By ActionsMenu,
		Note,noteTitle,Email,Litigation,Negotiation,Rental,Service,NewTransaction,Reserve,
		Check,Other,NewDocument,CreateFromTemplate,
		AttachAnExistingDocument,IndicateExistenceOfDocument,
		NewActivity, correspondence,General,Interview,NewMail,Reminder,Request,Warning, sendClaimAcknowledgementLetter,
		NewExposure,ChooseByCoverage,ClaimActions,AssignClaim,CloseClaim,PrintClaim,SyncStatus,
		ValidateClaimOnly,ValidateClaimAndExposure,ValidatePolicy,validForISO,abilityToPay, returnToClaimCenter,
		recoveryReserve, recovery, reopenClaim, closeClaim, abilityToPay_claims;
	
//	=============== FNOL elements ====================
	public static By fnolActionsMenu,
		fnolNewExposure,
		fnolChooseByCoverage,
		fnolMiscValuablesScheduled,
		fnolPersonalExcessCoverageType,
		fnolPersonalLiabilityPhysicalDamage;
	
//	=============== New Exposure elements ====================
	public static By policyLevelCoverage,liability, liabilityPropertyDamage, liabilityVehicleDamage,
		collisionVehicleDamage, comprehensiveVehicleDamage, liabilityMedicalPayments,
		liabilityCombinedSingleLimit, liabilityAutoBodilyInjury, towingAndLabor, firstPartyRental,
		underInsuredMotorist, underInsuredMotoristBodilyInjury, autoPropertyDamageProperty,
		liability_Property_damage, liability_Property_damage_property, liability_Property_damage_vehicle,
		medicalPayments, uninsuredMotoristBI, liabilityBodilyInjuryDamage, liabilityPersonalInjuryDamage,
		liabilityIncidentalBusinessDamage, liabilityLossOfUseDamage,
		fruadAndCyber, cyberAttack, cyberExtortion, fraud,
		building, rebuildingToCode, otherStructures, otherStructuresEarthQuake, otherStructuresExcessFlood, otherStructuresPropertyDamage,
		contents, contentsAtAListedResidence, contentsAtAnUnlistedResidence, contentsAwayFromAResidence,
		contentsEarthQuake, contentsExcessFlood, contentsMoldRemediation,
		identityFraudProtection, identityFraudCreditCard, identityFraudIdentityFraud,
		additionalLivingExpenses, additionalLivingExpensesEarthquake, additionalLivingExpensesExcessFlood, additionalLivingExpensesLossOfUseDamage,
		buildingDebrisRemoval, buildingDemandSurge, buildingEarthquake, buildingExcessFlood,
		buildingExtendedRebuildingProvision, buildingLandStabilization, buildingLandscaping, buildingLossAssessment,
		buildingLossMitigationMeasures, buildingMoldRemediation, buildingPropertyDamage,
		yachtHullDamage, yachtMachineryDamage, yachtTenderDamage, yachtRental, yachtInspectionAndPrecautionary,
		emergencyExpenses, fineArtContents, personalEffectsPersonalProperty,
		jonesAct, liabilityBIDamageWithSpaces, liabilityPropertyDamageWithSpaces, liabilityWatercraftDamage,
		trailerDamage, uninsuredBoaterBIDamage, additionalLivingExpensesLossOfUse, equipmentBreakdownHomeSystems;
	 
	
	static{
		ActionsMenu = By.xpath("//span[contains(@id,'MenuActions-btnInnerEl')]/../../..");
		Note 		= By.xpath("//span[contains(@id,'ClaimMenuActions_NewNote-textEl')]");
		Email		= By.id("Claim:ClaimMenuActions:ClaimNewOtherMenuItemSet:ClaimMenuActions_NewOther:ClaimMenuActions_Email-itemEl");
		Litigation	= By.id("Claim:ClaimMenuActions:ClaimNewOtherMenuItemSet:ClaimMenuActions_NewOther:ClaimMenuActions_NewMatter-itemEl");
		Negotiation = By.id("Claim:ClaimMenuActions:ClaimNewOtherMenuItemSet:ClaimMenuActions_NewOther:ClaimMenuActions_NewNegotiation-itemEl");
		Rental	 	= By.id("Claim:ClaimMenuActions:ClaimNewOtherMenuItemSet:ClaimMenuActions_NewOther:ClaimMenuActions_NewRental-itemEl");
		Service 	= By.id("Claim:ClaimMenuActions:ClaimNewOtherMenuItemSet:ClaimMenuActions_NewOther:ClaimMenuActions_NewServiceRequest-itemEl");
		
		NewTransaction 	= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewTransaction-itemEl");
		Reserve 		= By.xpath("//span[text()='Reserve']");
		Check			= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewTransaction:ClaimMenuActions_NewTransaction_CheckSet-itemEl");
		Other 			= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewTransaction:ClaimMenuActions_NewOtherTrans-itemEl");
		NewDocument 	= By.xpath("//span[text()='New Document']");
		CreateFromTemplate 			= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewDocument:ClaimNewDocumentMenuItemSet:ClaimNewDocumentMenuItemSet_Create-itemEl");
		AttachAnExistingDocument	= By.xpath("//span[text()='Attach an existing document']");
		IndicateExistenceOfDocument = By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewDocument:ClaimNewDocumentMenuItemSet:ClaimNewDocumentMenuItemSet_IndicateExists-itemEl");

		NewActivity 	= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewActivity-itemEl");
		correspondence 	= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewActivity:NewActivityMenuItemSet:0:NewActivityMenuItemSet_Category-itemEl"); 
		General 		= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewActivity:NewActivityMenuItemSet:1:NewActivityMenuItemSet_Category-itemEl");
		Interview 		= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewActivity:NewActivityMenuItemSet:2:NewActivityMenuItemSet_Category-itemEl"); 
		NewMail 		= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewActivity:NewActivityMenuItemSet:3:NewActivityMenuItemSet_Category-itemEl");
		Reminder 		= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewActivity:NewActivityMenuItemSet:4:NewActivityMenuItemSet_Category-itemEl");
		Request 		= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewActivity:NewActivityMenuItemSet:5:NewActivityMenuItemSet_Category-itemEl");
		Warning 		= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewActivity:NewActivityMenuItemSet:6:NewActivityMenuItemSet_Category-itemEl");
		NewExposure 	= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewExposure-itemEl");
		ChooseByCoverage= By.id("Claim:ClaimMenuActions:ClaimMenuActions_NewExposure:NewExposureMenuItemSet:NewExposureMenuItemSet_ByCoverage-itemEl");
		ClaimActions 	= By.id("Claim:ClaimMenuActions:ClaimFileMenuItemSet:ClaimMenuActions_ClaimActions-itemEl");
		AssignClaim 	= By.id("Claim:ClaimMenuActions:ClaimFileMenuItemSet:ClaimMenuActions_ClaimActions:ClaimMenuActions_Assign-itemEl");
		CloseClaim 		= By.xpath("//span[text()='Close Claim']");
		PrintClaim 		= By.id("Claim:ClaimMenuActions:ClaimFileMenuItemSet:ClaimMenuActions_ClaimActions:ClaimMenuActions_Print-itemEl");
		SyncStatus 		= By.id("Claim:ClaimMenuActions:ClaimFileMenuItemSet:ClaimMenuActions_ClaimActions:ClaimMenuActions_SyncStatus-itemEl");
		ValidateClaimOnly 			= By.id("Claim:ClaimMenuActions:ClaimFileMenuItemSet:ClaimMenuActions_ClaimActions:ClaimMenuActions_ClaimValidation-itemEl");
		ValidateClaimAndExposure 	= By.id("Claim:ClaimMenuActions:ClaimFileMenuItemSet:ClaimMenuActions_ClaimActions:ClaimMenuActions_ClaimExposureValidation-itemEl");
		ValidatePolicy 				= By.id("Claim:ClaimMenuActions:ClaimFileMenuItemSet:ClaimMenuActions_ClaimActions:ClaimMenuActions_PolicyValidation-itemEl");
		validForISO = By.id("Claim:ClaimMenuActions:ClaimFileMenuItemSet:ClaimMenuActions_ClaimActions:ClaimMenuActions_ClaimExposureValidation:2:item-textEl");
		abilityToPay = By.id("Claim:ClaimMenuActions:ClaimFileMenuItemSet:ClaimMenuActions_ClaimActions:ClaimMenuActions_ClaimExposureValidation:4:item-textEl");
		abilityToPay_claims = By.id("Claim:ClaimMenuActions:ClaimFileMenuItemSet:ClaimMenuActions_ClaimActions:ClaimMenuActions_ClaimValidation:4:item-textEl");

		
		//======================= FNOL elements ================
		fnolActionsMenu = By.xpath("//span[@id='FNOLWizard:FNOLMenuActions-btnInnerEl']/../../..");
		fnolNewExposure = By.xpath("//span[text()='New Exposure']");
		fnolChooseByCoverage = By.xpath("//span[text()='Choose by Coverage Type']");
		fnolMiscValuablesScheduled = By.xpath("//span[text()='Misc. Valuables Scheduled']");
		fnolPersonalExcessCoverageType = By.xpath("//span[text()='Personal Excess Coverage' and contains(@id,'ByCoverageType')]");
		fnolPersonalLiabilityPhysicalDamage = By.xpath("//span[text()='Personal Excess Personal Liability Physical Damage' and contains(@id,'ByCoverageType')]");
		
		//======================= New Exposure elements ================
		policyLevelCoverage = By.xpath("//span[text()='Policy Level Coverage']");
		liability = By.xpath("//span[text()='Liability']");
		liabilityPropertyDamage = By.xpath("//span[text()='Liability - PropertyDamage']");
		liabilityVehicleDamage = By.xpath("//span[text()='Liability - VehicleDamage']");
		liabilityMedicalPayments = By.xpath("//span[text()='Liability Medical Payment to Others - MedPay']");
		liability_Property_damage = By.xpath("//span[text()='Liability - Property damage']");
		liability_Property_damage_property = By.xpath("//span[text()='Liability - Property Damage - Property']");
		liability_Property_damage_vehicle = By.xpath("//span[text()='Liability - Property Damage - Vehicle']");				
		medicalPayments = By.xpath("//span[text()='Medical Payments']");
		uninsuredMotoristBI = By.xpath("//span[text()='Uninsured Motorist - BI']");
		collisionVehicleDamage = By.xpath("//span[text()='Collision - Vehicle Damage']");
		comprehensiveVehicleDamage = By.xpath("//span[text()='Comprehensive - Vehicle Damage']");
		liabilityCombinedSingleLimit = By.xpath("//span[text()='Liability Combined Single Limit']");
		liabilityAutoBodilyInjury = By.xpath("//span[text()='Liability - Auto bodily injury - Bodily Injury Damage']");
		towingAndLabor = By.xpath("//span[text()='Towing and Labor']");
		firstPartyRental = By.xpath("//span[text()='1st Party Rental']");
		underInsuredMotorist = By.xpath("//span[text()='UnderInsured Motorists']");
		underInsuredMotoristBodilyInjury = By.xpath("//span[text()='Under Insured Motorist - Bodily Injury']");
		liabilityBodilyInjuryDamage = By.xpath("//span[text()='Liability - BodilyInjuryDamage']");
		liabilityPersonalInjuryDamage = By.xpath("//span[text()='Liability - Personal Injury Damage']");
		liabilityIncidentalBusinessDamage = By.xpath("//span[text()='Liability - Incidental Business Damage ']");
		liabilityLossOfUseDamage = By.xpath("//span[text()='Liability - LossOfUseDamage']");
		autoPropertyDamageProperty = By.xpath("//span[text()='Auto property damage - Property']");
		fruadAndCyber = By.xpath("//span[text()='Fraud and Cyber Defense']");
		cyberAttack = By.xpath("//span[text()='Cyber Attack']");
		cyberExtortion = By.xpath("//span[text()='Cyber Extortion']");
		fraud = By.xpath("//span[text()='Fraud']");
		building = By.xpath("//span[text()='Building']");
		rebuildingToCode = By.xpath("//span[text()='Building - Rebuilding to Code']");
		buildingDebrisRemoval = By.xpath("//span[text()='Building - Debris Removal']");
		buildingDemandSurge = By.xpath("//span[text()='Building - Demand Surge']");
		buildingEarthquake = By.xpath("//span[text()='Building - Earthquake']");
		buildingExcessFlood = By.xpath("//span[text()='Building - Excess Flood']");
		buildingExtendedRebuildingProvision = By.xpath("//span[text()='Building - Extended Rebuilding Provision']");
		buildingLandStabilization = By.xpath("//span[text()='Building - Land Stabilization']");
		buildingLandscaping = By.xpath("//span[text()='Building - Landscaping ']");
		buildingLossAssessment = By.xpath("//span[text()='Building - Loss Assessment ']");
		buildingLossMitigationMeasures = By.xpath("//span[text()='Building - Loss Mitigation Measures']");
		buildingMoldRemediation = By.xpath("//span[text()='Building - Mold Remediation']");
		buildingPropertyDamage = By.xpath("//span[text()='Building - PropertyDamage']");
		otherStructures = By.xpath("//span[text()='Other Structures']");
		otherStructuresEarthQuake = By.xpath("//span[text()='Other Structures - Earthquake']");
		otherStructuresExcessFlood = By.xpath("//span[text()='Other Structures - Excess Flood']");
		otherStructuresPropertyDamage = By.xpath("//span[text()='Other Structures - Property Damage']");
		contents = By.xpath("//span[text()='Contents']");
		contentsAtAListedResidence = By.xpath("//span[text()='Contents - At a listed Residence']");
		contentsAtAnUnlistedResidence = By.xpath("//span[text()='Contents - At an unlisted Residence']");
		contentsAwayFromAResidence = By.xpath("//span[text()='Contents - Away from a Residence']");
		contentsEarthQuake = By.xpath("//span[text()='Contents - Earthquake']");
		contentsExcessFlood = By.xpath("//span[text()='Contents - Excess Flood']");
		contentsMoldRemediation = By.xpath("//span[text()='Contents - Mold Remediation']");
		identityFraudProtection = By.xpath("//span[text()='Identity Fraud Protection']");
		identityFraudCreditCard = By.xpath("//span[text()='Identity Fraud - Credit Card Fraud / Forgery']");
		identityFraudIdentityFraud = By.xpath("//span[text()='Identity Fraud - Identity Fraud']");
		additionalLivingExpenses  = By.xpath("//span[text()='Additional Living Expenses']");
		additionalLivingExpensesEarthquake = By.xpath("//span[text()='Additional Living Expenses - Earthquake']");
		additionalLivingExpensesExcessFlood = By.xpath("//span[text()='Additional Living Expenses - Excess Flood ']");
		additionalLivingExpensesLossOfUseDamage = By.xpath("//span[text()='Additional Living Expenses - Loss of Use Damage']");
		additionalLivingExpensesLossOfUse = By.xpath("//span[text()='Additional Living Expenses - Loss of Use ']");
		yachtHullDamage = By.xpath("//span[text()='Yacht - Hull Damage']");
		yachtMachineryDamage = By.xpath("//span[text()='Yacht - Machinery damage']");
		yachtTenderDamage = By.xpath("//span[text()='Yacht - Tender Damage']");
		yachtRental = By.xpath("//span[text()='Yacht - Rental']");
		yachtInspectionAndPrecautionary = By.xpath("//span[text()='Inspection and Precautionary']");
		emergencyExpenses = By.xpath("//span[text()='Emergency Expenses']");
		fineArtContents = By.xpath("//span[text()='Fine Art - Contents']");
		personalEffectsPersonalProperty = By.xpath("//span[text()='Personal Effects - Personal Property']");
		jonesAct = By.xpath("//span[text()='Jones Act']");
		liabilityBIDamageWithSpaces = By.xpath("//span[text()='Liability - Bodily Injury Damage']");
		liabilityPropertyDamageWithSpaces = By.xpath("//span[text()='Liability - Property Damage']");
		liabilityWatercraftDamage = By.xpath("//span[text()='Liability - Watercraft Damage']");
		trailerDamage = By.xpath("//span[text()='Trailer - Trailer Damage']");
		uninsuredBoaterBIDamage = By.xpath("//span[text()='Uninsured boater - Bodily Injury Damage']");
		equipmentBreakdownHomeSystems = By.xpath("//span[text()='Equipment Breakdown - Home Systems']");
		
		//======================= New Activity Elements ================
		sendClaimAcknowledgementLetter = By.xpath("//span[text()='Send claim acknowledgement letter']");
		//======================== Batch process info page=================
		returnToClaimCenter = By.xpath("//span[text()='Return to ClaimCenter']");
		
		recoveryReserve = By.xpath("//span[contains(@id,'ClaimMenuActions_NewTransaction_RecoveryReserveSet-textEl')]");
		recovery = By.xpath("//span[contains(@id,'ClaimMenuActions_NewTransaction_RecoverySet-textEl')]");
		reopenClaim = By.xpath("//span[contains(@id,'ClaimMenuActions_ClaimActions:ClaimMenuActions_ReopenClaim-textEl')]");
		closeClaim = By.xpath("//span[contains(@id,'ClaimMenuActions_ClaimActions:ClaimMenuActions_CloseClaim-textEl')]");
	}
}
