package com.pure.claim;

import org.openqa.selenium.By;

public class AddClaimInfoPage {
	static By description,
	pageTitle,location,address1,
	address2, city,country,state,zip,
	incidentOnly,incidentOnlyYes, incidentOnlyNo,
	lossCategory,lossCause,anySeriousInjuriesRadioYes,
	anySeriousInjuriesRadioNo,dateOfNotice,zipIcon,coverageQuestionYes,
	coverageQuestionNo,arsonInvolvedYes,arsonInvolvedNo,addressLine1,addressLine2,
	catastrophe,faultRating, specialInterestYes,specialInterestNo,paInvolvedYes,paInvolvedNo,
	horizontleSplitter, addPropertyIncident, title, addVesselIncident, addInjuriesIncident,
	addVehicleIncident;
	
	
	

	static{
		pageTitle = By.xpath("//span[contains(text(),'Add claim information')]");
		description = By.cssSelector("[id*=\"Description-inputEl\"]");
		location = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:NewClaimWizard_LossDetailsScreen:NewClaimLossDetailsDV:LossDetailsAddressDetailInputSet:LossLocation_Name-inputEl");
		addressLine1 = By.xpath("//label[text()='Address 1']/../..//input");
		addressLine2 = By.xpath("//label[text()='Address 2']/../..//input");
		city = By.cssSelector("[id*=\"City-inputEl\"]");
		country = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:NewClaimWizard_LossDetailsScreen:NewClaimLossDetailsDV:LossDetailsAddressDetailInputSet:LossLocation_County-inputEl");
		state = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:NewClaimWizard_LossDetailsScreen:NewClaimLossDetailsDV:LossDetailsAddressDetailInputSet:LossLocation_State-inputEl");
		zip = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//input");
		lossCategory = By.xpath("//label[text()='Loss Category']/../..//input");
		lossCause = By.xpath("//label[text()='Loss Cause']/../..//input");
		anySeriousInjuriesRadioYes = By.cssSelector("[id*=\"anySeriousinjuries_true-inputEl\"]");
		anySeriousInjuriesRadioNo = By.cssSelector("[id*=\"anySeriousinjuries_false-inputEl\"]");
		dateOfNotice = By.cssSelector("[id*=\"Notification_ReportedDate-inputEl\"]");
		zipIcon = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//a");
		coverageQuestionNo = By.cssSelector("[id*=\"Status_CoverageQuestion_false-inputEl\"]");
		coverageQuestionYes = By.cssSelector("[id*=\"Status_CoverageQuestion_true-inputEl\"]");
		incidentOnlyYes = By.cssSelector("[id*=\"Status_IncidentReport_true-inputEl\"]");
		incidentOnlyNo = By.cssSelector("[id*=\"Status_IncidentReport_false-inputEl\"]");
		arsonInvolvedYes = By.cssSelector("[id*=\"ArsonInvolved_true-inputEl\"]");
		arsonInvolvedNo = By.cssSelector("[id*=\"ArsonInvolved_false-inputEl\"]");
		catastrophe = By.cssSelector("[id*=\"Catastrophe_CatastropheNumber-inputEl\"]");
		faultRating = By.cssSelector("[id*=\"Notification_Fault-inputEl\"]");
		specialInterestYes = By.cssSelector("[id*=\"specialInterestRadioInput_true-inputEl\"]");
		specialInterestNo = By.cssSelector("[id*=\"specialInterestRadioInput_false-inputEl\"]");
		paInvolvedYes = By.cssSelector("[id*=\"PAInvolvedRadio_true-inputEl\"]");
		paInvolvedNo = By.cssSelector("[id*=\"PAInvolvedRadio_false-inputEl\"]");
		addPropertyIncident = By.xpath("//a[@id='FNOLWizard:AutoWorkersCompWizardStepSet:NewClaimWizard_LossDetailsScreen:NewClaimLossDetailsDV:EditableFixedPropertyIncidentsLV_tb:Add']");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		addVesselIncident = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:NewClaimWizard_LossDetailsScreen:NewClaimLossDetailsDV:PURE_EditableWatercraftIncidentsLV_tb:Add-btnInnerEl");
	
		//Duplicate claims
		horizontleSplitter = By.id("southPanel-splitter-collapseEl");
		
		addInjuriesIncident = By.cssSelector("[id*=\"EditableInjuryIncidentsLV_tb:Add-btnInnerEl\"]");
		
		addVehicleIncident = By.cssSelector("[id*=\"EditableVehicleIncidentsLV_tb:Add-btnInnerEl\"]");
	
	}
	
	
}
