package com.pure.claim;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ReviewAndSaveClaimPageLib extends ActionEngine{

	public ReviewAndSaveClaimPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void clickFinish() throws Throwable{
		click(ReviewAndSaveClaimPage.finishButton, "Finish Button");
		waitForMask();
		printPageTitle(NewClaimPage.title,"Page title");
		boolean flag = false;
		try{
			flag = driver.findElement(ReviewAndSaveClaimPage.validationResultsTab).isDisplayed();
		}catch(Exception ex){
			//Do nothing, as element is not expected to be present always
		}
		if(flag){
			click(ReviewAndSaveClaimPage.clearButton, "Clear validation errors in pop up");
			click(ReviewAndSaveClaimPage.finishButton, "Finish Button after clearing validation errors");
			waitForMask();
		}
	}
}
