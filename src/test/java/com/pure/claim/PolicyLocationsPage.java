package com.pure.claim;

import org.openqa.selenium.By;

public class PolicyLocationsPage {
	
	static By locationAopDeductible, locationSpecialDeductible, coveragesTab, 
	coveragesAopDeductibleList, coveragesSpecialDeductiblesList, coveragesDeductiblesList;
	
	static{
		locationAopDeductible = By.xpath("//div[contains(@id,'PolicyLocationLDV:PolicyLocationsLV-body')]//tr/td[5]/div");
		locationSpecialDeductible = By.xpath("//div[contains(@id,'PolicyLocationLDV:PolicyLocationsLV-body')]//tr/td[6]/div");
		coveragesTab = By.xpath("//span[contains(@id,'PolicyLocationLDV:PolicyLocationRiskDetailPanelSet:LocationCoveragesTab-btnInnerEl')]");
		coveragesAopDeductibleList = By.xpath("//div[contains(@id,'LocationCoverageListDetail:EditablePropertyCoveragesLV-body')]//td[3]");
		coveragesSpecialDeductiblesList = By.xpath("//div[contains(@id,'LocationCoverageListDetail:EditablePropertyCoveragesLV-body')]//td[4]");
		coveragesDeductiblesList = By.xpath("//div[contains(@id,'LocationCoverageListDetail:EditablePropertyCoveragesLV-body')]//td[5]");
	}

}
