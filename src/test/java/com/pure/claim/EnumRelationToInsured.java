package com.pure.claim;

public enum EnumRelationToInsured {
	self,
	relative,
	Friend,
	Agent,
	Attorney,
	Employee,
	Spouse,
	Child,
	Parent,
	GrandParent,
	DomesticPartner,
	Other,
	ClaimantsAttorney,
	ClaimantsInsuranceCo,
	RentalRepresentative,
	RiskManager,
	UnderWriter,
	SalesAndMarketing,
	MemberService
	
	
}
