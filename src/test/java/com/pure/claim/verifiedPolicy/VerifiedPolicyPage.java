/*Author::Satish*/
package com.pure.claim.verifiedPolicy;

import org.openqa.selenium.By;

public class VerifiedPolicyPage {

	public static By policyNumber;
	public static By firstName, lastName;
	public static By orgName,policyType,lossDate,ssn_TaxID;
	public static By city,state,zipCode,country,vin;
	public static By searchBtn;
	public static By reset;
	public static By verifyPolicyNumber;
	public static By verifyName;
	public static By verifyPolicyType;
	public static By resultTable;
	public static By buttonNext;
	public static By buttonCancel;
	public static By UnselectBtn,SelectBtn;
	public static By errorMessageContainer;
	public static By errorMessages;
	public static By okPopUp;
	//--------------------claim type - auto-----------------------
	public static By autoFirstAndFinalClaimType;
	public static By autoQuickClaimAutoClaimType;
	
	static {
		policyNumber = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:policyNumber-inputEl");
		firstName = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:Firstname-inputEl");
		lastName = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:Lastname-inputEl");
		orgName = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:org-inputEl");
		policyType = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:PolicyType-inputEl");
		//lossDate = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:date-inputEl");
		lossDate = By.xpath("//input[contains(@id,'date-inputEl')]");
		//input[contains(@id,'date-inputEl')]
		ssn_TaxID = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:FNOLWizard_PolicySearchInputSet:ssn-inputEl");
		city = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:FNOLWizard_PolicySearchInputSet:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:City-inputEl");
		state = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:FNOLWizard_PolicySearchInputSet:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:State-inputEl");
		zipCode = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:FNOLWizard_PolicySearchInputSet:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:PostalCode-inputEl");
		country = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:FNOLWizard_PolicySearchInputSet:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:Country-inputEl");
		vin = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:FNOLWizard_PolicySearchInputSet:vin-inputEl");
		searchBtn = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:Search");
		reset = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:Reset");
		verifyPolicyNumber = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:PolicyResultLV:0:PolicyNumber");
		verifyName = By.id("ext-gen2752");
		resultTable = By.cssSelector("table[id^=\"gridview\"]");
		buttonCancel= By.xpath("//span[contains(@id,'Cancel')]");
		buttonNext= By.xpath("//span[contains(@id,'Next')]");
		UnselectBtn = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:PolicyResultLV:0:unselectButton");
		SelectBtn = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:PolicyResultLV:0:selectButton");
		errorMessageContainer = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:_msgs");
		errorMessages = By.xpath("//div[@id='FNOLWizard:FNOLWizard_FindPolicyScreen:_msgs']//div[@class='message']");
		//--------------------claim type - auto-----------------------
		autoFirstAndFinalClaimType = By.xpath("//label[text()='Auto - Auto First and Final']/../input");
		autoQuickClaimAutoClaimType = By.xpath("//label[text()='Auto - Quick Claim Auto']/../input");
		okPopUp = By.xpath("//div[@id='messagebox-1001-toolbar-targetEl']//span[text()='OK']");
	}
	
}
