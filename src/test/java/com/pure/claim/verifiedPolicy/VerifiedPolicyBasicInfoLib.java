package com.pure.claim.verifiedPolicy;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class VerifiedPolicyBasicInfoLib extends ActionEngine {

	public VerifiedPolicyBasicInfoLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void setHowReported(String howReported) throws Throwable{
		type(VerifiedPolicyBasicInfoPage.howReported, howReported, "How Reported");
		type(VerifiedPolicyBasicInfoPage.howReported, Keys.TAB, "TAB key");
	}
	public void setReportedBy(String reporterName) throws Throwable{
		type(VerifiedPolicyBasicInfoPage.reporterName, reporterName, "Reporter Name");
		type(VerifiedPolicyBasicInfoPage.reporterName, Keys.TAB, "TAB key");
	}
	public String getReporterName() throws Throwable {
		return getText(VerifiedPolicyBasicInfoPage.reporterName, "ReporterName");
	}

	public void setReporterName(String reporterName) throws Throwable {
		type(VerifiedPolicyBasicInfoPage.reporterName, reporterName,
				"RelationToInsured Dropdown");
	}

	public String getRelationToInsured() throws Throwable {
		return getText(VerifiedPolicyBasicInfoPage.relationToInsured,
				"relationToInsured");
	}

	public void setRelationToInsured(String relationToInsured) throws Throwable{
		//Thread.sleep(500);//Stale element --> Remove this
		type(VerifiedPolicyBasicInfoPage.relationToInsured, relationToInsured.toString(), "Relation To Insured");
		type(VerifiedPolicyBasicInfoPage.relationToInsured, Keys.TAB, "TAB key");

		//Below section is to check the valid phone num, if not valid, enter right formated phone num.
		setPhoneNum();
	}

	private void setPhoneNum() throws Throwable{
		/*
		 * When the wrong format phone number is displayed, application makes it as editable and displays the error on the page.
		 * This function checks if the phone number displayed has the correct format 3digit-3digit-4digit.
		 * if not, types newFormatPhoneNum as new phone number.
		 * 
		 */
		String newFormatPhoneNum = "210-213-2121";
		boolean flag = isElementPresent(VerifiedPolicyBasicInfoPage.homePhone, "homePhone");
		if(flag){
			String homePhone = getAttributeByValue(	VerifiedPolicyBasicInfoPage.homePhone, "homePhone");
			if (homePhone.contains("-")){
				String[] phonenumber = homePhone.split("-");
				String first = phonenumber[0];
				String middle = phonenumber[1];
				String last = phonenumber[2];
				if ( first.length()	==3 &&
						middle.length()	==3 &&
						last.length()	==4){
					//don't do anything as format is correct [ie. 3digit-3digit-4digit]
				}else{
					clearData(VerifiedPolicyBasicInfoPage.homePhone);
					type(VerifiedPolicyBasicInfoPage.homePhone, newFormatPhoneNum,"HomePhone");
					type(VerifiedPolicyBasicInfoPage.homePhone, Keys.TAB,"HomePhone");
				}
			}else{
				clearData(VerifiedPolicyBasicInfoPage.homePhone);
				type(VerifiedPolicyBasicInfoPage.homePhone, newFormatPhoneNum,"HomePhone");
				type(VerifiedPolicyBasicInfoPage.homePhone, Keys.TAB,"HomePhone");
			}
		}
	}

	public String getLastName() throws Throwable {
		return getText(VerifiedPolicyBasicInfoPage.lastName, "LastName");
	}

	public void setLastName(String lastName) throws Throwable {
		type(VerifiedPolicyBasicInfoPage.lastName, lastName, "LastName");
	}

	public void clickOkBtn(String okBtn) throws Throwable {
		click(VerifiedPolicyBasicInfoPage.okBtn, "Ok Button");
	}
	public void clickToAddNewPerson(String iconReportedBy, String lastName) throws Throwable {
		click(VerifiedPolicyBasicInfoPage.iconReportedBy, "ReportedBy Icon");
		selectByValue(VerifiedPolicyBasicInfoPage.newPersonFromReporterNameList, "New Person", "newPersonFromReporterNameList");
		setLastName(lastName);
	}

	public void selectInvolvedVehicle(String vehicalName) throws Throwable{
		By vehicleCheckBox = By.xpath("//div[contains(text(),'"+vehicalName+"')]/..//input"); 
		if(isElementPresent(vehicleCheckBox, "vehicalName")){
			click(vehicleCheckBox,vehicalName );
		}
	}

	public void setContactInfo(String homePhone) throws Throwable{		
		type(VerifiedPolicyBasicInfoPage.homePhone, homePhone.toString(), "HomePhone");
	}

}
