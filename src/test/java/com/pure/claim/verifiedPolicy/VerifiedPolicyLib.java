package com.pure.claim.verifiedPolicy;
import org.json.simple.JSONObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;


@SuppressWarnings( "unchecked" )
public class VerifiedPolicyLib extends ActionEngine {
	private JSONObject jsonObj = new JSONObject();
	
	public VerifiedPolicyLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public String getPolicyNumber() throws Throwable {
		return getText(VerifiedPolicyPage.policyNumber, "Policy Number");
	}

	public void setPolicyNumber(String policyNumber) throws Throwable {
		type(VerifiedPolicyPage.policyNumber, policyNumber,
				"Policy Number text box");
		jsonObj.put("policyNumber", policyNumber);
	}

	public String getFirstName() throws Throwable {
		return getText(VerifiedPolicyPage.firstName, "First Name");
	}

	public void setFirstName(String firstName) throws Throwable {
		type(VerifiedPolicyPage.firstName, firstName, "First Name text box");
		jsonObj.put("firstName", firstName);
	}

	public String getLastName() throws Throwable {
		return getText(VerifiedPolicyPage.lastName, "Last Name");
	}

	public void setLastName(String lastName) throws Throwable {
		type(VerifiedPolicyPage.lastName, lastName, "Last Name text box");
		jsonObj.put("lastName", lastName);
	}

	public String getOrgName() throws Throwable {
		return getText(VerifiedPolicyPage.orgName, "Organization Name");
	}

	public void setOrgName(String orgName) throws Throwable {
		type(VerifiedPolicyPage.orgName, orgName, "Organization Name text box");
		jsonObj.put("orgName", orgName);
	}

	public String getLossDate() throws Throwable {
		String text = "";
		try {
			text = driver.findElement(VerifiedPolicyPage.lossDate).getAttribute("value");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return text;
	}

	public void setLossDate(String lossDate) throws Throwable {		
		try {
			new Actions(driver).sendKeys(driver.findElement(VerifiedPolicyPage.lossDate),lossDate).sendKeys(Keys.TAB).perform();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		jsonObj.put("lossDate", lossDate);		
		if(!getLossDate().equals(lossDate)){
			type(VerifiedPolicyPage.lossDate, lossDate, "Loss Date text box");
			type(VerifiedPolicyPage.lossDate, Keys.TAB,	"Tab Click");
		}else{
			reporter.SuccessReport("Enter text in :: " + VerifiedPolicyPage.lossDate, "Successfully Entered value :"+lossDate);
		}
	}

	public String getSSN_TaxID() throws Throwable {
		return getText(VerifiedPolicyPage.ssn_TaxID, "SSN_TaxID Name");
	}

	public void setSSN_TaxID(String ssn_TaxID) throws Throwable {
		type(VerifiedPolicyPage.ssn_TaxID, ssn_TaxID, "SSN_TaxID Name text box");
		jsonObj.put("ssn_TaxID", ssn_TaxID);
	}

	public String getCity() throws Throwable {
		return getText(VerifiedPolicyPage.city, "City Name");
	}

	public void setCity(String city) throws Throwable {
		type(VerifiedPolicyPage.city, city, "City Name text box");
		jsonObj.put("city", city);
	}

	public String getZIPCode() throws Throwable {
		return getText(VerifiedPolicyPage.zipCode, "ZIP Code");
	}

	public void setZIPCode(String zipCode) throws Throwable {
		type(VerifiedPolicyPage.zipCode, zipCode, "ZIP Code text box");
		jsonObj.put("zipCode", zipCode);
	}

	public String getVin() throws Throwable {
		return getText(VerifiedPolicyPage.vin, "VIN");
	}

	public void setVin(String vin) throws Throwable {
		type(VerifiedPolicyPage.zipCode, vin, "VIN text box");
		jsonObj.put("vin", vin);
	}

	public void selectPolicyType(String policyType) throws Throwable {
		type(VerifiedPolicyPage.policyType, policyType, "Policy Type Dropdown");
		jsonObj.put("policyType", policyType);
	}

	public String getSelectedPolicyTypeValue() throws Throwable {
		return getText(VerifiedPolicyPage.policyType, "PolicyType Dropdown");
	}

	public void selectState(String state) throws Throwable {
		type(VerifiedPolicyPage.state, state, "State Dropdown");
		jsonObj.put("state", state);
	}

	public String getSelectedStateValue() throws Throwable {
		return getText(VerifiedPolicyPage.state, "State Dropdown");
	}

	public void selectCountry(String country) throws Throwable {
		type(VerifiedPolicyPage.country, country, "Country Dropdown");
		jsonObj.put("country", country);
	}

	public String getSelectedCountryValue() throws Throwable {
		return getText(VerifiedPolicyPage.country, "Country Dropdown");
	}

	public void clickSearchButton() throws Throwable {
		click(VerifiedPolicyPage.searchBtn, "Search Button");
	}

	public void clickResetButton() throws Throwable {
		click(VerifiedPolicyPage.reset, "Reset Button");
	}

	public void clickCancelButton() throws Throwable {
		click(VerifiedPolicyPage.buttonCancel, "Cancel Button");
	}

	public void clickNextButton() throws Throwable {
		click(VerifiedPolicyPage.buttonNext, "Next Button");
	}

	// Search/Fetch the verified policy information fetched from dragon
	public void searchAndSelectVerifiedPolicy(String policyNumber, String lossDate)
			throws Throwable {
		setPolicyNumber(policyNumber);
		setLossDate(lossDate);
		clickSearchButton();
		//if(ErrorHandling()){
			selectSearchedPolicy();
		//}
	}
	
	public void setTypeOfClaim(String claimType) throws Throwable{
		switch(claimType.trim()){
			case "Auto - Auto First and Final":
				click(VerifiedPolicyPage.autoFirstAndFinalClaimType, "Claim type - Auto First and Final");
				break;
			case "Auto - Quick Claim Auto":
				click(VerifiedPolicyPage.autoQuickClaimAutoClaimType, "Claim type - Auto Quick Claim");
				break;
		}
	}

	// verify the verified policy information fetched from dragon

	public boolean verifyReset(String policyNumber, String firstName,
			String lastName, String policyType, String lossDate, String reset)
			throws Throwable {
		boolean status = false;
		setFirstName(firstName);
		setLastName(lastName);
		selectPolicyType(policyType);
		setLossDate(lossDate);
		clickResetButton();
		if (getFirstName() == null & getLastName() == null
				& getPolicyNumber() == null
				& getSelectedPolicyTypeValue() == "<none>") {
			status = true;
		}
		return status;
	}

	
	public boolean ErrorHandling() throws Throwable{
		//String errorMessage1 = "Loss Date is mandatory";
		//String errorMessage2 = "First Name and Last Name OR Company Name is Mandatory OR Please enter a Policy Number and also select the appropriate Policy Type";
		//String errorMessage3 = "The search returned zero results.";
		if(isElementPresent(VerifiedPolicyPage.errorMessages, "errorMessages")){			
			assertFalse(driver.findElements(VerifiedPolicyPage.errorMessages).size()>0,					
				driver.findElement(VerifiedPolicyPage.errorMessages).getText()+" error thrown");
			return false;
		}
		return true;
	}

	public void selectSearchedPolicy() throws Throwable{
		if(!isElementPresent(VerifiedPolicyPage.UnselectBtn,"UnSelect")){
			click(VerifiedPolicyPage.SelectBtn, "Select");
		}
	}
	
	public void unSelectSearchedPolicyAndReselect() throws Throwable{
		click(VerifiedPolicyPage.UnselectBtn, "UnSelect");
		waitForInVisibilityOfElement(VerifiedPolicyPage.UnselectBtn, "UnSelect");
		selectSearchedPolicy();
	}
	
	public void selectTypeOfClaim(String typeOfClaim) throws Throwable{
		switch (typeOfClaim.toUpperCase()){
		case "FIRSTANDFINALAUTO":
			click(VerifiedPolicyPage.autoFirstAndFinalClaimType,"Auto - Auto First and Final Claim type");
			break;
		case "QUICKCLAIMAUTO":
			click(VerifiedPolicyPage.autoQuickClaimAutoClaimType,"Auto - Quick Claim Auto Claim type");
			break;
		}
	}
	public void clickCancelWithPopUpHandle() throws Throwable {
		click(VerifiedPolicyPage.buttonCancel, "Cancel Button");
		String Window = driver.getWindowHandle();
		switchToWindow(Window);
		click(VerifiedPolicyPage.okPopUp,"OK POPUP");
		driver.switchTo().window(Window);
	}
	
}
