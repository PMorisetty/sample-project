package com.pure.claim.verifiedPolicy;

import org.openqa.selenium.By;

public class VerifiedPolicyBasicInfoPage {
	static By howReported, reporterName;
	static By relationToInsured, newPersonFromReporterNameList;
	static By iconReportedBy,lastName,okBtn;
	static By involvedVehicles;
	static By homePhone;
	
	static{
		howReported = By.xpath("//input[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_BasicInfoScreen:PanelRow:BasicInfoDetailViewPanelDV:HowReported-inputEl']");
//		reporterName = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_BasicInfoScreen:PanelRow:BasicInfoDetailViewPanelDV:ReportedBy_Name-inputEl");
		reporterName = By.cssSelector("[id*=\"ReportedBy_Name-inputEl\"]");
		relationToInsured = By.cssSelector("[id*=\"Claim_ReportedByType-inputEl\"]");
		iconReportedBy = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_BasicInfoScreen:PanelRow:BasicInfoDetailViewPanelDV:ReportedBy_Name:ReportedBy_NameMenuIcon");
		newPersonFromReporterNameList = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_BasicInfoScreen:PanelRow:BasicInfoDetailViewPanelDV:ReportedBy_Name:ClaimNewPersonOnlyPickerMenuItemSet:ClaimNewPersonOnlyPickerMenuItemSet_NewPersonMenuItem-textEl");
		lastName = By.id("NewContactPopup:ContactDetailScreen:ContactBasicsDV:PersonNameInputSet:GlobalPersonNameInputSet:LastName-inputEl");
		okBtn = By.id("NewContactPopup:ContactDetailScreen:ContactBasicsDV_tb:ContactDetailToolbarButtonSet:Update-btnInnerEl");
		involvedVehicles = By.id("//div[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_BasicInfoScreen:PanelRow:RightPanel:FNOLWizard_BasicInfoRightPanelSet:0:InsuredVehicleDV:InsuredVehicleInputGroup-legend-innerCt']");
		homePhone = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_BasicInfoScreen:PanelRow:BasicInfoDetailViewPanelDV:reporter_homephone:GlobalPhoneInputSet:NationalSubscriberNumber-inputEl");
	}
}