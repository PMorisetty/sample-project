package com.pure.claim;

import org.openqa.selenium.By;

public class SearchAddressBookPage {

	static By type, name, serviceState, searchBtn, createNewServiceProviderBtn, selectBtn, title, searchResultsDiv,nameSearchResult,serviceProviderName;
	
	static {
		type = By.xpath("//label[text()='Type']/../following-sibling::td//input");
		name = By.xpath("//label[text()='Name']/../following-sibling::td//input");
		serviceState = By.xpath("//label[text()='Service State']/../following-sibling::td//input");
		searchBtn = By.xpath("//a[contains(text(), 'eset')]/../preceding-sibling::td/a");
		createNewServiceProviderBtn = By.id("AddressBookPickerPopup:AddressBookSearchScreen:AddressBookSearchLV_tb:addNewContactToolbarButton-btnInnerEl");
		selectBtn = By.id("AddressBookPickerPopup:AddressBookSearchScreen:AddressBookSearchLV:0:_Select");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		searchResultsDiv = By.xpath("//div[@id='AddressBookSearch:AddressBookSearchScreen:AddressBookSearchLV-body']//tr");
	    nameSearchResult = By.id("AddressBookSearch:AddressBookSearchScreen:AddressBookSearchLV:0:DisplayName");
	    serviceProviderName = By.xpath("//label[text()='Name']/../..//div");
	}
}
