package com.pure.claim;

import org.openqa.selenium.By;

public class NewVehiclePage {

	static By vehicleMake, vehicleModel, vehicleYear,
	okBtn, title;
	
	static {
		vehicleMake = By.xpath("//input[@id='NewClaimWizard_NewPolicyVehiclePopup:NewClaimWizard_NewPolicyVehicleScreen:PolicyVehicleDetailPanelSet:PolicyVehicleDetailDV:Make-inputEl']");
		vehicleModel = By.xpath("//input[@id='NewClaimWizard_NewPolicyVehiclePopup:NewClaimWizard_NewPolicyVehicleScreen:PolicyVehicleDetailPanelSet:PolicyVehicleDetailDV:Model-inputEl']");
		vehicleYear = By.xpath("//input[@id='NewClaimWizard_NewPolicyVehiclePopup:NewClaimWizard_NewPolicyVehicleScreen:PolicyVehicleDetailPanelSet:PolicyVehicleDetailDV:Year-inputEl']");
		okBtn = By.id("NewClaimWizard_NewPolicyVehiclePopup:NewClaimWizard_NewPolicyVehicleScreen:Update-btnInnerEl");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
	}
}
