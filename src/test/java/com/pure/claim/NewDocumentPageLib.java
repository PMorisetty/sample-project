package com.pure.claim;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewDocumentPageLib extends ActionEngine{
	
	public NewDocumentPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void selectAttachment(String attachment) throws Throwable{
		type(NewDocumentPage.attchment, attachment, "Attachment");
		type(NewDocumentPage.attchment, Keys.TAB, "TAB Key");
	}
	
	public void setShareOnMemberPortal(String shareonmemberportal) throws Throwable{
		type(NewDocumentPage.shareOnMemberPortal, shareonmemberportal, "Share On Member Portal");
		type(NewDocumentPage.shareOnMemberPortal, Keys.TAB, "TAB Key");
	}
	
	public void setDescription(String description) throws Throwable{
		type(NewDocumentPage.description, description, "Description");
		type(NewDocumentPage.description, Keys.TAB, "TAB Key");
	}
	
	public void setType(String type) throws Throwable{
		type(NewDocumentPage.type, type, "Type");
		type(NewDocumentPage.type, Keys.TAB, "TAB Key");
	}
	
	/*public String getNameText() throws Throwable{
		waitForVisibilityOfElement(NewDocumentPage.name, "Name");
		return getText(NewDocumentPage.name, "Name");
	}
	*/
	public void clickUpdate() throws Throwable{
		waitForVisibilityOfElement(NewDocumentPage.updateBtn, "Update Button");
		click(NewDocumentPage.updateBtn, "Update Button");
	}
	

}
