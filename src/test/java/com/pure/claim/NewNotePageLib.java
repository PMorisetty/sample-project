package com.pure.claim;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewNotePageLib extends ActionEngine{

	public NewNotePageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void setShareOnMemberPortal(String shareOnPortal) throws Throwable{
		type(NewNotePage.shareOnMemberPortal, shareOnPortal, "Share On Member Portal");
		type(NewNotePage.shareOnMemberPortal, Keys.TAB, "TAB Key");
	}

	public void setTopic(String topic) throws Throwable{
		type(NewNotePage.topic, topic, "Topic");
		type(NewNotePage.topic, Keys.TAB, "TAB Key");
	}

	public void setConfidential(String confidential) throws Throwable{
		if(confidential.equalsIgnoreCase("yes")){
			click(NewNotePage.confidentialYes,"Confidential Yes");
		}else{
			click(NewNotePage.confidentialNo,"Confidential No");
		}
	}

	public void setText(String text) throws Throwable{
		type(NewNotePage.text, text, "Text");
		type(NewNotePage.text, Keys.TAB, "TAB Key");
	}
	
	public void setSubject(String subject) throws Throwable{
		type(NewNotePage.subject, subject, "Subject");
		type(NewNotePage.text, Keys.TAB, "TAB Key");
	}

	public String getUserTemplateText() throws Throwable{
		return getText(NewNotePage.text, "UseTemplate Text");
	}

	public void clickUpdate(String text) throws Throwable{
		scroll(NewNotePage.update,"Update button");
		click(NewNotePage.update,"Update button");
		waitForMask();
		waitForInVisibilityOfElement(NewNotePage.newNoteTitle,"New Note Title");
		reporter.SuccessReport("Successfully added New Note", text);
	}

	public void clickCancel() throws Throwable{
		scroll(NewNotePage.cancel,"Cancel button");
		click(NewNotePage.cancel,"Cancel button");
		waitForInVisibilityOfElement(NewNotePage.newNoteTitle,"New Note Title");
	}

	public void clickUseTemplate() throws Throwable{
		scroll(NewNotePage.useTemplate,"UseTemplate button");
		click(NewNotePage.useTemplate,"UseTemplate button");
	}

	public void clicUseTemplateSearch() throws Throwable{
		scroll(NewNotePage.search,"UseTemplate Search button");
		click(NewNotePage.search,"UseTemplate Search button");
	}

	public void selectTemplate() throws Throwable{
		scroll(NewNotePage.select,"UseTemplate select button");
		click(NewNotePage.select,"UseTemplate select button");
	}
	//-----------LINK DOCUMENT SECTION Starts---------------
	private void clickLinkDocument() throws Throwable{
		scroll(NewNotePage.linkDocument,"LinkDocument button");
		click(NewNotePage.linkDocument,"LinkDocument button");
		waitForMask();
	}
	
	private void selectDocument() throws Throwable
	{
		boolean clicked = false;
		int attempts = 0;
		while(!clicked && attempts < 30) {
			try {
				WebDriverWait wait = new WebDriverWait(driver, 8);
				wait.until(ExpectedConditions.elementToBeClickable(NewNotePage.selectDocument));
				scroll(NewNotePage.selectDocument,"Document checkbox");
				click(NewNotePage.selectDocument,"Document checkbox");
				scroll(NewNotePage.selectBtn,"Select button");
				click(NewNotePage.selectBtn,"Select button");
				clicked = true;
			} catch (Exception e) {
				click(NewExposurePage.refreshBtn, "Refresh Button");
			}
			attempts++;
		}
	}

	public void attachLinkDocument() throws Throwable {
		clickLinkDocument();
		selectDocument();
	}
	//-----------LINK DOCUMENT SECTION ends---------------
}
