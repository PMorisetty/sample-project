package com.pure.claim;

import org.openqa.selenium.By;

public class PersonalPropertyPage {

static By damageOrLossDescription, okBtn;
	
	static{
		damageOrLossDescription = By.xpath("//textarea[contains(@id,'Description-inputEl')]");
		okBtn = By.cssSelector("[id*=\"Update-btnInnerEl\"]");
		
	}
}
