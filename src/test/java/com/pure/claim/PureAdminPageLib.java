package com.pure.claim;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PureAdminPageLib extends ActionEngine{
	public PureAdminPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		
		this.driver = webDriver;
		this.reporter = reporter;

	}
	
	public void openmessageQueuesPage() throws Throwable{
		click(PureAdminPage.monitoringLink, "Monitoring Link");
		click(PureAdminPage.messageQueuesLink, "Message Queues Link");
	}
}
