package com.pure.claim;

import org.openqa.selenium.By;

public class NewDwellingIncidentPage {
	
	static By damageDescription, propertyDescription, okBtn;
	
	static{
		damageDescription = By.xpath("//textarea[contains(@id,'DamageDV:Description')]");
		propertyDescription = By.xpath("//input[contains(@id,'DamageDV:PropertyDescription')]");
		okBtn = By.cssSelector("[id*=\"Update-btnInnerEl\"]");
		
	}

}
