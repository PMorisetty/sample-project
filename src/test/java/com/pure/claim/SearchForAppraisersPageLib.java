package com.pure.claim;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class SearchForAppraisersPageLib extends ActionEngine{
	public SearchForAppraisersPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable{
		this.driver = driver;
		this.reporter = reporter;
		waitForMask();
		printPageTitle(SearchForAppraisersPage.title,"Page title");
	}
	
	private void clickAssignToIA() throws Throwable{
		click(SearchForAppraisersPage.assignToIA, "assignToIA");
	}
	
	private void clickAssignToPureExpress() throws Throwable{
		click(SearchForAppraisersPage.assignToPureExpress, "assignToPureExpress");
	}
	
	private void clickAssignToDRP() throws Throwable{
		click(SearchForAppraisersPage.assignToDRP, "assignToDRP");
	}
	
	private void clickShopOfChoice() throws Throwable{
		click(SearchForAppraisersPage.shopOfChoice, "shopOfChoice");
	}
	
	private void clickComSearch() throws Throwable{
		click(SearchForAppraisersPage.comSearch, "comSearch");
	}
	
	private void clickNoEIHAssignToIA() throws Throwable{
		JSClick(SearchForAppraisersPage.noEIHAssignToIA, "noEIHAssignToIA");
	}
	
	public void clickMethodOfInspection(String methodOfInspection) throws Throwable{
		switch(methodOfInspection){
		case "assignToIA":
			clickAssignToIA();
			break;
		case "assignToPureExpress":
			clickAssignToPureExpress();
			break;
		case "assignToDRP":
			clickAssignToDRP();
			break;
		case "shopOfChoice":
			clickShopOfChoice();
			break;
		case "comSearch":
			clickComSearch();
			break;
		case "noEIHAssignToIA":
			clickNoEIHAssignToIA();
			break;
		}
		waitForMask();
	}
	
	public void selectAppraiserName(String appraiserName) throws Throwable{
		String appraiserNameXpath = "//div[text()='"+appraiserName+"']/../..//a";
		By appraiserNameBy = By.xpath(appraiserNameXpath);
		click(appraiserNameBy, "appraiserName");
		waitForMask();
	}
	
	public void verifyAppraiserNameIsDisplayed(String methodOfInspection, String appraiserName) throws Throwable{
		clickMethodOfInspection(methodOfInspection);
		String appraiserNameXpath = "//div[text()='"+appraiserName+"']/../..//a";
		By appraiserNameBy = By.xpath(appraiserNameXpath);
		verifyElementPresent(appraiserNameBy, "Appraiser Name", true);
	}
	
	public void verifyTotalLossResult(String expectedLossResult) throws Throwable{
		assertText(SearchForAppraisersPage.totalLossResult , expectedLossResult);
	}
	
	public void verifyMethodOfInspectionIsDisplayed(String methodOfInspection) throws Throwable{
		By moiBy = null;
		String methodOfInsp = null;
		switch(methodOfInspection){
		case "assignToIA":
			moiBy = SearchForAppraisersPage.assignToIA;
			methodOfInsp = "Assign To IA";
			break;
		case "assignToPureExpress":
			moiBy = SearchForAppraisersPage.assignToPureExpress;
			methodOfInsp = "Assign to PURE Express";
			break;
		case "assignToDRP":
			moiBy = SearchForAppraisersPage.assignToDRP;
			methodOfInsp = "Assign To DRP";
			break;
		case "shopOfChoice":
			moiBy = SearchForAppraisersPage.shopOfChoice;
			methodOfInsp = "Select Shop of Choice(if available)";
			break;
		case "comSearch":
			moiBy = SearchForAppraisersPage.comSearch;
			methodOfInsp = "If Shop of Choice not present, and EIH <$3,500 assign to Comsearch";
			break;
		case "noEIHAssignToIA":
			moiBy = SearchForAppraisersPage.noEIHAssignToIA;
			methodOfInsp = "If Shop of Choice not present, there is no EIH, or EIH is >$3,500; Assign to IA";
			break;
		case "assignSalvageToCoPart":
			moiBy = SearchForAppraisersPage.assignSalvageToCoPart;
			methodOfInsp = "Assign Salvage to CoPart, Assign Appraisal to ComSearch";
			break;
		}
		verifyElementPresent(moiBy, methodOfInsp, true);
	}
	
	public void setZipcode(String number) throws Throwable{
		type(SearchForAppraisersPage.zipcode,number,"zipcode");
		type(SearchForAppraisersPage.zipcode,Keys.TAB,"Key tab");
	}
	
}
