package com.pure.claim;

import org.openqa.selenium.By;

public class MessageQueuesPage {
	static By claimNotificationStatus,
	documentStoreStatus,
	paymentNotificationStatus,
	assignmentTransportStatus,
	contactMessageTransportStatus,
	emailStatus,
	contactAutoSyncFailureStatus,
	voidCheckEmailNotificationStatus,
	isoStatus,
	metroStatus,	
	xactAssignmentTransportStatus,
	asyncDocumentUploadStatus;
	static{
		claimNotificationStatus = By.xpath("//a[text()='Claim Notification']/../../..//td[4]/div");
		documentStoreStatus= By.xpath("//a[text()='Document Store']/../../..//td[4]/div");
		paymentNotificationStatus= By.xpath("//a[text()='Payment Notification']/../../..//td[4]/div");
		assignmentTransportStatus= By.xpath("//a[text()='CCC Assignment Transport']/../../..//td[4]/div");
		contactMessageTransportStatus= By.xpath("//a[text()='Contact Message Transport']/../../..//td[4]/div");
		emailStatus= By.xpath("//a[text()='Email']/../../..//td[4]/div");
		contactAutoSyncFailureStatus= By.xpath("//a[text()='Contact Auto Sync Failure']/../../..//td[4]/div");
		voidCheckEmailNotificationStatus= By.xpath("//a[text()='Void Check Email Notification']/../../..//td[4]/div");
		isoStatus= By.xpath("//a[text()='ISO']/../../../..//td[4]/div");
		metroStatus= By.xpath("//a[text()='Metro']/../../..//td[4]/div");																
		xactAssignmentTransportStatus= By.xpath("//a[text()='Xact Assignment Transport']/../../..//td[4]/div");
		asyncDocumentUploadStatus= By.xpath("//a[text()='Async Document Upload']/../../..//td[4]/div");
	}
}
