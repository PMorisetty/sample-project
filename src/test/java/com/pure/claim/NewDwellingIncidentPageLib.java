package com.pure.claim;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewDwellingIncidentPageLib extends ActionEngine{
	
	public NewDwellingIncidentPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void setPropertyDescription(String propertyDescription) throws Throwable{
		type(NewDwellingIncidentPage.propertyDescription, propertyDescription, "propertyDescription");
	}
	
	public void setDamageDescription(String damageDescription) throws Throwable{
		type(NewDwellingIncidentPage.damageDescription, damageDescription, "damageDescription");
	}
	
	public void clickOk() throws Throwable{
		click(NewDwellingIncidentPage.okBtn, "okBtn");
		waitForMask();
	}
	
	public void fillDetails(String propertyDescription, String damageDescription) throws Throwable{
		setPropertyDescription(propertyDescription);
		setDamageDescription(damageDescription);
		clickOk();
	}

}
