package com.pure.claim;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.lifeCycle.SummaryOverviewPage;
import com.pure.report.CReporter;

public class DocumentsPageLib extends ActionEngine{
	public DocumentsPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		this.driver = webDriver;
		this.reporter = reporter;
		printPageTitle(DocumentsPage.title,"Page title");
	}
	
	public void selectDocumentForCheck(String docToSelect) throws Throwable{
		String docToSelectXpath = "//div[contains(text(),'" + docToSelect + "')]/../..//td[2]/div";
		By docToSelectBy = By.xpath(docToSelectXpath);
		click(docToSelectBy, "Document to select - "+docToSelect);
		waitForMask();
		click(DocumentsPage.selectBtn, "selectBtn");
	}
	
	// this method is an overloaded method the other method being openDocument(WebElement)
	public void openDocument(String docToSelect) throws Throwable{
		String docToSelectXpath = "//div[contains(text(),'" + docToSelect + "')]/../..//a";
		By docToSelectBy = By.xpath(docToSelectXpath);
		click(docToSelectBy, "Document to select - "+docToSelect);
		waitForMask();
	}
	
	//this method is an overloaded method the other method being openDocument(String)
	public String openDocument(WebElement docToOpen) throws Throwable{
		String parentWindow = getWindowHandle();
		try{
			docToOpen.click();
		}catch(Exception e){}
		waitForMask();
		//the no of windows will be 2 where pdf will be opened in a new tab
		waitForNumberOfWindowsToEqual(2);
		switchToWindow();
		return parentWindow;
	}
	
	private void verifyPolicyNoAndClaimNoInPdf(String policyNo, String claimNo) throws Throwable{
		verifyPolicyOrClaimNo(policyNo);
		claimNo = claimNo.replace("-", "");
		verifyPolicyOrClaimNo(claimNo);
	}
	
	public void verifyISOPDFDocuments(String noOfDocuments, String policyNo, String claimNo) throws Throwable{
		int noOfDocs = Integer.parseInt(noOfDocuments);
		waitUntilAllTheDocumentsAreDisplayed(noOfDocs);
		List<WebElement> docsList = driver.findElements(DocumentsPage.documentsView);
		for(int i=noOfDocs - 1; i>=0; i--){
			deleteDownloadedPdfs();
			String parentWindow = openDocument(docsList.get(i));
			verifyPolicyNoAndClaimNoInPdf(policyNo, claimNo);
			closeWindow();
			switchToParentWindow(parentWindow);
		}
	}
	
	private void waitUntilAllTheDocumentsAreDisplayed(int noOfDocs) throws Throwable{
		boolean isPresent = false;
		int attempts = 0;
		WebElement refreshDocumentsElement = null;
		while(!isPresent && attempts < 1000){
			try{
				refreshDocumentsElement = driver.findElement(DocumentsPage.refereshDocumentsBtn);
			}catch(Exception e){}
			try{
				List<WebElement> lastResponseElement = driver.findElements(DocumentsPage.documentsView);
				if(lastResponseElement.size()>=noOfDocs){
					isPresent = true;
				}else{
					Actions actions = new Actions(this.driver);
					actions.click(refreshDocumentsElement);
					actions.build().perform();
					waitForMask();
				}
			} catch (Exception e) {}
			attempts++;
		}
		if(isPresent){
			reporter.SuccessReport("IsVisible : ", "Last response from ISO is Visible");
		}
		else{
			reporter.failureReport("IsVisible : ", "Last response from ISO is Not Visible : ", driver);
		}
	}
	
	public void waitUntilRequiredDocumentIsDisplayed(String docToSelect) throws Throwable{
		boolean isPresent = false;
		int attempts = 0;
		String docToSelectXpath = "//div[contains(text(),'" + docToSelect + "')]/../..//a";
		By docToSelectBy = By.xpath(docToSelectXpath);
		while(!isPresent && attempts < 1000){
			try{
				WebElement ele = driver.findElement(docToSelectBy);
				if(ele.isDisplayed()){
					isPresent = true;
				}else{
					click(DocumentsPage.refereshDocumentsBtn, "Refresh Documents Btn");
				}
			} catch (Exception e) {}
			attempts++;
		}
		if(isPresent){
			reporter.SuccessReport("IsVisible : ", "Last response from ISO is Visible");
		}
		else{
			reporter.failureReport("IsVisible : ", "Last response from ISO is Not Visible : ", driver);
		}
	}
	public String getDocNameText()throws Throwable{
		waitForVisibilityOfElement(DocumentsPage.docName, "Document Name");
		return getText(DocumentsPage.docName, "Document Name");
	}
	public String getDocTypeText()throws Throwable{
		waitForVisibilityOfElement(DocumentsPage.docType, "docType");
		return getText(DocumentsPage.docType, "docType");
	}
	public void searchDocument(String name) throws Throwable{
		waitForVisibilityOfElement(DocumentsPage.claimDocName, "Claim Document Name");
		type(DocumentsPage.claimDocName, name, "Claim Document Name");
		click(DocumentsPage.searchBtn, " Search Button ");
	}
	
}
