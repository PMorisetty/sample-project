package com.pure.claim;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.lifeCycle.QuickCheckBasicsPage;
import com.pure.claim.lifeCycle.VehicleIncidentPageLib;
import com.pure.report.CReporter;
import com.thoughtworks.selenium.webdriven.commands.GetValue;

public class AddClaimInfoPageLib extends ActionEngine {

	public AddClaimInfoPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {	
		this.driver = webDriver;
		this.reporter = reporter;
		waitForVisibilityOfElement(AddClaimInfoPage.pageTitle, "Claim page tittle");
	}
	public void setDescription(String desc) throws Throwable{
		type(AddClaimInfoPage.description, desc, "Description text box");
	}

	public String getZip() throws Throwable {
		return getText(AddClaimInfoPage.zip, "ZIP");
	}

	public void setZip(String zip) throws Throwable {
		type(AddClaimInfoPage.zip, zip, "ZIP text box");
		type(AddClaimInfoPage.zip, Keys.TAB, "Tab key");
		click(AddClaimInfoPage.zipIcon,"ZipIcon");
		waitForMask();
		//Thread.sleep(2000);//Inevitable: Auto data fill is happening in the background
	}

	public String getLocation() throws Throwable {
		return getText(AddClaimInfoPage.location, "Location");
	}

	public void setLocation(String location) throws Throwable {
		type(AddClaimInfoPage.location, location, "Location dropdown");
	}

	public String getAddress1() throws Throwable {
		return getText(AddClaimInfoPage.addressLine1, "Address1");
	}

	public void setAddress1(String address1) throws Throwable {
		//isElementPresent(AddClaimInfoPage.addressLine1, address1);
		waitForMask();
		waitForVisibilityOfElement(AddClaimInfoPage.addressLine1, "addressLine1");
		waitForMask();
		scroll(AddClaimInfoPage.addressLine1, "addressLine1");
		typeUsingJavaScriptExecutor(AddClaimInfoPage.addressLine1, address1, "Address1 text box");
		type(AddClaimInfoPage.addressLine1, Keys.TAB, "Tab key");
	}
	public String getAddress2() throws Throwable {
		return getText(AddClaimInfoPage.addressLine2, "Address2");
	}

	public void setAddress2(String address2) throws Throwable {
		//isElementPresent(AddClaimInfoPage.addressLine2, address2);
		waitForMask();
		waitForVisibilityOfElement(AddClaimInfoPage.addressLine2, "addressLine2");
		waitForMask();
		scroll(AddClaimInfoPage.addressLine2, "addressLine2");
		typeUsingJavaScriptExecutor(AddClaimInfoPage.addressLine2, address2, "Address2 text box");
		type(AddClaimInfoPage.addressLine2, Keys.TAB, "Tab key");
	}

	public String getCity() throws Throwable {
		return getText(AddClaimInfoPage.city, "City");
	}

	public void setCity(String city) throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try{
			wait.until(ExpectedConditions.elementToBeClickable(AddClaimInfoPage.city));
			if(!getAttributeByValue(AddClaimInfoPage.city, "city").equalsIgnoreCase(city)){
				type(AddClaimInfoPage.city, city, "City text box");
				type(AddClaimInfoPage.city, Keys.TAB, "Tab key");
				waitForMask();
			}
		}
		catch(Exception ex){
			System.out.println("Unable to type city");
			ex.printStackTrace();
		}
	}

	public String getState() throws Throwable {
		return getText(AddClaimInfoPage.state, "State");
	}

	public void setState(String state) throws Throwable {
		type(AddClaimInfoPage.state, state, "State dropdown");
	}

	public String getCountry() throws Throwable {
		return getText(AddClaimInfoPage.country, "Country");
	}

	public void setCountry(String country) throws Throwable {
		type(AddClaimInfoPage.country, country, "Country dropdown");
	}

	public String getLossCatogery() throws Throwable {
		return getText(AddClaimInfoPage.lossCategory, "LossCategory");
	}

	public void setLossCatogery(String lossCategory) throws Throwable {
		WebElement element = driver.findElement(AddClaimInfoPage.lossCategory);		
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

		//isElementPresent(AddClaimInfoPage.lossCategory, lossCategory);
		type(AddClaimInfoPage.lossCategory, lossCategory,
				"LossCategory dropdown");
		type(AddClaimInfoPage.lossCategory, Keys.TAB,
				"Tab Click");
	}

	public String getLossCause() throws Throwable {
		return getText(AddClaimInfoPage.lossCause, "LossCause");
	}

	public void setLossCause(String lossCause) throws Throwable {
		type(AddClaimInfoPage.lossCause, lossCause, "LossCause dropdown");
		type(AddClaimInfoPage.lossCause, Keys.TAB,
				"Tab Click");
	}

	public void selectSeriousInjuryYes() throws Throwable {
		click(AddClaimInfoPage.anySeriousInjuriesRadioYes,
				"Radio button Serious injury YES");
	}

	public void selectSeriousInjuryNo() throws Throwable {
		click(AddClaimInfoPage.anySeriousInjuriesRadioNo,
				"Radio button Serious injury NO");
	}
	public void selectCoverageQuestionYes() throws Throwable {
		click(AddClaimInfoPage.coverageQuestionYes,
				"Radio button Coverage Question YES");
	}

	public void selectCoverageQuestionNo() throws Throwable {
		click(AddClaimInfoPage.coverageQuestionNo,
				"Radio button Coverage Question NO");
	}

	public void selectArsonQuestionYes() throws Throwable {
		click(AddClaimInfoPage.arsonInvolvedYes,
				"Radio button Arson Involved YES");
	}

	public void selectArsonQuestionNo() throws Throwable {
		click(AddClaimInfoPage.arsonInvolvedNo,
				"Radio button Arson Involved NO");
	}

	public void selectIncidentOnlyYes() throws Throwable {
		click(AddClaimInfoPage.incidentOnlyYes,
				"Radio button Incident Only YES");
	}

	public void selectIncidentOnlyNo() throws Throwable {
		click(AddClaimInfoPage.incidentOnlyNo,
				"Radio button Incident Only NO");
	}
	
	public void selectCoverageInQuestionYes() throws Throwable {
		waitForMask();
		scroll(AddClaimInfoPage.coverageQuestionYes, "Coverage In Question Yes");
		click(AddClaimInfoPage.coverageQuestionYes,
				"Radio button Coverage In Question YES");
	}

	public void selectCoverageInQuestionNo() throws Throwable {
		waitForMask();
		scroll(AddClaimInfoPage.coverageQuestionNo, "Coverage In Question No");
		click(AddClaimInfoPage.coverageQuestionNo,
				"Radio button Coverage In Question NO");
	}

	public void selectSpecialInterestYes() throws Throwable {
		click(AddClaimInfoPage.specialInterestYes,
				"Radio button Special Interest YES");
	}

	public void selectSpecialInterestNo() throws Throwable {
		click(AddClaimInfoPage.specialInterestNo,
				"Radio button Special Interest NO");
	}

	public void selectPaInvolvedYes() throws Throwable {
		click(AddClaimInfoPage.paInvolvedYes,
				"Radio button PA Involved YES");
	}

	public void selectPaInvolvedNo() throws Throwable {
		click(AddClaimInfoPage.paInvolvedNo,
				"Radio button PA Involved NO");
	}

	public void fillLossLocation(String postalCode,String address1, String address2) throws Throwable {
		setZip(postalCode);
		setAddress1(address1);
		setAddress2(address2);
	}

	public void setLossLocation(Hashtable<String, String> data) throws Throwable {
		
		setZip(data.get("lossLocationZip"));
		setAddress1(data.get("lossLocationAddress1"));
		setAddress2(data.get("lossLocationAddress2"));
		setCity(data.get("city"));
	}

	public void setLossDescription(String lossDescription) throws Throwable {
		type(AddClaimInfoPage.description, lossDescription, "lossDescription");
		type(AddClaimInfoPage.description, Keys.TAB, "Tab Click");
	}
	public void setLossDetails(Hashtable<String, String> data) throws Throwable{
		if(data.get("lossCategory") != null){
			setLossCatogery(data.get("lossCategory"));
		}
		if(data.get("lossCause") != null){
			setLossCause(data.get("lossCause"));
		}
		if(data.get("arsonInvolved") != null){
			if(data.get("arsonInvolved").equalsIgnoreCase("Yes")){
				selectArsonQuestionYes();
			}else{
				selectArsonQuestionNo();
			}
		}
		if(driver.findElement(AddClaimInfoPage.horizontleSplitter).isDisplayed()){
			click(AddClaimInfoPage.horizontleSplitter, "horizontleSplitter");
		}
		if(data.get("catastrophe") != null){
			type(AddClaimInfoPage.catastrophe, data.get("catastrophe"), "Catastrophe");
			type(AddClaimInfoPage.catastrophe, Keys.TAB, "Tab Click");
		}
		if(data.get("faultRating") != null){
			type(AddClaimInfoPage.faultRating, data.get("faultRating"), "Fault Rating");
			type(AddClaimInfoPage.faultRating, Keys.TAB, "Tab Click");
		}

		if(data.get("specialInterest") != null){
			if(data.get("specialInterest").equalsIgnoreCase("Yes")){
				selectSpecialInterestYes();
			}else{
				selectSpecialInterestNo();
			}
		}
		if(data.get("paInvolved") != null){
			if(data.get("paInvolved").equalsIgnoreCase("Yes")){
				selectPaInvolvedYes();
			}else{
				selectPaInvolvedNo();
			}
		}
		if(data.get("anySeriousInjury") != null){
			if(data.get("anySeriousInjury").equalsIgnoreCase("Yes")){
				selectSeriousInjuryYes();
			}else{
				selectSeriousInjuryNo();
			}
		}
		if(data.get("incidentOnly") != null){
			if(data.get("incidentOnly").equalsIgnoreCase("Yes")){
				selectIncidentOnlyYes();
			}else{
				selectIncidentOnlyNo();
			}
		}
		if(data.get("coverageInQuestion") != null){
			if(data.get("coverageInQuestion").equalsIgnoreCase("Yes")){
				selectCoverageInQuestionYes();
			}else{
				selectCoverageInQuestionNo();
			}
		}


	}
	
	public void enterVehiclesInformation(Hashtable<String, String> data) throws Throwable{
		//Get the only name of the vehicle from "Year Company Model" format
		String vehicleName=data.get("policylevelcoverage").split(" ")[1].trim();
		
		String damageDescription=data.get("damagedescription");
		String driverName=data.get("reporterName");
		String lossoccured = data.get("lossoccured");
		
		By vehicleToSelect = By.xpath("//a[text()='"+vehicleName+"']");
		if(isElementPresent(vehicleToSelect, "vehicleToSelect:"+vehicleName)){
			click(vehicleToSelect,"vehicleToSelect:"+vehicleName);
		}
		
		//click(vehicleToSelect,"vehicle name for more information");
		if(damageDescription!=null){
			if((data.get("airBagDeployed")==null) 
					|| (data.get("airBagDeployed").equalsIgnoreCase("null"))){
				new VehicleIncidentPageLib(driver, reporter).enterDetails(damageDescription, driverName, lossoccured);			
			}else if((data.get("airBagDeployed").equalsIgnoreCase("yes")) || (data.get("airBagDeployed").equalsIgnoreCase("no"))) {
				new VehicleIncidentPageLib(driver, reporter).fillMethodOfInspectionDetails(data);
			}
		}
	}
	
	public void AddPropertiesIncident(Hashtable<String, String> data) throws Throwable{
		click(AddClaimInfoPage.addPropertyIncident,"Property-Add");
		waitForVisibilityOfElement(PropertyLiabilityPage.propertyScreenHeader, "propertyScreenHeader");
		new PropertyLiabilityPageLib(driver, reporter).fillDetails(data);
	}
	
	public void addVesselIncident() throws Throwable{
		click(AddClaimInfoPage.addVesselIncident,"Property-Add");
		waitForMask();
	}
	
	public void addInjuriesIncident() throws Throwable{
		click(AddClaimInfoPage.addInjuriesIncident,"Injuries-Add");
		waitForMask();
	}
	
	public void addVehicleIncident() throws Throwable{
		click(AddClaimInfoPage.addVehicleIncident,"Vehicle-Add");
		waitForMask();
	}
	
	public void clickVehicle(String vehicleName) throws Throwable{
		
		By vehicleToSelect = By.xpath("//a[contains(text(),'"+vehicleName+"')]");
		if(isElementPresent(vehicleToSelect, "vehicleToSelect:"+vehicleName)){
			click(vehicleToSelect,"vehicleToSelect:"+vehicleName);
		}
	
	}


}
