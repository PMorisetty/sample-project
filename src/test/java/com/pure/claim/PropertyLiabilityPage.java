package com.pure.claim;

import org.openqa.selenium.By;

public class PropertyLiabilityPage {
	public static By propertyDescription,
	damageDescription,propertyName,
	propertyScreenHeader,AddressLine1,lossEstimate,
	AddressLine2, zip, zipIcon, okBtn;
	
	
	static{
		/*propertyDescription = By.cssSelector("input[id*=\"FixedPropertyIncidentDV:PropertyDescription\"]");
		damageDescription = By.cssSelector("textarea[id*=\"FixedPropertyIncidentDV:Description\"]");
		lossEstimate = By.cssSelector("input[id*=\"LossEstimate-inputEl\"]");*/
		propertyDescription = By.xpath("//label[text()='Property Description']/../following-sibling::td//input");
		damageDescription = By.xpath("//label[text()='Damage Description']/../following-sibling::td//textarea");
		lossEstimate = By.xpath("//label[text()='Loss Estimate']/../following-sibling::td//input");
		
		//propertyName = By.cssSelector("input[id*=\"Address_Picker-inputEl\"]");
		propertyScreenHeader = By.id("NewFixedPropertyIncidentPopup:NewFixedPropertyIncidentScreen:0");
		AddressLine1 = By.cssSelector("input[id*=\"AddressLine1-inputEl\"]");
		AddressLine2 = By.cssSelector("input[id*=\"AddressLine2-inputEl\"]");
		
		
		zip = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//input");
		zipIcon = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//a");
		okBtn = By.cssSelector("a[id*=\"FixedPropertyIncidentScreen:Update\"]");
		
		propertyName = By.xpath("//input[contains(@id,'Address_Picker-inputEl')]");
	}
}
