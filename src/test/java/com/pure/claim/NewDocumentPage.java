package com.pure.claim;

import org.openqa.selenium.By;

public class NewDocumentPage {
	public static By 
	attchment,
	shareOnMemberPortal,
	description,
	type,
	name,
	updateBtn;
	
	static{
		attchment = By.xpath("//label[text()='Attachment']/../..//input");
		shareOnMemberPortal = By.xpath("//label[text()='Share on Member Portal']/../..//input");
		description = By.xpath("//label[text()='Description']/../..//input");
		type = By.xpath("(//label[text()='Type']/../..//input)[2]");
		name = By.xpath("//label[text()='Name']/../..//input");
		updateBtn = By.id("ClaimNewDocumentLinkedWorksheet:NewDocumentLinkedScreen:Update-btnWrap");
	}
}
