package com.pure.claim;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ClaimCreationSummaryPageLib extends ActionEngine{
	
	public ClaimCreationSummaryPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public String getClaimNumber() throws Throwable{
		String claimNumber = "";
		claimNumber = getAttributeValue(ClaimCreationSummaryPage.claimNumberLabel, "innerText");
		claimNumber = claimNumber.split(" ")[1];
		return claimNumber;
	}
	
	public String getServiceRequestNumber() throws Throwable{		
		return getAttributeValue(ClaimCreationSummaryPage.serviceRequestNumberLabel, "innerText");		 
	}
	
	public void clickNewlyCreatedClaim() throws Throwable{
		click(ClaimCreationSummaryPage.newClaimSaved, "newly created claim");
	}
}
