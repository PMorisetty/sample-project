package com.pure.claim;

import org.openqa.selenium.By;

public class InjuryIncidentPage {
	static By okBtn, cancelBtn,
	injuredPerson,describeInjury,
	addBodyParts, areaOfBody, inputAreaOfBody,
	detailedBodypart,inputDetailedBodyPart, addMedicalDiagnosis,
	icdCode, icdCodeInput, description, descriptionInput,
	title;
	
	static{
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		okBtn = By.cssSelector("[id*=\"Update-btnInnerEl\"]");
		cancelBtn = By.cssSelector("[id*=\"Cancel-btnInnerEl\"]");
		injuredPerson = By.cssSelector("input[id$='Injured_Picker-inputEl']");
		describeInjury = By.cssSelector("textarea[id$='InjuryDescription-inputEl']");
		
		addBodyParts = By.cssSelector("[id*=\"EditableBodyPartDetailsLV_tb:Add-btnInnerEl\"]");
		areaOfBody = By.xpath("//div[contains(@id,'EditableBodyPartDetailsLV-body')]//td[2]");
		inputAreaOfBody = By.name("PrimaryBodyPart");
		detailedBodypart = By.xpath("//div[contains(@id,'EditableBodyPartDetailsLV-body')]//td[3]");
		inputDetailedBodyPart = By.name("DetailedBodyPart");
		
		addMedicalDiagnosis = By.cssSelector("[id*=\"MedicalDiagnosisLV_tb:Add-btnInnerEl\"]");		
		icdCode = By.xpath("//div[contains(@id,'MedicalDiagnosisLVInput:MedicalDiagnosisLV-body')]//td[2]");
		icdCodeInput = By.xpath("//input[@name='ICDCode']");
		//description = By.xpath("//div[contains(@id,'MedicalDiagnosisLVInput:MedicalDiagnosisLV-body')]//td[3]");
	}
}
