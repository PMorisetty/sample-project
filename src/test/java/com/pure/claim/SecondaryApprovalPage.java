package com.pure.claim;

import org.openqa.selenium.By;

public class SecondaryApprovalPage {
	static By tittle,
	ApproveSelectedRows;
	
	static{
		
		ApproveSelectedRows = By.xpath("//span[text()='Approve Selected Rows']");
		tittle = By.xpath("//span[text()='Secondary Approval Activities']");
	}

}
