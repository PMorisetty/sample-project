package com.pure.claim;

import org.openqa.selenium.By;

public class WatercraftIncidentPage {

	public static By damageDescription;
	public static By driverName;
	public static By lossOccured;
	public static By okBtn;
	public static By lossParty;
	public static By selectWatercraft;
	
	
	static{
		selectWatercraft = By.xpath("//input[contains(@id,'PURE_NewWaterCraftIncidentPopup:NewWatercraftIncidentScreen:PURE_WatercraftIncidentDV:Watercraft_Picker-inputEl')]");
		lossParty = By.xpath("//input[contains(@id,'PURE_NewWaterCraftIncidentPopup:NewWatercraftIncidentScreen:PURE_WatercraftIncidentDV:LossParty-inputEl')]");
		driverName = By.xpath("//input[contains(@id,'PURE_NewWaterCraftIncidentPopup:NewWatercraftIncidentScreen:PURE_WatercraftIncidentDV:Driver_Picker-inputEl')]");
		damageDescription = By.xpath("//textarea[contains(@id,'PURE_NewWaterCraftIncidentPopup:NewWatercraftIncidentScreen:PURE_WatercraftIncidentDV:Description-inputEl')]");
		lossOccured = By.xpath("//input[contains(@id,'PURE_NewWaterCraftIncidentPopup:NewWatercraftIncidentScreen:PURE_WatercraftIncidentDV:LossOccured-inputEl')]");
				
	}
}
