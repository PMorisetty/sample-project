package com.pure.claim;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PolicyLocationsPageLib extends ActionEngine{

	public PolicyLocationsPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void verifyLocationAOPDeductible(String expectedDeductible) throws Throwable{
		Double aopDeductibleAmountDouble = Double.parseDouble(expectedDeductible);
		String expctedAopDeductibleAmount = String.format("%,.2f", aopDeductibleAmountDouble);
		assertTextMatching(PolicyLocationsPage.locationAopDeductible, expctedAopDeductibleAmount, "AOP Deductible");
	}
	
	public void verifyLocationSpecialDeductible(String expectedDeductible) throws Throwable{
		Double specialDeductibleAmountDouble = Double.parseDouble(expectedDeductible);
		String expctedAopDeductibleAmount = String.format("%,.2f", specialDeductibleAmountDouble);
		assertTextMatching(PolicyLocationsPage.locationSpecialDeductible, expctedAopDeductibleAmount, "Special Deductible");
	}
	
	public void clickCoveragesTab() throws Throwable{
		click(PolicyLocationsPage.coveragesTab, "coveragesTab");
	}
	
	public void verifyCoveragesAOPDeductibleListAreNotDisplayed() throws Throwable{
		boolean flag = false;
		try{
			List<WebElement> aopDeductiblesList = driver.findElements(PolicyLocationsPage.coveragesAopDeductibleList);
			for(WebElement ele : aopDeductiblesList){
				if(!ele.getText().equals(" ")){
					flag = true;
					break;
				}
			}
		}catch(Exception e){
			reporter.failureReport("Verify AOP deductibles:", "unable to read the AOP deductibles", driver);
		}
		if(flag == true){
			reporter.failureReport("Verify AOP deductibles:", "AOP deductibles are displayed", driver);
		}else{
			reporter.SuccessReport("Verify AOP deductibles:", "AOP deductibles are not displayed");
		}
	}
	
	public void verifyCoveragesSpecialDeductiblesAreNotDisplayed() throws Throwable{
		boolean flag = false;
		try{
			List<WebElement> specialDeductiblesList = driver.findElements(PolicyLocationsPage.coveragesSpecialDeductiblesList);
			for(WebElement ele : specialDeductiblesList){
				if(!ele.getText().equals(" ")){
					flag = true;
					break;
				}
			}
		}catch(Exception e){
			reporter.failureReport("Verify Special deductibles:", "unable to read the Special deductibles", driver);
		}
		if(flag == true){
			reporter.failureReport("Verify Special deductibles:", "Special deductibles are displayed", driver);
		}else{
			reporter.SuccessReport("Verify Special deductibles:", "Special deductibles are not displayed");
		}
	}
	
	public void verifyCoveragesDeductiblesAReNotDisplayed() throws Throwable{
		boolean flag = false;
		try{
			List<WebElement> deductiblesList = driver.findElements(PolicyLocationsPage.coveragesDeductiblesList);
			for(WebElement ele : deductiblesList){
				if(!ele.getText().equals(" ")){
					flag = true;
					break;
				}
			}
		}catch(Exception e){
			reporter.failureReport("Verify deductibles:", "unable to read the deductibles", driver);
		}
		if(flag == true){
			reporter.failureReport("Verify deductibles:", "Deductibles are displayed", driver);
		}else{
			reporter.SuccessReport("Verify deductibles:", "Deductibles are not displayed");
		}
	}
	
	public void verifyDeductibles(Hashtable<String, String> data) throws Throwable{
		String aopDeductibleAmount = data.get("AOPDeductible");
		String specialDeductibleAmount = data.get("SpecialDeductible");
		
		verifyLocationAOPDeductible(aopDeductibleAmount);
		verifyLocationSpecialDeductible(specialDeductibleAmount);
		clickCoveragesTab();
		verifyCoveragesAOPDeductibleListAreNotDisplayed();
		verifyCoveragesSpecialDeductiblesAreNotDisplayed();
		verifyCoveragesDeductiblesAReNotDisplayed();
	}
}
