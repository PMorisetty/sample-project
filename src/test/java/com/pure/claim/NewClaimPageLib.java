package com.pure.claim;


import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyPage;
import com.pure.report.CReporter;

public class NewClaimPageLib extends ActionEngine{
	
	public NewClaimPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		this.driver = webDriver;
		this.reporter = reporter;
		waitForMask();
		printPageTitle(NewClaimPage.title,"Page title");
	}
	
	public void selectVerifiedPolicyOption() throws Throwable {	
		click(NewClaimPage.radiobtnVerifiedPolicy, "radiobtnVerifiedPolicy");	
	}
	public void selectUnVerifiedPolicyOption() throws Throwable {
		click(NewClaimPage.radiobtnUnverifiedPolicy, "Create Unverified Policy");
		waitForInVisibilityOfElement(NewClaimPage.search, "Search button");
	}
	public void clickCancel() throws Throwable{
		click(NewClaimPage.btnCancel, "btnCancel");
	}
	public void clickNext() throws Throwable{
		Thread.sleep(5000);
		click(NewClaimPage.btnNext, "btnNext");
		printPageTitle(NewClaimPage.title,"Page title");
		boolean duplicateClaimFlag = false;
		boolean policyPlugInErrorFlag = false;
		try{
			duplicateClaimFlag = driver.findElement(NewClaimPage.duplicateClaimTab).isDisplayed();
		}catch(Exception ex){
			//Do nothing, as element is not expected to be present always
		}
		if(duplicateClaimFlag){			
			isElementPresent(NewClaimPage.closeButton, "Close button");
			click(NewClaimPage.closeButton, "Close duplicate claims pop up");
			waitForInVisibilityOfElement(NewClaimPage.duplicateClaimTab,"Duplicate claim tab");
		}
		try{
			policyPlugInErrorFlag = driver.findElement(NewClaimPage.policyPluginErrorMsg).isDisplayed();
		}catch(Exception ex){/*Do nothing, as element is not expected to be present always*/}
		if(policyPlugInErrorFlag){
			new VerifiedPolicyLib(driver, reporter).unSelectSearchedPolicyAndReselect();
			click(NewClaimPage.btnNext, "btnNext");
			printPageTitle(NewClaimPage.title,"Page title");
		}
		
	}
	
	public void clickFinish() throws Throwable{
		click(NewClaimPage.btnFinish, "btnFinish");
		waitForMask();
		boolean flag = false;
		try{
			flag = driver.findElement(NewClaimPage.duplicateClaimTab).isDisplayed();
		}catch(Exception ex){
			//Do nothing, as element is not expected to be present always
		}
		if(flag){			
			isElementPresent(NewClaimPage.closeButton, "Close button");
			click(NewClaimPage.closeButton, "Close duplicate claims pop up");
			waitForInVisibilityOfElement(NewClaimPage.duplicateClaimTab,"Duplicate claim tab");
			click(NewClaimPage.btnFinish, "btnFinish");
			waitForMask();
		}
	}
	public void verifyTitle(String pageTitle) throws Throwable{
		String innerText = driver.findElement(NewClaimPage.pageTitle).getAttribute("innerText").trim();
		assertTrue(innerText.equals(pageTitle), "Page title");
	}
	
	//action tree operations
	
}
