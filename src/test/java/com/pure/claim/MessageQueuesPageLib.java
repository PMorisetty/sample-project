package com.pure.claim;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class MessageQueuesPageLib extends ActionEngine{

	public MessageQueuesPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{

		this.driver = webDriver;
		this.reporter = reporter;

	}

	public String getClaimNotificationStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.claimNotificationStatus, "Claim Notification Status");
		return getText(MessageQueuesPage.claimNotificationStatus, "Claim Notification Status");
	}
	public String getDocumentStoreStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.documentStoreStatus, "Document Store Status");
		return getText(MessageQueuesPage.documentStoreStatus, "Document Store Status");
	}
	public String getPaymentNotificationStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.paymentNotificationStatus, "Payment Notification Status");
		return getText(MessageQueuesPage.paymentNotificationStatus, "Payment Notification Status");
	}
	public String getAssignmentTransportStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.assignmentTransportStatus, "Assignment Transport Status");
		return getText(MessageQueuesPage.assignmentTransportStatus, "Assignment Transport Status");
	}
	public String getContactMessageTransportStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.contactMessageTransportStatus, "Contact Message Transport Status");
		return getText(MessageQueuesPage.contactMessageTransportStatus, "Contact Message Transport Status");
	}
	public String getEmailStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.emailStatus, "Email Status");
		return getText(MessageQueuesPage.emailStatus, "Email Status");
	}
	public String getContactAutoSyncFailureStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.contactAutoSyncFailureStatus, "Contact Auto Sync Failure Status");
		return getText(MessageQueuesPage.contactAutoSyncFailureStatus, "Contact Auto Sync Failure Status");
	}
	public String getCheckEmailNotificationStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.voidCheckEmailNotificationStatus, "Void Check Email Notification Status");
		return getText(MessageQueuesPage.voidCheckEmailNotificationStatus, "Void Check Email Notification Status");
	}
	public String getISOStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.isoStatus, "ISO Status");
		return getText(MessageQueuesPage.isoStatus, "ISO Status");
	}
	public String getMetroStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.metroStatus, "Metro Status");
		return getText(MessageQueuesPage.metroStatus, "Metro Status");
	}
	public String getXactAssignmentTransportStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.xactAssignmentTransportStatus, "Xact Assignment Transport Status");
		return getText(MessageQueuesPage.xactAssignmentTransportStatus, "Xact Assignment Transport Status");
	}
	public String getDocUploadStatus()throws Throwable{

		waitForVisibilityOfElement(MessageQueuesPage.asyncDocumentUploadStatus, "Async Document Upload Status");
		return getText(MessageQueuesPage.asyncDocumentUploadStatus, "Async Document Upload Status");
	}
	
	


}
