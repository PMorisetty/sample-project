package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class NewVendorCompanyPage {
	static By name,shareOnMemberPortal,preferredVendor,addBtn, role, inputRole, bankDataAddBtn, updateBtn;
	static By accountName, bankName, accountType, accountNumber, routingNumber, inputAccountName, inputBankName, inputAccountType, inputAccountNumber, inputRoutingNumber;
	
	static {
		name = By.xpath("//input[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV:OrganizationName:GlobalContactNameInputSet:Name-inputEl']");
		shareOnMemberPortal = By.xpath("//input[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV:showOnPortalType-inputEl']");
		preferredVendor = By.xpath("//input[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV:CompanyVendorAdditionalInfoInputSet:IsPreferredVendor_true-inputEl']");
		addBtn = By.xpath("//span[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV:ContactBasicsHeaderInputSet:EditableClaimContactRolesLV_tb:Add-btnInnerEl']");
		bankDataAddBtn = By.xpath("//span[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV:ContactEFTLV_tb:Add-btnInnerEl']");
		updateBtn = By.xpath("//span[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV_tb:ContactDetailToolbarButtonSet:UpdateToolbarButton-btnInnerEl']");
		
		role = By.xpath("//div[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV:ContactBasicsHeaderInputSet:EditableClaimContactRolesLV-body']//td[3]/div");
		inputRole = By.name("Role");
		
		accountName = By.xpath("//div[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV:ContactEFTLV-body']//td[2]/div");
		inputAccountName = By.name("AccountName");
		bankName = By.xpath("//div[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV:ContactEFTLV-body']//td[3]/div");
		inputBankName = By.name("BankName");
		accountType = By.xpath("//div[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV:ContactEFTLV-body']//td[4]/div");
		inputAccountType = By.name("BankAccountType");
		accountNumber = By.xpath("//div[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV:ContactEFTLV-body']//td[5]/div");
		inputAccountNumber = By.name("BankAccountNumber");
		routingNumber = By.xpath("//div[@id='NewPartyInvolvedPopup:ContactDetailScreen:ContactBasicsDV:ContactEFTLV-body']//td[6]/div");
		inputRoutingNumber = By.name("BankRoutingNumber");
		
		
		
		
	}
}
