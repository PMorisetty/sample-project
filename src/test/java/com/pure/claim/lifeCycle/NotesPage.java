package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class NotesPage

{
	static By notesDetailsBody, EmailSent,author,topic;

	static {
		notesDetailsBody = By.xpath("//div[contains(@id, 'NotesSearchScreen:ClaimNotesLV') and contains(@id, 'Body')]");
		EmailSent = By.xpath("//div[contains(text(),'Email Sent')]");
		
		author = By.xpath("//label[text()='Author']/ancestor::td[1]/..//div");
		topic = By.xpath("//label[text()='Topic']/ancestor::td[1]/..//div");
		}
}