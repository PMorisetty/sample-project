package com.pure.claim.lifeCycle;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.MessageQueuesPage;
import com.pure.report.CReporter;

public class BatchProcessInfoPageLib extends ActionEngine{
	public BatchProcessInfoPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void startAutomatedBulkInvoiceJob() throws Throwable{
		click(BatchProcessInfoPage.automatedBulkInvoiceRunBtn, "automatedBulkInvoiceRunBtn");
		waitUntilAutomatedBulkInvoiceRunIsDisplayed();
	}
	
	public void waitUntilAutomatedBulkInvoiceRunIsDisplayed() throws Throwable{
		boolean isEnabled = false;
		int attempts = 0;
		WebElement refreshResponseElement = null;
		
		while(!isEnabled && attempts < 100){
			try{
				refreshResponseElement = driver.findElement(BatchProcessInfoPage.refereshBtn);
			}catch(Exception e){}
			try{
				WebElement runButtonStatus = driver.findElement(BatchProcessInfoPage.automatedBulkInvoiceRunBtn);
				if(runButtonStatus.getAttribute("class").contains("g-actionable")){
					System.out.println(attempts+"{{{{{{{{{{{{{{");
					isEnabled = true;
					break;
				}else{
					System.out.println(attempts+"}}}}}}}}}}}}}}}}}}");
					Actions actions = new Actions(this.driver);
					actions.click(refreshResponseElement);
					actions.build().perform();
					waitForMask();
				}
			} catch (Exception e) {}
			attempts++;
		}
		if(isEnabled){
			reporter.SuccessReport("IsEnabled : ", "Automated Bulk Invoice Run is enabled");
		}
		else{
			reporter.failureReport("IsEnabled : ", "Automated Bulk Invoice Run is not enabled", driver);
		}
		
	}
	public void openClusterInforPage() throws Throwable{
		waitForVisibilityOfElement(BatchProcessInfoPage.clusterInfoLink,"Cluster Info Link");
		click(BatchProcessInfoPage.clusterInfoLink,"Cluster Info Link");
	}
	
}
