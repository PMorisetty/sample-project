package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class CloseLitigationPage {

	static By Resolution, Note, CloseLitigationBtn, CancelBtn;
	
	static{
		Resolution = By.xpath("//input[@id='CloseMatterPopup:CloseMatterScreen:CloseMatterInfoDV:Resolution-inputEl']");
		Note = By.xpath("//textarea[@id='CloseMatterPopup:CloseMatterScreen:CloseMatterInfoDV:Note-inputEl']");		
		CloseLitigationBtn = By.xpath("//span[@id='CloseMatterPopup:CloseMatterScreen:Update-btnInnerEl']");
		CancelBtn = By.xpath("//span[@id='CloseMatterPopup:CloseMatterScreen:Cancel-btnInnerEl']");
				
	}
}
