package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class QuickCheckBasicsPageLib extends ActionEngine{
	
	public QuickCheckBasicsPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		this.driver = webDriver;
		this.reporter = reporter;
		waitForMask();
		printPageTitle(QuickCheckBasicsPage.title,"Page title");
	}

	public void clickNext() throws Throwable{
		click(QuickCheckBasicsPage.nextBtn, "nextBtn");
		boolean duplicateCheckFlag = false;
		try{
			duplicateCheckFlag = driver.findElement(QuickCheckBasicsPage.duplicateCheckTab).isDisplayed();
		}catch(Exception ex){
			//Do nothing, as element is not expected to be present always
		}
		if(duplicateCheckFlag){			
			isElementPresent(QuickCheckBasicsPage.closeButton, "Close button");
			click(QuickCheckBasicsPage.closeButton, "Close duplicate claims pop up");
			waitForInVisibilityOfElement(QuickCheckBasicsPage.duplicateCheckTab,"Duplicate checks tab");
		}
	}
	public void clickCancel() throws Throwable{
		scroll(QuickCheckBasicsPage.cancelBtn, "cancelBtn");
		click(QuickCheckBasicsPage.cancelBtn, "cancelBtn");
		click(QuickCheckBasicsPage.cancelOkBtn, "cancelOkBtn");
		waitForMask();
	}

	public void setPrimaryPayeeName(String name) throws Throwable{
		type(QuickCheckBasicsPage.primaryPayeeName, name, "primary payee name");
		type(QuickCheckBasicsPage.primaryPayeeName, Keys.TAB, "tab key");
	}
	public void setPrimaryPayeeType(String $type) throws Throwable{
		type(QuickCheckBasicsPage.primaryPayeeType, $type, "primary payee type");
		type(QuickCheckBasicsPage.primaryPayeeType, Keys.TAB, "tab key");
	}

	public void setZip(String zip) throws Throwable{
		type(QuickCheckBasicsPage.zip, zip, "Zip");
		click(QuickCheckBasicsPage.zipAutoFill, "Address auto fill");
		Thread.sleep(1000);//Inevitable as form filling takes some time.
		waitForMask();
	}
	public void setAddress1(String Address1) throws Throwable{
		type(QuickCheckBasicsPage.Address1, Address1, "Address1");		
		type(QuickCheckBasicsPage.Address1, Keys.TAB, "tab key");
	}
	
	public void verifyDeductibleDecisionWhenOverrideDeductibleIsOnlyNo(String WaivedOrApplied, String waivedReason, String otherComments) throws Throwable{		
		if(WaivedOrApplied.equalsIgnoreCase("Applied")){			
			click(QuickCheckBasicsPage.deductibleDecisionApplied,"WaivedOrApplied");
			verifyDeductibleAmountAndOverrideDeductibleAreAutoPopulated();
		}else{
			if(isElementPresent(QuickCheckBasicsPage.deductibleTypeAOP, "deductibleTypeAOP"))
				setDeductibleType("AOP");
			click(QuickCheckBasicsPage.deductibleDecisionWaived,"WaivedOrApplied");
			type(QuickCheckBasicsPage.waivedReason,waivedReason, "waivedReason");
			type(QuickCheckBasicsPage.waivedReason,Keys.TAB, "tab key");
			type(QuickCheckBasicsPage.otherComments,otherComments, "otherComments");
			type(QuickCheckBasicsPage.otherComments,Keys.TAB, "tab key");
		}
	}
	
	public void verifyDeductibleDecisionWhenOverrideDeductibleYesNoOptionsAreDisplayed(String WaivedOrApplied, String waivedReason, String otherComments) throws Throwable{		
		if(WaivedOrApplied.equalsIgnoreCase("Applied")){			
			click(QuickCheckBasicsPage.deductibleDecisionApplied,"WaivedOrApplied");
			verifyDeductibleAmountAndOverrideDeductibleIsSetToNo();
		}else{
			click(QuickCheckBasicsPage.deductibleDecisionWaived,"WaivedOrApplied");
			type(QuickCheckBasicsPage.waivedReason,waivedReason, "waivedReason");
			type(QuickCheckBasicsPage.waivedReason,Keys.TAB, "tab key");
			type(QuickCheckBasicsPage.otherComments,otherComments, "otherComments");
			type(QuickCheckBasicsPage.otherComments,Keys.TAB, "tab key");
		}
	}
	
	public void verifyDeductibleDecisionIsNotDisplayed() throws Throwable{
		waitForInVisibilityOfElement(QuickCheckBasicsPage.deductibleDecisionApplied,"deductibleDecisionApplied");
		waitForInVisibilityOfElement(QuickCheckBasicsPage.deductibleDecisionWaived,"deductibleDecisionWaived");
	}
	
	public void verifyDeductibleTypeIsNotDisplayed() throws Throwable{
		waitForInVisibilityOfElement(QuickCheckBasicsPage.deductibleTypeAOP,"deductibleTypeAOP");
		waitForInVisibilityOfElement(QuickCheckBasicsPage.deductibleTypeSpecial,"deductibleTypeSpecial");
	}
	
	public void verifyDeductibleAmountAndOverrideDeductibleAreAutoPopulated() throws Throwable{
		assertText(QuickCheckBasicsPage.overrideDeductible, "No");
		boolean flag = false;
		WebElement deductibleAmount = null;
		try{
			deductibleAmount = driver.findElement(QuickCheckBasicsPage.deductibleAmount);
			flag = deductibleAmount.getText().length()>0;
		}catch(Exception e){}
		if(flag){
			reporter.SuccessReport("Deductible Amount lengtht", "Deductible Amount exists");
		}else{
			reporter.failureReport("Deductible Amount lengtht", "Deductible Amount does not exists", driver);
		}
	}
	
	public void verifyDeductibleAmountAndOverrideDeductibleIsSetToNo() throws Throwable{
		waitForVisibilityOfElement(QuickCheckBasicsPage.overrideDeductibleYes, "overrideDeductibleYes");
		String actualClass = getAttributeByClass(QuickCheckBasicsPage.overrideDeductibleNoParentTag, "overrideDeductibleNoParentTag");
		String expectedClass = "x-form-cb-checked";
		assertTextStringContains(actualClass, expectedClass);
		boolean flag = false;
		WebElement deductibleAmount = null;
		try{
			deductibleAmount = driver.findElement(QuickCheckBasicsPage.deductibleAmount);
			flag = deductibleAmount.getText().length()>0;
		}catch(Exception e){}
		if(flag){
			reporter.SuccessReport("Deductible Amount lengtht", "Deductible Amount exists");
		}else{
			reporter.failureReport("Deductible Amount lengtht", "Deductible Amount does not exists", driver);
		}
	}

	public void setPaymentAmount(String category, String amount) throws Throwable{
		waitForMask();
		Actions actions = new Actions(driver);
		actions.doubleClick(driver.findElement(QuickCheckBasicsPage.categoryFirstCell)).perform();

		type((QuickCheckBasicsPage.lineCategory),category,"Line Category");
		type((QuickCheckBasicsPage.lineCategory),Keys.TAB,"Tab");

		waitForMask();
		actions.doubleClick(driver.findElement(QuickCheckBasicsPage.paymentAmountFirstCell)).perform();
		type(QuickCheckBasicsPage.lineAmount,amount,"amount");
//		waitForMask();
		type(QuickCheckBasicsPage.lineAmount, Keys.TAB, "Tab");

	}
	public void setCity(String city) throws Throwable{
		if(!getText(QuickCheckBasicsPage.city, "city").equalsIgnoreCase(city)){
			type(QuickCheckBasicsPage.city,city,"city");
			type(QuickCheckBasicsPage.city,Keys.TAB,"Tab");
		}
	}
	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		setPrimaryPayeeName(data.get("reporterName"));
		setPrimaryPayeeType(data.get("PrimaryPayeeType"));
		setZip(data.get("lossLocationZip"));
		setAddress1(data.get("lossLocationAddress1"));
		setCity(data.get("city"));
		setPaymentAmount(data.get("category"), data.get("paymentamount"));
		clickNext();
	}
	public void setDeductibleType(String deductibletype) throws Throwable{
		if(deductibletype.equalsIgnoreCase("AOP")){
			click(QuickCheckBasicsPage.deductibleTypeAOP,"deductibleTypeAOP");
		}else{
			click(QuickCheckBasicsPage.deductibleTypeSpecial,"deductibleTypeSpecial");
		}
		
	}
	
	public void verifyMailToAddressDetails(Hashtable<String, String> data) throws Throwable{
		String expectedCountryName = data.get("countryName");
		String expectedPostalCode = data.get("postalCode");
		String expectedProvince = data.get("province");
		String expectedCity = data.get("partiesInvolvedCity");
		
		assertTextMatchingWithAttribute(QuickCheckBasicsPage.country, expectedCountryName, "country");
		assertTextMatchingWithAttribute(QuickCheckBasicsPage.postalCode, expectedPostalCode, "postal code");
		assertTextMatchingWithAttribute(QuickCheckBasicsPage.province, expectedProvince, "province");
		assertTextMatchingWithAttribute(QuickCheckBasicsPage.city, expectedCity, "city");
	}
	
	public void setPaymentMethod(String paymentMethod) throws Throwable{
		if(paymentMethod.equalsIgnoreCase("check")){
			scroll(QuickCheckBasicsPage.paymentMethodCheck, "paymentMethodCheck");
			click(QuickCheckBasicsPage.paymentMethodCheck, "paymentMethodCheck");
		}else{
			scroll(QuickCheckBasicsPage.paymentMethodElectronicFundTransfer, "paymentMethodElectronicFundTransfer");
			click(QuickCheckBasicsPage.paymentMethodElectronicFundTransfer, "paymentMethodElectronicFundTransfer");
		}
	}
	
	public void selectAccountType(String accountType) throws Throwable{
		if(accountType.equalsIgnoreCase("checking")){
			click(QuickCheckBasicsPage.accountTypeChecking, "accountTypeChecking");
		}else if(accountType.equalsIgnoreCase("savings")){
			click(QuickCheckBasicsPage.accountTypeSavings, "accountTypeSavings");
		}else if(accountType.equalsIgnoreCase("other")){
			click(QuickCheckBasicsPage.accountTypeOther, "accountTypeOther");
		}
	}
	
	public void fillPaymentMethodDetails(String name, String accountType, String accountNo, String routingNo) throws Throwable{
		type(QuickCheckBasicsPage.nameOnAccount, name, "nameOnAccount");
		type(QuickCheckBasicsPage.nameOnAccount, Keys.TAB, "Tab key");
		selectAccountType(accountType);
		type(QuickCheckBasicsPage.accountNo, accountNo, "accountNo");
		type(QuickCheckBasicsPage.accountNo, Keys.TAB, "Tab key");
		type(QuickCheckBasicsPage.routingNo, routingNo, "routingNo");
		type(QuickCheckBasicsPage.routingNo, Keys.TAB, "Tab key");
	}
	

}
