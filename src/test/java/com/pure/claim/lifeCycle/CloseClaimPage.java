package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class CloseClaimPage {
	
	static By closeClaimNotes, outcome, deductibleDecision, PAInvolvedYes, 
	PAInvolvedNo, closeClaimBtn, deductibleDataCorrectYes, overrideDecutible, waivedReason,
	otherComments, deductibleType, deductibleDataCorrectNo, deductibleTypeSpecial, deductibleTypeAOP,
	deductibleDecisionWaived, deductibleDecisionApplied, waivedReasonInput, otherCommentsInput, lossCauseYes;

	static{
		closeClaimNotes = By.xpath("//textarea[contains(@id, 'Note-inputEl')]");
		outcome = By.xpath("//input[contains(@id, 'Outcome-inputEl')]");
		deductibleDecision = By.xpath("//label[text()='Deductible Decision']/../../td/div");
		PAInvolvedYes = By.xpath("//input[contains(@id, 'PAInvolvedRadio_true-inputEl')]");
		PAInvolvedNo = By.xpath("//input[contains(@id, 'PAInvolvedRadio_false-inputEl')]");
		closeClaimBtn = By.xpath("//span[contains(@id, 'Update-btnInnerEl')]");
		deductibleDataCorrectYes = By.xpath("//input[contains(@id,'DeductibleConfirmation_true-inputEl')]");
		deductibleDataCorrectNo = By.xpath("//input[contains(@id,'DeductibleConfirmation_false-inputEl')]");
		overrideDecutible = By.xpath("//label[text()='Override Deductible']/../../td/div");
		waivedReason = By.xpath("//label[text()='Waived Reason']/../../td/div");
		otherComments = By.xpath("//label[text()='Other Comments']/../../td/div");
		deductibleType = By.xpath("//label[text()='Deductible Type']/../../td/div");
		deductibleTypeSpecial = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:DeductibleType_false-inputEl')]");
		deductibleTypeAOP = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:DeductibleType_true-inputEl')]");
		deductibleDecisionWaived = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:DeductibleWaived_true-inputEl')]");
		deductibleDecisionApplied = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:DeductibleWaived_false-inputEl')]");
		waivedReasonInput = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:Waive_Reason-inputEl')]");
		otherCommentsInput = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:Waived_OtherReason-inputEl')]");
		lossCauseYes = By.xpath("//input[@id='CloseClaimPopup:CloseClaimScreen:verifyLossDetails_true-inputEl']");
	}
}
