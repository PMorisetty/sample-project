package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class SubrogationPageLib extends ActionEngine{
	
	public SubrogationPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void setSubrogationStatus(Hashtable<String, String> data) throws Throwable{
		String status = data.get("SubrogationStatus");
		type(SubrogationPage.editSubrogationStatus, status, "Subrogation status");
		type(SubrogationPage.editSubrogationStatus, Keys.TAB, "Tab key");
		if(status.equalsIgnoreCase("closed")){
			String closeDate = data.get("SubrogationCloseDate");
			String outcome = data.get("SubrogationOutcome");
			
			type(SubrogationPage.closeDate, closeDate, "close date");
			type(SubrogationPage.closeDate, Keys.TAB, "Tab key");
			
			type(SubrogationPage.outcome, outcome, "outcome");
			type(SubrogationPage.outcome, Keys.TAB, "Tab key");
		}
	}
	
	public void clickUpdate() throws Throwable{
		click(SubrogationPage.update, "update btn");
	}
	
	public void clickEdit() throws Throwable{
		click(SubrogationPage.edit, "edit btn");
	}
	
	public void verifySubrogationStatus(String subrogationStatus) throws Throwable{
		assertText(SubrogationPage.subrogationStatus, subrogationStatus);		
	}
	
	public void clickAssign() throws Throwable{
		click(SubrogationPage.assignBtn, "Assign");
	}
	
	public void openFinancialsPage() throws Throwable{
		click(SubrogationPage.financialsPage, "Financials");
	}
	
	public void clickEditFinancialsPage() throws Throwable{
		click(SubrogationPage.editFinancials, "Edit Financials");
	}
	
	public void clickUpdateFinancialsPage() throws Throwable{
		click(SubrogationPage.updateFinancials, "Update Financials");
	}

}
