package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class BulkInvoicePage {

	public static By lastPage, dateReceivedLink;
	
	static{
		lastPage = By.xpath("//a[@data-qtip='Last Page']");
		dateReceivedLink = By.xpath("//span[text()='Date Received']");
	}
}
