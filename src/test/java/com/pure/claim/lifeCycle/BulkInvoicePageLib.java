package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class BulkInvoicePageLib extends ActionEngine{
	
	public BulkInvoicePageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void selectUploadedCSVFile(String fileName) throws Throwable{
		sortDateReceivedInDescendingOrder();
		String fileToSelect = "//a[text()='"+fileName+"']";
		By fileToSelectBy = By.xpath(fileToSelect);
		click(fileToSelectBy, fileName);
	}
	
	public void navigateToLastPageAndVerifyFileIsNotDisplayed(String fileName) throws Throwable{
		sortDateReceivedInDescendingOrder();
		String fileToSelect = "//a[text()='"+fileName+"']";
		By fileToSelectBy = By.xpath(fileToSelect);
		waitForInVisibilityOfElement(fileToSelectBy, fileName);
	}

	public void sortDateReceivedInDescendingOrder() throws Throwable{
		//click twice on date received link to sort in descending order
		click(BulkInvoicePage.dateReceivedLink, "dateReceived");
		click(BulkInvoicePage.dateReceivedLink, "dateReceived");
	}
	
	public void verifyStatus(String fileName, String expectedStatus) throws Throwable{
		sortDateReceivedInDescendingOrder();
		String actualStatus = "//a[text()='"+fileName+"']/../../../td[3]//a";
		By actualStatusBy = By.xpath(actualStatus);
		assertText(actualStatusBy, expectedStatus);
	}

}
