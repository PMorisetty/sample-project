package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CloseExposurePageLib extends ActionEngine{
	
	public CloseExposurePageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void close(String closeexposurenotes, String deductibleDecision) throws Throwable{
		type(CloseExposurePage.notes,closeexposurenotes,"closeexposurenotes");
		type(CloseExposurePage.notes,Keys.TAB,"tab key");
		type(CloseExposurePage.deductibleDecision ,deductibleDecision,"deductibleDecision entered:"+deductibleDecision);
		type(CloseExposurePage.deductibleDecision,Keys.TAB,"tab key");
		if(isElementPresent(CloseExposurePage.deductibleDataCorrect, "deductibleDataCorrect")){
			click(CloseExposurePage.deductibleDataCorrect, "deductibleDataCorrect");
			if(isElementPresent(CloseExposurePage.okBtn, "Ok btn on warning popup")){
				click(CloseExposurePage.okBtn,"warning ok btn");
			}
		}
		
	}
	
	public void setNote(String note) throws Throwable{
		type(CloseExposurePage.notes,note,"note");
		type(CloseExposurePage.notes,Keys.TAB,"tab key");
	}
	
	public void setOutcome(String outcome) throws Throwable{
		type(CloseExposurePage.outcome,outcome,"outcome");
		type(CloseExposurePage.outcome,Keys.TAB,"tab key");
	}
	
	public void setDeductibleDecision(String decision) throws Throwable{
		type(CloseExposurePage.deductibleDecisionInput,decision,"deductibleDecisionInput");
		type(CloseExposurePage.deductibleDecisionInput,Keys.TAB,"tab key");
	}
	
	public void clickClose() throws Throwable{
		click(CloseExposurePage.closeExposureBtn, "closeExposureBtn");
		if(isElementPresent(CloseExposurePage.okBtn, "okBtn"))
			click(CloseExposurePage.okBtn, "okBtn");
		waitForMask();
		boolean flag = false;
		try{
			flag = driver.findElement(CloseExposurePage.validationResultsTab).isDisplayed();
		}catch(Exception ex){
			//Do nothing, as element is not expected to be present always
		}
		if(flag){			
			isElementPresent(CloseExposurePage.clearBtn, "clearBtn");
			click(CloseExposurePage.clearBtn, "clearBtn");
			waitForInVisibilityOfElement(CloseExposurePage.validationResultsTab,"validationResultsTab");
			click(CloseExposurePage.closeExposureBtn, "closeExposureBtn");
			if(isElementPresent(CloseExposurePage.okBtn, "okBtn"))
				click(CloseExposurePage.okBtn, "okBtn");
		}
	}
	
	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		setNote(data.get("closeexposurenotes"));
		if(data.get("closeexposureoutcome")!=null){
			setOutcome(data.get("closeexposureoutcome"));
		}
		if(data.get("deductibleDataCorrect")!=null){
			setDeductibleDataCorrect(data.get("deductibleDataCorrect"));
		}
		if(data.get("deductibledecision")!=null){
			setDeductibleDecision(data.get("deductibledecision"));
		}
	}
	
	public void clickAwaitingOk() throws Throwable{ 
	    waitForVisibilityOfElement(CloseExposurePage.awaitingSubmissionOkBtn, "awaitingSubmissionOkBtn");
    	click(CloseExposurePage.awaitingSubmissionOkBtn, "awaitingSubmissionOkBtn");
	}   
	
	public void setDeductibleDataCorrect(String deductibleData) throws Throwable{
		if(deductibleData.equalsIgnoreCase("yes")){
			click(CloseExposurePage.deductibleDataYes, "deductibleDataYes");
		}else{
			click(CloseExposurePage.deductibleDataNo, "deductibleDataNo");
		}
	}
	
	public void verifyDeductibleManagementWorkSheet(String deductibleDecision, String deductibleAmount, String waivedReason, String otherComments, String ovverrideDeductible) throws Throwable{
		assertText(CloseExposurePage.deductibleDecision, deductibleDecision);
		if(deductibleDecision.equalsIgnoreCase("waived")){
			assertText(CloseExposurePage.waivedReason, waivedReason);
			assertText(CloseExposurePage.otherComments, otherComments);
		}else{
			Double deductibleAmountDouble = Double.parseDouble(deductibleAmount);
			String expctedDeductibleAmount = String.format("%.2f", deductibleAmountDouble);
			assertText(CloseExposurePage.deductibleAmount, expctedDeductibleAmount);
			assertText(CloseExposurePage.overrideDecutible, ovverrideDeductible);
		}
	}
	
	public void fillDMWDetails(String deductibleDecision, String deductibleAmount, String waivedReason, String otherComments, String ovverrideDeductible) throws Throwable{
		click(CloseExposurePage.deductibleDataNo, "deductibleDataNo");
		if(deductibleDecision.equalsIgnoreCase("waived")){
			click(CloseExposurePage.deductibleDecisionWaived, "deductibleDecisionWaived");
			type(CloseExposurePage.waivedReasonInput, waivedReason, "waivedReason");
			type(CloseExposurePage.waivedReasonInput, Keys.TAB, "Tab keys");
			waitForMask();
			type(CloseExposurePage.otherCommentsInput, otherComments, "otherComments");
			type(CloseExposurePage.otherCommentsInput, Keys.TAB, "Tab keys");
		}else{
			click(CloseExposurePage.deductibleDecisionApplied, "deductibleDecisionApplied");
		}
		click(CloseExposurePage.deductibleDataYes, "deductibleDataYes");
	}
	
	public void clearValidationsErrorAndClose() throws Throwable{
		if(isElementPresent(CloseExposurePage.clearBtn, "clearBtn")){
			click(CloseExposurePage.clearBtn, "clearBtn");
			waitForInVisibilityOfElement(CloseExposurePage.validationResultsTab,"validationResultsTab");
		}
		clickClose();
		clickAwaitingOk();
	}
}

