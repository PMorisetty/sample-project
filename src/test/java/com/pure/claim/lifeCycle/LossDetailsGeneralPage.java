package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class LossDetailsGeneralPage {
	
	public static By EditBtn, addBtn, UpdateBtn, CancelBtn;

	static {
		EditBtn = By.id("ClaimLossDetails:ClaimLossDetailsScreen:Edit-btnInnerEl");
		addBtn = By.id("ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:LossDetailsCardCV:LossDetailsDV:EditableVehicleIncidentsLV_tb:Add-btnInnerEl");
		UpdateBtn = By.xpath("//span[@id='ClaimLossDetails:ClaimLossDetailsScreen:Update-btnInnerEl']");
		CancelBtn = By.xpath("//span[@id='ClaimLossDetails:ClaimLossDetailsScreen:Cancel-btnInnerEl']");

	}
}
