package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class AssignActivitiesPageLib extends ActionEngine{
	
	public AssignActivitiesPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void assignActivity(String assignee) throws Throwable{
		type(AssignActivitiesPage.selectAssigneeFromList, assignee, "Assignee");
		type(AssignActivitiesPage.selectAssigneeFromList, Keys.TAB, "TAB");
		waitForMask();
		scroll(AssignActivitiesPage.assignBtn, "Assign Button");
		click(AssignActivitiesPage.assignBtn, "Assign Button");
	}
}
