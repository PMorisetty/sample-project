package com.pure.claim.lifeCycle;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class FinancialsPageLib extends ActionEngine{
	
	public FinancialsPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {		
		this.driver = webDriver;
		this.reporter = reporter;
		JSClick(ClaimsTree.Financials,"Financials");	
		waitForMask();
		printPageTitle(FinancialsPage.title,"Page title");
	}
	
	private void expandFinancialSummary() throws Throwable{
		click(FinancialsPage.claimFinancialsSummaryIcon, "summaryIcon");		
	}
	
	public void openQuickCheck() throws Throwable{
		//if(!isElementPresent(FinancialsPage.quickCheck, "QuickCheck")){
		//	expandFinanciaSummary();			
		//}
		expandFinancialSummary();
		click(FinancialsPage.quickCheck,"quickCheck");
	}
	
	public void openCreateCheck() throws Throwable{
		//if(!isElementPresent(FinancialsPage.quickCheck, "QuickCheck")){
		//	expandFinanciaSummary();			
		//}
		expandFinancialSummary();
		click(FinancialsPage.createCheck,"Create Check");
	}
	
	public void openEditReserve() throws Throwable{
		expandFinancialSummary();
		click(FinancialsPage.editReserve,"Edit Reserve");
	}
	
	public String getTotalPayment() throws Throwable{
		String totalPayment = getAttributeValue(FinancialsPage.totalPaymentsInSummary, "innerText");
		reporter.SuccessReport("Total Payments made:", totalPayment);
		return totalPayment;
	}
	
	public void openFinancials() throws Throwable{
		JSClick(ClaimsTree.Financials,"Financials");	
		waitForMask();
	}
	
	public void openChecks() throws Throwable{
		openFinancials();	
		JSClick(ClaimsTree.checks,"Checks");
		if(!isElementPresent(ChecksPage.cellCheckNumber,"cellCheckNumber")){
			JSClick(ClaimsTree.checks,"Checks");
		}
	}
	
	public void openTransactions() throws Throwable{
		openFinancials();
		JSClick(ClaimsTree.transactions,"transactions");
		
	}
	
	public void verifyItemsInCostCatagoryQuickCheckMenu() throws Throwable{
		//In most of the testcases,so far, have CostType = "Claim cost" and CostCatagory = "Other" in Reserve
		// Same will be listed in Financials Summary page.
		expandFinancialSummary();
		waitForMask();
		isVisible(FinancialsPage.editReserve, "Edit Reserve");
		isVisible(FinancialsPage.quickCheck, "Quick Check");
		isVisible(FinancialsPage.createCheck, "Create Check");
	}
	
	public void verifyExposureAndCostType(String exposureTextToVerify, String costTypeToVerify) throws Throwable{
		String exposureXpath = "//div[text()='" + exposureTextToVerify + "']";
		By exposureToBeVerified = By.xpath(exposureXpath);
		waitForVisibilityOfElement(exposureToBeVerified,"Exposure");
		String costTypeXpath = exposureXpath + "/../../../tr[2]/td[1]/div";
		By costToBeVerified = By.xpath(costTypeXpath);
		assertTextMatching(costToBeVerified, costTypeToVerify, "Cost Type");
		
	}
	
	public void openQuickCheckBasedOnCoverage(String coverage) throws Throwable{
		By financialIcon = By.xpath("//div[contains(text(), '"+coverage+"')]/../../following-sibling::tr[2]/td//img");
		click(financialIcon,"financialIcon");
		click(FinancialsPage.quickCheck,"quickCheck");
		waitForMask();
	}
	
	public void openCreateCheckBasedOnCoverage(String coverage) throws Throwable{
		By financialIcon = By.xpath("//div[contains(text(), '"+coverage+"')]/../../following-sibling::tr[2]/td//img");
		click(financialIcon,"financialIcon");
		click(FinancialsPage.createCheck,"Create Check");
		waitForMask();
	}
	
	public void verifyCostTypeAndExpensePACAAreDisplayedInSummaryPage(String costType, String expectedCostTypeReserveAmount, String expensePACA) throws Throwable{
		String actualCostTypeReserveAmountString = "//div[text()='"+costType+"']/../../td[3]//a";
		By actualCostTypeReserveAmountBy = By.xpath(actualCostTypeReserveAmountString);
		assertTextMatching(actualCostTypeReserveAmountBy, expectedCostTypeReserveAmount, "Cost Type reserve amount");
		assertTextMatching(FinancialsPage.expensePACAAmount, expensePACA, "Expense PACA reserve amount");
	}
	
	public void verifyCostTypeAndExpensePACAAreDisplayedInTransactionPage(String costType, String expectedCostTypeReserveAmount, String expensePACA, String transactionType) throws Throwable{
		verifyCostTypeAndExpensePACAAmounts(costType, expectedCostTypeReserveAmount, expensePACA);
		type(FinancialsPage.transactionType, transactionType, "transactionType");
		type(FinancialsPage.transactionType, Keys.TAB, "Tab key");
		verifyCostTypeAndExpensePACAAmounts(costType, expectedCostTypeReserveAmount, expensePACA);
	}
	
	public void verifyCostTypeAndExpensePACAAmounts(String costType, String expectedCostTypeReserveAmount, String expensePACA) throws Throwable{
		String actualCostTypeReserveAmountString = "//div[text()='"+costType+"']/../..//a[contains(@id,'Amount')]";
		By actualCostTypeReserveAmountBy = By.xpath(actualCostTypeReserveAmountString);
		assertTextMatching(actualCostTypeReserveAmountBy, expectedCostTypeReserveAmount, "Cost Type reserve amount");
		assertTextMatching(FinancialsPage.expensePACATransactionAmount, expensePACA, "Expense PACA reserve amount");
	}
	
	public void verifyCostTypeAmountAfterEditingReserve(String costType, String intialReserveAmount, String changeAmount) throws Throwable{
		String actualCostTypeReserveAmountString = "//div[text()='"+costType+"']/../..//a[contains(@id,'Amount')]";
		By actualCostTypeReserveAmountBy = By.xpath(actualCostTypeReserveAmountString);
		List<WebElement> acutalCostList = driver.findElements(actualCostTypeReserveAmountBy);
		String actualCostText[] = new String[2];
		int counter = 0;
		for(WebElement e: acutalCostList){
			actualCostText[counter] = e.getText();
			counter++;
		}
		assertTextStringContains(actualCostText[0], intialReserveAmount);
		assertTextStringContains(actualCostText[1], changeAmount);
	}
	
	public void verifyCreatedReserveAmount(String costType, String expectedAmount) throws Throwable{
		String actualAmount = "//div[text()='"+costType+"']/../../td[1]//a[text()='Reserve']/../../../td[3]//a";
		By actualAmountBy = By.xpath(actualAmount);
		assertTextMatching(actualAmountBy, expectedAmount, "Reserve Amount");
	}
	
	public void verifyAbilityToPayNoValidationsErrorMsg() throws Throwable{
		waitForVisibilityOfElement(FinancialsPage.validationResultsTab, "validationResultsTab");
		String expectedMsg = "No validation errors.";
		assertTextMatching(FinancialsPage.validationMsg, expectedMsg, "Validation message");
		click(FinancialsPage.clearValidationBtn, "clearValidationBtn");
	}
	
	public void verifyNoExposureFoundAtAbilityToPayError() throws Throwable{
		waitForVisibilityOfElement(FinancialsPage.validationResultsTab, "validationResultsTab");
		String expectedMsg = "The claim's description must not be null";
		List<WebElement> ele =driver.findElements(FinancialsPage.validationMsgsList);
		boolean isPresent = false;
		for(WebElement e:ele){
			if(e.getText().equalsIgnoreCase(expectedMsg)){
				isPresent = true;
				break;
			}
		}
		if(isPresent){
			reporter.SuccessReport("AssertText: Expected: " + expectedMsg,
					expectedMsg+" is present in the list");
		}else{
			reporter.failureReport("AssertText: Expected: " + expectedMsg,
					expectedMsg+" is not present in the list", driver);
		}
//		assertTextMatching(FinancialsPage.validationMsg, expectedMsg, "Validation message");
		click(FinancialsPage.clearValidationBtn, "clearValidationBtn");
	}
	
	public void verifyCreatedReserveUser(String costType, String expectedUser) throws Throwable{
		String actualUser = "//div[text()='"+costType+"']/../../td[1]//a[text()='Reserve']/../../../td[9]/div";
		By actualUserBy = By.xpath(actualUser);
		expectedUser = expectedUser.substring(0,expectedUser.length()-2);
		assertTextMatching(actualUserBy, expectedUser, "User - "+expectedUser);
	}
	
	public void verifyBulkInvoiceCreatedReserveUserAndAmount(String costType, String amount, String expectedUser) throws Throwable{
		String actualAmount = "//div[text()='"+expectedUser+"']/../..//div[text()='"+costType+"']/../..//a[contains(@id,'Amount')]";
		By actualAmountBy = By.xpath(actualAmount);
		waitForVisibilityOfElement(actualAmountBy, expectedUser);
		assertTextMatching(actualAmountBy, amount, "Amount is "+amount);
	}
	
	public void verifyBulkInvoicePaymentDetails(String costtype, String amount, String status, String user) throws Throwable{
		String actualAmount = "//div[text()='"+costtype+"']/../..//a[text()='Payment']/../../..//a[contains(text(),'"+amount+"')]";
		By actualAmountBy = By.xpath(actualAmount);
		waitForVisibilityOfElement(actualAmountBy, amount);
		
		String actualUser = "//div[text()='"+costtype+"']/../..//a[text()='Payment']/../../..//a[contains(text(),'"+amount+"')]/../../..//div[text()='"+user+"']";
		By actualUserBy = By.xpath(actualUser);
		waitForVisibilityOfElement(actualUserBy, user);
	}
	
	public void verifyBulkInvoiceReserveDetails(String costtype, String amount, String user) throws Throwable{
		String actualUser = "//div[text()='"+costtype+"']/../..//a[text()='Reserve']/../../..//a[contains(text(),'"+amount+"')]/../../..//div[text()='"+user+"']";
		By actualUserBy = By.xpath(actualUser);
		waitForVisibilityOfElement(actualUserBy, user);
	}
	
	public void verifyCreatedReserveUserAndAmount(String costtype, String amount, String user) throws Throwable{
		String actualUser = "//div[text()='"+costtype+"']/../..//a[text()='Payment']/../../..//a[contains(text(),'"+amount+"')]/../../..//div[text()='"+user+"']";
		By actualUserBy = By.xpath(actualUser);
		waitForVisibilityOfElement(actualUserBy, user);
		
	}
	
	public void verifyPaymentStatus(String status) throws Throwable{
		assertTextMatching(FinancialsPage.paymentStatus, status, "Status");
	}
	
	public void verifyOpenRecoveryReserveCreated(String exposure, String openRecoveryReserveAmt) throws Throwable{
		String exposureXpath = "//div[contains(text(),'"+exposure+"')]";
		waitForVisibilityOfElement(By.xpath(exposureXpath), "exposure");
		String amountXpath = exposureXpath+"/../..//td[2]/div/a[contains(text(),'"+ openRecoveryReserveAmt +"')]";
		waitForVisibilityOfElement(By.xpath(amountXpath), "openRecoveryReserveAmt");
	}
	
	public void verifyPACACreatedAtClaim(Boolean status) throws Throwable{
		verifyElementPresent(FinancialsPage.claimLevel, "claimLevel", status);
	}
}
