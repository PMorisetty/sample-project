package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class PartiesInvolvedPage {
	
	static By partiesInvolvedLink, contactsLink, usersLink, editBtn, bankAddBtn, bankDataTable, updateBtn;
	static By accountNameCell,bankNameCell,accountType_cell,yes_primary;
	
	static {
		partiesInvolvedLink = By.xpath("//span[contains(@class,'tree') and text()='Parties Involved']");
		contactsLink = By.xpath("//span[contains(@class,'tree') and text()='Contacts']");
		usersLink = By.xpath("//span[contains(@class,'tree') and text()='Users']");	
		
		editBtn = By.id("ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:ContactBasicsDV_tb:ContactDetailToolbarButtonSet:Edit-btnInnerEl");
		bankAddBtn = By.id("ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:ContactBasicsDV:ContactEFTLV_tb:Add-btnInnerEl");
		bankDataTable = By.xpath("//div[@id='ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:ContactBasicsDV:ContactEFTLV-body']/div//tr");
		
		updateBtn = By.id("ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:ContactBasicsDV_tb:ContactDetailToolbarButtonSet:UpdateToolbarButton-btnInnerEl");
		
		accountNameCell = By.xpath("//div[@id='ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:ContactBasicsDV:ContactEFTLV-body']/div//tr/td[2]/div");
		bankNameCell = By.xpath("//div[@id='ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:ContactBasicsDV:ContactEFTLV-body']/div//tr/td[3]/div");
		accountType_cell = By.xpath("//input[@name='BankAccountType']");
		yes_primary = By.xpath("//div[@id='ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:ContactBasicsDV:ContactEFTLV-body']/div//tr/td[7]/div//label[contains(text(),'Yes')]/preceding-sibling::input");
	}
}
