package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class WorkPlanPage {

	static By selectAllCheckBox, assignedAsMemberAdvocate,
	completeAllbtn, activitiesList, firstActivityCheckBox, skipBtn, completeActivityBtn, workplanFilterDropdown;
	static{
		selectAllCheckBox = By.xpath("//span[contains(@id,'rowcheckcolumn')]/div");
		completeAllbtn = By.id("ClaimWorkplan:ClaimWorkplanScreen:ClaimWorkplan_CompleteButton-btnInnerEl");
		activitiesList = By.xpath("//div[contains(@id, 'WorkplanLV-body')]//td[7]");
		firstActivityCheckBox = By.xpath("//div[contains(@id, 'WorkplanLV-body')]//tr[1]/td[1]/div");
		skipBtn = By.xpath("//span[contains(@id, 'SkipButton-btnInnerEl')]");
		assignedAsMemberAdvocate = By.xpath("//a[text()='You have been assigned as the Member Advocate on the Claim']");
		completeActivityBtn = By.xpath("//span[contains(@id,'ActivityDetailScreen_CompleteButton-btnInnerEl')]");
		workplanFilterDropdown = By.xpath("//input[@id='ClaimWorkplan:ClaimWorkplanScreen:WorkplanLV:WorkplanFilter-inputEl']");
	}
}
