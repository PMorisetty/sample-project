package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class EnterPayeeInformationPageLib extends ActionEngine{
		public EnterPayeeInformationPageLib(EventFiringWebDriver driver, CReporter reporter){
			this.driver = driver;
			this.reporter = reporter;
		}
		
		public void enterPrimaryPayeeName(String name) throws Throwable{
			type(EnterPayeeInformationPage.payeeName, name, "Primary payee name");
			type(EnterPayeeInformationPage.payeeName, Keys.TAB, "Tab key");
		}
		
		public void setPaymentMethod(String paymentMethod) throws Throwable{
			if(paymentMethod.equalsIgnoreCase("check")){
				click(EnterPayeeInformationPage.paymentMethodCheck, "paymentMethodCheck");
			}else{
				click(EnterPayeeInformationPage.paymentMethodElectronicFundTransfer, "paymentMethodElectronicFundTransfer");
			}
		}
		
		public void clickNext() throws Throwable{
			click(EnterPayeeInformationPage.nextBtn, "nextBtn");
		}
		
		public void fillPayeeAndPaymentDetails(String payeeName, String paymentMethod) throws Throwable{
			enterPrimaryPayeeName(payeeName);
			setPaymentMethod(paymentMethod);
		}
		
		public void selectAccountType(String accountType) throws Throwable{
			if(accountType.equalsIgnoreCase("checking")){
				click(EnterPayeeInformationPage.accountTypeChecking, "accountTypeChecking");
			}else if(accountType.equalsIgnoreCase("savings")){
				click(EnterPayeeInformationPage.accountTypeSavings, "accountTypeSavings");
			}else if(accountType.equalsIgnoreCase("other")){
				click(EnterPayeeInformationPage.accountTypeOther, "accountTypeOther");
			}
		}
		
		public void fillPaymentMethodDetails(String name, String accountType, String accountNo, String routingNo) throws Throwable{
			type(EnterPayeeInformationPage.nameOnAccount, name, "nameOnAccount");
			type(EnterPayeeInformationPage.nameOnAccount, Keys.TAB, "Tab key");
			selectAccountType(accountType);
			type(EnterPayeeInformationPage.accountNo, accountNo, "accountNo");
			type(EnterPayeeInformationPage.accountNo, Keys.TAB, "Tab key");
			type(EnterPayeeInformationPage.routingNo, routingNo, "routingNo");
			type(EnterPayeeInformationPage.routingNo, Keys.TAB, "Tab key");
		}
}
