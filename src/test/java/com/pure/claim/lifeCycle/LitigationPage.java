package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class LitigationPage {

	static By Name, LitigationType, State, TrialVenue, TrailVenueIcon, LegalVenueSelect, LegalVenueName, LegalVenueZip,
			LegalVenueAddress1, LegalVenueOKbtn, LawSuiteType, DateReported, Defendent, DefenseLawFirm, DefenseLawFirmIcon,
			SearchAddressBook_PTiff, ServiceState, SearchDefenceLawFirm, Plaintiff, PlaintiffIcon, SearchAddressBook_def, UpdateBtn,
			ClearValidationAlert, SelectLitigationName, VerifyLitigationNameUpdated, VerifyLitigationCreated, EditBtn, AssignBtn,
			VerifyAssignedTo, CloseBtn ;

	static {
		Name = By.xpath("//input[contains(@id,'Matter_Name-inputEl')]");
		LitigationType = By.xpath("//input[@id='NewMatter:NewMatterScreen:NewMatterDV:TrialDetails_MatterType-inputEl']");
		State = By.xpath("//input[@id='NewMatter:NewMatterScreen:NewMatterDV:CourtDistrict-inputEl']");
		TrialVenue = By.xpath("//input[@id='NewMatter:NewMatterScreen:NewMatterDV:TrialDetails_Venue-inputEl']");
		TrailVenueIcon = By.xpath("//a[@id='NewMatter:NewMatterScreen:NewMatterDV:TrialDetails_Venue:TrialDetails_VenueMenuIcon']");
		LegalVenueSelect = By.xpath("//span[contains(text(),'Legal Venue')]");
		LegalVenueName = By.xpath("//input[@id='NewContactPopup:ContactDetailScreen:ContactBasicsDV:OrganizationName-inputEl']");
		LegalVenueZip = By
				.xpath("//input[@id='NewContactPopup:ContactDetailScreen:ContactBasicsDV:PrimaryAddressInputSet:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:PostalCode-inputEl']");
		LegalVenueAddress1 = By
				.xpath("//input[@id='NewContactPopup:ContactDetailScreen:ContactBasicsDV:PrimaryAddressInputSet:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:AddressLine1-inputEl']");
		LegalVenueOKbtn = By.xpath("//span[@id='NewContactPopup:ContactDetailScreen:ContactBasicsDV_tb:ContactDetailToolbarButtonSet:Update-btnInnerEl']");
		LawSuiteType = By.xpath("//input[@id='NewMatter:NewMatterScreen:NewMatterDV:lawsuitType-inputEl']");
		DateReported = By.xpath("//input[@id='NewMatter:NewMatterScreen:NewMatterDV:noticeDate-inputEl']");
		
		Defendent = By.xpath("//input[@id='NewMatter:NewMatterScreen:NewMatterDV:Defendant-inputEl']");
//		DefenseLawFirm = By.xpath("//input[@id='NewMatter:NewMatterScreen:NewMatterDV:Counsel_DefenseLawFirm-inputEl']");
		DefenseLawFirmIcon = By.xpath("//a[@id='NewMatter:NewMatterScreen:NewMatterDV:Counsel_DefenseLawFirm:Counsel_DefenseLawFirmMenuIcon']");
		SearchAddressBook_def = By.xpath("//span[contains(@id,'Counsel_DefenseLawFirm:MenuItem_Search-textEl')]");
		
		Plaintiff = By.xpath("//input[@id='NewMatter:NewMatterScreen:NewMatterDV:Plaintiff-inputEl']");
		PlaintiffIcon = By.xpath("//a[@id='NewMatter:NewMatterScreen:NewMatterDV:Counsel_PlaintiffLawFirm:Counsel_PlaintiffLawFirmMenuIcon']");
		SearchAddressBook_PTiff = By.xpath("//span[contains(@id,'Counsel_PlaintiffLawFirm:MenuItem_Search-textEl')]");
		UpdateBtn = By.xpath("//span[contains(@id,':Update-btnInnerEl')]");
		ClearValidationAlert = By.xpath("//span[@id='WebMessageWorksheet:WebMessageWorksheetScreen:WebMessageWorksheet_ClearButton-btnInnerEl']");
		VerifyLitigationCreated = By.xpath("//div[@id='ClaimSummary:ClaimSummaryScreen:ClaimSummaryMattersLV-body']//a[contains(text(),'$')]");

		//Litigations Page
		SelectLitigationName = By.xpath("//div[@id='ClaimMatters:ClaimMatterScreen:MattersLV-body']//a[contains(text(),'$')]");
		VerifyLitigationNameUpdated = By.xpath("//div[@id='MatterDetailPage:MatterDetailScreen:MatterDetailsDV:Matter_Name-inputEl'][contains(text(),'$')]");
		EditBtn = By.xpath("//span[@id='MatterDetailPage:MatterDetailScreen:Edit-btnInnerEl']");

		AssignBtn = By.xpath("//span[contains(@id,'AssignButton-btnInnerEl')]");
		VerifyAssignedTo = By.xpath("//div[@id='MatterDetailPage:MatterDetailScreen:MatterDetailsDV:AssignedUser_Name-inputEl']");
		CloseBtn = By.xpath("//span[@id='MatterDetailPage:MatterDetailScreen:MatterDetailPage_CloseMatterButton-btnInnerEl']");
	}
}
