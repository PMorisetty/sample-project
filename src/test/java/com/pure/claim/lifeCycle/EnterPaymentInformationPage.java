package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class EnterPaymentInformationPage {

	static By reserveLine, paymentType, paymentAmountCell, paymentAmount, nextBtn,
	deductibleTypeAOP, deductibleTypeSpecial, deductibleDecisionApplied, deductibleDecisionWaived,
	waivedReason, otherComments, overrideDeductibleYes, overrideDeductibleNo, deductibleAmount, overrideDeductibleNoParentTag;
	
	static{
		reserveLine = By.xpath("//input[contains(@id,'NewPaymentDetailDV:ReserveLineInputSet:ReserveLine-inputEl')]");
		paymentType = By.xpath("//input[contains(@id,'NewPaymentDetailDV:Payment_PaymentType-inputEl')]");
		paymentAmountCell = By.xpath("//div[contains(@id,'NewPaymentDetailDV:EditablePaymentLineItemsLV-body')]//tbody//td[3]/div");
		paymentAmount = By.xpath("//input[@name='Amount']");
		nextBtn = By.id("NormalCreateCheckWizard:Next-btnInnerEl");
		deductibleTypeAOP = By.xpath("//input[contains(@id,'DeductibleType_true-inputEl')]");
		deductibleTypeSpecial = By.xpath("//input[contains(@id,'DeductibleType_false-inputEl')]");
		deductibleDecisionApplied = By.xpath("//input[contains(@id,'DeductibleWaived_false-inputEl')]");
		deductibleDecisionWaived = By.xpath("//input[contains(@id,'DeductibleWaived_true-inputEl')]");
		waivedReason = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:Waive_Reason-inputEl')]");
		otherComments = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:Waived_OtherReason-inputEl')]");
		overrideDeductibleYes = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:Deductible_Override_true-inputEl')]");
		overrideDeductibleNo = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:Deductible_Override_false-inputEl')]");
		deductibleAmount = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:Deductible_amount-inputEl')]");
		overrideDeductibleNoParentTag = By.xpath("//input[contains(@id,'Deductible_Override_false-inputEl')]/../../../../..");
	}
}
