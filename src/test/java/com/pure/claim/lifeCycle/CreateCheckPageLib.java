package com.pure.claim.lifeCycle;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CreateCheckPageLib extends ActionEngine {

	public CreateCheckPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void clickCancel() throws Throwable{
		click(CreateCheckPage.cancelBtn, "Cancel buton");
	}
	
	public void clickNext() throws Throwable{
		click(CreateCheckPage.nextBtn, "Next buton");
		boolean duplicateClaimFlag = false;
		try{
			duplicateClaimFlag = driver.findElement(CreateCheckPage.duplicateClaimTab).isDisplayed();
		}catch(Exception ex){
			//Do nothing, as element is not expected to be present always
		}
		if(duplicateClaimFlag){			
			isElementPresent(CreateCheckPage.closeButton, "Close button");
			click(CreateCheckPage.closeButton, "Close duplicate claims pop up");
			waitForInVisibilityOfElement(CreateCheckPage.duplicateClaimTab,"Duplicate claim tab");
		}
	}
	
	public void clickBack() throws Throwable{
		click(CreateCheckPage.backBtn, "Back buton");
	}
	
	public void clickFinish() throws Throwable{
		click(CreateCheckPage.finishBtn, "Finish buton");
	}
	
}
