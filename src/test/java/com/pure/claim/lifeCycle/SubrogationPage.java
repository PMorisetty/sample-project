package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class SubrogationPage {
	
	static By editSubrogationStatus, closeDate, outcome, update, edit, assignBtn, subrogationStatus,
		financialsPage, editFinancials, updateFinancials;
	
	static{
		editSubrogationStatus = By.xpath("//input[@id='SubrogationGeneral:ClaimSubroSummaryScreen:SubrogationMainDV:SubrogationStatus-inputEl']");
		closeDate = By.xpath("//input[@id='SubrogationGeneral:ClaimSubroSummaryScreen:SubrogationMainDV:SubroCloseDate-inputEl']");
		outcome = By.xpath("//input[@id='SubrogationGeneral:ClaimSubroSummaryScreen:SubrogationMainDV:SubroClosedOutcome-inputEl']");
		update = By.id("SubrogationGeneral:ClaimSubroSummaryScreen:Update-btnInnerEl");
		edit = By.id("SubrogationGeneral:ClaimSubroSummaryScreen:Edit-btnInnerEl");
		assignBtn = By.id("SubrogationGeneral:ClaimSubroSummaryScreen:ActivityDetailScreen_AssignButton-btnInnerEl");
		subrogationStatus = By.xpath("//div[@id='SubrogationGeneral:ClaimSubroSummaryScreen:SubrogationMainDV:SubrogationStatus-inputEl']");
		financialsPage = By.xpath("//td[@id='Claim:MenuLinks:Claim_ClaimSubrogationGroup:ClaimSubrogationGroup_SubrogationFinancials']//span");
		editFinancials = By.id("SubrogationFinancials:ClaimSubroFinancialsScreen:Edit-btnInnerEl");
		updateFinancials = By.id("SubrogationFinancials:ClaimSubroFinancialsScreen:Update-btnInnerEl");
	}

}
