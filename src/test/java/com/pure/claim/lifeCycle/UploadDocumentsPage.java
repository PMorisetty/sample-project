package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class UploadDocumentsPage {

	static By NewDocumentTittle,
	expandCollapseBtn, uploadBrouswebtn,
	documentName, shareOnPortal, description,typeDoc, updateBtn;
	static{
		NewDocumentTittle = By.xpath("//span[contains(@id,'NewDocumentLinkedScreen:ttlBar')]");
		expandCollapseBtn = By.id("southPanel-splitter-collapseEl");
		uploadBrouswebtn = By.xpath("//input[@name='fileContent']");
		documentName = By.xpath("//input[contains(@id,'NewDocumentName-inputEl')]");
		shareOnPortal = By.xpath("//input[contains(@id,'showOnPortalType-inputEl')]");
		description = By.xpath("//input[contains(@id,'Description-inputEl')]");		
		typeDoc = By.xpath("//input[contains(@id,'TypeKeyInput-inputEl')]");
		updateBtn = By.xpath("//span[contains(@id,'Update-btnInnerEl')]");
	}
}
