package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.SearchAddressBookPageLib;
import com.pure.report.CReporter;

public class LitigationPageLib extends ActionEngine {
	
	public LitigationPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable {
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void openLitigationTab() throws Throwable {
		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("Litigation");
	}
	
	public void typeLitigationName(String litigationName) throws Throwable{
		type(LitigationPage.Name, litigationName, "LitigationName");
		type(LitigationPage.Name, Keys.TAB, "Tab key");
	}
	
	public void typeLitigationType(String litigationType) throws Throwable{
		type(LitigationPage.LitigationType, litigationType, "LitigationType");
		type(LitigationPage.LitigationType, Keys.TAB, "Tab key");
	}
	
	public void typeState(String state) throws Throwable{
		type(LitigationPage.State, state, "state");
		type(LitigationPage.State, Keys.TAB, "Tab key");
	}
	
	public void clickTrailVenueIcon() throws Throwable{
		click(LitigationPage.TrailVenueIcon, "TrailVenueIcon");
	}
	
	public void selectLegalVenue() throws Throwable{
		click(LitigationPage.LegalVenueSelect, "LegalVenueSelect");
	}
	
	public void typeLegalVenueName(String legalVenueName) throws Throwable{
		type(LitigationPage.LegalVenueName, legalVenueName, "LegalVenueName");
		type(LitigationPage.LegalVenueName, Keys.TAB, "Tab key");
	}
	
	public void typeLegalVenueZip(String LegalVenueZip) throws Throwable{
		type(LitigationPage.LegalVenueZip, LegalVenueZip, "LegalVenueZip");
		type(LitigationPage.LegalVenueZip, Keys.TAB, "Tab key");
	}
	
	public void typeLegalVenueAddress1(String legalVenueAddress1) throws Throwable{
		type(LitigationPage.LegalVenueAddress1, legalVenueAddress1, "LegalVenueAddress1");
		type(LitigationPage.LegalVenueAddress1, Keys.TAB, "Tab key");
	}
	
	public void clickLegalVenueOKbtn() throws Throwable{
		click(LitigationPage.LegalVenueOKbtn, "LegalVenueOKbtn");
	}
	
	public void typeLawSuiteType(String lawSuiteType) throws Throwable{
		type(LitigationPage.LawSuiteType, lawSuiteType, "LawSuiteType");
		type(LitigationPage.LawSuiteType, Keys.TAB, "Tab key");
	}
	
	public void typeDateReported(String dateReported) throws Throwable{
		type(LitigationPage.DateReported, dateReported, "DateReported");
		type(LitigationPage.DateReported, Keys.TAB, "Tab key");
	}
	
	public void typeDefendent(String defendent) throws Throwable{
		type(LitigationPage.Defendent, defendent, "Defendent");
		type(LitigationPage.Defendent, Keys.TAB, "Tab key");
	}
	
	public void clickDefenseLawFirmIcon() throws Throwable{
		click(LitigationPage.DefenseLawFirmIcon, "DefenseLawFirmIcon");
	}
	
	public void clickSearchAddressBook_Def() throws Throwable{
		click(LitigationPage.SearchAddressBook_def, "SearchAddressBook_def");
	}
	
	public void clickPlaintiffIcon() throws Throwable{
		click(LitigationPage.PlaintiffIcon, "PlaintiffIcon");
	}
	
	public void typePlaintiff(String plaintiff) throws Throwable{
		type(LitigationPage.Plaintiff, plaintiff, "Plaintiff");
		type(LitigationPage.Plaintiff, Keys.TAB, "Tab key");
	}
	
	public void clickSearchAddressBook_PTiff() throws Throwable{
		click(LitigationPage.SearchAddressBook_PTiff, "SearchAddressBook_PTiff");
	}
	
	public void clickUpdateBtn() throws Throwable{
		click(LitigationPage.UpdateBtn, "UpdateBtn");
		if(isVisibleOnly(LitigationPage.ClearValidationAlert, "ClearValidationAlert")){
			clickClearValidationAlert();
			clickUpdateBtn();
		}
	}
	
	public void clickClearValidationAlert() throws Throwable{
		click(LitigationPage.ClearValidationAlert, "ClearValidationAlert");
	}
	
	public void clickSummarySideMenu() throws Throwable{
		click(ClaimsTree.Summary, "Summary");
	}
	
	public void clickLitigationSideMenu() throws Throwable{
		click(ClaimsTree.Litigation, "Litigation Side Menu");
	}
	
	public void selectLitiagtion(String litigationName) throws Throwable{
		String locator = xpathUpdator(LitigationPage.SelectLitigationName, litigationName);
		click(By.xpath(locator), "litigationName");
	}
	
	public void clickEditBtn() throws Throwable{
		click(ClaimsTree.EditBtn, "EditBtn");
	}
	
	public void VerifyLitigationNameUpdated(String VerifyLitigationNameUpdated) throws Throwable{
		String locator = xpathUpdator(LitigationPage.VerifyLitigationNameUpdated, VerifyLitigationNameUpdated);
		isElementPresent(By.xpath(locator), "UpdatedLitigationName", true);
	}
	
	public void verifyLitigationCreated(String LitigationNameCreated) throws Throwable{
		String locator = xpathUpdator(LitigationPage.VerifyLitigationCreated, LitigationNameCreated);
		isElementPresent(By.xpath(locator), "LitigationCreated", true);
	}
	
	public void clickAssignBtn() throws Throwable{
		click(LitigationPage.AssignBtn, "AssignBtn");
	}
	
	public void selectAssignTo(String assigneeName) throws Throwable{
		type(AssignActivitiesPage.selectAssigneeFromList, assigneeName, "selectAssigneeFromList");
		type(AssignActivitiesPage.selectAssigneeFromList, Keys.TAB, "TAB");
	}
	
	public void clickAssignToBtn() throws Throwable{
		click(AssignActivitiesPage.assignBtn, "assignToBtn");
		if(isVisibleOnly(LitigationPage.ClearValidationAlert, "ClearValidationAlert")){
			clickClearValidationAlert();
			clickAssignToBtn();
		}
	}
	
	public void VerifyAssigneeName(String assigneeName) throws Throwable{
		String assigneeNameDisplayed = getText(LitigationPage.VerifyAssignedTo, "VerifyAssignedTo");
		if(assigneeName.contains(assigneeNameDisplayed)) {
			reporter.SuccessReport("Verify if Assinee Name is updated", "Assignee Name is updated Successfully on Litigation Page");
		}
		else {
			reporter.failureReport("Verify if Assinee Name is updated", "Assignee Name is not updated on Litigation Page");
		}
	}
	
	public void clickCloseBtn() throws Throwable{
		click(LitigationPage.CloseBtn, "CloseBtn");
	}
	
	public void fillAllGeneralDetails(Hashtable<String, String> data) throws Throwable{
		typeLitigationName(data.get("litigationName"));
		typeLitigationType(data.get("litigationType"));
		typeState(data.get("state"));
		clickTrailVenueIcon();
		selectLegalVenue();
		typeLegalVenueName(data.get("legalVenueName"));
		typeLegalVenueAddress1(data.get("legalVenueAddress1"));
		typeLegalVenueZip(data.get("LegalVenueZip"));
		clickLegalVenueOKbtn();
	}
	
	public void fillAllLitigationDetails(Hashtable<String, String> data) throws Throwable{
		typeLawSuiteType(data.get("lawSuiteType"));
		typeDateReported(data.get("dateReported"));
	}
	
	public void fillAllDefenceDetails(Hashtable<String, String> data) throws Throwable{
		typeDefendent(data.get("defendent"));
		clickDefenseLawFirmIcon();
		clickSearchAddressBook_Def();
		SearchAddressBookPageLib addressBookPageLib = new SearchAddressBookPageLib(driver, reporter);
		addressBookPageLib.setServiceState(data.get("def_serviceState"));
		addressBookPageLib.clickSearch();
		addressBookPageLib.clickSelect();
	}
	
	public void fillAllPlaintiffDetails(Hashtable<String, String> data) throws Throwable{
		typePlaintiff(data.get("plaintiff"));
		clickPlaintiffIcon();
		clickSearchAddressBook_PTiff();
		SearchAddressBookPageLib addressBookPageLib = new SearchAddressBookPageLib(driver, reporter);
		addressBookPageLib.setServiceState(data.get("pTiff_serviceState"));
		addressBookPageLib.clickSearch();
		addressBookPageLib.clickSelect();
	}

}
