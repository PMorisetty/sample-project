package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class FinancialsPage {

	static By claimFinancialsSummaryIcon,
	quickCheck,viewFinancialSummaryOf,
	financialTittlebar,totalPaymentsInSummary,
	title, editReserve, createCheck, expensePACAAmount,
	expensePACATransactionAmount, transactionType, validationResultsTab,
	validationMsg, clearValidationBtn, validationMsgsList, paymentStatus, claimLevel;
	static{
		claimFinancialsSummaryIcon  = By.xpath("//img[@id='ClaimFinancialsSummary:ClaimFinancialsSummaryScreen:FinancialsSummaryPanelSet:FinancialsSummaryLV:2:FinancialsSummaryLabel:FinancialsSummaryLabelMenuIcon']");
		quickCheck = By.xpath("//div[contains(@id, 'QuickMenu_QuickCheck')]/a");
		viewFinancialSummaryOf = By.id("ClaimFinancialsSummary:ClaimFinancialsSummaryScreen:FinancialsSummaryRangeInput-inputCell");
		financialTittlebar = By.id("ClaimFinancialsChecks:ClaimFinancialsChecksScreen:ttlBar");
		totalPaymentsInSummary = By.id("ClaimFinancialsSummary:ClaimFinancialsSummaryScreen:FinancialsSummaryPanelSet:FinancialsSummaryLV:0:TotalPayments");		
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		editReserve = By.xpath("//div[contains(@id, 'QuickMenu_EditReserve')]/a");
		createCheck = By.xpath("//div[contains(@id, 'QuickMenu_CreateCheck')]/a");
		expensePACAAmount = By.xpath("//div[text()='Expense - PACA']/../../td[3]//a");
		expensePACATransactionAmount = By.xpath("//div[text()='Expense - PACA']/../..//a[contains(@id,'Amount')]");
		transactionType = By.id("ClaimFinancialsTransactions:ClaimFinancialsTransactionsScreen:TransactionsLVRangeInput-inputEl");
		validationResultsTab = By.id("wsTabBar:wsTab_0-btnInnerEl");
		validationMsg = By.xpath("//div[@class='message'][1]");
		validationMsgsList = By.xpath("//div[@class='message']");
		clearValidationBtn = By.xpath("//span[contains(@id,'ClearButton-btnInnerEl')]");
		paymentStatus = By.xpath("//div[@id='ClaimFinancialsChecks:ClaimFinancialsChecksScreen:ChecksLV-body']//tr/td[6]/div");
		claimLevel = By.xpath("//div[text()='Claim Level']");
	}
}
