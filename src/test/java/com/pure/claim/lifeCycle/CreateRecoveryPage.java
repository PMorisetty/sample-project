package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class CreateRecoveryPage {

	static By updateBtn, payer, reserveLine, recoveryCategory, newLineItem;
	
	static{
		payer = By.xpath("//input[@id='NewRecoverySet:NewRecoveryScreen:RecoveryDetailDV:Payer-inputEl']");
		reserveLine = By.xpath("//input[@id='NewRecoverySet:NewRecoveryScreen:RecoveryDetailDV:ReserveLineInputSet:ReserveLine-inputEl']");
		recoveryCategory = By.xpath("//input[@id='NewRecoverySet:NewRecoveryScreen:RecoveryDetailDV:RecoveryCategory-inputEl']");
		newLineItem = By.xpath("//div[contains(@id,'EditableRecoveryLineItemsLV-body')]");
		updateBtn = By.xpath("//span[@id='NewRecoverySet:NewRecoveryScreen:Update-btnInnerEl']");
	}
}
