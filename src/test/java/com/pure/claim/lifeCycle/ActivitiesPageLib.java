package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ActivitiesPageLib extends ActionEngine{
	
	public ActivitiesPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void filterActivities(String activity) throws Throwable{
		/*activity=any of the below text
		 * 1. "My activities today"
		 * 2. "Due within 7 days"
		 * 3. "All open external"
		 * 4. "Closed in last 30 days"
		 * 5. "All open"
		 * 6. "Overdue only"
		 */
		type(ActivitiesPage.activitiesFilter, activity, "Activity");
		type(ActivitiesPage.activitiesFilter, Keys.TAB, "TAB");
		
	}
	
	public String getActivityClaim() throws Throwable
	{
		return getText(ActivitiesPage.firstActivityClaim, "First Activity Claim Number").trim();
	}
	
	public boolean verifyPresenceOfActivityClaim(String claim) throws Throwable{
		By claimNumber = By.xpath("//div[contains(@id, 'DesktopActivitiesLV-body')]//td[7]/div/a[text()='"+claim+"']");
		return verifyElementPresent(claimNumber, "First activity Claim Number:"+claim, false);
	}
	
	public void sortRecentActivity(String claimNo) throws Throwable{
		scroll(ActivitiesPage.dueHeader, "Due Header");
		click(ActivitiesPage.lastPageIcon, "Last Page Icon");
		click(ActivitiesPage.dueHeader, "Due Header");
		By cliamActivity = By.xpath("//div[contains(@id, 'DesktopActivitiesLV-body')]//td[7]/div/a[text()='"+claimNo+"']/../../preceding-sibling::td[6]/div");
		if(!isElementPresent(cliamActivity, "Claim activity"))
		{
			click(ActivitiesPage.previousPageIcon, "Previous Page Icon");
		}
		else if(!isElementPresent(cliamActivity, "Claim activity"))
		{
			click(ActivitiesPage.previousPageIcon, "Previous Page Icon");
		}
		else if(!isElementPresent(cliamActivity, "Claim activity"))
		{
			click(ActivitiesPage.previousPageIcon, "Previous Page Icon");
		}
	}
	
	public void selectActivity(String claimNo) throws Throwable{
		By cliamActivity = By.xpath("//div[contains(@id, 'DesktopActivitiesLV-body')]//td[7]/div/a[text()='"+claimNo+"']/../../preceding-sibling::td[6]/div");
		scroll(cliamActivity, "Claim" +claimNo+ "Activity");
		click(cliamActivity, "Claim" +claimNo+ "Activity");
	}
	
	public void clickOnSubjectLinkOfActivity(String claimNo) throws Throwable{
		By subjectLink = By.xpath("//div[contains(@id, 'DesktopActivitiesLV-body')]//td[7]/div/a[text()='"+claimNo+"']/../../preceding-sibling::td[1]/div/a");
		scroll(subjectLink, "Claim" +claimNo+ "Subject");
		click(subjectLink, "Claim" +claimNo+ "Subject");
	}
	
	public void clickAssign() throws Throwable{
		scroll(ActivitiesPage.assignBtn, "Assign Button");
		click(ActivitiesPage.assignBtn, "Assign Button");
	}
	
	public void clickComplete() throws Throwable{
		scroll(ActivitiesPage.completeBtn, "Complete Button");
		click(ActivitiesPage.completeBtn, "Complete Button");
	}
	
	public void selectActivityBasedOnInvoice(String invoiceName) throws Throwable{
		By approveRow = By.xpath("//a[contains(text(),'"+invoiceName+"')]");
		scroll(approveRow, invoiceName);
		click(approveRow, invoiceName);
	}
}
