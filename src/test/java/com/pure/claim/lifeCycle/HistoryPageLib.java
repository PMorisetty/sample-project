package com.pure.claim.lifeCycle;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ServicesPage;
import com.pure.report.CReporter;

public class HistoryPageLib extends ActionEngine {
	
	public HistoryPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable {
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void clickHistorySideMenu() throws Throwable{
		click(ClaimsTree.History, "History Side Menu");
	}
	
	public void VerifyType(String type) throws Throwable{
		String typeDisplayed = getText(HistoryPage.Type, "Type");
		if(typeDisplayed.contains(type)) {
			reporter.SuccessReport("Verify Type in History table is updated", "Type is updated Successfully in History table");
		}
		else {
			reporter.failureReport("Verify Type in History table is updated", "Type is not updated in History table");
		}
	}

	public void VerifyRelatedTo(String relatedTo) throws Throwable{
		String RelatedToDisplayed = getText(HistoryPage.RelatedTo, "RelatedTo");
		if(RelatedToDisplayed.contains(relatedTo)) {
			reporter.SuccessReport("Verify RelatedTo in History table is updated", "RelatedTo is updated Successfully in History table");
		}
		else {
			reporter.failureReport("Verify RelatedTo in History table is updated", "RelatedTo is not updated in History table");
		}
	}
	
	public void VerifyUser(String user) throws Throwable{
		String UserDisplayed = getText(HistoryPage.User, "User");
		if(UserDisplayed.contains(user)) {
			reporter.SuccessReport("Verify User in History table is updated", "User is updated Successfully in History table");
		}
		else {
			reporter.failureReport("Verify User in History table is updated", "User is not updated in History table");
		}
	}
	
	public void VerifyDescription(String description) throws Throwable{
		String descriptionDisplayed = getText(HistoryPage.Description, "Description");
		if(descriptionDisplayed.contains(description)) {
			reporter.SuccessReport("Verify description in History table is updated", "description is updated Successfully in History table");
		}
		else {
			reporter.failureReport("Verify description in History table is updated", "description is not updated in History table");
		}
	}
	
	public String getTypeText() throws Throwable{
		waitForVisibilityOfElement(HistoryPage.Type, "Type");
		String type = driver.findElement(HistoryPage.Type).getText();
		
		return type;
	}
}
