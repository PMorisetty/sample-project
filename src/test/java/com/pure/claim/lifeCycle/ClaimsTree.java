package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class ClaimsTree {
	static By Summary,Overview,Status,HealthMetrics,MemberPortal,Workplan,
	LossDetails,Exposures,PartiesInvolved,Policy,Financials,Notes,Documents,
	PlanOfAction,Services,Litigation,History,FNOLSnapshot,LossDetailsFNOLSnapshot,
	Calendar,checks,transactions, EditBtn, secondaryApproval;

	static{		

		Summary = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Summary']");
		Overview = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Overview']");
		Status = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Status']");
		HealthMetrics = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Health Metrics']");
		MemberPortal = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Member Portal']");
		Workplan = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Workplan']");
		LossDetails = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Loss Details']");
		Exposures = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Exposures']");
		PartiesInvolved = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Parties Involved']");
		Policy = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Policy']");
		Financials = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Financials']");
		checks = By.xpath("//span[text()='Checks']");
		transactions = By.xpath("//span[text()='Transactions']");
		Notes = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Notes']");
		Documents = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Documents']");
		PlanOfAction = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Plan of Action']");
		Services = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Services']");
		Litigation = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Litigation']");
		History = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='History']");
		FNOLSnapshot = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='FNOL Snapshot']");
		Calendar = By.xpath("//div[@id='Claim:MenuLinks']//span[text()='Calendar']");
		LossDetailsFNOLSnapshot = By.id("Claim:MenuLinks:Claim_ClaimSnapshotGroup:ClaimSnapshotGroup_ClaimSnapshotLossDetails");
		EditBtn = By.xpath("//span[@id='MatterDetailPage:MatterDetailScreen:Edit-btnInnerEl']");
		secondaryApproval = By.xpath("//span[contains(text(),'Secondary Approval')]");
		
	}
}
