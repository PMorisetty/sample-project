package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CloseLitigationPageLib extends ActionEngine {
	
	public CloseLitigationPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable {
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void typeNote(String note) throws Throwable{
		type(CloseLitigationPage.Note, note, "Note");
		type(CloseLitigationPage.Note, Keys.TAB, "Tab key");
	}
	
	public void typeResolution(String resolution) throws Throwable{
		type(CloseLitigationPage.Resolution, resolution, "Resolution");
		type(CloseLitigationPage.Resolution, Keys.TAB, "Tab key");
	}

	public void clickCloseLitigationBtn() throws Throwable{
		click(CloseLitigationPage.CloseLitigationBtn, "CloseLitigationBtn");
		if(isVisibleOnly(LitigationPage.ClearValidationAlert, "ClearValidationAlert")){
			new LitigationPageLib(driver, reporter).clickClearValidationAlert();;
			clickCloseLitigationBtn();
		}
	}
	
	public void clickCancelBtn() throws Throwable{
		click(CloseLitigationPage.CancelBtn, "CancelBtn");
	}
	
}
