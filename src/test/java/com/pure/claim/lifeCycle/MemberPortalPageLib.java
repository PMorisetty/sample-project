package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class MemberPortalPageLib extends ActionEngine {

	public MemberPortalPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable {
		this.driver = driver;
		this.reporter = reporter;
		openMemberPortal();
	}

	public void openMemberPortal() throws Throwable {
		click(MemberPortalPage.MemberPortalTreeItem, "MemberPortalTreeItem");
	}

	public void clickEditBtn() throws Throwable {
		click(MemberPortalPage.EditBtn, "EditBtn");
	}

	public void clickUpdateBtn() throws Throwable {
		click(MemberPortalPage.UpdateBtn, "UpdateBtn");
	}

	public String getPortalStatus() throws Throwable {
		return getText(MemberPortalPage.PortalStatus, "PortalStatus");
	}

	public void verifyPortalStatus(String portalStatus) throws Throwable {
		String updatedPortalStatus = getPortalStatus();
		if (portalStatus.equalsIgnoreCase(updatedPortalStatus)) {
			reporter.SuccessReport("Portal Status is updated Successfully to " + updatedPortalStatus, "PortalStatus");
		} else {
			reporter.failureReport("Portal Status is not updated to " + updatedPortalStatus, "PortalStatus", driver);
		}
	}

	public void selectPortalStatus(String portalStatus) throws Throwable {
		click(MemberPortalPage.PortalStatusDropDownArrow, "PortalStatusDropDownArrow");
		String PortalStatusDropDown = xpathUpdator(MemberPortalPage.PortalStatusDropDown, portalStatus);
		click(By.xpath(PortalStatusDropDown), "PortalStatusDropDown");
		clickUpdateBtn();
	}

	public void verifyMemberPortalInfoPage() throws Throwable {
		waitForVisibilityOfElement(MemberPortalPage.EditBtn, "EditBtn");
		waitForVisibilityOfElement(MemberPortalPage.ShowAllRowsBtn, "ShowAllRowsBtn");
		waitForVisibilityOfElement(MemberPortalPage.ReassignBtn, "ReassignBtn");
		waitForVisibilityOfElement(MemberPortalPage.MemberPortalInfoLabel, "MemberPortalInfoLabel");
		waitForVisibilityOfElement(MemberPortalPage.MemberAdvocateLabel, "MemberAdvocateLabel");
		waitForVisibilityOfElement(MemberPortalPage.InfoToBeSharedLabel, "InfoToBeSharedLabel");
	}
}
