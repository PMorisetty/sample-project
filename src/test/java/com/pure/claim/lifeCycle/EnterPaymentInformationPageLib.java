package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class EnterPaymentInformationPageLib extends ActionEngine{
	
	public EnterPaymentInformationPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void enterReserveLine(String reserveLineValue) throws Throwable{
		type(EnterPaymentInformationPage.reserveLine, reserveLineValue, "reserveLine");
		type(EnterPaymentInformationPage.reserveLine, Keys.TAB, "Tab key");
	}
	
	public void enterPaymentType(String paymentType) throws Throwable{
		type(EnterPaymentInformationPage.paymentType, paymentType, "paymentType");
		type(EnterPaymentInformationPage.paymentType, Keys.TAB, "Tab key");
	}
	
	public void enterPaymentAmount(String paymentAmount) throws Throwable{
		Actions actions = new Actions(driver);
		actions.doubleClick(driver.findElement(EnterPaymentInformationPage.paymentAmountCell)).perform();
		type(EnterPaymentInformationPage.paymentAmount,paymentAmount,"paymentAmount");
		waitForMask();
		type(EnterPaymentInformationPage.paymentAmount, Keys.TAB, "Tab");
	}
	
	public void clickNext() throws Throwable{
		click(EnterPaymentInformationPage.nextBtn, "nextBtn");
	}
	
	public void fillDetails(String reserveLine, String paymentType, String paymentAmount) throws Throwable{
		enterReserveLine(reserveLine);
		enterPaymentType(paymentType);
		enterPaymentAmount(paymentAmount);
	}
	
	public void setDeductibleType(String deductibletype) throws Throwable{
		if(deductibletype.equalsIgnoreCase("AOP")){
			click(EnterPaymentInformationPage.deductibleTypeAOP,"deductibleTypeAOP");
		}else{
			click(EnterPaymentInformationPage.deductibleTypeSpecial,"deductibleTypeSpecial");
		}
	}
	
	public void verifyDeductibleDecisionWhenOverrideDeductibleYesNoOptionsAreDisplayed(String WaivedOrApplied, String waivedReason, String otherComments) throws Throwable{		
		if(WaivedOrApplied.equalsIgnoreCase("Applied")){			
			click(EnterPaymentInformationPage.deductibleDecisionApplied,"WaivedOrApplied");
			verifyDeductibleAmountAndOverrideDeductibleIsSetToNo();
		}else{
			click(EnterPaymentInformationPage.deductibleDecisionWaived,"WaivedOrApplied");
			type(EnterPaymentInformationPage.waivedReason,waivedReason, "waivedReason");
			type(EnterPaymentInformationPage.waivedReason,Keys.TAB, "tab key");
			type(EnterPaymentInformationPage.otherComments,otherComments, "otherComments");
			type(EnterPaymentInformationPage.otherComments,Keys.TAB, "tab key");
		}
	}
	
	public void verifyDeductibleAmountAndOverrideDeductibleIsSetToNo() throws Throwable{
		waitForVisibilityOfElement(EnterPaymentInformationPage.overrideDeductibleYes, "overrideDeductibleYes");
		String actualClass = getAttributeByClass(EnterPaymentInformationPage.overrideDeductibleNoParentTag, "overrideDeductibleNoParentTag");
		String expectedClass = "x-form-cb-checked";
		assertTextStringContains(actualClass, expectedClass);
		boolean flag = false;
		WebElement deductibleAmount = null;
		try{
			deductibleAmount = driver.findElement(EnterPaymentInformationPage.deductibleAmount);
			flag = deductibleAmount.getText().length()>0;
		}catch(Exception e){}
		if(flag){
			reporter.SuccessReport("Deductible Amount lengtht", "Deductible Amount exists");
		}else{
			reporter.failureReport("Deductible Amount lengtht", "Deductible Amount does not exists", driver);
		}
	}

}
