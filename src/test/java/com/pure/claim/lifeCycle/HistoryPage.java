package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class HistoryPage {

	static By Type, RelatedTo, User, EventTimeStamp, Description;
	
	static{
		Type = By.xpath("//div[@id='ClaimHistory:ClaimHistoryScreen:HistoryLV-body']//tr[1]/td[1]/div");
		RelatedTo = By.xpath("//div[@id='ClaimHistory:ClaimHistoryScreen:HistoryLV-body']//tr[1]/td[2]/div");
		User = By.xpath("//div[@id='ClaimHistory:ClaimHistoryScreen:HistoryLV-body']//tr[1]/td[3]/div");
		EventTimeStamp = By.xpath("//div[@id='ClaimHistory:ClaimHistoryScreen:HistoryLV-body']//tr[1]/td[4]/div");
		Description = By.xpath("//div[@id='ClaimHistory:ClaimHistoryScreen:HistoryLV-body']//tr[1]/td[5]/div");
	}
}
