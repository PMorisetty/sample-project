package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class VehicleIncidentPage {
	public static By vehicleIncidentHeader;
	public static By damageDescription;
	public static By driverName;
	public static By relationToOwner;
	public static By lossOccured;
	public static By okBtn;
	public static By selectVehicle, lossParty, year, make, cccMakeCode, model,
	airBagsDeployedYes, airBagsDeployedNo, operableYes, operableNo, injuries, primaryDamage,secondaryDamage, 
	differentInsuredAddressYes, differentInsuredAddressNo, addressLine1, addressLine2, city, zip, zipIcon,
	shopOfChoiceNo, shopOfChoiceYes, evaluateBtn, party, totalLossYes, totalLossNo;
	
	static{		
		vehicleIncidentHeader = By.id("EditVehicleIncidentPopup:EditVehicleIncidentScreen:0");
		damageDescription = By.xpath("//textarea[contains(@id,'VehIncidentDetailDV:VehicleIncidentDV:Description-inputEl')]");
		driverName = By.xpath("//input[contains(@id,'VehIncidentDetailDV:VehicleIncidentDV:Driver_Picker-inputEl')]");
		relationToOwner = By.xpath("//input[contains(@id,'VehicleIncidentDV:RelationToInsured-inputEl')]");
		lossOccured = By.xpath("//input[contains(@id,'VehIncidentDetailDV:VehicleIncidentDV:LossOccured-inputEl')]");
		okBtn = By.xpath("//span[text()='OK']");
		selectVehicle = By.xpath("//input[contains(@id,'VehIncidentDetailDV:VehicleIncidentDV:Vehicle_Picker-inputEl')]");
		lossParty = By.xpath("//input[contains(@id,'VehIncidentDetailDV:VehicleIncidentDV:LossParty-inputEl')]");
		year = By.xpath("//input[contains(@id,'VehIncidentDetailDV:VehicleIncidentDV:Vehicle_Year-inputEl')]");
		make = By.xpath("//input[contains(@id,'VehIncidentDetailDV:VehicleIncidentDV:Vehicle_Make-inputEl')]");
		cccMakeCode = By.xpath("//input[contains(@id,'VehIncidentDetailDV:VehicleIncidentDV:Vehicle_VehicleCCCMakeCode-inputEl')]");
		model = By.xpath("//input[contains(@id,'VehIncidentDetailDV:VehicleIncidentDV:Vehicle_Model-inputEl')]");
		airBagsDeployedYes = By.xpath("//input[contains(@id,'VehicleIncidentDV:Exposure_AirbagsDeployed_true-inputEl')]");
		airBagsDeployedNo = By.xpath("//input[contains(@id,'VehicleIncidentDV:Exposure_AirbagsDeployed_false-inputEl')]");
		operableYes = By.xpath("//input[contains(@id,'VehicleIncidentDV:Operable_true-inputEl')]");
		operableNo = By.xpath("//input[contains(@id,'VehicleIncidentDV:Operable_false-inputEl')]");
		injuries = By.xpath("//input[contains(@id,'VehicleIncidentDV:vehicleIncidentType-inputEl')]");
		primaryDamage = By.xpath("//input[contains(@id,'VehicleIncidentDV:PrimaryDamage-inputEl')]");
		differentInsuredAddressYes = By.xpath("//input[contains(@id,'VehicleIncidentDV:VehicleLocationInd_true-inputEl')]");
		differentInsuredAddressNo = By.xpath("//input[contains(@id,'VehicleIncidentDV:VehicleLocationInd_false-inputEl')]");
		addressLine1 = By.xpath("//label[text()='Address 1']/../..//input");
		addressLine2 = By.xpath("//label[text()='Address 2']/../..//input");
		city = By.cssSelector("[id*=\"City-inputEl\"]");
		zip = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//input");
		zipIcon = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//a");
		shopOfChoiceNo = By.xpath("//input[contains(@id,'VehicleIncidentDV:ShopOfChoice_false-inputEl')]");
		shopOfChoiceYes = By.xpath("//input[contains(@id,'VehicleIncidentDV:ShopOfChoice_true-inputEl')]");
		evaluateBtn = By.xpath("//a[contains(@id,'VehicleIncidentDV:EvaluateId')]");
		party = By.xpath("//input[contains(@id,'VehicleIncidentDV:LocationContactInput1-inputEl')]");
		totalLossYes = By.xpath("//input[contains(@id,'VehicleIncidentDV:TotalLoss_true-inputEl')]");
		totalLossNo = By.xpath("//input[contains(@id,'VehicleIncidentDV:TotalLoss_false-inputEl')]");
		
		secondaryDamage = By.xpath("//input[contains(@id,'VehicleIncidentDV:SecondaryDamage-inputEl')]");
		
	}
}
