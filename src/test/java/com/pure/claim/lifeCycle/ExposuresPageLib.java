package com.pure.claim.lifeCycle;


import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ExposuresPageLib extends ActionEngine{
	
	public ExposuresPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable{
		this.driver = driver;
		this.reporter = reporter;
		new SummaryOverviewPageLib(driver,reporter).openExposuresPage();
	}

	public void openExposures() throws Throwable{
		click(ExposuresPage.ExposuresTreeItem,"ExposuresTreeItem");
		waitForVisibilityOfElement(ExposuresPage.AssignBtn, "AssignBtn");
	}
	
	
	//Will return column w.r.t. last row in table
	private List<WebElement> getColumns() throws Throwable{
		waitForVisibilityOfElement(ExposuresPage.exposuresDiv, "exposuresDiv");
		List<WebElement> tableRows = null;
		List<WebElement> tableColumns = null;
		boolean flag = false;
		try{
			tableRows = driver.findElement(ExposuresPage.exposuresDiv).findElement(By.tagName("table")).findElements(By.tagName("tr"));
			tableColumns = tableRows.get(tableRows.size() - 1).findElements(By.tagName("td"));
			flag = true;
		}catch(Exception e){
			flag = false;
		}
		finally{
			if(!flag){
				reporter.failureReport("Find division tag for the element - "+ExposuresPage.exposuresDiv, "unable to find the Division tag for "+ExposuresPage.exposuresDiv, driver);
			}
		}
		return tableColumns;
	}
	
	
	public void verifyExposureAddedType(String expectedType) throws Throwable{
		WebElement elem = getColumns().get(2); //2-> selecting the coverage type
		String actualType = elem.getText();
		assertTextStringMatching(actualType, expectedType);
	}
	
	//overloaded with verifyExposureStatus(String, String)
	public void verifyExposureStatus(String expectedStatus) throws Throwable{
		WebElement elem = getColumns().get(7); //7-> selecting the coverage status
		String actualStatus = elem.getText();
		assertTextStringMatching(actualStatus, expectedStatus);	
	}
	
	//overloaded with verifyExposureStatus(String) 
	public void verifyExposureStatus(String coverage, String expectedStatus) throws Throwable{
		By statusBy = By.xpath("//div[text()='"+coverage+"']/../../td[8]");
		assertText(statusBy, expectedStatus);
	}
	
	public void verifyExposureSubType(String expectedSubType) throws Throwable{
		WebElement elem = getColumns().get(4); //4->selecting the coverage subtype
		String actualSubType = elem.getText();
		assertTextStringMatching(actualSubType, expectedSubType);	
	}
	
	public void closeExposure(Hashtable<String, String> data) throws Throwable{
		WebElement elem = getColumns().get(0);// selecting the first exposure listed
		elem.click();
		click(ExposuresPage.closeExposureBtn,"Close Exposure");
		CloseExposurePageLib closeExposurePageLib = new CloseExposurePageLib(driver, reporter);
		closeExposurePageLib.fillDetails(data);
		closeExposurePageLib.clickClose();
	}

	public void selectExposure() throws Throwable{
		click(ExposuresPage.selectExposureCheckBox,"select checkBox for exposure");// selecting the first exposure listed
	}
	
	/* This is for handling multiple exposures addition in one claim
	it is under development 
	public void selectCoverageType(String coverageType) throws Throwable{
		ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ChooseByCoverage");
		
		switch (coverageType) {
			case "Policy_Cyber_Attack":
				actionsPageLib.clickOn("policyLevelCoverage");
				actionsPageLib.clickOn("fraudAndCyber");
				actionsPageLib.clickOn("cyberAttack");
				break;

			case "Policy_Cyber_Extortion":
				actionsPageLib.clickOn("policyLevelCoverage");
				actionsPageLib.clickOn("fraudAndCyber");
				actionsPageLib.clickOn("cyberExtortion");
				break;
				
			case "Policy_Fraud":
				actionsPageLib.clickOn("policyLevelCoverage");
				actionsPageLib.clickOn("fraudAndCyber");
				actionsPageLib.clickOn("fraud");
				break;
	
			default:
				break;
		}
	}*/
	
	public void selectExposureBasedOnCoverage(String coverage) throws Throwable{
		By exposureCheckBox = By.xpath("//div[text()='"+coverage+"']/../preceding-sibling::td[3]/div/img");
		click(exposureCheckBox,"select checkBox for exposure");
	}
	
	public void openExposureBasedOnCoverage(String coverage) throws Throwable{
		By exposure = By.xpath("//div[text()='"+coverage+"']/../preceding-sibling::td[2]/div/a");
		click(exposure,"select exposure for "+coverage);
	}
	
	public void clickCreateReserve() throws Throwable{
		click(ExposuresPage.createReserveBtn,"Create Reserve Button");
	}
	
	public void clickExposureType() throws Throwable{
		click(ExposuresPage.exposureType, "exposureType");
	}
	
	public void clickCloseExposure() throws Throwable{
		click(ExposuresPage.closeExposureBtn, "closeExposureBtn");
	}
	
	// 1st Party Vehicle Page
	public void clickVehicleSalvage() throws Throwable{
		click(ExposuresPage.vehicleSalvage, "Vehicle Salvage");
	}
	
	public void selectSalvageService(String salvageService) throws Throwable{
		type(ExposuresPage.vehicleSalvage, salvageService , "Salvage Service");
		type(ExposuresPage.vehicleSalvage, Keys.TAB , "Tab Key");
	}

	public void clickUpdateBtn() throws Throwable{
		click(ExposuresPage.updateBtn, "Update Btn");
	}
}
