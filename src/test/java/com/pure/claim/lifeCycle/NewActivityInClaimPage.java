package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class NewActivityInClaimPage {
	
	static By subject, description, updateBtn, escalationDate, cancelBtn;
	
	static {
		
		subject = By.xpath("//input[contains(@id, 'Subject-inputEl')]");
		description = By.xpath("//textarea[contains(@id, 'Activity_Description-inputEl')]");
		escalationDate = By.xpath("//input[contains(@id, 'EscalationDate-inputEl')]");
		updateBtn = By.xpath("//span[contains(@id, 'UpdateButton-btnInnerEl')]");
		cancelBtn = By.xpath("//span[contains(@id, 'CancelButton-btnInnerEl')]");
	}

}
