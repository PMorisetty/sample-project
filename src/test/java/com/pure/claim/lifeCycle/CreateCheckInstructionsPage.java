package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class CreateCheckInstructionsPage {

	static By shareOnMemberPortal,deliveryMethod,
	AdvancedDirectPayRadioBtn,AdvancedDirectPayExplaination;
	static{
		shareOnMemberPortal = By.xpath("//input[contains(@id, 'showOnPortalType-inputEl')]");
		deliveryMethod = By.xpath("//input[contains(@id, 'CheckDeliveryMethod')]");
		AdvancedDirectPayRadioBtn = By.xpath("//input[contains(@id, 'AdvancedRadioInput_true')]");
		AdvancedDirectPayExplaination = By.xpath("//textarea[contains(@id, 'PaymentNote')]");
		
	}
}