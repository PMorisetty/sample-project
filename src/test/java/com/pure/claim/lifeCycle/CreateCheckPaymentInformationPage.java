package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class CreateCheckPaymentInformationPage {

	static By deductibleDecisionWaived, deductibleDecisionApplied,
	paymentAmountCell, paymentAmount, waivedReason,otherComments, overrideDeductibleYes, 
	overrideDeductibleNo, deduductibleAmount, overrideReason,
	aopDeductibleType, specialDeductibleType;
	
	static{
		deductibleDecisionWaived = By.xpath("//input[contains(@id, 'DeductibleWaived_true')]");
		deductibleDecisionApplied = By.xpath("//input[contains(@id, 'DeductibleWaived_false')]");
		paymentAmountCell = By.xpath("//div[contains(@id, 'EditablePaymentLineItemsLV-body')]//tbody//td[3]/div");
		paymentAmount = By.xpath("//input[@name='Amount']");
		waivedReason = By.xpath("//input[contains(@id, 'Waive_Reason')]");
		otherComments = By.xpath("//input[contains(@id, 'Waived_OtherReason')]");
		overrideDeductibleYes = By.xpath("//input[contains(@id,'Deductible_Override_true-inputEl')]");
		overrideDeductibleNo = By.xpath("//input[contains(@id,'Deductible_Override_false-inputEl')]");
		deduductibleAmount = By.xpath("//input[contains(@id,'Deductible_amount-inputEl')]");
		overrideReason = By.xpath("//input[contains(@id,'Override_OtherReason-inputEl')]");
		aopDeductibleType = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:DeductibleType_true-inputEl')]");
		specialDeductibleType = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:DeductibleType_false-inputEl')]");
	}
}
