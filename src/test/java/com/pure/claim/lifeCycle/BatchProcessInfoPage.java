package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class BatchProcessInfoPage {

	static By automatedBulkInvoiceRunBtn, automatedBulkInvoiceStopBtn, refereshBtn,clusterInfoLink;
	
	static{
		automatedBulkInvoiceRunBtn = By.xpath("//div[text()='Automated Bulk Invoice']/../..//*[contains(@id,'RunBatch')]");
		automatedBulkInvoiceStopBtn = By.xpath("//div[text()='Automated Bulk Invoice']/../..//*[contains(@id,'TerminateBatch')]");
		refereshBtn = By.xpath("//span[text()='Refresh']");
		clusterInfoLink = By.xpath("//span[text()='Cluster Info']");
		
	}
}
