package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class SummaryOverviewPage {

	static By trExposures,summaryTree,latestNotesBody,notesTree,
	paidFinancials, title, policyTree, lossDetailsTree, CCLDRRLink, notesList, nextBtn,
	viewDocumentsLink, exposuresTree, partiesInvolvedTree, exposuresDiv, adjusterName, subrogationTree,
	DocumentsTree, policyLocationsTree, financialsTree, checksFinancials;
	
	static By lossDate,noticeDate,lossLocation,HistoryTree;
	
	
	static{
		trExposures = By.xpath("//div[@id='ClaimSummary:ClaimSummaryScreen:ClaimSummaryExposuresLV-body']//tr");
		exposuresTree = By.xpath("//span[contains(@class,'tree') and text()='Exposures']");
		partiesInvolvedTree = By.xpath("//span[contains(@class,'tree') and text()='Parties Involved']");
		summaryTree = By.xpath("//span[contains(@class,'tree') and text()='Summary']");
		policyTree = By.xpath("//span[contains(@class,'tree') and text()='Policy']");
		policyLocationsTree = By.xpath("//span[contains(@class,'tree') and text()='Locations']");
		lossDetailsTree = By.xpath("//span[contains(@class,'tree') and text()='Loss Details']");
		CCLDRRLink = By.xpath("//span[contains(@class,'tree') and text()='CCLDRR']");
		notesTree = By.xpath("//span[contains(@class,'tree') and text()='Notes']");
		latestNotesBody = By.id("x-form-el-ClaimSummary:ClaimSummaryScreen:NotesLV:0:Body");
		paidFinancials= By.id("ClaimSummary:ClaimSummaryScreen:ClaimSummaryHeadlinePanelSet:Paid-inputEl");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		notesList = By.xpath("//div[contains(@id, 'ClaimSummaryScreen:NotesLV') and contains(@id, 'Body')]");
		nextBtn = By.xpath("//div[@id='ClaimSummary:ClaimSummaryScreen:NotesLV_header']//..//span[contains(@class,'next')]");
		viewDocumentsLink = By.xpath("//a[text()='View Documents']");
		exposuresDiv = By.id("ClaimSummary:ClaimSummaryScreen:ClaimSummaryExposuresLV-body");
		adjusterName = By.xpath("//span[@id='Claim:ClaimInfoBar:Adjuster-btnInnerEl']/span[2]");
		subrogationTree = By.xpath("//span[contains(@class,'tree') and text()='Subrogation']");
		DocumentsTree = By.xpath("//span[contains(@class,'tree') and text()='Documents']");
		financialsTree = By.xpath("//span[contains(@class,'tree') and text()='Financials']");
		checksFinancials = By.xpath("//span[contains(@class,'tree') and text()='Checks']");
		
		lossDate = By.xpath("//label[text()='Loss Date']/ancestor::td[1]/..//div");
		noticeDate = By.xpath("//label[text()='Notice Date']/ancestor::td[1]/..//div");
		lossLocation = By.xpath("//label[text()='Loss Location']/ancestor::td[1]/..//div");
		HistoryTree = By.xpath("//span[contains(@class,'tree') and text()='History']");
	}
}
