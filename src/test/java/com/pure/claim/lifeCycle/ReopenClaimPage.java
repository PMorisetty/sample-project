package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class ReopenClaimPage {

	static By reopenClaim;

	static {
		reopenClaim = By.xpath("//span[@id='ReopenClaimPopup:ReopenClaimScreen:Update-btnInnerEl']");
	}
}
