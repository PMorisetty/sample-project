package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class QuickCheckBasicsPage {

	static By cancelBtn, cancelOkBtn,
	nextBtn,finishBtn,backBtn,
	primaryPayeeName,primaryPayeeType,
	payToTheOrderOf, zip,zipAutoFill,Address1, city,
	deductibleDecisionWaived, deductibleDecisionApplied,
	deductibleAmount, overrideDeductible, overrideDeductibleYes, overrideDeductibleNo, overrideDeductibleNoParentTag,
	paymentAmountFirstCell,categoryFirstCell,categoryOther,lineAmount,
	categoryMileageReimbursement,waivedReason,otherComments, lineCategory,
	deductibleTypeAOP, deductibleTypeSpecial, title, postalCode, province, country,
	duplicateCheckTab, closeButton, paymentMethodCheck, paymentMethodElectronicFundTransfer,
	nameOnAccount, accountTypeChecking, accountTypeSavings, accountTypeOther, accountNo, routingNo;
	
	static{
		cancelBtn 		= By.id("QuickCreateCheckWizard:Cancel-btnInnerEl");
		cancelOkBtn 	= By.xpath("//span[text()='OK']");
		nextBtn 		= By.id("QuickCreateCheckWizard:Next-btnInnerEl");
		finishBtn 		= By.id("QuickCreateCheckWizard:Finish-btnInnerEl");
		backBtn 		= By.id("QuickCreateCheckWizard:Prev-btnInnerEl");
		primaryPayeeName = By.xpath("//input[@id='QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:PrimaryPayee_Name-inputEl']");
		primaryPayeeType = By.xpath("//input[@id='QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:PrimaryPayee_Type-inputEl']");
		payToTheOrderOf = By.xpath("//textarea[@id='QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:PayTo-inputEl']");
		zip 			= By.xpath("//input[@id='QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:CheckAddressDetailInputSet:LossLocation_ZIP-inputEl']");
		zipAutoFill 	= By.id("QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:CheckAddressDetailInputSet:LossLocation_ZIP:AutoFillIcon");
		Address1 		= By.xpath("//input[@id='QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:CheckAddressDetailInputSet:LossLocation_Address1-inputEl']");
		city 			= By.cssSelector("input[id*=\"LossLocation_City-inputEl\"]");
		deductibleDecisionWaived = By.xpath("//input[@id='QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:DeductibleManagementInputSet:DeductibleWaived_true-inputEl']");
		deductibleDecisionApplied= By.xpath("//input[@id='QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:DeductibleManagementInputSet:DeductibleWaived_false-inputEl']");
		deductibleAmount = By.xpath("//label[text()='Deductible Amount']/../..//div");
		overrideDeductible = By.xpath("//label[text()='Override Deductible']/../..//div");
		overrideDeductibleYes = By.xpath("//input[contains(@id,'Deductible_Override_true-inputEl')]");
		overrideDeductibleNo = By.xpath("//input[contains(@id,'Deductible_Override_false-inputEl')]");
		overrideDeductibleNoParentTag = By.xpath("//input[contains(@id,'Deductible_Override_false-inputEl')]/../../../../..");
		paymentAmountFirstCell = By.xpath("//div[@id='QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:EditablePaymentLineItemsLV-body']//tr[@tabindex='0']//td[3]");
		categoryFirstCell = By.xpath("//div[@id='QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:EditablePaymentLineItemsLV-body']//tr[@tabindex='0']//td[2]");
		categoryOther 	= By.xpath("//li[.='Other']");
		categoryMileageReimbursement = By.xpath("//li[.='Mileage reimbursement']");
		waivedReason	= By.xpath("//input[@id='QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:DeductibleManagementInputSet:Waive_Reason-inputEl']");
		otherComments 	= By.xpath("//input[@id='QuickCreateCheckWizard:QuickCheckWizard_QuickCheckBasicsScreen:QuickCheckBasicsDV:DeductibleManagementInputSet:Waived_OtherReason-inputEl']");
		lineCategory 	= By.xpath("//input[@name='LineCategory']");
		lineAmount 		= By.xpath("//input[@name='Amount']");
		deductibleTypeAOP = By.cssSelector("input[id*=\"DeductibleType_true-inputEl\"]");
		deductibleTypeSpecial = By.cssSelector("input[id*=\"DeductibleType_false-inputEl\"]");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		postalCode = By.xpath("//label[text()='Postal Code']/../..//input");
		province = By.xpath("//label[text()='Province']/../..//input");
		country = By.xpath("//label[text()='Country']/../..//input");
		duplicateCheckTab = By.id("wsTabBar:wsTab_0-btnInnerEl");
		closeButton = By.cssSelector("span[id$='DuplicatesWorksheet_CloseButton-btnInnerEl']");
		
		paymentMethodCheck = By.xpath("//input[contains(@id,'PaymentMethod_option1-inputEl')]");
		paymentMethodElectronicFundTransfer = By.xpath("//input[contains(@id,'PaymentMethod_option2-inputEl')]");
		nameOnAccount = By.xpath("//textarea[contains(@id,'EFTDataInputSet:PayTo-inputEl')]");
		accountTypeChecking = By.xpath("//input[contains(@id,'BankAccountType_option1-inputEl')]");
		accountTypeSavings = By.xpath("//input[contains(@id,'BankAccountType_option2-inputEl')]");
		accountTypeOther = By.xpath("//input[contains(@id,'BankAccountType_option3-inputEl')]");
		accountNo = By.xpath("//input[contains(@id,'BankAccountNumber-inputEl')]");
		routingNo = By.xpath("//input[contains(@id,'BankRoutingNumber-inputEl')]");
	}
}
