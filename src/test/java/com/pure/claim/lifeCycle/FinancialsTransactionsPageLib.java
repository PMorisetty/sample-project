package com.pure.claim.lifeCycle;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class FinancialsTransactionsPageLib extends ActionEngine{

	public FinancialsTransactionsPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void verifyReserveTypeAndCostTypeForFedExBulkInvoice(String reserveType, String costType) throws Throwable{
		assertText(FinancialsTransactionsPage.reserveTypeFromBulkInvoice, reserveType);
		assertText(FinancialsTransactionsPage.costTypeFromBulkInvoice, costType);
	}
}
