package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewVendorCompanyPageLib extends ActionEngine{
	
	public NewVendorCompanyPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void setName(String vendorName) throws Throwable{
		type(NewVendorCompanyPage.name, vendorName, "Vendor name");
		type(NewVendorCompanyPage.name, Keys.TAB, "TAB");
	}
	
	public void setShareOnMemberPortal(String shareOnMemberPortal) throws Throwable{
		type(NewVendorCompanyPage.shareOnMemberPortal, shareOnMemberPortal, "Share on Member Portal");
		type(NewVendorCompanyPage.shareOnMemberPortal, Keys.TAB, "TAB");
	}
	
	public void clickAddRole() throws Throwable{
		scroll(NewVendorCompanyPage.addBtn, "Add Role Button");
		click(NewVendorCompanyPage.addBtn, "Add Role Button");
		waitForMask();
	}
	
	public void setRole(String role) throws Throwable{ 
		try {
			//waitForVisibilityOfElement(NewVendorCompanyPage.role, "role");
			WebElement ele = driver.findElement(NewVendorCompanyPage.role);
			Actions actions = new Actions(driver);
			actions.moveToElement(ele).doubleClick(ele).perform();
			type(NewVendorCompanyPage.inputRole,role,"Role");
			type(NewVendorCompanyPage.inputRole, Keys.TAB, "TAB");;
			reporter.SuccessReport("Enter role", "Entered '"+role +"' in role."  );
		} catch (Exception e) {
			reporter.failureReport("AssertElementPresent :: ", role + " is not present in the page :: ", driver);		}
	}
	
	public void clickBankdataAdd() throws Throwable{
		click(NewVendorCompanyPage.bankDataAddBtn, "Bankdata Add Button");
	}
	
	public void clickUpdate() throws Throwable{
		scroll(NewVendorCompanyPage.updateBtn, "Update Button");
		click(NewVendorCompanyPage.updateBtn, "Update Button");
		waitForMask();
	}
	
	private void setAccountName(String accountName) throws Throwable{ 
		try {
			WebElement ele = driver.findElement(NewVendorCompanyPage.accountName);
			new Actions(driver).moveToElement(ele).doubleClick(ele).perform();
			type(NewVendorCompanyPage.inputAccountName,accountName,"Account Name");
			type(NewVendorCompanyPage.inputAccountName, Keys.TAB, "TAB");;
		} catch (Exception e) {
			reporter.failureReport("AssertElementPresent :: ", accountName + " is not present in the page :: ", driver);
		}
	}
	
	private void setBankName(String bankName) throws Throwable{ 
		try {
			WebElement ele = driver.findElement(NewVendorCompanyPage.bankName);
			new Actions(driver).moveToElement(ele).doubleClick(ele).perform();
			type(NewVendorCompanyPage.inputBankName,bankName,"Bak Name");
			type(NewVendorCompanyPage.inputBankName, Keys.TAB, "TAB");;
		} catch (Exception e) {
			reporter.failureReport("AssertElementPresent :: ", bankName + " is not present in the page :: ", driver);
		}
	}
	
	private void setAccountType(String accountType) throws Throwable{ 
		try {
			WebElement ele = driver.findElement(NewVendorCompanyPage.accountType);
			new Actions(driver).moveToElement(ele).doubleClick(ele).perform();
			type(NewVendorCompanyPage.inputAccountType,accountType,"Account Type");
			type(NewVendorCompanyPage.inputAccountType, Keys.TAB, "TAB");;
		} catch (Exception e) {
			reporter.failureReport("AssertElementPresent :: ", accountType + " is not present in the page :: ", driver);
		}
	}
	
	private void setAccountNumber(String accountNumber) throws Throwable{ 
		try {
			WebElement ele = driver.findElement(NewVendorCompanyPage.accountNumber);
			new Actions(driver).moveToElement(ele).doubleClick(ele).perform();
			type(NewVendorCompanyPage.inputAccountNumber,accountNumber,"Account Number");
			type(NewVendorCompanyPage.inputAccountNumber, Keys.TAB, "TAB");;
		} catch (Exception e) {
			reporter.failureReport("AssertElementPresent :: ", accountNumber + " is not present in the page :: ", driver);
		}
	}
	
	private void setRoutingNumber(String routingNumber) throws Throwable{ 
		try {
			WebElement ele = driver.findElement(NewVendorCompanyPage.routingNumber);
			new Actions(driver).moveToElement(ele).doubleClick(ele).perform();
			type(NewVendorCompanyPage.inputRoutingNumber,routingNumber,"Routing Number");
			type(NewVendorCompanyPage.inputRoutingNumber, Keys.TAB, "TAB");;
		} catch (Exception e) {
			reporter.failureReport("AssertElementPresent :: ", routingNumber + " is not present in the page :: ", driver);
		}
	}
	
	public void addBankData(Hashtable<String, String> data) throws Throwable
	{
		setAccountName(data.get("accountName"));
		setBankName(data.get("bankName"));
		setAccountType(data.get("accountType"));
		setAccountNumber(data.get("accountNumber"));
		setRoutingNumber(data.get("routingNumber"));
	}
}
