package com.pure.claim.lifeCycle;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ChecksPageLib  extends ActionEngine{
	
	public ChecksPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public String getCheckStatus() throws Throwable{
		return getAttributeValue(ChecksPage.checkStatus, "innerText");
	}
	
	public void clickCheckNo() throws Throwable{
		click(ChecksPage.checkNo, "checkNo");
	}
	
	public void verifyBulkInvoiceDetails(String amount, String status) throws Throwable{
		assertTextMatching(ChecksPage.fedExAmount, amount, "amount");
		assertText(ChecksPage.fedExStatus, status);
	}
	
	public void verifyPACADetails(String payTo, String pacaAmount) throws Throwable{
		assertTextMatching(ChecksPage.pacaPayTo, payTo, "payTo");
		assertTextMatching(ChecksPage.pacaAmount, pacaAmount, "pacaAmount");
	}
	
	public void clickPacaAmount() throws Throwable{
		click(ChecksPage.pacaAmount, "pacaAmount");
	}
	
}
