package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class ClaimStatusPage {

	static By editBtn, subrogationStatus, updateBtn, subrogationStatusDisplayed;
	
	static {
		editBtn = By.id("ClaimStatus:Edit-btnInnerEl");
		subrogationStatus = By.xpath("//input[@id='ClaimStatus:SubrogationStatus-inputEl']");
		updateBtn = By.id("ClaimStatus:Update-btnInnerEl");
		subrogationStatusDisplayed = By.xpath("//div[@id='ClaimStatus:SubrogationStatus-inputEl']");
	}
}
