package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class PartiesInvolvedContactsPage {
	
	static By contactsTitle, searchOrAddExistingContactBtn, serviceState, deleteBtn, newPerson, contactList,
	editBtn, zipCode, zipIcon, postalCode, postalCodeIcon, county, state, province, country, city, updateBtn, location,
	postalCodeErrorMsg;
	
	static {
		contactsTitle = By.id("ClaimContacts:ClaimContactsScreen:ttlBar");
		searchOrAddExistingContactBtn = By.id("ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:PeopleInvolvedDetailedLV_tb:ClaimContacts_AddExistingButton-btnInnerEl");
		deleteBtn = By.id("ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:PeopleInvolvedDetailedLV_tb:ClaimContacts_DeleteButton-btnInnerEl");
		newPerson = By.xpath("//span[contains(@id, 'ClaimContacts_CreateNewPersonButton-btnInnerEl')]");
		contactList = By.xpath("//div[contains(@id, 'PeopleInvolvedDetailedLV-body')]//td[2]/div");
		editBtn = By.id("ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:ContactBasicsDV_tb:ContactDetailToolbarButtonSet:Edit-btnInnerEl");
		zipCode = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//input");
		zipIcon = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//a");
		postalCode = By.xpath("//label[text()='Postal Code']/../..//input");
		postalCodeIcon = By.xpath("//label[text()='Postal Code']/../..//a");
		county = By.xpath("//label[text()='County']/../..//input");
		state = By.xpath("//label[text()='State']/../..//input[contains(@id,'GlobalAddressInputSet:State-inputEl')]");
		province = By.xpath("//label[text()='Province']/../..//input[contains(@id,'GlobalAddressInputSet:State-inputEl')]");
		country = By.xpath("//label[text()='Country']/../..//input");
		updateBtn = By.id("ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:ContactBasicsDV_tb:ContactDetailToolbarButtonSet:UpdateToolbarButton-btnInnerEl");
		location = By.xpath("//label[text()='Location']/../..//div");
		city = By.xpath("//label[text()='City']/../..//input");
		postalCodeErrorMsg = By.xpath("//div[contains(text(),'Postal Code must be six characters like \"A1A 1A1\"')][1]");
	}

}
