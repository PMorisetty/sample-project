package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class UsersPage {

	static By subrogaterRole, subrogationStatus;
	
	static {
		subrogationStatus = By.xpath("//div[@id='ClaimUsers:ClaimUsersScreen:ClaimUserDetailDV:ClaimUserAssignmentsLV-body']//tr/td[3]/div");
	}
}
