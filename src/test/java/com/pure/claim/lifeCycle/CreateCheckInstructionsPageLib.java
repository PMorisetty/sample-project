package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CreateCheckInstructionsPageLib extends ActionEngine{
	public CreateCheckInstructionsPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public String getDeliverMethod() throws Throwable {
		String text = "";
		try {
			text = driver.findElement(CreateCheckInstructionsPage.deliveryMethod).getAttribute("value");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return text;
	}
	
	public void setDeliverMethod(String deliveryMethod) throws Throwable{
		type(CreateCheckInstructionsPage.deliveryMethod ,deliveryMethod, "deliveryMethod");
		type(CreateCheckInstructionsPage.deliveryMethod, Keys.TAB,"tab key");
		waitForMask();
			
	}
	public void AdvancedDirectPay(String YesOrNo, String Explaination) throws Throwable{
		if(YesOrNo.equalsIgnoreCase("Yes")){
			click(CreateCheckInstructionsPage.AdvancedDirectPayRadioBtn,"AdvancedDirectPayRadioBtn");
			type(CreateCheckInstructionsPage.AdvancedDirectPayExplaination,Explaination,"AdvancedDirectPayExplaination");
		}
	}
	public void setShareOnMemberPortal(String shareOrNot) throws Throwable{		
		new Actions(driver).doubleClick(driver.findElement(CreateCheckInstructionsPage.shareOnMemberPortal)).perform();
		waitForMask();
		type(CreateCheckInstructionsPage.shareOnMemberPortal,shareOrNot, "shareOnMemberPortal");	
		type(CreateCheckInstructionsPage.shareOnMemberPortal, Keys.TAB,"tab key");
	}
	
	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		setDeliverMethod(data.get("deliverymethod"));		
		setShareOnMemberPortal(data.get("ShareOnMemberPortal"));
		AdvancedDirectPay(data.get("advanceddirectPay"), data.get("explainationfordirectpay"));
	}

}