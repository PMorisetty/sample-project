package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class QuickChecksDetailsPage {

	static By shareOnMemberPortal,shareOnMemberPortalNotnow,shareOnMemberPortalYes,
	shareOnMemberPortalNever, deliveryMethod,
	deliveryMethodStandardMail,
	deliveryMethodFedExExpress,
	deliveryMethodFedExTwoDay, deliveryMethodInputBox,
	deliveryMethodFedExOvernight,AdvancedDirectPayRadioBtn,AdvancedDirectPayExplaination,
	title, reasonForFedEx;
	static{
		shareOnMemberPortal = By.xpath("//input[@id='QuickCreateCheckWizard:CheckWizard_CheckInstructionsScreen:NewPaymentInstructionsDV:CheckWizardCheckSummaryInputSet:showOnPortalType-inputEl']");
		shareOnMemberPortalNever = By.xpath("//li[.='Never']");
		shareOnMemberPortalNotnow = By.xpath("//li[.='Not now - Maybe later']");
		shareOnMemberPortalYes = By.xpath("//li[.='Yes']");
		deliveryMethod = By.xpath("//input[@id='QuickCreateCheckWizard:CheckWizard_CheckInstructionsScreen:NewPaymentInstructionsDV:CheckDeliveryMethod-inputEl']");
		deliveryMethodStandardMail = By.xpath("//li[.='Standard Mail']");
		deliveryMethodFedExExpress = By.xpath("//li[.='FedEx Express Saver 3-day $']");
		deliveryMethodFedExTwoDay = By.xpath("//li[.='FedEx 2-day $$']");
		deliveryMethodFedExOvernight = By.xpath("//li[.='FedEx Overnight $$$']");
		AdvancedDirectPayRadioBtn = By.xpath("//input[@id='QuickCreateCheckWizard:CheckWizard_CheckInstructionsScreen:NewPaymentInstructionsDV:CheckWizardCheckSummaryInputSet:isAdvancedRadioInput_true-inputEl']");
		AdvancedDirectPayExplaination = By.xpath("//textarea[@id='QuickCreateCheckWizard:CheckWizard_CheckInstructionsScreen:NewPaymentInstructionsDV:CheckWizardCheckSummaryInputSet:PaymentNote-inputEl']");
		deliveryMethodInputBox = By.id("QuickCreateCheckWizard:CheckWizard_CheckInstructionsScreen:NewPaymentInstructionsDV:CheckDeliveryMethod-inputEl");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		reasonForFedEx = By.xpath("//textarea[contains(@id,'ReasonForFedex-inputEl')]");
	}
}
