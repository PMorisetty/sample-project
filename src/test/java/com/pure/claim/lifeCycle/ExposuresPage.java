package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class ExposuresPage{
	public static By ExposuresTreeItem, AssignBtn;
	public static By NewExposureBtn,selectAllCheckBox,closeExposureBtn, createReserveBtn;
	public static By exposureType, exposuresDiv, selectExposureCheckBox, vehicleSalvage, updateBtn;
	
	static{
		ExposuresTreeItem = By.xpath("//span[contains(@class,'tree')][.='Exposures']");
		AssignBtn = By.id("ClaimExposures:ClaimExposuresScreen:ClaimExposures_Assign-btnInnerEl");
		//NewExposureBtn = By.xpath("//span[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_AssignSaveScreen:NewExposureLV_tb:AddExposure-btnWrap']");
		NewExposureBtn = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_AssignSaveScreen:NewExposureLV_tb:AddExposure-btnInnerEl");
		selectAllCheckBox = By.xpath("//span[contains(@id,'rowcheckcolumn')]/div");
		closeExposureBtn = By.id("ClaimExposures:ClaimExposuresScreen:ClaimExposures_CloseExposure-btnInnerEl");
		exposureType = By.id("ClaimExposures:ClaimExposuresScreen:ExposuresLV:0:Type");
		exposuresDiv = By.cssSelector("div[id*=\"ExposuresLV-body\"]");
		selectExposureCheckBox = By.xpath("//div[@id='ClaimExposures:ClaimExposuresScreen:ExposuresLV-body']//img");
		createReserveBtn = By.xpath("//span[contains(@id,'CreateReserve-btnInnerEl')]");
		
		vehicleSalvage = By.xpath("//a[@id='ExposureDetail:ExposureDetailScreen:ExposureDetailDV:VehicleDamage_VehicleSalvageCardTab']");
		updateBtn = By.xpath("//span[@id='ExposureDetail:ExposureDetailScreen:Update-btnInnerEl']");
	}

}
