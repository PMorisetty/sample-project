package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class ApprovalPage {
	
	static By approveBtn, rejectBtn, cancelBtn, editBtn, approvalTab, rejectOkBtn, rejectionRationale;
	
	static{
		approveBtn = By.xpath("//span[contains(@id,'BulkInvoiceApprovalDetailWorksheet_ApproveButton_Pure-btnInnerEl')]");
		rejectBtn = By.xpath("//span[contains(@id,'BulkInvoiceApprovalDetailWorksheet_RejectButton-btnInnerEl')]");
		rejectOkBtn = By.xpath("//span[text()='OK']");
		cancelBtn = By.xpath("//span[contains(@id,'BulkInvoiceApprovalDetailWorksheet_CancelButton-btnInnerEl')]");
		editBtn = By.id("BulkInvoiceApprovalDetailWorksheet:ApprovalDetailScreen:Edit-btnInnerEl");
		approvalTab = By.id("wsTabBar:wsTab_0-btnInnerEl");
		rejectionRationale = By.xpath("//input[contains(@id,'RejectionRationale1-inputEl') or contains(@id,'rejectionRationale-inputEl')]");
	}

}
