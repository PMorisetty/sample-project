package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.report.CReporter;

public class EmailsPageLib extends ActionEngine {

	public EmailsPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable {
		this.driver = driver;
		this.reporter = reporter;
	}

	public void clickActionsBtn() throws Throwable {
		click(EmailsPage.ActiosBtn, "ActiosBtn");
	}

	public void clickEmailLink() throws Throwable {
		click(EmailsPage.EmailLink, "EmailLink");
	}

	public void clickRecipients_AddBtn() throws Throwable {
		click(EmailsPage.Recipients_AddBtn, "Recipients_AddBtn");
	}

	public void clickCheckboxRecipient(int count) throws Throwable {
		String xPath = xpathUpdator(EmailsPage.CheckboxRecipient, String.valueOf(count));
		click(By.xpath(xPath), "CheckboxRecipient");
	}

	public void clickNameOfToRecipient(int count) throws Throwable {
		String xPath = xpathUpdator(EmailsPage.NameOfToRecipientClick, String.valueOf(count));
		click(By.xpath(xPath), "clickNameOfToRecipient");
	}

	public void TypeNameOfToRecipient(String recipientName, int count) throws Throwable {
		String xPath = xpathUpdator(EmailsPage.NameOfToRecipientType, String.valueOf(count));
		type(By.xpath(xPath), recipientName, "TypeNameOfToRecipient");
	}

	public void clickEmailOfToRecipient(int count) throws Throwable {
		String xPath = xpathUpdator(EmailsPage.EmailOfToRecipient, String.valueOf(count));
		click(By.xpath(xPath), "clickEmailOfToRecipient");
	}

	public void TypeEmailOfToRecipient(String recipientEmail, int count) throws Throwable {
		String xPath = xpathUpdator(EmailsPage.EmailOfToRecipientType, String.valueOf(count));
		type(By.xpath(xPath), recipientEmail, "TypeEmailOfToRecipient");
	}

	public void AddCCRecipientsBtn() throws Throwable {
		click(EmailsPage.AddCCRecipientsBtn, "AddCCRecipientsBtn");
	}

	public void CCRecipients_AddBtn() throws Throwable {
		click(EmailsPage.CCRecipients_AddBtn, "CCRecipients_AddBtn");
	}

	public void clickCheckboxCCRecipient() throws Throwable {
		click(EmailsPage.CheckboxCCRecipient, "CheckboxCCRecipient");
	}

	public void clickNameOfCCRecipient() throws Throwable {
		click(EmailsPage.NameOfCCRecipientClick, "clickNameOfCCRecipient");
	}

	public void TypeNameOfCCRecipient(String recipientName) throws Throwable {
		type(EmailsPage.NameOfCCRecipientType, recipientName, "TypeNameOfCCRecipient");
	}

	public void clickEmailOfCCRecipient() throws Throwable {
		click(EmailsPage.EmailOfCCRecipient, "clickEmailOfCCRecipient");
	}

	public void TypeEmailOfCCRecipient(String recipientEmail) throws Throwable {
		type(EmailsPage.EmailOfCCRecipientType, recipientEmail, "TypeEmailOfCCRecipient");
	}

	public void AddBccRecipientsBtn() throws Throwable {
		click(EmailsPage.AddBccRecipientsBtn, "AddBccRecipientsBtn");
	}

	public void BccRecipients_AddBtn() throws Throwable {
		click(EmailsPage.BccRecipients_AddBtn, "BccRecipients_AddBtn");
	}

	public void clickCheckboxBccRecipient() throws Throwable {
		click(EmailsPage.CheckboxBccRecipient, "CheckboxBccRecipient");
	}

	public void clickNameOfBccRecipient() throws Throwable {
		click(EmailsPage.NameOfBccRecipientClick, "clickNameOfBccRecipient");
	}

	public void TypeNameOfBccRecipient(String recipientName) throws Throwable {
		type(EmailsPage.NameOfBccRecipientType, recipientName, "TypeNameOfBccRecipient");
	}

	public void clickEmailOfBccRecipient() throws Throwable {
		click(EmailsPage.EmailOfBccRecipient, "clickEmailOfBccRecipient");
	}

	public void TypeEmailOfBccRecipient(String recipientEmail) throws Throwable {
		type(EmailsPage.EmailOfBccRecipientType, recipientEmail, "TypeEmailOfBccRecipient");
	}

	public void TypeShareOnMemberPortal(String shareOnMemberPortal) throws Throwable {
		type(EmailsPage.ShareOnMemberPortal, shareOnMemberPortal, "ShareOnMemberPortal");
	}

	public void TypeSubject(String emailSubject) throws Throwable {
		type(EmailsPage.Subject, emailSubject, "Subject");
	}

	public void TypeBody(String emailBody) throws Throwable {
		type(EmailsPage.Body, emailBody, "Body");
	}

	public void clickAddAttachment() throws Throwable {
		click(EmailsPage.AddAttachment, "AddAttachment");
	}
	
	public void clickSendEmailBtn() throws Throwable {
		click(EmailsPage.SendEmailBtn, "SendEmailBtn");
	}
	
	private void selectAttachment(String attachmentName) throws Throwable {
		int size = driver.findElements(EmailsPage.SelectFileRows).size();
		int i = 1;
		while(i<=size){
			String selectFileXpath = xpathUpdator(EmailsPage.SelectFile, String.valueOf(i));
			String fileName = getText(By.xpath(selectFileXpath), "SelectFile");
			if(fileName.contains(attachmentName)){
				String selectBtn = xpathUpdator(EmailsPage.SelectBtn, String.valueOf(i));
				click(By.xpath(selectBtn), "SelectBtn");
				break;
			} else
				i++;
		}
	}

	public void fillToRecipientDetails(Hashtable<String, String> data) throws Throwable {
		int count = Integer.valueOf(data.get("NoOfToRecipients"));
		for(int i =1; i<=count; i++){
			if(i>1)
				clickRecipients_AddBtn();
			clickCheckboxRecipient(i);
			clickNameOfToRecipient(i);
			TypeNameOfToRecipient(data.get("toRecipientName_"+i),i);
			clickEmailOfToRecipient(i);
			TypeEmailOfToRecipient(data.get("toRecipientEmail_"+i),i);
		}
	}
	
	public void fillCCRecipientDetails(String ccRecipientName, String ccRecipientEmail) throws Throwable {
		AddCCRecipientsBtn();
		CCRecipients_AddBtn();
		clickCheckboxCCRecipient();
		clickNameOfCCRecipient();
		TypeNameOfCCRecipient(ccRecipientName);
		clickEmailOfCCRecipient();
		TypeEmailOfCCRecipient(ccRecipientEmail);
	}
	
	public void fillBccRecipientDetails(String bccRecipientName, String bccRecipientEmail) throws Throwable {
		AddBccRecipientsBtn();
		BccRecipients_AddBtn();
		clickCheckboxBccRecipient();
		clickNameOfBccRecipient();
		TypeNameOfBccRecipient(bccRecipientName);
		clickEmailOfBccRecipient();
		TypeEmailOfBccRecipient(bccRecipientEmail);
	}

	public void AddAttachment(String attachmentName) throws Throwable {
		clickAddAttachment();
		selectAttachment(attachmentName);
	}

	public void openEmailTab() throws Throwable {
		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("Email");
		if(!driver.findElement(EmailsPage.EmailPageTitle).isDisplayed()){
			click(UploadDocumentsPage.expandCollapseBtn,"expandCollapseBtn");
			waitForVisibilityOfElement(EmailsPage.EmailPageTitle, "EmailPageTitle");
			Thread.sleep(1000); //inevitable as opening the email slider is taking time
		}
	}

	public void verifyEmailIsSent() throws Throwable {
		isElementPresent(EmailsPage.EmailSent, "EmailSent", true);
	}
}
