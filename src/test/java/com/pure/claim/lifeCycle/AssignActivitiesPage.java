package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class AssignActivitiesPage{
	
	static By selectAssigneeFromList, assignBtn;

	static{
		selectAssigneeFromList = By.xpath("//input[contains(@id, 'SelectFromList-inputEl')]");
		assignBtn = By.xpath("//span[contains(@id, 'cAssignmentPopupScreen_ButtonButton-btnInnerEl')]");
		
	}
}
