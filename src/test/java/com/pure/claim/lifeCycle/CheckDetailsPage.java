package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class CheckDetailsPage {
	
	static By editDeliveryMethod, trackingNo, upToFinancials, coverage, checkDetailsTable;
	
	static{
		editDeliveryMethod = By.xpath("//span[contains(@id,'DeliveryMethodLV_tb:EditButton-btnInnerEl')]");
		trackingNo = By.xpath("//label[text()='Tracking No']/../..//div");
		upToFinancials = By.xpath("//a[contains(text(),'Up to Financials')]");
		
		coverage = By.xpath("//div[@id='ClaimFinancialsChecksDetail:ClaimFinancialsChecksDetailScreen:CheckDV:CheckSummaryPaymentsLV-body']//tr[1]/td[4]/div");
		checkDetailsTable = By.id("ClaimFinancialsChecks:ClaimFinancialsChecksScreen:ChecksLV");
	}

}
