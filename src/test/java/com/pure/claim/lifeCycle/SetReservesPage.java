package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class SetReservesPage {

	static By unfilledCellExposure, unfilledCellCostType, addBtn, coverageCell, costCatagory,
	currentlyAvailable, pendingApproval,newAvailableReserves,change,comments,
	saveBtn, cancelBtn, exposureBox, 
	costTypeNone,costTypeClaimCost,costTypeExpenseAO,costTypeExpenseDCC,
	costCategoryAutoBody, costCategoryDeductibleReimbursement, costCategoryGlass, costCategoryLegal,
	costCategoryMileageReimbursement,costCategoryOther,costCategoryRental,costCategorySalvage,costCategoryTowing;
	static By newReserve, title;
	
	static{
		unfilledCellExposure = By.xpath("//div[contains(@class,'x-grid-cell-inner') and contains(.,'Claim-level')]");
		unfilledCellCostType = By.xpath("//div[contains(@class,'x-grid-cell-inner') and contains(.,'none')]");
		addBtn = By.xpath("//span[@id='NewReserveSet:NewReserveSetScreen:Add-btnInnerEl']");
		saveBtn = By.xpath("//span[@id='NewReserveSet:NewReserveSetScreen:NewReserveSet_Save-btnInnerEl' or @id='NewReserveSet:NewReserveSetScreen:Update-btnInnerEl']");
		cancelBtn = By.id("NewReserveSet:NewReserveSetScreen:Cancel-btnInnerEl");
		exposureBox = By.xpath("//*[@name='Exposure']");
		//Reserve Table
		newReserve = By.xpath("//div[@id='NewReserveSet:NewReserveSetScreen:ReservesSummaryDV:EditableReservesLV-body']");
		
		coverageCell = By.xpath("//*[contains(@class,'x-grid-cell-inner') and contains(.,'Claim-level')]/../following-sibling::td[1]/div");
		costCatagory = By.xpath("//*[contains(@class,'x-grid-cell-inner') and contains(.,'Claim-level')]/../following-sibling::td[3]/div");
		currentlyAvailable = By.xpath("//*[contains(@class,'x-grid-cell-inner') and contains(.,'Claim-level')]/../following-sibling::td[4]/div");
		pendingApproval = By.xpath("//*[contains(@class,'x-grid-cell-inner') and contains(.,'Claim-level')]/../following-sibling::td[5]/div");
		newAvailableReserves = By.xpath("//*[contains(@class,'x-grid-cell-inner') and contains(.,'Claim-level')]/../following-sibling::td[6]/div");
		change = By.xpath("//*[contains(@class,'x-grid-cell-inner') and contains(.,'Claim-level')]/../following-sibling::td[7]/div");
		comments = By.xpath("//*[contains(@class,'x-grid-cell-inner') and contains(.,'Claim-level')]/../following-sibling::td[8]/div");
		
		costTypeNone = By.xpath("//li[text()='<none>']");
		costTypeClaimCost = By.xpath("//li[text()='Claim Cost']");
		costTypeExpenseAO = By.xpath("//li[text()='Expense - A&O']");
		costTypeExpenseDCC = By.xpath("//li[text()='Expense - D&CC']");
		
		costCategoryAutoBody = By.xpath("//li[text()='Auto body']");
		costCategoryDeductibleReimbursement = By.xpath("//li[text()='Deductible reimbursement']");
		costCategoryGlass = By.xpath("//li[text()='Glass']");
		costCategoryLegal = By.xpath("//li[text()='Legal']");
		costCategoryMileageReimbursement = By.xpath("//li[text()='Mileage reimbursement']");
		costCategoryOther = By.xpath("//li[text()='Other']");
		costCategoryRental = By.xpath("//li[text()='Rental']");
		costCategorySalvage = By.xpath("//li[text()='Salvage']");
		costCategoryTowing = By.xpath("//li[text()='Towing']");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
	}
	
}
