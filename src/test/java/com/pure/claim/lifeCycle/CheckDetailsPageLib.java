package com.pure.claim.lifeCycle;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CheckDetailsPageLib extends ActionEngine{
	
	public CheckDetailsPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void verifyTrackingNo(String trackingNo) throws Throwable{
		assertText(CheckDetailsPage.trackingNo, trackingNo);
	}
	
	public void navigateUptoFinancials() throws Throwable{
		click(CheckDetailsPage.upToFinancials, "upToFinancials");
	}
	
	public void verifyTrackingNoIsNotPresent() throws Throwable{
		waitForInVisibilityOfElement(CheckDetailsPage.trackingNo, "trackingNo");
	}
	
	public void verifyCoverage(String coverage) throws Throwable{
		assertTextMatching(CheckDetailsPage.coverage, coverage, "coverage");
	}
	
	public void verifyCheckDetalsPageWithTable() throws Throwable{
		waitForVisibilityOfElement(CheckDetailsPage.checkDetailsTable, "Check Details Table");
	}

}
