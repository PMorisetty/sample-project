package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.personalAuto.OperatorsAndVehiclesPage;
import com.pure.report.CReporter;

public class ClusterInfoPageLib extends ActionEngine{

	public ClusterInfoPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}

	public String getServerIDInBatchServer() throws Throwable{
		waitForVisibilityOfElement(ClusterInfoPage.serverIDInBatchServer, "ServerID In BatchServer");
		return getText(ClusterInfoPage.serverIDInBatchServer, "ServerID In BatchServer");
	}
	public boolean verifyServerIdsInClusterMember(Hashtable<String, String> data) throws Throwable{

		boolean serverIdPresence = false;
		String serverIDInClusterMember =null,serverID = null;
		int noOfServerIDs = Integer.valueOf(data.get("noOfServerIDs"));
		for(int i=1 ; i <= noOfServerIDs; i++){
			String tempLocator = xpathUpdator(ClusterInfoPage.serverIDInClusterMembers, String.valueOf(i));
			serverIDInClusterMember = getText(By.xpath(tempLocator),"ServerID In ClusterMembers");
			for(int j=1; j <= noOfServerIDs; j++){

				serverID = data.get("ServerID_"+j);
				if(serverID.equalsIgnoreCase(serverIDInClusterMember)){
					serverIdPresence = true;
					break;
				}
				else{
					serverIdPresence = false;
				}
			}
			if(!serverIdPresence){
				break;
			}
		}

		return serverIdPresence;
	}



	public boolean verifyClusterNowInClusterMember(Hashtable<String, String> data) throws Throwable{

		boolean clusterNowFlag = false;
		String clusterNowInClusterMember =null;
		int noOfServerIDs = Integer.valueOf(data.get("noOfServerIDs"));
		for(int i=1 ; i <= noOfServerIDs; i++){
			String tempLocator = xpathUpdator(ClusterInfoPage.inClusterNow, String.valueOf(i));
			clusterNowInClusterMember = getText(By.xpath(tempLocator),"clusterNow In ClusterMembers");
				if(clusterNowInClusterMember.equalsIgnoreCase("Yes")){
					clusterNowFlag = true;
				}
				else{
					clusterNowFlag = false;
					break;
				}
			}
			
		return clusterNowFlag;
	}
	
	public void clickReturnToClaimCenter() throws Throwable{

		click(ClusterInfoPage.menuButton,"Menu Button");
		waitForVisibilityOfElement(ClusterInfoPage.returnToClaimCenterLink, "Return To Claim Center");
	    click(ClusterInfoPage.returnToClaimCenterLink,"Return To Claim Center");
		
	}

}
