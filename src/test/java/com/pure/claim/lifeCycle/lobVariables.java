package com.pure.claim.lifeCycle;

import java.lang.reflect.Field;

/*
 * This class has LOB level variables.
 * These variables can be used to set get values
 * thought the lob's life cycle
 * Ex: new lobVariables().setValue("numOfExposures", 10);
 */

public class lobVariables {
	/*
	 * List down all the LOB level variables
	 */
	public static int numOfExposures = 0;
	public static int numOfReserves = 0;
	
	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	public void setValue(String nameOfVariable, int newValue) throws Throwable, Exception{		
		Field field = lobVariables.class.getDeclaredField(nameOfVariable);
		field.set(this, newValue);
	}
	public void setValue(String nameOfVariable, String newValue) throws Throwable, Exception{		
		Field field = lobVariables.class.getDeclaredField(nameOfVariable);
		field.set(this, newValue);
	}

	public void setValue(String nameOfVariable, char[] newValue) throws Throwable, Exception{		
		Field field = lobVariables.class.getDeclaredField(nameOfVariable);
		field.set(this, newValue);
	}
	public int getValueInt(String nameOfVariable) throws Exception, Throwable{
		Field field = lobVariables.class.getDeclaredField(nameOfVariable);
		return field.getInt(nameOfVariable);
	}
	public String getValueString(String nameOfVariable) throws Exception, Throwable{
		Field field = lobVariables.class.getDeclaredField(nameOfVariable);
		return field.get(nameOfVariable).toString();
	}
}
