package com.pure.claim.lifeCycle;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class UsersPageLib extends ActionEngine {

	public UsersPageLib(EventFiringWebDriver driver, CReporter reporter) {
		this.driver = driver;
		this.reporter = reporter;
	}

	public void clickSubrogationAdjuster() throws Throwable {
		int row = 0;
		List<WebElement> users = driver.findElements(By.xpath("//div[@id='ClaimUsers:ClaimUsersScreen:ClaimUsersLV-body']//tr"));
		for(int i=1; i<=users.size(); i++) {
			By userRole = By.xpath("//div[@id='ClaimUsers:ClaimUsersScreen:ClaimUsersLV-body']//tr["+ i +"]/td[5]/div");
			String role = getText(userRole, "User Role");
			if("Subrogation Adjuster".equalsIgnoreCase(role)) {
				row = i;
				break;
			}
		}
		By subrogratorAdjusterName = By.xpath("//div[@id='ClaimUsers:ClaimUsersScreen:ClaimUsersLV-body']//tr["+ row +"]/td[2]/div");
		click(subrogratorAdjusterName,"subrogratorAdjusterName");
	}
	
	public void verifySubrogationStatusOfUser(String status) throws Throwable {
		assertText(UsersPage.subrogationStatus, status);
	}
}
