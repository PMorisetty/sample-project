package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ApprovalPageLib extends ActionEngine{
	
	public ApprovalPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}

	public void clickApprove() throws Throwable{
		waitForVisibilityOfElement(ApprovalPage.approvalTab, "approval Tab");
		click(ApprovalPage.approveBtn, "approval btn");
	}
	
	public void clickReject(String rejectionRationale) throws Throwable{
		waitForVisibilityOfElement(ApprovalPage.approvalTab, "approval Tab");
		click(ApprovalPage.editBtn, "edit btn");
		type(ApprovalPage.rejectionRationale, rejectionRationale, "rejectionRationale");
		type(ApprovalPage.rejectionRationale, Keys.TAB, "Tab key");
		click(ApprovalPage.rejectBtn, "rejectOkBtn");
		waitForVisibilityOfElement(ApprovalPage.rejectOkBtn, "rejectOkBtn");
		click(ApprovalPage.rejectOkBtn, "rejectOkBtn");
	}
}
