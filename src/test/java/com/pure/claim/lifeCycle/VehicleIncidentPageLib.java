package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class VehicleIncidentPageLib extends ActionEngine{
	Hashtable<String, String>data;
	public VehicleIncidentPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void enterDamageDescription(String damageDescription) throws Throwable{
		type(VehicleIncidentPage.damageDescription,damageDescription,"damageDescription");
		type(VehicleIncidentPage.damageDescription, Keys.TAB,"tab key");
	}
	
	public void setDriverName(String driverName) throws Throwable{
		type(VehicleIncidentPage.driverName,driverName,"driver name");
		type(VehicleIncidentPage.driverName, Keys.TAB,"tab key");
	}
	
	public void setLossOccured(String lossoccured) throws Throwable{
		type(VehicleIncidentPage.lossOccured,lossoccured,"lossoccured");
		type(VehicleIncidentPage.lossOccured, Keys.TAB,"tab key");
	}
	
	public void enterDetails(String damageDescription,String driverName, String lossoccured) throws Throwable{
		enterDamageDescription(damageDescription);
		setDriverName(driverName);
		setLossOccured(lossoccured);	
		clickOkBtn();
	}
	/* @overload 
	 * This method is called when the vehicle to be set is a new one
	 * Hence model, make & year should be set
	 */
	public void setInvolvedVehicle(String vehicle, String lossParty, String year, String make, String model) throws Throwable{
		WebElement ele = driver.findElement(VehicleIncidentPage.selectVehicle);
		Actions actions = new Actions(driver);
		actions.doubleClick(ele).sendKeys(Keys.chord(Keys.CONTROL, "a"), vehicle).perform();
		waitForMask();
		actions.sendKeys(Keys.TAB).perform();
		waitForMask();
		type(VehicleIncidentPage.lossParty, lossParty, "Loss Party");
		type(VehicleIncidentPage.lossParty, Keys.TAB, "tab key");
		type(VehicleIncidentPage.year, year, "Year");
		type(VehicleIncidentPage.year, Keys.TAB, "tab key");
		type(VehicleIncidentPage.make, make, "make");
		type(VehicleIncidentPage.make, Keys.TAB, "tab key");
		waitForMask();
		type(VehicleIncidentPage.model, model, "model");
		type(VehicleIncidentPage.model, Keys.TAB, "tab key");
	}
	/* @overload 
	 * This method is called when the vehicle to be set is a from the drop list
	 * Hence model, make & year are auto populated and disabled
	 */
	public void setInvolvedVehicle(String vehicle, String lossParty) throws Throwable{
		type(VehicleIncidentPage.selectVehicle, vehicle, "Select vehicle");
		type(VehicleIncidentPage.selectVehicle, Keys.TAB, "Tab key");
		waitForMask();
		type(VehicleIncidentPage.lossParty, lossParty, "Loss Party");
		type(VehicleIncidentPage.lossParty, Keys.TAB, "tab key");
	}
	
	private void setAirBagDeployed() throws Throwable{
		if(data.get("airBagDeployed").equalsIgnoreCase("yes")){
			click(VehicleIncidentPage.airBagsDeployedYes, "Air Bags deployed Yes");
		}else{
			click(VehicleIncidentPage.airBagsDeployedNo, "Air Bags deployed No");
		}
	}
	
	private void setOperable() throws Throwable{
		if(data.get("operable").equalsIgnoreCase("yes")){
			click(VehicleIncidentPage.operableYes, "Operable Yes");
		}else{
			click(VehicleIncidentPage.operableNo, "Operable No");
		}
	}
	
	private void setInjuries() throws Throwable{
		type(VehicleIncidentPage.injuries, data.get("injuries"), "injuries");
		type(VehicleIncidentPage.injuries, Keys.TAB, "Tab key");
	}
	
	private void setPrimaryDamage() throws Throwable{
		type(VehicleIncidentPage.primaryDamage, data.get("primaryDamage"), "primaryDamage");
		type(VehicleIncidentPage.primaryDamage, Keys.TAB, "Tab key");
	}
	
	private void setDifferentInsuredAddress() throws Throwable{
		if(data.get("differentInsuredAddress").equalsIgnoreCase("yes")){
			click(VehicleIncidentPage.differentInsuredAddressYes, "Different Insured Address Yes");
			fillLossLocation(data);
		}else{
			click(VehicleIncidentPage.differentInsuredAddressNo, "Different Insured Address No");
		}
	}
	
	private void setZip(String zip) throws Throwable {
		type(VehicleIncidentPage.zip, zip, "ZIP text box");
		type(VehicleIncidentPage.zip, Keys.TAB, "Tab key");
		click(VehicleIncidentPage.zipIcon,"ZipIcon");
		Thread.sleep(2000);//Inevitable: Auto data fill is happening in the background
	}
	
	private void setAddress1(String address1) throws Throwable {
		type(VehicleIncidentPage.addressLine1, address1, "Address1 text box");
		type(VehicleIncidentPage.addressLine1, Keys.TAB, "Tab key");
	}
	
	private void setAddress2(String address2) throws Throwable {
		type(VehicleIncidentPage.addressLine2, address2, "Address2 text box");
		type(VehicleIncidentPage.addressLine2, Keys.TAB, "Tab key");
	}
	
	private void setCity(String city) throws Throwable {
		type(VehicleIncidentPage.city, city, "City text box");
		type(VehicleIncidentPage.city, Keys.TAB, "Tab key");
	}
	
	private void fillLossLocation(Hashtable<String, String>data) throws Throwable {
		setZip(data.get("vehicleLocationZip"));
		setAddress1(data.get("vehicleLocationAddress1"));
		setAddress2(data.get("vehicleLocationAddress2"));
		setCity(data.get("vehicleCity"));
	}
	
	private void setShopOfChoice() throws Throwable{
		if(data.get("shopOfChoice").equalsIgnoreCase("yes")){
			click(VehicleIncidentPage.shopOfChoiceYes, "Shop of choice Yes");
		}else{
			click(VehicleIncidentPage.shopOfChoiceNo, "Shop of choice No");
		}
		
	}
	
	private void clickEvalute() throws Throwable {
		click(VehicleIncidentPage.evaluateBtn, "Evaluate button");
		waitForMask();
	}
	
	public void fillMethodOfInspectionDetails(Hashtable<String, String> data) throws Throwable{
		this.data = data;
		setAirBagDeployed();
		setOperable();
		setInjuries();
		setPrimaryDamage();
		//--- using the below condition secondaryDamage for pureExpress search displaying in Search for Appraiser page ---ccc
		if((data.get("secondaryDamage")!=null)){
			setSecondaryDamage();
		}
//		setDifferentInsuredAddress();
		setParty();
		setShopOfChoice();
		clickEvalute();
	}
	
	public void assertMakeCode(String expectedMakeCode) throws Throwable{
		assertTextMatchingWithAttribute(VehicleIncidentPage.cccMakeCode, expectedMakeCode, "CCC make code");
	}
	
	public void setParty() throws Throwable{
		String partyName = data.get("reporterName");
		type(VehicleIncidentPage.party, partyName, "party");
		type(VehicleIncidentPage.party, Keys.TAB, "Tab key");
	}
	
	public void setTotalLoss() throws Throwable{
		if(data.get("shopOfChoice").equalsIgnoreCase("yes")){
			click(VehicleIncidentPage.totalLossYes, "Total Loss Yes");
		}else{
			click(VehicleIncidentPage.totalLossNo, "Total Loss No");
		}
	}
	
	public void clickOkBtn() throws Throwable{
		click(VehicleIncidentPage.okBtn, "OkBtn");
	}
	
	private void setSecondaryDamage() throws Throwable{
		type(VehicleIncidentPage.secondaryDamage, data.get("secondaryDamage"), "Secondary Damage");
		type(VehicleIncidentPage.secondaryDamage, Keys.TAB, "Tab key");
	}
	
	public void enterDetails_new(String damageDescription,String driverName, String lossoccured) throws Throwable{
		enterDamageDescription(damageDescription);
		setDriverName(driverName);
		setLossOccured(lossoccured);	
	}
}
