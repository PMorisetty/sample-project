package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class FinancialsTransactionsPage {

	static By reserveTypeFromBulkInvoice, costTypeFromBulkInvoice;
	
	static{
		reserveTypeFromBulkInvoice = By.xpath("//div[text()='FedEx BulkInvoice User']/../..//a[contains(@id,'TType')]");
		costTypeFromBulkInvoice = By.xpath("//div[text()='FedEx BulkInvoice User']/../..//td[6]/div");
	}
}
