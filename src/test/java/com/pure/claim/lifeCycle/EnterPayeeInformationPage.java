package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class EnterPayeeInformationPage {

	static By payeeName, paymentMethodCheck, nextBtn, paymentMethodElectronicFundTransfer,
	nameOnAccount, accountTypeChecking, accountTypeSavings, accountTypeOther, accountNo, routingNo;
	
	static{
		payeeName = By.xpath("//input[contains(@id,'NewCheckPayeeDV:PrimaryPayee_Name-inputEl')]");
		paymentMethodCheck = By.xpath("//input[contains(@id,'NewCheckPayeeDV:PaymentMethod_option1-inputEl')]");
		paymentMethodElectronicFundTransfer = By.xpath("//input[contains(@id,'NewCheckPayeeDV:PaymentMethod_option2-inputEl')]");
		nextBtn = By.id("NormalCreateCheckWizard:Next-btnInnerEl");
		nameOnAccount = By.xpath("//textarea[contains(@id,'EFTDataInputSet:PayTo-inputEl')]");
		accountTypeChecking = By.xpath("//input[contains(@id,'BankAccountType_option1-inputEl')]");
		accountTypeSavings = By.xpath("//input[contains(@id,'BankAccountType_option2-inputEl')]");
		accountTypeOther = By.xpath("//input[contains(@id,'BankAccountType_option3-inputEl')]");
		accountNo = By.xpath("//input[contains(@id,'BankAccountNumber-inputEl')]");
		routingNo = By.xpath("//input[contains(@id,'BankRoutingNumber-inputEl')]");
	}
}
