package com.pure.claim.lifeCycle;

public enum EnumAutoExposureTypes {
	comprehensive("Comprehensive - Vehicle Damage"),
	CollisionVehicleDamage("Collision - Vehicle Damage"),
	LiabilityAutoBodilyInjuryBodilyInjuryDamage("Liability - Auto bodily injury - Bodily Injury Damage"),
	MedicalPayments("Medical Payments"),
	UnderInsuredMotoristsBI("Under Insured Motorists - BI"),
	UninsuredMotoristBI("Uninsured Motorist - BI"),
	FirstPartyRental("1st Party Rental"),
	LiabilityPropertyDamageProperty("Liability - Property Damage - Property"),
	LiabilityPropertyDamageVehicle("Liability - Property Damage - Vehicle"),
	PIPDeath("PIP - Death"),
	PIPExtraordinaryMedical("PIP - Extraordinary Medical"),
	PIPFuneral("PIP - Funeral"),
	PIPLostWages("PIP - Lost Wages"),
	PIPMedical("PIP - Medical");

	private String value;

	EnumAutoExposureTypes(String value){
		this.value = value;
	}
	public String value(){
		return value;
	}
}
