package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class ActivitiesPage {
	
	static By activitiesFilter, firstActivityClaim, assignBtn, completeBtn, 
	approveBtn, rejectBtn, firstActivity, dueHeader, lastPageIcon, previousPageIcon;

	static{
		activitiesFilter = By.xpath("//input[contains(@id, 'DesktopActivitiesFilter-inputEl')]");
		firstActivityClaim = By.xpath("//div[contains(@id, 'DesktopActivitiesLV-body')]//td[7]/div/a");
		assignBtn = By.xpath("//span[contains(@id, 'AssignButton-btnInnerEl')]");
		completeBtn = By.xpath("//span[contains(@id, 'CompleteButton-btnInnerEl')]");
		approveBtn = By.xpath("//span[contains(@id, 'ApproveButton-btnInnerEl')]");
		rejectBtn = By.xpath("//span[contains(@id, 'RejectButton-btnInnerEl')]");
		firstActivity = By.xpath("//div[contains(@id, 'DesktopActivitiesLV-body')]//tr[1]/td[1]/div/img");
		dueHeader = By.xpath("//div[@id='DesktopActivities:DesktopActivitiesScreen:DesktopActivitiesLV']/div[2]//div[4]/div");
		lastPageIcon= By.xpath("//span[contains(@id, 'PrintButton-btnWrap')]/../following-sibling::a[4]");
		previousPageIcon = By.xpath("//span[contains(@id, 'PrintButton-btnWrap')]/../following-sibling::a[2]");
		
	}
}