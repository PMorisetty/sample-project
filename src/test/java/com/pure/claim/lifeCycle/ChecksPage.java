package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class ChecksPage {

	static By checkStatus, cellCheckNumber, checkNo, fedExAmount, fedExStatus, pacaPayTo, pacaAmount;
	
	static{
		checkStatus = By.xpath("//div[@id='ClaimFinancialsChecks:ClaimFinancialsChecksScreen:ChecksLV-body']//td[6]");
		cellCheckNumber = By.xpath("//span[text()='Check Number']");
		checkNo = By.id("ClaimFinancialsChecks:ClaimFinancialsChecksScreen:ChecksLV:0:CheckNumber");
		fedExAmount = By.xpath("//div[text()='FedEx']/../..//td[3]//a");
		fedExStatus = By.xpath("//div[text()='FedEx']/../..//td[6]/div");
		
		pacaPayTo = By.xpath("//div[@id='ClaimFinancialsChecks:ClaimFinancialsChecksScreen:ChecksLV-body']//tr[2]/td[2]/div");
		pacaAmount = By.xpath("//div[@id='ClaimFinancialsChecks:ClaimFinancialsChecksScreen:ChecksLV-body']//tr[2]/td[3]/div/a");
	}
}
