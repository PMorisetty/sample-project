package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class QuickChecksDetailsPageLib extends ActionEngine{
	public QuickChecksDetailsPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		this.driver = webDriver;
		this.reporter = reporter;
		waitForMask();
		printPageTitle(QuickChecksDetailsPage.title,"Page title");
	}
	
	public String getDeliverMethod() throws Throwable {
		String text = "";
		try {
			text = driver.findElement(QuickChecksDetailsPage.deliveryMethod).getAttribute("value");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return text;
	}
	
	public void setDeliverMethod(String deliveryMethod) throws Throwable{
		
		//click(QuickChecksDetailsPage.deliveryMethod,"deliveryMethod");
		new Actions(driver).doubleClick(driver.findElement(QuickChecksDetailsPage.deliveryMethod)).perform();
		Thread.sleep(200);//inevitable as dblclick opens new inputbox @ runtime.
		type(QuickChecksDetailsPage.deliveryMethodInputBox ,deliveryMethod, "deliveryMethod");
		type(QuickChecksDetailsPage.deliveryMethodInputBox, Keys.TAB,"tab key");
		waitForMask();
			
	}
	public void AdvancedDirectPay(String YesOrNo, String Explaination) throws Throwable{
		if(YesOrNo.equalsIgnoreCase("Yes")){
			click(QuickChecksDetailsPage.AdvancedDirectPayRadioBtn,"AdvancedDirectPayRadioBtn");
			type(QuickChecksDetailsPage.AdvancedDirectPayExplaination,Explaination,"AdvancedDirectPayExplaination");
		}
	}
	public void setShareOnMemberPortal(String shareOrNot) throws Throwable{		
		new Actions(driver).doubleClick(driver.findElement(QuickChecksDetailsPage.shareOnMemberPortal)).perform();
		waitForMask();
		type(QuickChecksDetailsPage.shareOnMemberPortal,shareOrNot, "shareOnMemberPortal");	
		type(QuickChecksDetailsPage.shareOnMemberPortal, Keys.TAB,"tab key");
	}
	public void clickFinish() throws Throwable{
		click(QuickCheckBasicsPage.finishBtn,"finishBtn");
		isElementPresent(FinancialsPage.financialTittlebar, "financialTittlebar");
		waitForMask();
		printPageTitle(FinancialsPage.title,"Page title");
	}
	
	public void setReasonForFedEx(String reason) throws Throwable{
		type(QuickChecksDetailsPage.reasonForFedEx,reason, "reasonForFedEx");	
		type(QuickChecksDetailsPage.reasonForFedEx, Keys.TAB,"tab key");
	}
	
	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		setDeliverMethod(data.get("deliverymethod"));
		if(data.get("reasonForFedEx")!=null){
			setReasonForFedEx(data.get("reasonForFedEx"));
		}
		setShareOnMemberPortal(data.get("ShareOnMemberPortal"));
		AdvancedDirectPay(data.get("advanceddirectPay"), data.get("explainationfordirectpay"));
		clickFinish();
	}

}
