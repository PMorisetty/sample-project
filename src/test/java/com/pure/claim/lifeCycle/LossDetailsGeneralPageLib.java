package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class LossDetailsGeneralPageLib extends ActionEngine{
	
	public LossDetailsGeneralPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable {
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void clickEditButton() throws Throwable{
		click(LossDetailsGeneralPage.EditBtn, "Edit button");
	}
	
	public void clickAddButton() throws Throwable{
		click(LossDetailsGeneralPage.addBtn,"Add button");
	}
	
	public void clickUpdateButton() throws Throwable{
		click(LossDetailsGeneralPage.UpdateBtn,"Update button");
	}
	
	public void clickVehicle() throws Throwable{
		By vehicleBy = By.xpath("//a[contains(text(),'Chevrolet')]");
		
		waitForVisibilityOfElement(vehicleBy, "Chevrolet vehicle link");
		click(vehicleBy,"Chevrolet vehicle link");
			
	}
}
