package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyPage;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import com.pure.report.CReporter;


public class UploadDocumentsPageLib extends ActionEngine{	 	

	public UploadDocumentsPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void expandNewDocumentpage() throws Throwable{
		if(!driver.findElement(UploadDocumentsPage.NewDocumentTittle).isDisplayed()){
			click(UploadDocumentsPage.expandCollapseBtn,"expandCollapseBtn");
			waitForVisibilityOfElement(UploadDocumentsPage.NewDocumentTittle, "NewDocumentTittle");
			Thread.sleep(3000); //inevitable as opening the new document slider is taking time
		}
	}
	public void collapseNewDocumentpage() throws Throwable{
		if(driver.findElement(UploadDocumentsPage.NewDocumentTittle).isDisplayed()){
			click(UploadDocumentsPage.expandCollapseBtn,"expandCollapseBtn");
		}
	}
	public String getDocName() throws Throwable {
		String text = "";
		try {
			
			text = driver.findElement(VerifiedPolicyPage.lossDate).getAttribute("value");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return text;
	}
	
	public void openNewDocument() throws Throwable{
		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("AttachAnExistingDocument");
		if(!driver.findElement(UploadDocumentsPage.NewDocumentTittle).isDisplayed()){
			click(UploadDocumentsPage.expandCollapseBtn,"expandCollapseBtn");
			waitForVisibilityOfElement(UploadDocumentsPage.NewDocumentTittle, "NewDocumentTittle");
			Thread.sleep(3000); //inevitable as opening the new document slider is taking time
		}
	}

	public void uploadDocument(String pathToFile, String ShareOnMemberPortal, String description, String docType) throws Throwable{
		try{
			waitForVisibilityOfElement(UploadDocumentsPage.description, "description");
			driver.findElement(UploadDocumentsPage.uploadBrouswebtn).sendKeys(pathToFile);
			waitForMask();
			int index = pathToFile.lastIndexOf("\\");
			String fileName = pathToFile.substring(index + 1);
			
			if((waitForVisibilityOfElement(UploadDocumentsPage.description,"New Document description")) &&
					(waitForVisibilityOfElement(UploadDocumentsPage.typeDoc,"Document type"))){
				Thread.sleep(2000);
				type(UploadDocumentsPage.description,description," description ");
				type(UploadDocumentsPage.description,Keys.TAB,"tab");
				Thread.sleep(2000);
				type(UploadDocumentsPage.shareOnPortal,ShareOnMemberPortal," ShareOnMemberPortal ");
				type(UploadDocumentsPage.shareOnPortal,Keys.TAB,"tab");
				Thread.sleep(2000);
				type(UploadDocumentsPage.typeDoc,docType," docType " );
				type(UploadDocumentsPage.typeDoc,Keys.TAB,"tab");
				Thread.sleep(2000);
				click(UploadDocumentsPage.updateBtn,"update Btn");
				waitForInVisibilityOfElement(UploadDocumentsPage.NewDocumentTittle,"New Document title");
				
				
				reporter.SuccessReport("Successfully uploaded document", fileName);
			}else{
				reporter.failureReport("Upload document failed", fileName, driver);
			}
			
			
		}catch(Exception e){
			LOG.info("Class name" + getCallerClassName() + "Method name : " + getCallerMethodName());
			LOG.info("++++++++++++++++++++++++++++Catch Block End+++++++++++++++++++++++++++++++++++++++++++");
			throw new RuntimeException(e);
		}
	}
}
