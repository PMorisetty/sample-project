package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class selectPolicyPageLib extends ActionEngine{
	
	public selectPolicyPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void fillDetailsAndSearch(String policyNo, String searchFor) throws Throwable{
		type(SelectPolicyPage.number, policyNo, "policy no");
		type(SelectPolicyPage.number, Keys.TAB, "Tab key");
		
		type(SelectPolicyPage.searchFor, searchFor, "Search For");
		type(SelectPolicyPage.searchFor, Keys.TAB, "Tab key");
		
		click(SelectPolicyPage.searchBtn, "Search Btn");
		
		click(SelectPolicyPage.selectBtn, "Select");
	}
	
}
