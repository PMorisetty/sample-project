package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CloseClaimPageLib extends ActionEngine{
	
	public CloseClaimPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void enterCloseClaimNotes(String closeClaimNotes) throws Throwable{
		type(CloseClaimPage.closeClaimNotes,closeClaimNotes,"closeClaimNotes entered:"+closeClaimNotes);
		type(CloseClaimPage.closeClaimNotes,Keys.TAB,"tab key");
	}
	
	public void enterOutcome(String outcome) throws Throwable{
		type(CloseClaimPage.outcome,outcome,"outcome entered:"+ outcome);
		type(CloseClaimPage.outcome,Keys.TAB,"tab key");
	}
	
	public void closeClaim(String closeClaimNotes, String outcome, String deductibleDecision,
			String paInvolved) throws Throwable{
		
		enterCloseClaimNotes(closeClaimNotes);
		enterOutcome(outcome);
		setDeductibleDecision(deductibleDecision);
		setPAInvolved(paInvolved);
		clickCloseClaim();
	}
	
	public void setDeductibleDecision(String deductibleDecision) throws Throwable{
		type(CloseClaimPage.deductibleDecision,deductibleDecision,"outcome entered:"+ deductibleDecision);
		type(CloseClaimPage.deductibleDecision,Keys.TAB,"tab key");
	}
	
	public void setPAInvolved(String paInvolved) throws Throwable{
		if(paInvolved.equalsIgnoreCase("yes")){
			click(CloseClaimPage.PAInvolvedYes,"PAInvolvedYes");
		}else{
			click(CloseClaimPage.PAInvolvedNo,"PAInvolvedNo");
		}
	}
	
	public void clickCloseClaim() throws Throwable{
		click(CloseClaimPage.closeClaimBtn, "CloseClaim Button");
	}

	public void verifyDeductibleManagementWorkSheet(String deductibleDecision, String waivedReason, 
			String otherComments, String ovverrideDeductible, String... deductibleType) throws Throwable{
		
		assertText(CloseClaimPage.deductibleDecision, deductibleDecision);
		if(deductibleDecision.equalsIgnoreCase("waived")){
			assertText(CloseClaimPage.waivedReason, waivedReason);
			assertText(CloseClaimPage.otherComments, otherComments);
		}else if(deductibleDecision.equalsIgnoreCase("applied")){
			assertText(CloseClaimPage.overrideDecutible, ovverrideDeductible);
		}
		if(deductibleType.length>0){
			String expectedDeductibleType = deductibleType[0];
			assertText(CloseClaimPage.deductibleType, expectedDeductibleType);
		}
	}
	
	public void setDeductibleDataCorrectYes() throws Throwable{
		click(CloseClaimPage.deductibleDataCorrectYes, "deductibleDataCorrectYes");
	}
	
	public void setDeductibleDataCorrectNo() throws Throwable{
		click(CloseClaimPage.deductibleDataCorrectNo, "deductibleDataCorrectNo");
	}
	
	public void DeductibleDecision(String WaivedOrApplied, String waivedReason, String otherComments) throws Throwable{		
		if(WaivedOrApplied.equalsIgnoreCase("Applied")){			
			click(CloseClaimPage.deductibleDecisionApplied,"WaivedOrApplied");
		}else{
			click(CloseClaimPage.deductibleDecisionWaived,"WaivedOrApplied");
			type(CloseClaimPage.waivedReason,waivedReason, "waivedReason");
			type(CloseClaimPage.waivedReason,Keys.TAB, "tab key");
			type(CloseClaimPage.otherComments,otherComments, "otherComments");
			type(CloseClaimPage.otherComments,Keys.TAB, "tab key");
		}
	}
	
	public void setDeductibleType(String deductibletype) throws Throwable{
		if(deductibletype.equalsIgnoreCase("AOP")){
			click(CloseClaimPage.deductibleTypeAOP,"deductibleTypeAOP");
		}else{
			click(CloseClaimPage.deductibleTypeSpecial,"deductibleTypeSpecial");
		}
	}
	
	public void clickLossCauseInfoCorrectYes() throws Throwable{
		click(CloseClaimPage.lossCauseYes, "lossCauseYes");
	}
}
