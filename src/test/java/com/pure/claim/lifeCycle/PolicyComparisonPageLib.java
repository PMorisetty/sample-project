package com.pure.claim.lifeCycle;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PolicyComparisonPageLib extends ActionEngine{

	public PolicyComparisonPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void clickFinish() throws Throwable{
		click(PolicyComparisonPage.finishBtn, "finishBtn");
	}
}
