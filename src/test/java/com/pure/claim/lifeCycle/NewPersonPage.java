package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class NewPersonPage {
	
	static By firstName, lastName, mobile, emailMain, zipCode, address1, dateOfBirth, gender,
	addRole, role, roleInput, updateBtn;
	
	static {
		
		firstName = By.xpath("//input[contains(@id, 'FirstName-inputEl')]");
		lastName = By.xpath("//input[contains(@id, 'LastName-inputEl')]");
		mobile = By.xpath("//input[contains(@id, 'Cell:GlobalPhoneInputSet:NationalSubscriberNumber-inputEl')]");
		emailMain = By.xpath("//input[contains(@id, 'Primary-inputEl')]");
		zipCode = By.xpath("//input[contains(@id, 'PostalCode-inputEl')]");
		address1 = By.xpath("//input[contains(@id, 'AddressLine1-inputEl')]");
		dateOfBirth = By.xpath("//input[contains(@id, 'DateOfBirth')]");
		gender = By.xpath("//input[contains(@id, 'Gender-inputEl')]");
		addRole = By.xpath("//span[contains(@id, 'Add-btnInnerEl')]");
		role = By.xpath("//div[contains(@id, 'EditableClaimContactRolesLV-body')]//td[3]/div");
		roleInput = By.xpath("//input[@name='Role']");
		updateBtn = By.xpath("//span[contains(@id, 'UpdateToolbarButton-btnInnerEl')]");
		
		

	}

}
