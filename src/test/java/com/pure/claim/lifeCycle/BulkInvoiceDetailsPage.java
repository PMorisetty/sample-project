package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class BulkInvoiceDetailsPage {

	static By refreshBtn, upToBulkInvoices, title, vendorName,
	invoiceLabel, statusLabel, invoiceItemDetailsLabel, docLinkedToBulkInvoiceLabel,
	approvalHistoryLabel, erroredLineItemLabel, checkDetailsLabel, paymentInstLabel,
	remittanceAdviceLabel, editBtn, cancelBtn, updateBtn, submitBtn, claimNotFoundClaimNo,
	claimNoNoExposureFoundAtAbilityToPay, approvalHistoryStatus;
	
	static{
		refreshBtn = By.xpath("//span[text()='Refresh']");
		editBtn = By.id("EditBulkInvoiceDetail:BulkInvoiceDetailScreen:Edit-btnInnerEl");
		cancelBtn = By.id("EditBulkInvoiceDetail:BulkInvoiceDetailScreen:Cancel-btnInnerEl");
		updateBtn = By.id("EditBulkInvoiceDetail:BulkInvoiceDetailScreen:ToolbarButton-btnInnerEl");
		submitBtn = By.id("EditBulkInvoiceDetail:BulkInvoiceDetailScreen:SubmitButton-btnInnerEl");
		upToBulkInvoices = By.id("EditBulkInvoiceDetail:EditBulkInvoiceDetail_UpLink");
		title = By.id("EditBulkInvoiceDetail:BulkInvoiceDetailScreen:ttlBar");
		vendorName = By.xpath("//label[text()='Vendor']/../..//td[2]/div");
		invoiceLabel = By.xpath("//label[text()='Invoice']");
		statusLabel = By.xpath("//label[text()='Status']");
		invoiceItemDetailsLabel = By.xpath("//label[text()='Invoice Item Details']");
		docLinkedToBulkInvoiceLabel = By.xpath("//label[text()='Documents Linked to Bulk Invoice']");
		approvalHistoryLabel = By.xpath("//label[text()='Approval History']");
		erroredLineItemLabel = By.xpath("//label[text()='Errored Line Items']");
		checkDetailsLabel = By.xpath("//label[text()='Check Details']");
		paymentInstLabel = By.xpath("//label[text()='Payment Instructions']");
		remittanceAdviceLabel = By.xpath("//label[text()='Remittance Advice']");
		claimNotFoundClaimNo = By.xpath("//div[text()='Claim Number Not found']/../../td[2]/div");
		claimNoNoExposureFoundAtAbilityToPay = By.xpath("//div[text()='No Exposure found at Ability to Pay']/../../td[2]/div");
		approvalHistoryStatus = By.xpath("//div[contains(@id,'BulkInvoiceApprovalHistoryLV-body')]//tbody//td[3]/div");
	}
}
