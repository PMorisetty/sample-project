package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class BulkInvoiceDetailsPageLib extends ActionEngine{
	
	public BulkInvoiceDetailsPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void verifyAllSectionsAreDisplayed() throws Throwable{
		waitForVisibilityOfElement(BulkInvoiceDetailsPage.invoiceLabel, "invoiceLabel");
		waitForVisibilityOfElement(BulkInvoiceDetailsPage.statusLabel, "statusLabel");
		waitForVisibilityOfElement(BulkInvoiceDetailsPage.invoiceItemDetailsLabel, "invoiceItemDetailsLabel");
		waitForVisibilityOfElement(BulkInvoiceDetailsPage.docLinkedToBulkInvoiceLabel, "docLinkedToBulkInvoiceLabel");
		waitForVisibilityOfElement(BulkInvoiceDetailsPage.approvalHistoryLabel, "approvalHistoryLabel");
		waitForVisibilityOfElement(BulkInvoiceDetailsPage.erroredLineItemLabel, "erroredLineItemLabel");
		waitForVisibilityOfElement(BulkInvoiceDetailsPage.checkDetailsLabel, "checkDetailsLabel");
		waitForVisibilityOfElement(BulkInvoiceDetailsPage.paymentInstLabel, "paymentInstLabel");
		waitForVisibilityOfElement(BulkInvoiceDetailsPage.remittanceAdviceLabel, "remittanceAdviceLabel");
	}
	
	public void claimDetails(String claimNo, String vendorReference, String invoiceNo, String reserveLine, String exposure, String amount, String deductionsAmount, String paymentType, String status) throws Throwable{
		String actualVendorReference = "//a[text()='"+claimNo+"']/../../..//td[3]/div";
		By actualVendorReferenceBy = By.xpath(actualVendorReference);
		assertText(actualVendorReferenceBy, vendorReference);
		
		String actualInvoiceNo = "//a[text()='"+claimNo+"']/../../..//td[4]/div";
		By actualInvoiceNoBy = By.xpath(actualInvoiceNo);
		assertText(actualInvoiceNoBy, invoiceNo);
		
		String actualReserveLine = "//a[text()='"+claimNo+"']/../../..//td[5]/div";
		By actualReserveLineBy = By.xpath(actualReserveLine);
		assertText(actualReserveLineBy, reserveLine);
		
		String actualExposure = "//a[text()='"+claimNo+"']/../../..//td[6]/div";
		By actualExposureBy = By.xpath(actualExposure);
		assertText(actualExposureBy, exposure);
		
		String actualPaymentType = "//a[text()='"+claimNo+"']/../../..//td[7]/div";
		By actualPaymentTypeBy = By.xpath(actualPaymentType);
		assertText(actualPaymentTypeBy, paymentType);
		
		String actualAmount = "//a[text()='"+claimNo+"']/../../..//td[8]/div";
		By actualAmountBy = By.xpath(actualAmount);
		actualAmount = getText(actualAmountBy, "actualAmount");
		assertTextStringContains(actualAmount, amount);
		
		String actualDeductions = "//a[text()='"+claimNo+"']/../../..//td[9]/div";
		By actualDeductionsBy = By.xpath(actualDeductions);
		actualDeductions = getText(actualDeductionsBy, "actualAmount");
		assertTextStringContains(actualDeductions, deductionsAmount);
		
		String actualStatus = "//a[text()='"+claimNo+"']/../../..//td[15]/div";
		By actualStatusBy = By.xpath(actualStatus);
		assertText(actualStatusBy, status);
	}
	
	public void verifyClaimStatus(String claimNo, String expectedStatus, String... rejectedReason) throws Throwable{
		String actualStatus = "//a[text()='"+claimNo+"']/../../..//td[15]/div";
		By actualStatusBy = By.xpath(actualStatus);
		assertText(actualStatusBy, expectedStatus);
		
		if(rejectedReason.length>0){
			String actualRejectionReason = "//a[text()='"+claimNo+"']/../../..//td[13]/div";
			By actualRejectionReasonBy = By.xpath(actualRejectionReason);
			assertText(actualRejectionReasonBy, rejectedReason[0]);
		}
		
	}
	
	public void verifyTitle() throws Throwable{
		String expectedTitle = "Bulk Invoice Details";
		assertText(BulkInvoiceDetailsPage.title, expectedTitle);
	}
	
	public void clickEdit() throws Throwable{
		click(BulkInvoiceDetailsPage.editBtn, "editBtn");
	}
	
	public void clickUpdate() throws Throwable{
		click(BulkInvoiceDetailsPage.updateBtn, "updateBtn");
	}
	
	public void clickCancel() throws Throwable{
		click(BulkInvoiceDetailsPage.cancelBtn, "cancelBtn");
	}
	
	public void clickClaimNo(String claimNo) throws Throwable{
		String claim = "//a[text()='"+claimNo+"']";
		By claimBy = By.xpath(claim);
		click(claimBy, claimNo);
	}

	public void verifyClaimIsEditable(String claimNo) throws Throwable{
		clickEdit();
		String claim = "//div[text()='"+claimNo+"']";
		By claimBy = By.xpath(claim);
		waitForVisibilityOfElement(claimBy, claimNo);
	}
	
	public void verifyClaimIsNotEditable(String claimNo) throws Throwable{
		clickEdit();
		String claim = "//a[text()='"+claimNo+"']";
		By claimBy = By.xpath(claim);
		waitForVisibilityOfElement(claimBy, claimNo);
	}
	
	public void verifyClaimIsPresentInErroredLine(String claimNo) throws Throwable{
		String errorClaim = "//a[text()='"+claimNo+"' and contains(@id,'BulkInvoiceErrorItems')]";
		By errorClaimBy = By.xpath(errorClaim);
		waitForVisibilityOfElement(errorClaimBy, claimNo);
	}
	
	public void clickSubmit() throws Throwable{
		click(BulkInvoiceDetailsPage.submitBtn, "submitBtn");
	}
	
	public void verifyApprovalHistoryStatus(String status) throws Throwable{
		assertText(BulkInvoiceDetailsPage.approvalHistoryStatus, status);
	}
	
	public void addClaimNoForClaimNotFoundLineItem(String claimNo) throws Throwable{
		clickEdit();
		WebElement ele = driver.findElement(BulkInvoiceDetailsPage.claimNotFoundClaimNo);
		Actions actions = new Actions(driver);
		actions.doubleClick(ele).sendKeys(claimNo).build().perform();
		waitForMask();
		actions.sendKeys(Keys.TAB).build().perform();
		waitForMask();
		clickUpdate();
	}
	
	public void retypeSameClaimNoForNoExposureFoundAtAbilityToPay() throws Throwable{
		clickEdit();
		//this is looped for 3 times as there is an application issue which it will not work in the first go
		for(int i=0; i<3; i++){
			WebElement ele = driver.findElement(BulkInvoiceDetailsPage.claimNoNoExposureFoundAtAbilityToPay);
			Actions actions = new Actions(driver);
			actions.doubleClick(ele).sendKeys(Keys.CONTROL,"a").sendKeys(Keys.DELETE).perform();
			waitForMask();
			actions.sendKeys(Keys.TAB).build().perform();
			waitForMask();
		}
		clickUpdate();
	}
	
	public void clickUptoBulkInvoices() throws Throwable{
		click(BulkInvoiceDetailsPage.upToBulkInvoices, "upToBulkInvoices");
	}
	
}
