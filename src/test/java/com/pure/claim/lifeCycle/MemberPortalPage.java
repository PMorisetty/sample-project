package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class MemberPortalPage {

	public static By MemberPortalTreeItem, EditBtn, ShowAllRowsBtn, ReassignBtn, MemberAdvocateLabel, VendorLabel,
			MemberPortalInfoLabel, InfoToBeSharedLabel, PortalStatus, PortalStatusDropDown, PortalStatusDropDownArrow,
			UpdateBtn, CancelBtn;

	static {
		MemberPortalTreeItem = By.xpath("//span[contains(@class,'tree')][.='Member Portal']");
		EditBtn = By.id("PURE_MemberPortal:Edit-btnInnerEl");
		ShowAllRowsBtn = By.xpath("//a[@id='PURE_MemberPortal:showAllRowsButton']");
		ReassignBtn = By.xpath("//span[@id='PURE_MemberPortal:reassignMA-btnInnerEl']");

		MemberAdvocateLabel = By.xpath("//label[text()='Member Advocate']");
		MemberPortalInfoLabel = By.xpath("//label[text()='Member Portal Information']");
		InfoToBeSharedLabel = By.xpath("//label[text()='Information to be shared on Portal']");
		VendorLabel = By.xpath("//label[@id='PURE_MemberPortal:4']");
		PortalStatus = By.xpath("//div[@id='PURE_MemberPortal:portalStatusTypeKeyInput-inputEl']");

//		PortalStatusDropDown = By.xpath("//input[@id='PURE_MemberPortal:portalStatusTypeKeyInput-inputEl']");
		PortalStatusDropDown = By.xpath("//div[@class='x-boundlist-list-ct x-unselectable']/ul/li[text()='$']");
		PortalStatusDropDownArrow = By.xpath("//table[contains(@id,'PURE_MemberPortal')]//td[@class=' x-trigger-cell x-unselectable']/div");
		UpdateBtn = By.xpath("//span[@id='PURE_MemberPortal:Update-btnInnerEl']");
		CancelBtn = By.xpath("//span[@id='PURE_MemberPortal:Cancel-btnInnerEl']");

	}

}
