package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class CloseExposurePage {
	static By closeExposureBtn,cancelBtn;
	static By notes,deductibleDecision, deductibleDecisionInput,
	deductibleDataCorrect, okBtn, outcome, clearBtn,
	validationResultsTab, deductibleAmount, overrideDecutible, 
	waivedReason, otherComments, deductibleDataYes, deductibleDataNo, awaitingSubmissionOkBtn,
	deductibleDecisionWaived, deductibleDecisionApplied, waivedReasonInput, otherCommentsInput;
	static{
		closeExposureBtn = By.id("CloseExposurePopup:CloseExposureScreen:Update-btnInnerEl");
		cancelBtn = By.id("CloseExposurePopup:CloseExposureScreen:Cancel-btnInnerEl");
		notes = By.xpath("//textarea[@id='CloseExposurePopup:CloseExposureScreen:CloseExposureInfoDV:Note-inputEl']");
		outcome = By.xpath("//input[contains(@id, 'Outcomerange-inputEl')]");
		deductibleDecision = By.xpath("//label[text()='Deductible Decision']/../../td/div");
		deductibleDecisionInput = By.xpath("//label[text()='Deductible Decision']/../..//input");
		deductibleDecisionWaived = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:DeductibleWaived_true-inputEl')]");
		deductibleDecisionApplied = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:DeductibleWaived_false-inputEl')]");
		deductibleAmount = By.xpath("//label[text()='Deductible Amount']/../../td/div");
		overrideDecutible = By.xpath("//label[text()='Override Deductible']/../../td/div");
		waivedReason = By.xpath("//label[text()='Waived Reason']/../../td/div");
		otherComments = By.xpath("//label[text()='Other Comments']/../../td/div");
		waivedReasonInput = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:Waive_Reason-inputEl')]");
		otherCommentsInput = By.xpath("//input[contains(@id,'DeductibleManagementInputSet:Waived_OtherReason-inputEl')]");
		deductibleDataCorrect = By.id("CloseExposurePopup:CloseExposureScreen:DeductibleConfirmation_true-inputEl");
		clearBtn = By.xpath("//span[contains(@id, 'ClearButton-btnInnerEl')]");
		validationResultsTab = By.id("wsTabBar:wsTab_0-btnInnerEl");
		//******************** closure warning popup elements
		okBtn = By.xpath("//div[contains(@id,'messagebox')]//span[text()='OK']");
		deductibleDataYes = By.xpath("//input[@id='CloseExposurePopup:CloseExposureScreen:DeductibleConfirmation_true-inputEl']");
		deductibleDataNo = By.xpath("//input[@id='CloseExposurePopup:CloseExposureScreen:DeductibleConfirmation_false-inputEl']");
		awaitingSubmissionOkBtn = By.xpath("//span[text()='OK']");
	}
}
