package com.pure.claim.lifeCycle;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class SummaryOverviewPageLib extends ActionEngine {

	public SummaryOverviewPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		/* if getNumberOfExposuresAdded() included in constructor
		 * Everytime object created out of SummaryOverviewPageLib,
		 * it gets the number of exposures added & set it to 
		 * lobVariables for future use.
		 */
		//click(SummaryOverviewPage.summaryTree, "Summary tree item");
		this.driver = webDriver;
		this.reporter = reporter;

		waitForMask();
		printPageTitle(SummaryOverviewPage.title,"Page title");
	}

	@SuppressWarnings("finally")
	public int getNumberOfExposuresAdded() throws Exception, Throwable{
		int numOfExposures = 0;
		try{
			waitForVisibilityOfElement(SummaryOverviewPage.summaryTree, "Summary");
			if(!driver.findElements(SummaryOverviewPage.trExposures).isEmpty()){
				numOfExposures = driver.findElements(SummaryOverviewPage.trExposures).size();
			}
		}
		finally{
			new lobVariables().setValue("numOfExposures", numOfExposures);
			return numOfExposures;
		}

	}

	public boolean checkPresenceOfNotes(String text) throws Throwable{
		By notesBody = By.xpath("//div[text()='"+text+"']");
		return isElementNotPresent(notesBody, "Notes "+text);
	}

	public boolean checkPresenceOfDocuments() throws Throwable{
		return isVisible(SummaryOverviewPage.viewDocumentsLink, "ViewDocuments Link");
	}

	public void verifyLatestNotes(String expText) throws Throwable{
		waitForMask();
		assertTextStringContains(getText(SummaryOverviewPage.latestNotesBody, "Notes Text"), expText);
	}

	public void verifyNotesInSummaryPage(Hashtable<String, String> data) throws Throwable{
		openSummaryPage();
		if (data.get("coverage")!= null){
			assertMatchingTextInList(SummaryOverviewPage.notesList, data.get("coverage"), "Latest Notes List");
		}
		if(data.get("compliance")!=null){
			assertMatchingTextInList(SummaryOverviewPage.notesList, data.get("compliance"), "Latest Notes List");
		}
		if(data.get("damages")!=null){
			assertMatchingTextInList(SummaryOverviewPage.notesList, data.get("damages"), "Latest Notes List");
		}
		if(data.get("resolutionPlan")!=null){
			assertMatchingTextInList(SummaryOverviewPage.notesList, data.get("resolutionPlan"), "Latest Notes List");
		}
		if(data.get("reserveAnalysis")!=null){
			assertMatchingTextInList(SummaryOverviewPage.notesList, data.get("reserveAnalysis"), "Latest Notes List");
		}		
		if(data.get("liability")!=null){
			clickNext();
			assertMatchingTextInList(SummaryOverviewPage.notesList, data.get("liability"), "Latest Notes List");
		}

	}

	public void verifyClaimClosedInSummaryNotes(String claimsCloseNotes) throws Throwable{
		assertMatchingTextInList(SummaryOverviewPage.notesList, claimsCloseNotes, "Latest Notes List");
	}

	public void openSummaryPage() throws Throwable{
		click(SummaryOverviewPage.summaryTree, "Summary tree item");
		printPageTitle(SummaryOverviewPage.title,"Page title");
	}

	public void openFinancialsPage() throws Throwable{
		click(SummaryOverviewPage.financialsTree, "financials tree item");
		printPageTitle(SummaryOverviewPage.title,"Page title");
	}

	public void openPolicyPage() throws Throwable{
		click(SummaryOverviewPage.policyTree, "Policy tree item");
	}

	public void openPolicyLocationsPage() throws Throwable{
		click(SummaryOverviewPage.policyLocationsTree, "Policy locations tree item");
	}

	public void openLossDetailsPage() throws Throwable{
		click(SummaryOverviewPage.lossDetailsTree, "Loss details tree item");
	}

	public void openSubrogationPage() throws Throwable{
		click(SummaryOverviewPage.subrogationTree, "Subrogation tree item");
	}

	public void clickCCLDRR() throws Throwable{
		click(SummaryOverviewPage.CCLDRRLink, "CCLDRR Link");
	}

	public void openNotesPage() throws Throwable{
		click(SummaryOverviewPage.notesTree, "Notes tree item");
	}

	public void openExposuresPage() throws Throwable{
		click(SummaryOverviewPage.exposuresTree, "Exposures tree item");
	}

	public void openWorkPlanPage() throws Throwable{
		click(ClaimsTree.Workplan, "Workplan tree item");
	}

	public void openServicesPage() throws Throwable{
		click(ClaimsTree.Services, "Services tree item");
	}

	public void openPartiesInvolvedPage() throws Throwable{
		click(SummaryOverviewPage.partiesInvolvedTree, "Parties Involved tree item");
	}

	public void openDocumentsPage() throws Throwable{
		click(SummaryOverviewPage.DocumentsTree, "Documents tree item");
	}
	
	public void openHistoryPage() throws Throwable{
		click(SummaryOverviewPage.HistoryTree, "History tree item");
	}

	public String getPaidAmount() throws Throwable {
		String amountPaid =  getAttributeValue(SummaryOverviewPage.paidFinancials, "innerText");
		reporter.SuccessReport("Amount paid shown in Summary page-Financials ", amountPaid);
		return amountPaid;

	}

	public void clickNext() throws Throwable{
		click(SummaryOverviewPage.nextBtn, "Next Link");
	}

	//Will return column w.r.t. last row in table
	private List<WebElement> getColumns() throws Throwable{
		waitForVisibilityOfElement(SummaryOverviewPage.exposuresDiv, "exposuresDiv");
		List<WebElement> tableRows = null;
		List<WebElement> tableColumns = null;
		boolean flag = false;
		try{
			tableRows = driver.findElement(SummaryOverviewPage.exposuresDiv).findElement(By.tagName("table")).findElements(By.tagName("tr"));
			tableColumns = tableRows.get(tableRows.size() - 1).findElements(By.tagName("td"));
			flag = true;
		}catch(Exception e){
			flag = false;
		}
		finally{
			if(!flag){
				reporter.failureReport("Find division tag for the element - "+ExposuresPage.exposuresDiv, "unable to find the Division tag for "+ExposuresPage.exposuresDiv, driver);
			}
		}
		return tableColumns;
	}

	public void verifyCoverageSubType(String expectedCoverageSubType) throws Throwable{
		WebElement elem = getColumns().get(4); 
		String actualCoverageSubType = elem.getText();
		assertTextStringMatching(actualCoverageSubType, expectedCoverageSubType);
	}

	public void verifyDeductible(String coverage, String deductibleAmount, String WaivedOrAppled, String... deductibleType) throws Throwable{
		WebElement elem = driver.findElement(By.xpath("//label[contains(text(),'"+coverage+"')]/../following-sibling::td/div")); 
		String actualText = elem.getText();
		assertTextStringContains(actualText, WaivedOrAppled);

		Double deductibleAmountDouble = Double.parseDouble(deductibleAmount);
		String expctedDeductibleAmount = String.format("%,.2f", deductibleAmountDouble);
		assertTextStringContains(actualText, expctedDeductibleAmount);
		if(deductibleType.length>0){
			String expectedDeductibleType = deductibleType[0];
			assertTextStringContains(actualText, expectedDeductibleType);
		}
	}

	public void verifyAdjuster(String expectedNameAssigned) throws Throwable{
		assertText(SummaryOverviewPage.adjusterName, expectedNameAssigned);
	}

	public void openSecondaryApproverPage() throws Throwable{
		click(ClaimsTree.secondaryApproval, "secondaryApproval tree item");
		printPageTitle(ClaimsTree.secondaryApproval,"Page title");
	}

	public void openStatusPage() throws Throwable{
		click(ClaimsTree.Status, "Status tree item");
		printPageTitle(ClaimsTree.Status,"Page title");
	}

	public void openChecksFinancialsPage() throws Throwable{
		click(SummaryOverviewPage.financialsTree, "financials tree item");
		printPageTitle(SummaryOverviewPage.title,"Page title");
		click(SummaryOverviewPage.checksFinancials, "Checks in financials tree item");
	}

	public String getLossDateText()throws Throwable{

		return  getText(SummaryOverviewPage.lossDate, "Loss Date");
	}
	public String getNoticeDateText()throws Throwable{

		return getText(SummaryOverviewPage.noticeDate, "Notice Date");
	}
	public String getLossLocationText()throws Throwable{

		return getText(SummaryOverviewPage.lossLocation, "Loss Location");
	}

}