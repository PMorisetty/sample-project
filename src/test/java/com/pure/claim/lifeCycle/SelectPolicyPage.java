package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class SelectPolicyPage {
	static By number, searchFor, searchBtn, selectBtn, unselectBtn;
	
	static{
		number = By.xpath("//label[text()='Number']/../..//input");
		searchFor = By.xpath("//label[text()='Search For']/../..//input");
		searchBtn = By.id("ClaimPolicySelectPolicyPopup:ClaimPolicySelectPolicyScreen:PolicySearchDV:SearchAndResetInputSet:SearchLinksInputSet:Search");
		selectBtn = By.xpath("//a[text()='Select']");
		unselectBtn = By.xpath("//a[text()='Unselect']");
	}

}
