package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class SetCheckInstructionsPage {
	
	static By shareOnMemberPortal,shareOnMemberPortalNotnow,shareOnMemberPortalYes,
	shareOnMemberPortalNever, deliveryMethod,
	deliveryMethodStandardMail,
	deliveryMethodFedExExpress,
	deliveryMethodFedExTwoDay, deliveryMethodInputBox,
	deliveryMethodFedExOvernight,AdvancedDirectPayRadioBtn,AdvancedDirectPayExplaination,
	title, reasonForFedEx, finishBtn, linkDocBtn, cancelBtn, okBtn;
	static{
		shareOnMemberPortal = By.xpath("//input[contains(@id,'showOnPortalType-inputEl')]");
		shareOnMemberPortalNever = By.xpath("//li[.='Never']");
		shareOnMemberPortalNotnow = By.xpath("//li[.='Not now - Maybe later']");
		shareOnMemberPortalYes = By.xpath("//li[.='Yes']");
		deliveryMethod = By.xpath("//input[contains(@id,'CheckDeliveryMethod-inputEl')]");
		deliveryMethodStandardMail = By.xpath("//li[.='Standard Mail']");
		deliveryMethodFedExExpress = By.xpath("//li[.='FedEx Express Saver 3-day $']");
		deliveryMethodFedExTwoDay = By.xpath("//li[.='FedEx 2-day $$']");
		deliveryMethodFedExOvernight = By.xpath("//li[.='FedEx Overnight $$$']");
		AdvancedDirectPayRadioBtn = By.xpath("//input[contains(@id,'isAdvancedRadioInput_true-inputEl')]");
		AdvancedDirectPayExplaination = By.xpath("//textarea[contains(@id,'PaymentNote-inputEl')]");
		deliveryMethodInputBox = By.xpath("//input[contains(@id,'NewPaymentInstructionsDV:CheckDeliveryMethod-inputEl')]");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		reasonForFedEx = By.xpath("//textarea[contains(@id,'ReasonForFedex-inputEl')]");
		finishBtn = By.id("NormalCreateCheckWizard:Finish-btnInnerEl");
		linkDocBtn = By.id("NormalCreateCheckWizard:CheckWizard_CheckInstructionsScreen:Button_Reserve_LinkMultiDocument-btnInnerEl");
		cancelBtn = By.id("NormalCreateCheckWizard:Cancel-btnInnerEl");
		okBtn = By.xpath("//span[text()='OK']");
	}

}
