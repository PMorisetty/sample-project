package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class EmailsPage {

	static By EmailPageTitle, ActiosBtn, EmailLink, Recipients_AddBtn, CheckboxRecipient, NameOfToRecipientClick,
			NameOfToRecipientType, EmailOfToRecipient, EmailOfToRecipientType, AddCCRecipientsBtn, CCRecipients_AddBtn,
			CheckboxCCRecipient, NameOfCCRecipientClick, NameOfCCRecipientType, EmailOfCCRecipient,
			EmailOfCCRecipientType, AddBccRecipientsBtn, BccRecipients_AddBtn, CheckboxBccRecipient,
			NameOfBccRecipientClick, NameOfBccRecipientType, EmailOfBccRecipient, EmailOfBccRecipientType,
			ShareOnMemberPortal, Subject, Body, AddAttachment, SendEmailBtn, SelectFile, SelectBtn, SelectFileRows, EmailSent;

	static{
		EmailPageTitle = By.xpath("//span[contains(@id,'EmailWorksheet:CreateEmailScreen:ttlBar')]");
		ActiosBtn = By.xpath("//a[@id='Claim:ClaimMenuActions']");
		EmailLink = By.xpath("//div[@id='Claim:ClaimMenuActions:ClaimNewOtherMenuItemSet:ClaimMenuActions_NewOther:ClaimMenuActions_Email']");
		Recipients_AddBtn = By.xpath("//span[@id='EmailWorksheet:CreateEmailScreen:ToRecipientLV_tb:Add-btnInnerEl']");
		CheckboxRecipient = By.xpath("//div[@id='EmailWorksheet:CreateEmailScreen:ToRecipientLV']//table//tr[$]//img[@class='x-grid-checkcolumn']");
		NameOfToRecipientClick = By.xpath("//div[@id='EmailWorksheet:CreateEmailScreen:ToRecipientLV-body']//tbody/tr[$]/td[3]/div");
		NameOfToRecipientType = By.xpath("//input[@name='ToName']");
		EmailOfToRecipient = By.xpath("//div[@id='EmailWorksheet:CreateEmailScreen:ToRecipientLV-body']//tbody/tr[$]/td[4]/div");
		EmailOfToRecipientType = By.xpath("//input[@name='ToEmail']");
		
		AddCCRecipientsBtn = By.xpath("//span[@id='EmailWorksheet:CreateEmailScreen:ShowCCRecipients-btnInnerEl']");
		CCRecipients_AddBtn = By.xpath("//span[@id='EmailWorksheet:CreateEmailScreen:CcRecipientLV_tb:Add-btnInnerEl']");
		CheckboxCCRecipient = By.xpath("//div[@id='EmailWorksheet:CreateEmailScreen:CcRecipientLV']//img[@class='x-grid-checkcolumn']");
		NameOfCCRecipientClick = By.xpath("//div[@id='EmailWorksheet:CreateEmailScreen:CcRecipientLV-body']//tbody/tr/td[3]/div");
		NameOfCCRecipientType = By.xpath("//input[@name='CcName']");
		EmailOfCCRecipient = By.xpath("//div[@id='EmailWorksheet:CreateEmailScreen:CcRecipientLV-body']//tbody/tr/td[4]/div");
		EmailOfCCRecipientType = By.xpath("//input[@name='CcEmail']");
		
		AddBccRecipientsBtn = By.xpath("//span[@id='EmailWorksheet:CreateEmailScreen:ShowBCCRecipients-btnInnerEl']");
		BccRecipients_AddBtn = By.xpath("//span[@id='EmailWorksheet:CreateEmailScreen:BccRecipientLV_tb:Add-btnInnerEl']");
		CheckboxBccRecipient = By.xpath("//div[@id='EmailWorksheet:CreateEmailScreen:BccRecipientLV']//img[@class='x-grid-checkcolumn']");
		NameOfBccRecipientClick = By.xpath("//div[@id='EmailWorksheet:CreateEmailScreen:BccRecipientLV-body']//tbody/tr/td[3]/div");
		NameOfBccRecipientType = By.xpath("//input[@name='BccName']");
		EmailOfBccRecipient = By.xpath("//div[@id='EmailWorksheet:CreateEmailScreen:BccRecipientLV-body']//tbody/tr/td[4]/div");
		EmailOfBccRecipientType = By.xpath("//input[@name='BccEmail']");
		
		ShareOnMemberPortal = By.xpath("//input[@id='EmailWorksheet:CreateEmailScreen:showOnPortalType-inputEl']");
		Subject = By.xpath("//input[@id='EmailWorksheet:CreateEmailScreen:TextInput0-inputEl']");
		Body = By.xpath("//textarea[@id='EmailWorksheet:CreateEmailScreen:TextAreaInput0-inputEl']");
		AddAttachment = By.xpath("//a[@id='EmailWorksheet:CreateEmailScreen:AttachedDocumentsLV_tb:AddDocumentButton']");
		SendEmailBtn =  By.xpath("//span[@id='EmailWorksheet:CreateEmailScreen:ToolbarButton0-btnInnerEl']");
		SelectFile = By.xpath("//div[@id='PickExistingDocumentPopup:Claim_DocumentsScreen:PickExistingDocumentsLV-body']//tr[$]/td[4]/div");
		SelectBtn = By.xpath("//div[@id='PickExistingDocumentPopup:Claim_DocumentsScreen:PickExistingDocumentsLV-body']//tr[$]/td[1]/div/a");
		SelectFileRows = By.xpath("//div[@id='PickExistingDocumentPopup:Claim_DocumentsScreen:PickExistingDocumentsLV-body']//tr");
		EmailSent = By.xpath("//div[contains(text(),'Email Sent')]");
	}
}
