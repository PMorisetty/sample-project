package com.pure.claim.lifeCycle;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CreateRecoveryPageLib extends ActionEngine{

	public CreateRecoveryPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		this.driver = webDriver;
		this.reporter = reporter;
		waitForMask();
	}
	
	public void addNewRecoveryLineItem(Hashtable<String, String> data) throws Throwable{
		setPayer(data.get("payer"));
		setReserveLine(data.get("reserveLine"));
		setRecoveryCategory(data.get("recoveryCategory"));
		
		String reporterName = data.get("category");
		String CostType = data.get("amount");
//		click(CreateRecoveryPage.addBtn, "Add Btn");
		enterLineItemDetails(reporterName, CostType);
		waitForVisibilityOfElement(CreateRecoveryPage.updateBtn, "Update btn");
		scroll(CreateRecoveryPage.updateBtn, "scroll to Update btn");
		JSClick(CreateRecoveryPage.updateBtn, "Update btn");
		reporter.SuccessReport("", "Successfully added recovery");
	}
	
	public void enterLineItemDetails(String category, String amount) throws Throwable{
		List<WebElement> numColumns = getColumns();
		
		WebElement categoryCell = numColumns.get(1).findElement(By.tagName("div"));
		enterValueInCell(categoryCell, "LineCategory", category);
		
		WebElement amountCell = getColumns().get(3).findElement(By.tagName("div")); 
		enterValueInCell(amountCell, "Amount", amount);		

		/*enterValueInCell(driver.findElement(CreateRecoveryPage.exposure), "Exposure", reporterName);
		enterValueInCell(driver.findElement(CreateRecoveryPage.costType), "CostType", costType);
		enterValueInCell(driver.findElement(CreateRecoveryPage.costCategory), "CostCategory", CostCategory);
		enterValueInCell(driver.findElement(CreateRecoveryPage.newOpenRecoveryReserve), "NewOpenRecoveryReserves", NewOpenRecoveryReserve);*/
		
		
	}

	private List<WebElement> getColumns(){
		List<WebElement> tableRows = driver.findElement(CreateRecoveryPage.newLineItem).findElement(By.tagName("table")).findElement(By.tagName("tbody")).findElements(By.tagName("tr"));
		List<WebElement> tableColumns = tableRows.get(tableRows.size() - 1).findElements(By.tagName("td"));
		return tableColumns;
	}
	
	public void enterValueInCell(WebElement cellElement,String cellName, String value) throws Throwable{
		Actions actions = new Actions(driver);
		//actions.doubleClick(cellElement).perform();
		actions.moveToElement(cellElement).doubleClick().perform();
		Thread.sleep(1000);
//		waitForMask();
		By dynXpth = By.xpath("//input[@name='"+cellName+"']");
		type(dynXpth,value, " typed in "+cellName);
		type(dynXpth,Keys.TAB,"tab key");
		Thread.sleep(1000);//inevitable
		waitForMask();
	}
	
	public void setPayer(String payer) throws Throwable{
		type(CreateRecoveryPage.payer, payer, "Payer");
		type(CreateRecoveryPage.payer, Keys.TAB, "Tab Key");
	}
	
	public void setReserveLine(String reserveLine) throws Throwable{
		type(CreateRecoveryPage.reserveLine, reserveLine, "ReserveLine");
		type(CreateRecoveryPage.reserveLine, Keys.TAB, "Tab Key");
	}
	
	public void setRecoveryCategory(String recoveryCategory) throws Throwable{
		type(CreateRecoveryPage.recoveryCategory, recoveryCategory, "RecoveryCategory");
		type(CreateRecoveryPage.recoveryCategory, Keys.TAB, "Tab Key");
	}
}
