package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;


public class WorkPlanPageLib extends ActionEngine{
	
	public WorkPlanPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {
		this.driver = webDriver;
		this.reporter = reporter;
		click(ClaimsTree.Workplan, "workplan");
	}

	public void completeAllActivities() throws Throwable{
		click(WorkPlanPage.selectAllCheckBox,"selectAllActivities checkBox");
		click(WorkPlanPage.completeAllbtn,"completeAllbtn");
		int i = 0;
		while(!driver.findElement(WorkPlanPage.completeAllbtn).isEnabled() || i<10){
			//Thread.sleep(500);
			i++;
			if(i==10){
				break;
			}
		}
	}
	
	public void verifyPresenceOfActivitySubject(String subject, String status) throws Throwable{
		if(status.equalsIgnoreCase("True"))
		{
		assertMatchingTextInList(WorkPlanPage.activitiesList, subject, "Activities List");
		}
		else
		{
			By activity = By.xpath("//div[contains(@id, 'WorkplanLV-body')]//td[7]/div/a[text()='"+subject+"']/../../preceding-sibling::td[1]");
			verifyElementPresent(activity, "Activity with subject "+subject, false);
		}
	}
	public void verifyPresenceOfActivitySubject(String[] subjects) throws Throwable{
		for(String subject: subjects){
			assertMatchingTextInList(WorkPlanPage.activitiesList, subject, "Activities List");	
		}		
	}
	
	public String[] defaultWorkplanActities(){
		//For every new claim, below default activities are created
		String[] defaultActivities = {"Make initial contact with insured", "New Claim Review", "Initial 30 day file review"};
		/*defaultActivities[0] = "Make initial contact with insured";
		defaultActivities[1] = "New Claim Review";
		defaultActivities[2] = "Initial 30 day file review";
		*/return defaultActivities;
	}
	
	public void selectWorkPlanActivityBasedOnSubject(String subject) throws Throwable{
		By activity = By.xpath("//div[contains(@id, 'WorkplanLV-body')]//td[7]/div/a[text()='"+subject+"']/../../preceding-sibling::td[6]/div");
		click(activity,"Activity checkBox");
	}
	
	public void clickSkip() throws Throwable{
		click(WorkPlanPage.skipBtn,"Skip Button");
	}
	
	public void clickOnSubjectLink(String subject) throws Throwable{
		By subjectLink = By.xpath("//div[contains(@id, 'WorkplanLV-body')]//td[7]/div/a[text()='"+subject+"']");
		scroll(subjectLink, subject+ "Subject");
		click(subjectLink, subject+ "Subject");
	}
	
	public void completeMemberAdvocateOnTheClaim() throws Throwable{
		click(WorkPlanPage.assignedAsMemberAdvocate, "assignedAsMemberAdvocate");
		waitForVisibilityOfElement(WorkPlanPage.completeActivityBtn, "completeActivityBtn");
		click(WorkPlanPage.completeActivityBtn, "completeActivityBtn");
	}
	
	public void selectWorkplanFilterDropdown(String workplanFilter) throws Throwable{
		type(WorkPlanPage.workplanFilterDropdown, workplanFilter, "workplanFilter");
		type(WorkPlanPage.workplanFilterDropdown, Keys.TAB, "Tab Key");
	}
	
}
