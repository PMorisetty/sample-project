package com.pure.claim.lifeCycle;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class SetRecoveryReservesPageLib extends ActionEngine{

	public SetRecoveryReservesPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		this.driver = webDriver;
		this.reporter = reporter;
		waitForMask();
	}
	
	public void click() throws Throwable{
		click(SetRecoveryReservesPage.addBtn, "Add Btn");
	}
	

	public void addNewRecoveryReserve(Hashtable<String, String> data) throws Throwable{
		String reporterName = data.get("reportername");
		String CostTpe = data.get("costtype");
		String CostCategory = data.get("costcategory");
		String NewOpenRecoveryReserve= data.get("newOpenRecoveryReserve");
		
		click(SetRecoveryReservesPage.addBtn, "Add Btn");
		enterRecoveryReserveDetails(reporterName, CostTpe, CostCategory, NewOpenRecoveryReserve);
		waitForVisibilityOfElement(SetRecoveryReservesPage.saveBtn, "Save btn");
		scroll(SetRecoveryReservesPage.saveBtn, "scroll to save btn");
		JSClick(SetRecoveryReservesPage.saveBtn, "Save btn");
		reporter.SuccessReport("", "Successfully added reserve");
	}
	
	public void enterRecoveryReserveDetails(String reporterName, String costType, String CostCategory, String NewOpenRecoveryReserve) throws Throwable{
		List<WebElement> numColumns = getColumns();
		
		WebElement exposureCell = numColumns.get(1).findElement(By.tagName("div"));
		enterValueInCell(exposureCell, "Exposure", reporterName);
		
		WebElement costTypeCell = getColumns().get(3).findElement(By.tagName("div")); 
		enterValueInCell(costTypeCell, "CostType", costType);		

		WebElement costcategoryCell = getColumns().get(4).findElement(By.tagName("div")); 
		enterValueInCell(costcategoryCell, "CostCategory", CostCategory);
		
		WebElement newOpenRecoveryReserve = getColumns().get(7).findElement(By.tagName("div")); 
//		enterValueInCell(newOpenRecoveryReserve, "NewOpenRecoveryReserves", NewOpenRecoveryReserve);
		enterValueInCell(driver.findElement(SetRecoveryReservesPage.newOpenRecoveryReserve), "NewOpenRecoveryReserves", NewOpenRecoveryReserve);
		
		/*enterValueInCell(driver.findElement(SetRecoveryReservesPage.exposure), "Exposure", reporterName);
		enterValueInCell(driver.findElement(SetRecoveryReservesPage.costType), "CostType", costType);
		enterValueInCell(driver.findElement(SetRecoveryReservesPage.costCategory), "CostCategory", CostCategory);
		enterValueInCell(driver.findElement(SetRecoveryReservesPage.newOpenRecoveryReserve), "NewOpenRecoveryReserves", NewOpenRecoveryReserve);*/
		
		
	}

	private List<WebElement> getColumns(){
		List<WebElement> tableRows = driver.findElement(SetRecoveryReservesPage.newReserve).findElement(By.tagName("table")).findElement(By.tagName("tbody")).findElements(By.tagName("tr"));
		List<WebElement> tableColumns = tableRows.get(tableRows.size() - 1).findElements(By.tagName("td"));
		return tableColumns;
	}
	
	public void enterValueInCell(WebElement cellElement,String cellName, String value) throws Throwable{
		Actions actions = new Actions(driver);
		//actions.doubleClick(cellElement).perform();
		actions.moveToElement(cellElement).doubleClick().perform();
		Thread.sleep(1000);
//		waitForMask();
		By dynXpth = By.xpath("//input[@name='"+cellName+"']");
		type(dynXpth,value, " typed in "+cellName);
		type(dynXpth,Keys.TAB,"tab key");
		Thread.sleep(1000);//inevitable
		waitForMask();
	}
	
}
