package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class ExposureDetailsPage {
	
	static By editBtn, updateBtn, claimant, claimantType, cancelBtn, deductibleType,
	deductibleDecision, deductibleAmount, overrideDecutible, waivedReason, otherComments, uptoExposures;
	
	static{
		editBtn = By.id("ExposureDetail:ExposureDetailScreen:Edit-btnInnerEl");
		claimant = By.xpath("//label[text()='Claimant']/../..//input");
		claimantType = By.xpath("//label[text()='Type']/../..//input");
		updateBtn = By.id("ExposureDetail:ExposureDetailScreen:Update-btnInnerEl");
		cancelBtn = By.id("ExposureDetail:ExposureDetailScreen:Cancel-btnInnerEl");
		deductibleDecision = By.xpath("//label[text()='Deductible Decision']/../../td/div");
		deductibleAmount = By.xpath("//label[text()='Deductible Amount']/../../td/div");
		overrideDecutible = By.xpath("//label[text()='Override Deductible']/../../td/div");
		waivedReason = By.xpath("//label[text()='Waived Reason']/../../td/div");
		otherComments = By.xpath("//label[text()='Other Comments']/../../td/div");
		deductibleType = By.xpath("//label[text()='Deductible Type']/../../td/div");
		uptoExposures = By.id("ExposureDetail:ExposureDetail_UpLink");
	}

}
