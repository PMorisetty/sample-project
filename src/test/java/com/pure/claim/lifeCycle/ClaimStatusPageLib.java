package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.LossDetailsPage;
import com.pure.report.CReporter;

public class ClaimStatusPageLib extends ActionEngine {

	public ClaimStatusPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void clickEditBtn() throws Throwable {
		click(ClaimStatusPage.editBtn, "editBtn");
	}
	
	public void setSubrogationStatus(String subrogationStatus) throws Throwable {
		type(ClaimStatusPage.subrogationStatus, subrogationStatus, "Subrogation Status");
		type(ClaimStatusPage.subrogationStatus, Keys.TAB, "Tab key");
	}
	
	public void clickUpdateBtn() throws Throwable {
		click(ClaimStatusPage.updateBtn, "UpdateBtn");
	}
	
	public void verifySubrogationStatus(String subrogationStatusDisplayed) throws Throwable{
		assertText(ClaimStatusPage.subrogationStatusDisplayed, subrogationStatusDisplayed);		
	}
}
