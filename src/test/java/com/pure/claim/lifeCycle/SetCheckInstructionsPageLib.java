package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class SetCheckInstructionsPageLib extends ActionEngine{
	
	public SetCheckInstructionsPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void setDeliverMethod(String deliveryMethod) throws Throwable{
		
		//click(SetCheckInstructionsPage.deliveryMethod,"deliveryMethod");
		new Actions(driver).doubleClick(driver.findElement(SetCheckInstructionsPage.deliveryMethod)).perform();
		Thread.sleep(200);//inevitable as dblclick opens new inputbox @ runtime.
		type(SetCheckInstructionsPage.deliveryMethodInputBox ,deliveryMethod, "deliveryMethod");
		type(SetCheckInstructionsPage.deliveryMethodInputBox, Keys.TAB,"tab key");
		waitForMask();
			
	}
	
	public void setShareOnMemberPortal(String shareOrNot) throws Throwable{		
		new Actions(driver).doubleClick(driver.findElement(SetCheckInstructionsPage.shareOnMemberPortal)).perform();
		waitForMask();
		type(SetCheckInstructionsPage.shareOnMemberPortal,shareOrNot, "shareOnMemberPortal");	
		type(SetCheckInstructionsPage.shareOnMemberPortal, Keys.TAB,"tab key");
	}
	
	public void AdvancedDirectPay(String YesOrNo, String Explaination) throws Throwable{
		if(YesOrNo.equalsIgnoreCase("Yes")){
			click(SetCheckInstructionsPage.AdvancedDirectPayRadioBtn,"AdvancedDirectPayRadioBtn");
			type(SetCheckInstructionsPage.AdvancedDirectPayExplaination,Explaination,"AdvancedDirectPayExplaination");
		}
	}
	
	public void clickFinish() throws Throwable{
		click(SetCheckInstructionsPage.finishBtn,"finishBtn");
		waitForMask();
	}
	
	public void clickLinkDocument() throws Throwable{
		click(SetCheckInstructionsPage.linkDocBtn, "linkDocBtn");
	}
	
	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		setDeliverMethod(data.get("deliverymethod"));
		setShareOnMemberPortal(data.get("ShareOnMemberPortal"));
		AdvancedDirectPay(data.get("advanceddirectPay"), data.get("explainationfordirectpay"));
		clickFinish();
	}
	
	public void clickCancel() throws Throwable{
		click(SetCheckInstructionsPage.cancelBtn,"cancelBtn");
		waitForMask();
		boolean flag = false;
		try{
			flag = driver.findElement(SetCheckInstructionsPage.okBtn).isDisplayed();
		}catch(Exception ex){
			//Do nothing, as element is not expected to be present always
		}
		if(flag){			
			click(SetCheckInstructionsPage.okBtn, "okBtn");
			waitForInVisibilityOfElement(SetCheckInstructionsPage.okBtn,"okBtn");
		}
	}

}
