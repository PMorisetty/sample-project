package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CreateCheckPaymentInformationPageLib extends ActionEngine{
	
	public CreateCheckPaymentInformationPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		this.driver = webDriver;
		this.reporter = reporter;
	}

	
	public void deductibleDecision(String WaivedOrApplied, String waivedReason, String otherComments) throws Throwable{		
		if(WaivedOrApplied.equalsIgnoreCase("Applied")){			
			click(CreateCheckPaymentInformationPage.deductibleDecisionApplied,"WaivedOrApplied");
		}else{
			click(CreateCheckPaymentInformationPage.deductibleDecisionWaived,"WaivedOrApplied");
			type(CreateCheckPaymentInformationPage.waivedReason,waivedReason, "waivedReason");
			type(CreateCheckPaymentInformationPage.waivedReason,Keys.TAB, "tab key");
			type(CreateCheckPaymentInformationPage.otherComments,otherComments, "otherComments");
			type(CreateCheckPaymentInformationPage.otherComments,Keys.TAB, "tab key");
		}
	}
	
	public void setDeductibleType(String deductibleType) throws Throwable{
		if(deductibleType.equalsIgnoreCase("aop")){
			click(CreateCheckPaymentInformationPage.aopDeductibleType, "aopDeductibleType");
		}else{
			click(CreateCheckPaymentInformationPage.specialDeductibleType, "specialDeductibleType");
		}
	}
	
	public void setOverrideDeductible(String overrideDeductible, String deductibleAmount, String overrideReason) throws Throwable{
		if(overrideDeductible.equalsIgnoreCase("yes")){
			click(CreateCheckPaymentInformationPage.overrideDeductibleYes, "overrideDeductibleYes");
			
			type(CreateCheckPaymentInformationPage.deduductibleAmount, deductibleAmount, "deductibleAmount");
			type(CreateCheckPaymentInformationPage.deduductibleAmount, Keys.TAB, "Tab key");
			
			type(CreateCheckPaymentInformationPage.overrideReason, overrideReason, "overrideReason");
			type(CreateCheckPaymentInformationPage.overrideReason, Keys.TAB, "Tab key");
		}
	}

	public void setPaymentAmount(String amount) throws Throwable{
		Actions actions = new Actions(driver);
		actions.doubleClick(driver.findElement(CreateCheckPaymentInformationPage.paymentAmountCell)).perform();
		type(CreateCheckPaymentInformationPage.paymentAmount,amount,"amount");
		type(CreateCheckPaymentInformationPage.paymentAmount, Keys.TAB, "Tab");

	}

}

