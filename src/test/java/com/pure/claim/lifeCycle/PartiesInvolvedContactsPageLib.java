package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PartiesInvolvedContactsPageLib extends ActionEngine{
	
	public PartiesInvolvedContactsPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void clicksearchOrAddExistingContact() throws Throwable{
		click(PartiesInvolvedContactsPage.searchOrAddExistingContactBtn, "SearchOrAddExisting Contact Button");
	}
	
	public boolean checkListedContactsExist(String vendorName) throws Throwable{
		By requiredContact = By.xpath("//div[@id='ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:PeopleInvolvedDetailedLV']//td/div[text()='"+vendorName+"']");
		return isElementPresent(requiredContact, vendorName , true);

	}
	
	public void deleteContact(String vendorName) throws Throwable{
		By selectContact = By.xpath("//div[@id='ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:PeopleInvolvedDetailedLV']//td/div[text()='"+vendorName+"']/../preceding-sibling::td//img");
		click(selectContact, "Select "+vendorName);
		click(PartiesInvolvedContactsPage.deleteBtn, "Delete Button");
	}
	
	public void clickNewPerson() throws Throwable{
		click(PartiesInvolvedContactsPage.newPerson, "New Person Button");
	}
	
	public void verifyNewlyAddedContactInPartiesInvoled(String firstName, String lastName) throws Throwable{
		String contact = firstName+" "+lastName;
		assertMatchingTextInList(PartiesInvolvedContactsPage.contactList, contact, "Parties Involved List");
	}
	
	public void selectContact(String contactName) throws Throwable{
		By contactToBeSelected = By.xpath("//div[@id='ClaimContacts:ClaimContactsScreen:PeopleInvolvedDetailedListDetail:PeopleInvolvedDetailedLV']//td/div[text()='"+contactName+"']");
		click(contactToBeSelected, contactName);
	}

	public void clickEdit() throws Throwable{
		click(PartiesInvolvedContactsPage.editBtn, "edit btn");
	}
	
	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		setCountryName(data.get("countryName"));
		verifyChangesInZipAndPostalCodeAfterChangingCountry();
		setPostalCode(data.get("postalCode"));
		setProvince(data.get("province"));
		setCity(data.get("partiesInvolvedCity"));
	}
	
	public void setCountryName(String countryName) throws Throwable{
		type(PartiesInvolvedContactsPage.country, countryName, "country");
		type(PartiesInvolvedContactsPage.country, Keys.TAB, "Tab key");
	}
	
	public void verifyChangesInZipAndPostalCodeAfterChangingCountry() throws Throwable{
		waitForInVisibilityOfElement(PartiesInvolvedContactsPage.zipCode, "zip code");
		waitForInVisibilityOfElement(PartiesInvolvedContactsPage.state, "state");
		waitForInVisibilityOfElement(PartiesInvolvedContactsPage.county, "county");
		
		waitForVisibilityOfElement(PartiesInvolvedContactsPage.postalCode, "postal code");
		waitForVisibilityOfElement(PartiesInvolvedContactsPage.province, "province");
	}
	
	public void setPostalCode(String postalCode) throws Throwable{
		type(PartiesInvolvedContactsPage.postalCode, postalCode, "postal code");
		type(PartiesInvolvedContactsPage.postalCode, Keys.TAB, "Tab key");
		click(PartiesInvolvedContactsPage.postalCodeIcon,"postalCodeIcon");
		waitForMask();
	}
	
	public void setProvince(String province) throws Throwable{
		type(PartiesInvolvedContactsPage.province, province, "province");
		type(PartiesInvolvedContactsPage.province, Keys.TAB, "Tab key");
	}
	
	public void setCity(String city) throws Throwable{
		type(PartiesInvolvedContactsPage.city, city, "city");
		type(PartiesInvolvedContactsPage.city, Keys.TAB, "Tab key");
	}
	
	public void clickUpdate() throws Throwable{
		click(PartiesInvolvedContactsPage.updateBtn, "update btn");
	}
	
	public void verifyProvince(String expectedProvince) throws Throwable{
		assertTextMatching(PartiesInvolvedContactsPage.location, expectedProvince, "Province");
	}
	
	public void verifyPostalCode(String expectedPostalCode) throws Throwable{
		assertTextMatching(PartiesInvolvedContactsPage.location, expectedPostalCode, "Postal code");
	}
	
	public void verifyPostalCodeErrorMessage() throws Throwable{
		isVisible(PartiesInvolvedContactsPage.postalCodeErrorMsg, "postal code error message");
	}
}