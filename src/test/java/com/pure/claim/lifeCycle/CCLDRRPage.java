package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class CCLDRRPage {

	static By editBtn,
	coverage, 
	compliance, 
	liability, 
	damages, 
	resolutionPlan, 
	reserveAnalysis,
	updateBtn,
	okBtn;
	
	static{
		editBtn = By.xpath("//span[contains(@id,'CLDR_Screen:Edit-btnInnerEl')]");
		coverage = By.xpath("//textarea[contains(@id,'Coverage_TextAreaInput-inputEl')]");
		compliance = By.xpath("//textarea[contains(@id,'Comp_TextAreaInput-inputEl')]");
		liability = By.xpath("//textarea[contains(@id,'Liability_TextAreaInput-inputEl')]");
		damages = By.xpath("//textarea[contains(@id,'Deposition_TextAreaInput-inputEl')]");
		resolutionPlan = By.xpath("//textarea[contains(@id,'Resolution_TextAreaInput-inputEl')]");
		reserveAnalysis = By.xpath("//textarea[contains(@id,'Recoveries_TextAreaInput-inputEl')]");
		updateBtn = By.xpath("//span[contains(@id,'Update-btnInnerEl')]");
		okBtn = By.xpath("//span[text()='OK']");
	}
	
}