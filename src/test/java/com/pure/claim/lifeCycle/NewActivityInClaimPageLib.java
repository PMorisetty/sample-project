package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewActivityInClaimPageLib extends ActionEngine{
	
	public NewActivityInClaimPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void setSubject(String subject) throws Throwable{
		type(NewActivityInClaimPage.subject, subject, "Subject");
		type(NewActivityInClaimPage.subject, Keys.TAB, "TAB");
	}
	
	public String getSubject() throws Throwable{
		return getAttributeByValue(NewActivityInClaimPage.subject, "Subject");
	}
	
	public void setDescription(String description) throws Throwable{
		type(NewActivityInClaimPage.description, description, "Description");
		type(NewActivityInClaimPage.description, Keys.TAB, "TAB");
	}
	
	public void clickUpdate() throws Throwable{
		scroll(NewActivityInClaimPage.updateBtn, "Update Button");
		click(NewActivityInClaimPage.updateBtn, "Update Button");
		waitForMask();
	}
	
	public void clickCancel() throws Throwable{
		scroll(NewActivityInClaimPage.cancelBtn, "Cancel Button");
		click(NewActivityInClaimPage.cancelBtn, "Cancel Button");
		waitForMask();
	}
	
	public void verifyEscalationDate() throws Throwable{
		String escalationDate = getAttributeByValue(NewActivityInClaimPage.escalationDate, "Escalation Date");
		if(escalationDate.isEmpty())
			reporter.failureReport("verifyEscalationDate", "Escalation date is not available", driver);
		else
			reporter.SuccessReport("verifyEscalationDate", "Escalation date is available: "+escalationDate);
	}
	
}
