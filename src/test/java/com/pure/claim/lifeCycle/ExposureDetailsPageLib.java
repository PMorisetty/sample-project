package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ExposureDetailsPageLib extends ActionEngine{
	public ExposureDetailsPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void clickEdit() throws Throwable{
		click(ExposureDetailsPage.editBtn, "editBtn");
	}
	
	public void setClaimantAndType(String cliamant, String type) throws Throwable{
		type(ExposureDetailsPage.claimant, cliamant, "cliamant");
		type(ExposureDetailsPage.claimant, Keys.TAB, "tab key");
		
		type(ExposureDetailsPage.claimantType, type, "claimantType");
		type(ExposureDetailsPage.claimantType, Keys.TAB, "tab key");
	}
	
	public void clickUpdate() throws Throwable{
		click(ExposureDetailsPage.updateBtn, "updateBtn");
	}
	
	public void clickCancel() throws Throwable{
		click(ExposureDetailsPage.cancelBtn, "cancelBtn");
	}
	
	public void verifyDeductibleManagementWorkSheet(String deductibleDecision, String deductibleAmount, String waivedReason, String otherComments, String ovverrideDeductible, String... deductibleType) throws Throwable{
		assertText(ExposureDetailsPage.deductibleDecision, deductibleDecision);
		Double deductibleAmountDouble = Double.parseDouble(deductibleAmount);
		String expctedDeductibleAmount = String.format("%.2f", deductibleAmountDouble);
		assertText(ExposureDetailsPage.deductibleAmount, expctedDeductibleAmount);
		if(deductibleDecision.equalsIgnoreCase("waived")){
			assertText(ExposureDetailsPage.waivedReason, waivedReason);
			assertText(ExposureDetailsPage.otherComments, otherComments);
		}else if(deductibleDecision.equalsIgnoreCase("applied")){
			assertText(ExposureDetailsPage.overrideDecutible, ovverrideDeductible);
		}
		if(deductibleType.length>0){
			String expectedDeductibleType = deductibleType[0];
			assertText(ExposureDetailsPage.deductibleType, expectedDeductibleType);
		}
		
	}
	
	public void clickUpToExposures() throws Throwable{
		click(ExposureDetailsPage.uptoExposures, "uptoExposures");
	}
}
