package com.pure.claim.lifeCycle;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ReopenClaimPageLib extends ActionEngine{
	
	public ReopenClaimPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void clickReopenClaimBtn() throws Throwable{
		click(ReopenClaimPage.reopenClaim, "reopenClaim");
	}

}
