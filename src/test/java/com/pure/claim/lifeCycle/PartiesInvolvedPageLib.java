package com.pure.claim.lifeCycle;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PartiesInvolvedPageLib extends ActionEngine{
	
	public PartiesInvolvedPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void openPartiesInvolved() throws Throwable{
		click(PartiesInvolvedPage.partiesInvolvedLink, "Parties Involved Link");
	}
	
	public void openContacts() throws Throwable{
		click(PartiesInvolvedPage.contactsLink, "Contacts Link");
	}
	
	public void openUsers() throws Throwable{
		click(PartiesInvolvedPage.usersLink, "Users Link");
	}
	
	public void clickEditbutton() throws Throwable{
		click(PartiesInvolvedPage.editBtn, "Edit button");
	}
	
	public void clickUpdatebutton() throws Throwable{
		click(PartiesInvolvedPage.updateBtn, "Update button");
	}
	
	public void clickAddbutton_bank() throws Throwable{
		
		waitForVisibilityOfElement(PartiesInvolvedPage.bankAddBtn, "add button in Bank");
		click(PartiesInvolvedPage.bankAddBtn, "add button in Bank");
	}
	
	public void enterBankDetails(String accountName, String bankName, String accountType, String accountNumber, String routingNumber) throws Throwable{
		//List<WebElement> numColumns = getColumns();
		
		try {
		
		enterValueInCell(driver.findElement(PartiesInvolvedPage.accountNameCell), "AccountName", accountName);
		
		//WebElement bankNameCell = getColumns().get(2).findElement(By.tagName("div"));
		enterValueInCell(driver.findElement(PartiesInvolvedPage.bankNameCell), "BankName", bankName);
		//getColumns().get(3).findElement(By.tagName("div")).sendKeys(Keys.TAB);

		//WebElement accountTypeCell = getColumns().get(4).findElement(By.tagName("div"));//driver.findElement(By.xpath("//div[contains(@id,'ext-gen')]"));
		//type(accountTypeCell,accountType, " typed in accountType");
		  
		clickUntil(PartiesInvolvedPage.accountType_cell, PartiesInvolvedPage.accountType_cell, "AccountType");
		type(PartiesInvolvedPage.accountType_cell,accountType, " typed in accountType");
		type(PartiesInvolvedPage.accountType_cell, Keys.TAB, "key tab");
		//enterValueInCell(accountTypeCell, "AccountType", accountType);
		
		By accountNumber_Cell = By.xpath("//input[@name='BankAccountNumber']");
		clickUntil(accountNumber_Cell, accountNumber_Cell, "AccountNumber");
		type(accountNumber_Cell,accountNumber, " typed in Account Number");
		type(accountNumber_Cell,Keys.TAB,"keys tab");
		//enterValueInCell(accountNumberCell, "AccountNumber", accountNumber);
		
		By routingNumber_Cell = By.xpath("//input[@name='BankRoutingNumber']");
		clickUntil(routingNumber_Cell, routingNumber_Cell, "Routing Number");
		type(routingNumber_Cell,routingNumber, " typed in RoutingNumber");
		type(routingNumber_Cell,Keys.TAB,"keys tab");
		//enterValueInCell(routingNumberCell, "routingNumber", routingNumber);
		
		click(PartiesInvolvedPage.yes_primary, "Primary Yes radio button");
		
		
		} catch (Exception e) {
			System.out.println("error: "+ e.getMessage());
			reporter.failureReport("PartiesInvolvedPageLib element ",
					"Failed to locate element on PartiesInvolvedPage", driver);
			throw new RuntimeException();
			
		}
		
	}
	
	public void enterValueInCell(WebElement cellElement,String cellName, String value) throws Throwable{
		Actions actions = new Actions(driver);
		//actions.doubleClick(cellElement).perform();
		actions.moveToElement(cellElement).doubleClick().perform();
		waitForMask();
		By dynXpth = By.xpath("//input[@name='"+cellName+"']");
		type(dynXpth,value, " typed in "+cellName);
		type(dynXpth,Keys.TAB,"tab key");
		waitForMask();
		
	}
	
	@SuppressWarnings("unchecked")
	private List<WebElement> getColumns(){
		List<WebElement> tableRows = (List<WebElement>) driver.findElement(PartiesInvolvedPage.bankDataTable);
		List<WebElement> tableColumns = tableRows.get(tableRows.size() - 1).findElements(By.tagName("td"));
		return tableColumns;
	}
	
}
