package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class ClusterInfoPage {

	static By serverIDInBatchServer,serverIDInClusterMembers,inClusterNow,menuButton,returnToClaimCenterLink;
	
	static{
		serverIDInBatchServer = By.xpath("//span[text()='Batch Server']/../../../../../../../../..//label[text()='Server ID']/../..//div");
		serverIDInClusterMembers = By.xpath("//span[text()='Server ID']/../../../../../..//tr[$]/td[1]/div");
		inClusterNow = By.xpath("//span[text()='Server ID']/../../../../../..//tr[$]/td[2]/div");
		menuButton = By.id(":TabLinkMenuButton-btnIconEl");
		returnToClaimCenterLink = By.xpath("//span[text()='Return to ClaimCenter']");
	}
}