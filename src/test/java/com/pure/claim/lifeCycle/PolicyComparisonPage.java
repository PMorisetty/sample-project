package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class PolicyComparisonPage {

	static By finishBtn;
	
	static{
		finishBtn = By.id("PolicyRefreshWizard:Finish-btnInnerEl");
	}
}
