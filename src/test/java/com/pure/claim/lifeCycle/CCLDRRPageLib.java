package com.pure.claim.lifeCycle;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CCLDRRPageLib  extends ActionEngine{
	
	public CCLDRRPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void clickEdit() throws Throwable{
		click(CCLDRRPage.editBtn, "Edit Button");
	}
	
	public void setCoverage(String coverage) throws Throwable{
		type(CCLDRRPage.coverage, coverage, "Coverage");
		type(CCLDRRPage.coverage, Keys.TAB, "TAB");
	}
	
	public void setCompliance(String compliance) throws Throwable{
		type(CCLDRRPage.compliance, compliance, "Compliance");
		type(CCLDRRPage.compliance, Keys.TAB, "TAB");
	}
	
	public void setLiability(String liability) throws Throwable{
		type(CCLDRRPage.liability, liability, "Liability");
		type(CCLDRRPage.liability, Keys.TAB, "TAB");
	}
	
	public void setDamages(String damages) throws Throwable{
		type(CCLDRRPage.damages, damages, "Damages");
		type(CCLDRRPage.damages, Keys.TAB, "TAB");
	}
	
	public void setResolutionPlan(String resolutionPlan) throws Throwable{
		type(CCLDRRPage.resolutionPlan, resolutionPlan, "Resolution Plan");
		type(CCLDRRPage.resolutionPlan, Keys.TAB, "TAB");
	}
	
	public void setReserveAnalysis(String reserveAnalysis) throws Throwable{
		type(CCLDRRPage.reserveAnalysis, reserveAnalysis, "Reserve Analysis");
		type(CCLDRRPage.reserveAnalysis, Keys.TAB, "TAB");
	}
	
	public void clickUpdate() throws Throwable{
		click(CCLDRRPage.updateBtn, "Update Button");
	}
	
	public void clickOk() throws Throwable{
		click(CCLDRRPage.okBtn, "Ok Button");
	}
}
