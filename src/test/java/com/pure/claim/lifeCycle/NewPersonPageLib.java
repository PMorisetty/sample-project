package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewPersonPageLib extends ActionEngine{
	
	public NewPersonPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void setFirstName(String firstName) throws Throwable{
		type(NewPersonPage.firstName, firstName, "First Name");
		type(NewPersonPage.firstName, Keys.TAB, "TAB");
	}
	
	public void setLastName(String lastName) throws Throwable{
		type(NewPersonPage.lastName, lastName, "Last Name");
		type(NewPersonPage.lastName, Keys.TAB, "TAB");
	}
	
	public void setMobile(String mobile) throws Throwable{
		type(NewPersonPage.mobile, mobile, "Mobile");
		type(NewPersonPage.mobile, Keys.TAB, "TAB");
	}
	
	public void setEmailMain(String email) throws Throwable{
		type(NewPersonPage.emailMain, email, "Email");
		type(NewPersonPage.emailMain, Keys.TAB, "TAB");
	}
	
	public void setZipCode(String zipCode) throws Throwable{
		type(NewPersonPage.zipCode, zipCode, "Zip Code");
		type(NewPersonPage.zipCode, Keys.TAB, "TAB");
	}
	
	public void setAddressLine1(String address) throws Throwable{
		type(NewPersonPage.address1, address, "Address Line1");
		type(NewPersonPage.address1, Keys.TAB, "TAB");
	}
	
	public void setdateOfBirth(String dateOfBirth) throws Throwable{
		type(NewPersonPage.dateOfBirth, dateOfBirth, "DateOfBirth");
		type(NewPersonPage.dateOfBirth, Keys.TAB, "TAB");
	}
	
	public void setGender(String gender) throws Throwable{
		type(NewPersonPage.gender, gender, "Gender");
		type(NewPersonPage.gender, Keys.TAB, "TAB");
	}
	
	public void fillDetails(Hashtable<String, String> data) throws Throwable
	{
		setFirstName(data.get("firstName"));
		setLastName(data.get("lastName"));
		setMobile(data.get("mobile"));
		setEmailMain(data.get("email"));
		setZipCode(data.get("zipCode"));
		setAddressLine1(data.get("address1"));
		setdateOfBirth(data.get("dateOfBirth"));
		setGender(data.get("gender"));
	}
	
	public void clickAddRole() throws Throwable{
		scroll(NewPersonPage.addRole, "Add Role Button");
		click(NewPersonPage.addRole, "Add Role Button");
	}
	
	public void setRole(String role) throws Throwable{ 
		try {
			//waitForVisibilityOfElement(NewVendorCompanyPage.role, "role");
			WebElement ele = driver.findElement(NewPersonPage.role);
			Actions actions = new Actions(driver);
			actions.moveToElement(ele).doubleClick(ele).perform();
			type(NewPersonPage.roleInput,role,"Role");
			type(NewPersonPage.roleInput, Keys.TAB, "TAB");;
			reporter.SuccessReport("Enter role", "Entered '"+role +"' in role."  );
		} catch (Exception e) {
			reporter.failureReport("AssertElementPresent :: ", role + " is not present in the page :: ", driver);		}
	}
	
	public void clickUpdate() throws Throwable{
		scroll(NewPersonPage.updateBtn, "Update Button");
		click(NewPersonPage.updateBtn, "Update Button");
		waitForMask();
	}
	
}
