package com.pure.claim.lifeCycle;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NotesPageLib extends ActionEngine {

	/*
	 * Navigate to Tree-Notes
	 */
	
	public NotesPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void verifyNotesInfo(Hashtable<String, String> data) throws Throwable{
				
		if (data.get("coverage")!= null){
			assertMatchingTextInList(NotesPage.notesDetailsBody, data.get("coverage"), "Notes");
		}
		if(data.get("compliance")!=null){
			assertMatchingTextInList(NotesPage.notesDetailsBody, data.get("compliance"), "Notes");
		}
		if(data.get("damages")!=null){
			assertMatchingTextInList(NotesPage.notesDetailsBody, data.get("damages"), "Notes");
		}
		if(data.get("resolutionPlan")!=null){
			assertMatchingTextInList(NotesPage.notesDetailsBody, data.get("resolutionPlan"), "Notes");
		}
		if(data.get("reserveAnalysis")!=null){
			assertMatchingTextInList(NotesPage.notesDetailsBody, data.get("reserveAnalysis"), "Notes");
		}		
		if(data.get("liability")!=null){			
			assertMatchingTextInList(NotesPage.notesDetailsBody, data.get("liability"), "Notes");
		}		
		if(data.get("text")!=null){			
			assertMatchingTextInList(NotesPage.notesDetailsBody, data.get("text"), "Notes");
		}
	}
	public void verifyNotesInfo(String text) throws Throwable{
		
		if (text!= null){
			assertMatchingTextInList(NotesPage.notesDetailsBody, text, "Notes");
		}
	}
	
	public void verifyDeductibleMsgInNote(String coverageType, String deductibleAmount, String waivedOrApplied, String overrideDeductible) throws Throwable{
		waivedOrApplied = waivedOrApplied.toLowerCase();
		Double deductibleAmountDouble = Double.parseDouble(deductibleAmount);
		String expctedDeductibleAmount = String.format("%.2f", deductibleAmountDouble);
		String expectedNote = "The "+coverageType+" deductible of $"+expctedDeductibleAmount+" has been "+waivedOrApplied;
		if(overrideDeductible!=null && overrideDeductible.equalsIgnoreCase("yes")){
			expectedNote += ". Note the deductible was overriden";
		}
		assertMatchingTextInList(NotesPage.notesDetailsBody, expectedNote, "Notes");
	}
	
	public void verifyBulkInvoiceDetails(String bulkInvoiceFileName, String costtype, String amount) throws Throwable{
		assertMatchingTextInList(NotesPage.notesDetailsBody, bulkInvoiceFileName, "bulkInvoiceFileName");
		assertMatchingTextInList(NotesPage.notesDetailsBody, costtype, "costtype");
		assertMatchingTextInList(NotesPage.notesDetailsBody, amount, "amount");
	}
	
	
	public void verifyEmailSent() throws Throwable{
		isElementPresent(NotesPage.EmailSent, "EmailSent", true);
	}
	
	public String getAutorText()throws Throwable{

		return getText(NotesPage.author, "Author");
	}
	public String getTopicText()throws Throwable{

		return getText(NotesPage.topic, "Topic");
	}
}