package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class SetRecoveryReservesPage {

	static By addBtn, newReserve, saveBtn, exposure, costType, costCategory, newOpenRecoveryReserve;

	static {
		addBtn = By.xpath("//span[@id='NewRecoveryReserveSet:NewReserveSetScreen:Add-btnInnerEl']");
		newReserve = By.xpath("//div[@id='NewRecoveryReserveSet:NewReserveSetScreen:RecoveryReservesSummaryDV:EditableRecoveryReservesLV-body']");
		saveBtn = By.xpath("//span[@id='NewRecoveryReserveSet:NewReserveSetScreen:Update-btnInnerEl']");
		exposure = By.xpath("//div[@id='NewRecoveryReserveSet:NewReserveSetScreen:RecoveryReservesSummaryDV:EditableRecoveryReservesLV-body']//tbody/tr/td[2]/div");
		costType = By.xpath("//div[@id='NewRecoveryReserveSet:NewReserveSetScreen:RecoveryReservesSummaryDV:EditableRecoveryReservesLV-body']//tbody/tr/td[4]/div");
		costCategory = By.xpath("//div[@id='NewRecoveryReserveSet:NewReserveSetScreen:RecoveryReservesSummaryDV:EditableRecoveryReservesLV-body']//tbody/tr/td[5]/div");
		newOpenRecoveryReserve = By.xpath("//div[@id='NewRecoveryReserveSet:NewReserveSetScreen:RecoveryReservesSummaryDV:EditableRecoveryReservesLV-body']//tbody/tr/td[7]/div");
	}
}
