package com.pure.claim.lifeCycle;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.report.CReporter;

public class SetReservesPageLib extends ActionEngine{

	public SetReservesPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable{
		this.driver = webDriver;
		this.reporter = reporter;
		waitForMask();
		printPageTitle(SetReservesPage.title,"Page title");
	}
	
	public void addNewReserveFromActionsMenu(Hashtable<String, String> data) throws Throwable{
		
		ActionsPageLib ActionsPageLib = new ActionsPageLib(driver, reporter);
		ActionsPageLib.clickAndHoverOnActions();
		ActionsPageLib.clickOn("Reserve");
		addNewReserve(data);
	}
	
	public void addNewReserveFromExposuresPage(Hashtable<String, String> data) throws Throwable{
		ExposuresPageLib exposuresPageLib = new ExposuresPageLib(driver, reporter);
		exposuresPageLib.selectExposure();
		exposuresPageLib.clickCreateReserve();
		addNewReserve(data);
	}
	
	private void addNewReserve(Hashtable<String, String> data) throws Throwable{

		String reporterName = data.get("reportername");
		String CostTpe = data.get("costtype");
		String CostCategory = data.get("costcategory");
		String NewAvailableReserve= data.get("newavailablereserve");
		
		lobVariables lobVariables = new lobVariables();
		boolean flag = false;
		if(lobVariables.getValueInt("numOfExposures")>0){	
			if(isElementPresent(SetReservesPage.unfilledCellExposure, "Unfilled Reserve cell exposure") ||
					(isElementPresent(SetReservesPage.unfilledCellCostType, "Unfilled Reserve cell cost type"))){
				//click(SetReservesPage.addBtn,"Add Reserver btn");
				//String firstExposure = "//li[contains(text(),'" + reporterName +"')]";
				//driver.findElement(By.xpath(firstExposure)).click();
				flag = true;
			}else{
				click(SetReservesPage.addBtn,"Add Reserver btn");
				flag = true;
			}
			if (flag){
				enterReserveDetails(reporterName, CostTpe, CostCategory, NewAvailableReserve);
				waitForVisibilityOfElement(SetReservesPage.saveBtn, "Save btn");
				Thread.sleep(1000);
				scroll(SetReservesPage.saveBtn, "scroll to save btn");
				Thread.sleep(2000);
				JSClick(SetReservesPage.saveBtn, "Save btn");
				reporter.SuccessReport("", "Successfully added reserve");
			}
		}
	}
	
	public void enterReserveDetails(String reporterName, String costType, String CostCategory, String NewAvailableReserve) throws Throwable{
		List<WebElement> numColumns = getColumns();
		
		WebElement exposureCell = numColumns.get(1).findElement(By.tagName("div"));
		enterValueInCell(exposureCell, "Exposure", reporterName);
		
		WebElement costTypeCell = getColumns().get(3).findElement(By.tagName("div")); 
		enterValueInCell(costTypeCell, "CostType", costType);		

		WebElement costcategoryCell = getColumns().get(4).findElement(By.tagName("div")); 
		enterValueInCell(costcategoryCell, "CostCategory", CostCategory);
		
		WebElement newAvailableReserveCell = getColumns().get(7).findElement(By.tagName("div")); 
		enterValueInCell(newAvailableReserveCell, "NewAmount", NewAvailableReserve);
	}

	public void enterValueInCell(WebElement cellElement,String cellName, String value) throws Throwable{
		Actions actions = new Actions(driver);
		//actions.doubleClick(cellElement).perform();
		actions.moveToElement(cellElement).doubleClick().perform();
		waitForMask();
		By dynXpth = By.xpath("//input[@name='"+cellName+"']");
		type(dynXpth,value, " typed in "+cellName);
		type(dynXpth,Keys.TAB,"tab key");
		Thread.sleep(1000);//inevitable
		waitForMask();
		
	}
	
	public void setCostTypeAndCategory(String value) throws Throwable{
		switch (value) {
		case "none":
			click(SetReservesPage.costTypeNone,"costTypeNone");
			break;
		case "Claim Cost":
			click(SetReservesPage.costTypeClaimCost ,"costTypeClaimCost");
			break;
		case "Expense - A&O":
			click(SetReservesPage.costTypeExpenseAO ,"costTypeExpenseAO");
			break;
		case "Auto body":
			click(SetReservesPage.costCategoryAutoBody ,"costCategoryAutoBody");
			break;
		case "Reimbursement":
			click(SetReservesPage.costCategoryDeductibleReimbursement ,"costCategoryDeductibleReimbursement");
			break;
		case "Glass":
			click(SetReservesPage.costCategoryGlass ,"costCategoryGlass");
			break;
		case "Legal":
			click(SetReservesPage.costCategoryLegal ,"costCategoryLegal");
			break;
		case "Mileage reimbursement":
			click(SetReservesPage.costCategoryMileageReimbursement ,"costTypeExpenseDCC");
			break;
		case "Other":
			click(SetReservesPage.costCategoryOther ,"costCategoryOther");
			break;
		case "Rental":
			click(SetReservesPage.costCategoryRental ,"costCategoryRental");
			break;
		case "Salvage":
			click(SetReservesPage.costCategorySalvage ,"costCategorySalvage");
			break;
		case "Towing":
			click(SetReservesPage.costCategoryTowing ,"costCategoryTowing");
			break;			
		default:
			break;
		}
	}
	//Will return column w.r.t. last row in table
	private List<WebElement> getColumns(){
		List<WebElement> tableRows = driver.findElement(SetReservesPage.newReserve).findElement(By.tagName("table")).findElement(By.tagName("tbody")).findElements(By.tagName("tr"));
		List<WebElement> tableColumns = tableRows.get(tableRows.size() - 1).findElements(By.tagName("td"));
		return tableColumns;
	}
	
	public void clickSave() throws Throwable{
		scroll(SetReservesPage.saveBtn, "scroll to save btn");
		click(SetReservesPage.saveBtn, "Save btn");
		waitForMask();
			
	}

	public void enterNewAvailableReserveValueAlone(String newAvaialableReserve) throws Throwable{
		WebElement newAvailableReserveCell = getColumns().get(7).findElement(By.tagName("div")); 
		enterValueInCell(newAvailableReserveCell, "NewAmount", newAvaialableReserve);
	}
	
	public void verifyChangeAmount(String expectedChangeAmount) throws Throwable{
		WebElement changeAmountElement = getColumns().get(8).findElement(By.tagName("div"));
		String actualChangeAmount = changeAmountElement.getText();
		assertTextStringContains(actualChangeAmount, expectedChangeAmount);
		JSClick(SetReservesPage.saveBtn, "Save btn");
		reporter.SuccessReport("", "Successfully edited available reserve amount");
	}
	
	public void clickCancel() throws Throwable{
		JSClick(SetReservesPage.cancelBtn, "Cancel btn");
		reporter.SuccessReport("", "Successfully clicked on cancel btn");
	}
	
}


