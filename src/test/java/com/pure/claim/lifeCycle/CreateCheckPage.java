package com.pure.claim.lifeCycle;

import org.openqa.selenium.By;

public class CreateCheckPage{

	static By cancelBtn, nextBtn, backBtn, finishBtn, duplicateClaimTab,
	closeButton;
	
	static{
		cancelBtn = By.xpath("//span[contains(@id, 'Cancel-btnInnerEl')]");
		nextBtn = By.xpath("//span[contains(@id, 'Next-btnInnerEl')]");
		backBtn = By.xpath("//span[contains(@id, 'Prev-btnInnerEl')]");
		finishBtn = By.xpath("//span[contains(@id, 'Finish-btnInnerEl')]");
		duplicateClaimTab = By.id("wsTabBar:wsTab_0-btnInnerEl");
		closeButton = By.cssSelector("span[id$='DuplicatesWorksheet_CloseButton-btnInnerEl']");
	}
}