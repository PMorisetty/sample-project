package com.pure.claim;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewVehiclePageLib extends ActionEngine{
	
	public NewVehiclePageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable{
		this.driver = driver;
		this.reporter = reporter;
		waitForMask();
		printPageTitle(NewVehiclePage.title, "Page title");
	}
	
	public void setMake(String make) throws Throwable{
		type(NewVehiclePage.vehicleMake, make, "Vehicle make");
	}

	public void setModel(String model) throws Throwable{
		type(NewVehiclePage.vehicleModel, model, "Vehicle model");
	}
	
	public void setYear(String year) throws Throwable{
		type(NewVehiclePage.vehicleYear, year, "Vehicle year");
	}
	
	public void clickOk() throws Throwable{
		click(NewVehiclePage.okBtn, "Ok Button");
		waitForMask();
		printPageTitle(NewClaimPage.title,"Page title");
	}
	
}
