package com.pure.claim;

import org.openqa.selenium.By;

public class SaveAndAssignClaimPage {
	static By title, assignClaimAndAllExposuresOption;
	static By assignDropDown, newExposuresDropDown;
	static By note,finishButton,validationResultsTab;
	static By clearButton,exposuresAdded, assignPicker;
	
	static{
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		assignClaimAndAllExposuresOption = By.cssSelector("input[id$='CommonAssignChoice_Choice-inputEl']");
		assignDropDown = By.cssSelector("input[id$='CommonAssign-inputEl']");
		newExposuresDropDown = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_AssignSaveScreen:NewExposureLV_tb:AddExposure-btnWrap");
		note = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_AssignSaveScreen:Note-inputEl");
		finishButton = By.id("FNOLWizard:Finish-btnInnerEl");
		validationResultsTab = By.id("wsTabBar:wsTab_0-btnInnerEl");
		clearButton = By.cssSelector("span[id$='WebMessageWorksheet_ClearButton-btnInnerEl']");
		exposuresAdded = By.xpath("//div[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_AssignSaveScreen:NewExposureLV-body']//tr]");
		assignPicker = By.cssSelector("a[id$='CommonAssign:CommonAssign_PickerButton']");
	}
}