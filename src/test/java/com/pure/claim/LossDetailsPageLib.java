package com.pure.claim;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class LossDetailsPageLib extends ActionEngine{

	public LossDetailsPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void verifyLossItemsText(String expectedText) throws Throwable{
		assertText(LossDetailsPage.lossDetailsText, expectedText);
	}
	
	public void verifyLossItemVehicle(String expectedVehicleMake) throws Throwable{
		assertText(LossDetailsPage.vehicleMake, expectedVehicleMake);
	}
	
	public void verifyLossItemVessel(String expectedVesselModel) throws Throwable{
		assertText(LossDetailsPage.watercraftModel, expectedVesselModel);
	}
	
	public void addLossItemIncident(String lossItem) throws Throwable{
		click(LossDetailsPage.editBtn, "editBtn");
		waitForMask();
		click(LossDetailsPage.addLossItemBtn, "addLossItemBtn");
		switch(lossItem){
			case "Dwelling":
				click(LossDetailsPage.dwellingLossItem, "dwellingLossItem");
				break;
			case "Personal Property":
				click(LossDetailsPage.personalPropertyLossItem, "personalPropertyLossItem");
				break;
			case "Property Liability":
				click(LossDetailsPage.propertyLiabilityLossItem, "propertyLiabilityLossItem");
				break;
		}
	}
	
	public void clickUpdate() throws Throwable{
		click(LossDetailsPage.updateBtn, "updateBtn");
		if(isVisibleOnly(LossDetailsPage.ClearValidationAlert, "ClearValidationAlert")){
			clickClearValidationAlert();
			clickUpdate();
		}
	}
	
	public void clickEdit() throws Throwable{
		click(LossDetailsPage.editBtn, "editBtn");
	}
	
	public void setLossDate(String dateOfLoss) throws Throwable{
		type(LossDetailsPage.dateOfLoss, dateOfLoss, "dateOfLoss");
		type(LossDetailsPage.dateOfLoss, Keys.TAB, "tab key");
	}
	
	public void clickISO() throws Throwable{
		click(LossDetailsPage.isoLink, "ISO link");
	}
	
	public void clickDetails() throws Throwable{
		click(LossDetailsPage.detailsLink, "Details link");
	}
	
	public void verifyISOButtonsAreDisplayed() throws Throwable{
		waitForVisibilityOfElement(LossDetailsPage.editBtn, "editBtn");
		waitForVisibilityOfElement(LossDetailsPage.sendToISOBtn, "sendToISOBtn");
		waitForVisibilityOfElement(LossDetailsPage.refereshResponsesBtn, "refereshResponsesBtn");
	}
	
	public void verifyIsoStatus(String expectedStatus) throws Throwable{
		assertText(LossDetailsPage.isoStatus, expectedStatus);
	}
	
	public void verifyIsoDateSent() throws Throwable{
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy h:mm a");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		assertTextMatching(LossDetailsPage.isoDateSent, strDate, "ISO date sent");
	}
	
	public String verifyIsoLastResponse() throws Throwable{
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy h:mm a");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		assertTextMatching(LossDetailsPage.isoLastResponse, strDate, "ISO Last Response");
		sdfDate = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
		strDate = sdfDate.format(now);
		return strDate;
	}
	
	public void verifyKnownToISO(String expectedKnownToISO) throws Throwable{
		assertTextMatching(LossDetailsPage.knownToISO, expectedKnownToISO, "ISO Refresh Response");
	}
	
	public void verifyIsoDateSentIsEmpty() throws Throwable{
		String dateSent = getText(LossDetailsPage.isoDateSent, "Date sent to ISO");
		if(dateSent.equalsIgnoreCase("")){
			reporter.SuccessReport("Verify Date Sent To ISO is empty", "Date Sent to ISO is empty");
		}else{
			reporter.failureReport("Verify Date Sent To ISO is empty", "Date Sent to ISO is not empty", driver);
		}
	}
	
	public void verifyIsoLastResponseIsEmpty() throws Throwable{
		String dateSent = getText(LossDetailsPage.isoLastResponse, "Last Response from ISO");
		if(dateSent.equalsIgnoreCase("")){
			reporter.SuccessReport("Verify Last Response from ISO is empty", "Last Response from ISO is empty");
		}else{
			reporter.failureReport("Verify Last Response from ISO is empty", "Last Response from ISO is not empty", driver);
		}
	}
	
	public void verifyknownToISOIsEmpty() throws Throwable{
		String dateSent = getText(LossDetailsPage.knownToISO, "known to ISO");
		if(dateSent.equalsIgnoreCase("")){
			reporter.SuccessReport("known to  ISO is empty", "known to  ISO is empty");
		}else{
			reporter.failureReport("known to from ISO is empty", "known to ISO is not empty", driver);
		}
	}
	
	public void verifySendToIsoButtonIsDisabled() throws Throwable{
		String disabledClassName = "x-item-disabled x-disabled x-btn-disabled x-btn-default-toolbar-small-disabled";
		String attributeValue = getAttributeByClass(LossDetailsPage.sendToISOBtnAncestor, "sendToISOBtn");
		assertTextStringContains(attributeValue, disabledClassName);
		
	}
	
	public void clickSendToIsoButton() throws Throwable{
		By statusBy = LossDetailsPage.isoStatus;
		WebElement ele = driver.findElement(statusBy);
		String status = ele.getText();
		if(!status.equalsIgnoreCase("Sent")){
			click(LossDetailsPage.sendToISOBtn, "sendToISOBtn");
		}
	}
	
	public void waitUntilLastResponseFromISOIsDisplayed() throws Throwable{
		boolean isPresent = false;
		int attempts = 0;
		WebElement refreshResponseElement = null;
		
		
		while(!isPresent && attempts < 1000){
			try{
				refreshResponseElement = driver.findElement(LossDetailsPage.refereshResponsesBtn);
			}catch(Exception e){}
			try{
				WebElement lastResponseElement = driver.findElement(LossDetailsPage.isoLastResponse);
				if(lastResponseElement.getText().length()>0){
					isPresent = true;
					break;
				}else{
					//below code clicks multiple times until reponse is displayed
					//so don't use click of ActionEngine class which prints in report
					Actions actions = new Actions(this.driver);
					actions.click(refreshResponseElement);
					actions.build().perform();
					waitForMask();
				}
			} catch (Exception e) {}
			attempts++;
		}
		if(isPresent){
			reporter.SuccessReport("IsVisible : ", "Last response from ISO is Visible");
		}
		else{
			reporter.failureReport("IsVisible : ", "Last response from ISO is Not Visible : ", driver);
		}
		
	}
	
	public void setLossDescription(String description) throws Throwable{
		type(LossDetailsPage.lossDescription, description, "description");
		type(LossDetailsPage.lossDescription, Keys.TAB, "tab key");
	}

	public void setFaultRating(String faultRating) throws Throwable{
		type(AddClaimInfoPage.faultRating, faultRating, "Fault Rating");
		type(AddClaimInfoPage.faultRating, Keys.TAB, "Tab Click");
	}
	
	public void setInsuredLiability(String insuredLiability) throws Throwable{
		type(LossDetailsPage.insuredLiability, insuredLiability, "Insured Liability %");
		type(LossDetailsPage.insuredLiability, Keys.TAB, "Tab Click");
	}
	
	public void setSubrogatableRadioBtn(String status) throws Throwable{
		if("Yes".equalsIgnoreCase(status))
			click(LossDetailsPage.subrogatableRadioBtnYES, "Subrogatable RadioBtn YES");
		else if("No".equalsIgnoreCase(status))
			click(LossDetailsPage.subrogatableRadioBtnNO, "Subrogatable RadioBtn NO");
	}
	
	public void setReferToSubroRadioBtn(String status) throws Throwable{
		if("Yes".equalsIgnoreCase(status))
			click(LossDetailsPage.referToSubroRadioBtnYES, "referToSubro RadioBtn YES");
		else if("No".equalsIgnoreCase(status))
			click(LossDetailsPage.referToSubroRadioBtnNO, "referToSubro RadioBtn NO");
	}
	
	public void clickClearValidationAlert() throws Throwable{
		click(LossDetailsPage.ClearValidationAlert, "ClearValidationAlert");
	}

	public void verifySubrogationStatus(String subrogationStatus) throws Throwable{
		assertText(LossDetailsPage.subrogationStatus, subrogationStatus);		
	}
	
	public void selectVehicle(String vehicleName) throws Throwable{
		By vehicleToSelect = By.xpath("//a[contains(text(),'"+vehicleName+"')]");
		click(vehicleToSelect,"vehicleToSelect:"+vehicleName);
	}
	
	public void verifySubrogatableStatus(String subrogatableStatus) throws Throwable{
		assertText(LossDetailsPage.subrogatableStatus, subrogatableStatus);		
	}
	
	public void clickAssignToXactAnalysis() throws Throwable{
		click(LossDetailsPage.assignToXactAnalysis, "AssignToXactAnalysis");
	}
	
	public void setLargeLossRadioBtn(String status) throws Throwable{
		if("Yes".equalsIgnoreCase(status))
			click(LossDetailsPage.largeLossRadioBtnYES, "LargeLoss RadioBtn YES");
		else if("No".equalsIgnoreCase(status))
			click(LossDetailsPage.largeLossRadioBtnNO, "LargeLoss RadioBtn NO");
	}
 }
