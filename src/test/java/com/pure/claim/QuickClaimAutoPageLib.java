package com.pure.claim;

import java.util.Hashtable;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class QuickClaimAutoPageLib extends ActionEngine{

	public QuickClaimAutoPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable{
		this.driver = driver;
		this.reporter = reporter;
		waitForMask();
		printPageTitle(QuickClaimAutoPage.title,"Page title");
	}

	public void selectIncidentOnlyYes() throws Throwable {
		click(QuickClaimAutoPage.incidentOnlyYes,
				"Radio button Incident Only YES");
	}

	public void selectIncidentOnlyNo() throws Throwable {
		click(QuickClaimAutoPage.incidentOnlyNo,
				"Radio button Incident Only NO");
	}
	
	public void selectCoverageInQuestionYes() throws Throwable {
		click(QuickClaimAutoPage.coverageQuestionYes,
				"Radio button Coverage In Question YES");
	}

	public void selectCoverageInQuestionNo() throws Throwable {
		click(QuickClaimAutoPage.coverageQuestionNo,
				"Radio button Coverage In Question NO");
	}
	
	public void setZip(String zip) throws Throwable {
		type(QuickClaimAutoPage.zip, zip, "ZIP text box");
		click(QuickClaimAutoPage.zipIcon,"ZipIcon");
		Thread.sleep(2000);//Inevitable: Auto data fill is happening in the background
	}
	
	public void setAddress1(String address1) throws Throwable {
		//isElementPresent(AddClaimInfoPage.addressLine1, address1);
		type(QuickClaimAutoPage.addressLine1, address1, "Address1 text box");
		type(QuickClaimAutoPage.addressLine1, Keys.TAB, "Tab key");
	}
	
	public void setAddress2(String address2) throws Throwable {
		//isElementPresent(AddClaimInfoPage.addressLine2, address2);
		type(QuickClaimAutoPage.addressLine2, address2, "Address1 text box");
		type(QuickClaimAutoPage.addressLine2, Keys.TAB, "Tab key");
	}
	
	public void setCity(String city) throws Throwable {
		type(QuickClaimAutoPage.city, city, "City text box");
		type(QuickClaimAutoPage.city, Keys.TAB, "Tab key");
		waitForMask();
	}

	public void setLossLocation(Hashtable<String, String> data) throws Throwable {

		setZip(data.get("lossLocationZip"));
		setAddress1(data.get("lossLocationAddress1"));
		setAddress2(data.get("lossLocationAddress2"));
		setCity(data.get("city"));
	}
	
	public void setLossCatogery(String lossCategory) throws Throwable {
		WebElement element = driver.findElement(QuickClaimAutoPage.lossCategory);		
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

		//isElementPresent(AddClaimInfoPage.lossCategory, lossCategory);
		type(QuickClaimAutoPage.lossCategory, lossCategory,
				"LossCategory dropdown");
		type(QuickClaimAutoPage.lossCategory, Keys.TAB,
				"Tab Click");
	}
	
	public void setLossCause(String lossCause) throws Throwable {
		type(QuickClaimAutoPage.lossCause, lossCause, "LossCause dropdown");
		type(QuickClaimAutoPage.lossCause, Keys.TAB,
				"Tab Click");
	}

	public void setLossDescription(String lossDescription) throws Throwable {
		type(QuickClaimAutoPage.description, lossDescription, "lossDescription");
		type(QuickClaimAutoPage.description, Keys.TAB, "Tab Click");
	}
	public void setLossDetails(Hashtable<String, String> data) throws Throwable{
		if(data.get("lossCategory") != null){
			setLossCatogery(data.get("lossCategory").toString());
		}
		if(data.get("lossCause") != null){
			setLossCause(data.get("lossCause").toString());
		}
		
		if(data.get("incidentOnly") != null){
			if(data.get("incidentOnly").toString().equalsIgnoreCase("Yes")){
				selectIncidentOnlyYes();
			}else{
				selectIncidentOnlyNo();
			}
		}
		if(data.get("coverageInQuestion") != null){
			if(data.get("coverageInQuestion").toString().equalsIgnoreCase("Yes")){
				selectCoverageInQuestionYes();
			}else{
				selectCoverageInQuestionNo();
			}
		}
	}
	
	public void selectName(String name, String secondaryapprovalusername) throws Throwable{
		type(QuickClaimAutoPage.assignDropDown, name, " Name in assignment dropdown");
		type(QuickClaimAutoPage.assignDropDown, Keys.TAB, "TAB Key");
		if(!getAttributeByValue(QuickClaimAutoPage.assignDropDown, " assign DropDown ").equalsIgnoreCase(name)){
			click(QuickClaimAutoPage.assignPicker," assignPicker search whom to assign ");
			new SearchAssignerPageLib(driver, reporter).findUserAndAssign(secondaryapprovalusername);
		}
	}

	public void setReportedBy(String reporterName) throws Throwable{
		type(QuickClaimAutoPage.reporterName, reporterName, "Reporter Name");
		type(QuickClaimAutoPage.reporterName, Keys.TAB, "TAB key");
	}
	
	public void setRelationToInsured(String relationToInsured) throws Throwable{
		type(QuickClaimAutoPage.relationToInsured, relationToInsured.toString(), "Relation To Insured");
		type(QuickClaimAutoPage.relationToInsured, Keys.TAB, "TAB key");
	}
	
	public void clickFinish() throws Throwable{
		click(QuickClaimAutoPage.finishButton, "Finish Button");
		boolean duplicateClaimFlag = false;
		boolean validationResultsFlag = false;
		try{
			duplicateClaimFlag = driver.findElement(QuickClaimAutoPage.duplicateClaimTab).isDisplayed();
		}catch(Exception ex){
			//Do nothing, as element is not expected to be present always
		}
		if(duplicateClaimFlag){			
			isElementPresent(QuickClaimAutoPage.closeButton, "Close button");
			click(QuickClaimAutoPage.closeButton, "Close duplicate claims pop up");
			waitForInVisibilityOfElement(QuickClaimAutoPage.duplicateClaimTab,"Duplicate claim tab");
			click(QuickClaimAutoPage.finishButton, "Finish Button after closing duplicate claims tab");
			waitForMask();
		}
		try{
			validationResultsFlag = driver.findElement(QuickClaimAutoPage.validationResultsTab).isDisplayed();
		}catch(Exception ex){
			//Do nothing, as element is not expected to be present always
		}
		if(validationResultsFlag){
			click(QuickClaimAutoPage.clearButton, "Clear validation errors in pop up");
			click(QuickClaimAutoPage.finishButton, "Finish Button after clearing validation errors");
			waitForMask();
		}
	}
	
	public void addVehicleIncident() throws Throwable{
		click(QuickClaimAutoPage.addVehicle, "Add New Vehicle Incident");
		waitForMask();
	}
	
}
