package com.pure.claim;

import org.openqa.selenium.Keys;

import com.pure.accelerators.ActionEngine;

public class CoverageTermPageLib extends ActionEngine{
	public void setApplicableTo(String applicableTo) throws Throwable{
		type(CoverageTermPage.applicableTo, applicableTo, "Applicable To");
		type(CoverageTermPage.applicableTo, Keys.TAB, "TAB key");
	}
	public void setPer(String per) throws Throwable{
		type(CoverageTermPage.per, per, "Per");
		type(CoverageTermPage.per, Keys.TAB, "TAB key");
	}
	public void setCode(String code) throws Throwable{
		type(CoverageTermPage.code, code, "Code");
		type(CoverageTermPage.code, Keys.TAB, "TAB key");
	}
	public void setDescription(String description) throws Throwable{
		type(CoverageTermPage.description, description, "Description");
	}
	public void setValue(String value) throws Throwable{
		type(CoverageTermPage.value, value, "Value");
	}
	public void setUnits(String units) throws Throwable{
		type(CoverageTermPage.units, units, "Units");
	}
	public void setAmount(String amount) throws Throwable{
		type(CoverageTermPage.amount, amount, "Amount");
	}
	public void clickOkButton() throws Throwable{
		click(CoverageTermPage.okButton, "Ok Button");
	}
	public void clickCancelButton() throws Throwable{
		click(CoverageTermPage.cancelButton, "Cancel Button");
	}
	public void verifyCoverageType(String coverageType) throws Throwable{
		assertTextStringMatching(coverageType, getAttributeValue(CoverageTermPage.typeLabel, "innerText"));
	}

}
