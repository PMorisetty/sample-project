package com.pure.claim;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewVesselPageLib extends ActionEngine{
	
	public NewVesselPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable{
		this.driver = driver;
		this.reporter = reporter;
		waitForMask();
		printPageTitle(NewVesselPage.title,"Page title");
	}
	
	public void addVesselDetails(Hashtable<String, String> data) throws Throwable{
		String year = data.get("vesselYear");
		String length = data.get("vesselLength");
		String manufacturer = data.get("vesselManufacturer");
		String model = data.get("vesselModel");
		String hull = data.get("vesselHull");
		
		type(NewVesselPage.year, year, "vesselYear");
		type(NewVesselPage.length, length, "vesselLength");
		type(NewVesselPage.manufacturer, manufacturer, "vesselManufacturer");
		type(NewVesselPage.model, model, "vesselModel");
		type(NewVesselPage.hull, hull, "vesselHull");
		
		click(NewVesselPage.okBtn, "Ok button");
		waitForMask();
		printPageTitle(NewVesselPage.title,"Page title");
	}

}
