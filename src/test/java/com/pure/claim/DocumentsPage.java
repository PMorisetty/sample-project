package com.pure.claim;

import org.openqa.selenium.By;

public class DocumentsPage {
	static By title, cancel, name, searchBtn, resetBtn, viewClaimDocumentsBtn, refereshDocumentsBtn, documentsView, selectBtn, docName, docType, claimDocName, searchedDocName,searchedDocType ;
	static{
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		cancel = By.id("PickExistingDocumentPopup:Claim_DocumentsScreen:PickExistingDocumentPopup_CancelButton-btnInnerEl");
		name = By.xpath("//input[@id='PickExistingDocumentPopup:Claim_DocumentsScreen:ClaimDocumentSearchDV:NameOrID-inputEl']");
		viewClaimDocumentsBtn = By.id("ClaimDocuments:Claim_DocumentsScreen:ToolbarButton-btnInnerEl");
		refereshDocumentsBtn = By.id("ClaimDocuments:Claim_DocumentsScreen:refreshButton-btnInnerEl");
		searchBtn = By.xpath("//a[contains(@id,'SearchAndResetInputSet:SearchLinksInputSet:Search')]");
		resetBtn = By.xpath("//a[contains(@id,'SearchAndResetInputSet:SearchLinksInputSet:Reset')]");
		documentsView = By.xpath("//a[text()='View']");
		selectBtn = By.id("PickExistingMultipleDocumentPopup:Claim_DocumentsScreen:PickExistingDocumentsLV_tb:LinkDocument_Button-btnInnerEl");
		docName = By.xpath("//div[@id='ClaimDocuments:Claim_DocumentsScreen:DocumentsLV-body']//td[2]/div");
		docType = By.xpath("//div[@id='ClaimDocuments:Claim_DocumentsScreen:DocumentsLV-body']//td[3]/div");
	    claimDocName = By.xpath("//input[@id='ClaimDocuments:Claim_DocumentsScreen:ClaimDocumentSearchDV:NameOrID-inputEl']");
	
	}
}
