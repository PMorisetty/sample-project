	package com.pure.claim;

	import java.util.ArrayList;
	import java.util.HashMap;

	import org.openqa.selenium.By;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.support.events.EventFiringWebDriver;

	import com.pure.accelerators.ActionEngine;
	import com.pure.claim.lifeCycle.EnumAutoExposureTypes;
	import com.pure.claim.lifeCycle.ExposuresPage;
	import com.pure.report.CReporter;

	public class SaveAndAssignClaimLib  extends ActionEngine {
	
		public SaveAndAssignClaimLib(EventFiringWebDriver webDriver, CReporter reporter) {
			this.driver = webDriver;
			this.reporter = reporter;
		}

		public void selectAssignClaimAndAllExposuresOption() throws Throwable{
			click(SaveAndAssignClaimPage.assignClaimAndAllExposuresOption, "assignClaimAndAllExposuresOption");
		}

		public void selectName(String... strings) throws Throwable{
			//1st value=appraiser name
			//2nd value = secondaryapprovalusername		
			String name = strings[0];
			type(SaveAndAssignClaimPage.assignDropDown, name, " Name in assignment dropdown");
			type(SaveAndAssignClaimPage.assignDropDown, Keys.TAB, "TAB Key");
			
			if(strings.length>1){
				String secondaryapprovalusername = strings[1];
				if(secondaryapprovalusername!=null && !getAttributeByValue(SaveAndAssignClaimPage.assignDropDown, " assign DropDown ").equalsIgnoreCase(name)){
					click(SaveAndAssignClaimPage.assignPicker," assignPicker search whom to assign ");
					new SearchAssignerPageLib(driver, reporter).findUserAndAssign(secondaryapprovalusername);
				}
			}
		
		}			
	

		public void setNote(String note) throws Throwable{
			type(SaveAndAssignClaimPage.note, note, "Additional note");
		
		}

		public void clickFinish() throws Throwable{
			click(SaveAndAssignClaimPage.finishButton, "Finish Button");
			waitForMask();
			boolean flag = false;
			try{
				flag = driver.findElement(SaveAndAssignClaimPage.validationResultsTab).isDisplayed();
			}catch(Exception ex){
				//Do nothing, as element is not expected to be present always
			}
			if(flag){
				click(SaveAndAssignClaimPage.clearButton, "Clear validation errors in pop up");
				click(SaveAndAssignClaimPage.finishButton, "Finish Button after clearing validation errors");
				waitForMask();
			}
		}


		public void addNewExposure(String policylevelcoverage, String exposuretype, String exposuretype2) throws Throwable{
			/*
			scroll(ExposuresPage.NewExposureBtn,"New Exposure btn");
			click(ExposuresPage.NewExposureBtn,"NewExposureBtn");
			Thread.sleep(1000);//The exposure expanding menu is resettling down. Need to re-click.
		
			click(ExposuresPage.NewExposureBtn,"NewExposureBtn");
			By elemPolicylevelcoverage = By.xpath("//span[contains(text(),'" + policylevelcoverage +"')]/../..");
			boolean flag = false;
			flag = isElementPresent(elemPolicylevelcoverage,"Exposure dropdown");
			if(!flag){
				click(ExposuresPage.NewExposureBtn,"NewExposureBtn");
			}
			*/
			By elemPolicylevelcoverage = By.xpath("//span[contains(text(),'" + policylevelcoverage +"')]/../..");
			clickUntil(ExposuresPage.NewExposureBtn, elemPolicylevelcoverage, "New Exposure Button");
			waitForMask();
			clickUntil(ExposuresPage.NewExposureBtn, elemPolicylevelcoverage, "New Exposure Button");
		
			//Thread.sleep(1000);//inevitable: Wait till the drop down list opens completely.
			scroll(elemPolicylevelcoverage,"elemPolicylevelcoverage: "+policylevelcoverage);
		
			By elemExposureType = By.xpath("//span[contains(text(),'"+exposuretype+ "')]/..");		
			scroll(elemExposureType,"exposureType");
			click(elemExposureType, exposuretype + " exposureType ");	
		
			if(exposuretype2!=null){
				By elemExposureType2 = By.xpath("//span[contains(text(),'"+exposuretype2+ "')]/..");		
				scroll(elemExposureType2,"exposureType");
				click(elemExposureType2, exposuretype2 + " exposureType ");
			}

		}

		@SuppressWarnings("rawtypes")
		public ArrayList getExposuresAdded() throws Throwable{
			ArrayList<Object> arrayListExposures= new ArrayList<>();
			try{
				int numOfExposures = driver.findElements(SaveAndAssignClaimPage.exposuresAdded).size();
				ArrayList<Object> arrayListExposuresActual= new ArrayList<>(numOfExposures);
				if(numOfExposures>0){
					HashMap<String, String> exposureValues= new HashMap<String, String>();
					for(int i=1;i<=numOfExposures;i++){					
						exposureValues.put("Type", getText(By.xpath("//div[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_AssignSaveScreen:NewExposureLV-body']//tr["+i+"]//td["+3+"]"),"Type"));
						exposureValues.put("Coverage", getText(By.xpath("//div[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_AssignSaveScreen:NewExposureLV-body']//tr["+i+"]//td["+4+"]"),"Coverage"));
						exposureValues.put("Claimant", getText(By.xpath("//div[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_AssignSaveScreen:NewExposureLV-body']//tr["+i+"]//td["+5+"]"),"Claimant"));
						exposureValues.put("Involving", getText(By.xpath("//div[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_AssignSaveScreen:NewExposureLV-body']//tr["+i+"]//td["+6+"]"),"Involving"));
						exposureValues.put("Status", getText(By.xpath("//div[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_AssignSaveScreen:NewExposureLV-body']//tr["+i+"]//td["+7+"]"),"Status"));
						arrayListExposures.add(exposureValues);
					}				
				}
				arrayListExposures = arrayListExposuresActual;			
			}catch(Exception ex){
				//nothing expected
			}
			return arrayListExposures;
		}
		


			/**
			 * Dynamic xpath for exposure to select
			 * @param String exposureType
			 * @return By
			 */
			public By getExposureElementToSelect(String exposureType){
				By autoExposureType = null;
				switch (exposureType.toLowerCase()) {
				case "comprehensive":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.comprehensive.value() + "')]/..");			
					break;
				case "collision":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.CollisionVehicleDamage.value() + "')]");			
					break;
				case "bodily injury":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.LiabilityAutoBodilyInjuryBodilyInjuryDamage.value() + "')]");			
					break;
				case "medical payments":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.MedicalPayments.value() + "')]");			
					break;
				case "insured motorists":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.UnderInsuredMotoristsBI.value() + "')]");			
					break;
				case "party rental":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.FirstPartyRental.value() + "')]");			
					break;
				case "property damage - property":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.LiabilityPropertyDamageProperty.value() + "')]");			
					break;
				case "property damage - vehicle":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.LiabilityPropertyDamageVehicle.value() + "')]");			
					break;
				case "pip - death":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.PIPDeath.value() + "')]");			
					break;
				case "pip - extraordinary medical":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.PIPExtraordinaryMedical.value() + "')]");			
					break;
				case "pip - funeral":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.PIPFuneral.value() + "')]");			
					break;
				case "pip - lost wages":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.PIPLostWages.value() + "')]");			
					break;
				case "pip - medical":
					autoExposureType = By.xpath("//span[contains(text(),'"+EnumAutoExposureTypes.PIPMedical.value() + "')]");			
					break;			
				default:
					break;
				}
				return autoExposureType;		
			}
		}
