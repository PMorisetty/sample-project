package com.pure.claim;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class RentalPageLib extends ActionEngine{
	
	public RentalPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void setZipCodeText(String zipcode) throws Throwable{
		type(RentalPage.zipCode, zipcode, "ZipCode");
		type(RentalPage.zipCode, Keys.TAB, "TAB Key");
	}
	public void clickSearch() throws Throwable{
		scroll(RentalPage.search,"Search button");
		click(RentalPage.search,"Search button");
	}
}
