package com.pure.claim;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class AssignmentDetailsPageLib extends ActionEngine{
	
	public AssignmentDetailsPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void setRelatesTo(String relatesTo) throws Throwable{
		type(AssignmentDetailsPage.relatesTo, relatesTo, "Relates To");
		type(AssignmentDetailsPage.relatesTo, Keys.TAB, "Tab Click");
	}
	
	public void clickAddBtn() throws Throwable{
		click(AssignmentDetailsPage.addBtn, "AddBtn");
	}
	
	// 'Select Services to Add' Page
	public void selectServiceToAdd(String service) throws Throwable{
		By serviceXpath = By.xpath("//div[@id='SelectServicesPopup:SpecialistServiceTreeLV-body']//tr//span[.='"+ service +"']/../input");
		click(serviceXpath, "Property");
		click(AssignmentDetailsPage.addServiceBtn, "Add");
	}
	
	public void setSendTo(String sendTo) throws Throwable{
		type(AssignmentDetailsPage.sendTo, sendTo, "Send To");
		type(AssignmentDetailsPage.sendTo, Keys.TAB, "Tab Click");
	}
	
	public void setEstimator(String estimator) throws Throwable{
		type(AssignmentDetailsPage.estimator, estimator, "Estimator");
		type(AssignmentDetailsPage.estimator, Keys.TAB, "Tab Click");
	}
	
	public void clickUpdateBtn() throws Throwable{
		click(AssignmentDetailsPage.updateBtn, "Update");
	}

}
