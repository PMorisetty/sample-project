package com.pure.claim;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewExposurePageLib extends ActionEngine {

	public NewExposurePageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {
		this.driver = webDriver;
		this.reporter = reporter;
		//wait till page opened.
		isElementPresent(NewExposurePage.coverageShown, "coverage");
		waitForMask();
		printPageTitle(NewExposurePage.title,"Page title");
	}
	public void setClaimant(String claimant) throws Throwable {
		type(NewExposurePage.claimant, claimant, claimant+" claimant");
		type(NewExposurePage.claimant, Keys.ENTER, "Enter Key");
		//Thread.sleep(1000);
	}
	public void setType(String sType) throws Throwable{
		type(NewExposurePage.claimantType,sType,"Claimant Type");
		type(NewExposurePage.claimantType, Keys.TAB, "TAB Key");
	}
	public String getCoverageSelected() throws Throwable{
		return getText(NewExposurePage.coverageShown, "coverage shown");
	}
	
	public void clickOK() throws Throwable{
		click(NewExposurePage.okBtn, "Ok buton");
		waitForMask();
		reporter.SuccessReport("Exposure:","Successfully added exposure");
		printPageTitle(NewClaimPage.title,"Page title");
		
	}
	
	public void verifyExposureMessage() throws Throwable{
		if(isVisible(NewExposurePage.exposureMsg, "Exposure Message")==false)
		{
			click(NewExposurePage.okBtn, "Ok buton");
		}
		waitForMask();
	}
	public void clickRefresh() throws Throwable{
		click(NewExposurePage.refreshBtn, "Refresh Button");
		waitForMask();
	}
	public void clickUpdate() throws Throwable{
		click(NewExposurePage.updateBtn, "update buton");
		waitForMask();
		reporter.SuccessReport("Exposure:","Successfully added exposure");
	}
	public void filldetails(Hashtable<String, String> data) throws Throwable{
		setClaimant(data.get("claimant"));
		setType(data.get("claimantType"));	
	}
	public void setPropertyDescription(String propertyDescription) throws Throwable{
		type(NewExposurePage.propertyDescription,propertyDescription,"Property Description");
	}
	public void setDamageDescription(String damageDescription) throws Throwable{
		type(NewExposurePage.damageDescription,damageDescription,"Damage Description");
	}
	public void openNewIncident() throws Throwable{
		click(NewExposurePage.newPropertyIncidentIcon, "New Property incident icon");
		click(NewExposurePage.newPropertyIncident, "New Property incident");
	}
	public void openEditIncident() throws Throwable{
		click(NewExposurePage.newPropertyIncidentIcon, "New Property incident icon");
		click(NewExposurePage.editPropertyIncident, "Edit Property incident");
	}
	public void setIAUsed(String iaused) throws Throwable{
		if(iaused.equalsIgnoreCase("yes")){
			click(NewExposurePage.iaUsedYES,"iaUsedYES");
		}else{
			click(NewExposurePage.iaUsedNO,"iaUsedNO");
		}
	}
	public void setDeskAdjustment(String deskadjustment) throws Throwable{
		if(deskadjustment.equalsIgnoreCase("yes")){
			click(NewExposurePage.deskAdjustmentYES,"deskAdjustmentYES");
		}else{
			click(NewExposurePage.deskAdjustmentNO,"deskAdjustmentNO");
		}
	}
	public void setLossOfUse(String lossOfUse) throws Throwable{
		if(lossOfUse.equalsIgnoreCase("yes")){
			click(NewExposurePage.lossOfUseYes,"lossOfUseYes");
		}else{
			click(NewExposurePage.lossOfUseNo,"lossOfUseNo");
		}
	}
	public void setLossEstimate(String lossEstimate) throws Throwable{
		type(NewExposurePage.lossEstimate, lossEstimate, "Loss Estimate");
	}
	public void setLostItem(String lostItem) throws Throwable{
		type(NewExposurePage.lostItem, lostItem, "Loss Estimate");
		type(NewExposurePage.lostItem, Keys.TAB, "Tab keys");
	}
	public void setLocation(String location) throws Throwable{
		type(NewExposurePage.location, location, "Location");
		type(NewExposurePage.location, Keys.TAB, "Tab keys");
		waitForMask();
	}
	public void setVehicleIncident(String vehicleIncident) throws Throwable{
		type(NewExposurePage.vehicleIncident, vehicleIncident, "vehicleIncident");
		type(NewExposurePage.vehicleIncident, Keys.TAB, "Tab keys");
		waitForMask();
	}
}
