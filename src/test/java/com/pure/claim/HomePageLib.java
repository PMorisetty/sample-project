package com.pure.claim;
import junit.framework.Assert;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class HomePageLib extends ActionEngine{
	Actions action;
	public HomePageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
		action = new Actions(driver);
	}
	public void login(String userName, String password) throws Throwable{
		type(HomePage.userName,userName, "User Name");
		type(HomePage.password, password, "Password");
		JSClick(HomePage.loginButton, "Login Button");
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(HomePage.loginButton));
	}
	
	public void pureLogin(String userName, String password) throws Throwable{
		click(HomePage.gotoLink,"Goto Link");
		type(HomePage.pureUserName,userName, "User Name");
		type(HomePage.purePassword, password, "Password");
		JSClick(HomePage.pureLoginButton, "Login Button");
		//WebDriverWait wait = new WebDriverWait(driver, 10);
	}
	
	public void verifyTitleOfClaimCenter() throws Throwable{
		
		String title = getTitle();
		assertTitle("Guidewire ClaimCenter (Prod TestUser)");
		System.out.println("ClaimCenter Page With Title:" + title);
	}
	
	
	
	public void logout() throws Throwable{
		click(HomePage.settingIcon, "Setting Icon");
		click(HomePage.logoutLink, "Logout link");
		try{
			if(driver.findElement(HomePage.messageBoxOkButton).isDisplayed()){
				click(HomePage.messageBoxOkButton, "Confirmation box - OK Button");
			}
		}catch(Exception ex){
			//Do nothing, as element is not expected every time
		}

	}
	public int getDesktopOffset(){
		return driver.findElement(HomePage.desktopMenu).getSize().width - 10;
	}
	public int getClaimOffset(){
		return driver.findElement(HomePage.claimsMenu).getSize().width - 10;
	}
	public int getSearchOffset(){
		return driver.findElement(HomePage.searchMenu).getSize().width - 10;
	}
	public int getAddressBookOffset(){
		return driver.findElement(HomePage.addressBookMenu).getSize().width - 10;
	}

	public void moveToDesktopMenu() throws InterruptedException{
		action.moveToElement(driver.findElement(HomePage.desktopMenu), getDesktopOffset(), 10).click().build().perform();
	}
	public void navigateToActivitesInDesktopMenu() throws InterruptedException{
		moveToDesktopMenu();
		//		action.moveToElement(driver.findElement(HomePage.desktopMenu), getDesktopOffset(), 10).click().build().perform();
		action.moveToElement(driver.findElement(HomePage.activitiesLinkInDesktop)).click().build().perform();		
	}
	public void navigateToClaimsInDesktopMenu() throws InterruptedException{
		moveToDesktopMenu();
		//		action.moveToElement(driver.findElement(HomePage.desktopMenu), getDesktopOffset(), 10).click().build().perform();
		action.moveToElement(driver.findElement(HomePage.claimsLinkInDesktop)).click().build().perform();	
	}
	public void navigateToExposuresInDesktopMenu() throws InterruptedException{
		moveToDesktopMenu();
		//		action.moveToElement(driver.findElement(HomePage.desktopMenu), getDesktopOffset(), 10).click().build().perform();	
		action.moveToElement(driver.findElement(HomePage.exposuresLinkInDesktop)).click().build().perform();	
	}
	public void navigateToSubrogationInDesktopMenu() throws InterruptedException{
		moveToDesktopMenu();
		//		action.moveToElement(driver.findElement(HomePage.desktopMenu), getDesktopOffset(), 10).click().build().perform();	
		action.moveToElement(driver.findElement(HomePage.subrogationLinkInDesktop)).click().build().perform();	
	}
	public void navigateToMaServicesInDesktopMenu() throws InterruptedException{
		moveToDesktopMenu();
		//		action.moveToElement(driver.findElement(HomePage.desktopMenu), getDesktopOffset(), 10).click().build().perform();
		action.moveToElement(driver.findElement(HomePage.maServicesLinkInDesktop)).click().build().perform();	
	}
	public void navigateToBulkInvoicesInDesktopMenu() throws InterruptedException{
		moveToDesktopMenu();
		//		action.moveToElement(driver.findElement(HomePage.desktopMenu), getDesktopOffset(), 10).click().build().perform();
		action.moveToElement(driver.findElement(HomePage.bulkInvoicesInDesktop)).click().build().perform();	
	}
	public void navigateToMyCalenderInDesktopMenu() throws Throwable{
		moveToDesktopMenu();
		//		action.moveToElement(driver.findElement(HomePage.desktopMenu), getDesktopOffset(), 10).click().build().perform();	
		action.moveToElement(driver.findElement(HomePage.desktopMenuCalenderSubMenu)).build().perform();	
		//Thread.sleep(2000);
		waitForVisibilityOfElement(HomePage.myCalenderLink, "CalendarLink");
		action.moveToElement(driver.findElement(HomePage.myCalenderLink)).click().build().perform();	
	}
	public void moveToClaimMenu() throws Throwable{
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(HomePage.claimsMenu));
			wait.until(ExpectedConditions.elementToBeClickable(HomePage.claimsMenu));
			waitForMask();
			action.moveToElement(driver.findElement(HomePage.claimsMenu), getClaimOffset(), 10).click().build().perform();
		} catch (Throwable e) {
			reporter.failureReport("Move To ClaimMenu", "Failed to navigate to claims menu", driver);
		}


	}
	public void navigateToNewClaimInClaimMenu() throws Throwable{
		moveToClaimMenu();
		action.moveToElement(driver.findElement(HomePage.newClaimLink)).click().build().perform();
	}
	
	public void navigateToClaimCenterLink() throws Throwable{
		String window = driver.getWindowHandle();
		action.moveToElement(driver.findElement(HomePage.claimCenterLink)).click().build().perform();
		switchToWindow(window);
	}
	
	public void enterClaimNumber(String claimNumber) throws Throwable{
		moveToClaimMenu();
		action.moveToElement(driver.findElement(HomePage.claimNumberTextbox)).click().sendKeys(claimNumber)
		.sendKeys(Keys.RETURN)
		.build().perform();
	}
	public void searchClaim(String claimNumber) throws Throwable{		
		moveToClaimMenu();		
		waitForVisibilityOfElement(HomePage.claimNumberTextbox, "claimNumberTextbox");
		type(HomePage.claimNumberTextbox,claimNumber,"claimNum");
		type(HomePage.claimNumberTextbox,Keys.ENTER,"Enter key ");
		waitForMask();
	}
	public void moveToSearchMenu() throws InterruptedException{
		action.moveToElement(driver.findElement(HomePage.searchMenu), getSearchOffset(), 10).click().build().perform();
	}
	public void navigateToSimpleSearchInSearchMenu() throws Throwable{
		moveToSearchMenu();
		action.moveToElement(driver.findElement(HomePage.searchMenuClaimsSubMenu)).build().perform();	
		waitForVisibilityOfElement(HomePage.simpleSearchLink, "SimpleSearch Link");
		action.moveToElement(driver.findElement(HomePage.simpleSearchLink)).click().build().perform();
	}
	public void navigateToAdvancedSearchInSearchMenu() throws Throwable{
		moveToSearchMenu();
		action.moveToElement(driver.findElement(HomePage.searchMenuClaimsSubMenu)).build().perform();	
		waitForVisibilityOfElement(HomePage.advancedSearchLink, "Advanced Search Link");
		action.moveToElement(driver.findElement(HomePage.advancedSearchLink)).click().build().perform();
	}
	public void navigateToSearchByContactInSearchMenu() throws Throwable{
		moveToSearchMenu();
		action.moveToElement(driver.findElement(HomePage.searchMenuClaimsSubMenu)).build().perform();	
		waitForVisibilityOfElement(HomePage.searchByContactLink, "SearchByContact Link");
		action.moveToElement(driver.findElement(HomePage.searchByContactLink)).click().build().perform();
	}
	public void navigateToActivitiesInSearchMenu() throws InterruptedException{
		moveToSearchMenu();
		action.moveToElement(driver.findElement(HomePage.activitiesLink)).click().build().perform();
	}
	public void navigateToChecksInSearchMenu() throws InterruptedException{
		moveToSearchMenu();
		action.moveToElement(driver.findElement(HomePage.checksLink)).click().build().perform();
	}
	public void navigateToRecoveriesInSearchMenu() throws InterruptedException{
		moveToSearchMenu();
		action.moveToElement(driver.findElement(HomePage.recoveriesLink)).click().build().perform();
	}
	public void navigateToSearchInAddressBookMenu() throws Throwable{
		waitForVisibilityOfElement(HomePage.addressBookMenu, "Address Book Menu");
		action.moveToElement(driver.findElement(HomePage.addressBookMenu), getAddressBookOffset(), 10).click().build().perform();
		action.moveToElement(driver.findElement(HomePage.searchLink)).click().build().perform();
	}
	public void navigateToBulkInvoices() throws Throwable{
		click(HomePage.bulkInvoice, "bulkInvoice");
	}

	public void navigateToBatchProcessInfo() throws Throwable{
		action.keyDown(Keys.ALT).keyDown(Keys.SHIFT).sendKeys("t").sendKeys(Keys.NULL).build().perform();
		waitForMask();
	}

	//*************************************************************************
	// Navigation/Click tree items
	public void openSecondaryApproval() throws Throwable{
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try{
			wait.until(ExpectedConditions.elementToBeClickable(HomePage.secondaryApproval));
			click(HomePage.secondaryApproval, "secondary approval");
		}
		catch(Exception ex){
			System.out.println("Unable to click secondary approval link");
			ex.printStackTrace();
		}
	}
	
	public void openActivities() throws Throwable{
		click(HomePage.activities, "activities");
	}
	public void navigateToAdministrationInAdministrationMenu() throws Throwable{
		waitForVisibilityOfElement(HomePage.administrationMenu, "Administration Menu");
		action.moveToElement(driver.findElement(HomePage.administrationMenu), getAddressBookOffset(), 10).click().build().perform();
		action.moveToElement(driver.findElement(HomePage.administrationLink)).click().build().perform();
	}
}
