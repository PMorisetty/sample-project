package com.pure.claim;

import org.openqa.selenium.By;

public class RentalPage {
	
	public static By zipCode,search;

	
	static{
		zipCode = By.xpath("//label[text()='Zip Code']/../..//input");
		search = By.id("RentalWizard:RentalWizard_FindBranchScreen:RentalWizardFindBranchPanelSet:Search");
	}

}
