package com.pure.claim;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class SearchAssignerPageLib extends ActionEngine{
	
	public SearchAssignerPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable{
		this.driver = driver;
		this.reporter = reporter;
		waitForMask();
		printPageTitle(SearchAssignerPage.title,"Page title");
	}
	
	public void findUserAndAssign(String secondaryapprovalusername) throws Throwable{
		type(SearchAssignerPage.username,secondaryapprovalusername,"secondary approval username:"+secondaryapprovalusername);
		click(SearchAssignerPage.searchBtn,"search btn");
		waitForVisibilityOfElement(SearchAssignerPage.assignBtnForSecondaryApprover, "Assign");
		click(SearchAssignerPage.assignBtnForSecondaryApprover,"assignBtnForSecondaryApprover");
		reporter.SuccessReport("" , "Successfully assigned claim for "+secondaryapprovalusername);
	}
	
	public void findUserAndAssign_AssignActivities(String name) throws Throwable{
		type(SearchAssignerPage.selecFromList,name,"Select From List");
		type(SearchAssignerPage.selecFromList,Keys.TAB, "Keys Tab");
		click(SearchAssignerPage.assignBtn,"assign btn");
	}
}
