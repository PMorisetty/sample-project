package com.pure.claim;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class AssignPageLib extends ActionEngine{
	public AssignPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable{
		this.driver = driver;
		this.reporter = reporter;
		printPageTitle(AssignPage.title,"Page title");
	}
	
	public void assignClaimFromList(String assignClaimTo) throws Throwable{
		type(AssignPage.assignDropDown, assignClaimTo, " Name in assignment dropdown");
		type(AssignPage.assignDropDown, Keys.TAB, "TAB Key");
	}
	
	public void clickAssign() throws Throwable{
		click(AssignPage.assignBtn, "Assign Button");
		waitForMask();
	}

}
