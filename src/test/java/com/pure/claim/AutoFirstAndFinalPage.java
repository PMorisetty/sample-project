package com.pure.claim;

import org.openqa.selenium.By;

public class AutoFirstAndFinalPage {
	
	static By autoBodyVendorReporter, insuredVendorReporter;
	static By coverageInQuestionYes, coverageInQuestionNo;
	static By vendorMenuIcon, vendorSearchButton;
	static By vehicle, lossDescription;
	static By claimantName, claimantType;
	static By financialsName, financialsPayeeType, amount;
	static By location;
	static By title;
	static By finishButton;
	static By duplicateClaimTab;
	static By closeButton;
	
	static{
		autoBodyVendorReporter = By.xpath("//label[text()='Auto Body Vendor']/../input");
		insuredVendorReporter = By.xpath("//label[text()='Insured']/../input");
		coverageInQuestionYes = By.xpath("//input[contains(@id, 'coverageInQuestion_true-inputEl')]");
		coverageInQuestionNo = By.xpath("//input[contains(@id, 'coverageInQuestion_false-inputEl')]");
		vendorMenuIcon = By.xpath("//a[@id='FNOLWizard:FNOLWizard_AutoFirstAndFinalScreen:AutoFirstAndFinalReportedPanelSet:FNOLWizardAutoFirstAndFinalPanelSet:GlassScreenVendorReporter:GlassScreenVendorReporterMenuIcon']");
		vendorSearchButton = By.xpath("//span[@id='FNOLWizard:FNOLWizard_AutoFirstAndFinalScreen:AutoFirstAndFinalReportedPanelSet:FNOLWizardAutoFirstAndFinalPanelSet:GlassScreenVendorReporter:MenuItem_Search-textEl']");
		vehicle = By.xpath("//label[text()='Select vehicle']/../following-sibling::td//input");
		lossDescription = By.xpath("//label[text()='Loss Description']/../following-sibling::td/textarea");
		claimantName = By.xpath("//input[contains(@id, 'Claimant_Picker-inputEl')]");
		claimantType = By.xpath("//input[contains(@id, 'Claimant_Type-inputEl')]");
		financialsName = By.xpath("//input[contains(@id, 'Payee_Picker-inputEl')]");
		financialsPayeeType = By.xpath("//input[contains(@id, 'Check_Payee-inputEl')]");
		amount = By.xpath("//input[contains(@id, 'CheckAmount-inputEl')]");
		location = By.xpath("//input[contains(@id, 'Address_Picker-inputEl')]");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		finishButton = By.id("FNOLWizard:Finish-btnInnerEl");
		duplicateClaimTab = By.id("wsTabBar:wsTab_0-btnInnerEl");
		closeButton = By.cssSelector("span[id$='NewClaimDuplicatesWorksheet_CloseButton-btnInnerEl']");
	}

}
