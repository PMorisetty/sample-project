package com.pure.claim;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.report.CReporter;

public class NewClaimPage extends NewClaimPageLib{

	public NewClaimPage(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {
		super(webDriver, reporter);
	}

	static By radiobtnVerifiedPolicy;
	static By radiobtnUnverifiedPolicy;
	static By btnCancel;
	static By btnNext;	
	static By pageTitle;
	static By search;
	static By duplicateClaimTab;
	static By closeButton;
	static By title;
	static By btnFinish;
	static By policyPluginErrorMsg;
	static{
		radiobtnVerifiedPolicy = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:ScreenMode_true-inputEl");
		radiobtnUnverifiedPolicy = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:ScreenMode_false-inputEl");
		btnCancel= By.id("FNOLWizard:Cancel-btnInnerEl");
		btnNext= By.id("FNOLWizard:Next-btnInnerEl");		
		pageTitle = By.cssSelector("span[id$=ttlBar]");
		search = By.id("FNOLWizard:FNOLWizard_FindPolicyScreen:FNOLWizardFindPolicyPanelSet:Search");
		duplicateClaimTab = By.id("wsTabBar:wsTab_0-btnInnerEl");
		closeButton = By.cssSelector("span[id$='NewClaimDuplicatesWorksheet_CloseButton-btnInnerEl']");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		btnFinish = By.id("FNOLWizard:Finish-btnInnerEl");
		policyPluginErrorMsg = By.xpath("//div[text()='The policy plugin was unable to locate policy information based on your criteria. Please try again or create an unverified policy.']");
	}
	

}
