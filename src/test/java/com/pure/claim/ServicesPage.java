package com.pure.claim;

import org.openqa.selenium.By;

public class ServicesPage {
	
	static By emergencyMagementService, debrisRemoval,
	emergencyMagementServiceVendor, debrisRemovalVendor,
	detailsTab, historyTab, activitiesTab, documentsTab, notesTab, invoicesTab,
	editBtn, reassignBtn, cancelBtn, assignBtn, claimServiceRequest,submitSupplementBtn, confirmRepairableBtn,
	progress, instructionsToVendor, serviceAddress, vendorName, coverage, serviceType, serviceNoLink, actionRequiredMsg,estimateReceivableLink,
	reviewForpayLink, paymentAmount, invoiceNumber, check_yes_radiobtn, updatebtn, reserveLine, shareOnMemberPortal, deliveryMethod,
	approveBtn, cancelSubmitSupplementBtn, EstimateReceivedChkbox, assign_Btn, clearBtn, deductableDecision_applied_radiobtn,
	payment_EFT, reviewForpayLink_S1, accountName, bankName, accountType, accountNumber, routingNumber, rejectButton, okBtn,
	notes_msg, estimateReceivableLink_S01, confirmTotalLoss,serviceRecord,serviceNumber;
	
	
	static{
		emergencyMagementService = By.xpath("//input[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_ServicesScreen:PolicyPanelSet:FNOLWizard_ServicesPolicyPanelSet:EMSInputGroup:_checkbox']");
		debrisRemoval = By.xpath("//input[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_ServicesScreen:PolicyPanelSet:FNOLWizard_ServicesPolicyPanelSet:DebrisRemovalInputGroup:_checkbox']");
		emergencyMagementServiceVendor = By.xpath("//input[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_ServicesScreen:PolicyPanelSet:FNOLWizard_ServicesPolicyPanelSet:EMSInputGroup:EMSPicker-inputEl']");
		debrisRemovalVendor = By.xpath("//input[@id='FNOLWizard:AutoWorkersCompWizardStepSet:FNOLWizard_ServicesScreen:PolicyPanelSet:FNOLWizard_ServicesPolicyPanelSet:DebrisRemovalInputGroup:DebrisRemovalPicker-inputEl']");
		detailsTab = By.id("ClaimServiceRequests:DetailsTab-btnInnerEl");
		historyTab = By.id("ClaimServiceRequests:HistoryTab-btnInnerEl");
		activitiesTab = By.id("ClaimServiceRequests:ActivitiesTab-btnInnerEl");
		documentsTab = By.id("ClaimServiceRequests:DocumentsTab-btnInnerEl");
		notesTab = By.id("ClaimServiceRequests:NotesTab-btnInnerEl");
		invoicesTab = By.id("ClaimServiceRequests:InvoicesTab-btnInnerEl");
		editBtn = By.xpath("//span[text()='Edit']");
		reassignBtn = By.xpath("//span[text()='Reassign']");
		cancelBtn = By.xpath("//span[text()='Cancel']");
		assignBtn = By.xpath("//span[text()='Assign']");
		progress = By.xpath("//div[contains(@id,'ServiceRequestDV:Progress-inputEl')]");
		instructionsToVendor = By.xpath("//div[contains(@id,'ServiceRequestDV:vendorInstructionType-inputEl')]");
		serviceAddress = By.id("ClaimServiceRequests:ServiceRequestPanelRow:ServiceRequestDV:ServiceAddress:CCAddressInputSet:globalAddressContainer:globalAddress:GlobalAddressInputSet:AddressSummary-inputEl");
		vendorName = By.id("ClaimServiceRequests:ServiceRequestPanelRow:ServiceRequestDV:Specialist-inputEl");
		coverage = By.id("ClaimServiceRequests:ServiceRequestPanelRow:ServiceRequestDV:deductibleCoverageInput-inputEl");
		serviceType = By.xpath("//div[contains(@id,'ServiceToPerform:InstructionServicesLV-body')]//td[3]");
		serviceNoLink = By.id("ClaimServiceRequests:ServiceRequestList:ServiceRequestLV:0:ServiceRequestNumber");
		actionRequiredMsg = By.id("ClaimServiceRequests:ServiceRequestList:ServiceRequestLV:0:ActionRequired");
		claimServiceRequest = By.id("ClaimServiceRequests:ServiceRequestActivityLV:ServiceRequestFilter-inputEl");
		estimateReceivableLink = By.xpath("//a[@id='ClaimServiceRequests:ServiceRequestActivityLV:0:Subject']");
		estimateReceivableLink_S01 = By.xpath("//a[@id='ClaimServiceRequests:ServiceRequestActivityLV:2:Subject']");
		submitSupplementBtn = By.xpath("//span[text()='Submit Supplement']");
		cancelSubmitSupplementBtn = By.id("ActivityDetailViaClaimWorksheet:ActivityDetailScreen:ReadOnlyActivityDetailWorksheet_CancelButton-btnInnerEl");
		reviewForpayLink = By.xpath("//a[text()='Review for pay: E01']");
		reviewForpayLink_S1 = By.xpath("//a[text()='Review for pay: S01']");
		paymentAmount = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:CheckAmountId-inputEl");
		invoiceNumber = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:Check_InvoiceNumber-inputEl");
		confirmRepairableBtn = By.id("ActivityDetailViaClaimWorksheet:ActivityDetailScreen:ActivityDetailScreen_ApproveButton");
		check_yes_radiobtn = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:includeDocRadioInput_true-boxLabelEl");
		updatebtn =  By.xpath("//span[text()='Update']");
		approveBtn =  By.xpath("//span[text()='Approve']");
		reserveLine= By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:ReserveLineId-inputEl");
		shareOnMemberPortal = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:showOnPortalType-inputEl");
		deliveryMethod = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:CheckDeliveryMethod-inputEl");
		EstimateReceivedChkbox = By.xpath("//img[@class='x-grid-checkcolumn']");
		assign_Btn = By.id("ClaimServiceRequests:ServiceRequestActivityLV_tb:AssignButton-btnInnerEl");
		clearBtn = By.id("WebMessageWorksheet:WebMessageWorksheetScreen:WebMessageWorksheet_ClearButton-btnInnerEl");
		deductableDecision_applied_radiobtn = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:DeductibleManagementInputSet:DeductibleWaived_false-inputEl");
		payment_EFT = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:PaymentMethod_option2-inputEl");
		
		accountName = By.name("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:EFTDataInputSet:PayTo");
		bankName = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:EFTDataInputSet:BankName-inputEl");
		accountType = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:EFTDataInputSet:BankAccountType_option2-inputEl");
		accountNumber = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:EFTDataInputSet:BankAccountNumber-inputEl");
		routingNumber = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:PaymentApprovalDetailDV:EFTDataInputSet:BankRoutingNumber-inputEl");
		rejectButton = By.id("PaymentApprovalDetailWorksheet:ApprovalDetailScreen:ApprovalDetailWorksheet_RejectButton-btnInnerEl");
		okBtn = By.xpath("//span[contains(text(),'OK')]");
		notes_msg = By.xpath("//div[@id='x-form-el-ClaimServiceRequests:ServiceRequestNotesLV:0:Subject']");
		confirmTotalLoss = By.id("ActivityDetailViaClaimWorksheet:ActivityDetailScreen:ActivityDetailScreen_RejectButton-btnInnerEl");
	
		serviceRecord = By.xpath("//span[text()='Service #']/ancestor::td[2]//tr[1]/td[3]//a");
		serviceNumber = By.xpath("//label[text()='Service Number']/ancestor::td[1]/..//div");
	}

}
