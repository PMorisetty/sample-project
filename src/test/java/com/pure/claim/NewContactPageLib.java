package com.pure.claim;

import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewContactPageLib extends ActionEngine{

	//ensure required page opened
	public NewContactPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {
		this.driver = webDriver;
		this.reporter = reporter;
		waitForVisibilityOfElement(NewContactPage.lastName, "Last name");
		printPageTitle(NewClaimPage.title,"Page title");
	}

	
	public void setPrefix(String prefix) throws Throwable{
		type(NewContactPage.prefix, prefix, "Prefix");
	}
	
	public void setFirstname(String firstName) throws Throwable{
		type(NewContactPage.firstName, firstName, "First Name");
	}
	public void setMiddleName(String middleName) throws Throwable{
		type(NewContactPage.middleName, middleName, "Middle Name");
	}
	public void setLastName(String lastName) throws Throwable{
		type(NewContactPage.lastName, lastName, "Last Name");		
	}
	public void setSuffix(String suffix) throws Throwable{
		type(NewContactPage.suffix, suffix, "Suffix");
	}
	public void clickOK() throws Throwable{		
		scroll(NewContactPage.btnOK, "btnOK");
		waitForVisibilityOfElement(NewContactPage.btnOK, "btnOK");
		click(NewContactPage.btnOK, "OK");
	}
	public void setAddress1(String address1) throws Throwable{
		type(NewContactPage.address1, address1, "Address 1");
	}
	public void setAddress2(String address2) throws Throwable{
		type(NewContactPage.address2, address2, "Address 2");
	}
	public void setZip(String zip) throws Throwable{
		type(NewContactPage.zip, zip, "Zip");
		type(NewContactPage.zip, Keys.TAB, "Tab key");
		click(NewContactPage.zipAutoFill, "Address auto fill");
		//Thread.sleep(1000);//Inevitable as form filling takes some time.
		//waitForMask();
	}
	
	public void fillContactDetails(JSONObject contactDetails) throws Throwable{
		//basic info
		setPrefix(contactDetails.get("prefix").toString());
		setFirstname(contactDetails.get("firstName").toString());
		setMiddleName(contactDetails.get("middleName").toString());
		setLastName(contactDetails.get("lastName").toString());
		setSuffix(contactDetails.get("suffix").toString());
		//address details
		setZip(contactDetails.get("zip").toString());
		setAddress1(contactDetails.get("address1").toString());
		setAddress2(contactDetails.get("address2").toString());
		//save info
		clickOK();
	}

	public void fillContactDetails(Hashtable<String, String> data) throws Throwable{
		//basic info
		setPrefix(data.get("insuredPrefix"));
		setFirstname(data.get("insuredFirstName"));
		setMiddleName(data.get("insuredMiddleName"));
		setLastName(data.get("insuredLastName"));
		setSuffix(data.get("insuredSuffix"));
		//address details
		setZip(data.get("insuredZip"));
		setAddress1(data.get("insuredAddress1"));
		setAddress2(data.get("insuredAddress2"));
		//save info
		clickOK();
	}
}
