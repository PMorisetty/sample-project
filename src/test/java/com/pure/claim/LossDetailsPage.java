package com.pure.claim;

import org.openqa.selenium.By;

public class LossDetailsPage {

	static By lossDetailsText, vehicleMake, watercraftModel,
	editBtn, addLossItemBtn, dwellingLossItem, personalPropertyLossItem, propertyLiabilityLossItem,
	faultRating, updateBtn, isoLink, detailsLink, sendToISOBtn, sendToISOBtnAncestor, refereshResponsesBtn,
	isoStatus, isoDateSent, isoLastResponse, knownToISO, dateOfLoss, lossDescription, subrogatableRadioBtnYES, subrogatableRadioBtnNO,
	ClearValidationAlert, subrogationStatus, referToSubroRadioBtnYES, referToSubroRadioBtnNO, insuredLiability, subrogatableStatus,
	assignToXactAnalysis, largeLossRadioBtnYES, largeLossRadioBtnNO;
	
	static{
		lossDetailsText = By.xpath("//textarea[@id='ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:IncidentPanelSet:0:Description-inputEl']");
		vehicleMake = By.id("ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:LossDetailsCardCV:LossDetailsDV:EditableVehicleIncidentsLV:0:Make");
		watercraftModel = By.id("ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:LossDetailsCardCV:LossDetailsDV:PURE_EditableWatercraftIncidentsLV:0:ModelCell");
		editBtn = By.id("ClaimLossDetails:ClaimLossDetailsScreen:Edit-btnInnerEl");
		addLossItemBtn = By.id("ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:IncidentPanelSet_tb:AddIncidentButton-btnInnerEl");
		dwellingLossItem = By.xpath("//span[text()='Dwelling']");
		personalPropertyLossItem = By.xpath("//span[text()='Personal Property']");
		propertyLiabilityLossItem = By.xpath("//span[text()='Property Liability']");
		faultRating = By.id("ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:LossDetailsDV:Notification_Fault-inputEl");
		updateBtn = By.id("ClaimLossDetails:ClaimLossDetailsScreen:Update-btnInnerEl");
		isoLink = By.id("ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:LossDetailsCardCV:Claim_ISOCardTab-btnIconEl");
		detailsLink = By.id("ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:LossDetailsCardCV:Claim_DetailsCardTab-btnInnerEl");
		sendToISOBtn = By.id("ClaimLossDetails:ClaimLossDetailsScreen:ClaimLossDetails_SendToISOButton-btnInnerEl");
		sendToISOBtnAncestor = By.xpath("//span[@id='ClaimLossDetails:ClaimLossDetailsScreen:ClaimLossDetails_SendToISOButton-btnInnerEl']/ancestor::a");
		refereshResponsesBtn = By.id("ClaimLossDetails:ClaimLossDetailsScreen:ClaimLossDetails_RefreshButton-btnInnerEl");
		isoStatus = By.xpath("//div[@id='ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:LossDetailsCardCV:ISODetailsDV:Status-inputEl']");
		isoDateSent = By.xpath("//div[@id='ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:LossDetailsCardCV:ISODetailsDV:SendDate-inputEl']");
		isoLastResponse = By.xpath("//div[@id='ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:LossDetailsCardCV:ISODetailsDV:ReceiveDate-inputEl']");
		knownToISO = By.xpath("//div[@id='ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:LossDetailsCardCV:ISODetailsDV:KnownToISO-inputEl']");
		dateOfLoss = By.xpath("//label[text()='Date of Loss']/../..//input");
		lossDescription = By.xpath("//textarea[contains(@id,'LossDetailsDV:Description-inputEl')]");
		
		subrogatableRadioBtnYES = By.xpath("//input[contains(@id,'pure_subrogatable_true-inputEl')]");
		subrogatableRadioBtnNO = By.xpath("//input[contains(@id,'pure_subrogatable_false-inputEl')]");
		ClearValidationAlert = By.xpath("//span[@id='WebMessageWorksheet:WebMessageWorksheetScreen:WebMessageWorksheet_ClearButton-btnInnerEl']");
		subrogationStatus = By.xpath("//div[@id='ClaimLossDetails:ClaimLossDetailsScreen:LossDetailsPanelSet:pureSubroLargeLoss:PureSubroLargeLossInputSet:pure_subrogatable-inputEl']");
		referToSubroRadioBtnYES = By.xpath("//input[contains(@id,'PureSubroLargeLossInputSet:SIinfo_EscalateSubro_true-inputEl')]");
		referToSubroRadioBtnNO = By.xpath("//input[contains(@id,'PureSubroLargeLossInputSet:SIinfo_EscalateSubro_false-inputEl')]");
		insuredLiability = By.xpath("//input[contains(@id,'LossDetailsCardCV:LossDetailsDV:Notification_AtFaultPercentage-inputEl')]");
		subrogatableStatus = By.xpath("//div[contains(@id,'PureSubroLargeLossInputSet:pure_subrogatable-inputEl')]");
		assignToXactAnalysis = By.xpath("//span[@id='ClaimLossDetails:ClaimLossDetailsScreen:ClaimLossDetails_AssignToXactButton-btnInnerEl']");
		largeLossRadioBtnYES = By.xpath("//input[contains(@id,'PureSubroLargeLossInputSet:pure_largeLoss_true-inputEl')]");
		largeLossRadioBtnNO = By.xpath("//input[contains(@id,'PureSubroLargeLossInputSet:pure_largeLoss_false-inputEl')]");
	}
	
}
