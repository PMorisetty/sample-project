package com.pure.claim;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class SecondaryApprovalPageLib extends ActionEngine {
	public SecondaryApprovalPageLib(EventFiringWebDriver webDriver, CReporter reporter) throws Throwable {
		this.driver = webDriver;
		this.reporter = reporter;
		isElementPresent(SecondaryApprovalPage.tittle, "secondary approval page tittle");
		printPageTitle(SecondaryApprovalPage.tittle,"Page title");
	}

	public void ApproveSelectedRow(String claimNumber) throws Throwable{
		try{
			List<WebElement> checkbox = driver.findElements(By.xpath("//a[text()='"+claimNumber+"']/../../preceding-sibling::td/div"));
			for(WebElement ele: checkbox){
				ele.click();
				waitForMask();
			}
		}catch(Exception e){}
		scroll(SecondaryApprovalPage.ApproveSelectedRows,"ApproveSelectedRows");
		click(SecondaryApprovalPage.ApproveSelectedRows,"ApproveSelectedRows");
		reporter.SuccessReport("Selected Claim " + claimNumber +" from the secondary approval list",
				"Approved");
		//================================================================================
		//Wait till the approve selected rows button gets disabled.
		int i = 0;
		try{
			while(driver.findElement(SecondaryApprovalPage.ApproveSelectedRows).isEnabled() && i<10){		
				i++;
				if(i==10){
					break;
				}
			}
		}finally{
			
		}
	}
}
