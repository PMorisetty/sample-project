package com.pure.claim;

import org.openqa.selenium.By;

public class ReviewAndSaveClaimPage {
	
	public static By finishButton;
	public static By clearButton;
	public static By validationResultsTab;
	
	
	static{
		finishButton = By.id("FNOLWizard:Finish-btnInnerEl");
		clearButton = By.cssSelector("span[id$='WebMessageWorksheet_ClearButton-btnInnerEl']");
		validationResultsTab = By.id("wsTabBar:wsTab_0-btnInnerEl");
	
	}

}
