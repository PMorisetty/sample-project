package com.pure.claim;

import org.openqa.selenium.By;

public class SearchForAppraisersPage {
	static By assignToIA, assignToPureExpress, assignToDRP, pureTestIASelect, assignSalvageToCoPart, 
	shopOfChoice, comSearch, noEIHAssignToIA, title, totalLossResult, zipcode;
	
	static{
		assignToIA = By.xpath("//a[text()='Assign to IA']");
		assignToPureExpress = By.xpath("//a[text()='Assign to PURE Express']");
		assignToDRP = By.xpath("//a[text()='Assign to DRP']");
		shopOfChoice = By.xpath("//a[text()='Select Shop of Choice(if available)']");
		comSearch = By.xpath("//a[text()='EIH w/photos < $5000  Ex-RI $2000/MA-1500  assign Comsearch']");
		noEIHAssignToIA = By.xpath("//a[text()='NO EIH from SOC or EIH from SOC with NO Photos = Assign to Non-Network Shop of Choice (if avail)']");
		assignSalvageToCoPart = By.xpath("//div[text()='Assign Salvage to CoPart, Assign Appraisal to ComSearch']");
		pureTestIASelect = By.xpath("//div[text()='PURE TEST IA']/../..//a");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		totalLossResult = By.cssSelector("div[id$=\"totalLossLbl-inputEl\"]");
		
		zipcode = By.id("VehicleIncidentPredictiveAppraiserAssignmentSearchPopup:PostalCode-inputEl");
	}

}
