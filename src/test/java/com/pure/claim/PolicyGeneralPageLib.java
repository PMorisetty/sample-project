package com.pure.claim;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PolicyGeneralPageLib extends ActionEngine{
	
	public PolicyGeneralPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable{
		this.driver = driver;
		this.reporter = reporter;
		waitForMask();
		printPageTitle(PolicyGeneralPage.title,"Page title");
	}
	
	public void clickEditBtn() throws Throwable{
		click(PolicyGeneralPage.editBtn, "Edit Button");
		waitForMask();
	}
	
	//Will return column w.r.t. last row in table
	private List<WebElement> getColumns() throws Throwable{
		waitForVisibilityOfElement(PolicyGeneralPage.policyLevelCoverageDiv, "Policy level coverage div");
		List<WebElement> tableRows = driver.findElement(PolicyGeneralPage.policyLevelCoverageDiv).findElement(By.tagName("table")).findElements(By.tagName("tr"));
		List<WebElement> tableColumns = tableRows.get(tableRows.size() - 1).findElements(By.tagName("td"));
		return tableColumns;
	}
	
	public void addPolicyLevelCoverages(Hashtable<String, String> data)throws Throwable{
		String coveragetype,deductible,exposurelimit,incidentlimit,policyCoverageNote;
		coveragetype 	= data.get("coveragetype");
		deductible		= data.get("deductible");
		exposurelimit	= data.get("exposurelimit");
		incidentlimit	= data.get("incidentlimit");
		policyCoverageNote = data.get("policyCoverageNote");
		
		click(PolicyGeneralPage.addBtnPolicyCoverage,"addBtnPolicyCoverage");
		waitForMask();
		setPolicyLevelCoverageType(coveragetype);
		setPolicyLevelCoverageDeductible(deductible);
		setPolicyLevelCoverageExposureLimit(exposurelimit);
		setPolicyLevelCoverageIncidentLimit(incidentlimit);
		setPolicyLevelCoverageNote(policyCoverageNote);
		click(PolicyGeneralPage.updateBtn,"Update button");
		waitForMask();
	}
	
	public void setPolicyLevelCoverageType(String type) throws Throwable{
		WebElement elem = getColumns().get(1); 
		Actions actions = new Actions(driver);
		actions.moveToElement(elem).doubleClick(elem).perform();
		type(PolicyGeneralPage.policyLevelCoverageType,type,"Coverage type");
		actions.sendKeys(Keys.TAB).perform();
		waitForMask();
		reporter.SuccessReport("Enter coverage type", "Entered '"+type +"' in coverage type."  );
	}
	
	public void setPolicyLevelCoverageDeductible(String deductible) throws Throwable{
		WebElement elem = getColumns().get(2); 
		new Actions(driver).doubleClick(elem).sendKeys(deductible)
		.build().perform();
		reporter.SuccessReport("Enter coverage deductible", "Entered '"+deductible +"' in coverage deductible."  );
	}
	
	public void setPolicyLevelCoverageExposureLimit(String exposureLimit) throws Throwable{
		WebElement elem = getColumns().get(3); 
		new Actions(driver).doubleClick(elem).sendKeys(exposureLimit)
		.build().perform();
		reporter.SuccessReport("Enter coverage exposure limit", "Entered '"+exposureLimit +"' in coverage exposure limit."  );
	}
	
	public void setPolicyLevelCoverageIncidentLimit(String incidentLimit) throws Throwable{
		WebElement elem = getColumns().get(4); 
		new Actions(driver).doubleClick(elem).sendKeys(incidentLimit)
		.build().perform();
		reporter.SuccessReport("Enter coverage incident limit", "Entered '"+incidentLimit +"' in coverage incident limit."  );
	}
	
	public void setPolicyLevelCoverageNote(String note) throws Throwable{
		WebElement elem = getColumns().get(5); 
		Actions actions = new Actions(driver);
		actions.doubleClick(elem).sendKeys(note)
		.build().perform();
		actions.sendKeys(Keys.TAB).perform();
		waitForMask();
		reporter.SuccessReport("Enter coverage additional note", "Entered '"+note +"' in coverage additional note."  );
	}
	
	public void clickSelectPolicy() throws Throwable{
		click(PolicyGeneralPage.selectPolicyBtn, "selectPolicyBtn");
	}
	
	public void verifyPolicyNumber(String expectedPolicyNo) throws Throwable{
		assertText(PolicyGeneralPage.policyNumber, expectedPolicyNo);
	}

}
