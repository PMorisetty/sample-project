package com.pure.claim;

import org.openqa.selenium.By;

public class QuickClaimAutoPage {
	
	static By reporterName;
	static By relationToInsured;
	static By description,
	location,address1,
	address2, city,country,state,zip,
	incidentOnly,incidentOnlyYes, incidentOnlyNo,
	coverageQuestionYes,coverageQuestionNo, title,
	addressLine1,addressLine2,
	lossCategory, lossCause, dateOfNotice, zipIcon,
	assignDropDown, assignPicker,
	finishButton, validationResultsTab, clearButton,
	duplicateClaimTab, closeButton, addVehicle;
	
	static{
		reporterName = By.cssSelector("[id*=\"ReportedBy_Name-inputEl\"]");
		relationToInsured = By.cssSelector("[id*=\"Claim_ReportedByType-inputEl\"]");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		description = By.cssSelector("[id*=\"Description-inputEl\"]");
		location = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:NewClaimWizard_LossDetailsScreen:NewClaimLossDetailsDV:LossDetailsAddressDetailInputSet:LossLocation_Name-inputEl");
		addressLine1 = By.xpath("//label[text()='Address 1']/../..//input");
		addressLine2 = By.xpath("//label[text()='Address 2']/../..//input");
		city = By.cssSelector("[id*=\"City-inputEl\"]");
		country = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:NewClaimWizard_LossDetailsScreen:NewClaimLossDetailsDV:LossDetailsAddressDetailInputSet:LossLocation_County-inputEl");
		state = By.id("FNOLWizard:AutoWorkersCompWizardStepSet:NewClaimWizard_LossDetailsScreen:NewClaimLossDetailsDV:LossDetailsAddressDetailInputSet:LossLocation_State-inputEl");
		zip = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//input");
		lossCategory = By.xpath("//label[text()='Loss Category']/../..//input");
		lossCause = By.xpath("//label[text()='Loss Cause']/../..//input");
		dateOfNotice = By.cssSelector("[id*=\"Notification_ReportedDate-inputEl\"]");
		zipIcon = By.xpath("//label[text()='Zip Code' or text()='ZIP Code']/../..//a");
		coverageQuestionNo = By.cssSelector("[id*=\"Status_CoverageQuestion_false-inputEl\"]");
		coverageQuestionYes = By.cssSelector("[id*=\"Status_CoverageQuestion_true-inputEl\"]");
		incidentOnlyYes = By.cssSelector("[id*=\"Status_IncidentReport_true-inputEl\"]");
		incidentOnlyNo = By.cssSelector("[id*=\"Status_IncidentReport_false-inputEl\"]");
		assignDropDown = By.cssSelector("input[id$='CommonAssign-inputEl']");
		assignPicker = By.cssSelector("a[id$='CommonAssign:CommonAssign_PickerButton']");
		finishButton = By.id("FNOLWizard:Finish-btnInnerEl");
		validationResultsTab = By.id("wsTabBar:wsTab_0-btnInnerEl");
		clearButton = By.cssSelector("span[id$='WebMessageWorksheet_ClearButton-btnInnerEl']");
		duplicateClaimTab = By.id("wsTabBar:wsTab_0-btnInnerEl");
		closeButton = By.cssSelector("span[id$='NewClaimDuplicatesWorksheet_CloseButton-btnInnerEl']");
		addVehicle = By.id("FNOLWizard:FNOLWizard_NewQuickClaimScreen:QuickClaimDV:EditableVehicleIncidentsLV_tb:Add-btnInnerEl");
	}

}
