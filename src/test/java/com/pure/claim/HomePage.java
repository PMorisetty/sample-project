package com.pure.claim;

import org.openqa.selenium.By;

public class HomePage {
	public static By userName;
	public static By password;
	public static By loginButton;
	public static By gotoLink;
	public static By pureUserName;
	public static By purePassword;
	public static By pureLoginButton;
	public static By desktopMenu;
	public static By activitiesLinkInDesktop;
	public static By claimsLinkInDesktop;
	public static By exposuresLinkInDesktop;
	public static By subrogationLinkInDesktop;
	public static By maServicesLinkInDesktop;
	public static By bulkInvoicesInDesktop;
	public static By desktopMenuCalenderSubMenu;
	public static By myCalenderLink;
	public static By claimsMenu;
	public static By newClaimLink;
	public static By claimNumberTextbox;
	public static By searchMenu;
	public static By searchMenuClaimsSubMenu;
	public static By simpleSearchLink;
	public static By advancedSearchLink;
	public static By searchByContactLink;
	public static By activitiesLink;
	public static By checksLink;
	public static By recoveriesLink;
	public static By addressBookMenu;
	public static By searchLink;
	public static By settingIcon;
	public static By logoutLink, secondaryApproval;
	public static By bulkInvoice, activities; 
	
	static By messageBox;
	static By messageBoxOkButton;
	static By messageBoxCancelButton,administrationMenu,administrationLink;
	
	public static By claimCenterLink;
	
	
	static {
		userName = By.id("Login:LoginScreen:LoginDV:username-inputEl");
		password = By.id("Login:LoginScreen:LoginDV:password-inputEl");
		loginButton = By.id("Login:LoginScreen:LoginDV:submit-btnInnerEl");
		gotoLink = By.xpath("//a[@class='link goto']");
		pureUserName = By.id("okta-signin-username");
		purePassword = By.id("okta-signin-password");
		pureLoginButton = By.id("okta-signin-submit");
		desktopMenu = By.id("TabBar:DesktopTab");
		activitiesLinkInDesktop = By.id("TabBar:DesktopTab:Desktop_DesktopActivities-textEl");
		claimsLinkInDesktop = By.id("TabBar:DesktopTab:Desktop_DesktopClaims-textEl");
		exposuresLinkInDesktop = By.id("TabBar:DesktopTab:Desktop_DesktopExposures-textEl");
		subrogationLinkInDesktop = By.id("TabBar:DesktopTab:Desktop_DesktopPURESubro-itemEl");
		maServicesLinkInDesktop = By.id("TabBar:DesktopTab:Desktop_DesktopPUREMA-textEl");
		bulkInvoicesInDesktop = By.id("TabBar:DesktopTab:Desktop_BulkPay-textEl");
		desktopMenuCalenderSubMenu = By.id("TabBar:DesktopTab:Desktop_DesktopCalendarGroup");
		myCalenderLink = By.id("TabBar:DesktopTab:Desktop_DesktopCalendarGroup:DesktopCalendarGroup_Calendar-itemEl");
		claimsMenu = By.xpath("//a[@id='TabBar:ClaimTab']");
		newClaimLink = By.id("TabBar:ClaimTab:ClaimTab_FNOLWizard-itemEl");
		claimNumberTextbox = By.id("TabBar:ClaimTab:ClaimTab_FindClaim-inputEl");
		searchMenu = By.id("TabBar:SearchTab");
		searchMenuClaimsSubMenu = By.id("TabBar:SearchTab:Search_ClaimSearchesGroup");
		simpleSearchLink = By.id("TabBar:SearchTab:Search_ClaimSearchesGroup:ClaimSearchesGroup_SimpleClaimSearch-textEl");
		advancedSearchLink = By.id("TabBar:SearchTab:Search_ClaimSearchesGroup:ClaimSearchesGroup_ClaimSearch-itemEl");
		searchByContactLink = By.id("TabBar:SearchTab:Search_ClaimSearchesGroup:ClaimSearchesGroup_FreeTextClaimSearch-textEl");
		activitiesLink = By.id("TabBar:SearchTab:Search_ActivitySearch-textEl");
		checksLink = By.id("TabBar:SearchTab:Search_PaymentSearch-textEl");
		recoveriesLink = By.id("TabBar:SearchTab:Search_RecoverySearch-textEl");
		addressBookMenu = By.id("TabBar:AddressBookTab");
		searchLink = By.id("TabBar:AddressBookTab:AddressBook_AddressBookSearch-itemEl");
		settingIcon = By.id(":TabLinkMenuButton-btnIconEl");
		logoutLink = By.id("TabBar:LogoutTabBarLink-textEl");
		secondaryApproval = By.id("Desktop:MenuLinks:Desktop_PURESecondaryApprovalActivities");
		
		messageBox = By.cssSelector(".x-message-box");
		messageBoxOkButton = By.xpath("//div[contains(@class,'x-message-box')]//span[contains(@id, 'btnInnerEl')][.='OK']");
		messageBoxCancelButton = By.xpath("//div[contains(@class,'x-message-box')]//span[contains(@id, 'btnInnerEl')][.='Cancel']");
		
//		bulkInvoice = By.id("Desktop:MenuLinks:Desktop_BulkPay");
		bulkInvoice = By.xpath("//span[text()='Bulk Invoices']");
		activities = By.id("Desktop:MenuLinks:Desktop_DesktopActivities");
		
		claimCenterLink = By.xpath("//a[contains(@href,'calimcenter')]");
		
		administrationMenu = By.id("TabBar:AdminTab");
		administrationLink = By.id("TabBar:AdminTab:AdminTab_Admin-itemEl");
	}
}
