package com.pure.claim;

import org.openqa.selenium.By;

public class NewNotePage {
	
	public static By 
	newNoteTitle, 
	shareOnMemberPortal, 
	topic, 
	confidentialYes,
	confidentialNo, 
	text, 
	update, 
	cancel,
	useTemplate,
	search,
	select,
	linkDocument,
	searchDocument,
	selectDocument,
	selectBtn,
	clearBtn,
	zeroResultsMsg,
	returnToNote,
	subject;
	
	static{
		newNoteTitle = By.xpath("//span[text()='New Note']");
		shareOnMemberPortal = By.xpath("//label[text()='Share on Member Portal']/../..//input");
		topic = By.xpath("//label[text()='Topic']/../..//input");
		confidentialYes = By.cssSelector("[id*=\"Confidential_true-inputEl\"]");
		confidentialNo = By.cssSelector("[id*=\"Confidential_false-inputEl\"]");
		text = By.xpath("//label[text()='Text']/../..//textarea");
		update = By.xpath("//span[contains(@id,'Update') and contains(@id,'btnInnerEl')]");
		cancel = By.id("NewNoteWorksheet:NewNoteScreen:Cancel-btnInnerEl");
		
		useTemplate = By.id("NewNoteWorksheet:NewNoteScreen:NewNoteWorksheet_UseTemplateButton-btnInnerEl");
		search = By.id("PickNoteTemplatePopup:PickNoteTemplateScreen:NoteTemplateSearchDV:SearchAndResetInputSet:SearchLinksInputSet:Search");
		select = By.id("PickNoteTemplatePopup:PickNoteTemplateScreen:NoteTemplateSearchResultLV:0:_Select");
		
		linkDocument = By.id("NewNoteWorksheet:NewNoteScreen:NewNoteWorksheet_AddMultipleDocumentButton-btnInnerEl");
		searchDocument = By.xpath("//a[contains(@id, 'SearchLinksInputSet:Search')]");
		selectDocument = By.xpath("//div[@id='PickExistingMultipleDocumentPopup:Claim_DocumentsScreen:PickExistingDocumentsLV-body']//td[2]//img");
		selectBtn = By.id("PickExistingMultipleDocumentPopup:Claim_DocumentsScreen:PickExistingDocumentsLV_tb:LinkDocument_Button-btnInnerEl");
		clearBtn = By.xpath("//span[contains(@id, 'ClearButton-btnInnerEl')]");
		zeroResultsMsg = By.xpath("//div[text()='The search returned zero results.']");
		returnToNote = By.xpath("//a[text()='Return to Note']");
		
		subject = By.xpath("//label[text()='Subject']/ancestor::td[1]/..//input");
	}
}

