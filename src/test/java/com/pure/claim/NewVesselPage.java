package com.pure.claim;

import org.openqa.selenium.By;

public class NewVesselPage {

	static By title, year, length, manufacturer, model, hull,
	okBtn, cancelBtn;
	
	static{
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		year = By.xpath("//input[@id='PURE_NewClaimWizard_NewPolicyWatercraftPopup:PURE_PolicyVesselDetailPanelSet:Vessel_Year-inputEl']");
		length = By.xpath("//input[@id='PURE_NewClaimWizard_NewPolicyWatercraftPopup:PURE_PolicyVesselDetailPanelSet:Vessel_Length-inputEl']");
		manufacturer = By.xpath("//input[@id='PURE_NewClaimWizard_NewPolicyWatercraftPopup:PURE_PolicyVesselDetailPanelSet:Vessel_Manufacturer-inputEl']");
		model = By.xpath("//input[@id='PURE_NewClaimWizard_NewPolicyWatercraftPopup:PURE_PolicyVesselDetailPanelSet:Vessel_Model-inputEl']");
		hull = By.xpath("//input[@id='PURE_NewClaimWizard_NewPolicyWatercraftPopup:PURE_PolicyVesselDetailPanelSet:Vessle_HullID-inputEl']");
		okBtn = By.id("PURE_NewClaimWizard_NewPolicyWatercraftPopup:Update-btnInnerEl");
		cancelBtn = By.id("PURE_NewClaimWizard_NewPolicyWatercraftPopup:Cancel-btnInnerEl");
	}
}
