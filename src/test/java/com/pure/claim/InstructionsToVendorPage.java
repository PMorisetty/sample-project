package com.pure.claim;

import org.openqa.selenium.By;

public class InstructionsToVendorPage {
	static By coverage, instructionsToVendor, locationContact, updateBtn, snapSheetContactNo, title,
				additionalInst, comSearchDocMsg, attachBtn, party;
	
	static {
		coverage = By.xpath("//input[contains(@id,'mappedCoverage-inputEl')]");
		instructionsToVendor = By.xpath("//input[contains(@id,'vendorInstructionType-inputEl')]");
		locationContact = By.xpath("//input[contains(@id,'CustomerContact-inputEl')]");
		updateBtn = By.xpath("//span[contains(@id,'Update-btnInnerEl')]");
		snapSheetContactNo = By.xpath("//input[contains(@id,'SnapsheetCell-inputEl')]");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		additionalInst = By.xpath("//textarea[contains(@id,'InstructionText-inputEl')]");
		comSearchDocMsg = By.xpath("//span[@id='PURE_EditServiceRequestPopup:isComSearchVisible']");
		attachBtn = By.cssSelector("span[id$=\"AddDocumentButton-btnInnerEl\"]");
		party = By.xpath("//input[contains(@id,'LocationContactInput1-inputEl')]");
	}
}
