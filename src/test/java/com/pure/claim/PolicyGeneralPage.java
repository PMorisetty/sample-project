package com.pure.claim;

import org.openqa.selenium.By;

public class PolicyGeneralPage {

	static By editBtn, policyLevelCoverageDiv, policyLevelCoverageType, addBtnPolicyCoverage, title, updateBtn,
	selectPolicyBtn, policyNumber;
	static{
		editBtn = By.id("ClaimPolicyGeneral:ClaimPolicyGeneralScreen:ClaimPolicyGeneral_EditButton-btnInnerEl");
		policyLevelCoverageDiv = By.cssSelector("div[id*=\"PolicyCoveragesLV-body\"]");
		policyLevelCoverageType = By.xpath("//input[@name='CoverageType']");
		addBtnPolicyCoverage = By.xpath("//span[contains(@id,'PolicyCoveragesLV_tb:Add-btnEl')]");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		updateBtn = By.id("ClaimPolicyGeneral:ClaimPolicyGeneralScreen:Update-btnInnerEl");
		selectPolicyBtn = By.id("ClaimPolicyGeneral:ClaimPolicyGeneralScreen:ClaimPolicyGeneral_SelectPolicyButton-btnInnerEl");
		policyNumber = By.xpath("//label[text()='Policy Number']/../..//div");
	}
}
