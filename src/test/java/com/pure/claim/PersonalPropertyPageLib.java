package com.pure.claim;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PersonalPropertyPageLib extends ActionEngine{

	public PersonalPropertyPageLib(EventFiringWebDriver driver, CReporter reporter){
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void setDamageOrLossDescription(String damageOrLossDescription) throws Throwable{
		type(PersonalPropertyPage.damageOrLossDescription, damageOrLossDescription, "damageOrLossDescription");
	}
	
	public void clickOk() throws Throwable{
		click(PersonalPropertyPage.okBtn, "okBtn");
		waitForMask();
	}
	
	public void fillDetails(String damageOrLossDescription) throws Throwable{
		setDamageOrLossDescription(damageOrLossDescription);
		clickOk();
	}
}
