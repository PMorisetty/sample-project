package com.pure.claim;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class InjuryIncidentPageLib extends ActionEngine{

	public InjuryIncidentPageLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable{
		this.driver = driver;
		this.reporter = reporter;
	}
	
	public void clickOk() throws Throwable{
		scroll(InjuryIncidentPage.okBtn, "okBtn");
		click(InjuryIncidentPage.okBtn, "okBtn");
	}
	
	public void setInjuredPerson(String injuredPerson) throws Throwable{
		type(InjuryIncidentPage.injuredPerson, injuredPerson, "injuredPerson");
		type(InjuryIncidentPage.injuredPerson, Keys.TAB, "Tab key");
	}
	
	public void setDescribeInjuries(String describeInjuries) throws Throwable{
		type(InjuryIncidentPage.describeInjury, describeInjuries, "describeInjuries");
	}
	
	public void addBodyParts(String areaOfBody, String detailedBodyPart) throws Throwable{
		click(InjuryIncidentPage.addBodyParts, "addBodyParts");
		WebElement ele = driver.findElement(InjuryIncidentPage.areaOfBody);
		Actions actions = new Actions(driver);
		actions.doubleClick(ele).sendKeys(Keys.BACK_SPACE).perform();
		type(InjuryIncidentPage.inputAreaOfBody, areaOfBody, "inputAreaOfBody");
		type(InjuryIncidentPage.inputAreaOfBody, Keys.TAB, "Tab key");
		
		ele = driver.findElement(InjuryIncidentPage.inputDetailedBodyPart);
		actions.doubleClick(ele).sendKeys(Keys.BACK_SPACE).perform();
		type(InjuryIncidentPage.inputDetailedBodyPart, detailedBodyPart, "inputDetailedBodyPart");
		type(InjuryIncidentPage.inputDetailedBodyPart, Keys.TAB, "Tab key");
	}
	
	public void addMedicalDiagnosis(String icdCode) throws Throwable{
		click(InjuryIncidentPage.addMedicalDiagnosis, "addMedicalDiagnosis");
		click(InjuryIncidentPage.icdCode, "icdCode");
		type(InjuryIncidentPage.icdCodeInput, icdCode, "ICD code input");
		type(InjuryIncidentPage.icdCodeInput, Keys.TAB, "Tab key");
		waitUntilJQueryReady();
		
	}
	
	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		String injuredPerson = data.get("injuredPerson");
		String description = data.get("injuryDescription");
		String areaOfBody = data.get("areaOfBody");
		String detailedBodyPart = data.get("detailedBodyPart");
		String icdCode = data.get("icdCode");
		
		setInjuredPerson(injuredPerson);
		setDescribeInjuries(description);
		addBodyParts(areaOfBody, detailedBodyPart);
		addMedicalDiagnosis(icdCode);
		clickOk();
	}
	
	
}
