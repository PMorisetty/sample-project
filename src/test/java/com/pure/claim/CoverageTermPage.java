package com.pure.claim;

import org.openqa.selenium.By;

public class CoverageTermPage {
	static By okButton;
	static By cancelButton;
	static By applicableTo;
	static By per;
	static By typeLabel;
	static By code;
	static By description;
	static By value;
	static By units;
	static By amount;

	static{
		okButton = By.id("CovTermPopup:Update-btnInnerEl");
		cancelButton = By.id("CovTermPopup:Cancel-btnInnerEl");
		applicableTo = By.id("CovTermPopup:ModelRestriction-inputEl");
		per = By.id("CovTermPopup:ModelAggregation-inputEl");
		typeLabel = By.id("CovTermPopup:Type-inputEl");
		code = By.id("CovTermPopup:CovTermTypeDetailsInputSet:CovTermTypeDetailsInputSet:Code-inputEl");
		description = By.id("CovTermPopup:CovTermTypeDetailsInputSet:CovTermTypeDetailsInputSet:Description-inputEl");
		value = By.id("CovTermPopup:CovTermTypeDetailsInputSet:CovTermTypeDetailsInputSet:NumericValue-inputEl");
		units = By.id("CovTermPopup:CovTermTypeDetailsInputSet:CovTermTypeDetailsInputSet:Units-inputEl");
		amount = By.id("CovTermPopup:CovTermTypeDetailsInputSet:CovTermTypeDetailsInputSet:FinancialAmount-inputEl");
	}
}
