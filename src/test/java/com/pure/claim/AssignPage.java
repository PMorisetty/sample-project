package com.pure.claim;

import org.openqa.selenium.By;

public class AssignPage {

	static By title, assignDropDown, assignBtn;
	
	static {
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		assignDropDown = By.cssSelector("input[id$=\"AssignmentPopupDV:SelectFromList-inputEl\"]");
		assignBtn = By.cssSelector("span[id$=\"cAssignmentPopupScreen_ButtonButton-btnInnerEl\"]");
	}
}
