package com.pure.claim;

import org.openqa.selenium.By;

public class NewExposurePage {

	static By okBtn,cancelBtn, updateBtn,
	coverageShown, claimant,
	claimantType,propertyDescription,damageDescription,
	iaUsedYES,iaUsedNO, deskAdjustmentYES, deskAdjustmentNO,
	newPropertyIncidentIcon, newPropertyIncident, editPropertyIncident,
	lossOfUseYes,lossOfUseNo,lossEstimate,lostItem,
	location, title, exposureMsg, refreshBtn, vehicleIncident;
	
	static{
		okBtn = By.xpath("//span[contains(@id,'Update-btnInnerEl')]");
		updateBtn = By.id("NewExposure:NewExposureScreen:Update-btnInnerEl");
		cancelBtn = By.id("NewClaimWizard_NewExposurePopup:NewClaimWizard_ExposurePageScreen:Cancel-btnInnerEl");
		coverageShown = By.xpath("//input[contains(@id,'Coverage-inputEl')]");
		claimant = By.xpath("//input[contains(@id,'Claimant_Picker-inputEl')]");
		claimantType = By.xpath("//input[contains(@id,'Claimant_Type-inputEl')]");
		propertyDescription = By.xpath("//input[contains(@id,'Description-inputEl')]");
		damageDescription = By.xpath("//textarea[contains(@id,'Description-inputEl')]");
		iaUsedYES = By.xpath("//input[contains(@id,'IAUsed_true-inputEl')]");
		iaUsedNO = By.xpath("//input[contains(@id,'IAUsed_false-inputEl')]");
		deskAdjustmentYES = By.xpath("//input[contains(@id,'Deskadjustment_true-inputEl')]");
		deskAdjustmentNO = By.xpath("//input[contains(@id,'Deskadjustment_false-inputEl')]");
		lossOfUseYes = By.xpath("//input[contains(@id,'Exposure_LossofUse_true-inputEl')]");
		lossOfUseNo = By.xpath("//input[contains(@id,'Exposure_LossofUse_false-inputEl')]");
		lossEstimate = By.xpath("//input[contains(@id,'LossEstimate-inputEl')]");
		newPropertyIncidentIcon = By.xpath("//a[contains(@id,'IncidentMenuIcon')]");
		newPropertyIncident = By.xpath("//span[contains(text(),'New Incident')]");
		editPropertyIncident = By.xpath("//span[contains(@id, 'EditIncidentMenuItem')]");
		location = By.xpath("//input[contains(@id,'Address_Picker-inputEl')]");
		lostItem = By.cssSelector("input[id*=\"lostItem-inputEl\"]");
		title = By.xpath("//div[@id='centerPanel']//span[@class='g-title' and contains(@id,'ttlBar')]");
		exposureMsg = By.xpath("//div[contains(@id, 'ClaimExposuresScreen:_msgs')]");
		refreshBtn = By.xpath("//span[contains(@id, 'Refresh-btnInnerEl')]");
		vehicleIncident = By.xpath("//input[contains(@id,'Vehicle_Incident-inputEl')]");
	}
}
