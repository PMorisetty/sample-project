package com.pure.claim;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class SearchAddressBookPageLib extends ActionEngine{
	
	public SearchAddressBookPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void setType(String contractorType) throws Throwable{
		type(SearchAddressBookPage.type, contractorType, "Contractor Type");
		type(SearchAddressBookPage.type, Keys.TAB, "Tab keys");
	}
	
	public void setName(String contractorName) throws Throwable{
		type(SearchAddressBookPage.name, contractorName, "Contractor name");
	}
	
	public void setServiceState(String serviceState) throws Throwable{
		type(SearchAddressBookPage.serviceState, serviceState, "Service State");
	}
	
	public void clickSearch() throws Throwable{
		click(SearchAddressBookPage.searchBtn, "Search button");
	}
	
	public void clickSelect() throws Throwable{
		waitForVisibilityOfElement(SearchAddressBookPage.selectBtn, "Select button");
		click(SearchAddressBookPage.selectBtn, "Select button");
	}

	public void searchNSelectFromAddressBook(Hashtable<String, String> data) throws Throwable{
		printPageTitle(SearchAddressBookPage.title,"Page title");
		setType(data.get("searchType"));
		setName(data.get("searchName"));
		clickSearch();
		clickSelect();
	}
	
	public void clickCreateNewServiceProvider() throws Throwable{
		click(SearchAddressBookPage.createNewServiceProviderBtn, "Create New Service Provider button");
	}
	
	public void verifyNumberOfSearchResults()throws Throwable{
		waitForMask();
		int noOfSearchResults = getElementsSize(SearchAddressBookPage.searchResultsDiv);
		if(noOfSearchResults>0)
			reporter.SuccessReport("Search Results", "One or More Search Results are available");
		else
			reporter.failureReport("Search Results", "No Search Results are available", driver);
	}
	
	public void clickNameSearchResult()throws Throwable{
		waitForVisibilityOfElement(SearchAddressBookPage.nameSearchResult, "Results Name Link");
		click(SearchAddressBookPage.nameSearchResult, "Results Name Link");
	}
	
	public String getServiceProviderNameText()throws Throwable{
		waitForVisibilityOfElement(SearchAddressBookPage.serviceProviderName, "ServiceProviderName");
		return getText(SearchAddressBookPage.serviceProviderName, "ServiceProviderName");
	}
	
	
}
