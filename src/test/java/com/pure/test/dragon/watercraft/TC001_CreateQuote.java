package com.pure.test.dragon.watercraft;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.HomeLib;
import com.pure.dragon.watercraft.PremiumDetailsLib;
import com.pure.dragon.watercraft.QuoteCreation;
import com.pure.report.ConfigFileReadWrite;
import com.pure.utilities.TestUtil;

public class TC001_CreateQuote extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestData(), counter);
	}

	@DataProvider
	private Object[][] getTestData() {
		TestUtil.getData("TC001_CreateQuote", TestDataDragon_Watercraft,
				"TestDetails");
		return TestUtil.getData("TC001_CreateQuote", TestDataDragon_Watercraft,
				"Testdata");
	}
	
	@Test(dataProvider = "getTestData")
	public void TC001_CreateQuote_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			String newPolicyNumber = new QuoteCreation(driver, reporter).createQuote(data);
			System.out.println("Policy Number::::::::::::" + newPolicyNumber);
//			PremiumDetailsLib plib = new PremiumDetailsLib(driver, reporter);
//			plib.updateRater(data);
			status = true;
		}
	}

	@AfterMethod
	public void afterTest() throws Throwable {
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		// Qtest.UpdateQTestResults(status, "US4441_Quote Creation", startTime,
		// endTime);
	}
}
