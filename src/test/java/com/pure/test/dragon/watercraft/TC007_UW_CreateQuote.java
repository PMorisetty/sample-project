package com.pure.test.dragon.watercraft;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.watercraft.QuoteCreation;
import com.pure.report.ConfigFileReadWrite;
import com.pure.utilities.TestUtil;

public class TC007_UW_CreateQuote extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestData(), counter);
	}
	
	@DataProvider
	private Object[][] getTestData() {
		TestUtil.getData("TC007_CreateQuote", TestDataDragon_WatercraftUW,
				"TestDetails");
		return TestUtil.getData("TC007_CreateQuote", TestDataDragon_WatercraftUW,
				"Testdata");
	}
	
	@Test(dataProvider = "getTestData")
	public void TC007_UW_CreateQuote_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			String newPolicyNumber = new QuoteCreation(driver, reporter).createQuote(data);
			System.out.println("Policy Number::::::::::::" + newPolicyNumber);
			PolicyRenewalLib renewal = new PolicyRenewalLib(driver, reporter);
			renewal.navigateToTransationOrEndorsementTab();
			// click on create renewal button
			renewal.clickCreateRenewalBtn();
			// click on renewal entry
			renewal.clickRenewalEntry();
			// Process the renewal
			UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
			uwReferralsPageLib.clickUnderwriterReferralsTab();
			uwReferralsPageLib.verifyUWReferralForRenewal();
			status = true;
		}
	}

	@AfterMethod
	public void afterTest() throws Throwable {
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		// Qtest.UpdateQTestResults(status, "US4441_Quote Creation", startTime,
		// endTime);
	}
}
