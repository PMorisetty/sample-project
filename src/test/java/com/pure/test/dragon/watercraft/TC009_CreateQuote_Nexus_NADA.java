package com.pure.test.dragon.watercraft;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.AgencyListLib;
import com.pure.dragon.CustomerSummaryPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.NewQuoteBasicInformationLib;
import com.pure.dragon.NewQuoteBasicInformationPage;
import com.pure.dragon.NewQuoteInformationPageLib;
import com.pure.dragon.QuotesLib;
import com.pure.dragon.enumSearchBy;
import com.pure.dragon.watercraft.QuoteCreation;
import com.pure.dragon.watercraft.WatercraftPolicyPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.utilities.TestUtil;

public class TC009_CreateQuote_Nexus_NADA extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}
	
	@DataProvider
	private Object[][] getTestData() {
		return TestUtil.getData("TC009_LexisNexis", TestDataDragon_Watercraft_Lexis_Nexis,
				"TestData");
	}
	
	@Test(dataProvider = "getTestData")
	public void TC009_CreateQuote_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			openApplication("UW");
			HomeLib homeLib = new HomeLib(driver, reporter);
			homeLib.login(data.get("username"), data.get("password"));
			// openApplicationViaTiles();
			homeLib.navigateToQuotes();
			QuotesLib quotesLib = new QuotesLib(driver, reporter);
			quotesLib.clickNewQuoteButton();
			AgencyListLib agencyListLib = new AgencyListLib(driver, reporter);
			agencyListLib.searchAgency(enumSearchBy.BrokerName,
					data.get("brokerName"));
			agencyListLib.SelectAgency(data.get("brokerName"));
			NewQuoteBasicInformationLib newQuote_BasicInformationLib = new NewQuoteBasicInformationLib(driver, reporter);
			newQuote_BasicInformationLib.fillDetails(data);
			newQuote_BasicInformationLib.clickOnlyNxtBtn();
			NewQuoteInformationPageLib newQuoteInformationPageLib = new NewQuoteInformationPageLib(driver, reporter);
			new CustomerSummaryPageLib(driver, reporter).clickNewQuoteBtn();
			newQuoteInformationPageLib.fillDetails("Watercraft", data.get("insuredRiskState"),
					data.get("insuredRiskState") + " temp " + new Date().getDate(),
					data.get("licensedProducer"), data.get("advisor_" + region));
			newQuoteInformationPageLib.clickCreateQuoteBtn();
			WatercraftPolicyPageLib policyPage = new WatercraftPolicyPageLib(driver, reporter);
			policyPage.selectLexisNexisQuestion("Yes");
			System.out.println("Validation in progress");
			policyPage.deleteAllWatercraft();
			
		}
	}

	@AfterMethod
	public void afterTest() throws Throwable {
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		// Qtest.UpdateQTestResults(status, "US4441_Quote Creation", startTime,
		// endTime);
	}
}
