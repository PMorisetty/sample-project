package com.pure.test.dragon.watercraft;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.BillingPageLib;
import com.pure.dragon.CustomerListLib;
import com.pure.dragon.CustomerSummaryPageLib;
import com.pure.dragon.CustomersTABnavigationsLib;
import com.pure.dragon.FinalSummaryPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.InsuranceScoreManagementLib;
import com.pure.dragon.NewQuoteBasicInformationLib;
import com.pure.dragon.PolicyCurrentSummaryPageLib;
import com.pure.dragon.PolicyDeliveryPageLib;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PremiumPageLib;
import com.pure.dragon.QuotesTABnavigationsLib;
import com.pure.dragon.SubjectivitiesPageLib;
import com.pure.dragon.SummaryPageLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.dragon.homeOwners.QuoteDetailsPageLib;
import com.pure.dragon.watercraft.NavigationPageLib;
import com.pure.dragon.watercraft.PremiumDetailsLib;
import com.pure.dragon.watercraft.QuoteCreation;
import com.pure.dragon.watercraft.VesselDescriptionPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.utilities.TestUtil;

public class TC003_CreateQuoteAgent extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestData(), counter);
	}
	@DataProvider
	private Object[][] getTestData() {
		TestUtil.getData("TC003_CreateQuoteAgent", TestDataDragon_Watercraft,
				"TestDetails");
		return TestUtil.getData("TC003_CreateQuoteAgent", TestDataDragon_Watercraft,
				"Testdata");
	}
	
	@Test(dataProvider = "getTestData")
	public void TC003_CreateQuoteAgent_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			String customerID = new QuoteCreation(driver, reporter).createQuoteAgent(data);
			HomeLib homeLib = new HomeLib(driver, reporter);
			homeLib.login(data.get("username"), data.get("password"));
			homeLib.navigateToCustomers();
			//Search for customer and open quote
			new CustomerListLib(driver, reporter).SelectCustomer(customerID);
			if(data.get("Insurance_Score")!=null && data.get("Insurance_Score")!="0" && !data.get("Insurance_Score").contains("No Hit")){
				new CustomersTABnavigationsLib(driver, reporter).NavigateToInsuranceScoreManagement();
				new InsuranceScoreManagementLib(driver, reporter).addNewInsuranceScore(data);					
			}else{
				new CustomersTABnavigationsLib(driver, reporter).NavigateTosummary();
				new CustomerSummaryPageLib(driver, reporter).selectQuote();
				new QuotesTABnavigationsLib(driver, reporter).navigateToQuoteDetails();
			}
//			NewQuoteBasicInformationLib newQuote_BasicInformationLib = new NewQuoteBasicInformationLib(driver, reporter);
//			newQuote_BasicInformationLib.navigateToQuoteDetailsTab();
			NavigationPageLib navigator = new NavigationPageLib(driver, reporter);
			//Vessel Description page
			String year = data.get("watercraft1_year");
			String manufacturer = data.get("watercraft1_manufacturer");
			String model = data.get("watercraft1_model");
			navigator.goToVessel(year, manufacturer, model);
			//Set review date of severe weather plan
			new VesselDescriptionPageLib(driver, reporter).selectHurricaneOverrideGradeIfVisible(data.get("watercraft1_HurricaneGradeOverrideValue"));
			new VesselDescriptionPageLib(driver, reporter).setHurricaneReviewDateIfVisible(data.get("watercraft1_HurricaneGradeOverrideReviewDate"));
			// Click rate button
			CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
			commonPageLib.clickRateBtn();
			PremiumPageLib premiumPageLib = new PremiumPageLib(driver, reporter);
			premiumPageLib.clickPremiumDetailsButton();
			//verify final premium
			PremiumDetailsLib premiumDetailsLib =  new PremiumDetailsLib(driver, reporter);
			premiumDetailsLib.verifyBasePremium(data);
			premiumDetailsLib.clickReturnToPremiumSummaryPage();

			// Navigate to underwriter referrals section
			UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
			uwReferralsPageLib.clickUnderwriterReferralsTab();

			uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
			uwReferralsPageLib.fillOverridden();
			// Select Underwriter
			uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
			// Comments for broker
			uwReferralsPageLib.setCommentsForBroker(data.get("uwComments"));
			// Accept quote
			uwReferralsPageLib.clickAcceptButton();

			// Summary Page - Request issue
			SummaryPageLib summaryPageLib = new SummaryPageLib(driver, reporter);
			summaryPageLib.clickRequestIssueButton();
			premiumPageLib.clickQuoteProposalButton();
			//		for FL state
			uwReferralsPageLib.fillUnderwriterDetailsForFL(data);
			// Bind policy
			premiumPageLib.clickBindButton();

			// Select document delivery type
			PolicyDeliveryPageLib policyDeliveryPageLib = new PolicyDeliveryPageLib(driver, reporter);
			policyDeliveryPageLib.selectDocumentDeliveryType(data);

			// Click next to go to Billing section
			policyDeliveryPageLib.clickNextButton();

			// Select bill delivery
			BillingPageLib billingPageLib = new BillingPageLib(driver, reporter);
			billingPageLib.selectBillingAddress("SendToBilling");

			// click confirm button
			billingPageLib.clickConfirmBtn();

			// Request Bind
			FinalSummaryPageLib finalSummaryPageLib = new FinalSummaryPageLib(driver, reporter);
			finalSummaryPageLib.clickRequestBind();

			// Verify generated policy number
			PolicyCurrentSummaryPageLib policyCurrentSummaryPageLib = new PolicyCurrentSummaryPageLib(driver, reporter);
			policyCurrentSummaryPageLib.verifyPolicyGenerated();
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			//navigate to endorsement tab
			policyEndorsement.navigateToTransationOrEndorsementTab();
			policyEndorsement.navigateToNBtransaction();
			policyEndorsement.navigateToSubjectivities();

			// override subjectivities
			SubjectivitiesPageLib subjectivitiesPageLib = new SubjectivitiesPageLib(driver, reporter);
			subjectivitiesPageLib.overrideValues();
			// Manually bind the policy
			subjectivitiesPageLib.clickManualBindButton();
			policyCurrentSummaryPageLib.getPolicyNumber();
			status = true;
		}
	}

	@AfterMethod
	public void afterTest() throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		// Qtest.UpdateQTestResults(status, "US4441_Quote Creation", startTime,
		// endTime);
	}
}
