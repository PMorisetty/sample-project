package com.pure.test.dragon.regression.excessLiability;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.ChangeSummaryPageLib;
import com.pure.dragon.EndorsementModifiedPremiumPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PoliciesPageLib;
import com.pure.dragon.PolicyEndorsement;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.PolicyRenewalPage;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.enumSearchBy;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.report.TestResult;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC001_NewBusinessPolicy_Endo_Renew_Cancel_Reinst_EX extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	String sessionKey;
	
	@BeforeMethod
	public void beforeTest() {
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC001_NewBusinessPolicy_Endo_Renew_Cancel_Reinst_EX_Script", TestDataDragonRegressionEX,
				"TestDetails");
	}
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_NewBusinessPolicy_Endo_Renew_Cancel_Reinst_EX_Script", TestDataDragonRegressionEX,
				"LoginData");
	}
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC001_NewBusinessPolicy_Endo_Renew_Cancel_Reinst_EX_Script", TestDataDragonRegressionEX,
				"BasicInfo");
	}	
	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC001_NewBusinessPolicy_Endo_Renew_Cancel_Reinst_EX_Script", TestDataDragonRegressionEX,
				"QuoteDetails");
	}
	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC001_NewBusinessPolicy_Endo_Renew_Cancel_Reinst_EX_Script", TestDataDragonRegressionEX,
				"AgencyDetails");
	}
	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC001_NewBusinessPolicy_Endo_Renew_Cancel_Reinst_EX_Script", TestDataDragonRegressionEX,
				"DocumentDelivery");
	}
	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(				
				getTestDataFor_Login(),
				getTestDataFor_BasicInfo(),  
				getTestDataFor_DocumentDelivery(),getTestDataFor_QuoteDetails(),
				getTestDataFor_TestDetails(),getTestDataFor_AgencyDetails());
	}
	@Test(dataProvider = "getTestData")
	public void TC001_NewBusinessPolicy_Endo_Renew_Cancel_Reinst_EX_Script(Hashtable<String, String> data) throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
				sessionKey = "TC001_NB_" + data.get("state");
				data.put("SessionID",sessionKey);
			executeMode = true;
			/*********Policy Creation**************/
			String newPolicyNumber = new QuoteCreation(driver,reporter, testCyber).createQuote_ExcessLiability(data);
			System.out.println("Policy Number::::::::::::" + newPolicyNumber );
			/*********Endorsement_ExcessLiability**************/
			reporter.createHeader("Endorsement for state : "+data.get("state"));
			// policies tab navigation
			new HomeLib(driver, reporter).navigateToPolicies();
			new PoliciesPageLib(driver, reporter).searchPolicy(enumSearchBy.PolicyNumber, newPolicyNumber);
			new PoliciesPageLib(driver, reporter).clickSearchedPolicy(newPolicyNumber);
			PolicyEndorsementLib policyEndorsementLib = new PolicyEndorsementLib(driver, reporter);
			policyEndorsementLib.navigateToTransationOrEndorsementTab();
			policyEndorsementLib.clickNewBtn();
			policyEndorsementLib.policyEndorsement(data.get("chooseTransactionTypeEndorsement"), data.get("endorsementType"),
					data.get("endorsementEffectiveDate"), data.get("notes"));
			policyEndorsementLib.clickNextBtn();
			PolicyRenewalLib policyRenewalLib = new PolicyRenewalLib(driver, reporter);
			new CommonPageLib(driver, reporter).clickReviewChangesBtn();
			new ChangeSummaryPageLib(driver, reporter).clickRateBtn();
			new EndorsementModifiedPremiumPageLib(driver, reporter).clickIssueButton();
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			/*********Renewal_ExcessLiability**************/
			reporter.createHeader("Renewal for state : "+data.get("state"));
			PolicyRenewalLib renewal = new PolicyRenewalLib(driver, reporter);
			renewal.navigateToTransationOrEndorsementTab();
			renewal.clickCreateRenewalBtn();
			renewal.clickRenewalEntry();
			renewal.processRenewal(data, testCyber);
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			/*********Cancellation_ExcessLiability**************/
			reporter.createHeader("Cancellation for state : "+data.get("state"));
			// click on new button
			policyEndorsementLib.clickNewBtn();

			policyEndorsementLib.policyCancellationFirstStep(data.get("chooseTransactionTypeCancellation"), data.get("requestedBy"),
					data.get("newTransEffectiveDate"), data.get("notes"));
			// Click on next button
			policyEndorsementLib.clickNextBtn();

			if(isVisibleOnly(PolicyEndorsement.modifySequenceBtn, "Modify Out of Sequence Button")){
				policyEndorsementLib.clickModifyOutofSequenceBtn();
			}
			// enter the all the required data select cancellation method,reason.
			policyEndorsementLib.policyCancellationSecondStep(data.get("cancellationMethod"), data.get("cancellationreason"));
			policyEndorsementLib.clickProcessBtn();
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();

			/*********Reinstatement_ExcessLiability**************/
			reporter.createHeader("Reinstatement for state : "+data.get("state"));
			// click on new button
			policyEndorsementLib.clickNewBtn();

			// enter the all the required data select Reinstatement method,notes,effective date....
			policyEndorsementLib.policyReinstatementFirstStep(data.get("chooseTransactionTypeReInstatement"),data.get("newTransEffectiveDate"),
					data.get("description"),data.get("notesforReInstatement"));			// Click on next button
			policyEndorsementLib.clickNextBtn();
			// enter the all the required data select Reinstatement claim loss info,reason.
			policyEndorsementLib.policyReinstatementSecondStep(data.get("claimsLostPostCancellation"), data.get("reinstatementReason"));
			//Click on Process button
			policyEndorsementLib.clickProcessBtn();
			status = true;

		}
	}
	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result) throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
	}
}
