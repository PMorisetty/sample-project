package com.pure.test.dragon.regression.personalAuto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.json.simple.JSONObject;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.ChangeSummaryPageLib;
import com.pure.dragon.EndorsementModifiedPremiumPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PolicyEndorsement;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.PolicyRenewalPage;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC007_NewBusinessPolicy_Rewrite_PA extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	/*boolean testCyber = Boolean.parseBoolean(
	ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));*/
	boolean testCyber = false;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	String sessionKey;
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}
	
	@DataProvider
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.mergeData(getTestDataFor_BasicInfo(), TestUtil.getData("TC007", TestDataDragonRegression_PA, "PolicyDetails"));
	}
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.mergeData(TestUtil.getData_NewFormat(TestDataDragonRegression_PA, "Vehicles","TC007"),TestUtil.getData_NewFormat(TestDataDragonRegression_PA, "Drivers","TC007"));
	}

	@Test(dataProvider = "getTestDataFor_TestDetails")
	public void TC007_NewBusinessPolicy_Rewrite_PA_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			//initialize qtest object
			executeMode = true;
			/*********Policy Creation**************/
			reporter.createHeader("Policy Creation for state : "+data.get("state"));
			List<HashMap<String, String>> newPolicyNumber = new QuoteCreation(driver, reporter,testCyber).createQuote_PersonalAuto(data);
			System.out.println("Policy Number::::::::::::" + newPolicyNumber );
			/*********Rewrite_Homeowners**************/
			reporter.createHeader("Rewrite_Homeowners for state : "+data.get("state"));
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			//navigate to endorsement tab
			policyEndorsement.navigateToTransationOrEndorsementTab();
			
			// click on new button
			policyEndorsement.clickNewBtn();
			
			policyEndorsement.policyCancellationFirstStep(data.get("transactionTypeCancel"), data.get("RequestedBy"),
					data.get("NewTransactionEffectiveDate"), data.get("Notes"));
			// Click on next button
			policyEndorsement.clickNextBtn();
			
			if(isVisibleOnly(PolicyEndorsement.modifySequenceBtn, "Modify Out of Sequence Button")){
				policyEndorsement.clickModifyOutofSequenceBtn();
			}
			// enter the all the required data select cancellation method,reason.
			policyEndorsement.policyCancellationSecondStep(data.get("CancelMethod"), data.get("CancelReason"));
			policyEndorsement.clickProcessBtn();
			
			//click on exit transaction
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			
			// click on new button
			policyEndorsement.clickNewBtn();
			policyEndorsement.policyNewBusinessRewrite(data.get("transactionTypeRewrite"), data.get("NewTransactionEffectiveDateRewrite"), data.get("RewriteNotes"));
			
			// Click on next button
			policyEndorsement.clickNextBtn();
			policyEndorsement.clickNextBtn();
			
			CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
			//Click on review changes button
			commonPageLib.clickReviewChangesBtn();
			
			//Click on rate button
			new ChangeSummaryPageLib(driver, reporter).clickRateBtn();
			
			PolicyRenewalLib renewal = new PolicyRenewalLib(driver, reporter);
			if(isVisibleOnly(PolicyRenewalPage.reviewReferals, "Review Referals Button")){
				// click on review referrals button
				renewal.clickReviewReferalsBtn();
				// overriding the under writer notes			
				UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
				uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
				uwReferralsPageLib.fillOverridden();
				// Select Underwriter
				if("PROD".equalsIgnoreCase(region)){
					uwReferralsPageLib.selectUnderwriter(data.get("uwName_prod"));
				}else{
					uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
				}
				uwReferralsPageLib.clickApprovedButton();				
			}
			
			//Click on Complete rewrite transaction button
			new EndorsementModifiedPremiumPageLib(driver, reporter).clickCompleteRewriteTransaction();
		}
	}

	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result) throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
	}
}

