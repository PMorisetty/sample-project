package com.pure.test.dragon.regression.homeOwners;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.ChangeSummaryPageLib;
import com.pure.dragon.EndorsementModifiedPremiumPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PoliciesPageLib;
import com.pure.dragon.PolicyEndorsement;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.enumSearchBy;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.dragon.homeOwners.QuoteDetailsPageLib;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	/*boolean testCyber = Boolean.parseBoolean(
	ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));*/
	boolean testCyber = false;

	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	
	String sessionKey;
	
	@BeforeMethod(alwaysRun = true)
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"TestDetails");
	}
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"LoginData");
	}
	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"AgencyDetails");
	}
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"BasicInfo");
	}
	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"QuoteDetails");
	}
	private Object[][] getTestDataFor_OptionalCoverage() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"OptionalCoverage");
	}
	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"LocationDetails");
	}
	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"PreviousClaims");
	}
	private Object[][] getTestDataFor_AdditionalInterests() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"AdditionalInterests");
	}
	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"MemberInfo");
	}
	private Object[][] getTestDataFor_ManuscriptEndorsements() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"ManuscriptEndorsements");
	}
	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"UnderwriterRefferal");
	}
	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"DocumentDelivery");
	}
	private Object[][] getTestDataFor_Endorsement() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"Endorsement");
	}
	private Object[][] getTestDataFor_CancelPolicy() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"CancelPolicy");
	}
	private Object[][] getTestDataFor_ReinstatementPolicy() {
		return TestUtil.getData("TC006_NewBusinessPolicy_Endo_Renew_Cancel_HO_Script", TestDataDragonRegressionHO,
				"ReinstatementPolicy");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(),
				getTestDataFor_AgencyDetails(), getTestDataFor_BasicInfo(),
				getTestDataFor_QuoteDetails(), getTestDataFor_LocationDetails(),
				getTestDataFor_OptionalCoverage(), 
				getTestDataFor_PreviousClaims(), getTestDataFor_AdditionalInterests(),
				getTestDataFor_MemberInfo(), getTestDataFor_ManuscriptEndorsements(),
				getTestDataFor_UnderwriterRefferal(), getTestDataFor_Endorsement(),
				getTestDataFor_CancelPolicy(), getTestDataFor_ReinstatementPolicy(),
				getTestDataFor_DocumentDelivery(),getTestDataFor_TestDetails());
	}

	@Test(dataProvider = "getTestData")
	public void TC001_NewBusinessPolicy_HO_Homeowners_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			//initialize qtest object
			executeMode = true;
			/*********Policy Creation**************/
			reporter.createHeader("Policy Creation for state : "+data.get("state"));
			String newPolicyNumber = new QuoteCreation(driver, reporter,testCyber).createQuote(data);
			System.out.println("Policy Number::::::::::::" + newPolicyNumber );
			/*********Endorsement_Homeowners**************/
			reporter.createHeader("Endorsement_Homeowners for state : "+data.get("state"));
			// policies tab navigation
			new HomeLib(driver, reporter).navigateToPolicies();
			// search & select policy
			PoliciesPageLib policiespage = new PoliciesPageLib(driver, reporter);
			policiespage.searchPolicy(enumSearchBy.PolicyNumber,newPolicyNumber );
			//Select policy
			policiespage.clickSearchedPolicy(newPolicyNumber);
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			//navigate to endorsement tab
			policyEndorsement.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsement.clickNewBtn();
			
			policyEndorsement.policyEndorsement(data.get("chooseTransactionTypeEndorsement"), data.get("EndorsementType"),
												data.get("EndorsementEffectiveDate"), data.get("Notes"));
			policyEndorsement.clickNextBtn(); 	
			QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
			//quoteDetailsPageLib.setOtherStructureValue("200");
			new CommonPageLib(driver, reporter).clickReviewChangesBtn();
			new ChangeSummaryPageLib(driver, reporter).clickRateBtn();
			new EndorsementModifiedPremiumPageLib(driver, reporter).clickIssueButton();
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			/*********Renewal_Homeowners**************/
			reporter.createHeader("Renewal_Homeowners for state : "+data.get("state"));
			
			PolicyRenewalLib renewal = new PolicyRenewalLib(driver, reporter);
			renewal.navigateToTransationOrEndorsementTab();
			// click on create renewal button
			renewal.clickCreateRenewalBtn();
			// click on renewal entry
			renewal.clickRenewalEntry();			
			// Process the renewal
			renewal.processRenewal(data,testCyber);
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			/*********Cancellation_Homeowners**************/
			reporter.createHeader("Cancellation_Homeowners for state : "+data.get("state"));
			// click on new button
			policyEndorsement.clickNewBtn();
			
			policyEndorsement.policyCancellationFirstStep(data.get("Cancellation"), data.get("RequestedBy"),
					data.get("NewTransactionEffectiveDate"), data.get("Notes"));
			// Click on next button
			policyEndorsement.clickNextBtn();
			
			if(isVisibleOnly(PolicyEndorsement.modifySequenceBtn, "Modify Out of Sequence Button")){
				policyEndorsement.clickModifyOutofSequenceBtn();
			}
			// enter the all the required data select cancellation method,reason.
			policyEndorsement.policyCancellationSecondStep(data.get("CancelMethod"), data.get("CancelReason"));
			policyEndorsement.clickProcessBtn();
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			}
	}

	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result) throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
	}
}


