package com.pure.test.dragon.regression.excessLiability;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.ChangeSummaryPageLib;
import com.pure.dragon.EndorsementModifiedPremiumPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PoliciesPageLib;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.enumSearchBy;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.report.TestResult;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC002_NewBusinessPolicy_Rewrite_EX extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	String sessionKey;

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);

	}
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Rewrite_EX_Script", TestDataDragonRegressionEX,
				"TestDetails");
	}
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Rewrite_EX_Script", TestDataDragonRegressionEX,
				"LoginData");
	}
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Rewrite_EX_Script", TestDataDragonRegressionEX,
				"BasicInfo");
	}	
	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Rewrite_EX_Script", TestDataDragonRegressionEX,
				"QuoteDetails");
	}
	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Rewrite_EX_Script", TestDataDragonRegressionEX,
				"AgencyDetails");
	}
	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Rewrite_EX_Script", TestDataDragonRegressionEX,
				"DocumentDelivery");
	}
	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(				
				getTestDataFor_Login(),
				getTestDataFor_BasicInfo(),  
				getTestDataFor_DocumentDelivery(),getTestDataFor_QuoteDetails(),
				getTestDataFor_TestDetails(),getTestDataFor_AgencyDetails());
	}
	@Test(dataProvider = "getTestData")
	public void TC002_NewBusinessPolicy_Rewrite_EX_Script(Hashtable<String, String> data) throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			sessionKey = "TC002_Rewrite_EX_" + data.get("state");
			data.put("SessionID",sessionKey);
			executeMode = true;
			openApplication("UW");
			/*********Policy Creation**************/
			reporter.createHeader("Policy Creation for state : "+data.get("state"));
			String policyNumber = new QuoteCreation(driver,reporter, testCyber).createQuote_ExcessLiability(data);
			System.out.println("Policy Number::::::::::::" + policyNumber );
			/*********Rewrite_ExcessLiability**************/
			reporter.createHeader("Rewrite_ExcessLiability for state : "+data.get("state"));			

			PolicyEndorsementLib policyEndorsementLib = new PolicyEndorsementLib(driver, reporter);
			//navigate to endorsement tab
			policyEndorsementLib.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsementLib.clickNewBtn();
			policyEndorsementLib.policyCancellationFirstStep(data.get("chooseTransactionTypeCancellation"), data.get("requestedBy"),
					data.get("newTransEffectiveDate"), data.get("notes"));
			// Click on next button
			policyEndorsementLib.clickNextBtn();
			// enter the all the required data select cancellation method,reason.
			policyEndorsementLib.policyCancellationSecondStep(data.get("cancellationMethod"), data.get("cancellationreason"));
			policyEndorsementLib.clickProcessBtn();
			PolicyRenewalLib policyRenewalLib = new PolicyRenewalLib(driver, reporter);
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			policyEndorsementLib.clickNewBtn();
			policyEndorsementLib.policyNewBusinessRewrite(data.get("chooseTransactionTypeRewrite"), data.get("newTransactionEffectiveDateRewrite"),
					data.get("RewriteNotes"));
			policyEndorsementLib.clickNextBtn();
			policyEndorsementLib.clickNextBtn();
			new CommonPageLib(driver, reporter).clickReviewChangesBtn();
			new ChangeSummaryPageLib(driver, reporter).clickRateBtn();
			//Click on Complete rewrite transaction button
			new EndorsementModifiedPremiumPageLib(driver, reporter).clickCompleteReviewTransactionBtn();
			status = true;
		}
	}
	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result) throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
	}
}
