package com.pure.test.dragon.regression.homeOwners;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.ChangeSummaryPageLib;
import com.pure.dragon.EndorsementModifiedPremiumPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PolicyEndorsement;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.PolicyRenewalPage;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC008_NewBusinessPolicy_Rewrite_HO extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	/*boolean testCyber = Boolean.parseBoolean(
	ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));*/
	boolean testCyber = false;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	String sessionKey;

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"TestDetails");
	}
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"LoginData");
	}
	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"AgencyDetails");
	}
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"BasicInfo");
	}
	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"QuoteDetails");
	}
	private Object[][] getTestDataFor_OptionalCoverage() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"OptionalCoverage");
	}
	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"LocationDetails");
	}
	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"PreviousClaims");
	}
	private Object[][] getTestDataFor_AdditionalInterests() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"AdditionalInterests");
	}
	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"MemberInfo");
	}
	private Object[][] getTestDataFor_ManuscriptEndorsements() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"ManuscriptEndorsements");
	}
	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"UnderwriterRefferal");
	}
	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"DocumentDelivery");
	}
	private Object[][] getTestDataFor_CancelPolicy() {
		return TestUtil.getData("TC008_NewBusinessPolicy_Rewrite_HO_Script", TestDataDragonRegressionHO,
				"CancelPolicy");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(),
				getTestDataFor_AgencyDetails(), getTestDataFor_BasicInfo(),
				getTestDataFor_QuoteDetails(), getTestDataFor_LocationDetails(),
				getTestDataFor_OptionalCoverage(), 
				getTestDataFor_PreviousClaims(), getTestDataFor_AdditionalInterests(),
				getTestDataFor_MemberInfo(), getTestDataFor_ManuscriptEndorsements(),
				getTestDataFor_UnderwriterRefferal(), getTestDataFor_CancelPolicy(),
				getTestDataFor_DocumentDelivery(),getTestDataFor_TestDetails());
	}

	@Test(dataProvider = "getTestData")
	public void TC004_NewBusinessPolicy_Rewrite_HO_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			//initialize qtest object
			executeMode = true;
			/*********Policy Creation**************/
			reporter.createHeader("Policy Creation for state : "+data.get("state"));
			String newPolicyNumber = new QuoteCreation(driver, reporter,testCyber).createQuote(data);
			System.out.println("Policy Number::::::::::::" + newPolicyNumber );
			/*********Rewrite_Homeowners**************/
			reporter.createHeader("Rewrite_Homeowners for state : "+data.get("state"));
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			//navigate to endorsement tab
			policyEndorsement.navigateToTransationOrEndorsementTab();
			
			// click on new button
			policyEndorsement.clickNewBtn();
			
			policyEndorsement.policyCancellationFirstStep(data.get("Cancellation"), data.get("RequestedBy"),
					data.get("NewTransactionEffectiveDate"), data.get("Notes"));
			// Click on next button
			policyEndorsement.clickNextBtn();
			
			if(isVisibleOnly(PolicyEndorsement.modifySequenceBtn, "Modify Out of Sequence Button")){
				policyEndorsement.clickModifyOutofSequenceBtn();
			}
			// enter the all the required data select cancellation method,reason.
			policyEndorsement.policyCancellationSecondStep(data.get("CancelMethod"), data.get("CancelReason"));
			policyEndorsement.clickProcessBtn();
			
			//click on exit transaction
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			
			// click on new button
			policyEndorsement.clickNewBtn();
			policyEndorsement.policyNewBusinessRewrite(data.get("transactionTypeRewrite"), data.get("NewTransactionEffectiveDateRewrite"), data.get("RewriteNotes"));
			
			// Click on next button
			policyEndorsement.clickNextBtn();
			policyEndorsement.clickNextBtn();
			
			CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
			//Click on review changes button
			commonPageLib.clickReviewChangesBtn();
			
			//Click on rate button
			new ChangeSummaryPageLib(driver, reporter).clickRateBtn();
			
			PolicyRenewalLib renewal = new PolicyRenewalLib(driver, reporter);
			if(isVisibleOnly(PolicyRenewalPage.reviewReferals, "Review Referals Button")){
				// click on review referrals button
				renewal.clickReviewReferalsBtn();
				// overriding the under writer notes			
				UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
				uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
				uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
				
				uwReferralsPageLib.fillOverridden();
				uwReferralsPageLib.clickApprovedButton();
			}
			
			//Click on Complete rewrite transaction button
			new EndorsementModifiedPremiumPageLib(driver, reporter).clickCompleteRewriteTransaction();
		}
	}

	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result) throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
	}
}

