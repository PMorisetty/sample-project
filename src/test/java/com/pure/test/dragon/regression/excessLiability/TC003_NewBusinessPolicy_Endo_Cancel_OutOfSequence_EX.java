package com.pure.test.dragon.regression.excessLiability;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.ChangeSummaryPageLib;
import com.pure.dragon.EndorsementModifiedPremiumPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PoliciesPageLib;
import com.pure.dragon.PolicyEndorsement;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.enumSearchBy;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.report.TestResult;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC003_NewBusinessPolicy_Endo_Cancel_OutOfSequence_EX extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	String sessionKey;

	@BeforeMethod
	public void beforeTest() {
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}


	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC003_NewBusinessPolicy_Endo_Cancel_OutOfSequence_EX_Script", TestDataDragonRegressionEX,
				"TestDetails");
	}
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC003_NewBusinessPolicy_Endo_Cancel_OutOfSequence_EX_Script", TestDataDragonRegressionEX,
				"LoginData");
	}
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC003_NewBusinessPolicy_Endo_Cancel_OutOfSequence_EX_Script", TestDataDragonRegressionEX,
				"BasicInfo");
	}	
	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC003_NewBusinessPolicy_Endo_Cancel_OutOfSequence_EX_Script", TestDataDragonRegressionEX,
				"QuoteDetails");
	}
	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC003_NewBusinessPolicy_Endo_Cancel_OutOfSequence_EX_Script", TestDataDragonRegressionEX,
				"AgencyDetails");
	}
	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC003_NewBusinessPolicy_Endo_Cancel_OutOfSequence_EX_Script", TestDataDragonRegressionEX,
				"DocumentDelivery");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(				
				getTestDataFor_Login(),
				getTestDataFor_BasicInfo(),  
				getTestDataFor_DocumentDelivery(),getTestDataFor_QuoteDetails(),
				getTestDataFor_TestDetails(),getTestDataFor_AgencyDetails());
	}
	@Test(dataProvider = "getTestData")
	public void TC003_NewBusinessPolicy_Endo_Cancel_OutOfSequence_EX_Script(Hashtable<String, String> data) throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			sessionKey = "TC003_OOS_EX_" + data.get("state");
			data.put("SessionID",sessionKey);
			executeMode = true;
			openApplication("UW");

			/*********Policy Creation**************/
			String policyNumber = new QuoteCreation(driver,reporter, testCyber).createQuote_ExcessLiability(data);
			HomeLib homeLib = new HomeLib(driver, reporter);
			CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
			/*********Endorsement_Cancellation_OutOfSequence_ExcessLiability**************/
			homeLib.navigateToPolicies();
			new PoliciesPageLib(driver, reporter).searchPolicy(enumSearchBy.PolicyNumber, policyNumber);
			new PoliciesPageLib(driver, reporter).clickSearchedPolicy(policyNumber);
			PolicyEndorsementLib policyEndorsementLib = new PolicyEndorsementLib(driver, reporter);
			//navigate to endorsement tab
			policyEndorsementLib.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsementLib.clickNewBtn();
			policyEndorsementLib.policyEndorsement(data.get("chooseTransactionTypeEndorsement"), data.get("endorsementType"),
					data.get("endorsementEffectiveDate"), data.get("notes"));
			policyEndorsementLib.clickNextBtn();
			commonPageLib.clickReviewChangesBtn();
			new ChangeSummaryPageLib(driver, reporter).clickRateBtn();
			new EndorsementModifiedPremiumPageLib(driver, reporter).clickIssueButton();
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickOnPolicyLink();
			// click on new button
			policyEndorsementLib.clickNewBtn();
			policyEndorsementLib.policyCancellationFirstStep(data.get("chooseTransactionTypeCancellation"), data.get("requestedBy"),
					data.get("newTransEffectiveDate"), data.get("notes"));
			policyEndorsementLib.clickNextBtn();
			if(isVisibleOnly(PolicyEndorsement.modifySequenceBtn, "Modify Out of Sequence Button")){
				policyEndorsementLib.clickModifyOutofSequenceBtn();
			}
			// enter the all the required data select cancellation method,reason.
			policyEndorsementLib.policyCancellationSecondStep(data.get("cancellationMethod"), data.get("cancellationreason"));
			/*policyEndorsementLib.clickModifyOutofSequenceBtn();*/
			policyEndorsementLib.clickProcessBtn();
			status = true;
		}
	}
	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result) throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
	}
}
