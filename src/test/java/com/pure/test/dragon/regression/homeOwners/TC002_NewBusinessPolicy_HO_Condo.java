package com.pure.test.dragon.regression.homeOwners;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.HomeLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.report.TestResult;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC002_NewBusinessPolicy_HO_Condo extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	/*boolean testCyber = Boolean.parseBoolean(
	ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));*/
	boolean testCyber = false;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	String sessionKey;
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"TestDetails");
	}
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"LoginData");
	}
	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"AgencyDetails");
	}
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"BasicInfo");
	}
	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"QuoteDetails");
	}
	private Object[][] getTestDataFor_OptionalCoverage() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"OptionalCoverage");
	}
	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"LocationDetails");
	}
	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"PreviousClaims");
	}
	private Object[][] getTestDataFor_AdditionalInterests() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"AdditionalInterests");
	}
	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"MemberInfo");
	}
	private Object[][] getTestDataFor_ManuscriptEndorsements() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"ManuscriptEndorsements");
	}
	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"UnderwriterRefferal");
	}
	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC002_NewBusinessPolicy_HO_Condo_Script", TestDataDragonRegressionHO,
				"DocumentDelivery");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(),
				getTestDataFor_AgencyDetails(), getTestDataFor_BasicInfo(),
				getTestDataFor_QuoteDetails(), 
				getTestDataFor_LocationDetails(), getTestDataFor_OptionalCoverage(),
				getTestDataFor_PreviousClaims(), getTestDataFor_AdditionalInterests(),
				getTestDataFor_MemberInfo(), getTestDataFor_ManuscriptEndorsements(),
				getTestDataFor_UnderwriterRefferal(),
				getTestDataFor_DocumentDelivery(),getTestDataFor_TestDetails());
	}

	@Test(dataProvider = "getTestData")
	public void TC002_NewBusinessPolicy_HO_Condo_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			/*********Policy Creation**************/
			reporter.createHeader("Policy Creation for state : "+data.get("state"));
			String newPolicyNumber = new QuoteCreation(driver, reporter,testCyber).createQuote(data);
			System.out.println("Policy Number::::::::::::" + newPolicyNumber );
		}
	}

	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result) throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
	}
}

