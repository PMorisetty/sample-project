package com.pure.test.dragon.regression.collection;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.ChangeSummaryPageLib;
import com.pure.dragon.EndorsementModifiedPremiumPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PolicyEndorsement;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.report.TestResult;
import com.pure.test.dragon.collection.QuoteCreation_CO;
import com.pure.utilities.TestUtil;

public class TC002_NewBusinessPolicy_Endo_CO extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = false;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	String sessionKey;
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "TestDetails");
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "LoginData");
	}

	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "AgencyDetails");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "BasicInfo");
	}

	private Object[][] getTestDataFor_PolicyPage() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "PolicyPage");
	}

	private Object[][] getTestDataFor_CoverageByClass() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "CoverageByClass");
	}

	private Object[][] getTestDataFor_CoverageScheduled() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "CoverageScheduled");
	}

	private Object[][] getTestDataFor_ClaimsHistory() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "ClaimsHistory");
	}

	private Object[][] getTestDataFor_AdditionalBindingInfo() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "AdditionalBindingInfo");
	}

	private Object[][] getTestDataFor_AdditionalInsured() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "AdditionalInsured");
	}

	private Object[][] getTestDataFor_ManuscriptEndorsements() {
		return TestUtil
				.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "ManuscriptEndorsements");
	}

	private Object[][] getTestDataFor_Underwriter() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "Underwriter");
	}

	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "DocumentDelivery");
	}

	private Object[][] getTestDataFor_Endorsement() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "Endorsement");
	}
	
	private Object[][] getTestDataFor_CancelPolicy() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "CancelPolicy");
	}

	private Object[][] getTestDataFor_Reinstatement() {
		return TestUtil.getData("TC002_NewBusinessPolicy_Endo_CO", TestDataDragonRegression_CO, "Reinstatement");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(),getTestDataFor_TestDetails(),
				getTestDataFor_AgencyDetails(), getTestDataFor_BasicInfo(), getTestDataFor_PolicyPage(),
				getTestDataFor_CoverageByClass(), getTestDataFor_CoverageScheduled(), getTestDataFor_ClaimsHistory(),
				getTestDataFor_AdditionalBindingInfo(), getTestDataFor_AdditionalInsured(),
				getTestDataFor_ManuscriptEndorsements(), getTestDataFor_Underwriter(),
				getTestDataFor_DocumentDelivery(), getTestDataFor_Endorsement(), getTestDataFor_CancelPolicy(), 
				getTestDataFor_Reinstatement());
	}

	@Test(dataProvider = "getTestData")
	public void TC002_NewBusinessPolicy_Endo_CO_Script(Hashtable<String, String> data) throws Throwable {
		if ((data.get("execute") != null) && (data.get("execute").equalsIgnoreCase("Y"))) {
			//initialize qtest object			
			executeMode = true;
			
			/*********Policy Creation**************/
			reporter.createHeader("Policy Creation for state : "+data.get("state"));
			String newPolicyNumber = new QuoteCreation_CO(driver, reporter).createQuote(data);
			System.out.println("Policy Number::::::::::::" + newPolicyNumber );
			/*********Endorsement_Collections**************/
			/*reporter.createHeader("Endorsement_Collections for state : "+data.get("state"));
			PolicyEndorsementLib policyEndorsementLib = new PolicyEndorsementLib(driver, reporter);
			policyEndorsementLib.navigateToTransationOrEndorsementTab();
			policyEndorsementLib.clickNewBtn();
			policyEndorsementLib.policyEndorsement(data.get("ChooseTransactionType_Endo"),
													data.get("EndorsementType"),
														data.get("EndorsementEffectiveDate"),
															data.get("Notes_Endo"));
			policyEndorsementLib.clickNextBtn();
			new CommonPageLib(driver, reporter).clickReviewChangesBtn();
			new ChangeSummaryPageLib(driver, reporter).clickRateBtn();
			new EndorsementModifiedPremiumPageLib(driver, reporter).clickIssueButton();
			//Loook in to below action, complete it.
			//new PolicyCurrentSummaryPageLib(driver, reporter).downloadVerifyEndorsementPolicy();
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();*/
		}
	}

	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result) throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		//		Qtest.UpdateQTestResults(status, "US4441_Cancel and Reinstate", startTime, endTime);
	}

}
