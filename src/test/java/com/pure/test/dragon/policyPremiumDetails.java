 package com.pure.test.dragon;

public class policyPremiumDetails implements Comparable<policyPremiumDetails> {

	String ROWnum;
	String PolicyNumber;
	String TotalPremiumValue;
	String SurplusContributionValue;
	String GrandTotalvalue;
	String BaseRateValue, BaseRateHurricaneValue, BaseRateNonHurricaneValue, creditScoreFactor, AgeOfHomeFactor,
			AOPDeductibleFactor;
	String OptionalCoveragePremium, LocationPremiumValue,
			TotalLocationPremiumValue;
	String AdditionalRatePerThousand,LiabilityPremium,LiabilityExtensionRate,LiabilityExtensionNoOfPremises,FinalPartialLimitsFactorDwelling,
	FinalPartialLimitsFactorContents,territoryZone;
	
	String TierFactorV2BasePremium, TierFactorV2WindPremium;

	public String getRownum() {	return this.ROWnum;	}
	public void setRownum(String row) {	this.ROWnum = row; }
	
	public String getTerritoryZone() { return this.territoryZone; }
	public void setTerritoryZone(String territoryZone) {this.territoryZone=territoryZone;}
	
	public void setLiabilityPremium(String LiabilityPremium) { this.LiabilityPremium = LiabilityPremium; }
	public String getLiabilityPremium() { return this.LiabilityPremium;	}
	
	public void setLiabilityExtensionRate(String LiabilityExtensionRate) { this.LiabilityExtensionRate = LiabilityExtensionRate; }
	public String getLiabilityExtensionRate() {	return this.LiabilityExtensionRate;	}
	
	public void setLiabilityExtensionNoOfPremises(String LiabilityExtensionNoOfPremises) { this.LiabilityExtensionNoOfPremises = LiabilityExtensionNoOfPremises;}
	public String getLiabilityExtensionNoOfPremises() {	return this.LiabilityExtensionNoOfPremises;	}
	
	public void setFinalPartialLimitsFactorDwelling(String FinalPartialLimitsFactorDwelling) { this.FinalPartialLimitsFactorDwelling = FinalPartialLimitsFactorDwelling;}
	public String getFinalPartialLimitsFactorDwelling() { return this.FinalPartialLimitsFactorDwelling;	}
	
	public void setFinalPartialLimitsFactorContents(String FinalPartialLimitsFactorContents) { this.FinalPartialLimitsFactorContents = FinalPartialLimitsFactorContents;}
	
	public String getTierFactorV2BasePremium(){return this.TierFactorV2BasePremium;};
	public void setTierFactorV2BasePremium(String tierFactorV2BasePremium){this.TierFactorV2BasePremium=tierFactorV2BasePremium;};
	
	public String getTierFactorV2WindPremium(){return this.TierFactorV2WindPremium;};
	public void setTierFactorV2WindPremium(String tierFactorV2WindPremium){this.TierFactorV2WindPremium=tierFactorV2WindPremium;};
	
	
	public String getFinalPartialLimitsFactorContents() { return this.FinalPartialLimitsFactorContents;	}
	
	public String getPolicyNumber() { return this.PolicyNumber;	};
	public void setPolicyNumber(String pNum) {this.PolicyNumber = pNum;	}

	public String getBaseRateValue() { return this.BaseRateValue; };
	public void setBaseRateValue(String baseRate) {	this.BaseRateValue = baseRate;	}
	
	public String getBaseRateHurricaneValue() {	return this.BaseRateHurricaneValue;	};
	public void setBaseRateHurricaneValue(String baseRate) { this.BaseRateHurricaneValue = baseRate; }
	
	public String getBaseRateNonHurricaneValue() { return this.BaseRateNonHurricaneValue; };
	public void setBaseRateNonHurricaneValue(String baseRate) {	this.BaseRateNonHurricaneValue = baseRate;	}

	public String getcreditScoreFactor() {	return this.creditScoreFactor;	};
	public void setcreditScoreFactor(String creditscorefactor) {this.creditScoreFactor = creditscorefactor;	}

	public String getAgeOfHomeFactor() {return this.AgeOfHomeFactor;};
	public void setAgeOfHomeFactor(String ageofhomefactor) { this.AgeOfHomeFactor = ageofhomefactor; }

	public String getAOPDeductibleFactor() { return this.AOPDeductibleFactor;};
	public void setAOPDeductibleFactor(String AOPfactor) { this.AOPDeductibleFactor = AOPfactor; }

	public String getOptionalCoveragePremium() {return this.OptionalCoveragePremium;};
	public void setOptionalCoveragePremium(String optionalCoveragePremium) {this.OptionalCoveragePremium = optionalCoveragePremium;	}


	public String getTotalPremiumValue() {return this.TotalPremiumValue;};
	public void setTotalPremiumValue(String totalPrem) {this.TotalPremiumValue = totalPrem;	};

	public String getLocationPremiumValue() {return this.LocationPremiumValue;	};
	public void setLocationPremiumValue(String locationPremiumValue) {this.LocationPremiumValue = locationPremiumValue;	};

	public String getTotalLocationPremiumValue() {	return this.TotalLocationPremiumValue;	};
	public void setTotalLocationPremiumValue(String totalLocationPremiumValue) {this.TotalLocationPremiumValue = totalLocationPremiumValue;	};

	public String getSurplusContributionValue() {return this.SurplusContributionValue;};
	public void setSurplusContributionValue(String surplus) {this.SurplusContributionValue = surplus;};

	public String getGrandTotalvalue() {return this.GrandTotalvalue;};
	public void setGrandTotalvalue(String grand) {this.GrandTotalvalue = grand;	};

	public String getAdditionalRatePerThousand() {return this.AdditionalRatePerThousand;};
	public void setAdditionalRatePerThousand(String additionalRatePerThousand) {this.AdditionalRatePerThousand = additionalRatePerThousand;	};

	@Override
	public int compareTo(policyPremiumDetails obj) {
		if (Integer.valueOf(this.ROWnum) > Integer.valueOf(obj.getRownum())) {
				return 1;
			}else if(Integer.valueOf(this.ROWnum) < Integer.valueOf(obj.getRownum())){
				return -1;
		} else {
			return 0;
		}
	}
}
