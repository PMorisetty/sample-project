package com.pure.test.dragon.smoke;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.HomeLib;
import com.pure.utilities.TestUtil;

public class DragonLogin extends ActionEngine{

	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean loginChecked =false;
	/*boolean testCyber = Boolean.parseBoolean(
		ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));*/
	boolean testCyber = false;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	JSONObject jsCycleInfo = null;
	String sessionKey;


	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"LoginData");
	}
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"BasicInfo");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(),getTestDataFor_BasicInfo());
	}	

	@Test(dataProvider = "getTestData")
	public void DragonLogin_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y") && !loginChecked)){
			HomeLib homeLib = new HomeLib(driver, reporter);
			if(("PROD").equalsIgnoreCase(region)){
				openApplication("UW");
				homeLib.loginOKTA(data.get("username_prod"), data.get("password_prod"));
				System.out.println("Successfully Logged into OKTA");
			}else{
				openApplication("UW");
				homeLib.login(data.get("username"), data.get("password"));
				System.out.println("Successfully Logged into "+ region);
			}
			loginChecked =true;
		}
	}
}


