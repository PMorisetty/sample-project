package com.pure.test.dragon.smoke;


import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.accelerators.TestEngineWeb;
import com.pure.dragon.ChangeSummaryPageLib;
import com.pure.dragon.EndorsementModifiedPremiumPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.dragon.homeOwners.QuoteDetailsPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.report.TestResult;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC001_HO extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	/*boolean testCyber = Boolean.parseBoolean(
	ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));*/
	boolean testCyber = false;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	String sessionKey;


	@BeforeMethod(alwaysRun = true)
	public void beforeTest() {
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"LoginData");
	}

	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"AgencyDetails");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"BasicInfo");
	}

	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"QuoteDetails");
	}

	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"LocationDetails");
	}

	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"PreviousClaims");
	}

	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"MemberInfo");
	}

	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"UnderwriterRefferal");
	}

	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"DocumentDelivery");
	}

	private Object[][] getTestDataFor_Endorsement() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke, "Endorsement");
	}
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"TestDetails");
	}
	private Object[][] getTestDataFor_ManuScriptEndorsement() {
		return TestUtil.getData("TC001_HO_Script", TestDataDragonSmoke,
				"ManuScript Endorsement");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(),
				getTestDataFor_AgencyDetails(), getTestDataFor_BasicInfo(),
				getTestDataFor_QuoteDetails(),
				getTestDataFor_LocationDetails(),
				getTestDataFor_PreviousClaims(), getTestDataFor_MemberInfo(),
				getTestDataFor_UnderwriterRefferal(),
				getTestDataFor_DocumentDelivery(),getTestDataFor_TestDetails(),
				getTestDataFor_Endorsement(),
				getTestDataFor_ManuScriptEndorsement());
	}

	@Test(dataProvider = "getTestData")
	public void TC001_HO_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){			
			sessionKey = "TC001_HO_" + data.get("state");
			data.put("SessionID",sessionKey);
			executeMode = true;
			String newPolicyNumber = new QuoteCreation(driver, reporter,testCyber).createQuote(data);
			System.out.println("Policy Number::::::::::::" + newPolicyNumber);

			//navigate to endorsement tab
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			policyEndorsement.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsement.clickNewBtn();
			// enter the all the required data select cancellation,cancelled
			// by,effective date,notes.
			policyEndorsement.policyEndorsement(data.get("chooseTransactionTypeEndorsement"), data.get("EndorsementType"),
					data.get("EndorsementEffectiveDate"), data.get("Notes"));
			policyEndorsement.clickNextBtn();
			QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
			quoteDetailsPageLib.setOtherStructureValue("200");
			new CommonPageLib(driver, reporter).clickReviewChangesBtn();
			new ChangeSummaryPageLib(driver, reporter).clickRateBtn();
			new EndorsementModifiedPremiumPageLib(driver, reporter).clickIssueButton();
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			// policy Renewal process started
			// navigate to endorsement tab
			PolicyRenewalLib renewal = new PolicyRenewalLib(driver, reporter);
			renewal.navigateToTransationOrEndorsementTab();
			// click on create renewal button
			renewal.clickCreateRenewalBtn();
			// click on renewal entry
			renewal.clickRenewalEntry();
			if(!"PROD".equalsIgnoreCase(region)){
				// Process the renewal
				renewal.processRenewal(data, testCyber);
				//Exit transaction			
				new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			}
			status = true;
		}
	}

	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result) throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
	}
}
