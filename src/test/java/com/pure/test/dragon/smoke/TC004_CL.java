package com.pure.test.dragon.smoke;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.apache.commons.lang3.text.StrTokenizer;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.accelerators.TestEngineWeb;
import com.pure.dragon.BillingPageLib;
import com.pure.dragon.CustomerSummaryPageLib;
import com.pure.dragon.FinalSummaryPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PolicyCurrentSummaryPageLib;
import com.pure.dragon.PolicyDeliveryPageLib;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.PremiumPageLib;
import com.pure.dragon.QuotesLib;
import com.pure.dragon.SubjectivitiesPageLib;
import com.pure.dragon.SummaryPage;
import com.pure.dragon.SummaryPageLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.report.TestResult;
import com.pure.utilities.TestUtil;

public class TC004_CL extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	/*boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));*/
	boolean testCyber = false;

	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	String sessionKey;

	@BeforeMethod(alwaysRun = true)
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC004_CL_Script", TestDataDragonSmoke,
				"TestDetails");
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC004_CL_Script", TestDataDragonSmoke,
				"LoginData");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC004_CL_Script", TestDataDragonSmoke,
				"BasicInfo");
	}	

	private Object[][] getTestDataFor_CopyQuote() {
		return TestUtil.getData("TC004_CL_Script", TestDataDragonSmoke,
				"CopyQuote");
	}

	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC004_CL_Script", TestDataDragonSmoke,
				"DocumentDelivery");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(				
				getTestDataFor_Login(),
				getTestDataFor_BasicInfo(),
				getTestDataFor_CopyQuote(),
				getTestDataFor_DocumentDelivery(),
				getTestDataFor_TestDetails());
	}

	@Test(dataProvider = "getTestData")

	public void TC004_CL_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			sessionKey = "TC004_CL_" + data.get("state");
			data.put("SessionID",sessionKey);
			executeMode = true;
			openApplication("UW");		

			HomeLib homeLib = new HomeLib(driver, reporter);
			if(("PROD").equalsIgnoreCase(region)){
				//homeLib.loginOKTA("Pureonlineautomation1@mailinator.com", "Pr0dt3sting");
				homeLib.loginOKTA(data.get("username_prod"), data.get("password_prod"));
			}else{
				homeLib.login(data.get("username"), data.get("password"));
			}
			// openApplicationViaTiles();
			homeLib.navigateToQuotes();	
			//Create a copy of existing Quote
			QuotesLib quotesLib = new QuotesLib(driver, reporter);
	        quotesLib.uncheck90DaysQuotes();
			String newQuoteName = data.get("newcopyquotename")+startTime;
			if(("PROD").equalsIgnoreCase(region)){
				quotesLib.createCopyQuote(data.get("quoteName_prod"), newQuoteName);
			}else if(("QA").equalsIgnoreCase(region)){
				quotesLib.createCopyQuote(data.get("quoteName_QA"), newQuoteName);
			}else if(("STG").equalsIgnoreCase(region)){
				quotesLib.createCopyQuote(data.get("quoteName_STG"), newQuoteName);
			}

			CustomerSummaryPageLib customerSummaryPageLib = new CustomerSummaryPageLib(driver, reporter);
			customerSummaryPageLib.selectQuote(newQuoteName);
			SummaryPageLib summaryPageLib = new SummaryPageLib(driver, reporter);
			summaryPageLib.clickCompleteQuote();
			new CommonPageLib(driver, reporter).clickRateBtn();

			if(!isVisibleOnly(SummaryPage.requestIssueButton, "Request Issue Button")){

				// overriding the under writer notes			
				UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
				uwReferralsPageLib.clickUnderwriterReferralsTab();
				uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
				uwReferralsPageLib.fillOverridden();
				//if(data.get("state").equalsIgnoreCase("CA")){
					uwReferralsPageLib.selectUnderwriter();
				
				// Accept the refferal
				uwReferralsPageLib.clickAcceptButton();
			}

			summaryPageLib.clickRequestIssueButton();

			new PremiumPageLib(driver, reporter).clickBindButton();
			//Select document delivery type
			PolicyDeliveryPageLib policyDeliveryPageLib = new PolicyDeliveryPageLib(driver, reporter);
			policyDeliveryPageLib.selectDocumentDeliveryType(data);
			policyDeliveryPageLib.clickNextButton();
			// Select bill delivery
			BillingPageLib billingPageLib = new BillingPageLib(driver, reporter);
			billingPageLib.selectBillingAddress("SendToBilling");
			// click confirm button
			billingPageLib.clickConfirmBtn();
			// Request Bind
			FinalSummaryPageLib finalSummaryPageLib = new FinalSummaryPageLib(driver, reporter);
			finalSummaryPageLib.clickRequestBind();

			//navigate to endorsement tab
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			policyEndorsement.navigateToTransationOrEndorsementTab();
			policyEndorsement.navigateToNBtransaction();
			policyEndorsement.navigateToSubjectivities();
			// override subjectivities
			SubjectivitiesPageLib subjectivitiesPageLib = new SubjectivitiesPageLib(driver, reporter);
			subjectivitiesPageLib.overrideValues();
			// Manually bind the policy
			subjectivitiesPageLib.clickManualBindButton();
			String newPolicyNumber = new PolicyCurrentSummaryPageLib(driver, reporter).getPolicyNumber();
			reporter.SuccessReport("Policy Number: ", newPolicyNumber);
			// navigate to endorsement tab
			PolicyRenewalLib renewal = new PolicyRenewalLib(driver, reporter);
			renewal.navigateToTransationOrEndorsementTab();
			// click on create renewal button
			renewal.clickCreateRenewalBtn();
			// click on renewal entry
			renewal.clickRenewalEntry();
			// navigate to policy image tab
			renewal.clickPolicyImageTab();
			//change limit to 100,000 and rate and verify on premium tab
			renewal.clickReviewChangesBtn();
			// click on renewed button
			renewal.clickRenewedPremiumBtn();
			if(!"PROD".equalsIgnoreCase(region)){
				renewal.processRenewal(data, testCyber);
				//Exit transaction
				new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			}
			status = true;
		}



	}

	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result) throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}		
	}
}

