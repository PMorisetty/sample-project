package com.pure.test.dragon.smoke;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.accelerators.TestEngineWeb;
import com.pure.dragon.ChangeSummaryPageLib;
import com.pure.dragon.EndorsementModifiedPremiumPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.InsuranceScoreManagementLib;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.PolicyRenewalPage;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.UnderwriterAlertsPageLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.dragon.homeOwners.QuoteDetailsPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.report.TestResult;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC002_PA extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	/*boolean testCyber = Boolean.parseBoolean(
	ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));*/
	boolean testCyber = false;

	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	String sessionKey;

	@BeforeMethod(alwaysRun = true)
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_TestDetails(), counter);
	}

	@DataProvider
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.mergeData(getTestDataFor_BasicInfo(), TestUtil.getData("TC001", TestDataDragonSmokePA, "PolicyDetails"));
	}
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.mergeData(TestUtil.getData_NewFormat(TestDataDragonSmokePA, "Vehicles","TC001"),
				TestUtil.getData_NewFormat(TestDataDragonSmokePA, "Drivers","TC001"));
	}
	@Test(dataProvider = "getTestDataFor_TestDetails")
	public void TC002_PA_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			sessionKey = "TC002_PA_" + data.get("state");
			data.put("SessionID",sessionKey);
			executeMode = true;
			List<HashMap<String, String>> policyNumber = new QuoteCreation(driver, reporter,testCyber).createQuote_PersonalAuto(data);
			System.out.println("Quote Number is :" +policyNumber);	
			//navigate to endorsement tab
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			policyEndorsement.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsement.clickNewBtn();
			// enter the all the required data select cancellation,cancelled
			// by,effective date,notes.
			policyEndorsement.policyEndorsement(data.get("chooseTransactionTypeEndorsement"), data.get("EndorsementType"),
					data.get("EndorsementEffectiveDate"), data.get("Notes"));
			policyEndorsement.clickNextBtn();

			/*QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
			quoteDetailsPageLib.setOtherStructureValue("200");*/
			new CommonPageLib(driver, reporter).clickReviewChangesBtn();
			new ChangeSummaryPageLib(driver, reporter).clickRateBtn();
			PolicyRenewalLib renewal = new PolicyRenewalLib(driver, reporter);
			if(isVisibleOnly(PolicyRenewalPage.reviewReferals, "Review Referals Button")){
				// click on review referrals button
				renewal.clickReviewReferalsBtn();
				// overriding the under writer notes			
				UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
				uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
				uwReferralsPageLib.fillOverridden();
				if("PROD".equalsIgnoreCase(region)){
					uwReferralsPageLib.selectUnderwriter(data.get("uwName_prod"));
				}else{
					uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
				}
				// Approve the referral
				uwReferralsPageLib.clickApprovedButton();
			}

			new EndorsementModifiedPremiumPageLib(driver, reporter).clickIssueButton();
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			// policy Renewal process started
			// navigate to endorsement tab
			renewal.navigateToTransationOrEndorsementTab();
			// click on create renewal button
			if(!"PROD".equalsIgnoreCase(region)){
				renewal.clickCreateRenewalBtn();
				// click on renewal entry
				renewal.clickRenewalEntry();		
				// Process the renewal
				renewal.processRenewal(data, testCyber);
				//Exit transaction			
				new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			}
		}
	}

	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result) throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
	}
}
