package com.pure.test.dragon;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.support.MyListener;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC001_HO_PolicyCreation extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;	
	/*boolean testCyber = Boolean.parseBoolean(
		ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));*/
	boolean testCyber = false;
	ConcurrentHashMap<String, String> policyDetails = new ConcurrentHashMap<String, String>();

	/*public String ROWnum="";
	String newPolicyNumber="";

	public double TotalPremiumValue =0;
	public double SurplusContributionValue =0;
	public double GrandTotalvalue =0;
	 */

	XSSFWorkbook wb;
	Sheet sheet;
	List<policyPremiumDetails> listOfPolicies = new ArrayList<policyPremiumDetails>();

	@BeforeClass
	public void createPolicySheet() throws Exception{

		wb = new XSSFWorkbook();
		sheet = wb.createSheet("kk");
		Row row = sheet.createRow(0);
		row.createCell(0).setCellValue("ROWnum");
		row.createCell(1).setCellValue("PolicyNumber");
		row.createCell(2).setCellValue("BaseRateValue");
		row.createCell(3).setCellValue("CreditScoreFactor");
		row.createCell(4).setCellValue("AgeOfHomeFactor");
		row.createCell(5).setCellValue("AOPDeductibleFactor");
		row.createCell(6).setCellValue("OptionalCoveragePremium");
		row.createCell(7).setCellValue("TotalPremiumValue");
		row.createCell(8).setCellValue("SurplusContributionValue");
		row.createCell(9).setCellValue("GrandTotalvalue");		

	}

	@BeforeMethod
	public void beforeTest() throws Exception {
		startTime = new Date();
		//reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);


	}


	@DataProvider(parallel=true)
	private Object[][] getTestDataFor_VA_HO_Inforce() {
		return TestUtil.getData("TC001_Cyber_CreateQuote", TestDataDragonVAHO,
				"input");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException {
		// this.reporter.initTestCaseDescription("");
		return TestUtil.getAllData(getTestDataFor_VA_HO_Inforce());
	}

	@Test(dataProvider = "getTestDataFor_VA_HO_Inforce")
	public void TC001_HO_CreateQuote_Script(Hashtable<String, String> data)
			throws Throwable {
		WebDriver tempWebDriver = null;
		EventFiringWebDriver tempDriver = null;

		try {
			DesiredCapabilities capab = null;
			String downloadFilepath = System.getProperty("user.dir") + "\\Downloads";
			String driversPath = System.getProperty("user.dir") + "\\Drivers\\";

			HashMap<String, Object> chromePrefs = new HashMap<>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.default_directory", downloadFilepath);

			capab = DesiredCapabilities.chrome();
			System.setProperty("webdriver.chrome.driver", driversPath + "chromedriver_32Bit.exe");
			ChromeOptions options = new ChromeOptions();

			options.setExperimentalOption("prefs", chromePrefs);
			capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			options.addArguments("--start-maximized");
			options.addArguments("test-type");
			options.addArguments("chrome.switches", "--disable-extensions");
			capab.setCapability(ChromeOptions.CAPABILITY, options);
			if (seleniumgridurl.equalsIgnoreCase("local")) {
				tempWebDriver = new ChromeDriver(capab);
				tempDriver = new EventFiringWebDriver(tempWebDriver);
			}else{			
				capab.setCapability("uuid", universalID);
				LOG.info("Assigning remote webdriver instance to this.WebDriver");
				tempWebDriver = new Augmenter().augment(new RemoteWebDriver(new URL(seleniumgridurl), capab));
				LOG.info("this.WebDriver :::" + this.WebDriver.toString());
				((RemoteWebDriver)tempWebDriver).setFileDetector(new LocalFileDetector());
				LOG.info("Assigned remote webdriver instance to this.tempWebDriver");
				tempDriver = new EventFiringWebDriver(tempWebDriver);
				MyListener myListener = new MyListener();
				tempDriver.register(myListener);
			}

			LOG.info("Driver launch :: " + browser);		
			LOG.info("Assigned event firing webdriver instance to driver");
			LOG.info("eventfiring driver ::: " + tempDriver.toString());

			if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
				executeMode = true;				
				QuoteCreation qq= new QuoteCreation(tempDriver, reporter, testCyber);
							
				policyPremiumDetails tempPolicy = new policyPremiumDetails();
				tempPolicy.setRownum(data.get("Row"));
				tempPolicy.setPolicyNumber(qq.createQuote(data));				
				tempPolicy.setBaseRateValue(qq.BaseRateValue);				
				tempPolicy.setcreditScoreFactor(qq.CreditScoreFactor);
				tempPolicy.setAgeOfHomeFactor(qq.AgeOfHomeFactor);
				tempPolicy.setAOPDeductibleFactor(qq.AOPDeductibleFactor);
				tempPolicy.setOptionalCoveragePremium(Double.toString(qq.OptionalCoveragePremium));				
				tempPolicy.setGrandTotalvalue(Double.toString(qq.GrandTotalvalue));
				tempPolicy.setSurplusContributionValue(Double.toString(qq.SurplusContributionValue));
				tempPolicy.setTotalPremiumValue(Double.toString(qq.TotalPremiumValue));
				listOfPolicies.add(tempPolicy);
										
			}
		} catch (Exception e) {
			throw e;

		} finally{
			if(tempDriver !=null){
				tempDriver.quit();
			}
		}


	}



	@AfterClass
	public void writetoExcel() throws Exception{	
		Collections.sort(listOfPolicies);
		for(int i = 0; i< listOfPolicies.size(); i++){
			policyPremiumDetails temp = listOfPolicies.get(i);
			int rowIndex = i+1;
			Row currentRow = sheet.createRow(rowIndex);
			currentRow.createCell(0).setCellValue(temp.getRownum());
			if(temp.getPolicyNumber()!=""){
				currentRow.createCell(1).setCellValue(temp.getPolicyNumber());				
				currentRow.createCell(2).setCellValue(temp.getBaseRateValue());
				currentRow.createCell(3).setCellValue(temp.getcreditScoreFactor());
				currentRow.createCell(4).setCellValue(temp.getAgeOfHomeFactor());
				currentRow.createCell(5).setCellValue(temp.getAOPDeductibleFactor());
				currentRow.createCell(6).setCellValue(temp.getOptionalCoveragePremium());									
				currentRow.createCell(7).setCellValue(temp.getTotalPremiumValue());
				currentRow.createCell(8).setCellValue(temp.getSurplusContributionValue());
				currentRow.createCell(9).setCellValue(temp.getGrandTotalvalue());
			}
		}
		File HOExcel = new File("VA-HOPolicies.xlsx");
		if(HOExcel.exists()){
			HOExcel.delete();	
		}
		FileOutputStream fileOut = new FileOutputStream("VA-HOPolicies.xlsx");		
		wb.write(fileOut);

		fileOut.close();
	}


}



