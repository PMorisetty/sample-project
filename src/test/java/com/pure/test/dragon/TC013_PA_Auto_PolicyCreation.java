package com.pure.test.dragon;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.support.MyListener;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC013_PA_Auto_PolicyCreation extends ActionEngine {

	boolean status = false;
	int counter = 0;
	boolean executeMode = false;	
	/*boolean testCyber = Boolean.parseBoolean(
		ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));*/
	boolean testCyber = false;
	ConcurrentHashMap<String, String> policyDetails = new ConcurrentHashMap<String, String>();

	XSSFWorkbook wb;
	Sheet sheet;
	List<Object> listOfPolicies = new ArrayList<Object>();

	@BeforeClass
	public void createPolicySheet() throws Exception{
		wb = new XSSFWorkbook();
		sheet = wb.createSheet("PA-Auto Policies");
		Row row = sheet.createRow(0);
		row.createCell(0).setCellValue("S.No.");
		row.createCell(1).setCellValue("Original Policy Number");
		row.createCell(2).setCellValue("Policy Number");
		row.createCell(3).setCellValue("TotalPremiumValue");
		row.createCell(4).setCellValue("SurplusContributionValue");
		row.createCell(5).setCellValue("GrandTotalvalue");
		row.createCell(6).setCellValue("VehicleName");
		row.createCell(7).setCellValue("LiabilityCSL");
		row.createCell(8).setCellValue("PIP");
		row.createCell(9).setCellValue("MedicalPayments");		
		row.createCell(10).setCellValue("UnderinsuredMotorist(CSL)");
		row.createCell(11).setCellValue("UnderinsuredMotoristPD(CSL)");
		row.createCell(12).setCellValue("Comprehensive");
		row.createCell(13).setCellValue("Collision");
		row.createCell(14).setCellValue("TowingAndLabor");
		row.createCell(15).setCellValue("VehiclePremium");
		row.createCell(16).setCellValue("BodilyInjury");
		row.createCell(17).setCellValue("PropertyDamage");
		row.createCell(18).setCellValue("UnderinsuredMotoristBI");
		row.createCell(19).setCellValue("UnderinsuredMotoristPD");
		row.createCell(20).setCellValue("BodilyInjuryRate");
		row.createCell(21).setCellValue("PropertyDamageRate");
		row.createCell(22).setCellValue("PIPRate");
		row.createCell(23).setCellValue("MedicalPaymentsRate");
		row.createCell(24).setCellValue("UnderinsuredMotorist(CSL)Rate");
		row.createCell(25).setCellValue("UnderinsuredMotoristPD(CSL)Rate");
		row.createCell(26).setCellValue("ComprehensiveRate");
		row.createCell(27).setCellValue("CollisionRate");
		row.createCell(28).setCellValue("UnderinsuredMotoristBIRate");
		row.createCell(29).setCellValue("UnderinsuredMotoristPDRate");
	}

	@DataProvider(parallel = true)
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.mergeData(getTestDataFor_PolicyDetails(), TestUtil.getData("TC001", TestDataDragonPAAuto, "PolicyDetails"));
	}
	private Object[][] getTestDataFor_PolicyDetails() {
		return TestUtil.mergeData(getTestDataFor_BasicInfo(), TestUtil.getData("Included in PA",TestDataDragonPAAuto, "Policy"));
	}
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.mergeData(TestUtil.getData_NewFormat(TestDataDragonPAAuto, "Vehicles"),TestUtil.getData_NewFormat(TestDataDragonPAAuto, "Drivers"));
	}

	@Test(dataProvider = "getTestDataFor_TestDetails")
	public void TC013_PA_Auto_PolicyCreation_Script(Hashtable<String, String> data)
			throws Throwable {
		WebDriver tempWebDriver = null;
		EventFiringWebDriver tempDriver = null;
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			try {
				DesiredCapabilities capab = null;
				String downloadFilepath = System.getProperty("user.dir") + "\\Downloads";
				String driversPath = System.getProperty("user.dir") + "\\Drivers\\";

				HashMap<String, Object> chromePrefs = new HashMap<>();
				chromePrefs.put("profile.default_content_settings.popups", 0);
				chromePrefs.put("download.default_directory", downloadFilepath);

				capab = DesiredCapabilities.chrome();
				System.setProperty("webdriver.chrome.driver", driversPath + "chromedriver_32Bit.exe");
				ChromeOptions options = new ChromeOptions();

				options.setExperimentalOption("prefs", chromePrefs);
				capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				options.addArguments("--start-maximized");
				options.addArguments("test-type");
				options.addArguments("chrome.switches", "--disable-extensions");
				capab.setCapability(ChromeOptions.CAPABILITY, options);
				if (seleniumgridurl.equalsIgnoreCase("local")) {
					tempWebDriver = new ChromeDriver(capab);
					tempDriver = new EventFiringWebDriver(tempWebDriver);
				}else{			
					capab.setCapability("uuid", universalID);
					LOG.info("Assigning remote webdriver instance to this.WebDriver");
					tempWebDriver = new Augmenter().augment(new RemoteWebDriver(new URL(seleniumgridurl), capab));
					LOG.info("this.WebDriver :::" + this.WebDriver.toString());
					((RemoteWebDriver)tempWebDriver).setFileDetector(new LocalFileDetector());
					LOG.info("Assigned remote webdriver instance to this.tempWebDriver");
					tempDriver = new EventFiringWebDriver(tempWebDriver);
					MyListener myListener = new MyListener();
					tempDriver.register(myListener);
				}

				LOG.info("Driver launch :: " + browser);		
				LOG.info("Assigned event firing webdriver instance to driver");
				LOG.info("eventfiring driver ::: " + tempDriver.toString());


				QuoteCreation qq= new QuoteCreation(tempDriver, reporter);
				List<HashMap<String, String>> policyDetails = qq.createQuote_PersonalAuto(data);
				/*tempPolicy.setBaseRateValue(qq.BaseRateValue);
//				tempPolicy.setcreditScoreFactor(qq.CreditScoreFactor);
//				tempPolicy.setAgeOfHomeFactor(qq.AgeOfHomeFactor);
//				tempPolicy.setAOPDeductibleFactor(qq.AOPDeductibleFactor);
//				tempPolicy.setOptionalCoveragePremium(Double.toString(qq.OptionalCoveragePremium));				
				tempPolicy.setGrandTotalvalue(Double.toString(qq.GrandTotalvalue));
				tempPolicy.setSurplusContributionValue(Double.toString(qq.SurplusContributionValue));
				tempPolicy.setTotalPremiumValue(Double.toString(qq.TotalPremiumValue));
//				tempPolicy.setLocationPremiumValue(Double.toString(qq.LocationPremiumValue));
//				tempPolicy.setTotalLocationPremiumValue(Double.toString(qq.TotalLocationPremiumValue));
//				tempPolicy.setAdditionalRatePerThousand(Double.toString(qq.AdditionalRatePerThousand));
*/				listOfPolicies.add(policyDetails);


			} catch (Exception e) {
				throw e;

			} finally{
				if(tempDriver !=null){
					tempDriver.quit();
				}
			}
		}

	}

	@AfterClass(alwaysRun = true)
	public void writetoExcel() throws Exception{
//		Collections.sort(listOfPolicies);
		int rowCounter = 1;
		for(int i = 0; i< listOfPolicies.size(); i++){
			List<HashMap<String, String>> temp = (List<HashMap<String, String>>) listOfPolicies.get(i);
			for(int internalCounter = 0; internalCounter<temp.size(); internalCounter++){
				Row currentRow = sheet.createRow(rowCounter);
				currentRow.createCell(0).setCellValue(rowCounter);
				rowCounter++;
				HashMap<String, String> tempMap = temp.get(internalCounter);
				currentRow.createCell(1).setCellValue(tempMap.get("OriginalPolicyNumber"));	
				currentRow.createCell(2).setCellValue(tempMap.get("PolicyNumber"));	
				currentRow.createCell(3).setCellValue(tempMap.get("TotalPremiumValue"));	
				currentRow.createCell(4).setCellValue(tempMap.get("SurplusContributionValue"));	
				currentRow.createCell(5).setCellValue(tempMap.get("GrandTotalvalue"));	
				currentRow.createCell(6).setCellValue(tempMap.get("VehicleName"));	
				currentRow.createCell(7).setCellValue(tempMap.get("LiabilityCSL"));	
				currentRow.createCell(8).setCellValue(tempMap.get("PIP"));	
				currentRow.createCell(9).setCellValue(tempMap.get("MedicalPayments"));	
				currentRow.createCell(10).setCellValue(tempMap.get("UnderinsuredMotorist(CSL)"));	
				currentRow.createCell(11).setCellValue(tempMap.get("UnderinsuredMotoristPD(CSL)"));	
				currentRow.createCell(12).setCellValue(tempMap.get("Comprehensive"));	
				currentRow.createCell(13).setCellValue(tempMap.get("Collision"));	
				currentRow.createCell(14).setCellValue(tempMap.get("TowingAndLabor"));	
				currentRow.createCell(15).setCellValue(tempMap.get("VehiclePremium"));	
				currentRow.createCell(16).setCellValue(tempMap.get("BodilyInjury"));	
				currentRow.createCell(17).setCellValue(tempMap.get("PropertyDamage"));	
				currentRow.createCell(18).setCellValue(tempMap.get("UnderinsuredMotoristBI"));	
				currentRow.createCell(19).setCellValue(tempMap.get("UnderinsuredMotoristPD"));	
				currentRow.createCell(20).setCellValue(tempMap.get("BodilyInjuryRate"));	
				currentRow.createCell(21).setCellValue(tempMap.get("PropertyDamageRate"));	
				currentRow.createCell(22).setCellValue(tempMap.get("PIPRate"));	
				currentRow.createCell(23).setCellValue(tempMap.get("MedicalPaymentsRate"));	
				currentRow.createCell(24).setCellValue(tempMap.get("UnderinsuredMotorist(CSL)Rate"));	
				currentRow.createCell(25).setCellValue(tempMap.get("UnderinsuredMotoristPD(CSL)Rate"));	
				currentRow.createCell(26).setCellValue(tempMap.get("ComprehensiveRate"));	
				currentRow.createCell(27).setCellValue(tempMap.get("CollisionRate"));	
				currentRow.createCell(28).setCellValue(tempMap.get("UnderinsuredMotoristBIRate"));	
				currentRow.createCell(29).setCellValue(tempMap.get("UnderinsuredMotoristPDRate"));	
			}
		}
		File HOExcel = new File("PA-AutoPolicies.xlsx");
		if(HOExcel.exists()){
			HOExcel.delete();	
		}
		FileOutputStream fileOut = new FileOutputStream("PA-AutoPolicies.xlsx");		
		wb.write(fileOut);
		fileOut.close();
	}
	
	
}