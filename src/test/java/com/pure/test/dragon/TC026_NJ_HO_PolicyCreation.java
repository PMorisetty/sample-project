package com.pure.test.dragon;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.support.MyListener;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC026_NJ_HO_PolicyCreation extends ActionEngine {

	boolean status = false;
	int counter = 0;
	boolean executeMode = false;	
	/*boolean testCyber = Boolean.parseBoolean(
		ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));*/
	boolean testCyber = false;
	ConcurrentHashMap<String, String> policyDetails = new ConcurrentHashMap<String, String>();

	XSSFWorkbook wb;
	Sheet sheet;
	List<policyPremiumDetails> listOfPolicies = new ArrayList<policyPremiumDetails>();
	/*
	@BeforeClass
	public void createPolicySheet() throws Exception{
		wb = new XSSFWorkbook();
		sheet = wb.createSheet("kk");
		Row row = sheet.createRow(0);
		row.createCell(0).setCellValue("ROWnum");
		row.createCell(1).setCellValue("QuoteNumber");
		row.createCell(2).setCellValue("BaseRateValue");
		row.createCell(3).setCellValue("BaseRateHurricaneValue");
		row.createCell(4).setCellValue("BaseRateNonHurricaneValue");
		row.createCell(5).setCellValue("CreditScoreFactor");
		row.createCell(6).setCellValue("AgeOfHomeFactor");
		row.createCell(7).setCellValue("AOPDeductibleFactor");
		row.createCell(8).setCellValue("OptionalCoveragePremium");
		row.createCell(9).setCellValue("TotalPremiumValue");
		row.createCell(10).setCellValue("SurplusContributionValue");
		row.createCell(11).setCellValue("GrandTotalvalue");		
		row.createCell(12).setCellValue("LocationPremiumValue");
		row.createCell(13).setCellValue("TotalLocationPremiumValue");
		row.createCell(14).setCellValue("AdditionalRatePerThousand");
		row.createCell(15).setCellValue("liabilityPremium");
		row.createCell(16).setCellValue("liabilityExtensionRate");
		row.createCell(17).setCellValue("liabilityExtensionNoOfPremises");
		row.createCell(18).setCellValue("finalPartialLimitsFactorDwelling");
		row.createCell(19).setCellValue("finalPartialLimitsFactorContents");
		row.createCell(20).setCellValue("Rating_Zone");
	}

	@DataProvider(parallel=true)
	private Object[][] getTestDataFor_NJ_HO_Inforce() {
		return TestUtil.getData("TC026_NJ_HO_CreateQuote_Script", TestDataDragonILHO,
				"input");
	}*/

	@Test//(dataProvider = "getTestDataFor_NJ_HO_Inforce")
	public void TC026_NJ_HO_CreateQuote_Script()//(Hashtable<String, String> data)
			throws Throwable {
		WebDriver tempWebDriver = null;
		EventFiringWebDriver tempDriver = null;
		//if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			try {
				DesiredCapabilities capab = null;
				String downloadFilepath = System.getProperty("user.dir") + "\\Downloads";
				String driversPath = System.getProperty("user.dir") + "\\Drivers\\";

				HashMap<String, Object> chromePrefs = new HashMap<>();
				chromePrefs.put("profile.default_content_settings.popups", 0);
				chromePrefs.put("download.default_directory", downloadFilepath);

				capab = DesiredCapabilities.chrome();
				System.setProperty("webdriver.chrome.driver", driversPath + "chromedriver_32Bit.exe");
				ChromeOptions options = new ChromeOptions();

				options.setExperimentalOption("prefs", chromePrefs);
				capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				options.addArguments("--start-maximized");
				options.addArguments("test-type");
				options.addArguments("chrome.switches", "--disable-extensions");
				capab.setCapability(ChromeOptions.CAPABILITY, options);
				if (seleniumgridurl.equalsIgnoreCase("local")) {
					tempWebDriver = new ChromeDriver(capab);
					tempDriver = new EventFiringWebDriver(tempWebDriver);
				}else{			
					capab.setCapability("uuid", universalID);
					LOG.info("Assigning remote webdriver instance to this.WebDriver");
					tempWebDriver = new Augmenter().augment(new RemoteWebDriver(new URL(seleniumgridurl), capab));
					LOG.info("this.WebDriver :::" + this.WebDriver.toString());
					((RemoteWebDriver)tempWebDriver).setFileDetector(new LocalFileDetector());
					LOG.info("Assigned remote webdriver instance to this.tempWebDriver");
					tempDriver = new EventFiringWebDriver(tempWebDriver);
					MyListener myListener = new MyListener();
					tempDriver.register(myListener);
				}

				LOG.info("Driver launch :: " + browser);		
				LOG.info("Assigned event firing webdriver instance to driver");
				LOG.info("eventfiring driver ::: " + tempDriver.toString());


				executeMode = true;
				//tempDriver.get("www.facebook.com");
				type(By.xpath("//input[@id='email']"),"nidhirajgr8.in@gmail.com","login");
				type(By.xpath("//input[@id='pass']"),"cruise#$90","pass");
				click(By.xpath("//a[text()='View 7 more comments']"),"7 more btn");
				Thread.sleep(5000);
				
				
/*				QuoteCreation qq= new QuoteCreation(tempDriver, reporter, testCyber);
				policyPremiumDetails tempPolicy = new policyPremiumDetails();
				String rownum = data.get("Row");
				String quoteNum=qq.createQuote(data);				
				String baseRateValue = qq.BaseRateValue;	
				String baseRateHurricaneValue = qq.BaseRateHurricaneValue;
				String baseRateNonHurricaneValue=qq.BaseRateNonHurricaneValue;
				String creditScoreFactor = qq.CreditScoreFactor;
				String ageOfHomeFactor = qq.AgeOfHomeFactor;
				String AOPDeductibleFactor = qq.AOPDeductibleFactor;
				String optionalCoveragePremium = Double.toString(qq.OptionalCoveragePremium);				
				String grandTotalvalue = Double.toString(qq.GrandTotalvalue);
				String surplusContributionValue = Double.toString(qq.SurplusContributionValue);
				String totalPremiumValue = Double.toString(qq.TotalPremiumValue);
				String locationPremiumValue = Double.toString(qq.LocationPremiumValue);
				String totalLocationPremiumValue = Double.toString(qq.TotalLocationPremiumValue);
				String additionalRatePerThousand = Double.toString(qq.AdditionalRatePerThousand);
				String liabilityPremium = qq.liabilityPremium;
				String liabilityExtensionRate = qq.liabilityExtensionRate;
				String liabilityExtensionNoOfPremises = qq.liabilityExtensionNoOfPremises;
				String finalPartialLimitsFactorDwelling = qq.finalPartialLimitsFactorDwelling;
				String finalPartialLimitsFactorContents = qq.finalPartialLimitsFactorContents;
				String territoryZone = data.get("Rating_Zone");
				
				
				tempPolicy.setRownum(rownum);
				tempPolicy.setPolicyNumber(quoteNum);				
				tempPolicy.setBaseRateValue(baseRateValue);
				tempPolicy.setBaseRateHurricaneValue(baseRateHurricaneValue);
				tempPolicy.setBaseRateNonHurricaneValue(baseRateNonHurricaneValue);
				tempPolicy.setcreditScoreFactor(creditScoreFactor);
				tempPolicy.setAgeOfHomeFactor(ageOfHomeFactor);
				tempPolicy.setAOPDeductibleFactor(AOPDeductibleFactor);
				tempPolicy.setOptionalCoveragePremium(optionalCoveragePremium);				
				tempPolicy.setGrandTotalvalue(grandTotalvalue);
				tempPolicy.setSurplusContributionValue(surplusContributionValue);
				tempPolicy.setTotalPremiumValue(totalPremiumValue);
				tempPolicy.setLocationPremiumValue(locationPremiumValue);
				tempPolicy.setTotalLocationPremiumValue(totalLocationPremiumValue);
				tempPolicy.setAdditionalRatePerThousand(additionalRatePerThousand);
				tempPolicy.setLiabilityPremium(liabilityPremium);
				tempPolicy.setLiabilityExtensionRate(liabilityExtensionRate);
				tempPolicy.setLiabilityExtensionNoOfPremises(liabilityExtensionNoOfPremises);
				tempPolicy.setFinalPartialLimitsFactorDwelling(finalPartialLimitsFactorDwelling);
				tempPolicy.setFinalPartialLimitsFactorContents(finalPartialLimitsFactorContents);
				tempPolicy.setTerritoryZone(territoryZone);
				listOfPolicies.add(tempPolicy);

				String strStepsDes = "rownum,"+rownum+","+
						"PolicyNumber," + quoteNum+","+
						"BaseRateValue," + baseRateValue+","+
						"BaseRateHurricaneValue," + baseRateHurricaneValue+","+
						"BaseRateNonHurricaneValue," + baseRateNonHurricaneValue+","+
						"creditScoreFactor," + creditScoreFactor+","+
						"creditScoreFactor,"+ ageOfHomeFactor+","+
						"AOPDeductibleFactor," + AOPDeductibleFactor+","+
						"OptionalCoveragePremium," + optionalCoveragePremium+","+
						"TotalPremiumValue," + grandTotalvalue+	","+
						"SurplusContributionValue," + surplusContributionValue+","+
						"GrandTotalvalue," + totalPremiumValue+","+
						"LocationPremiumValue," + locationPremiumValue+","+
						"TotalLocationPremiumValue," + totalLocationPremiumValue +","+
						"AdditionalRatePerThousand,"+ additionalRatePerThousand;
				reporter.SuccessReport("policy infor", strStepsDes);*/

			} catch (Exception e) {
				throw e;

			} finally{
				if(tempDriver !=null){
					tempDriver.quit();
				}
			}
		//}

	}



	@AfterClass
	public void writetoExcel() throws Throwable{	
		/*Collections.sort(listOfPolicies);
		for(int i = 0; i< listOfPolicies.size(); i++){
			policyPremiumDetails temp = listOfPolicies.get(i);
			int rowIndex = i+1;
			Row currentRow = sheet.createRow(rowIndex);
			currentRow.createCell(0).setCellValue(temp.getRownum());
			if(temp.getPolicyNumber()!=""){
				currentRow.createCell(1).setCellValue(temp.getPolicyNumber());				
				currentRow.createCell(2).setCellValue(temp.getBaseRateValue());
				currentRow.createCell(3).setCellValue(temp.getBaseRateHurricaneValue());
				currentRow.createCell(4).setCellValue(temp.getBaseRateNonHurricaneValue());
				currentRow.createCell(5).setCellValue(temp.getcreditScoreFactor());
				currentRow.createCell(6).setCellValue(temp.getAgeOfHomeFactor());
				currentRow.createCell(7).setCellValue(temp.getAOPDeductibleFactor());
				currentRow.createCell(8).setCellValue(temp.getOptionalCoveragePremium());									
				currentRow.createCell(9).setCellValue(temp.getTotalPremiumValue());
				currentRow.createCell(10).setCellValue(temp.getSurplusContributionValue());
				currentRow.createCell(11).setCellValue(temp.getGrandTotalvalue());
				currentRow.createCell(12).setCellValue(temp.getLocationPremiumValue());
				currentRow.createCell(13).setCellValue(temp.getTotalLocationPremiumValue());
				currentRow.createCell(14).setCellValue(temp.getAdditionalRatePerThousand());
				currentRow.createCell(15).setCellValue(temp.getLiabilityPremium());
				currentRow.createCell(16).setCellValue(temp.getLiabilityExtensionRate());
				currentRow.createCell(17).setCellValue(temp.getLiabilityExtensionNoOfPremises());
				currentRow.createCell(18).setCellValue(temp.getFinalPartialLimitsFactorDwelling());
				currentRow.createCell(19).setCellValue(temp.getFinalPartialLimitsFactorContents());
				System.out.println(
						"rownum,"+temp.getRownum()+","+
						"PolicyNumber," + temp.getPolicyNumber()+
						"BaseRateValue," + temp.getBaseRateValue()+
						"BaseRateHurricaneValue," + temp.getBaseRateHurricaneValue()+
						"BaseRateNonHurricaneValue," + temp.getBaseRateNonHurricaneValue()+
						"creditScoreFactor," + temp.getcreditScoreFactor()+
						"creditScoreFactor,"+ temp.getAgeOfHomeFactor()+
						"AOPDeductibleFactor," + temp.getAOPDeductibleFactor()+
						"OptionalCoveragePremium," + temp.getOptionalCoveragePremium()+	
						"TotalPremiumValue," + temp.getTotalPremiumValue()+	
						"SurplusContributionValue," + temp.getSurplusContributionValue()+
						"GrandTotalvalue," + temp.getGrandTotalvalue()+
						"LocationPremiumValue," + temp.getLocationPremiumValue() +
						"TotalLocationPremiumValue," + temp.getTotalLocationPremiumValue() +
						"AdditionalRatePerThousand,"+ temp.getAdditionalRatePerThousand()

						);				
			}
			currentRow.createCell(20).setCellValue(temp.getTerritoryZone());
		}
		File HOExcel = new File("NJ-HOPolicies.xlsx");
		if(HOExcel.exists()){
			HOExcel.delete();	
		}
		FileOutputStream fileOut = new FileOutputStream("NJ-HOPolicies.xlsx");		
		wb.write(fileOut);
		fileOut.close();*/
	} 
	
}