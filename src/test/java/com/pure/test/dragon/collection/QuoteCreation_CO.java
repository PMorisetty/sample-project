package com.pure.test.dragon.collection;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.BillingPageLib;
import com.pure.dragon.FinalSummaryPageLib;
import com.pure.dragon.InsuranceScoreManagementLib;
import com.pure.dragon.PolicyCurrentSummaryPageLib;
import com.pure.dragon.PolicyDeliveryPageLib;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PremiumPageLib;
import com.pure.dragon.SubjectivitiesPageLib;
import com.pure.dragon.SummaryPageLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.collection.AdditionalBindingInfoPageLib;
import com.pure.dragon.collection.AdditionalInsuredPageLib;
import com.pure.dragon.collection.CollectionPolicyPageLib;
import com.pure.dragon.collection.CoverageByClassPageLib;
import com.pure.dragon.collection.CoverageScheduledPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.dragon.homeOwners.ManuscriptEndorsementsPageLib;
import com.pure.dragon.homeOwners.PreviousClaimDetailsPageLib;
import com.pure.report.CReporter;
import com.pure.test.cyber.commons.QuoteCreation;

public class QuoteCreation_CO extends ActionEngine {

	boolean testCyber = false;
	String policyNumber;

	public QuoteCreation_CO(EventFiringWebDriver webDriver, CReporter reporter) {
		// TODO Auto-generated constructor stub
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public String createQuote(Hashtable<String, String> data) throws Throwable {
		openApplication("UW");
		new QuoteCreation(driver, reporter, testCyber).initiateQuoteCreation(data);
		

		CollectionPolicyPageLib collectionPolicyPageLib = new CollectionPolicyPageLib(driver, reporter);
		
		if(data.get("Insurance_Score")!=null && data.get("Insurance_Score")!="0"){
			new PolicyEndorsementLib(driver, reporter).navigateToCustomerDetails();
			new InsuranceScoreManagementLib(driver, reporter).addNewInsuranceScore(data);					
		}
		collectionPolicyPageLib.fillAllFields(data);

		CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
		commonPageLib.clickNextBtn();

		CoverageByClassPageLib coverageByClassPageLib = new CoverageByClassPageLib(driver, reporter);
		switch(data.get("state")){
		case "CA":
				coverageByClassPageLib.fillAllWorldwideJewelryFields(data);
				coverageByClassPageLib.fillAllBankVaultedJewelryFields(data);
				coverageByClassPageLib.fillAllFineArtsFields(data);
				coverageByClassPageLib.fillAllCollectiblesFields(data);
				coverageByClassPageLib.fillAllCoinsSilverStampsFields(data);
				coverageByClassPageLib.fillAllWineFields(data);
				break;
		default:
				coverageByClassPageLib.fillAllWorldwideJewelryFields(data);
				coverageByClassPageLib.fillAllBankVaultedJewelryFields(data);
				coverageByClassPageLib.fillAllFineArtsCollectiblesFields(data);
				coverageByClassPageLib.fillAllCoinsSilverStampsFields(data);
				coverageByClassPageLib.fillAllWineFields(data);
				break;
		}
		commonPageLib.clickNextBtn();

		CoverageScheduledPageLib coverageScheduledPageLib = new CoverageScheduledPageLib(driver, reporter);
		coverageScheduledPageLib.addAnotheItem(data);
		commonPageLib.clickNextBtn();

		PreviousClaimDetailsPageLib claimsHistoryPageLib = new PreviousClaimDetailsPageLib(driver, reporter);
		claimsHistoryPageLib.radioBtn_LossClaimHistory(data.get("priorLosses"));
		claimsHistoryPageLib.addAnotheItem(data);
		commonPageLib.clickNextBtn();
		if("CA".equalsIgnoreCase(data.get("state")))
			commonPageLib.clickNextBtn();
		AdditionalBindingInfoPageLib additionalBindingInfoPageLib = new AdditionalBindingInfoPageLib(driver, reporter);
		additionalBindingInfoPageLib.set_Occupation(data.get("Occupation"));
		additionalBindingInfoPageLib.set_Employer(data.get("Employer"));
		additionalBindingInfoPageLib.radioBtn_ExistingAgencyClient(data.get("ExistingAgencyClient"));
		additionalBindingInfoPageLib.radioBtn_AnyCompanyRefusedToInsure(data.get("AnyCompanyRefusedToInsure"));
		additionalBindingInfoPageLib.radioBtn_CoverageRenewed(data.get("CoverageRenewed"));
		commonPageLib.clickNextBtn();

		AdditionalInsuredPageLib additionalInsuredPageLib = new AdditionalInsuredPageLib(driver, reporter);
		additionalInsuredPageLib.addAnotheItem(data);
		commonPageLib.clickNextBtn();

		// OptionalCoveragesPageLib coveragesPageLib = new
		// OptionalCoveragesPageLib();
		commonPageLib.clickNextBtn();

		ManuscriptEndorsementsPageLib manuscriptEndorsementsPageLib = new ManuscriptEndorsementsPageLib(driver,
				reporter);
		manuscriptEndorsementsPageLib.addAnotheItem(data);
		commonPageLib.clickNextBtn();
		manuscriptEndorsementsPageLib.setEndorsementText_CO(data.get("EndorsementText"));
		commonPageLib.clickNextBtn();
		commonPageLib.clickNextBtn();

		commonPageLib.clickRateBtn();
		UnderwriterReferralsPageLib underwriterReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
		underwriterReferralsPageLib.clickUnderwriterReferralsTab();
		underwriterReferralsPageLib.fillUnderwriterNotes(data.get("underwriterNote"));
		underwriterReferralsPageLib.selectUnderwriter(data.get("uwName"));
		underwriterReferralsPageLib.clickAcceptButton();

		SummaryPageLib summaryPageLib = new SummaryPageLib(driver, reporter);
		summaryPageLib.clickRequestIssueButton();

		PremiumPageLib premiumPageLib = new PremiumPageLib(driver, reporter);
		premiumPageLib.clickBindButton();

		PolicyDeliveryPageLib policyDeliveryPageLib = new PolicyDeliveryPageLib(driver, reporter);
		policyDeliveryPageLib.setElectronicDelivery(data.get("memberEmail"), data.get("phoneNumber"));
		policyDeliveryPageLib.clickNextButton();

		BillingPageLib billingPageLib = new BillingPageLib(driver, reporter);
		billingPageLib.selectBillingAddress("SendToBilling");
		billingPageLib.clickConfirmBtn();

		FinalSummaryPageLib finalSummaryPageLib = new FinalSummaryPageLib(driver, reporter);
		finalSummaryPageLib.clickRequestBind();

		PolicyEndorsementLib policyEndorsementLib = new PolicyEndorsementLib(driver, reporter);
		policyEndorsementLib.navigateToTransationOrEndorsementTab();
		policyEndorsementLib.navigateToNBtransaction();
		policyEndorsementLib.navigateToSubjectivities();

		SubjectivitiesPageLib subjectivitiesPageLib = new SubjectivitiesPageLib(driver, reporter);
		subjectivitiesPageLib.overrideValues();
		subjectivitiesPageLib.clickManualBindButton();

		PolicyCurrentSummaryPageLib policyCurrentSummaryPageLib = new PolicyCurrentSummaryPageLib(driver, reporter);
		policyNumber = policyCurrentSummaryPageLib.getPolicyNumber();
//		policyCurrentSummaryPageLib.downloadVerifyNBPolicy();
		
		return policyNumber;
		
	}
}
