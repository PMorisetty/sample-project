package com.pure.test.PredictiveAnalytics;

import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.HomeLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC001_PredictiveAnalysis_CreateQuote extends ActionEngine {
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));

	@BeforeMethod
	public void beforeTest() {
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC001_PredictiveAnalysis_CreateQuote", TestDataPredictiveAnalytics,
				"TestDetails");
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_PredictiveAnalysis_CreateQuote", TestDataPredictiveAnalytics,
				"LoginData");
	}

	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC001_PredictiveAnalysis_CreateQuote", TestDataPredictiveAnalytics,
				"AgencyDetails");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC001_PredictiveAnalysis_CreateQuote", TestDataPredictiveAnalytics,
				"BasicInfo");
	}

	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC001_PredictiveAnalysis_CreateQuote", TestDataPredictiveAnalytics,
				"QuoteDetails");
	}

	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC001_PredictiveAnalysis_CreateQuote", TestDataPredictiveAnalytics,
				"LocationDetails");
	}

	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC001_PredictiveAnalysis_CreateQuote", TestDataPredictiveAnalytics,
				"PreviousClaims");
	}

	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC001_PredictiveAnalysis_CreateQuote", TestDataPredictiveAnalytics,
				"MemberInfo");
	}

	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC001_PredictiveAnalysis_CreateQuote", TestDataPredictiveAnalytics,
				"UnderwriterRefferal");
	}

	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC001_PredictiveAnalysis_CreateQuote", TestDataPredictiveAnalytics,
				"DocumentDelivery");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException {
		// this.reporter.initTestCaseDescription("");
		return TestUtil.getAllData(getTestDataFor_Login(),
				getTestDataFor_TestDetails(),getTestDataFor_AgencyDetails(),
				getTestDataFor_BasicInfo(), getTestDataFor_QuoteDetails(),
				getTestDataFor_LocationDetails(),
				getTestDataFor_PreviousClaims(), getTestDataFor_MemberInfo(),
				getTestDataFor_UnderwriterRefferal(),
				getTestDataFor_DocumentDelivery());
	}

	@Test(dataProvider = "getTestData")
	public void TC001_PredictiveAnalysis_CreateQuote_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			String quoteNumber = new QuoteCreation(driver, reporter, testCyber).createQuotePredictiveAnalytics(data);
			System.out.println("Quote Number::::::::::::" + quoteNumber);
		}
	}

	@AfterMethod
	public void afterMethod() throws Throwable {
		LOG.info("Inside After Method");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
	}
}
