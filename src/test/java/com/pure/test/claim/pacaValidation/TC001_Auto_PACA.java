package com.pure.test.claim.pacaValidation;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.LossDetailsPageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.SecondaryApprovalPageLib;
import com.pure.claim.lifeCycle.ChecksPageLib;
import com.pure.claim.lifeCycle.CloseClaimPageLib;
import com.pure.claim.lifeCycle.ExposuresPageLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.QuickCheckBasicsPageLib;
import com.pure.claim.lifeCycle.QuickChecksDetailsPageLib;
import com.pure.claim.lifeCycle.ReopenClaimPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.lifeCycle.WorkPlanPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;


public class TC001_Auto_PACA extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;
	boolean loginStatus = false;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_Auto_PACA_Script", TestDataClaimPACARules, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC001_Auto_PACA_Script", TestDataClaimPACARules, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC001_Auto_PACA_Script", TestDataClaimPACARules, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC001_Auto_PACA_Script", TestDataClaimPACARules, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC001_Auto_PACA_Script", TestDataClaimPACARules, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC001_Auto_PACA_Script", TestDataClaimPACARules, "Exposures");
	}
	
	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC001_Auto_PACA_Script", TestDataClaimPACARules, "Reserves");
	}
	private Object[][] getTestDataFor_QuickCheck() {
		return TestUtil.getData("TC001_Auto_PACA_Script", TestDataClaimPACARules, "QuickCheck");
	}

	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC001_Auto_PACA_Script", TestDataClaimPACARules, "VehicleIncident");
	}
	private Object[][] getTestDataFor_Documents() {
		return TestUtil.getData("TC001_Auto_PACA_Script", TestDataClaimPACARules, "Documents");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC001_Auto_PACA_Script", TestDataClaimPACARules, "TestDetails");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),getTestDataFor_ExposureDetails(),
				getTestDataFor_Reserves(),getTestDataFor_QuickCheck(),getTestDataFor_VehicleIncident(),getTestDataFor_Documents(),getTestDataFor_TestDetails());
	}

	
	
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	
	@Test(dataProvider = "getTestData")
	public void TC001_Auto_PACA_Script(Hashtable<String, String> data) throws Throwable{
		if("Y".equalsIgnoreCase(data.get("execute"))){
			HomePageLib homePageLib = new HomePageLib(driver, reporter);
			openApplication("Claim");
			homePageLib.login(data.get("username"), data.get("password"));
			loginStatus =true;
			homePageLib.navigateToNewClaimInClaimMenu();
	
			//New claim creation starts
			NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
			newClaimPageLib.selectVerifiedPolicyOption();
	
			VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
			verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
			newClaimPageLib.clickNext();		
			
			//Verify navigated to step2
			//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
			VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);
	
			//Reported by info		
			VerifiedPolicyBasicInfoLib.setHowReported(data.get("howReported"));
			VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
			VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
			VerifiedPolicyBasicInfoLib.selectInvolvedVehicle(data.get("policylevelcoverage"));
			newClaimPageLib.clickNext();
	
			//Verify navigated to step3
			//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");
	
			//Enter loss details info
			AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver,reporter);
			addClaimInfoPageLib.enterVehiclesInformation(data);
			addClaimInfoPageLib.setLossDetails(data);
			addClaimInfoPageLib.setLossDescription(data.get("description"));
			addClaimInfoPageLib.setLossLocation(data);
			newClaimPageLib.clickNext();//Duplicate claims too handled in this.
			
			//Verify navigated to step4
			//newClaimPageLib.verifyTitle("Step 4 of 4: Save and Assign Claim");
			
			//Fill adjuster details and note
			
			SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
			saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
			saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
			saveAndAssignClaimLib.setNote(data.get("additionalNote"));	
			//New Exposure addition
			saveAndAssignClaimLib.addNewExposure(data.get("policylevelcoverage"), data.get("exposuretype"),data.get("exposuretype2"));
			NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
			newExposurePageLib.filldetails(data);
			newExposurePageLib.clickOK();
			//Submit the claim
			saveAndAssignClaimLib.clickFinish();    
			String claimNumber = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
			homePageLib.searchClaim(claimNumber);
			
			//Verify the notes on summary page
			SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
			summaryOverviewPageLib.verifyLatestNotes(data.get("additionalNote"));
			summaryOverviewPageLib.getNumberOfExposuresAdded();
	
			ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
			actionsPageLib.clickAndHoverOnActions();
			actionsPageLib.clickOn("ValidateClaimAndExposure");
			actionsPageLib.clickOn("AbilityToPay");
			
			// Set Large Loss - Added for PACA rule
			summaryOverviewPageLib.openLossDetailsPage();
			LossDetailsPageLib lossDetailsPageLib = new LossDetailsPageLib(driver, reporter);
			lossDetailsPageLib.clickEdit();
			lossDetailsPageLib.setLargeLossRadioBtn(data.get("largeLoss"));
			lossDetailsPageLib.clickUpdate();
			
			//Add reserves
			SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
			setReservesPageLib.addNewReserveFromActionsMenu(data);
			
			//open financial page and verify the added reserves 
			FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
			financialsPageLib.openFinancials();
			financialsPageLib.verifyCostTypeAndExpensePACAAreDisplayedInSummaryPage(data.get("costtype"), data.get("newavailablereserve"), data.get("PACARate"));
			
			//QuickCheck basics & details		
			actionsPageLib.clickAndHoverOnActions();
			actionsPageLib.clickOn("ValidateClaimAndExposure");
			actionsPageLib.clickOn("ValidForISO");
			financialsPageLib.openQuickCheck();		
	
			QuickCheckBasicsPageLib quickCheckBasicsPageLib = new QuickCheckBasicsPageLib(driver, reporter);		
			quickCheckBasicsPageLib.verifyDeductibleDecisionWhenOverrideDeductibleIsOnlyNo(data.get("WaivedOrApplied"),data.get("waivedReason"),data.get("otherComments"));
			quickCheckBasicsPageLib.fillDetails(data);
			new QuickChecksDetailsPageLib(driver, reporter).fillDetails(data);
			
			//check the status of Quick Check added
			financialsPageLib.openChecks();		
			assertTextStringMatching(new ChecksPageLib(driver, reporter).getCheckStatus(),"Pending approval");
			
			
			homePageLib.logout();
			homePageLib.login(data.get("secondaryapprovalusername"), data.get("secondaryapprovalpassword"));
			homePageLib.openSecondaryApproval();
			new SecondaryApprovalPageLib(driver, reporter).ApproveSelectedRow(claimNumber);
			homePageLib.logout();
				
			homePageLib.login(data.get("username"), data.get("password"));
			homePageLib.searchClaim(claimNumber);
			//Check Payments on Summary page
			summaryOverviewPageLib.openSummaryPage();
			summaryOverviewPageLib.getPaidAmount();
			//Check Payments on Financials page------------------
			financialsPageLib.openFinancials();
			financialsPageLib.getTotalPayment();
			//check the status of Quick Check added
			financialsPageLib.openChecks();
			assertTextStringMatching(new ChecksPageLib(driver, reporter).getCheckStatus(),"Awaiting submission");
			
			WorkPlanPageLib workPlanPageLib = new WorkPlanPageLib(driver, reporter);
			workPlanPageLib.selectWorkplanFilterDropdown("My open activities");
			workPlanPageLib.completeAllActivities();
			
			new ExposuresPageLib(driver, reporter).closeExposure((data));
			//Check payment status on closing Exposure
			financialsPageLib.openFinancials();
			financialsPageLib.openChecks();
			ChecksPageLib checksPageLib = new ChecksPageLib(driver, reporter);
			checksPageLib.verifyPACADetails("Pure Risk Management", data.get("PACARate"));
			actionsPageLib.clickOn("closeClaim");
			new CloseClaimPageLib(driver, reporter).closeClaim(data.get("closeClaimNotes"), data.get("outcome"), data.get("deductibleDecisionClaim"), data.get("paInvolved"));
			
			//ReopenClaim
			actionsPageLib.clickOn("reopenClaim");
			new ReopenClaimPageLib(driver, reporter).clickReopenClaimBtn();
				
			/*new WorkPlanPageLib().completeAllActivities();
			new ExposuresPageLib().closetAllExposures(data.get("closeexposurenotes"), data.get("deductibleDecision"));
			new CloseClaimPageLib().closeClaim(data.get("closeClaimNotes"), data.get("outcome"));*/
			 
			status = true;		
		}
	}
	
	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result)  throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		if(loginStatus)
			new HomePageLib(driver, reporter).logout();
	}
}
