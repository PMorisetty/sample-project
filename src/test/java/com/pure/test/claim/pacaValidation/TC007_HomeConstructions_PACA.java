package com.pure.test.claim.pacaValidation;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.EnumLOBType;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.SecondaryApprovalPageLib;
import com.pure.claim.lifeCycle.ChecksPageLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.QuickCheckBasicsPageLib;
import com.pure.claim.lifeCycle.QuickChecksDetailsPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyBasicInformationPageLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyMainPageLib;
import com.pure.utilities.TestUtil;

public class TC007_HomeConstructions_PACA extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;
	boolean loginStatus = false;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_Coverages() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "Coverages");
	}

	private Object[][] getTestDataFor_SearchAddressBook() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "SearchAddressBook");
	}

	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "Exposures");
	}

	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "Reserves");
	}

	private Object[][] getTestDataFor_QuickCheck() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "QuickCheck");
	}

	private Object[][] getTestDataFor_Documents() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "Documents");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC007_HomeConstructions_PACA_Script", TestDataClaimPACARules, "TestDetails");
	}
	

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_Coverages(),getTestDataFor_SearchAddressBook(),getTestDataFor_Exposures(),
				getTestDataFor_Reserves(),getTestDataFor_QuickCheck(),getTestDataFor_Documents(),getTestDataFor_TestDetails());
	}

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	@Test(dataProvider = "getTestData")
	public void TC007_HomeConstructions_PACA_Script(Hashtable<String, String> data) throws Throwable{
		if("Y".equalsIgnoreCase(data.get("execute"))){
			String policyNumber = "HC" + (int) (new Date().getTime()/10000);
			//initialize qtest object
			HomePageLib homePageLib = new HomePageLib(driver, reporter);
			openApplication("Claim");
			homePageLib.login(data.get("username"), data.get("password"));
			loginStatus = true;
			homePageLib.navigateToNewClaimInClaimMenu();
			NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
			newClaimPageLib.selectUnVerifiedPolicyOption();
			//Thread.sleep(2000);
			//Initial step in claim creation
			UnverifiedPolicyMainPageLib unverifiedPolicyMainPageLib = new UnverifiedPolicyMainPageLib(driver, reporter);
			unverifiedPolicyMainPageLib.setLOBType(EnumLOBType.HomeConstruction);
			unverifiedPolicyMainPageLib.setPolicyNumber(policyNumber);
			unverifiedPolicyMainPageLib.setDateOfLoss(data.get("dateOfLoss"));

			//Basic Info section
			unverifiedPolicyMainPageLib.setEffectiveDate(data.get("effectiveDate"));
			unverifiedPolicyMainPageLib.setExpirationDate(data.get("expirationDate"));
			unverifiedPolicyMainPageLib.setProjectType(data.get("projectType"));
			//Insured details
			unverifiedPolicyMainPageLib.fillInsuredDetails(data);

			//unverifiedPolicyMainPageLib.addPolicyLevelCoverages(data);

			//General Contractor
			unverifiedPolicyMainPageLib.fillGeneralConractorDetails(data);

			//add policy level coverage for getting exposures
			unverifiedPolicyMainPageLib.addPolicyLevelCoverages(data);

			//navigate to next page
			newClaimPageLib.clickNext();

			//Verify navigated to step2
			//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");

			//Reported by info
			UnverifiedPolicyBasicInformationPageLib unverifiedPolicyBasicInformationPageLib = new UnverifiedPolicyBasicInformationPageLib(driver, reporter);
			unverifiedPolicyBasicInformationPageLib.setHowReported(data.get("howReported"));
			unverifiedPolicyBasicInformationPageLib.setReportedBy(data.get("reporterName"));
			unverifiedPolicyBasicInformationPageLib.setRelationToInsured(data.get("relationToInsured"));
			//Main contact info
			unverifiedPolicyBasicInformationPageLib.selectReporter(data.get("mainContact"), data.get("mainContactPersonName"));

			//navigate to next page
			newClaimPageLib.clickNext();

			//Verify navigated to step3
			//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

			//Enter loss details info
			AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
			addClaimInfoPageLib.setLossDetails(data);
			addClaimInfoPageLib.setLossDescription(data.get("description"));
			addClaimInfoPageLib.setLossLocation(data);

			//navigate to next page
			newClaimPageLib.clickNext();

			//navigate to next page
			newClaimPageLib.clickNext();

			//Verify navigated to step4
			//newClaimPageLib.verifyTitle("Step 4 of 4: Save and Assign Claim");

			//Fill adjuster details and note
			SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
			saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
			saveAndAssignClaimLib.selectName(data.get("assignClaimTo"),data.get("secondaryapprovalusername"));
			saveAndAssignClaimLib.setNote(data.get("additionalNote"));
			//add new exposure
			saveAndAssignClaimLib.addNewExposure(data.get("policylevelcoverage"), data.get("exposuretype"), data.get("exposuretype2"));
			NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
			newExposurePageLib.setDamageDescription(data.get("damageDescription"));
			newExposurePageLib.setLossOfUse(data.get("lossOfUse"));
			newExposurePageLib.setLossEstimate(data.get("lossEstimate"));
			newExposurePageLib.setLocation(data.get("lossLocation"));
			newExposurePageLib.filldetails(data);
			newExposurePageLib.clickOK();
			//Submit the claim
			saveAndAssignClaimLib.clickFinish();
			String claimNumber = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
			homePageLib.searchClaim(claimNumber);

			//Verify the notes on summary page
			SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
			summaryOverviewPageLib.verifyLatestNotes(data.get("additionalNote"));
			summaryOverviewPageLib.getNumberOfExposuresAdded();
			
			ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
			actionsPageLib.clickAndHoverOnActions();
			actionsPageLib.clickOn("ValidateClaimAndExposure");
			actionsPageLib.clickOn("AbilityToPay");

			//Add Reserve
			SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
			setReservesPageLib.addNewReserveFromActionsMenu(data);
			
			//QuickCheck basics & details
			actionsPageLib.clickAndHoverOnActions();
			actionsPageLib.clickOn("ValidateClaimAndExposure");
			actionsPageLib.clickOn("ValidForISO");

			FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
			financialsPageLib.openQuickCheck();		
			new QuickCheckBasicsPageLib(driver, reporter).fillDetails(data);		
			new QuickChecksDetailsPageLib(driver, reporter).fillDetails(data);
			
			//check the status of Quick Check added
			financialsPageLib.openChecks();		
			assertTextStringMatching(new ChecksPageLib(driver, reporter).getCheckStatus(),"Pending approval");

			homePageLib.logout();

			homePageLib.login(data.get("secondaryapprovalusername"), data.get("secondaryapprovalpassword"));
			homePageLib.openSecondaryApproval();
			new SecondaryApprovalPageLib(driver, reporter).ApproveSelectedRow(claimNumber);
			homePageLib.logout();
			homePageLib.login(data.get("username"), data.get("password"));
			homePageLib.searchClaim(claimNumber);
			//Check Payments on Financials page------------------
			financialsPageLib.openFinancials();
			financialsPageLib.getTotalPayment();
			//check the status of Quick Check added
			financialsPageLib.openChecks();
			assertTextStringMatching(new ChecksPageLib(driver, reporter).getCheckStatus(),"Awaiting submission");

			/*	
			new WorkPlanPageLib().completeAllActivities();
			new ExposuresPageLib().closetAllExposures(data.get("closeexposurenotes"), data.get("deductibleDecision"));
			new CloseClaimPageLib().closeClaim(data.get("closeClaimNotes"), data.get("outcome"));
			 */
			status = true;
		}
	}

	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result)  throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		if(loginStatus)
			new HomePageLib(driver, reporter).logout();
	}
}

