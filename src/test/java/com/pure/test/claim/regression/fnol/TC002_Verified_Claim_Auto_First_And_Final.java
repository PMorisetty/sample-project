package com.pure.test.claim.regression.fnol;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AutoFirstAndFinalPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.SearchAddressBookPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;


public class TC002_Verified_Claim_Auto_First_And_Final extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC002_Verified_Claim_Auto_First_And_Final_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC002_Verified_Claim_Auto_First_And_Final_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC002_Verified_Claim_Auto_First_And_Final_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_SearchAddressBook() {
		return TestUtil.getData("TC002_Verified_Claim_Auto_First_And_Final_Script", TestDataClaim, "SearchAddressBook");
	}
	
	private Object[][] getTestDataFor_AutoFirstAndFinal() {
		return TestUtil.getData("TC002_Verified_Claim_Auto_First_And_Final_Script", TestDataClaim, "AutoFirstAndFinal");
	}
	
	
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_SearchAddressBook(),getTestDataFor_TestDetails(),
				getTestDataFor_AutoFirstAndFinal());
	}

	@Test(dataProvider = "getTestData")
	public void TC002_Verified_Claim_Auto_First_And_Final_Script(Hashtable<String, String> data) throws Throwable{

		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		verifiedPolicyLib.setTypeOfClaim(data.get("typeOfClaim"));
		new NewClaimPageLib(driver,reporter).clickNext();
		
		AutoFirstAndFinalPageLib autoFirstAndFinalPageLib = new AutoFirstAndFinalPageLib(driver, reporter);
		autoFirstAndFinalPageLib.searchVendor();
		
		new SearchAddressBookPageLib(driver, reporter).searchNSelectFromAddressBook(data);
		
		autoFirstAndFinalPageLib.fillDetails(data);//handles duplicate claims as well
		new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		
		status = true;		 		
	}

}
