package com.pure.test.claim.regression.addressbook;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.HomePageLib;
import com.pure.claim.SearchAddressBookPageLib;
import com.pure.claim.lifeCycle.NewVendorCompanyPageLib;
import com.pure.claim.lifeCycle.PartiesInvolvedContactsPageLib;
import com.pure.claim.lifeCycle.PartiesInvolvedPageLib;
import com.pure.utilities.TestUtil;

public class TC001_TC002_AddressBook_PartiesInvolved_SearchAddExistingContact extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC001_TC002_AddressBook_PartiesInvolved_SearchAddExistingContact_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_TC002_AddressBook_PartiesInvolved_SearchAddExistingContact_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC001_TC002_AddressBook_PartiesInvolved_SearchAddExistingContact_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_SearchAddressBook() {
		return TestUtil.getData("TC001_TC002_AddressBook_PartiesInvolved_SearchAddExistingContact_Script", TestDataClaim, "SearchAddressBook");
	}

	private Object[][] getTestDataFor_NewVendorCompany() {
		return TestUtil.getData("TC001_TC002_AddressBook_PartiesInvolved_SearchAddExistingContact_Script", TestDataClaim, "NewVendor(Company)");
	}
	
	private Object[][] getTestDataFor_ClaimSearch() {
		return TestUtil.getData("TC001_TC002_AddressBook_PartiesInvolved_SearchAddExistingContact_Script", TestDataClaim, "ClaimSearch");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_TestDetails(), getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_SearchAddressBook(),getTestDataFor_NewVendorCompany(), getTestDataFor_ClaimSearch());
	}
	
	
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	
	@Test(dataProvider = "getTestData")
	public void TC001_TC002_AddressBook_PartiesInvolved_SearchAddExistingContact_Script(Hashtable<String, String> data) throws Throwable{

		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();
		
		//Search for a claim
		homePageLib.searchClaim(data.get("claimNo"));
		
		//click on parties involved tab
		PartiesInvolvedPageLib partiesInvolvedPageLib = new PartiesInvolvedPageLib(driver, reporter);
		partiesInvolvedPageLib.openPartiesInvolved();
		
		//Click on "Search/Add Existing Contact Button"
		PartiesInvolvedContactsPageLib partiesInvolvedContactsPageLib = new PartiesInvolvedContactsPageLib(driver, reporter);
		partiesInvolvedContactsPageLib.clicksearchOrAddExistingContact();
		
		//select service provider
		SearchAddressBookPageLib searchAddressBookPageLib = new SearchAddressBookPageLib(driver, reporter);
		searchAddressBookPageLib.setType(data.get("searchType"));
		searchAddressBookPageLib.clickSearch();
		
		//create new service provider
		searchAddressBookPageLib.clickCreateNewServiceProvider();
		
		//Enter vendor details
		NewVendorCompanyPageLib newVendorCompanyPageLib = new NewVendorCompanyPageLib(driver, reporter);
		newVendorCompanyPageLib.setName(data.get("vendorName"));
		newVendorCompanyPageLib.setShareOnMemberPortal(data.get("shareOnMemberPortal"));
		
		//add role
		newVendorCompanyPageLib.clickAddRole();
		newVendorCompanyPageLib.setRole(data.get("role"));
		
		//add bank data
		newVendorCompanyPageLib.clickBankdataAdd();
		newVendorCompanyPageLib.addBankData(data);
		
		//update
		newVendorCompanyPageLib.clickUpdate();
		
		partiesInvolvedContactsPageLib.checkListedContactsExist(data.get("vendorName"));
		partiesInvolvedContactsPageLib.deleteContact(data.get("vendorName"));
		
		status = true;		 		
	}
	
	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		//Qtest.UpdateQTestResults(status, "Address Book_Parties involved screen_Search/Add Existing Contact", startTime, endTime);
	}
}
