package com.pure.test.claim.regression.litigation;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.PropertyLiabilityPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.LitigationPageLib;
import com.pure.claim.lifeCycle.PartiesInvolvedContactsPageLib;
import com.pure.claim.lifeCycle.PartiesInvolvedPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;

public class TC005_Litigation_CreateNewMatter_PartiesInvolved extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC005_Litigation_CreateNewMatter_PartiesInvolved", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC005_Litigation_CreateNewMatter_PartiesInvolved", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC005_Litigation_CreateNewMatter_PartiesInvolved", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC005_Litigation_CreateNewMatter_PartiesInvolved", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC005_Litigation_CreateNewMatter_PartiesInvolved", TestDataClaim, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC005_Litigation_CreateNewMatter_PartiesInvolved", TestDataClaim, "Exposures");
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC005_Litigation_CreateNewMatter_PartiesInvolved", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_PropertyIncident() {
		return TestUtil.getData("TC005_Litigation_CreateNewMatter_PartiesInvolved", TestDataClaim, "PropertyIncident");
	}

	private Object[][] getTestDataFor_Litigation() {
		return TestUtil.getData("TC005_Litigation_CreateNewMatter_PartiesInvolved", TestDataClaim, "Litigation");
	}

	private Object[][] getTestDataFor_SearchAddressBook() {
		return TestUtil.getData("TC005_Litigation_CreateNewMatter_PartiesInvolved", TestDataClaim, "SearchAddressBook");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(), getTestDataFor_BasicInfo(),
				getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(), getTestDataFor_ExposureDetails(),
				getTestDataFor_TestDetails(), getTestDataFor_PropertyIncident(), getTestDataFor_Litigation(),
				getTestDataFor_SearchAddressBook());
	}


	@Test(dataProvider = "getTestData")
	public void TC005_Litigation_CreateNewMatter_PartiesInvolved_Script(Hashtable<String, String> data) throws Throwable {

		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		// New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		newClaimPageLib.clickNext();

		// Verify navigated to step2
		// newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		// Reported by info
		VerifiedPolicyBasicInfoLib.setHowReported(data.get("howReported"));
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		// Verify navigated to step3
		// newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		// Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		addClaimInfoPageLib.AddPropertiesIncident(data);
		newClaimPageLib.clickNext();// Duplicate claims too handled in this.

		// -----Services---
		// Verify navigated to step4
		// click next
		newClaimPageLib.clickNext();

		// Fill adjuster details and note

		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		saveAndAssignClaimLib.setNote(data.get("additionalNote"));
		// New Exposure addition
		saveAndAssignClaimLib.addNewExposure(data.get("policylevelcoverage"), data.get("exposuretype"),
				data.get("exposuretype2"));
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();

		new PropertyLiabilityPageLib(driver, reporter).fillDetails(data);

		newExposurePageLib.setIAUsed(data.get("iaused"));
		newExposurePageLib.setDeskAdjustment(data.get("deskadjustment"));
		newExposurePageLib.clickOK();

		// Submit the claim
		saveAndAssignClaimLib.clickFinish();
		String claimNumber = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		homePageLib.searchClaim(claimNumber);

		// Verify that the user is able to create New matter from Actions under
		// Litigation
		LitigationPageLib litigationPageLib = new LitigationPageLib(driver, reporter);
		litigationPageLib.openLitigationTab();
		litigationPageLib.fillAllGeneralDetails(data);
		litigationPageLib.fillAllLitigationDetails(data);
		litigationPageLib.fillAllDefenceDetails(data);
		litigationPageLib.fillAllPlaintiffDetails(data);
		litigationPageLib.clickUpdateBtn();
		litigationPageLib.clickSummarySideMenu();
		litigationPageLib.verifyLitigationCreated(data.get("litigationName"));
		
		// Parties Involved
		PartiesInvolvedPageLib partiesInvolvedPageLib = new PartiesInvolvedPageLib(driver, reporter);
//		partiesInvolvedPageLib.clickPartiesInvolved();
		partiesInvolvedPageLib.openPartiesInvolved();
		PartiesInvolvedContactsPageLib partiesInvolvedContactsPageLib = new PartiesInvolvedContactsPageLib(driver, reporter);
		if(partiesInvolvedContactsPageLib.checkListedContactsExist(data.get("legalVenueName")))
			reporter.SuccessReport("Verify legalVenueName Conatct", "LegalVenueName Contact verified");
		else
			reporter.failureReport("Verify legalVenueName Conatct", "LegalVenueName Contact details incorrect", driver);
		
		if(partiesInvolvedContactsPageLib.checkListedContactsExist(data.get("defendent")))
			reporter.SuccessReport("Verify defendent Conatct", "Defendent Contact verified");
		else
			reporter.failureReport("Verify defendent Conatct", "Defendent Contact details incorrect", driver);
		
		if(partiesInvolvedContactsPageLib.checkListedContactsExist(data.get("plaintiff")))
			reporter.SuccessReport("Verify plaintiff Conatct", "Plaintiff Contact verified");
		else
			reporter.failureReport("Verify plaintiff Conatct", "Plaintiff Contact details incorrect", driver);
		
		status = true;
	}

}
