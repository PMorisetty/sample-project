package com.pure.test.claim.regression.notes;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.NewNotePageLib;
import com.pure.claim.PropertyLiabilityPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.CCLDRRPageLib;
import com.pure.claim.lifeCycle.ExposuresPageLib;
import com.pure.claim.lifeCycle.NotesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.Qtest;
import com.pure.utilities.TestUtil;

public class TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_ClaimSearch() {
		return TestUtil.getData("TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script", TestDataClaim, "ClaimSearch");
	}

	private Object[][] getTestDataFor_NewNotes() {
		return TestUtil.getData("TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script", TestDataClaim, "NewNotes");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script", TestDataClaim, "Exposures");
	}

	private Object[][] getTestDataFor_PropertyIncident() {
		return TestUtil.getData("TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script", TestDataClaim, "PropertyIncident");
	}
	
	private Object[][] getTestDataFor_CCLDRR() {
		return TestUtil.getData("TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script", TestDataClaim, "CCLDRR");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_TestDetails(), getTestDataFor_Login(), getTestDataFor_ClaimSearch(),
				getTestDataFor_NewNotes(), getTestDataFor_GeneralInfo(), getTestDataFor_BasicInfo(), getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(), getTestDataFor_Exposures(), getTestDataFor_PropertyIncident(), getTestDataFor_CCLDRR());
	}

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	@Test(dataProvider = "getTestData")
	public void TC002_NotesVerificationInLinkDocLossDetailsCloseExposureClaim_Script(Hashtable<String, String> data) throws Throwable{

		/****************Claim Creation*****************/
		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver, reporter).clickNext();		

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.

		//click next
		newClaimPageLib.clickNext();

		//Fill adjuster details and note

		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));

		//Submit the claim
		saveAndAssignClaimLib.clickFinish();    
		String cliamNo = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		
		
		//Search for a claim
		homePageLib.searchClaim(cliamNo);
		
		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		NewNotePageLib newNotePageLib = new NewNotePageLib(driver, reporter);
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		PropertyLiabilityPageLib propertyLiabilityPageLib = new PropertyLiabilityPageLib(driver, reporter);			
		NotesPageLib notesPageLib = new NotesPageLib(driver, reporter);
			
		/****************Create New Exposure*****************/
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ChooseByCoverage");
		actionsPageLib.clickOn("policyLevelCoverage");
		actionsPageLib.clickOn("liability");
		actionsPageLib.clickOn("liabilityPropertyDamage");
		
		newExposurePageLib.openNewIncident();
		propertyLiabilityPageLib.fillDetails(data);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.setIAUsed(data.get("iaused"));
		newExposurePageLib.setDeskAdjustment(data.get("deskadjustment"));
		newExposurePageLib.clickOK();
		newExposurePageLib.verifyExposureMessage();
		
		/****************TC004_Notes_Note Work space_Link Document*****************/
		reporter.createHeader("TC004_Notes_Note Work space_Link Document");
		//click on Notes from Actions Menu
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("Note");

		//Enter all required fields
		newNotePageLib.setShareOnMemberPortal(data.get("shareOnMemberPortal"));
		newNotePageLib.setTopic(data.get("topic"));
		newNotePageLib.setConfidential(data.get("confidential"));
		newNotePageLib.setText(data.get("text"));
		
		//Link Document
		newNotePageLib.attachLinkDocument();
		newNotePageLib.clickUpdate(data.get("text"));

		//verify created notes in summary page
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.openSummaryPage();
		summaryOverviewPageLib.verifyLatestNotes(data.get("text"));
		summaryOverviewPageLib.checkPresenceOfDocuments();

		//Verify created notes in Notes page
		summaryOverviewPageLib.openNotesPage();
		notesPageLib.verifyNotesInfo(data.get("text"));
		summaryOverviewPageLib.checkPresenceOfDocuments();
		
		
		/****************TC005_Notes_Create Note_Loss Details Screen_CCLDRR section*****************/
		reporter.createHeader("TC005_Notes_Create Note_Loss Details Screen_CCLDRR section");
		//navigate CCLDRR from loss details
		summaryOverviewPageLib.openLossDetailsPage();
		summaryOverviewPageLib.clickCCLDRR();		
		//click on edit button
		CCLDRRPageLib CCLDRRPageLib = new CCLDRRPageLib(driver, reporter);
		CCLDRRPageLib.clickEdit();
		
		CCLDRRPageLib.setCoverage(data.get("coverage"));
		CCLDRRPageLib.setCompliance(data.get("compliance"));
		CCLDRRPageLib.setLiability(data.get("liability"));
		CCLDRRPageLib.setDamages(data.get("damages"));
		CCLDRRPageLib.setResolutionPlan(data.get("resolutionPlan"));
		CCLDRRPageLib.setReserveAnalysis(data.get("reserveAnalysis"));
		
		CCLDRRPageLib.clickUpdate();
		CCLDRRPageLib.clickOk();
		
		//verify created notes in summary page
		summaryOverviewPageLib.verifyNotesInSummaryPage(data);

		
		//Verify created notes in Notes page
		summaryOverviewPageLib.openNotesPage();
		notesPageLib.verifyNotesInfo(data);
		
		/****************TC006_Notes_Note_Close Exposure*****************/
		reporter.createHeader("TC006_Notes_Note_Close Exposure");
		new ExposuresPageLib(driver, reporter).closeExposure(data);
		
		//verify created notes in summary page
		summaryOverviewPageLib.openSummaryPage();
		summaryOverviewPageLib.verifyLatestNotes(data.get("closeexposurenotes"));

		//Verify created notes in Notes page
		summaryOverviewPageLib.openNotesPage();
		notesPageLib.verifyNotesInfo(data.get("closeexposurenotes"));
		
		status = true;		 		
	}

	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		Qtest.UpdateQTestResults(status, "Verify the functionality of ''Link Document'' in Note Work space", startTime, endTime);
	}
}

