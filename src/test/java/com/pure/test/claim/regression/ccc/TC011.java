package com.pure.test.claim.regression.ccc;


import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.InstructionsToVendorPageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.SearchForAppraisersPageLib;
import com.pure.claim.ServicesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.lifeCycle.VehicleIncidentPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.SOAPEngine;
import com.pure.utilities.TestUtil;


public class TC011 extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC011", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC011", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC011", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC011", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC011", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC011", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC011", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC011", TestDataClaim, "VehicleIncident");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(), getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_ExposureDetails(), getTestDataFor_TestDetails(), getTestDataFor_VehicleIncident());
	}

	
	
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	
	@Test(dataProvider = "getTestData")
	public void TC011_script(Hashtable<String, String> data) throws Throwable{

		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver,reporter).clickNext();		
		
		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setHowReported(data.get("howReported"));
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		VerifiedPolicyBasicInfoLib.selectInvolvedVehicle(data.get("policylevelcoverage"));
		VerifiedPolicyBasicInfoLib.setContactInfo("234-581-9589");
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver,reporter);
		addClaimInfoPageLib.enterVehiclesInformation(data);
		
		SearchForAppraisersPageLib searchForAppraisersPageLib = new SearchForAppraisersPageLib(driver, reporter);
		searchForAppraisersPageLib.clickMethodOfInspection(data.get("methodOfInspection"));
		searchForAppraisersPageLib.selectAppraiserName(data.get("appraiserName"));
		
		InstructionsToVendorPageLib instructionsToVendorPageLib = new InstructionsToVendorPageLib(driver, reporter);
		instructionsToVendorPageLib.fillDetails(data);
		instructionsToVendorPageLib.clickUpdate();
		
		new VehicleIncidentPageLib(driver, reporter).enterDetails(data.get("damagedescription"), data.get("reporterName"), data.get("lossoccured"));
		
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.
		
		//Verify navigated to step4
		//newClaimPageLib.verifyTitle("Step 4 of 4: Save and Assign Claim");
		
		//Fill adjuster details and note
		
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();
		
		ClaimCreationSummaryPageLib claimCreationSummaryPageLib = new ClaimCreationSummaryPageLib(driver, reporter);
		String claimNo = claimCreationSummaryPageLib.getClaimNumber();
		String serviceReqNo = claimCreationSummaryPageLib.getServiceRequestNumber();
		claimCreationSummaryPageLib.clickNewlyCreatedClaim();
		
		/**************************************************/
		/***********SOAP request to upload CSR document ***/
		/*******************************************************START*/		
		System.out.println("SOAP Request Starts here============");
		String claimID = claimNo+"-"+serviceReqNo;
		String endPoint = "https://ccmaint.purehnw.com/cc/ws/com/pure/webservices/CCC/returndata/DocumentSvc";
		String xmlTemplateFilePath = System.getProperty("user.dir") + "/resources/CSR.xml";
		
		System.out.println("XMLFilepath locaion is ============"+xmlTemplateFilePath);
		
		//Prepare UniqueTransactionID, LossReferenceID for dynamic xml creation [residing in SOAPEngine's constructor
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("mes:UniqueTransactionID", claimID+"#01#2017-07-21T17:48:47.129Z");
		parameters.put("mes:LossReferenceID", claimID);
		
		System.out.println("Hashmap parameters are =========="+parameters);
		
		SOAPEngine se = new SOAPEngine(endPoint, xmlTemplateFilePath, parameters);
		se.execute();
		//String response = se.getNodeValueByXpath("/Description/text()");//TO get/read 'SUCCESS' message response string.
		String response = se.getSOAPResponseXML();
		System.out.println("SOAP response============/n"+response);
		/**********************************************************END*/
		
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.openServicesPage();
		
		//verify the status on summary page
		ServicesPageLib servicesPageLib = new ServicesPageLib(driver, reporter);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.clickDetailsTab();
		Thread.sleep(10000);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.verifyLogsInHistory("Claim Summary Report document uploaded into onbase");
		
		/**************************************************/
		/***********SOAP request to upload DI document ***/
		/*******************************************************START*/	
		xmlTemplateFilePath = System.getProperty("user.dir") + "/resources/DI.xml";
		
		
		HashMap<String, String> parameters1 = new HashMap<String, String>();
		parameters1.put("mes:UniqueTransactionID", claimID+"#T01#2017-07-21T17:48:47.129Z");
		parameters1.put("mes:LossReferenceID", claimID);
		
		se = new SOAPEngine(endPoint, xmlTemplateFilePath, parameters1);
		se.execute();
//		response = se.getNodeValueByXpath("/Description/text()");
		response = se.getSOAPResponseXML();
		System.out.println("SOAP response============/n"+response);
		//verify the status on summary page
		
		servicesPageLib.clickDetailsTab();
		Thread.sleep(15000);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.verifyLogsInHistory("Digital Image document uploaded into onbase");
		
		xmlTemplateFilePath = System.getProperty("user.dir") + "/resources/PI_Estimate.xml";
		
		HashMap<String, String> parameters2 = new HashMap<String, String>();
		parameters2.put("mes:UniqueTransactionID", claimID+"#D01#2017-07-21T17:48:47.129Z");
		parameters2.put("mes:LossReferenceID", claimID);
		
		se = new SOAPEngine(endPoint, xmlTemplateFilePath, parameters2);
		se.execute();
//		response = se.getNodeValueByXpath("/Description/text()");
		response = se.getSOAPResponseXML();
		System.out.println("SOAP response============"+response);
		/**********************************************************END*/
		
		//verify the status on summary page
		servicesPageLib.clickDetailsTab();
		Thread.sleep(15000);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.verifyLogsInHistory("Estimate of Record document uploaded into onbase");
		
		status = true;
	}
	
	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		//Qtest.UpdateQTestResults(status, "Create Personal Auto claim for Verified Policy", startTime, endTime);
	}
}
