package com.pure.test.claim.regression.fnol;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.EnumLOBType;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.QuickClaimAutoPageLib;
import com.pure.claim.lifeCycle.VehicleIncidentPageLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyMainPageLib;
import com.pure.utilities.TestUtil;

public class TC006_Unverified_Claim_Auto_Quick_Claim_Auto extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC006_Unverified_Claim_Auto_Quick_Claim_Auto_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC006_Unverified_Claim_Auto_Quick_Claim_Auto_Script", TestDataClaim, "LoginData");
	}
	
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC006_Unverified_Claim_Auto_Quick_Claim_Auto_Script", TestDataClaim, "BasicInfo");
	}
	
	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC006_Unverified_Claim_Auto_Quick_Claim_Auto_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC006_Unverified_Claim_Auto_Quick_Claim_Auto_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_SearchAddressBook() {
		return TestUtil.getData("TC006_Unverified_Claim_Auto_Quick_Claim_Auto_Script", TestDataClaim, "SearchAddressBook");
	}
	
	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC006_Unverified_Claim_Auto_Quick_Claim_Auto_Script", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC006_Unverified_Claim_Auto_Quick_Claim_Auto_Script", TestDataClaim, "VehicleIncident");
	}
	
	private Object[][] getTestDataFor_Coverages() {
		return TestUtil.getData("TC006_Unverified_Claim_Auto_Quick_Claim_Auto_Script", TestDataClaim, "Coverages");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_SearchAddressBook(),getTestDataFor_TestDetails(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(),
				getTestDataFor_ClaimSubmission(),getTestDataFor_VehicleIncident(),
				getTestDataFor_Coverages());
	}

	@Test(dataProvider = "getTestData")
	public void TC006_Unverified_Claim_Auto_Quick_Claim_Auto_Script(Hashtable<String, String> data) throws Throwable{
		String policyNumber = "PA" + (int) (new Date().getTime()/10000);

		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectUnVerifiedPolicyOption();
		//Thread.sleep(2000);
		//Initial step in claim creation
		UnverifiedPolicyMainPageLib unverifiedPolicyMainPageLib = new UnverifiedPolicyMainPageLib(driver, reporter);
		unverifiedPolicyMainPageLib.setLOBType(EnumLOBType.PersonalAuto);
		unverifiedPolicyMainPageLib.setPolicyNumber(policyNumber);
		unverifiedPolicyMainPageLib.setTypeOfClaim(data.get("typeOfClaim"));
		unverifiedPolicyMainPageLib.setDateOfLoss(data.get("dateOfLoss"));

		//Basic Info section
		unverifiedPolicyMainPageLib.setEffectiveDate(data.get("effectiveDate"));
		unverifiedPolicyMainPageLib.setExpirationDate(data.get("expirationDate"));
		//Insured details
		unverifiedPolicyMainPageLib.fillInsuredDetails(data);
		//add policy level coverage
		unverifiedPolicyMainPageLib.addPolicyLevelCoverages(data);
		//add vehicle
		unverifiedPolicyMainPageLib.addVehicle(data);

		//navigate to next page
		newClaimPageLib.clickNext();
		
		//Quick claim auto
		QuickClaimAutoPageLib quickClaimAutoPageLib = new QuickClaimAutoPageLib(driver, reporter);
		quickClaimAutoPageLib.setReportedBy(data.get("reporterName"));
		quickClaimAutoPageLib.setRelationToInsured(data.get("relationToInsured"));
		
		quickClaimAutoPageLib.setLossDetails(data);
		quickClaimAutoPageLib.setLossDescription(data.get("description"));
		quickClaimAutoPageLib.setLossLocation(data);
		quickClaimAutoPageLib.addVehicleIncident();
		
		VehicleIncidentPageLib vehicleIncidentPageLib = new VehicleIncidentPageLib(driver, reporter);
		vehicleIncidentPageLib.setInvolvedVehicle(data.get("vehicle"), data.get("lossParty"));
		vehicleIncidentPageLib.enterDetails(data.get("damagedescription"), data.get("reporterName"), data.get("lossoccured"));
		
		quickClaimAutoPageLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		
		// click finish
		quickClaimAutoPageLib.clickFinish();
		new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		status = true;
	}

}