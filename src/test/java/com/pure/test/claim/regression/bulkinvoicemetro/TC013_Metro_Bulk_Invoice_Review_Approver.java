package com.pure.test.claim.regression.bulkinvoicemetro;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.lifeCycle.ActivitiesPageLib;
import com.pure.claim.lifeCycle.ApprovalPageLib;
import com.pure.claim.lifeCycle.BatchProcessInfoPageLib;
import com.pure.claim.lifeCycle.BulkInvoiceDetailsPageLib;
import com.pure.claim.lifeCycle.BulkInvoicePageLib;
import com.pure.utilities.TestUtil;


public class TC013_Metro_Bulk_Invoice_Review_Approver extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC013_Metro_Bulk_Invoice_Review_Approver_Script", TestDataClaim, "LoginData");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC013_Metro_Bulk_Invoice_Review_Approver_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_BulkInvoice() {
		return TestUtil.getData("TC013_Metro_Bulk_Invoice_Review_Approver_Script", TestDataClaim, "BulkInvoice");
	}
	
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(),getTestDataFor_TestDetails(),
				getTestDataFor_BulkInvoice());
	}
	
	@Test(dataProvider = "getTestData")
	public void TC013_Metro_Bulk_Invoice_Review_Approver_Script(Hashtable<String, String> data) throws Throwable{

		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		/*homePageLib.login(data.get("username"), data.get("password"));
		
		homePageLib.navigateToBatchProcessInfo();
		
		BatchProcessInfoPageLib batchProcessInfoPageLib = new BatchProcessInfoPageLib(driver, reporter);
		batchProcessInfoPageLib.startAutomatedBulkInvoiceJob();
		
		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("returnToClaimCenter");
		
		homePageLib.navigateToBulkInvoices();
		
		BulkInvoicePageLib bulkInvoicePageLib =new BulkInvoicePageLib(driver, reporter);
		bulkInvoicePageLib.selectUploadedCSVFile(data.get("bulkInvoiceFileName"));
		
		BulkInvoiceDetailsPageLib bulkInvoiceDetailsPageLib = new BulkInvoiceDetailsPageLib(driver, reporter);
		
		bulkInvoiceDetailsPageLib.clickSubmit();
		bulkInvoiceDetailsPageLib.verifyApprovalHistoryStatus(data.get("status2"));
		bulkInvoiceDetailsPageLib.clickUptoBulkInvoices();
		bulkInvoicePageLib.verifyStatus(data.get("bulkInvoiceFileName"), data.get("invoicestatus1"));//in review
		homePageLib.logout();*/
		homePageLib.login(data.get("approverusername"), data.get("approverpassword"));
		homePageLib.openActivities();
		
		ActivitiesPageLib activitiesPageLib = new ActivitiesPageLib(driver, reporter);
		activitiesPageLib.selectActivityBasedOnInvoice(data.get("bulkInvoiceFileName"));
		new ApprovalPageLib(driver, reporter).clickReject(data.get("rejectionRationale"));
		
		homePageLib.logout();
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToBulkInvoices();
		BulkInvoicePageLib bulkInvoicePageLib =new BulkInvoicePageLib(driver, reporter);
		bulkInvoicePageLib.verifyStatus(data.get("bulkInvoiceFileName"), data.get("invoicestatus2"));//rejected
		
		status = true;
	}
}
