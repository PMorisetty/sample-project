package com.pure.test.claim.regression.addressbook;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.HomePageLib;
import com.pure.claim.SearchAddressBookPageLib;
import com.pure.utilities.TestUtil;

public class TC003_AddressBook_TypeOfServiceProvider extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC003_AddressBook_TypeOfServiceProvider_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC003_AddressBook_TypeOfServiceProvider_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC003_AddressBook_TypeOfServiceProvider_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_SearchAddressBook() {
		return TestUtil.getData("TC003_AddressBook_TypeOfServiceProvider_Script", TestDataClaim, "SearchAddressBook");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_TestDetails(), getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_SearchAddressBook());
	}

	
	@Test(dataProvider = "getTestData")
	public void TC003_AddressBook_TypeOfServiceProvider_Script(Hashtable<String, String> data) throws Throwable{
		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		
		//Click on Address book tab
		homePageLib.navigateToSearchInAddressBookMenu();
		
		//Search for address
		SearchAddressBookPageLib searchAddressBookPageLib = new SearchAddressBookPageLib(driver, reporter);
		searchAddressBookPageLib.setType(data.get("searchType"));
		searchAddressBookPageLib.setServiceState(data.get("serviceState"));
		searchAddressBookPageLib.clickSearch();
		
		//verify search results
		searchAddressBookPageLib.verifyNumberOfSearchResults();
		
		status = true;		 		
	}
	
}
