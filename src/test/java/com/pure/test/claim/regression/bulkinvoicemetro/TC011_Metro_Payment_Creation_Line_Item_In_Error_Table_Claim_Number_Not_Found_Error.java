package com.pure.test.claim.regression.bulkinvoicemetro;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.PropertyLiabilityPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.ActivitiesPageLib;
import com.pure.claim.lifeCycle.ApprovalPageLib;
import com.pure.claim.lifeCycle.BatchProcessInfoPageLib;
import com.pure.claim.lifeCycle.BulkInvoiceDetailsPageLib;
import com.pure.claim.lifeCycle.BulkInvoicePageLib;
import com.pure.claim.lifeCycle.ChecksPageLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.QuickCheckBasicsPageLib;
import com.pure.claim.lifeCycle.QuickChecksDetailsPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;


public class TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "LoginData");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_BulkInvoice() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "BulkInvoice");
	}
	
	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "Reserves");
	}
	
	private Object[][] getTestDataFor_QuickCheck() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "QuickCheck");
	}

	private Object[][] getTestDataFor_PropertyIncident() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "PropertyIncident");
	}
	
	private Object[][] getTestDataFor_Documents() {
		return TestUtil.getData("TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script", TestDataClaim, "Documents");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),getTestDataFor_ExposureDetails(),
				getTestDataFor_Reserves(),getTestDataFor_QuickCheck(),getTestDataFor_PropertyIncident(),getTestDataFor_Documents(),getTestDataFor_TestDetails(), getTestDataFor_BulkInvoice());
	}
	
	@Test(dataProvider = "getTestData")
	public void TC011_Metro_Payment_Creation_Line_Item_In_Error_Table_Claim_Number_Not_Found_Error_Script(Hashtable<String, String> data) throws Throwable{

		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		newClaimPageLib.clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setHowReported(data.get("howReported"));
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		addClaimInfoPageLib.AddPropertiesIncident(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.

		//-----Services---
		//Verify navigated to step4
		//click next
		newClaimPageLib.clickNext();

		//Fill adjuster details and note

		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		saveAndAssignClaimLib.setNote(data.get("additionalNote"));	
		//New Exposure addition
		saveAndAssignClaimLib.addNewExposure(data.get("policylevelcoverage"), data.get("exposuretype"),data.get("exposuretype2"));
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();
		
		new PropertyLiabilityPageLib(driver, reporter).fillDetails(data);
		
		newExposurePageLib.setIAUsed(data.get("iaused"));
		newExposurePageLib.setDeskAdjustment(data.get("deskadjustment"));
		newExposurePageLib.clickOK();

		//Submit the claim
		saveAndAssignClaimLib.clickFinish();    
		String claimNumber = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		new ClaimCreationSummaryPageLib(driver, reporter).clickNewlyCreatedClaim();

		//Verify the notes on summary page
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.verifyLatestNotes(data.get("additionalNote"));
		summaryOverviewPageLib.getNumberOfExposuresAdded();
		
		ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("AbilityToPay");
		
		//Add Reserve
		SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
		setReservesPageLib.addNewReserveFromActionsMenu(data);

		//QuickCheck basics		
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("ValidForISO");
		FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
		financialsPageLib.openQuickCheck();		

		QuickCheckBasicsPageLib quickCheckBasicsPageLib = new QuickCheckBasicsPageLib(driver, reporter);	
		quickCheckBasicsPageLib.setDeductibleType(data.get("deductibleType"));
		quickCheckBasicsPageLib.verifyDeductibleDecisionWhenOverrideDeductibleIsOnlyNo(data.get("WaivedOrApplied"),data.get("waivedReason"),data.get("otherComments"));
		quickCheckBasicsPageLib.fillDetails(data);
		//QuickCheck Details
		new QuickChecksDetailsPageLib(driver, reporter).fillDetails(data);

		homePageLib.navigateToBatchProcessInfo();
		
		BatchProcessInfoPageLib batchProcessInfoPageLib = new BatchProcessInfoPageLib(driver, reporter);
		batchProcessInfoPageLib.startAutomatedBulkInvoiceJob();
		
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("returnToClaimCenter");
		
		homePageLib.navigateToBulkInvoices();
		
		BulkInvoicePageLib bulkInvoicePageLib =new BulkInvoicePageLib(driver, reporter);
		bulkInvoicePageLib.selectUploadedCSVFile(data.get("bulkInvoiceFileName"));
		
		BulkInvoiceDetailsPageLib bulkInvoiceDetailsPageLib = new BulkInvoiceDetailsPageLib(driver, reporter);
		bulkInvoiceDetailsPageLib.addClaimNoForClaimNotFoundLineItem(claimNumber);
		bulkInvoiceDetailsPageLib.verifyClaimStatus(claimNumber, data.get("status1"));
		bulkInvoiceDetailsPageLib.clickClaimNo(claimNumber);
		
		financialsPageLib.openTransactions();
		
		financialsPageLib.verifyCreatedReserveAmount(data.get("costtype2"), "-");
		
		homePageLib.moveToDesktopMenu();
		homePageLib.navigateToBulkInvoices();
		
		bulkInvoicePageLib.selectUploadedCSVFile(data.get("bulkInvoiceFileName"));
		
		bulkInvoiceDetailsPageLib.clickSubmit();
		bulkInvoiceDetailsPageLib.verifyApprovalHistoryStatus(data.get("status2"));
		bulkInvoiceDetailsPageLib.clickUptoBulkInvoices();
		bulkInvoicePageLib.verifyStatus(data.get("bulkInvoiceFileName"), data.get("invoicestatus"));//in review
		homePageLib.logout();
		homePageLib.login(data.get("approverusername"), data.get("approverpassword"));
		homePageLib.openActivities();
		
		ActivitiesPageLib activitiesPageLib = new ActivitiesPageLib(driver, reporter);
		activitiesPageLib.selectActivityBasedOnInvoice(data.get("bulkInvoiceFileName"));
		new ApprovalPageLib(driver, reporter).clickApprove();
		bulkInvoiceDetailsPageLib.verifyClaimStatus(data.get("claimNo"), data.get("status1"));
		
		bulkInvoiceDetailsPageLib.clickClaimNo(data.get("claimNo"));
		financialsPageLib.openChecks();
		ChecksPageLib checksPageLib = new ChecksPageLib(driver, reporter);
		checksPageLib.verifyBulkInvoiceDetails(data.get("bulkInvoiceAmount"), data.get("bulkInvoiceStatus"));
		financialsPageLib.openTransactions();
		financialsPageLib.verifyBulkInvoicePaymentDetails(data.get("costtype"), data.get("bulkInvoiceAmount"), data.get("bulkInvoiceStatus"), data.get("createdUser1"));
		financialsPageLib.verifyCreatedReserveUserAndAmount(data.get("costtype"), data.get("bulkInvoiceAmount"), data.get("createdUser1"));
		financialsPageLib.verifyCreatedReserveUserAndAmount(data.get("costtype"), data.get("bulkInvoiceAmount"), data.get("createdUser2"));
		
		status = true;
	}
	
}
