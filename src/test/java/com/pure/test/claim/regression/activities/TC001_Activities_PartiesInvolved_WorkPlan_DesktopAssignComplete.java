package com.pure.test.claim.regression.activities;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.ActivitiesPageLib;
import com.pure.claim.lifeCycle.AssignActivitiesPageLib;
import com.pure.claim.lifeCycle.NewPersonPageLib;
import com.pure.claim.lifeCycle.PartiesInvolvedContactsPageLib;
import com.pure.claim.lifeCycle.PartiesInvolvedPageLib;
import com.pure.claim.lifeCycle.WorkPlanPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;

public class TC001_Activities_PartiesInvolved_WorkPlan_DesktopAssignComplete extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC001_Activities_PartiesInvolved_WorkPlan_DesktopAssignComplete_Script", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_Activities_PartiesInvolved_WorkPlan_DesktopAssignComplete_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC001_Activities_PartiesInvolved_WorkPlan_DesktopAssignComplete_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC001_Activities_PartiesInvolved_WorkPlan_DesktopAssignComplete_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC001_Activities_PartiesInvolved_WorkPlan_DesktopAssignComplete_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC001_Activities_PartiesInvolved_WorkPlan_DesktopAssignComplete_Script", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC001_Activities_PartiesInvolved_WorkPlan_DesktopAssignComplete_Script", TestDataClaim, "VehicleIncident");
	}
	
	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC001_Activities_PartiesInvolved_WorkPlan_DesktopAssignComplete_Script", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_NewPerson() {
		return TestUtil.getData("TC001_Activities_PartiesInvolved_WorkPlan_DesktopAssignComplete_Script", TestDataClaim, "NewPerson");
	}
	

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_TestDetails(), getTestDataFor_Login(), getTestDataFor_NewPerson(),
				getTestDataFor_GeneralInfo(), getTestDataFor_BasicInfo(), getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_VehicleIncident(), getTestDataFor_Exposures());
	}

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	@Test(dataProvider = "getTestData")
	public void TC001_Activities_PartiesInvolved_WorkPlan_DesktopAssignComplete_Script(Hashtable<String, String> data) throws Throwable{

		/****************Claim Creation*****************/
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver,reporter).clickNext();		
		
		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		VerifiedPolicyBasicInfoLib.selectInvolvedVehicle(data.get("policylevelcoverage"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver,reporter);
		addClaimInfoPageLib.enterVehiclesInformation(data);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.
		
		//Fill adjuster details and note
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();
		String claimNo = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		
		
		//Search for a claim
		homePageLib.searchClaim(claimNo);
		
		/****************TC001_Activities_Parties involved screen****************/
		reporter.createHeader("TC001_Activities_Parties involved screen");
		//click Parties Involved tab from left side menu
		PartiesInvolvedPageLib partiesInvolvedPageLib = new PartiesInvolvedPageLib(driver, reporter);
		partiesInvolvedPageLib.openPartiesInvolved();
		
		//Navigate to New Person Page
		PartiesInvolvedContactsPageLib partiesInvolvedContactsPageLib = new PartiesInvolvedContactsPageLib(driver, reporter);
		partiesInvolvedContactsPageLib.clickNewPerson();
		
		//Fill all details
		NewPersonPageLib newPersonPageLib = new NewPersonPageLib(driver, reporter);
		newPersonPageLib.fillDetails(data);
		newPersonPageLib.clickAddRole();
		newPersonPageLib.setRole(data.get("role"));
		newPersonPageLib.clickUpdate();
		
		//Verify newly added contact in Parties Involved list
		partiesInvolvedContactsPageLib.verifyNewlyAddedContactInPartiesInvoled(data.get("firstName"), data.get("lastName"));
		
		/****************TC002_Activities_Claim_Work plan****************/
		reporter.createHeader("TC002_Activities_Claim_Work plan");
		WorkPlanPageLib workPlanPageLib = new WorkPlanPageLib(driver, reporter);	
		String[] defaultActivities = workPlanPageLib.defaultWorkplanActities();
		workPlanPageLib.verifyPresenceOfActivitySubject(defaultActivities);
		
		/****************TC004_Activities_Desktop_Type of Activities****************/
		reporter.createHeader("TC004_Activities_Desktop_Type of Activities");
		homePageLib.navigateToActivitesInDesktopMenu();
		ActivitiesPageLib activitiesPageLib = new ActivitiesPageLib(driver, reporter);
		activitiesPageLib.filterActivities("All open");
		String openClaim = activitiesPageLib.getActivityClaim();
		activitiesPageLib.filterActivities("Closed in last 30 days");
		String closedClaim = activitiesPageLib.getActivityClaim();
		activitiesPageLib.verifyPresenceOfActivityClaim(openClaim);
		activitiesPageLib.filterActivities("All open");
		activitiesPageLib.verifyPresenceOfActivityClaim(closedClaim);
		
		/****************TC005_Activities_Desktop_Activities_Assign****************/
		reporter.createHeader("TC005_Activities_Desktop_Activities_Assign");
		activitiesPageLib.filterActivities("My activities today");
		activitiesPageLib.sortRecentActivity(claimNo);
		activitiesPageLib.selectActivity(claimNo);
		activitiesPageLib.clickAssign();
		
		AssignActivitiesPageLib assignActivitiesPageLib = new AssignActivitiesPageLib(driver, reporter);
		assignActivitiesPageLib.assignActivity(data.get("assignClaimTo"));
		
		/****************TC006_Activities_Desktop_Completion of Activities****************/
		reporter.createHeader("TC006_Activities_Desktop_Completion of Activities");
		activitiesPageLib.selectActivity(claimNo);
		activitiesPageLib.clickComplete();
		activitiesPageLib.verifyPresenceOfActivityClaim(claimNo);

		status = true;		 		
	}

	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		//Qtest.UpdateQTestResults(status, "Verify that the new Person can be added from the Parties involved screen", startTime, endTime);
	}
}

