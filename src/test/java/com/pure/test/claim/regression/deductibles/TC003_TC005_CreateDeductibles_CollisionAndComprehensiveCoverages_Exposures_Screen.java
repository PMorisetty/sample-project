package com.pure.test.claim.regression.deductibles;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.SecondaryApprovalPageLib;
import com.pure.claim.lifeCycle.CloseExposurePageLib;
import com.pure.claim.lifeCycle.ExposureDetailsPageLib;
import com.pure.claim.lifeCycle.ExposuresPageLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.QuickCheckBasicsPageLib;
import com.pure.claim.lifeCycle.QuickChecksDetailsPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.lifeCycle.VehicleIncidentPageLib;
import com.pure.claim.lifeCycle.WorkPlanPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;

public class TC003_TC005_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC003_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC003_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC003_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC003_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC003_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen_Script", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC003_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC003_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen_Script", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC003_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen_Script", TestDataClaim, "VehicleIncident");
	}
	
	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC003_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen_Script", TestDataClaim, "Reserves");
	}
	
	private Object[][] getTestDataFor_QuickCheck() {
		return TestUtil.getData("TC003_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen_Script", TestDataClaim, "QuickCheck");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_TestDetails(), getTestDataFor_Exposures(), getTestDataFor_VehicleIncident(),
				getTestDataFor_Reserves(), getTestDataFor_QuickCheck());
	}

	
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	
	@Test(dataProvider = "getTestData")
	public void TC003_CreateDeductibles_CollisionAndComprehensiveCoverages_Exposures_Screen_Script(Hashtable<String, String> data) throws Throwable{
		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		/**********************Claim Creation*********************/
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver,reporter).clickNext();		
		
		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver,reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.
		
		//Fill adjuster details and note
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();
		ClaimCreationSummaryPageLib claimCreationSummaryPageLib = new ClaimCreationSummaryPageLib(driver, reporter);
		String claimNo = claimCreationSummaryPageLib.getClaimNumber();
		
		//Search for a claim
		homePageLib.searchClaim(claimNo);
		
		/**********************Create Exposure for collision coverage type*********************/
		ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ChooseByCoverage");
		actionsPageLib.clickOnVehicleInvolved(data.get("coverageType"));
		actionsPageLib.clickOn(data.get("coverageType1"));
		
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();
		
		VehicleIncidentPageLib vehicleIncidentPageLib = new VehicleIncidentPageLib(driver, reporter);
		vehicleIncidentPageLib.enterDetails(data.get("damageDescription"), data.get("driverName"), data.get("lossoccured"));
		newExposurePageLib.clickUpdate();
		
		/**********************Create Exposure for Comprehensive coverage type*********************/
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ChooseByCoverage");
		actionsPageLib.clickOnVehicleInvolved(data.get("coverageType"));
		actionsPageLib.clickOn(data.get("coverageType2"));
		
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();
		
		vehicleIncidentPageLib.enterDetails(data.get("damageDescription"), data.get("driverName"), data.get("lossoccured"));
		newExposurePageLib.clickUpdate();
		
		/**************************Verify deductibles in summary page*************************/
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.openSummaryPage();
		summaryOverviewPageLib.verifyDeductible(data.get("coverageSubType1"), data.get("deductibleAmount"), "Not Applied");
		summaryOverviewPageLib.verifyDeductible(data.get("coverageSubType2"), data.get("deductibleAmount"), "Not Applied");
	
		/**********************Create Reserve for collision coverage type*********************/
		ExposuresPageLib exposuresPageLib = new ExposuresPageLib(driver, reporter);
		exposuresPageLib.selectExposureBasedOnCoverage(data.get("coverageSubType1"));
		exposuresPageLib.clickCreateReserve();
		
		//Add reserves
		SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
		setReservesPageLib.enterReserveDetails(data.get("reporterName"), data.get("costtype"), data.get("costcategory"), data.get("newavailablereserve"));
		setReservesPageLib.clickSave();
		
		/**********************Create Reserve for Comprehensive coverage type*********************/
		summaryOverviewPageLib.openExposuresPage();
		exposuresPageLib.selectExposureBasedOnCoverage(data.get("coverageSubType2"));
		exposuresPageLib.clickCreateReserve();
		
		//Add reserves
		setReservesPageLib.enterReserveDetails(data.get("reporterName"), data.get("costtype"), data.get("costcategory"), data.get("newavailablereserve"));
		setReservesPageLib.clickSave();
		
		/**********************Collision coverages_Quick check*********************/
		reporter.createHeader("Quick check wizard for Collision coverage");
		FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
		financialsPageLib.openQuickCheckBasedOnCoverage(data.get("coverageSubType1"));
		
		QuickCheckBasicsPageLib quickCheckBasicsPageLib = new QuickCheckBasicsPageLib(driver, reporter);		
		quickCheckBasicsPageLib.verifyDeductibleDecisionWhenOverrideDeductibleIsOnlyNo(data.get("WaivedOrApplied1"),data.get("waivedReason"),data.get("otherComments"));
		quickCheckBasicsPageLib.fillDetails(data);
		new QuickChecksDetailsPageLib(driver, reporter).fillDetails(data);
		
		summaryOverviewPageLib.openSummaryPage();
		summaryOverviewPageLib.verifyDeductible(data.get("coverageSubType1"), data.get("deductibleAmount"), data.get("WaivedOrApplied1"));
		
		/*******************Comprehensive Coverage_Quick Check********/
		reporter.createHeader("Quick check wizard for Comprehensive coverage");
		financialsPageLib.openFinancials();
		financialsPageLib.openQuickCheckBasedOnCoverage(data.get("coverageSubType2"));
		
		quickCheckBasicsPageLib.verifyDeductibleDecisionWhenOverrideDeductibleIsOnlyNo(data.get("WaivedOrApplied2"),data.get("waivedReason"),data.get("otherComments"));
		quickCheckBasicsPageLib.fillDetails(data);
		new QuickChecksDetailsPageLib(driver, reporter).fillDetails(data);
		
		summaryOverviewPageLib.openSummaryPage();
		summaryOverviewPageLib.verifyDeductible(data.get("coverageSubType2"), data.get("deductibleAmount"), data.get("WaivedOrApplied2"));
		
		summaryOverviewPageLib.openExposuresPage();
		exposuresPageLib.openExposureBasedOnCoverage(data.get("coverageSubType1"));
		
		ExposureDetailsPageLib exposureDetailsPageLib = new ExposureDetailsPageLib(driver, reporter);
		exposureDetailsPageLib.clickEdit();
		exposureDetailsPageLib.verifyDeductibleManagementWorkSheet(data.get("WaivedOrApplied1"), data.get("deductibleAmount"), data.get("waivedReason"), data.get("otherComments"), data.get("overrideDeductible"));
		exposureDetailsPageLib.clickCancel();
		exposureDetailsPageLib.clickUpToExposures();
		
		exposuresPageLib.openExposureBasedOnCoverage(data.get("coverageSubType2"));
		exposureDetailsPageLib.clickEdit();
		exposureDetailsPageLib.verifyDeductibleManagementWorkSheet(data.get("WaivedOrApplied2"), data.get("deductibleAmount"), data.get("waivedReason"), data.get("otherComments"), data.get("ovverideDeductible"));
		exposureDetailsPageLib.clickCancel();
		exposureDetailsPageLib.clickUpToExposures();
		
		/******TC005_Verify_Create_Deductible_Collision_Comprehensive_Coverages_Close_Exposure*******/
		homePageLib.logout();
		homePageLib.login(data.get("secondaryapprovalusername"), data.get("secondaryapprovalpassword"));
		homePageLib.openSecondaryApproval();
		new SecondaryApprovalPageLib(driver, reporter).ApproveSelectedRow(claimNo);
		homePageLib.logout();
			
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.searchClaim(claimNo);
		
		summaryOverviewPageLib.openWorkPlanPage();
		
		new WorkPlanPageLib(driver, reporter).completeAllActivities();
		
		summaryOverviewPageLib.openExposuresPage();
		
		exposuresPageLib.selectExposureBasedOnCoverage(data.get("coverageSubType1"));
		exposuresPageLib.clickCloseExposure();
		
		CloseExposurePageLib closeExposurePageLib = new CloseExposurePageLib(driver, reporter);
		closeExposurePageLib.verifyDeductibleManagementWorkSheet(data.get("WaivedOrApplied1"), data.get("deductibleAmount"), data.get("waivedReason"), data.get("otherComments"), data.get("overrideDeductible"));
		closeExposurePageLib.fillDetails(data);
		closeExposurePageLib.clickClose();
		closeExposurePageLib.clickAwaitingOk();
		
		exposuresPageLib.verifyExposureStatus(data.get("coverageSubType1"), data.get("exposureStatus1"));
		
		exposuresPageLib.selectExposureBasedOnCoverage(data.get("coverageSubType2"));
		exposuresPageLib.clickCloseExposure();
		
		closeExposurePageLib.verifyDeductibleManagementWorkSheet(data.get("WaivedOrApplied2"), data.get("deductibleAmount"), data.get("waivedReason"), data.get("otherComments"), data.get("overrideDeductible"));
		closeExposurePageLib.fillDetails(data);
		closeExposurePageLib.clickClose();
		closeExposurePageLib.clickAwaitingOk();
		
		exposuresPageLib.verifyExposureStatus(data.get("coverageSubType2"), data.get("exposureStatus2"));
		
		status = true;
		
	}
	
	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		//Qtest.UpdateQTestResults(status, "Verify deductibles creation at coverage level for collision and comprehensive coverage", startTime, endTime);
	}
}

