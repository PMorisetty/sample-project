package com.pure.test.claim.regression.fnol;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.ReviewAndSaveClaimPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;


public class TC012_Verified_Claim_ExcessLiability extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC012_Verified_Claim_ExcessLiability_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC012_Verified_Claim_ExcessLiability_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC012_Verified_Claim_ExcessLiability_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC012_Verified_Claim_ExcessLiability_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC012_Verified_Claim_ExcessLiability_Script", TestDataClaim, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC012_Verified_Claim_ExcessLiability_Script", TestDataClaim, "TestDetails");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_TestDetails());
	}

	@Test(dataProvider = "getTestData")
	public void TC012_Verified_Claim_ExcessLiability_Script(Hashtable<String, String> data) throws Throwable{

		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		newClaimPageLib.clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 8: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));

		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 8: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();

		//Verify navigated to step4
		//newClaimPageLib.verifyTitle("Step 4 of 8: Manage exposures");
		
		newClaimPageLib.clickNext();

		//Verify navigated to step5
		//newClaimPageLib.verifyTitle("Step 5 of 8: Manage parties involved");
		reporter.SuccessReport("Page opened", "Step 5 of 8: Manage parties involved");
		newClaimPageLib.clickNext();

		//Verify navigated to step6
		//newClaimPageLib.verifyTitle("Step 6 of 8: Services");		
		//newClaimPageLib.clickNext();
		//Verify navigated to step7
		//newClaimPageLib.verifyTitle("Step 7 of 8: Save and Assign Claim");
		reporter.SuccessReport("Page opened", "Step 7 of 8: Save and Assign Claim");
		//Fill adjuster details and note		
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		//saveAndAssignClaimLib.setNote(data.get("additionalNote"));

		newClaimPageLib.clickNext();

		//Verify navigated to step8
		//newClaimPageLib.verifyTitle("Step 8 of 8: Review and Save Claim");

		new ReviewAndSaveClaimPageLib(driver, reporter).clickFinish();

		new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		
		status = true;
	}

}
