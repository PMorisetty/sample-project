package com.pure.test.claim.regression.notes;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewNotePageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.NotesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.Qtest;
import com.pure.utilities.TestUtil;

public class TC001_NotesVerificationInActionsCancelUseTemplate extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC001_NotesVerificationInActionsCancelUseTemplate_Script", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_NotesVerificationInActionsCancelUseTemplate_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_ClaimSearch() {
		return TestUtil.getData("TC001_NotesVerificationInActionsCancelUseTemplate_Script", TestDataClaim, "ClaimSearch");
	}

	private Object[][] getTestDataFor_NewNotes() {
		return TestUtil.getData("TC001_NotesVerificationInActionsCancelUseTemplate_Script", TestDataClaim, "NewNotes");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC001_NotesVerificationInActionsCancelUseTemplate_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC001_NotesVerificationInActionsCancelUseTemplate_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC001_NotesVerificationInActionsCancelUseTemplate_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC001_NotesVerificationInActionsCancelUseTemplate_Script", TestDataClaim, "ClaimSubmission");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_TestDetails(), getTestDataFor_Login(), getTestDataFor_ClaimSearch(),
				getTestDataFor_NewNotes(), getTestDataFor_GeneralInfo(), getTestDataFor_BasicInfo(), getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission());
	}

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	@Test(dataProvider = "getTestData")
	public void TC001_NotesVerificationInActionsCancelUseTemplate_Script(Hashtable<String, String> data) throws Throwable{

		/****************Claim Creation*****************/
		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver, reporter).clickNext();		

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.

		//click next
		newClaimPageLib.clickNext();

		//Fill adjuster details and note

		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));

		//Submit the claim
		saveAndAssignClaimLib.clickFinish();    
		String cliamNo = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		
		//Search for a claim
		homePageLib.searchClaim(cliamNo);
		
		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		NewNotePageLib newNotePageLib = new NewNotePageLib(driver, reporter);
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		NotesPageLib notesPageLib = new NotesPageLib(driver, reporter);
				
		/****************TC002_Notes_Note Work space_Cancel button*****************/
		reporter.createHeader("TC002_Notes_Note Work space_Cancel button");
		//click on Notes from Actions Menu
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("Note");

		//Enter all required fields
		newNotePageLib.setShareOnMemberPortal(data.get("shareOnMemberPortal"));
		newNotePageLib.setTopic(data.get("topic"));
		newNotePageLib.setConfidential(data.get("confidential"));
		newNotePageLib.setText(data.get("text"));

		//click cancel button
		newNotePageLib.clickCancel();

		//Check for invisibility of notes
		summaryOverviewPageLib.checkPresenceOfNotes(data.get("text"));
		
		/****************TC001_Notes_Actions_Create of Notes*****************/
		reporter.createHeader("TC001_Notes_Actions_Create of Notes");
		//navigate to new note
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("Note");

		//Enter all required fields
		newNotePageLib.setShareOnMemberPortal(data.get("shareOnMemberPortal"));
		newNotePageLib.setTopic(data.get("topic"));
		newNotePageLib.setConfidential(data.get("confidential"));
		newNotePageLib.setText(data.get("text"));

		//click update button
		newNotePageLib.clickUpdate(data.get("text"));

		//verify created notes in summary page
		summaryOverviewPageLib.verifyLatestNotes(data.get("text"));

		//Verify created notes in Notes page
		summaryOverviewPageLib.openNotesPage();
		notesPageLib.verifyNotesInfo(data);
		
		/****************TC003_Notes_Note Work space_Use Template*****************/
		reporter.createHeader("TC003_Notes_Note Work space_Use Template");
		//click on Notes from Actions Menu
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("Note");

		//Enter all required fields
		newNotePageLib.setShareOnMemberPortal(data.get("shareOnMemberPortal"));
		newNotePageLib.clickUseTemplate();
		newNotePageLib.setTopic(data.get("topic"));
		newNotePageLib.clicUseTemplateSearch();
		newNotePageLib.selectTemplate();
		String text = newNotePageLib.getUserTemplateText();

		//click update button
		newNotePageLib.clickUpdate(text);

		//verify created notes in summary page
		summaryOverviewPageLib.openSummaryPage();
		summaryOverviewPageLib.verifyLatestNotes(text);

		//Verify created notes in Notes page
		summaryOverviewPageLib.openNotesPage();
		notesPageLib.verifyNotesInfo(text);
		
		status = true;		 		
	}

	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		Qtest.UpdateQTestResults(status, "Verify that the ''Note'' can be created through the Action button", startTime, endTime);
	}
}

