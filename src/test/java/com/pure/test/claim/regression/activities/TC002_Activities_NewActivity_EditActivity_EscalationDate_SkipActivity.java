package com.pure.test.claim.regression.activities;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.ActivitiesPageLib;
import com.pure.claim.lifeCycle.NewActivityInClaimPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.lifeCycle.WorkPlanPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;

public class TC002_Activities_NewActivity_EditActivity_EscalationDate_SkipActivity extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC002_Activities_NewActivity_EditActivity_EscalationDate_SkipActivity_Script", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC002_Activities_NewActivity_EditActivity_EscalationDate_SkipActivity_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC002_Activities_NewActivity_EditActivity_EscalationDate_SkipActivity_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC002_Activities_NewActivity_EditActivity_EscalationDate_SkipActivity_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC002_Activities_NewActivity_EditActivity_EscalationDate_SkipActivity_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC002_Activities_NewActivity_EditActivity_EscalationDate_SkipActivity_Script", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC002_Activities_NewActivity_EditActivity_EscalationDate_SkipActivity_Script", TestDataClaim, "VehicleIncident");
	}
	
	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC002_Activities_NewActivity_EditActivity_EscalationDate_SkipActivity_Script", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_NewActivity() {
		return TestUtil.getData("TC002_Activities_NewActivity_EditActivity_EscalationDate_SkipActivity_Script", TestDataClaim, "NewActivity");
	}


	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_TestDetails(), getTestDataFor_Login(),
				getTestDataFor_GeneralInfo(), getTestDataFor_BasicInfo(), getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_VehicleIncident(), getTestDataFor_Exposures(), getTestDataFor_NewActivity());
	}

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	@Test(dataProvider = "getTestData")
	public void TC002_Activities_NewActivity_EditActivity_EscalationDate_SkipActivity_Script(Hashtable<String, String> data) throws Throwable{

		/****************Claim Creation*****************/
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver,reporter).clickNext();		
		
		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		VerifiedPolicyBasicInfoLib.selectInvolvedVehicle(data.get("policylevelcoverage"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver,reporter);
		addClaimInfoPageLib.enterVehiclesInformation(data);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.
		
		//Fill adjuster details and note
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();    
		String claimNo = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		
		/****************TC007_Activities_Action_New Activities****************/
		reporter.createHeader("TC007_Activities_Action_New Activities");
		//Search for a claim
		homePageLib.searchClaim(claimNo);
			
		//Navigate to SendClaimAcknowledgementLetter
		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("correspondence");
		actionsPageLib.clickOn("SendClaimAcknowledgementLetter");
		
		//Enter details in new activity page
		NewActivityInClaimPageLib newActivityInClaimPageLib = new NewActivityInClaimPageLib(driver, reporter);
		String subject = newActivityInClaimPageLib.getSubject();
		newActivityInClaimPageLib.setDescription(data.get("description"));
		newActivityInClaimPageLib.clickUpdate();
		
		//verify presence of activity
		WorkPlanPageLib workPlanPageLib = new WorkPlanPageLib(driver, reporter);
		workPlanPageLib.verifyPresenceOfActivitySubject(subject, "True");
		
		/****************TC008_Activities_Action_Edit Activities****************/
		reporter.createHeader("TC008_Activities_Action_Edit Activities");
		//Search for a claim
		homePageLib.searchClaim(claimNo);
		
		homePageLib.navigateToActivitesInDesktopMenu();
		ActivitiesPageLib activitiesPageLib = new ActivitiesPageLib(driver, reporter);
		activitiesPageLib.sortRecentActivity(claimNo);
		activitiesPageLib.clickOnSubjectLinkOfActivity(claimNo);
		
		newActivityInClaimPageLib.setSubject(data.get("subject"));
		newActivityInClaimPageLib.clickUpdate();
		
		//verify presence of updated activity subject
		workPlanPageLib.verifyPresenceOfActivitySubject(data.get("subject"), "True");
		
		/****************TC009_Activities_Work Plan_Activities-Escalation Dates****************/
		reporter.createHeader("TC009_Activities_Work Plan_Activities-Escalation Dates");
		
		//Search for a claim
		homePageLib.searchClaim(claimNo);
		
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.openWorkPlanPage();
		
		workPlanPageLib.clickOnSubjectLink(data.get("subject"));
		newActivityInClaimPageLib.verifyEscalationDate();
		newActivityInClaimPageLib.clickCancel();
		
		/****************TC011_Activities_Work Plan_Activities-Skip Activity****************/
		reporter.createHeader("TC011_Activities_Work Plan_Activities-Skip Activity");
		//Search for a claim
		homePageLib.searchClaim(claimNo);
		
		summaryOverviewPageLib.openWorkPlanPage();
		workPlanPageLib.selectWorkPlanActivityBasedOnSubject(data.get("subject"));
		workPlanPageLib.clickSkip();
		
		workPlanPageLib.verifyPresenceOfActivitySubject(data.get("subject"), "False");
		
		status = true;		 		
	}

	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		//Qtest.UpdateQTestResults(status, "Verify that the newly creaated and edited Activities can be viewed in work plan screen", startTime, endTime);
	}
}

