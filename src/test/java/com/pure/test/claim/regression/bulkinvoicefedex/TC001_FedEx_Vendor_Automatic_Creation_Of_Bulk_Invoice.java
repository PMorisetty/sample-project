package com.pure.test.claim.regression.bulkinvoicefedex;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.lifeCycle.BatchProcessInfoPageLib;
import com.pure.claim.lifeCycle.BulkInvoiceDetailsPageLib;
import com.pure.claim.lifeCycle.BulkInvoicePageLib;
import com.pure.utilities.TestUtil;


public class TC001_FedEx_Vendor_Automatic_Creation_Of_Bulk_Invoice extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_FedEx_Vendor_Automatic_Creation_Of_Bulk_Invoice_Script", TestDataClaim, "LoginData");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC001_FedEx_Vendor_Automatic_Creation_Of_Bulk_Invoice_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_BulkInvoice() {
		return TestUtil.getData("TC001_FedEx_Vendor_Automatic_Creation_Of_Bulk_Invoice_Script", TestDataClaim, "BulkInvoice");
	}
	
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(),getTestDataFor_TestDetails(),
				getTestDataFor_BulkInvoice());
	}

	
	@Test(dataProvider = "getTestData")
	public void TC001_FedEx_Vendor_Automatic_Creation_Of_Bulk_Invoice_Script(Hashtable<String, String> data) throws Throwable{

		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		
		homePageLib.navigateToBatchProcessInfo();
		
		BatchProcessInfoPageLib batchProcessInfoPageLib = new BatchProcessInfoPageLib(driver, reporter);
		batchProcessInfoPageLib.startAutomatedBulkInvoiceJob();
		
		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("returnToClaimCenter");
		
		homePageLib.navigateToBulkInvoices();
		
		new BulkInvoicePageLib(driver, reporter).selectUploadedCSVFile(data.get("bulkInvoiceFileName"));
		new BulkInvoiceDetailsPageLib(driver, reporter).verifyTitle();
		
		status = true;		 		
	}
}
