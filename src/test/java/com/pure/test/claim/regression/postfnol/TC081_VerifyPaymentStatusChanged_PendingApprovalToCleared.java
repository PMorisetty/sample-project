package com.pure.test.claim.regression.postfnol;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.PropertyLiabilityPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.SecondaryApprovalPageLib;
import com.pure.claim.lifeCycle.ChecksPageLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.QuickCheckBasicsPageLib;
import com.pure.claim.lifeCycle.QuickChecksDetailsPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.Qtest;
import com.pure.utilities.TestUtil;


public class TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared", TestDataClaim, "Reserves");
	}
	
	private Object[][] getTestDataFor_PropertyIncident() {
		return TestUtil.getData("TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared", TestDataClaim, "PropertyIncident");
	}
	
	private Object[][] getTestDataFor_PaymentaInfoDetails() {
		return TestUtil.getData("TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared", TestDataClaim, "PaymentaInfoDetails");
	}
	
	private Object[][] getTestDataFor_QuickCheck() {
		return TestUtil.getData("TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared", TestDataClaim, "QuickCheck");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_TestDetails(),getTestDataFor_Exposures(), getTestDataFor_Reserves(),
				getTestDataFor_PropertyIncident(), getTestDataFor_PaymentaInfoDetails(), getTestDataFor_QuickCheck());
	}

	@Test(dataProvider = "getTestData")
	public void TC081_VerifyPaymentStatusChanged_PendingApprovalToCleared_Script(Hashtable<String, String> data) throws Throwable{

		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver, reporter).clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.

		//-----Services---
		//Verify navigated to step4
		//click next
		newClaimPageLib.clickNext();

		//Fill adjuster details and note

		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		saveAndAssignClaimLib.setNote(data.get("additionalNote"));	
		//New Exposure addition
		saveAndAssignClaimLib.addNewExposure(data.get("policylevelcoverage"), data.get("exposuretype"),data.get("exposuretype2"));
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();
		
		new PropertyLiabilityPageLib(driver, reporter).fillDetails(data);
		
		newExposurePageLib.setIAUsed(data.get("iaused"));
		newExposurePageLib.setDeskAdjustment(data.get("deskadjustment"));
		newExposurePageLib.clickOK();
		
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();
		ClaimCreationSummaryPageLib claimCreationSummaryPageLib = new ClaimCreationSummaryPageLib(driver, reporter);
		String claimNo = claimCreationSummaryPageLib.getClaimNumber();
		claimCreationSummaryPageLib.clickNewlyCreatedClaim();
		
		//Verify the notes on summary page
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.verifyLatestNotes(data.get("additionalNote"));
		summaryOverviewPageLib.getNumberOfExposuresAdded();
		
		ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("AbilityToPay");
		
		//Add Reserve
		SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
		setReservesPageLib.addNewReserveFromActionsMenu(data);
		
		//QuickCheck basics		
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("ValidForISO");
		FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
		financialsPageLib.openFinancials();
		financialsPageLib.openQuickCheck();
		
		QuickCheckBasicsPageLib quickCheckBasicsPageLib = new QuickCheckBasicsPageLib(driver, reporter);
		quickCheckBasicsPageLib.setPaymentMethod(data.get("paymentMethod"));
		quickCheckBasicsPageLib.fillPaymentMethodDetails(data.get("reporterName"), data.get("accountType"), 
				data.get("accountNumber"), data.get("routingNumber"));
		quickCheckBasicsPageLib.setPaymentAmount(data.get("category"), data.get("paymentAmount"));
//		quickCheckBasicsPageLib.setDeductibleType(data.get("deductibleType"));
		quickCheckBasicsPageLib.verifyDeductibleDecisionWhenOverrideDeductibleIsOnlyNo(data.get("WaivedOrApplied"),data.get("waivedReason"),data.get("otherComments"));
		quickCheckBasicsPageLib.clickNext();
		QuickChecksDetailsPageLib quickChecksDetailsPageLib = new QuickChecksDetailsPageLib(driver, reporter);
		quickChecksDetailsPageLib.setShareOnMemberPortal(data.get("ShareOnMemberPortal"));
		quickChecksDetailsPageLib.AdvancedDirectPay(data.get("advanceddirectPay"), data.get("explainationfordirectpay"));
		quickChecksDetailsPageLib.clickFinish();
		
		//check the status of Quick Check added
		financialsPageLib.openChecks();		
		assertTextStringMatching(new ChecksPageLib(driver, reporter).getCheckStatus(),"Pending approval");
		homePageLib.logout();
		
		homePageLib.login(data.get("secondaryapprovalusername"), data.get("secondaryapprovalpassword"));
		summaryOverviewPageLib.openSecondaryApproverPage();
		new SecondaryApprovalPageLib(driver, reporter).ApproveSelectedRow(claimNo);
		homePageLib.logout();
		
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.searchClaim(claimNo);
		financialsPageLib.openFinancials();
		financialsPageLib.openChecks();
		assertTextStringMatching(new ChecksPageLib(driver, reporter).getCheckStatus(),"Awaiting submission");
		
		reporter.createHeader("Further steps - Verify the status of the payment after next batch run cannot be automated");
		status = true;
	}

}
