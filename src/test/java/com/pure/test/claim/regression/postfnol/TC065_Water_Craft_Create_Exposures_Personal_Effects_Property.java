package com.pure.test.claim.regression.postfnol;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.EnumLOBType;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.NewVesselPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.WatercraftIncidentPageLib;
import com.pure.claim.lifeCycle.ExposuresPageLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyBasicInformationPageLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyMainPageLib;
import com.pure.utilities.TestUtil;

public class TC065_Water_Craft_Create_Exposures_Personal_Effects_Property extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC065_Water_Craft_Create_Exposures_Personal_Effects_Property_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC065_Water_Craft_Create_Exposures_Personal_Effects_Property_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC065_Water_Craft_Create_Exposures_Personal_Effects_Property_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC065_Water_Craft_Create_Exposures_Personal_Effects_Property_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC065_Water_Craft_Create_Exposures_Personal_Effects_Property_Script", TestDataClaim, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_Coverages() {
		return TestUtil.getData("TC065_Water_Craft_Create_Exposures_Personal_Effects_Property_Script", TestDataClaim, "Coverages");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC065_Water_Craft_Create_Exposures_Personal_Effects_Property_Script", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC065_Water_Craft_Create_Exposures_Personal_Effects_Property_Script", TestDataClaim, "VehicleIncident");
	}
	
	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC065_Water_Craft_Create_Exposures_Personal_Effects_Property_Script", TestDataClaim, "Exposures");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_Coverages(),getTestDataFor_TestDetails(), getTestDataFor_VehicleIncident(),
				getTestDataFor_Exposures());
	}

	@Test(dataProvider = "getTestData")
	public void TC065_Water_Craft_Create_Exposures_Personal_Effects_Property_Script(Hashtable<String, String> data) throws Throwable{
		String policyNumber = "PW" + (int) (new Date().getTime()/10000);

		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectUnVerifiedPolicyOption();
		//Thread.sleep(2000);
		//Initial step in claim creation
		UnverifiedPolicyMainPageLib unverifiedPolicyMainPageLib = new UnverifiedPolicyMainPageLib(driver, reporter);
		unverifiedPolicyMainPageLib.setLOBType(EnumLOBType.Watercraft);
		unverifiedPolicyMainPageLib.setPolicyNumber(policyNumber);
		unverifiedPolicyMainPageLib.setDateOfLoss(data.get("dateOfLoss"));

		//Basic Info section
		unverifiedPolicyMainPageLib.setEffectiveDate(data.get("effectiveDate"));
		unverifiedPolicyMainPageLib.setExpirationDate(data.get("expirationDate"));
		//Insured details
		unverifiedPolicyMainPageLib.fillInsuredDetails(data);

		unverifiedPolicyMainPageLib.addPolicyLevelCoverages(data);
		
		// Add new vessel
		unverifiedPolicyMainPageLib.clickAddVessels();
		new NewVesselPageLib(driver, reporter).addVesselDetails(data);

		//navigate to next page
		newClaimPageLib.clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");

		//Reported by info
		UnverifiedPolicyBasicInformationPageLib unverifiedPolicyBasicInformationPageLib = new UnverifiedPolicyBasicInformationPageLib(driver, reporter);
		unverifiedPolicyBasicInformationPageLib.setReportedBy(data.get("reporterName"));
		unverifiedPolicyBasicInformationPageLib.setRelationToInsured(data.get("relationToInsured"));
		unverifiedPolicyBasicInformationPageLib.selectInvolvedVessel(data.get("vesselManufacturer"));

		//navigate to next page
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		
		addClaimInfoPageLib.addVesselIncident();
		
		WatercraftIncidentPageLib watercraftIncidentPageLib = new WatercraftIncidentPageLib(driver, reporter);
		watercraftIncidentPageLib.setInvolvedVehicle(data.get("watercraft"), data.get("lossParty"));
		watercraftIncidentPageLib.enterDetails(data.get("damagedescription"), data.get("reporterName"), data.get("lossoccured"));

		//navigate to next page
		newClaimPageLib.clickNext();

		//Verify navigated to step4
		//newClaimPageLib.verifyTitle("Step 4 of 4: Save and Assign Claim");

		newClaimPageLib.clickNext();

		//Fill adjuster details and note
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"),data.get("secondaryapprovalusername"));
		
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();
		ClaimCreationSummaryPageLib claimCreationSummaryPageLib = new ClaimCreationSummaryPageLib(driver, reporter);
		claimCreationSummaryPageLib.getClaimNumber();
		claimCreationSummaryPageLib.clickNewlyCreatedClaim();
		
		ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ChooseByCoverage");
		actionsPageLib.clickOn("policyLevelCoverage");
		actionsPageLib.clickOn(data.get("coverageType1"));
		
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.setPropertyDescription(data.get("propertyDescription"));
		newExposurePageLib.setDamageDescription(data.get("damageDescription"));
		
		newExposurePageLib.clickUpdate();

		new ExposuresPageLib(driver, reporter).verifyExposureSubType(data.get("coverageSubType"));
		
		status = true;
	}

}
