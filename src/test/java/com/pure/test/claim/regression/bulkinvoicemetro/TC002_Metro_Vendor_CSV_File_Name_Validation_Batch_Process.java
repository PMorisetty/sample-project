package com.pure.test.claim.regression.bulkinvoicemetro;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.lifeCycle.BatchProcessInfoPageLib;
import com.pure.claim.lifeCycle.BulkInvoicePageLib;
import com.pure.utilities.TestUtil;


public class TC002_Metro_Vendor_CSV_File_Name_Validation_Batch_Process extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC002_Metro_Vendor_CSV_File_Name_Validation_Batch_Process_Script", TestDataClaim, "LoginData");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC002_Metro_Vendor_CSV_File_Name_Validation_Batch_Process_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_BulkInvoice() {
		return TestUtil.getData("TC002_Metro_Vendor_CSV_File_Name_Validation_Batch_Process_Script", TestDataClaim, "BulkInvoice");
	}
	
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(),getTestDataFor_TestDetails(),
				getTestDataFor_BulkInvoice());
	}
	
	@Test(dataProvider = "getTestData")
	public void TC002_Metro_Vendor_CSV_File_Name_Validation_Batch_Process_Script(Hashtable<String, String> data) throws Throwable{

		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		
		homePageLib.navigateToBatchProcessInfo();
		
		BatchProcessInfoPageLib batchProcessInfoPageLib = new BatchProcessInfoPageLib(driver, reporter);
		batchProcessInfoPageLib.startAutomatedBulkInvoiceJob();
		
		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("returnToClaimCenter");
		
		homePageLib.navigateToBulkInvoices();
		
		new BulkInvoicePageLib(driver, reporter).navigateToLastPageAndVerifyFileIsNotDisplayed(data.get("bulkInvoiceFileName"));
		
		status = true;		 		
	}

}
