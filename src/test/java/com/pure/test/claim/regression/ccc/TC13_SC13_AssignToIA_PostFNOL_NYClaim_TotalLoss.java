package com.pure.test.claim.regression.ccc;


import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.InstructionsToVendorPageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.SearchForAppraisersPageLib;
import com.pure.claim.ServicesPageLib;
import com.pure.claim.lifeCycle.CheckDetailsPageLib;
import com.pure.claim.lifeCycle.ExposuresPageLib;
import com.pure.claim.lifeCycle.LossDetailsGeneralPageLib;
import com.pure.claim.lifeCycle.PartiesInvolvedPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.lifeCycle.VehicleIncidentPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.SOAPEngine;
import com.pure.utilities.TestUtil;


public class TC13_SC13_AssignToIA_PostFNOL_NYClaim_TotalLoss extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC13_SC13_AssignToIA_PostFNOL_NYClaim_TotalLoss", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC13_SC13_AssignToIA_PostFNOL_NYClaim_TotalLoss", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC13_SC13_AssignToIA_PostFNOL_NYClaim_TotalLoss", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC13_SC13_AssignToIA_PostFNOL_NYClaim_TotalLoss", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC13_SC13_AssignToIA_PostFNOL_NYClaim_TotalLoss", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC13_SC13_AssignToIA_PostFNOL_NYClaim_TotalLoss", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC13_SC13_AssignToIA_PostFNOL_NYClaim_TotalLoss", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC13_SC13_AssignToIA_PostFNOL_NYClaim_TotalLoss", TestDataClaim, "VehicleIncident");
	}
	
	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC13_SC13_AssignToIA_PostFNOL_NYClaim_TotalLoss", TestDataClaim, "Reserves");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(), getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_ExposureDetails(), getTestDataFor_TestDetails(), getTestDataFor_VehicleIncident(),getTestDataFor_Reserves());
	}

	
	
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	
	@Test(dataProvider = "getTestData")
	public void TC13_SC13_AssignToIA_PostFNOL_NYClaim_TotalLoss_Script(Hashtable<String, String> data) throws Throwable{

		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver,reporter).clickNext();		
		
		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setHowReported(data.get("howReported"));
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		VerifiedPolicyBasicInfoLib.selectInvolvedVehicle(data.get("policylevelcoverage"));
		VerifiedPolicyBasicInfoLib.setContactInfo("234-581-9589");
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver,reporter);
		addClaimInfoPageLib.clickVehicle(data.get("policylevelcoverage"));
		
		new VehicleIncidentPageLib(driver, reporter).enterDetails(data.get("damagedescription"), data.get("reporterName"), data.get("lossoccured"));
				
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.
		
		//Verify navigated to step4
		//newClaimPageLib.verifyTitle("Step 4 of 4: Save and Assign Claim");
		
		//Fill adjuster details and note
		
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"));  //  secondaryapprovalusername
		
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();
		
		ClaimCreationSummaryPageLib claimCreationSummaryPageLib = new ClaimCreationSummaryPageLib(driver, reporter);
		String claimNo = claimCreationSummaryPageLib.getClaimNumber();
		//String serviceReqNo = claimCreationSummaryPageLib.getServiceRequestNumber();
		claimCreationSummaryPageLib.clickNewlyCreatedClaim();
		
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.openLossDetailsPage();
		
		LossDetailsGeneralPageLib lossDetalsGeneralPageLib = new LossDetailsGeneralPageLib(driver, reporter);
		lossDetalsGeneralPageLib.clickEditButton();
		lossDetalsGeneralPageLib.clickVehicle();
		//lossDetalsGeneralPageLib.clickAddButton();
		
		VehicleIncidentPageLib vehicleIncidentPageLib = new VehicleIncidentPageLib(driver, reporter);
		vehicleIncidentPageLib.fillMethodOfInspectionDetails(data);
		//vehicleIncidentPageLib.enterDetails(data.get("damagedescription"), data.get("reporterName"), data.get("lossoccured"));
		
		SearchForAppraisersPageLib searchForAppraisersPageLib = new SearchForAppraisersPageLib(driver, reporter);
		searchForAppraisersPageLib.clickMethodOfInspection(data.get("methodOfInspection"));
		searchForAppraisersPageLib.selectAppraiserName(data.get("appraiserName"));
		
		InstructionsToVendorPageLib instructionsToVendorPageLib = new InstructionsToVendorPageLib(driver, reporter);
		instructionsToVendorPageLib.fillDetails(data);
		instructionsToVendorPageLib.clickUpdate();
		
		vehicleIncidentPageLib.clickOkBtn();
		lossDetalsGeneralPageLib.clickUpdateButton();
		
		summaryOverviewPageLib.openServicesPage();
		ServicesPageLib servicesPageLib = new ServicesPageLib(driver, reporter);
		String serviceReqNo = servicesPageLib.getServiceNumber();
			
		/**********************Create Exposure for collision coverage type*********************/
		ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ChooseByCoverage");
		actionsPageLib.clickOnVehicleInvolved(data.get("coverageType"));
		actionsPageLib.clickOn(data.get("coverageType1"));
		
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();
		
		//VehicleIncidentPageLib vehicleIncidentPageLib = new VehicleIncidentPageLib(driver, reporter);
		vehicleIncidentPageLib.enterDetails(data.get("damagedescription"), data.get("driverName"), data.get("lossoccured"));
		newExposurePageLib.clickUpdate();
		
		/**********************Create Reserve for collision coverage type*********************/
		ExposuresPageLib exposuresPageLib = new ExposuresPageLib(driver, reporter);
		exposuresPageLib.selectExposureBasedOnCoverage(data.get("coverageSubType1"));
		exposuresPageLib.clickCreateReserve();
		
		//Add reserves
		SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
		setReservesPageLib.enterReserveDetails(data.get("reporterName"), data.get("costtype"), data.get("costcategory"), data.get("newavailablereserve"));
		setReservesPageLib.clickSave();
		
		summaryOverviewPageLib.openServicesPage();
		//verify the status on summary page
		
		servicesPageLib.clickHistoryTab();
		servicesPageLib.clickDetailsTab();
		
		/**************************************************/
		/***********SOAP request to upload CSR document ***/
		/*******************************************************START*/		
		System.out.println("SOAP Request Starts here============");
		String claimID = claimNo+"-"+serviceReqNo;
		String endPoint = "https://ccmaint.purehnw.com/cc/ws/com/pure/webservices/CCC/returndata/DocumentSvc";
		String endPoint_event = "https://ccmaint.purehnw.com/cc/ws/com/pure/webservices/CCC/returndata/EventSvc";
		String xmlTemplateFilePath = System.getProperty("user.dir") + "/resources/CSR.xml";
		
		System.out.println("XMLFilepath locaion is ============"+xmlTemplateFilePath);
		
		//Prepare UniqueTransactionID, LossReferenceID for dynamic xml creation [residing in SOAPEngine's constructor
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("mes:UniqueTransactionID", claimID+"#01#2017-07-21T17:48:47.129Z");
		parameters.put("mes:LossReferenceID", claimID);
		
		System.out.println("Hashmap parameters are =========="+parameters);
		
		SOAPEngine se = new SOAPEngine(endPoint, xmlTemplateFilePath, parameters);
		se.execute();
		//String response = se.getNodeValueByXpath("/Description/text()");//TO get/read 'SUCCESS' message response string.
		String response = se.getSOAPResponseXML();
		System.out.println("SOAP response for CSR============/n"+response);
		/**********************************************************END*/
		
		servicesPageLib.clickHistoryTab();
		servicesPageLib.clickDetailsTab();
		Thread.sleep(15000);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.verifyLogsInHistory("Claim Summary Report document uploaded into onbase");
		
		/**************************************************/
		/***********SOAP request to upload DI document ***/
		/*******************************************************START*/	
		xmlTemplateFilePath = System.getProperty("user.dir") + "/resources/DI.xml";
		
		
		HashMap<String, String> parameters1 = new HashMap<String, String>();
		parameters1.put("mes:UniqueTransactionID", claimID+"#T01#2017-07-21T17:48:47.129Z");
		parameters1.put("mes:LossReferenceID", claimID);
		
		se = new SOAPEngine(endPoint, xmlTemplateFilePath, parameters1);
		se.execute();
//		response = se.getNodeValueByXpath("/Description/text()");
		response = se.getSOAPResponseXML();
		System.out.println("SOAP response for DI============/n"+response);
		//verify the status on summary page
		
		servicesPageLib.clickDetailsTab();
		Thread.sleep(15000);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.verifyLogsInHistory("Digital Image document uploaded into onbase");
		
		/**************************************************/
		/***********SOAP request to upload PI_Estimate document ***/
		/*******************************************************START*/			
		xmlTemplateFilePath = System.getProperty("user.dir") + "/resources/PI_Estimate.xml";
		
		HashMap<String, String> parameters2 = new HashMap<String, String>();
		parameters2.put("mes:UniqueTransactionID", claimID+"#D01#2017-07-21T17:48:47.129Z");
		parameters2.put("mes:LossReferenceID", claimID);
		
		se = new SOAPEngine(endPoint, xmlTemplateFilePath, parameters2);
		se.execute();
//		response = se.getNodeValueByXpath("/Description/text()");
		response = se.getSOAPResponseXML();
		System.out.println("SOAP response for PI_ESTIMATE============/n"+response);
		/**********************************************************END*/
		
		//verify the status on summary page
		servicesPageLib.clickDetailsTab();
		Thread.sleep(15000);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.verifyLogsInHistory("Estimate of Record document uploaded into onbase");
		
		
		/**************************************************/
		/***********SOAP request to upload QAAR document ***/
		/*******************************************************START*/			
		xmlTemplateFilePath = System.getProperty("user.dir") + "/resources/QAAR.xml";
		
		HashMap<String, String> param_QAAR = new HashMap<String, String>();
		param_QAAR.put("mes:UniqueTransactionID", claimID+"#E01#2017-07-21T17:48:47.129Z");
		param_QAAR.put("mes:LossReferenceID", claimID);
		
		se = new SOAPEngine(endPoint, xmlTemplateFilePath, param_QAAR);
		se.execute();
//		response = se.getNodeValueByXpath("/Description/text()");
		response = se.getSOAPResponseXML();
		System.out.println("SOAP response for QAAR============/n"+response);
		/**********************************************************END*/
		
		//verify the status on summary page
		servicesPageLib.clickDetailsTab();
		Thread.sleep(15000);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.verifyLogsInHistory("Audit report QAAR document uploaded into onbase");
		
		/**************************************************/
		/***********SOAP request to upload AADT document ***/
		/*******************************************************START*/			
		xmlTemplateFilePath = System.getProperty("user.dir") + "/resources/AADT.xml";
		
		HashMap<String, String> parameters_aadt = new HashMap<String, String>();
		parameters_aadt.put("UniqueTransactionID", claimID+"#k74#OQAREventTrigger#INTF_MSG_001#1501100048253#7746");
		parameters_aadt.put("LossReferenceID", claimID);
		
		se = new SOAPEngine(endPoint_event, xmlTemplateFilePath, parameters_aadt);
		se.execute();
//		response = se.getNodeValueByXpath("/Description/text()");
		response = se.getSOAPResponseXML();
		System.out.println("SOAP response for AADT============/n"+response);
		/**********************************************************END*/
		
		//verify the status on summary page
		servicesPageLib.clickDetailsTab();
		Thread.sleep(15000);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.verifyLogsInHistory("Appointment Date has been set (AADT)");
	
		
		/**************************************************/
		/***********SOAP request to upload EDSUM document ***/
		/*******************************************************START*/			
		xmlTemplateFilePath = System.getProperty("user.dir") + "/resources/EDSUM.xml";
		
		HashMap<String, String> param_EDSUM = new HashMap<String, String>();
		param_EDSUM.put("n11:UniqueTransactionID", "PVUG#"+claimID+"#E01#2017-07-21T17:48:47.129Z");
		param_EDSUM.put("n11:LossReferenceID", claimID);
		
		se = new SOAPEngine(endPoint_event, xmlTemplateFilePath, param_EDSUM);
		se.execute();
//		response = se.getNodeValueByXpath("/Description/text()");
		response = se.getSOAPResponseXML();
		System.out.println("SOAP response for EDSUM ============/n"+response);
		/**********************************************************END*/
		
		//verify the status on summary page
		servicesPageLib.clickDetailsTab();
		Thread.sleep(15000);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.verifyLogsInHistory("Estimate Data Summary (EDSUM E01) received, status moved to In Progress.");
	
		/**************************************************/
		/***********SOAP request to upload RPBLE document ***/
		/*******************************************************START*/			
		xmlTemplateFilePath = System.getProperty("user.dir") + "/resources/RPBLE.xml";
		
		HashMap<String, String> param_RPBLE = new HashMap<String, String>();
		param_RPBLE.put("UniqueTransactionID", "PVUG#"+claimID+"#01#hEOUTEvntkrigger#2801_MhG_001#1571000488953#7746");
		param_RPBLE.put("LossReferenceID", claimID);
		
		se = new SOAPEngine(endPoint_event, xmlTemplateFilePath, param_RPBLE);
		se.execute();
//		response = se.getNodeValueByXpath("/Description/text()");
		response = se.getSOAPResponseXML();
		System.out.println("SOAP response for RPBLE ============/n"+response);
		/**********************************************************END*/
		
		//verify the status on summary page
		servicesPageLib.clickDetailsTab();
		Thread.sleep(15000);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.verifyLogsInHistory("Vehicle Repairable (RPRBL)");
		
		summaryOverviewPageLib.openServicesPage();
		//-- Clicking on Activities tab
		servicesPageLib.clickActivitiesTab();
		//ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimOnly");
		actionsPageLib.clickOn("AbilityToPayClaimsOnly");
		
		servicesPageLib.clickClearBtn_ValidationResults();
		servicesPageLib.setClaimServiceActivity("All activities");
		
		servicesPageLib.clickEstimateReceivedLink();

		servicesPageLib.clickConformTotlaLossBtn();
		
		Thread.sleep(10000);
		servicesPageLib.clickHistoryTab();
		servicesPageLib.verifyLogsInHistory("Vehicle is confirmed as a Total Loss, CCC updated successfully (CNFTL)");
		
		status = true;
	}
	
	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		//Qtest.UpdateQTestResults(status, "Create Personal Auto claim for Verified Policy", startTime, endTime);
	}
}
