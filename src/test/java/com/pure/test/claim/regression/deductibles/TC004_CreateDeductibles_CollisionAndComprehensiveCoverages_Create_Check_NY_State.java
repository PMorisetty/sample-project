package com.pure.test.claim.regression.deductibles;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.CreateCheckInstructionsPageLib;
import com.pure.claim.lifeCycle.CreateCheckPageLib;
import com.pure.claim.lifeCycle.CreateCheckPaymentInformationPageLib;
import com.pure.claim.lifeCycle.ExposuresPageLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.NotesPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.lifeCycle.VehicleIncidentPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;

public class TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State_Script", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State_Script", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State_Script", TestDataClaim, "VehicleIncident");
	}
	
	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State_Script", TestDataClaim, "Reserves");
	}
	
	private Object[][] getTestDataFor_CreateCheck() {
		return TestUtil.getData("TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State_Script", TestDataClaim, "CreateCheck");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_TestDetails(), getTestDataFor_Exposures(), getTestDataFor_VehicleIncident(),
				getTestDataFor_Reserves(), getTestDataFor_CreateCheck());
	}
	
	@Test(dataProvider = "getTestData")
	public void TC004_CreateDeductibles_CollisionAndComprehensiveCoverages_Create_Check_NY_State_Script(Hashtable<String, String> data) throws Throwable{

		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		/**********************Claim Creation*********************/
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver,reporter).clickNext();		
		
		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver,reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.
		
		//Fill adjuster details and note
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();
		ClaimCreationSummaryPageLib claimCreationSummaryPageLib = new ClaimCreationSummaryPageLib(driver, reporter);
		String claimNo = claimCreationSummaryPageLib.getClaimNumber();
		
		//Search for a claim
		homePageLib.searchClaim(claimNo);
		
		/**********************Create Exposure for collision coverage type*********************/
		ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ChooseByCoverage");
		actionsPageLib.clickOnVehicleInvolved(data.get("coverageType"));
		actionsPageLib.clickOn(data.get("coverageType1"));
		
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();
		
		VehicleIncidentPageLib vehicleIncidentPageLib = new VehicleIncidentPageLib(driver, reporter);
		vehicleIncidentPageLib.enterDetails(data.get("damageDescription"), data.get("driverName"), data.get("lossoccured"));
		newExposurePageLib.clickUpdate();
		
		/**********************Create Exposure for Comprehensive coverage type*********************/
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ChooseByCoverage");
		actionsPageLib.clickOnVehicleInvolved(data.get("coverageType"));
		actionsPageLib.clickOn(data.get("coverageType2"));
		
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();
		
		vehicleIncidentPageLib.enterDetails(data.get("damageDescription"), data.get("driverName"), data.get("lossoccured"));
		newExposurePageLib.clickUpdate();
	
		/**********************Create Reserve for collision coverage type*********************/
		ExposuresPageLib exposuresPageLib = new ExposuresPageLib(driver, reporter);
		exposuresPageLib.selectExposureBasedOnCoverage(data.get("coverageSubType1"));
		exposuresPageLib.clickCreateReserve();
		
		//Add reserves
		SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
		setReservesPageLib.enterReserveDetails(data.get("reporterName"), data.get("costtype"), data.get("costcategory"), data.get("newavailablereserve"));
		setReservesPageLib.clickSave();
		
		/**********************Create Reserve for Comprehensive coverage type*********************/
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.openExposuresPage();
		exposuresPageLib.selectExposureBasedOnCoverage(data.get("coverageSubType2"));
		exposuresPageLib.clickCreateReserve();
		
		//Add reserves
		setReservesPageLib.enterReserveDetails(data.get("reporterName"), data.get("costtype"), data.get("costcategory"), data.get("newavailablereserve"));
		setReservesPageLib.clickSave();
		
		/**********************Collision coverages_Create check*********************/
		reporter.createHeader("Full check wizard for Collision coverage");
		FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
		financialsPageLib.openCreateCheckBasedOnCoverage(data.get("coverageSubType1"));
		
		CreateCheckPageLib createCheckPageLib = new CreateCheckPageLib(driver, reporter);
		createCheckPageLib.clickNext();
		
		CreateCheckPaymentInformationPageLib createCheckPaymentInformationPageLib = new CreateCheckPaymentInformationPageLib(driver, reporter);
		createCheckPaymentInformationPageLib.setPaymentAmount(data.get("paymentamount"));
		createCheckPaymentInformationPageLib.deductibleDecision(data.get("WaivedOrApplied"), data.get("waivedReason"), data.get("otherComments"));
		createCheckPaymentInformationPageLib.setOverrideDeductible(data.get("overrideDeductible"), data.get("overrideDeductibleAmount"), data.get("overrideReason"));
		createCheckPageLib.clickNext();
		
		new CreateCheckInstructionsPageLib(driver, reporter).fillDetails(data);
		createCheckPageLib.clickFinish();
		
		summaryOverviewPageLib.openSummaryPage();
		summaryOverviewPageLib.verifyDeductible(data.get("coverageSubType1"), data.get("overrideDeductibleAmount"), data.get("WaivedOrApplied"));
		
		summaryOverviewPageLib.openNotesPage();
		new NotesPageLib(driver, reporter).verifyDeductibleMsgInNote(data.get("coverageSubType1"), data.get("overrideDeductibleAmount"), data.get("WaivedOrApplied"), data.get("overrideDeductible"));
		
		/*******************Comprehensive Coverage_Create Check********/
		reporter.createHeader("Full check wizard for Comprehensive coverage");
		financialsPageLib.openFinancials();
		financialsPageLib.openCreateCheckBasedOnCoverage(data.get("coverageSubType2"));
		
		createCheckPageLib.clickNext();
		
		createCheckPaymentInformationPageLib.setPaymentAmount(data.get("paymentamount"));
		createCheckPaymentInformationPageLib.deductibleDecision(data.get("WaivedOrApplied"), data.get("waivedReason"), data.get("otherComments"));
		createCheckPaymentInformationPageLib.setOverrideDeductible(data.get("overrideDeductible"), data.get("overrideDeductibleAmount"), data.get("overrideReason"));
		createCheckPageLib.clickNext();
		
		new CreateCheckInstructionsPageLib(driver, reporter).fillDetails(data);
		createCheckPageLib.clickFinish();
		
		summaryOverviewPageLib.openSummaryPage();
		summaryOverviewPageLib.verifyDeductible(data.get("coverageSubType2"), data.get("overrideDeductibleAmount"), data.get("WaivedOrApplied"));
		
		summaryOverviewPageLib.openNotesPage();
		new NotesPageLib(driver, reporter).verifyDeductibleMsgInNote(data.get("coverageSubType2"), data.get("overrideDeductibleAmount"), data.get("WaivedOrApplied"), data.get("overrideDeductible"));
		
		status = true;		 		
	}
	
}

