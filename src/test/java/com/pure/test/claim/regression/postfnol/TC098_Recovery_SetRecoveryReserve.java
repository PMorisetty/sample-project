package com.pure.test.claim.regression.postfnol;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.ExposuresPageLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.SetRecoveryReservesPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.Qtest;
import com.pure.utilities.TestUtil;


public class TC098_Recovery_SetRecoveryReserve extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC098_Recovery_SetRecoveryReserve_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC098_Recovery_SetRecoveryReserve_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC098_Recovery_SetRecoveryReserve_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC098_Recovery_SetRecoveryReserve_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC098_Recovery_SetRecoveryReserve_Script", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC098_Recovery_SetRecoveryReserve_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC098_Recovery_SetRecoveryReserve_Script", TestDataClaim, "Exposures");
	}

	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC098_Recovery_SetRecoveryReserve_Script", TestDataClaim, "Reserves");
	}
	
		private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC098_Recovery_SetRecoveryReserve_Script", TestDataClaim, "VehicleIncident");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_TestDetails(), getTestDataFor_ExposureDetails(), getTestDataFor_Reserves(), 
			 getTestDataFor_VehicleIncident());
	}

	@Test(dataProvider = "getTestData")
	public void TC098_Recovery_SetRecoveryReserve_Script(Hashtable<String, String> data) throws Throwable{

		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		newClaimPageLib.clickNext();		
		
		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setHowReported(data.get("howReported"));
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		VerifiedPolicyBasicInfoLib.selectInvolvedVehicle(data.get("policylevelcoverage"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver,reporter);
		addClaimInfoPageLib.enterVehiclesInformation(data);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.
		
		//Verify navigated to step4
		//newClaimPageLib.verifyTitle("Step 4 of 4: Save and Assign Claim");
		
		//Fill adjuster details and note
		
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		saveAndAssignClaimLib.setNote(data.get("additionalNote"));	
		//New Exposure addition
		saveAndAssignClaimLib.addNewExposure(data.get("policylevelcoverage"), data.get("exposuretype"),data.get("exposuretype2"));
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.clickOK();
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();    
		String claimNumber = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		homePageLib.searchClaim(claimNumber);
		
		//Verify the notes on summary page
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.verifyLatestNotes(data.get("additionalNote"));
		summaryOverviewPageLib.getNumberOfExposuresAdded();
		
		ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("AbilityToPay");
		
		//Add reserves
		SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
		setReservesPageLib.addNewReserveFromActionsMenu(data);
		
//		homePageLib.searchClaim("WA-002-229");
		//Set Recovery Reserve
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("Other");
		actionsPageLib.clickOn("recoveryReserve");
		
		SetRecoveryReservesPageLib setRecoveryReservesPageLib = new SetRecoveryReservesPageLib(driver, reporter);
		setRecoveryReservesPageLib.addNewRecoveryReserve(data);
		
		summaryOverviewPageLib.openFinancialsPage();
		
		FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
		financialsPageLib.verifyOpenRecoveryReserveCreated(data.get("reportername"), data.get("newOpenRecoveryReserve"));
		
		status = true;
	}

}
