package com.pure.test.claim.regression.fnol;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.EnumLOBType;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.ReviewAndSaveClaimPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyBasicInformationPageLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyMainPageLib;
import com.pure.utilities.TestUtil;


public class TC013_Unverified_Claim_ExcessLiability extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC013_Unverified_Claim_ExcessLiability_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC013_Unverified_Claim_ExcessLiability_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC013_Unverified_Claim_ExcessLiability_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC013_Unverified_Claim_ExcessLiability_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC013_Unverified_Claim_ExcessLiability_Script", TestDataClaim, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC013_Unverified_Claim_ExcessLiability_Script", TestDataClaim, "Exposures");
	}

	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC013_Unverified_Claim_ExcessLiability_Script", TestDataClaim, "VehicleIncident");
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC013_Unverified_Claim_ExcessLiability_Script", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_PropertyIncident() {
		return TestUtil.getData("TC013_Unverified_Claim_ExcessLiability_Script", TestDataClaim, "PropertyIncident");
	}
	
	private Object[][] getTestDataFor_Coverages() {
		return TestUtil.getData("TC013_Unverified_Claim_ExcessLiability_Script", TestDataClaim, "Coverages");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_ExposureDetails(),getTestDataFor_VehicleIncident(),getTestDataFor_TestDetails(),
				getTestDataFor_PropertyIncident(), getTestDataFor_Coverages());
	}

	@Test(dataProvider = "getTestData")
	public void TC013_Unverified_Claim_ExcessLiability_Script(Hashtable<String, String> data) throws Throwable{

		String policyNumber = "EX" + (int) (new Date().getTime()/10000);

		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectUnVerifiedPolicyOption();
		//Thread.sleep(2000);
		//Initial step in claim creation
		UnverifiedPolicyMainPageLib unverifiedPolicyMainPageLib = new UnverifiedPolicyMainPageLib(driver, reporter);
		unverifiedPolicyMainPageLib.setLOBType(EnumLOBType.ExcessLiability);
		unverifiedPolicyMainPageLib.setPolicyNumber(policyNumber);
		unverifiedPolicyMainPageLib.setDateOfLoss(data.get("dateOfLoss"));

		//Basic Info section
		unverifiedPolicyMainPageLib.setEffectiveDate(data.get("effectiveDate"));
		unverifiedPolicyMainPageLib.setExpirationDate(data.get("expirationDate"));
		//Insured details
		unverifiedPolicyMainPageLib.fillInsuredDetails(data);
		//add policy level coverage
		unverifiedPolicyMainPageLib.addPolicyLevelCoverages(data);
		
		//navigate to next page
		newClaimPageLib.clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");

		//Reported by info
		UnverifiedPolicyBasicInformationPageLib unverifiedPolicyBasicInformationPageLib = new UnverifiedPolicyBasicInformationPageLib(driver, reporter);
		unverifiedPolicyBasicInformationPageLib.setReportedBy(data.get("reporterName"));
		unverifiedPolicyBasicInformationPageLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 8: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);		
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.

		//Verify navigated to step4
		//newClaimPageLib.verifyTitle("Step 4 of 8: Manage exposures");
		
		newClaimPageLib.clickNext();

		//Verify navigated to step5
		//newClaimPageLib.verifyTitle("Step 5 of 8: Manage parties involved");

		newClaimPageLib.clickNext();

		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));

		newClaimPageLib.clickNext();

		//Verify navigated to step8
		//newClaimPageLib.verifyTitle("Step 8 of 8: Review and Save Claim");

		new ReviewAndSaveClaimPageLib(driver, reporter).clickFinish();

		new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		
		status = true;
	}

}
