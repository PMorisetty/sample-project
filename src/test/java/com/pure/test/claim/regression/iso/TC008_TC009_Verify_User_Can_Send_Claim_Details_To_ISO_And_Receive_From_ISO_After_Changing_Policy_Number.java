package com.pure.test.claim.regression.iso;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.DocumentsPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.InjuryIncidentPageLib;
import com.pure.claim.LossDetailsPageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.PolicyGeneralPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.ExposuresPageLib;
import com.pure.claim.lifeCycle.PolicyComparisonPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.lifeCycle.WorkPlanPageLib;
import com.pure.claim.lifeCycle.selectPolicyPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.Qtest;
import com.pure.utilities.TestUtil;

public class TC008_TC009_Verify_User_Can_Send_Claim_Details_To_ISO_And_Receive_From_ISO_After_Changing_Policy_Number extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC008_TC009_Verify_User_Can_Send_Claim_Details_To_ISO_And_Receive_From_ISO_After_Changing_Policy_Number_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC008_TC009_Verify_User_Can_Send_Claim_Details_To_ISO_And_Receive_From_ISO_After_Changing_Policy_Number_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC008_TC009_Verify_User_Can_Send_Claim_Details_To_ISO_And_Receive_From_ISO_After_Changing_Policy_Number_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC008_TC009_Verify_User_Can_Send_Claim_Details_To_ISO_And_Receive_From_ISO_After_Changing_Policy_Number_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC008_TC009_Verify_User_Can_Send_Claim_Details_To_ISO_And_Receive_From_ISO_After_Changing_Policy_Number_Script", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC008_TC009_Verify_User_Can_Send_Claim_Details_To_ISO_And_Receive_From_ISO_After_Changing_Policy_Number_Script", TestDataClaim, "VehicleIncident");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC008_TC009_Verify_User_Can_Send_Claim_Details_To_ISO_And_Receive_From_ISO_After_Changing_Policy_Number_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC008_TC009_Verify_User_Can_Send_Claim_Details_To_ISO_And_Receive_From_ISO_After_Changing_Policy_Number_Script", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_ISO() {
		return TestUtil.getData("TC008_TC009_Verify_User_Can_Send_Claim_Details_To_ISO_And_Receive_From_ISO_After_Changing_Policy_Number_Script", TestDataClaim, "ISO");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_VehicleIncident(),getTestDataFor_TestDetails(), getTestDataFor_Exposures(),
				getTestDataFor_ISO());
	}

	
	
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	
	@Test(dataProvider = "getTestData")
	public void TC008_TC009_Verify_User_Can_Send_Claim_Details_To_ISO_And_Receive_From_ISO_After_Changing_Policy_Number_Script(Hashtable<String, String> data) throws Throwable{

		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver,reporter).clickNext();		
		
		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		VerifiedPolicyBasicInfoLib.selectInvolvedVehicle(data.get("policylevelcoverage"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver,reporter);
		addClaimInfoPageLib.enterVehiclesInformation(data);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.
		
		//Verify navigated to step4
		//newClaimPageLib.verifyTitle("Step 4 of 4: Save and Assign Claim");
		
		//Fill adjuster details and note
		
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();    
		String claimNumber = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		homePageLib.searchClaim(claimNumber);
		
		ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ChooseByCoverage");
		actionsPageLib.clickOnVehicleInvolved(data.get("coverageType1"));
		actionsPageLib.clickOn(data.get("coverageType2"));
		
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openNewIncident();
		
		new InjuryIncidentPageLib(driver, reporter).fillDetails(data);
		
		newExposurePageLib.clickUpdate();
		
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.openLossDetailsPage();
		
		LossDetailsPageLib lossDetailsPageLib = new LossDetailsPageLib(driver, reporter);
		lossDetailsPageLib.clickISO();
//		lossDetailsPageLib.verifyIsoLastResponseIsEmpty();
//		lossDetailsPageLib.verifyknownToISOIsEmpty();
		
		// click on Send to ISO button
		lossDetailsPageLib.clickSendToIsoButton();
//		lossDetailsPageLib.verifyIsoDateSent();
		lossDetailsPageLib.verifyIsoStatus(data.get("ISOStatus"));
		
		summaryOverviewPageLib.openWorkPlanPage();
		new WorkPlanPageLib(driver, reporter).completeAllActivities();
		
		summaryOverviewPageLib.openExposuresPage();
		new ExposuresPageLib(driver, reporter).closeExposure(data);
		
		summaryOverviewPageLib.openPolicyPage();
		
		PolicyGeneralPageLib policyGeneralPageLib = new PolicyGeneralPageLib(driver, reporter);
		policyGeneralPageLib.clickSelectPolicy();
		
		selectPolicyPageLib selectPolicyPageLib = new selectPolicyPageLib(driver, reporter);
		selectPolicyPageLib.fillDetailsAndSearch(data.get("newPolicyNum"), data.get("searchFor"));
		
		new PolicyComparisonPageLib(driver, reporter).clickFinish();
		
		policyGeneralPageLib.verifyPolicyNumber(data.get("newPolicyNum"));
		
		summaryOverviewPageLib.openLossDetailsPage();
		lossDetailsPageLib.clickISO();
		lossDetailsPageLib.verifyIsoStatus(data.get("ISOStatus"));
		
		/*TC009_Verify_User_Can_Receive_Response_From_ISO_After_Changing_Policy_No*/
		reporter.createHeader("Receive response from ISO after changing policy no");
		lossDetailsPageLib.waitUntilLastResponseFromISOIsDisplayed();
//		lossDetailsPageLib.verifyIsoLastResponse();
		lossDetailsPageLib.verifyKnownToISO(data.get("knownToISO"));
		summaryOverviewPageLib.openDocumentsPage();
		
		DocumentsPageLib documentsPageLib = new DocumentsPageLib(driver, reporter);
		documentsPageLib.verifyISOPDFDocuments(data.get("noOfDocuments"), data.get("policyNum"), claimNumber);
		
		status = true;
	}
	
	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		Qtest.UpdateQTestResults(status, "Create Personal Auto claim for Verified Policy", startTime, endTime);
	}
}
