package com.pure.test.claim.regression.ccc;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.lifeCycle.VehicleIncidentPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;


public class TC010_Verify_If_CCC_Make_Code_Matches_With_Vehicle_Code_Then_Auto_Populate_CCC_Make_Code extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC010_Verify_If_CCC_Make_Code_Matches_With_Vehicle_Code_Then_Auto_Populate_CCC_Make_Code_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC010_Verify_If_CCC_Make_Code_Matches_With_Vehicle_Code_Then_Auto_Populate_CCC_Make_Code_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC010_Verify_If_CCC_Make_Code_Matches_With_Vehicle_Code_Then_Auto_Populate_CCC_Make_Code_Script", TestDataClaim, "BasicInfo");
	}
	
	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC010_Verify_If_CCC_Make_Code_Matches_With_Vehicle_Code_Then_Auto_Populate_CCC_Make_Code_Script", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC010_Verify_If_CCC_Make_Code_Matches_With_Vehicle_Code_Then_Auto_Populate_CCC_Make_Code_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC010_Verify_If_CCC_Make_Code_Matches_With_Vehicle_Code_Then_Auto_Populate_CCC_Make_Code_Script", TestDataClaim, "VehicleIncident");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(), getTestDataFor_ExposureDetails(),
				getTestDataFor_TestDetails(), getTestDataFor_VehicleIncident());
	}

	
	
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	
	@Test(dataProvider = "getTestData")
	public void TC010_Verify_If_CCC_Make_Code_Matches_With_Vehicle_Code_Then_Auto_Populate_CCC_Make_Code_Script(Hashtable<String, String> data) throws Throwable{

		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver,reporter).clickNext();		
		
		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setHowReported(data.get("howReported"));
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		VerifiedPolicyBasicInfoLib.selectInvolvedVehicle(data.get("policylevelcoverage"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver,reporter);
//		addClaimInfoPageLib.enterVehiclesInformation(data);
		addClaimInfoPageLib.addVehicleIncident();
		
		VehicleIncidentPageLib vehicleIncidentPageLib = new VehicleIncidentPageLib(driver, reporter);
		vehicleIncidentPageLib.setInvolvedVehicle(data.get("vehicle1"), data.get("lossParty1"), 
							data.get("year1"), data.get("make1"), data.get("model1"));
		
		vehicleIncidentPageLib.assertMakeCode(data.get("makeCode1"));
		
		vehicleIncidentPageLib.setInvolvedVehicle(data.get("vehicle2"), data.get("lossParty2"), 
							data.get("year2"), data.get("make2"), data.get("model2"));

		vehicleIncidentPageLib.assertMakeCode(data.get("makeCode2"));
		
//		new VehicleIncidentPageLib(driver, reporter).assertMakeCode("none");
		
		status = true;		 		
	}
	
	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		//Qtest.UpdateQTestResults(status, "Create Personal Auto claim for Verified Policy", startTime, endTime);
	}
}
