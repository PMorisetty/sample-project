package com.pure.test.claim.regression.bulkinvoicefedex;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.lifeCycle.BatchProcessInfoPageLib;
import com.pure.claim.lifeCycle.BulkInvoiceDetailsPageLib;
import com.pure.claim.lifeCycle.BulkInvoicePageLib;
import com.pure.utilities.TestUtil;


public class TC004_FedEx_Vendor_Table_Field_Values_Line_Item_Level extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC004_FedEx_Vendor_Table_Field_Values_Line_Item_Level_Script", TestDataClaim, "LoginData");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC004_FedEx_Vendor_Table_Field_Values_Line_Item_Level_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_BulkInvoice() {
		return TestUtil.getData("TC004_FedEx_Vendor_Table_Field_Values_Line_Item_Level_Script", TestDataClaim, "BulkInvoice");
	}
	
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(),getTestDataFor_TestDetails(),
				getTestDataFor_BulkInvoice());
	}
	
	@Test(dataProvider = "getTestData")
	public void TC004_FedEx_Vendor_Table_Field_Values_Line_Item_Level_Script(Hashtable<String, String> data) throws Throwable{

		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		
		homePageLib.navigateToBatchProcessInfo();
		
		BatchProcessInfoPageLib batchProcessInfoPageLib = new BatchProcessInfoPageLib(driver, reporter);
		batchProcessInfoPageLib.startAutomatedBulkInvoiceJob();
		
		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("returnToClaimCenter");
		
		homePageLib.navigateToBulkInvoices();
		
		new BulkInvoicePageLib(driver, reporter).selectUploadedCSVFile(data.get("bulkInvoiceFileName"));
		new BulkInvoiceDetailsPageLib(driver, reporter).claimDetails(data.get("claimNo"), data.get("vendorReference"), 
				data.get("invoiceNo"), data.get("reserveLine"), data.get("exposure"), data.get("amount"), 
				data.get("deductions"), data.get("paymentType"), data.get("status"));
		
		status = true;
	}

}
