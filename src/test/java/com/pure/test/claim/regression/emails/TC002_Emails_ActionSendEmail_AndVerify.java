package com.pure.test.claim.regression.emails;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.PropertyLiabilityPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.EmailsPageLib;
import com.pure.claim.lifeCycle.NotesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.lifeCycle.UploadDocumentsPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;

public class TC002_Emails_ActionSendEmail_AndVerify extends ActionEngine {

	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC002_Emails_ActionSendEmail_AndVerify", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC002_Emails_ActionSendEmail_AndVerify", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC002_Emails_ActionSendEmail_AndVerify", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC002_Emails_ActionSendEmail_AndVerify", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC002_Emails_ActionSendEmail_AndVerify", TestDataClaim, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC002_Emails_ActionSendEmail_AndVerify", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_Documents() {
		return TestUtil.getData("TC002_Emails_ActionSendEmail_AndVerify", TestDataClaim, "Documents");
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC002_Emails_ActionSendEmail_AndVerify", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_PropertyIncident() {
		return TestUtil.getData("TC002_Emails_ActionSendEmail_AndVerifyTC002_Emails_ActionSendEmail_AndVerify", TestDataClaim, "PropertyIncident");
	}
	
	private Object[][] getTestDataFor_Emails() {
		return TestUtil.getData("TC002_Emails_ActionSendEmail_AndVerify", TestDataClaim, "Emails");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),getTestDataFor_ExposureDetails(),
				getTestDataFor_Documents(),getTestDataFor_TestDetails(),
				getTestDataFor_PropertyIncident(), getTestDataFor_Emails());
	}

	@Test(dataProvider = "getTestData")
	public void TC002_Emails_ActionSendEmail_AndVerify_Script(Hashtable<String, String> data) throws Throwable{

		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		newClaimPageLib.clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setHowReported(data.get("howReported"));
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		addClaimInfoPageLib.AddPropertiesIncident(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.

		//-----Services---
		//Verify navigated to step4
		//click next
		newClaimPageLib.clickNext();

		//Fill adjuster details and note

		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		saveAndAssignClaimLib.setNote(data.get("additionalNote"));	
		//New Exposure addition
		saveAndAssignClaimLib.addNewExposure(data.get("policylevelcoverage"), data.get("exposuretype"),data.get("exposuretype2"));
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();
		
		new PropertyLiabilityPageLib(driver, reporter).fillDetails(data);
		
		newExposurePageLib.setIAUsed(data.get("iaused"));
		newExposurePageLib.setDeskAdjustment(data.get("deskadjustment"));
		newExposurePageLib.clickOK();

		//Submit the claim
		saveAndAssignClaimLib.clickFinish();    
		String claimNumber = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		homePageLib.searchClaim(claimNumber);
		
		//document upload
		String pathToPolicyDocument = System.getProperty("user.dir") + "\\TestData\\PolicyDocuments\\Policy_Info.txt";
		UploadDocumentsPageLib documentsPageLib = new UploadDocumentsPageLib(driver, reporter);
		documentsPageLib.openNewDocument();
		documentsPageLib.uploadDocument(pathToPolicyDocument, data.get("ShareOnMemberPortal"),
				data.get("docdescription"), data.get("doctype"));
		
		//Verify that email can be send to "To', "CC" and 'BCC" recipients
		EmailsPageLib emailsPageLib = new EmailsPageLib(driver, reporter);
		emailsPageLib.openEmailTab();
		emailsPageLib.fillToRecipientDetails(data);
		emailsPageLib.fillCCRecipientDetails(data.get("ccRecipientName"), data.get("ccRecipientEmail"));
		emailsPageLib.fillBccRecipientDetails(data.get("bccRecipientName"), data.get("bccRecipientEmail"));
		emailsPageLib.TypeShareOnMemberPortal(data.get("shareOnMemberPortal_Email"));
		emailsPageLib.TypeSubject(data.get("emailSubject"));
		emailsPageLib.TypeBody(data.get("emailBody"));
		emailsPageLib.AddAttachment(data.get("attachmentName"));
		emailsPageLib.clickSendEmailBtn();
		emailsPageLib.verifyEmailIsSent();
		
		new SummaryOverviewPageLib(driver, reporter).openNotesPage();
		new NotesPageLib(driver, reporter).verifyEmailSent();
	}
}
