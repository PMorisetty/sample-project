package com.pure.test.claim.regression.fnol;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.EnumLOBType;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyBasicInformationPageLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyMainPageLib;
import com.pure.utilities.TestUtil;


public class TC016_Unverified_Claim_MemberAdvocateService extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC016_Unverified_Claim_MemberAdvocateService_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC016_Unverified_Claim_MemberAdvocateService_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC016_Unverified_Claim_MemberAdvocateService_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC016_Unverified_Claim_MemberAdvocateService_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC016_Unverified_Claim_MemberAdvocateService_Script", TestDataClaim, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC016_Unverified_Claim_MemberAdvocateService_Script", TestDataClaim, "Exposures");
	}

	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC016_Unverified_Claim_MemberAdvocateService_Script", TestDataClaim, "VehicleIncident");
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC016_Unverified_Claim_MemberAdvocateService_Script", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_PropertyIncident() {
		return TestUtil.getData("TC016_Unverified_Claim_MemberAdvocateService_Script", TestDataClaim, "PropertyIncident");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_ExposureDetails(),getTestDataFor_VehicleIncident(),getTestDataFor_TestDetails(),
				getTestDataFor_PropertyIncident());
	}

	@Test(dataProvider = "getTestData")
	public void TC016_Unverified_Claim_MemberAdvocateService_Script(Hashtable<String, String> data) throws Throwable{

		String memberID = "MA" + (int) (new Date().getTime()/10000);

		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectUnVerifiedPolicyOption();
		//Thread.sleep(2000);
		//Initial step in claim creation
		UnverifiedPolicyMainPageLib unverifiedPolicyMainPageLib = new UnverifiedPolicyMainPageLib(driver, reporter);
		unverifiedPolicyMainPageLib.setLOBType(EnumLOBType.MemberAdvocateServiceRequest);
		unverifiedPolicyMainPageLib.setMemberID(memberID);
		unverifiedPolicyMainPageLib.setDateOfRequest(data.get("dateOfRequest"));

		//Member details
		unverifiedPolicyMainPageLib.fillInsuredDetails(data);
		unverifiedPolicyMainPageLib.setMemberOrProspect(data.get("memberOrProspect"));

		//navigate to next page
		newClaimPageLib.clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 2: Basic information");

		//Reported by info
		UnverifiedPolicyBasicInformationPageLib unverifiedPolicyBasicInformationPageLib = new UnverifiedPolicyBasicInformationPageLib(driver, reporter);
		unverifiedPolicyBasicInformationPageLib.setReportedBy(data.get("reporterName"));
		unverifiedPolicyBasicInformationPageLib.setRelationToInsured(data.get("relationToInsured"));
		unverifiedPolicyBasicInformationPageLib.setServiceType(data.get("serviceType"));
		unverifiedPolicyBasicInformationPageLib.setServiceLocation(data.get("serviceLocation"));
		newClaimPageLib.clickFinish();

		new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		
		status = true;
	}

}
