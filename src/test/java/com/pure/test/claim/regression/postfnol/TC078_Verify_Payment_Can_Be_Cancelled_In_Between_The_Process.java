package com.pure.test.claim.regression.postfnol;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.PropertyLiabilityPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.EnterPayeeInformationPageLib;
import com.pure.claim.lifeCycle.EnterPaymentInformationPageLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.SetCheckInstructionsPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.lifeCycle.UploadDocumentsPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.Qtest;
import com.pure.utilities.TestUtil;

public class TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "Exposures");
	}

	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "Reserves");
	}
	
	private Object[][] getTestDataFor_CreateCheck() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "CreateCheck");
	}

	private Object[][] getTestDataFor_Documents() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "Documents");
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_PropertyIncident() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "PropertyIncident");
	}
	
	private Object[][] getTestDataFor_PaymentInfoDetails() {
		return TestUtil.getData("TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script", TestDataClaim, "PaymentInfoDetails");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),getTestDataFor_ExposureDetails(),
				getTestDataFor_Reserves(),getTestDataFor_CreateCheck(),getTestDataFor_Documents(),getTestDataFor_TestDetails(),
				getTestDataFor_PropertyIncident(), getTestDataFor_PaymentInfoDetails());
	}

	@Test(dataProvider = "getTestData")
	public void TC078_Verify_Payment_Can_Be_Cancelled_In_Between_The_Process_Script(Hashtable<String, String> data) throws Throwable{

		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		newClaimPageLib.clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setHowReported(data.get("howReported"));
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		addClaimInfoPageLib.AddPropertiesIncident(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.

		//-----Services---
		//Verify navigated to step4
		//click next
		newClaimPageLib.clickNext();

		//Fill adjuster details and note

		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		saveAndAssignClaimLib.setNote(data.get("additionalNote"));	
		//New Exposure addition
		saveAndAssignClaimLib.addNewExposure(data.get("policylevelcoverage"), data.get("exposuretype"),data.get("exposuretype2"));
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();
		
		new PropertyLiabilityPageLib(driver, reporter).fillDetails(data);
		
		newExposurePageLib.setIAUsed(data.get("iaused"));
		newExposurePageLib.setDeskAdjustment(data.get("deskadjustment"));
		newExposurePageLib.clickOK();

		//Submit the claim
		saveAndAssignClaimLib.clickFinish();    
		String claimNumber = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		new ClaimCreationSummaryPageLib(driver, reporter).clickNewlyCreatedClaim();

		//Verify the notes on summary page
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.verifyLatestNotes(data.get("additionalNote"));
		summaryOverviewPageLib.getNumberOfExposuresAdded();
		
		ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("AbilityToPay");
		
		//Add Reserve
		SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
		setReservesPageLib.addNewReserveFromActionsMenu(data);
						
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("ValidForISO");
		FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
		financialsPageLib.openCreateCheck();
		
		EnterPayeeInformationPageLib enterPayeeInformationPageLib = new EnterPayeeInformationPageLib(driver, reporter);
		enterPayeeInformationPageLib.fillPayeeAndPaymentDetails(data.get("reporterName"), data.get("paymentMethod"));
		enterPayeeInformationPageLib.clickNext();
		
		EnterPaymentInformationPageLib enterPaymentInformationPageLib = new EnterPaymentInformationPageLib(driver, reporter);
		enterPaymentInformationPageLib.fillDetails(data.get("reserveLine"), data.get("paymentType"), data.get("paymentAmount"));
		enterPaymentInformationPageLib.setDeductibleType(data.get("deductibleType"));
		enterPaymentInformationPageLib.verifyDeductibleDecisionWhenOverrideDeductibleYesNoOptionsAreDisplayed(data.get("WaivedOrApplied"),data.get("waivedReason"),data.get("otherComments"));
		enterPaymentInformationPageLib.clickNext();
		
		SetCheckInstructionsPageLib setCheckInstructionsPageLib = new SetCheckInstructionsPageLib(driver, reporter);
		setCheckInstructionsPageLib.setDeliverMethod(data.get("deliverymethod"));
		setCheckInstructionsPageLib.setShareOnMemberPortal(data.get("ShareOnMemberPortal"));
		setCheckInstructionsPageLib.AdvancedDirectPay(data.get("advanceddirectPay"), data.get("explainationfordirectpay"));
		setCheckInstructionsPageLib.clickCancel();
		status = true;
	}

}
