package com.pure.test.claim.regression.bulkinvoicemetro;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.lifeCycle.ActivitiesPageLib;
import com.pure.claim.lifeCycle.ApprovalPageLib;
import com.pure.claim.lifeCycle.BatchProcessInfoPageLib;
import com.pure.claim.lifeCycle.BulkInvoiceDetailsPageLib;
import com.pure.claim.lifeCycle.BulkInvoicePageLib;
import com.pure.claim.lifeCycle.ChecksPageLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.NotesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.utilities.TestUtil;


public class TC009_Metro_Vendor_No_AAndO_Expense_Reserve_Line_Payment_Creation extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC009_Metro_Vendor_No_AAndO_Expense_Reserve_Line_Payment_Creation_Script", TestDataClaim, "LoginData");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC009_Metro_Vendor_No_AAndO_Expense_Reserve_Line_Payment_Creation_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_BulkInvoice() {
		return TestUtil.getData("TC009_Metro_Vendor_No_AAndO_Expense_Reserve_Line_Payment_Creation_Script", TestDataClaim, "BulkInvoice");
	}
	
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(),getTestDataFor_TestDetails(),
				getTestDataFor_BulkInvoice());
	}
	
	@Test(dataProvider = "getTestData")
	public void TC009_Metro_Vendor_No_AAndO_Expense_Reserve_Line_Payment_Creation_Script(Hashtable<String, String> data) throws Throwable{

		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		
		homePageLib.navigateToBatchProcessInfo();
		
		BatchProcessInfoPageLib batchProcessInfoPageLib = new BatchProcessInfoPageLib(driver, reporter);
//		batchProcessInfoPageLib.startAutomatedBulkInvoiceJob();
		
		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("returnToClaimCenter");
		
		homePageLib.navigateToBulkInvoices();
		
		BulkInvoicePageLib bulkInvoicePageLib =new BulkInvoicePageLib(driver, reporter);
		bulkInvoicePageLib.selectUploadedCSVFile(data.get("bulkInvoiceFileName"));
		
		BulkInvoiceDetailsPageLib bulkInvoiceDetailsPageLib = new BulkInvoiceDetailsPageLib(driver, reporter);
		bulkInvoiceDetailsPageLib.clickClaimNo(data.get("claimNo"));
		
		FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
		financialsPageLib.openChecks();
		
		/*ChecksPageLib checksPageLib = new ChecksPageLib(driver, reporter);
		checksPageLib.clickCheckNo();
		CheckDetailsPageLib checkDetailsPageLib = new CheckDetailsPageLib(driver, reporter);
		checkDetailsPageLib.verifyTrackingNoIsNotPresent();
		checkDetailsPageLib.navigateUptoFinancials();*/
		
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("AbilityToPay");
		financialsPageLib.verifyAbilityToPayNoValidationsErrorMsg();
		
		financialsPageLib.openTransactions();
		financialsPageLib.verifyBulkInvoiceCreatedReserveUserAndAmount(data.get("costtype"), data.get("amount"), data.get("createdUser"));
		
		homePageLib.moveToDesktopMenu();
		homePageLib.navigateToBulkInvoices();
		
		bulkInvoicePageLib.selectUploadedCSVFile(data.get("bulkInvoiceFileName"));
		
		bulkInvoiceDetailsPageLib.clickSubmit();
		bulkInvoiceDetailsPageLib.verifyApprovalHistoryStatus(data.get("status2"));
		bulkInvoiceDetailsPageLib.clickUptoBulkInvoices();
		bulkInvoicePageLib.verifyStatus(data.get("bulkInvoiceFileName"), data.get("invoicestatus"));
		homePageLib.logout();
		homePageLib.login(data.get("approverusername"), data.get("approverpassword"));
		homePageLib.openActivities();
		
		ActivitiesPageLib activitiesPageLib = new ActivitiesPageLib(driver, reporter);
		activitiesPageLib.selectActivityBasedOnInvoice(data.get("bulkInvoiceFileName"));
		new ApprovalPageLib(driver, reporter).clickApprove();
		homePageLib.logout();
		homePageLib.login(data.get("username"), data.get("password"));
		
		bulkInvoicePageLib.verifyStatus(data.get("claimNo"), data.get("bulkInvoiceStatus"));
		bulkInvoicePageLib.selectUploadedCSVFile(data.get("bulkInvoiceFileName"));

		
		bulkInvoiceDetailsPageLib.clickClaimNo(data.get("claimNo"));
		financialsPageLib.openFinancials();
		financialsPageLib.openChecks();
		ChecksPageLib checksPageLib = new ChecksPageLib(driver, reporter);
		checksPageLib.verifyBulkInvoiceDetails(data.get("bulkInvoiceAmount"), data.get("bulkInvoiceStatus"));
		financialsPageLib.openTransactions();
		financialsPageLib.verifyBulkInvoicePaymentDetails(data.get("costtype"), data.get("bulkInvoiceAmount"), data.get("bulkInvoiceStatus"), data.get("createdUser2"));
		financialsPageLib.verifyBulkInvoiceReserveDetails(data.get("costtype"), data.get("bulkInvoiceAmount"), data.get("createdUser2"));
		financialsPageLib.verifyBulkInvoiceReserveDetails(data.get("costtype"), data.get("bulkInvoiceAmount"), data.get("createdUser3"));
		
		new SummaryOverviewPageLib(driver, reporter).openNotesPage();
		new NotesPageLib(driver, reporter).verifyBulkInvoiceDetails(data.get("bulkInvoiceFileName"), data.get("costtype"), data.get("bulkInvoiceAmount"));
		status = true;
	}

}
