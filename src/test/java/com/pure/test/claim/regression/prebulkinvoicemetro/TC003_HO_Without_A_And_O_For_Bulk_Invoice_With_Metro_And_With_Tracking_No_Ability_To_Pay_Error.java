package com.pure.test.claim.regression.prebulkinvoicemetro;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.PropertyLiabilityPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.QuickCheckBasicsPageLib;
import com.pure.claim.lifeCycle.QuickChecksDetailsPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.Qtest;
import com.pure.utilities.TestUtil;

public class TC003_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Ability_To_Pay_Error extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script", TestDataClaim, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script", TestDataClaim, "Exposures");
	}

	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script", TestDataClaim, "Reserves");
	}
	
	private Object[][] getTestDataFor_QuickCheck() {
		return TestUtil.getData("TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script", TestDataClaim, "QuickCheck");
	}

	private Object[][] getTestDataFor_Documents() {
		return TestUtil.getData("TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script", TestDataClaim, "Documents");
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script", TestDataClaim, "TestDetails");
	}

	private Object[][] getTestDataFor_PropertyIncident() {
		return TestUtil.getData("TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script", TestDataClaim, "PropertyIncident");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),getTestDataFor_ExposureDetails(),
				getTestDataFor_Reserves(),getTestDataFor_QuickCheck(),getTestDataFor_Documents(),getTestDataFor_TestDetails(),
				getTestDataFor_PropertyIncident());
	}



	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}


	@Test(dataProvider = "getTestData")
	public void TC001_HO_Without_A_And_O_For_Bulk_Invoice_With_Metro_And_With_Tracking_No_Script(Hashtable<String, String> data) throws Throwable{

		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		newClaimPageLib.clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setHowReported(data.get("howReported"));
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
//		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		addClaimInfoPageLib.AddPropertiesIncident(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.

		//-----Services---
		//Verify navigated to step4
		//click next
		newClaimPageLib.clickNext();

		//Fill adjuster details and note

		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		saveAndAssignClaimLib.setNote(data.get("additionalNote"));	
		//New Exposure addition
		saveAndAssignClaimLib.addNewExposure(data.get("policylevelcoverage"), data.get("exposuretype"),data.get("exposuretype2"));
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();
		
		new PropertyLiabilityPageLib(driver, reporter).fillDetails(data);
		
		newExposurePageLib.setIAUsed(data.get("iaused"));
		newExposurePageLib.setDeskAdjustment(data.get("deskadjustment"));
		newExposurePageLib.clickOK();

		//Submit the claim
		saveAndAssignClaimLib.clickFinish();    
		new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		new ClaimCreationSummaryPageLib(driver, reporter).clickNewlyCreatedClaim();

		//Verify the notes on summary page
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.verifyLatestNotes(data.get("additionalNote"));
		summaryOverviewPageLib.getNumberOfExposuresAdded();
		
		/*ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("AbilityToPay");*/
		
		//Add Reserve
		SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
		setReservesPageLib.addNewReserveFromActionsMenu(data);

		//QuickCheck basics		
		/*actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("ValidForISO");*/
		FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
		financialsPageLib.openQuickCheck();		

		QuickCheckBasicsPageLib quickCheckBasicsPageLib = new QuickCheckBasicsPageLib(driver, reporter);	
		quickCheckBasicsPageLib.setDeductibleType(data.get("deductibleType"));
		quickCheckBasicsPageLib.verifyDeductibleDecisionWhenOverrideDeductibleIsOnlyNo(data.get("WaivedOrApplied"),data.get("waivedReason"),data.get("otherComments"));
		quickCheckBasicsPageLib.fillDetails(data);
		//QuickCheck Details
		new QuickChecksDetailsPageLib(driver, reporter).fillDetails(data);
		
		status = true;
	}

	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		Qtest.UpdateQTestResults(status, "Create Personal Auto claim for Verified Policy", startTime, endTime);
	}
}
