package com.pure.test.claim.regression.ccc;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.InstructionsToVendorPageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.SearchForAppraisersPageLib;
import com.pure.claim.ServicesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.lifeCycle.VehicleIncidentPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;


public class TC002_Verify_Details_Displayed_In_Service_Tab extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC002_Verify_Details_Displayed_In_Service_Tab_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC002_Verify_Details_Displayed_In_Service_Tab_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC002_Verify_Details_Displayed_In_Service_Tab_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC002_Verify_Details_Displayed_In_Service_Tab_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC002_Verify_Details_Displayed_In_Service_Tab_Script", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC002_Verify_Details_Displayed_In_Service_Tab_Script", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC002_Verify_Details_Displayed_In_Service_Tab_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC002_Verify_Details_Displayed_In_Service_Tab_Script", TestDataClaim, "VehicleIncident");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(), getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_ExposureDetails(), getTestDataFor_TestDetails(), getTestDataFor_VehicleIncident());
	}

	
	
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}

	
	@Test(dataProvider = "getTestData")
	public void TC002_Verify_Details_Displayed_In_Service_Tab_Script(Hashtable<String, String> data) throws Throwable{

		
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver,reporter).clickNext();		
		
		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setHowReported(data.get("howReported"));
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		VerifiedPolicyBasicInfoLib.selectInvolvedVehicle(data.get("policylevelcoverage"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver,reporter);
		addClaimInfoPageLib.enterVehiclesInformation(data);
		
		SearchForAppraisersPageLib searchForAppraisersPageLib = new SearchForAppraisersPageLib(driver, reporter);
		searchForAppraisersPageLib.clickMethodOfInspection(data.get("methodOfInspection"));
		searchForAppraisersPageLib.selectAppraiserName(data.get("appraiserName"));
		
		InstructionsToVendorPageLib instructionsToVendorPageLib = new InstructionsToVendorPageLib(driver, reporter);
		instructionsToVendorPageLib.fillDetails(data);
		instructionsToVendorPageLib.clickUpdate();
		
		new VehicleIncidentPageLib(driver, reporter).enterDetails(data.get("damagedescription"), data.get("reporterName"), data.get("lossoccured"));
		
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.
		
		//Verify navigated to step4
		//newClaimPageLib.verifyTitle("Step 4 of 4: Save and Assign Claim");
		
		//Fill adjuster details and note
		
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();
		
		ClaimCreationSummaryPageLib claimCreationSummaryPageLib = new ClaimCreationSummaryPageLib(driver, reporter);
		claimCreationSummaryPageLib.getClaimNumber();
		claimCreationSummaryPageLib.getServiceRequestNumber();
		claimCreationSummaryPageLib.clickNewlyCreatedClaim();
		
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.openServicesPage();
		
		ServicesPageLib servicesPageLib = new ServicesPageLib(driver, reporter);
		servicesPageLib.verifyAllTheTabsAreDisplayed();
		servicesPageLib.verifyBtnsInDetailsTab();
		servicesPageLib.verifyProgressStatusRequested();
		servicesPageLib.verifyInstToVendor(data.get("vendorInstructions"));
//		servicesPageLib.verifyServiceAddress(data);
		servicesPageLib.verifyVendorName(data.get("appraiserName"));
		status = true;		 		
	}
	
	@AfterMethod
	public void afterTest() {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		//Qtest.UpdateQTestResults(status, "Create Personal Auto claim for Verified Policy", startTime, endTime);
	}
}
