package com.pure.test.claim.regression.fnol;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AutoFirstAndFinalPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.EnumLOBType;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.SearchAddressBookPageLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyMainPageLib;
import com.pure.utilities.TestUtil;

public class TC005_Unverified_Claim_Auto_First_And_Final extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC005_Unverified_Claim_Auto_First_And_Final_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC005_Unverified_Claim_Auto_First_And_Final_Script", TestDataClaim, "LoginData");
	}
	
	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC005_Unverified_Claim_Auto_First_And_Final_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC005_Unverified_Claim_Auto_First_And_Final_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_SearchAddressBook() {
		return TestUtil.getData("TC005_Unverified_Claim_Auto_First_And_Final_Script", TestDataClaim, "SearchAddressBook");
	}
	
	private Object[][] getTestDataFor_AutoFirstAndFinal() {
		return TestUtil.getData("TC005_Unverified_Claim_Auto_First_And_Final_Script", TestDataClaim, "AutoFirstAndFinal");
	}
	
	
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_SearchAddressBook(),getTestDataFor_TestDetails(),
				getTestDataFor_AutoFirstAndFinal(), getTestDataFor_BasicInfo());
	}

	@Test(dataProvider = "getTestData")
	public void TC005_Unverified_Claim_Auto_First_And_Final_Script(Hashtable<String, String> data) throws Throwable{
		String policyNumber = "PA" + (int) (new Date().getTime()/10000);

		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectUnVerifiedPolicyOption();
		//Thread.sleep(2000);
		//Initial step in claim creation
		UnverifiedPolicyMainPageLib unverifiedPolicyMainPageLib = new UnverifiedPolicyMainPageLib(driver, reporter);
		unverifiedPolicyMainPageLib.setLOBType(EnumLOBType.PersonalAuto);
		unverifiedPolicyMainPageLib.setPolicyNumber(policyNumber);
		unverifiedPolicyMainPageLib.setTypeOfClaim(data.get("typeOfClaim"));
		unverifiedPolicyMainPageLib.setDateOfLoss(data.get("dateOfLoss"));

		//Basic Info section
		unverifiedPolicyMainPageLib.setEffectiveDate(data.get("effectiveDate"));
		unverifiedPolicyMainPageLib.setExpirationDate(data.get("expirationDate"));
		//Insured details
		unverifiedPolicyMainPageLib.fillInsuredDetails(data);

		unverifiedPolicyMainPageLib.addVehicle(data);

		//navigate to next page
		newClaimPageLib.clickNext();
		
		AutoFirstAndFinalPageLib autoFirstAndFinalPageLib = new AutoFirstAndFinalPageLib(driver, reporter);
		autoFirstAndFinalPageLib.searchVendor();
		
		new SearchAddressBookPageLib(driver, reporter).searchNSelectFromAddressBook(data);
		
		autoFirstAndFinalPageLib.fillDetails(data);//handles duplicate claims as well
		new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		
		status = true;
	}

}
