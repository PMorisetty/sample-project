package com.pure.test.claim.regression.partiesinvolved;


import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.PropertyLiabilityPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.PartiesInvolvedContactsPageLib;
import com.pure.claim.lifeCycle.PartiesInvolvedPageLib;
import com.pure.claim.lifeCycle.QuickCheckBasicsPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.Qtest;
import com.pure.utilities.TestUtil;


public class TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim_Script", TestDataClaim, "ClaimSubmission");
	}
	
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim_Script", TestDataClaim, "TestDetails");
	}
	
	private Object[][] getTestDataFor_PartiesInvolved() {
		return TestUtil.getData("TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim_Script", TestDataClaim, "PartiesInvolved");
	}
	
	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim_Script", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim_Script", TestDataClaim, "Reserves");
	}
	
	private Object[][] getTestDataFor_PropertyIncident() {
		return TestUtil.getData("TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim_Script", TestDataClaim, "PropertyIncident");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_TestDetails(),getTestDataFor_PartiesInvolved(), getTestDataFor_Exposures(),
				getTestDataFor_Reserves(), getTestDataFor_PropertyIncident());
	}


	@Test(dataProvider = "getTestData")
	public void TC001_Verify_Adjuster_Role_User_Can_Edit_Details_Of_Different_Role_User_Associated_With_Claim_Script(Hashtable<String, String> data) throws Throwable{

		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		new NewClaimPageLib(driver, reporter).clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.

		//-----Services---
		//Verify navigated to step4
		//click next
		newClaimPageLib.clickNext();

		//Fill adjuster details and note

		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));
		
		//New Exposure addition
		saveAndAssignClaimLib.addNewExposure(data.get("policylevelcoverage"), data.get("exposuretype"),data.get("exposuretype2"));
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.filldetails(data);
		newExposurePageLib.openEditIncident();
		
		new PropertyLiabilityPageLib(driver, reporter).fillDetails(data);
		
		newExposurePageLib.setIAUsed(data.get("iaused"));
		newExposurePageLib.setDeskAdjustment(data.get("deskadjustment"));
		newExposurePageLib.clickOK();
		
		//Submit the claim
		saveAndAssignClaimLib.clickFinish();
		ClaimCreationSummaryPageLib claimCreationSummaryPageLib = new ClaimCreationSummaryPageLib(driver, reporter);
		claimCreationSummaryPageLib.getClaimNumber();
		claimCreationSummaryPageLib.clickNewlyCreatedClaim();
		new SummaryOverviewPageLib(driver, reporter).getNumberOfExposuresAdded();
		
		ActionsPageLib actionsPageLib  = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("AbilityToPay");
		
		//Add Reserve
		SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
		setReservesPageLib.addNewReserveFromActionsMenu(data);
		
		new PartiesInvolvedPageLib(driver, reporter).openPartiesInvolved();
		
		PartiesInvolvedContactsPageLib partiesInvolvedContactsPageLib = new PartiesInvolvedContactsPageLib(driver, reporter);
		partiesInvolvedContactsPageLib.selectContact(data.get("reporterName"));
		partiesInvolvedContactsPageLib.clickEdit();
		partiesInvolvedContactsPageLib.fillDetails(data);
		partiesInvolvedContactsPageLib.clickUpdate();
		partiesInvolvedContactsPageLib.verifyProvince(data.get("provinceCode"));
		partiesInvolvedContactsPageLib.verifyPostalCode(data.get("postalCode"));
		
		FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
		financialsPageLib.openQuickCheck();
		new QuickCheckBasicsPageLib(driver, reporter).verifyMailToAddressDetails(data);
		
		status = true;
	}

}
