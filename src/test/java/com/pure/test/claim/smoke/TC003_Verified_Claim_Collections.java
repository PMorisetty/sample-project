package com.pure.test.claim.smoke;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewExposurePageLib;
import com.pure.claim.NewNotePageLib;
import com.pure.claim.ReviewAndSaveClaimPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.SecondaryApprovalPageLib;
import com.pure.claim.lifeCycle.ChecksPageLib;
import com.pure.claim.lifeCycle.FinancialsPageLib;
import com.pure.claim.lifeCycle.QuickCheckBasicsPageLib;
import com.pure.claim.lifeCycle.QuickChecksDetailsPageLib;
import com.pure.claim.lifeCycle.SetReservesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.lifeCycle.UploadDocumentsPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyBasicInfoLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.utilities.TestUtil;


public class TC003_Verified_Claim_Collections extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");


	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC003_Verified_Claim_Collections_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC003_Verified_Claim_Collections_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC003_Verified_Claim_Collections_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC003_Verified_Claim_Collections_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC003_Verified_Claim_Collections_Script", TestDataClaim, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_ExposureDetails() {
		return TestUtil.getData("TC003_Verified_Claim_Collections_Script", TestDataClaim, "Exposures");
	}

	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC003_Verified_Claim_Collections_Script", TestDataClaim, "Reserves");
	}
	private Object[][] getTestDataFor_QuickCheck() {
		return TestUtil.getData("TC003_Verified_Claim_Collections_Script", TestDataClaim, "QuickCheck");
	}

	private Object[][] getTestDataFor_VehicleIncident() {
		return TestUtil.getData("TC003_Verified_Claim_Collections_Script", TestDataClaim, "VehicleIncident");
	}
	private Object[][] getTestDataFor_Documents() {
		return TestUtil.getData("TC003_Verified_Claim_Collections_Script", TestDataClaim, "Documents");
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC003_Verified_Claim_Collections_Script", TestDataClaim, "TestDetails");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),getTestDataFor_ExposureDetails(),
				getTestDataFor_Reserves(),getTestDataFor_QuickCheck(),getTestDataFor_VehicleIncident(),getTestDataFor_Documents(),getTestDataFor_TestDetails());
	}




	@Test(dataProvider = "getTestData")
	public void TC003_Verified_Claim_Collections_Script(Hashtable<String, String> data) throws Throwable{

		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();

		//New claim creation starts
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectVerifiedPolicyOption();

		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.searchAndSelectVerifiedPolicy(data.get("policyNum"), data.get("dateOfLoss"));
		newClaimPageLib.clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 8: Basic information");
		VerifiedPolicyBasicInfoLib VerifiedPolicyBasicInfoLib = new VerifiedPolicyBasicInfoLib(driver, reporter);

		//Reported by info		
		VerifiedPolicyBasicInfoLib.setReportedBy(data.get("reporterName"));
		VerifiedPolicyBasicInfoLib.setRelationToInsured(data.get("relationToInsured"));
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 8: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);		
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);
		newClaimPageLib.clickNext();//Duplicate claims too handled in this.

		//Verify navigated to step4
		//newClaimPageLib.verifyTitle("Step 4 of 8: Manage exposures");

		ActionsPageLib actionsPageLib = new ActionsPageLib(driver, reporter);
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("fnolNewExposure");
		actionsPageLib.clickOn("fnolChooseByCoverage");
		actionsPageLib.clickOn("fnolMiscValuablesScheduled");

		//		New Exposure addition 
		NewExposurePageLib newExposurePageLib = new NewExposurePageLib(driver, reporter);
		newExposurePageLib.setPropertyDescription(data.get("propertyDescription"));
		newExposurePageLib.setDamageDescription(data.get("damageDescription"));
		newExposurePageLib.filldetails(data);
		newExposurePageLib.clickOK();

		//add new note
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("Note");
		NewNotePageLib newNotePageLib = new NewNotePageLib(driver, reporter);
		newNotePageLib.setShareOnMemberPortal(data.get("ShareOnMemberPortal"));
		newNotePageLib.setTopic(data.get("topic"));
		newNotePageLib.setConfidential(data.get("confidential"));
		newNotePageLib.setText(data.get("policyNum")+" "+data.get("text"));
		newNotePageLib.clickUpdate(data.get("policyNum")+" "+data.get("text"));

		newClaimPageLib.clickNext();

		//Verify navigated to step5
		//newClaimPageLib.verifyTitle("Step 5 of 8: Manage parties involved");

		newClaimPageLib.clickNext();

		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"), data.get("secondaryapprovalusername"));

		newClaimPageLib.clickNext();

		//Verify navigated to step8
		//newClaimPageLib.verifyTitle("Step 8 of 8: Review and Save Claim");

		new ReviewAndSaveClaimPageLib(driver, reporter).clickFinish();

		String claimNumber = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
		homePageLib.searchClaim(claimNumber);

		//Verify the notes on summary page
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib(driver, reporter);
		summaryOverviewPageLib.verifyLatestNotes(data.get("policyNum")+" "+data.get("text"));
		summaryOverviewPageLib.getNumberOfExposuresAdded();

		//ActionsPageLib actionsPageLib  = new ActionsPageLib();
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("AbilityToPay");

		//Add Reserve
		SetReservesPageLib setReservesPageLib = new SetReservesPageLib(driver, reporter);				
		setReservesPageLib.addNewReserveFromActionsMenu(data);
		//QuickCheck basics & details
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("ValidForISO");
		FinancialsPageLib financialsPageLib = new FinancialsPageLib(driver, reporter);
		financialsPageLib.openQuickCheck();				

		QuickCheckBasicsPageLib quickCheckBasicsPageLib = new QuickCheckBasicsPageLib(driver, reporter);
		quickCheckBasicsPageLib.fillDetails(data);
		new QuickChecksDetailsPageLib(driver, reporter).fillDetails(data);

		//check the status of Quick Check added
		financialsPageLib.openChecks();		
		assertTextStringMatching(new ChecksPageLib(driver, reporter).getCheckStatus(),"Pending approval");

		//document upload

		String pathToPolicyDocument = "./TestData/PolicyDocuments/Policy_Info.txt";
		UploadDocumentsPageLib documentsPageLib = new UploadDocumentsPageLib(driver, reporter);
		documentsPageLib.openNewDocument();
		documentsPageLib.uploadDocument(pathToPolicyDocument, data.get("ShareOnMemberPortal"),
				data.get("docdescription"), data.get("doctype"));


		homePageLib.logout();
		homePageLib.login(data.get("secondaryapprovalusername"), data.get("secondaryapprovalpassword"));
		homePageLib.openSecondaryApproval();
		new SecondaryApprovalPageLib(driver, reporter).ApproveSelectedRow(claimNumber);
		homePageLib.logout();


		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.searchClaim(claimNumber);
		//Check Payments on Summary page
		summaryOverviewPageLib.openSummaryPage();
		summaryOverviewPageLib.getPaidAmount();
		//Check Payments on Financials page------------------
		financialsPageLib.openFinancials();
		financialsPageLib.getTotalPayment();
		//check the status of Quick Check added
		financialsPageLib.openChecks();
		assertTextStringMatching(new ChecksPageLib(driver, reporter).getCheckStatus(),"Awaiting submission");
		/*
		new WorkPlanPageLib().completeAllActivities();
		new ExposuresPageLib().closetAllExposures(data.get("closeexposurenotes"), data.get("deductibleDecision"));
		new CloseClaimPageLib().closeClaim(data.get("closeClaimNotes"), data.get("outcome"));
		 */
		status = true;
	}

}
