package com.pure.test.claim.smoke;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import javax.management.RuntimeErrorException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.EnumLOBType;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyBasicInformationPageLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyMainPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.testmanagement.QTestAPI_Bridge;
import com.pure.testmanagement.QTestRunLog;
import com.pure.testmanagement.QTestAPI_Bridge.tcResult;
import com.pure.utilities.TestUtil;
import com.pure.utilities.Xls_Reader;

public class Blog_OTO1 extends ActionEngine{
	boolean status = false;
//	Date startTime, endTime;
//	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//	QTestAPI_Bridge qTest = new QTestAPI_Bridge("pureinsurance");
//	JSONObject jsCycleInfo = null;
//	QTestRunLog tcLog = null;
//	tcResult scriptResult = tcResult.UNEXECUTED;	
	private Object[][] getTestDataFor_Login() {
		try
		{
			if(TestDataClaim==null)
			{
				TestDataClaim = new Xls_Reader("./TestData/TestDataClaim.xlsx");
				//LOG.error("Sreekanth Created TestDataClaim " + TestDataClaim.toString());
			}
		}
		catch(Exception e)
		{
			LOG.error("Sreekanth ERROR IN getTestDataFor_Login", e);
		}
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_Coverages() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "Coverages");
	}
	
	private Object[][] getTestDataFor_SearchAddressBook() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "SearchAddressBook");
	}
	
	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "Exposures");
	}
	
	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "Reserves");
	}
	
	private Object[][] getTestDataFor_QuickCheck() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "QuickCheck");
	}
	
	private Object[][] getTestDataFor_Documents() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "Documents");
	}
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "TestDetails");
	}
	
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		Object[][] dataArr = null;
		try
		{
			dataArr = TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
					getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
					getTestDataFor_Coverages(),getTestDataFor_SearchAddressBook(),getTestDataFor_Exposures(),
					getTestDataFor_Reserves(),getTestDataFor_QuickCheck(),getTestDataFor_Documents(),getTestDataFor_TestDetails());
		}
		catch(Exception e)
		{
			LOG.error("ERROR GETTING DATA", e);
			throw new RuntimeException(e);
		}
		return dataArr;
	}

/*	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		try {
			qTest.authorize();
			qTest.loadProject("z_QConnect - Sample Project");
			//JSONObject jsCycleInfo = qTest.getTestcyclefromTestRunID("49226931");
			qTest.createTestCycle("RELEASE_2.0");
			jsCycleInfo = qTest.getProjectElement("releases| test-cycles", "My Testing| " + qTest.timestamp);
		} catch (Exception e) {
			qTest = null;
			e.printStackTrace();
		}	
	}
	*/

	@Test(dataProvider = "getTestData")
	public void Blog_OTO1(Hashtable<String, String> data) throws Throwable{
		String policyNumber = "DW" + (int) (new Date().getTime()/10000);

			driver.get("https://www.google.com/");
			reporter.SuccessReport("Launch Blog_OTO1.com", "Launched URL Successfully");
			reporter.failureReport("Blog page", "Known Error in blog page", driver);
			
		/*
		//initialize qtest object
		if(qTest != null){
			String scriptName = ConfigFileReadWrite.read(ReporterConstants.qtestConfigFile, "BLOG_PAGE");
			String testRunName = "BLOG_PAGE";
			tcLog = qTest.startTestRunLog(jsCycleInfo, scriptName, testRunName);
			tcLog.setStartTime(dateFormatter.format(new Date()));
		}
		*/
		
//		String claimNumber = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();
//		if(tcLog != null){
//			tcLog.appendNote("Claim Number:::" + claimNumber);
//		tcLog.appendNote("Claim created successfully.");
//		}
		/* cannot be executed beyond this as we cannot add exposures, so we can't make the payment
		String claimNumber = new ClaimCreationSummaryPageLib().getClaimNumber();
		new ClaimCreationSummaryPageLib().clickNewlyCreatedClaim();
		
		//Add reserves
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib();
		summaryOverviewPageLib.verifyLatestNotes(data.get("additionalNote"));
		summaryOverviewPageLib.getNumberOfExposuresAdded();
		
		ActionsPageLib actionsPageLib  = new ActionsPageLib();
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("AbilityToPay");
		
		SetReservesPageLib setReservesPageLib = new SetReservesPageLib();
		setReservesPageLib.addNewReserve(data);
		
		//QuickCheck basics & details
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("ValidForISO");
		new FinancialsPageLib().openQuickCheck();		
		new QuickCheckBasicsPageLib().fillDetails(data);;		
		new QuickChecksDetailsPageLib().fillDetails(data);;
		
				
		//document upload
		String pathToPolicyDocument = System.getProperty("user.dir") + "/TestData/PolicyDocuments/Policy_Info.txt";
		UploadDocumentsPageLib documentsPageLib = new UploadDocumentsPageLib();
		documentsPageLib.uploadDocument(pathToPolicyDocument, data.get("ShareOnMemberPortal"),
				data.get("docdescription"), data.get("doctype"));
		
		
		homePageLib.logout();
		homePageLib.login(data.get("secondaryapprovalusername"), data.get("secondaryapprovalpassword"));
		homePageLib.openSecondaryApproval();
		new SecondaryApprovalPageLib().ApproveSelectedRow(claimNumber);
		*/
		//homePageLib.logout();
		status = true;
		// Sreekanth scriptResult = tcResult.PASSED; 
	}
/* Sreekanth
	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result)  throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
		if(tcLog != null){
			tcLog.setEndTime(dateFormatter.format(new Date()));
			tcLog.addAttachment(reporter.getDetailedFilePath(), "application/octet-stream");
			if (result.getStatus() == ITestResult.FAILURE){
				scriptResult = tcResult.FAILED;
				tcLog.addAttachment(reporter.getFailureScreenshot(), "application/octet-stream");
			}
			tcLog.setExecutionStatus(scriptResult);
			qTest.postTestRunLog(tcLog);
			scriptResult = tcResult.UNEXECUTED;
			jsCycleInfo = null;
			tcLog = null;
		}
		//Qtest.UpdateQTestResults(status, "Create Personal Auto claim for Verified Policy", startTime, endTime);
	}
	*/
}

