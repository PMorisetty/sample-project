package com.pure.test.claim.smoke;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.AddClaimInfoPageLib;
import com.pure.claim.ClaimCreationSummaryPageLib;
import com.pure.claim.EnumLOBType;
import com.pure.claim.HomePageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.SaveAndAssignClaimLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyBasicInformationPageLib;
import com.pure.claim.unverifiedPolicy.UnverifiedPolicyMainPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.utilities.TestUtil;

public class TC008_Unverified_Claim_WorkersComp extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "GeneralInfo");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "BasicInfo");
	}

	private Object[][] getTestDataFor_ClaimInfo() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "ClaimInfo");
	}

	private Object[][] getTestDataFor_ClaimSubmission() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "ClaimSubmission");
	}

	private Object[][] getTestDataFor_Coverages() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "Coverages");
	}

	private Object[][] getTestDataFor_SearchAddressBook() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "SearchAddressBook");
	}

	private Object[][] getTestDataFor_Exposures() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "Exposures");
	}

	private Object[][] getTestDataFor_Reserves() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "Reserves");
	}

	private Object[][] getTestDataFor_QuickCheck() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "QuickCheck");
	}

	private Object[][] getTestDataFor_Documents() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "Documents");
	}
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC008_Unverified_Claim_WorkersComp_Script", TestDataClaim, "TestDetails");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_GeneralInfo(),
				getTestDataFor_BasicInfo(),getTestDataFor_ClaimInfo(), getTestDataFor_ClaimSubmission(),
				getTestDataFor_Coverages(),getTestDataFor_SearchAddressBook(),getTestDataFor_Exposures(),
				getTestDataFor_Reserves(),getTestDataFor_QuickCheck(),getTestDataFor_Documents(),getTestDataFor_TestDetails());
	}

	@Test(dataProvider = "getTestData")
	public void TC008_Unverified_Claim_WorkersComp_Script(Hashtable<String, String> data) throws Throwable{
		String policyNumber = "DW" + (int) (new Date().getTime()/10000);
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		homePageLib.navigateToNewClaimInClaimMenu();
		NewClaimPageLib newClaimPageLib = new NewClaimPageLib(driver, reporter);
		newClaimPageLib.selectUnVerifiedPolicyOption();
		//Thread.sleep(2000);
		//Initial step in claim creation
		UnverifiedPolicyMainPageLib unverifiedPolicyMainPageLib = new UnverifiedPolicyMainPageLib(driver, reporter);
		unverifiedPolicyMainPageLib.setLOBType(EnumLOBType.WorkersComp);
		unverifiedPolicyMainPageLib.setPolicyNumber(policyNumber);
		unverifiedPolicyMainPageLib.setDateOfLoss(data.get("dateOfLoss"));

		//Basic Info section
		unverifiedPolicyMainPageLib.setEffectiveDate(data.get("effectiveDate"));
		unverifiedPolicyMainPageLib.setExpirationDate(data.get("expirationDate"));

		//Insured details
		unverifiedPolicyMainPageLib.fillInsuredDetails(data);

		//add policy level coverage for getting exposures
		//		unverifiedPolicyMainPageLib.addPolicyLevelCoverages(data);

		//navigate to next page
		newClaimPageLib.clickNext();

		//Verify navigated to step2
		//newClaimPageLib.verifyTitle("Step 2 of 4: Basic information");

		//Reported by info
		UnverifiedPolicyBasicInformationPageLib unverifiedPolicyBasicInformationPageLib = new UnverifiedPolicyBasicInformationPageLib(driver, reporter);
		unverifiedPolicyBasicInformationPageLib.setReportedBy(data.get("reporterName"));
		unverifiedPolicyBasicInformationPageLib.setRelationToInsured(data.get("relationToInsured"));

		//injured worker name
		unverifiedPolicyBasicInformationPageLib.setInjuredWorkerName(data.get("injuredWorkerName"));

		//Main contact info
		unverifiedPolicyBasicInformationPageLib.selectReporter(data.get("mainContact"), data.get("mainContactPersonName"));

		//navigate to next page
		newClaimPageLib.clickNext();

		//Verify navigated to step3
		//newClaimPageLib.verifyTitle("Step 3 of 4: Add claim information");

		//Enter loss details info
		AddClaimInfoPageLib addClaimInfoPageLib = new AddClaimInfoPageLib(driver, reporter);
		addClaimInfoPageLib.setLossDetails(data);
		addClaimInfoPageLib.setLossDescription(data.get("description"));
		addClaimInfoPageLib.setLossLocation(data);

		//navigate to next page
		newClaimPageLib.clickNext();

		//navigate to next page
		//newClaimPageLib.clickNext();

		//Verify navigated to step4
		//newClaimPageLib.verifyTitle("Step 4 of 4: Save and Assign Claim");

		//Fill adjuster details and note
		SaveAndAssignClaimLib saveAndAssignClaimLib = new SaveAndAssignClaimLib(driver, reporter);
		saveAndAssignClaimLib.selectAssignClaimAndAllExposuresOption();
		saveAndAssignClaimLib.selectName(data.get("assignClaimTo"),data.get("secondaryapprovalusername"));
		saveAndAssignClaimLib.setNote(data.get("additionalNote"));

		//Submit the claim
		saveAndAssignClaimLib.clickFinish();
		String claimNumber = new ClaimCreationSummaryPageLib(driver, reporter).getClaimNumber();


		
		
		
		
		
		
		
		/* cannot be executed beyond this as we cannot add exposures, so we can't make the payment
		String claimNumber = new ClaimCreationSummaryPageLib().getClaimNumber();
		new ClaimCreationSummaryPageLib().clickNewlyCreatedClaim();

		//Add reserves
		SummaryOverviewPageLib summaryOverviewPageLib = new SummaryOverviewPageLib();
		summaryOverviewPageLib.verifyLatestNotes(data.get("additionalNote"));
		summaryOverviewPageLib.getNumberOfExposuresAdded();

		ActionsPageLib actionsPageLib  = new ActionsPageLib();
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("AbilityToPay");

		SetReservesPageLib setReservesPageLib = new SetReservesPageLib();
		setReservesPageLib.addNewReserve(data);

		//QuickCheck basics & details
		actionsPageLib.clickAndHoverOnActions();
		actionsPageLib.clickOn("ValidateClaimAndExposure");
		actionsPageLib.clickOn("ValidForISO");
		new FinancialsPageLib().openQuickCheck();		
		new QuickCheckBasicsPageLib().fillDetails(data);;		
		new QuickChecksDetailsPageLib().fillDetails(data);;



		//document upload
		String pathToPolicyDocument = System.getProperty("user.dir") + "/TestData/PolicyDocuments/Policy_Info.txt";
		UploadDocumentsPageLib documentsPageLib = new UploadDocumentsPageLib();
		documentsPageLib.uploadDocument(pathToPolicyDocument, data.get("ShareOnMemberPortal"),
				data.get("docdescription"), data.get("doctype"));


		homePageLib.logout();
		homePageLib.login(data.get("secondaryapprovalusername"), data.get("secondaryapprovalpassword"));
		homePageLib.openSecondaryApproval();
		new SecondaryApprovalPageLib().ApproveSelectedRow(claimNumber);
		 */
		homePageLib.logout();
		status = true;
	}
}

