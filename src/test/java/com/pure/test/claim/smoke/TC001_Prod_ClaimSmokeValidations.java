package com.pure.test.claim.smoke;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import org.json.simple.parser.ParseException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.pure.accelerators.ActionEngine;
import com.pure.claim.ActionsPageLib;
import com.pure.claim.DocumentsPageLib;
import com.pure.claim.HomePageLib;
import com.pure.claim.MessageQueuesPageLib;
import com.pure.claim.NewClaimPageLib;
import com.pure.claim.NewDocumentPageLib;
import com.pure.claim.NewNotePageLib;
import com.pure.claim.PureAdminPageLib;
import com.pure.claim.RentalPageLib;
import com.pure.claim.SearchAddressBookPageLib;
import com.pure.claim.lifeCycle.BatchProcessInfoPageLib;
import com.pure.claim.lifeCycle.ClusterInfoPageLib;
import com.pure.claim.lifeCycle.NotesPageLib;
import com.pure.claim.lifeCycle.SummaryOverviewPageLib;
import com.pure.claim.verifiedPolicy.VerifiedPolicyLib;
import com.pure.utilities.TestUtil;

public class TC001_Prod_ClaimSmokeValidations extends ActionEngine{

	boolean status = false;
	Date startTime, endTime;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_Prod_ClaimSmokeValidations_Script", TestDataClaim, "LoginData");
	}

	private Object[][] getTestDataFor_GeneralInfo() {
		return TestUtil.getData("TC001_Prod_ClaimSmokeValidations_Script", TestDataClaim, "GeneralInfo");
	}
	private Object[][] getTestDataFor_Note() {
		return TestUtil.getData("TC001_Prod_ClaimSmokeValidations_Script", TestDataClaim, "Note");
	}

	private Object[][] getTestDataFor_Policy() {
		return TestUtil.getData("TC001_Prod_ClaimSmokeValidations_Script", TestDataClaim, "Policy");
	}
	private Object[][] getTestDataFor_AddressBook() {
		return TestUtil.getData("TC001_Prod_ClaimSmokeValidations_Script", TestDataClaim, "AddressBook");
	}
	private Object[][] getTestDataFor_Rental() {
		return TestUtil.getData("TC001_Prod_ClaimSmokeValidations_Script", TestDataClaim, "Rental");
	}
	private Object[][] getTestDataFor_ClusterInfo() {
		return TestUtil.getData("TC001_Prod_ClaimSmokeValidations_Script", TestDataClaim, "ClusterInfo");
	}
	private Object[][] getTestDataFor_Document() {
		return TestUtil.getData("TC001_Prod_ClaimSmokeValidations_Script", TestDataClaim, "Document");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(),getTestDataFor_GeneralInfo(),getTestDataFor_Note(),getTestDataFor_Policy(),getTestDataFor_AddressBook(),getTestDataFor_Rental(),getTestDataFor_ClusterInfo(),getTestDataFor_Document());
	}
	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
	}


	@Test(dataProvider = "getTestData")
	public void TC001_Prod_ClaimSmokeValidations_Script(Hashtable<String, String> data) throws Throwable{

		//TC001
		openApplication("Claim");
		HomePageLib homePageLib = new HomePageLib(driver, reporter);

		homePageLib.pureLogin(data.get("username_prod"), data.get("password_prod"));
		homePageLib.navigateToClaimCenterLink();
		homePageLib.verifyTitleOfClaimCenter();


		//TC002
		homePageLib.searchClaim(data.get("claimNo"));

		SummaryOverviewPageLib summaryoverviewpage = new SummaryOverviewPageLib(driver, reporter);
		String LossDate = summaryoverviewpage.getLossDateText();
		String NoticeDate = summaryoverviewpage.getNoticeDateText();
		String LossLocation = summaryoverviewpage.getLossLocationText();
		boolean condition = false;
		String message = "LossDate,NotcieDate,LossLocation Values Should Present";
		if(!(LossDate.equalsIgnoreCase("") || NoticeDate.equalsIgnoreCase("")||LossLocation.equalsIgnoreCase("")|| LossDate.equalsIgnoreCase("null") || NoticeDate.equalsIgnoreCase("null")||LossLocation.equalsIgnoreCase("null"))){
			condition = true;
			System.out.println("Loss Date :" + LossDate + " Notice Date :" + NoticeDate + " Loss Location :" + LossLocation);

		}
		if(condition){
			reporter.SuccessReport("LossDate,NoticeDate,Loss Location Values are Present "+ LossDate +" " + NoticeDate + " " + LossLocation,message);
		}else{
			reporter.softFailureReport("LossDate,NoticeDate,Loss Location Values are Present "+ LossDate +" " + NoticeDate + " " + LossLocation,message);
		}

		//TC010
		summaryoverviewpage.openNotesPage();
		NotesPageLib notespage = new NotesPageLib(driver, reporter);
		String Author = notespage.getAutorText();
		String Topic = notespage.getTopicText();
		condition = false;
		message = "Author,Topic Values Should Present";
		if(!(Author.equalsIgnoreCase("") || Topic.equalsIgnoreCase("")|| Author.equalsIgnoreCase("null") || Topic.equalsIgnoreCase("null"))){
			condition = true;
			System.out.println("Author :" + Author + " Topic :" + Topic);

		}
		if(condition){
			reporter.SuccessReport("Author Topic Values are Present "+ Author +" " + Topic,message);
		}else{
			reporter.softFailureReport("Author Topic Values are Not Present "+ Author +" " + Topic,message);
		}

		//TC011

		summaryoverviewpage.openDocumentsPage();
		DocumentsPageLib documentspage = new DocumentsPageLib(driver, reporter);
		String DocName = documentspage.getDocNameText();
		condition = false;
		message = "Document Should Present";
		if(!(DocName.equalsIgnoreCase("") || DocName.equalsIgnoreCase("null"))){
			condition = true;
			System.out.println("Doc Name :" + DocName);

		}
		if(condition){
			reporter.SuccessReport("Document Value is Present " + DocName,message);
		}else{
			reporter.softFailureReport("Document Value is Not Present " + DocName,message);
		}

		//TC012 --Not Priority

		/*	summaryoverviewpage.openServicesPage();
		ServicesPageLib servicespage = new ServicesPageLib(driver, reporter);
		String ServiceNumber = servicespage.getServiceNumber();
		condition = false;
		message = "Service Number Should Present";
		if(!(ServiceNumber.equalsIgnoreCase("") || ServiceNumber.equalsIgnoreCase("null"))){
			condition = true;
			System.out.println("Service Number :" + ServiceNumber);

		}
		if(condition){
			reporter.SuccessReport("Service Number is Present "+ ServiceNumber ,message);
		}else{
			reporter.softFailureReport("Service Number is Not Present "+ ServiceNumber ,message);
		}

		//TC013 --Not Priority
		summaryoverviewpage.openHistoryPage();
		HistoryPageLib HistoryPageLib = new HistoryPageLib(driver, reporter);
		String Type = HistoryPageLib.getTypeText();
		condition = false;
		message = "Type Should Present";
		if(!(Type.equalsIgnoreCase("") || Type.equalsIgnoreCase("null"))){
			condition = true;
			System.out.println("Type :" + Type);

		}
		if(condition){
			reporter.SuccessReport("Type Value is Present "+ Type ,message);
		}else{
			reporter.softFailureReport("Type Value is Not Present "+ Type,message);
		}*/

		//TC014

		ActionsPageLib actionspage = new ActionsPageLib(driver, reporter);
		actionspage.clickAndHoverOnActions();
		actionspage.clickOn("Note");
		NewNotePageLib newNotePageLib = new NewNotePageLib(driver, reporter);
		newNotePageLib.setShareOnMemberPortal(data.get("shareOnMemberPortal"));
		newNotePageLib.setTopic(data.get("topic"));
		newNotePageLib.setConfidential(data.get("confidential"));
		newNotePageLib.setText(data.get("text"));
		newNotePageLib.setSubject(data.get("Subject"));

		//click update button
		newNotePageLib.clickUpdate(data.get("text"));

		//TC015

		actionspage.clickAndHoverOnActions();
		actionspage.clickOn("AttachAnExistingDocument");
		NewDocumentPageLib newdocumentpagelib = new NewDocumentPageLib(driver, reporter);
	//	newdocumentpagelib.selectAttachment(data.get("Attachment"));
		newdocumentpagelib.setDescription(data.get("NewDocDescription"));
		newdocumentpagelib.setShareOnMemberPortal(data.get("NewDocShareOnMemberPortal"));
		newdocumentpagelib.setType(data.get("NewDocType"));
		//String newDocName = newdocumentpagelib.getNameText();
		newdocumentpagelib.clickUpdate();

		summaryoverviewpage.openDocumentsPage();
		documentspage.searchDocument(data.get("NewDocDescription"));
		
		DocName = documentspage.getDocNameText();
		String DocType = documentspage.getDocTypeText();
		String name = data.get("NewDocDescription")+" - "+data.get("NewDocType");

		condition = false;
		message = "Document Name and Document Type Should Match With Name,Type Provided in the New Document Page";
		if(DocName.contains(name)){
			reporter.SuccessReport("Document Name is Matching :",message);
		}else{
			reporter.softFailureReport("Document Name is Matching :",message);
		}
		if(DocType.equalsIgnoreCase(data.get("NewDocType"))){
			reporter.SuccessReport("Document Type is Matching :",message);
		}else{
			reporter.softFailureReport("Document Type is Matching :",message);
		}

		//TC016
		actionspage.clickAndHoverOnActions();
		actionspage.clickOn("Rental");
		RentalPageLib rentalpagelib = new RentalPageLib(driver, reporter);
		rentalpagelib.setZipCodeText(data.get("ZipCode"));
		rentalpagelib.clickSearch();


		//TC018
		homePageLib.navigateToNewClaimInClaimMenu();
		VerifiedPolicyLib verifiedPolicyLib = new VerifiedPolicyLib(driver, reporter);
		verifiedPolicyLib.setPolicyNumber(data.get("policyNum"));
		verifiedPolicyLib.setLossDate(data.get("dateOfLoss"));
		verifiedPolicyLib.clickSearchButton();
		new NewClaimPageLib(driver, reporter).clickNext();	
		verifiedPolicyLib.clickCancelWithPopUpHandle();

		//TC020
		homePageLib.navigateToAdministrationInAdministrationMenu();
		PureAdminPageLib pureadminpagelib = new PureAdminPageLib(driver, reporter);
		pureadminpagelib.openmessageQueuesPage();
		MessageQueuesPageLib messagequeueslib = new MessageQueuesPageLib(driver, reporter);
		String claimNotificationStatus = messagequeueslib.getClaimNotificationStatus();
	//	String documentStoreStatus = messagequeueslib.getDocumentStoreStatus();
		String paymentNotificationStatus = messagequeueslib.getPaymentNotificationStatus();
		String assignmentTransportStatus = messagequeueslib.getAssignmentTransportStatus();
		String contactMessageTransportStatus = messagequeueslib.getContactMessageTransportStatus();
		String emailStatus = messagequeueslib.getEmailStatus();
		String contactAutoSyncFailureStatus = messagequeueslib.getContactAutoSyncFailureStatus();
		String voidCheckEmailNotificationStatus = messagequeueslib.getCheckEmailNotificationStatus();
		String isoStatus = messagequeueslib.getISOStatus();
		String metroStatus = messagequeueslib.getMetroStatus();
		String xactAssignmentTransportStatus = messagequeueslib.getXactAssignmentTransportStatus();
		String asyncDocumentUploadStatus = messagequeueslib.getDocUploadStatus();

		condition = false;
		message = "Status Should be Started For All Destinations";
		if(claimNotificationStatus.equalsIgnoreCase("started")&& paymentNotificationStatus.equalsIgnoreCase("started")&& assignmentTransportStatus.equalsIgnoreCase("started")&&contactMessageTransportStatus.equalsIgnoreCase("started")&& emailStatus.equalsIgnoreCase("started")&& contactAutoSyncFailureStatus.equalsIgnoreCase("started")&& voidCheckEmailNotificationStatus.equalsIgnoreCase("started")&& isoStatus.equalsIgnoreCase("started")&& metroStatus.equalsIgnoreCase("started")&& xactAssignmentTransportStatus.equalsIgnoreCase("started")&& asyncDocumentUploadStatus.equalsIgnoreCase("started")){
			condition = true;
			System.out.println("claimNotificationStatus :" + claimNotificationStatus +
					"PaymentNotificationStatus :" + paymentNotificationStatus +
					"AssignmentTransportStatus :" + assignmentTransportStatus +
					"ContactMessageTransportStatus :" + contactMessageTransportStatus +
					"EmailStatus :" + emailStatus +
					"ContactAutoSyncFailureStatus :" + contactAutoSyncFailureStatus +
					"VoidCheckEmailNotificationStatus :" + voidCheckEmailNotificationStatus +
					"ISOStatus :" + isoStatus +
					"MetroStatus :" + metroStatus +
					"XactAssignmentTransportStatus :" + xactAssignmentTransportStatus +
					"AsyncDocumentUploadStatus :" + asyncDocumentUploadStatus 
					);

		}
		if(condition){
			reporter.SuccessReport("Destination Status is STARTED ",message);
		}else{
			reporter.softFailureReport("Destination Status is Not STARTED ",message);
		}

		//TC021

		String selectAll = Keys.chord(Keys.ALT, Keys.SHIFT,"T");
		homePageLib.enterClaimNumber(selectAll);
		Thread.sleep(160000);
		BatchProcessInfoPageLib batchprocesspagelib = new BatchProcessInfoPageLib(driver, reporter);
		batchprocesspagelib.openClusterInforPage();
		ClusterInfoPageLib clusterinfopagelib = new ClusterInfoPageLib(driver, reporter);
		boolean ServerIDPresent = clusterinfopagelib.verifyServerIdsInClusterMember(data);
		boolean clusterNowFlag = clusterinfopagelib.verifyClusterNowInClusterMember(data);
		message = "Server ID's Should Present and In Cluster Info Should be Yes";
		if(ServerIDPresent){
			reporter.SuccessReport("Server ID's Are Matching :",message);
		}else{
			reporter.softFailureReport("Server ID's Are Not Matching :",message);
		}
		if(clusterNowFlag){
			reporter.SuccessReport("In Cluster Info is Yes :",message);
		}else{
			reporter.softFailureReport("In Cluster Info is Not Yes :",message);
		}
		clusterinfopagelib.clickReturnToClaimCenter();

		//TC022
		homePageLib.navigateToSearchInAddressBookMenu();
		SearchAddressBookPageLib searchAddressBookPageLib = new SearchAddressBookPageLib(driver, reporter);
		searchAddressBookPageLib.setType(data.get("searchType"));
		searchAddressBookPageLib.setName(data.get("addressBookName"));
		searchAddressBookPageLib.clickSearch();
		searchAddressBookPageLib.clickNameSearchResult();
		name = searchAddressBookPageLib.getServiceProviderNameText();
		condition = false;
		message = "Name Value Should Present";
		if(!(name.equalsIgnoreCase("") || name.equalsIgnoreCase("null"))){
			condition = true;
			System.out.println("Name" + name);

		}
		if(condition){
			reporter.SuccessReport("Name Value is Present "+ name  ,message);
		}else{
			reporter.softFailureReport("Name Value is Not Present "+ name,message);
		}

	}

	@AfterMethod(alwaysRun = true)
	public void afterTest(ITestResult result)  throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		endTime = new Date();
	}
}


