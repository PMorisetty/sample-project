package com.pure.test.claim.smoke;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.claim.HomePageLib;
import com.pure.utilities.TestUtil;
import com.pure.utilities.Xls_Reader;


public class ClaimLogin extends ActionEngine{
	boolean status = false;
	Date startTime, endTime;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	JSONObject jsCycleInfo = null;

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC001_Verified_Claim_Auto_Script", TestDataClaim, "LoginData");
	}
	private Object[][] getTestDataFor_TestDetails(){
		return TestUtil.getData("TC001_Verified_Claim_Auto_Script", TestDataClaim, "TestDetails");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_TestDetails());
	}

	@Test(dataProvider = "getTestData")
	public void TC001_Verified_Claim_Auto_Script(Hashtable<String, String> data) throws Throwable{
		HomePageLib homePageLib = new HomePageLib(driver, reporter);
		openApplication("Claim");
		homePageLib.login(data.get("username"), data.get("password"));
		System.out.println("Successfully Logged into Claims");

	}
}
