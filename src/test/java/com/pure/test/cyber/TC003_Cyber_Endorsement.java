package com.pure.test.cyber;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.*;
import com.pure.dragon.homeOwners.*;
import com.pure.report.ConfigFileReadWrite;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC003_Cyber_Endorsement extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC003_Cyber_Endorsement", TestDataDragon, "LoginData");
	}

	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC003_Cyber_Endorsement", TestDataDragon, "AgencyDetails");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC003_Cyber_Endorsement", TestDataDragon, "BasicInfo");
	}

	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC003_Cyber_Endorsement", TestDataDragon, "QuoteDetails");
	}

	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC003_Cyber_Endorsement", TestDataDragon, "LocationDetails");
	}

	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC003_Cyber_Endorsement", TestDataDragon, "PreviousClaims");
	}

	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC003_Cyber_Endorsement", TestDataDragon, "MemberInfo");
	}

	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC003_Cyber_Endorsement", TestDataDragon, "UnderwriterRefferal");
	}

	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC003_Cyber_Endorsement", TestDataDragon, "DocumentDelivery");
	}
	private Object[][] getTestDataFor_Endorsement() {
		return TestUtil.getData("TC003_Cyber_Endorsement", TestDataDragon, "Endorsement");
	}
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC003_Cyber_Endorsement", TestDataDragon,"TestDetails");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_AgencyDetails(),
				getTestDataFor_BasicInfo(),getTestDataFor_QuoteDetails(), getTestDataFor_LocationDetails(),
				getTestDataFor_PreviousClaims(),getTestDataFor_MemberInfo(), getTestDataFor_UnderwriterRefferal(),
				getTestDataFor_DocumentDelivery(),getTestDataFor_Endorsement(),getTestDataFor_TestDetails());
	}
	@Test(dataProvider = "getTestData")
	public void TC003_Cyber_Endorsement_Script(Hashtable<String, String> data) throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			String newPolicyNumber = new QuoteCreation(driver, reporter,testCyber).createQuote(data);	
			System.out.println("Policy Number"+ newPolicyNumber);
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			//navigate to endorsement tab
			policyEndorsement.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsement.clickNewBtn();
			// enter the all the required data select cancellation,cancelled
			// by,effective date,notes.
			policyEndorsement.policyEndorsement(data.get("ChooseTransactionType"), data.get("EndorsementType"),
					data.get("EndorsementEffectiveDate"), data.get("Notes"));
			// Click on next button
			policyEndorsement.clickNextBtn();
			//update cyber limits and verify deductible and questions
			//		quoteDetailsPageLib.verifyCyberLimitQuestions();
			//change limit to 100,000 and rate and verify on premium tab
			QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
			quoteDetailsPageLib.selectCyberLimit("100,000");
			ChangeSummaryPageLib changeSummaryPageLib = new ChangeSummaryPageLib(driver, reporter);
			CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
			commonPageLib.clickReviewChangesBtn();
			changeSummaryPageLib.clickRateBtn();
			//verify and click more changes 
			EndorsementModifiedPremiumPageLib endorsementModifiedPremiumPageLib = new EndorsementModifiedPremiumPageLib(driver, reporter);
			endorsementModifiedPremiumPageLib.verifyCoverageExists("Fraud and Cyber Defense");
			endorsementModifiedPremiumPageLib.verifyCoverageLimits("Fraud and Cyber Defense", "100,000");
			endorsementModifiedPremiumPageLib.verifyCoverageDeductible("Fraud and Cyber Defense", "500");
			endorsementModifiedPremiumPageLib.verifyCoveragePremium("Fraud and Cyber Defense", "250");
			endorsementModifiedPremiumPageLib.clickMoreChangesButton();

			//change limit to 250,000 and rate and verify on premium tab
			quoteDetailsPageLib.selectCyberLimit("250,000");
			quoteDetailsPageLib.selectMoneyStolen("No");
			commonPageLib.clickReviewChangesBtn();
			changeSummaryPageLib.clickRateBtn();
			//verify and click more changes 
			endorsementModifiedPremiumPageLib.verifyCoverageExists("Fraud and Cyber Defense");
			endorsementModifiedPremiumPageLib.verifyCoverageLimits("Fraud and Cyber Defense", "250,000");
			endorsementModifiedPremiumPageLib.verifyCoverageDeductible("Fraud and Cyber Defense", "1,000");
			endorsementModifiedPremiumPageLib.verifyCoveragePremium("Fraud and Cyber Defense", "625");
			endorsementModifiedPremiumPageLib.clickReviewReferralsButton();


			UnderwriterAlertsPageLib underwriterAlertsPageLib = new UnderwriterAlertsPageLib(driver, reporter);
			UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
			uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
			uwReferralsPageLib.fillOverridden();
			uwReferralsPageLib.selectUnderwriter("Automation 0");
			underwriterAlertsPageLib.clickApprovedButton();
			//Issue endorsement
			endorsementModifiedPremiumPageLib.clickIssueButton();

			status = true;
		}

	}
	@AfterMethod
	public void afterTest() throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		//Qtest.UpdateQTestResults(status, "US4432_Endorsement", startTime, endTime);
	}
}
