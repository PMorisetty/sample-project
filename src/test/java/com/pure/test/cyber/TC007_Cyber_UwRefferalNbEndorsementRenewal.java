package com.pure.test.cyber;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.*;
import com.pure.dragon.homeOwners.*;
import com.pure.report.ConfigFileReadWrite;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC007_Cyber_UwRefferalNbEndorsementRenewal extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon, "LoginData");
	}

	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon, "AgencyDetails");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon, "BasicInfo");
	}

	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon, "QuoteDetails");
	}

	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon, "LocationDetails");
	}

	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon, "PreviousClaims");
	}

	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon, "MemberInfo");
	}

	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon, "UnderwriterRefferal");
	}

	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon, "DocumentDelivery");
	}
	private Object[][] getTestDataFor_Endorsement() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon, "Endorsement");
	}
	private Object[][] getTestDataFor_Renewal() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon, "Renewal");
	}
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC007_Cyber_UwRefferalNbEndorsementRenewal", TestDataDragon,
				"TestDetails");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_AgencyDetails(),
				getTestDataFor_BasicInfo(),getTestDataFor_QuoteDetails(), getTestDataFor_LocationDetails(),
				getTestDataFor_PreviousClaims(),getTestDataFor_MemberInfo(), getTestDataFor_UnderwriterRefferal(),getTestDataFor_TestDetails(),
				getTestDataFor_DocumentDelivery(),getTestDataFor_Endorsement(), getTestDataFor_Renewal());
	}
	@Test(dataProvider = "getTestData")
	public void TC007_Cyber_UwRefferalNbEndorsementRenewal_Script(Hashtable<String, String> data) throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			@SuppressWarnings("unused")
			String newPolicyNumber = new QuoteCreation(driver, reporter, testCyber).createQuote(data);	
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			//navigate to endorsement tab
			policyEndorsement.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsement.clickNewBtn();
			// enter the all the required data select cancellation,cancelled
			// by,effective date,notes.
			policyEndorsement.policyEndorsement(data.get("ChooseTransactionType"), data.get("EndorsementType"),
					data.get("EndorsementEffectiveDate"), data.get("Notes"));
			// Click on next button
			policyEndorsement.clickNextBtn();

			//change limit to 1,000,000 and rate and verify on premium tab
			QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
			quoteDetailsPageLib.selectCyberLimit(data.get("endorsementCyberLimit"));//1M in Endorsement
			quoteDetailsPageLib.selectMoneyStolen(data.get("endorsementIsMoneyStolen"));//No for referral question 3
			quoteDetailsPageLib.selectProtectedByActiveSub(data.get("endorsementIsProtectedByActiveSub"));
			ChangeSummaryPageLib changeSummaryPageLib = new ChangeSummaryPageLib(driver, reporter);
			CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
			commonPageLib.clickReviewChangesBtn();
			changeSummaryPageLib.clickRateBtn();

			//verify and click more changes 
			EndorsementModifiedPremiumPageLib endorsementModifiedPremiumPageLib = new EndorsementModifiedPremiumPageLib(driver, reporter);
			endorsementModifiedPremiumPageLib.clickReviewReferralsButton();

			UnderwriterAlertsPageLib underwriterAlertsPageLib = new UnderwriterAlertsPageLib(driver, reporter);
			//Verify referral question 2
			UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
			uwReferralsPageLib.verifyCyberUWRefferal2();
			uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
//			uwReferralsPageLib.fillOverridden();
			uwReferralsPageLib.selectUnderwriter("Automation 0");
			underwriterAlertsPageLib.clickApprovedButton();
			//Issue endorsement
			endorsementModifiedPremiumPageLib.clickIssueButton();
			//Exit transaction
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();

			// navigate to endorsement tab
			PolicyRenewalLib renewal = new PolicyRenewalLib(driver, reporter);
			// click on create renewal button
			renewal.clickCreateRenewalBtn();
			// click on renewal entry
			renewal.clickRenewalEntry();
			// navigate to policy image tab
			renewal.clickPolicyImageTab();
			//change limit to 250,000 and rate and verify on premium tab
			quoteDetailsPageLib.selectCyberLimit(data.get("renewalCyberLimit"));//250,000 in Renewal
			quoteDetailsPageLib.selectMoneyStolen(data.get("renewalIsMoneyStolen"));//Yes for referral question 3
			// click on review changes button
			renewal.clickReviewChangesBtn();
			// click on renewed button
			renewal.clickRenewedPremiumBtn();
			// click on review referrals button
			renewal.clickReviewReferalsBtn();
			//Verify referral question 3
			uwReferralsPageLib.verifyCyberUWRefferal3();
			// overriding the under writer notes
			uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
			uwReferralsPageLib.fillOverridden();
			// Accept the renewal
			renewal.clickAcceptBtn();
			// Process the renewal
			renewal.processRenewal(data, testCyber);
			//Exit transaction
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			status = true;
		}
	}
	@AfterMethod
	public void afterTest() throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		//		Qtest.UpdateQTestResults(status, "US4432_Endorsement", startTime, endTime);
	}
}
