package com.pure.test.cyber;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.CustomerSummaryPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.NewQuoteInformationPageLib;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.homeOwners.QuoteDetailsPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC012_Cyber_TwoQuotesFraudCheckForSameInsured extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));

	@BeforeMethod
	public void beforeMethod() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC012_Cyber_TwoQuotesFraudCheck",
				TestDataDragon, "LoginData");
	}

	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC012_Cyber_TwoQuotesFraudCheck",
				TestDataDragon, "AgencyDetails");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC012_Cyber_TwoQuotesFraudCheck",
				TestDataDragon, "BasicInfo");
	}

	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC012_Cyber_TwoQuotesFraudCheck",
				TestDataDragon, "QuoteDetails");
	}

	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC012_Cyber_TwoQuotesFraudCheck",
				TestDataDragon, "LocationDetails");
	}

	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC012_Cyber_TwoQuotesFraudCheck",
				TestDataDragon, "PreviousClaims");
	}

	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC012_Cyber_TwoQuotesFraudCheck",
				TestDataDragon, "MemberInfo");
	}

	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC012_Cyber_TwoQuotesFraudCheck",
				TestDataDragon, "UnderwriterRefferal");
	}

	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC012_Cyber_TwoQuotesFraudCheck",
				TestDataDragon, "DocumentDelivery");
	}

	private Object[][] getTestDataFor_Cancellation() {
		return TestUtil.getData("TC012_Cyber_TwoQuotesFraudCheck",
				TestDataDragon, "CancelPolicy");
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC012_Cyber_TwoQuotesFraudCheck", TestDataDragon, "TestDetails");
	}



	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(),getTestDataFor_TestDetails(),
				getTestDataFor_AgencyDetails(), getTestDataFor_BasicInfo(),
				getTestDataFor_QuoteDetails(),
				getTestDataFor_LocationDetails(),
				getTestDataFor_PreviousClaims(), getTestDataFor_MemberInfo(),
				getTestDataFor_UnderwriterRefferal(),
				getTestDataFor_DocumentDelivery(),
				getTestDataFor_Cancellation());
	}

	@Test(dataProvider = "getTestData")
	public void TC012_Cyber_TwoQuotesFraudCheck_Script(Hashtable<String, String> data) throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			String newPolicyNumber = new QuoteCreation(driver, reporter, testCyber).createQuote(data);
			new PolicyEndorsementLib(driver, reporter).navigateToCustomerDetails();
			CustomerSummaryPageLib customerSummaryPageLib = new CustomerSummaryPageLib(driver, reporter);
			customerSummaryPageLib.clickNewQuoteBtn();
			NewQuoteInformationPageLib newQuoteInformationPageLib = new NewQuoteInformationPageLib(driver, reporter);
			newQuoteInformationPageLib.fillDetails(data.get("insuranceLine"), data.get("state"), data.get("quoteName"),
					data.get("licensedProducer"), data.get("advisor"));
			newQuoteInformationPageLib.clickCreateQuoteBtn();
			QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
			quoteDetailsPageLib.setEffectiveDate(data.get("effectiveDate"));
			quoteDetailsPageLib.verifyCyberNotAvailable(newPolicyNumber);
			status = true;
		}
	}

	@AfterMethod
	public void afterMethod() throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		// Qtest.UpdateQTestResults(status, "US4441_Cancel and Reinstate",
		// startTime, endTime);
	}

}
