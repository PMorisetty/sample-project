package com.pure.test.cyber;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.CustomerListLib;
import com.pure.dragon.CustomerSummaryPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.NewQuoteBasicInformationLib;
import com.pure.dragon.PremiumPageLib;
import com.pure.dragon.SummaryPageLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC013_Cyber_Agent_UW_validation extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));

	@BeforeMethod
	public void beforeMethod() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC013_Cyber_Agent_UW_validation",
				TestDataDragon, "LoginData");
	}

	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC013_Cyber_Agent_UW_validation",
				TestDataDragon, "AgencyDetails");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC013_Cyber_Agent_UW_validation",
				TestDataDragon, "BasicInfo");
	}

	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC013_Cyber_Agent_UW_validation",
				TestDataDragon, "QuoteDetails");
	}

	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC013_Cyber_Agent_UW_validation",
				TestDataDragon, "LocationDetails");
	}

	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC013_Cyber_Agent_UW_validation",
				TestDataDragon, "PreviousClaims");
	}

	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC013_Cyber_Agent_UW_validation",
				TestDataDragon, "MemberInfo");
	}

	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC013_Cyber_Agent_UW_validation",
				TestDataDragon, "UnderwriterRefferal");
	}

	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC013_Cyber_Agent_UW_validation",
				TestDataDragon, "DocumentDelivery");
	}

	private Object[][] getTestDataFor_Cancellation() {
		return TestUtil.getData("TC013_Cyber_Agent_UW_validation",
				TestDataDragon, "CancelPolicy");
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC013_Cyber_Agent_UW_validation", TestDataDragon, "TestDetails");
	}



	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(),getTestDataFor_TestDetails(),
				getTestDataFor_AgencyDetails(), getTestDataFor_BasicInfo(),
				getTestDataFor_QuoteDetails(),
				getTestDataFor_LocationDetails(),
				getTestDataFor_PreviousClaims(), getTestDataFor_MemberInfo(),
				getTestDataFor_UnderwriterRefferal(),
				getTestDataFor_DocumentDelivery(),
				getTestDataFor_Cancellation());
	}

	@Test(dataProvider = "getTestData")
	public void TC013_Cyber_Agent_UW_validation_Script(Hashtable<String, String> data) throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			String customerID = new QuoteCreation(driver, reporter, testCyber).createQuoteAsAgent(data);
			//Login with UW
			HomeLib homeLib = new HomeLib(driver, reporter);
			homeLib.login(data.get("username"), data.get("password"));
			homeLib.navigateToCustomers();
			//Search for customer and open quote
			new CustomerListLib(driver, reporter).SelectCustomer(customerID);
			new CustomerSummaryPageLib(driver, reporter).selectQuote();
			NewQuoteBasicInformationLib newQuote_BasicInformationLib = new NewQuoteBasicInformationLib(driver, reporter);
			newQuote_BasicInformationLib.navigateToQuoteDetailsTab();

			// Click rate button
			CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
			commonPageLib.clickRateBtn();
			PremiumPageLib premiumPageLib = new PremiumPageLib(driver, reporter);
			premiumPageLib.verifyCoverageExists("Fraud and Cyber Defense");

			// Navigate to underwriter referrals section
			UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
			uwReferralsPageLib.clickUnderwriterReferralsTab();
			uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
			uwReferralsPageLib.fillOverridden();
			//Since 1 elem is left
			uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
			uwReferralsPageLib.fillOverridden();
			// Select Underwriter
			uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
			// Comments for broker
			uwReferralsPageLib.setCommentsForBroker(data.get("uwComments"));
			// Accept quote
			uwReferralsPageLib.clickAcceptButton();

			// Summary Page - Request issue
			SummaryPageLib summaryPageLib = new SummaryPageLib(driver, reporter);
			summaryPageLib.clickExitButton();

			//Logout of UW
			homeLib.logout();

			//Login as agent
			homeLib.loginAsAgent(data.get("brokerNumberAgent"), data.get("usernameAgent"), data.get("passwordAgent"));
			homeLib.navigateToCustomers_Agent();
			//Search for customer and open quote
			new CustomerListLib(driver, reporter).SelectCustomer(customerID);
			new CustomerSummaryPageLib(driver, reporter).selectQuote();
			premiumPageLib.navigateToPremiumDetailsTab();
			//Verify Premium tab is active
			premiumPageLib.verifyPremiumTabActive();
			premiumPageLib.verifyCoverageExists("Fraud and Cyber Defense");
			premiumPageLib.verifyCyberFraudFirstCoverage();
			status = true;
		}
	}

	@AfterMethod
	public void afterMethod() throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		// Qtest.UpdateQTestResults(status, "US4441_Cancel and Reinstate",
		// startTime, endTime);
	}

}
