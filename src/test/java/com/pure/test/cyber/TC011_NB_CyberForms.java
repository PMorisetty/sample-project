package com.pure.test.cyber;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.*;
import com.pure.dragon.homeOwners.*;
import com.pure.report.ConfigFileReadWrite;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC011_NB_CyberForms extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}

	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"TestDetails");
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"LoginData");
	}

	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"AgencyDetails");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"BasicInfo");
	}

	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"QuoteDetails");
	}

	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"LocationDetails");
	}

	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"PreviousClaims");
	}

	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"MemberInfo");
	}

	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"UnderwriterRefferal");
	}

	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"DocumentDelivery");
	}
	private Object[][] getTestDataFor_Endorsement() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"Endorsement");
	}
	private Object[][] getTestDataFor_Renewal() {
		return TestUtil.getData("TC011_NB_CyberForms", TestDataDragon,
				"Renewal");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(),
				getTestDataFor_TestDetails(),getTestDataFor_AgencyDetails(),
				getTestDataFor_BasicInfo(), getTestDataFor_QuoteDetails(),
				getTestDataFor_LocationDetails(),
				getTestDataFor_PreviousClaims(), getTestDataFor_MemberInfo(),
				getTestDataFor_UnderwriterRefferal(),
				getTestDataFor_DocumentDelivery(),
				getTestDataFor_Endorsement(),getTestDataFor_Renewal());
	}

	@Test(dataProvider = "getTestData")
	public void TC011_NB_CyberForms_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			@SuppressWarnings("unused")
			String newPolicyNumber = new QuoteCreation(driver, reporter, testCyber).createQuote(data);
			PolicyCurrentSummaryPageLib policyCurrentSummaryPageLib = new PolicyCurrentSummaryPageLib(driver, reporter);
			policyCurrentSummaryPageLib.downloadVerifyNBPolicy();

			/*
			 * Endorsement 1 - remove Cyber from policy
			 */
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			//navigate to endorsement tab
			policyEndorsement.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsement.clickNewBtn();

			policyEndorsement.policyEndorsement(data.get("ChooseTransactionType"), data.get("EndorsementType"),
					data.get("EndorsementEffectiveDate"), data.get("Notes1"));
			// Click on next button
			policyEndorsement.clickNextBtn();
			//Remove Cyber
			QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
			quoteDetailsPageLib.selectCyberDefenseCoverage(data.get("cyberDefenseCoverageE1"));
			CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
			commonPageLib.clickReviewChangesBtn();
			ChangeSummaryPageLib changeSummaryPageLib = new ChangeSummaryPageLib(driver, reporter);
			changeSummaryPageLib.clickRateBtn();
			EndorsementModifiedPremiumPageLib endorsementModifiedPremiumPageLib = new EndorsementModifiedPremiumPageLib(driver, reporter);
			//		endorsementModifiedPremiumPageLib.clickReviewReferralsButton();

			//Issue endorsement
			endorsementModifiedPremiumPageLib.clickIssueButton();
			//Exit transaction
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();

			/*
			 * Endorsement 2 - Add Cyber to policy
			 */
			//navigate to endorsement tab
			policyEndorsement.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsement.clickNewBtn();

			policyEndorsement.policyEndorsement(data.get("ChooseTransactionType"), data.get("EndorsementType"),
					data.get("EndorsementEffectiveDate"), data.get("Notes2"));
			// Click on next button
			policyEndorsement.clickNextBtn();
			//Add Cyber and Fraud Coverage
			quoteDetailsPageLib.selectCyberDefenseCoverage(data.get("cyberDefenseCoverageE2"));
			quoteDetailsPageLib.selectCyberLimit(data.get("cyberLimitE2"));
			commonPageLib.clickReviewChangesBtn();

			changeSummaryPageLib.clickRateBtn();

			//Issue endorsement
			endorsementModifiedPremiumPageLib.clickIssueButton();
			//Verify Cyber Form in Endorsement PDF
			policyCurrentSummaryPageLib.downloadVerifyEndorsementPolicy();
			//Exit transaction
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();

			/*
			 * Endorsement 3 - remove Cyber from policy for renewal transaction
			 */
			//navigate to endorsement tab
			policyEndorsement.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsement.clickNewBtn();
			// enter the all the required data select cancellation,cancelled
			// by,effective date,notes.
			policyEndorsement.policyEndorsement(data.get("ChooseTransactionType"), data.get("EndorsementType"),
					data.get("EndorsementEffectiveDate"), data.get("Notes3"));
			// Click on next button
			policyEndorsement.clickNextBtn();
			//Remove Cyber
			quoteDetailsPageLib.selectCyberDefenseCoverage(data.get("cyberDefenseCoverageE3"));
			commonPageLib.clickReviewChangesBtn();

			changeSummaryPageLib.clickRateBtn();

			//Issue endorsement
			endorsementModifiedPremiumPageLib.clickIssueButton();
			//Exit transaction
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();

			/*
			 * Renewal - Add Cyber to policy
			 */
			// navigate to endorsement tab
			PolicyRenewalLib renewal = new PolicyRenewalLib(driver, reporter);
			// click on create renewal button
			renewal.clickCreateRenewalBtn();
			// click on renewal entry
			renewal.clickRenewalEntry();
			// navigate to policy image tab
			renewal.clickPolicyImageTab();
			quoteDetailsPageLib.selectCyberLimit(data.get("renewalCyberLimit"));
			quoteDetailsPageLib.selectMoneyStolen(data.get("renewalIsMoneyStolen"));
			// click on review changes button
			renewal.clickReviewChangesBtn();
			// click on renewed button
			renewal.clickRenewedPremiumBtn();
			// click on review referrals button
			renewal.clickReviewReferalsBtn();
			// overriding the under writer notes
			UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
			uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
			uwReferralsPageLib.fillOverridden();
			// Accept the renewal
			renewal.clickAcceptBtn();
			// Process the renewal
			renewal.processRenewal(data, testCyber);
			//Verify Cyber Form in Endorsement PDF
			policyCurrentSummaryPageLib.downloadVerifyRenewalPolicy();
			//Exit transaction
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			status = true;
		}
	}

	@AfterMethod
	public void afterTest() throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		// Qtest.UpdateQTestResults(status, "US4441_Quote Creation", startTime,
		// endTime);
	}
}
