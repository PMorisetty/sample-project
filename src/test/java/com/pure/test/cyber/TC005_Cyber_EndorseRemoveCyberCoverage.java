package com.pure.test.cyber;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.ChangeSummaryPageLib;
import com.pure.dragon.EndorsementModifiedPremiumPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.dragon.homeOwners.QuoteDetailsPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC005_Cyber_EndorseRemoveCyberCoverage	extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData(
				"TC005_Cyber_EndorseRemoveCyberCoverage",
				TestDataDragon, "LoginData");
	}

	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData(
				"TC005_Cyber_EndorseRemoveCyberCoverage",
				TestDataDragon, "AgencyDetails");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData(
				"TC005_Cyber_EndorseRemoveCyberCoverage",
				TestDataDragon, "BasicInfo");
	}

	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData(
				"TC005_Cyber_EndorseRemoveCyberCoverage",
				TestDataDragon, "QuoteDetails");
	}

	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData(
				"TC005_Cyber_EndorseRemoveCyberCoverage",
				TestDataDragon, "LocationDetails");
	}

	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData(
				"TC005_Cyber_EndorseRemoveCyberCoverage",
				TestDataDragon, "PreviousClaims");
	}

	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData(
				"TC005_Cyber_EndorseRemoveCyberCoverage",
				TestDataDragon, "MemberInfo");
	}

	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData(
				"TC005_Cyber_EndorseRemoveCyberCoverage",
				TestDataDragon, "UnderwriterRefferal");
	}
	private Object[][] getTestDataFor_Endorsement() {
		return TestUtil.getData("TC005_Cyber_EndorseRemoveCyberCoverage", TestDataDragon, "Endorsement");
	}
	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData(
				"TC005_Cyber_EndorseRemoveCyberCoverage",
				TestDataDragon, "DocumentDelivery");
	}
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC005_Cyber_EndorseRemoveCyberCoverage", TestDataDragon,
				"TestDetails");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(),
				getTestDataFor_AgencyDetails(), getTestDataFor_BasicInfo(),
				getTestDataFor_QuoteDetails(),
				getTestDataFor_LocationDetails(),
				getTestDataFor_PreviousClaims(), getTestDataFor_MemberInfo(),
				getTestDataFor_UnderwriterRefferal(),getTestDataFor_TestDetails() ,
				getTestDataFor_DocumentDelivery(),getTestDataFor_Endorsement());
	}

	@Test(dataProvider = "getTestData")
	public void TC005_Cyber_EndorseRemoveCyberCoverage_Script(
			Hashtable<String, String> data) throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			String newPolicyNumber = new QuoteCreation(driver, reporter, testCyber).createQuote(data);
			System.out.println("Policy Number::::::::::::" + newPolicyNumber);
			//String newPolicyNumber = "HO233396900";
			// Policy Endorsement (Remove Cyber coverage) process started HO233396900
			// navigate to endorsement tab
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			policyEndorsement.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsement.clickNewBtn();
			// enter the all the required data select cancellation,cancelled
			// by,effective date,notes.
			policyEndorsement.policyEndorsement(data.get("ChooseTransactionType"),
					data.get("EndorsementType"),
					data.get("EndorsementEffectiveDate"), data.get("Notes"));
			// Click on next button
			policyEndorsement.clickNextBtn();
			// update cyber coverage with No
			QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
			quoteDetailsPageLib.selectCyberDefenseCoverage(data
					.get("cyberDefenseCoverageNo"));
			CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
			commonPageLib.clickReviewChangesBtn();
			ChangeSummaryPageLib changeSummaryPageLib = new ChangeSummaryPageLib(driver, reporter);
			changeSummaryPageLib.clickRateBtn();
			EndorsementModifiedPremiumPageLib endorsementModifiedPremiumPageLib = new EndorsementModifiedPremiumPageLib(driver, reporter);
			endorsementModifiedPremiumPageLib.clickIssueButton();
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			status = true;
		}

	}

	@AfterMethod
	public void afterTest() throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		//Qtest.UpdateQTestResults(status, "Policy Create,Endorse on remove cyber coverage Success",startTime,	endTime);
	}
}
