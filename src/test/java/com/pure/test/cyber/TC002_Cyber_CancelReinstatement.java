package com.pure.test.cyber;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.DiaryLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PoliciesPageLib;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.enumSearchBy;
import com.pure.report.ConfigFileReadWrite;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC002_Cyber_CancelReinstatement extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "TestDetails");
	}
	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "LoginData");
	}

	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "AgencyDetails");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "BasicInfo");
	}

	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "QuoteDetails");
	}

	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "LocationDetails");
	}

	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "PreviousClaims");
	}

	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "MemberInfo");
	}

	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "UnderwriterRefferal");
	}

	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "DocumentDelivery");
	}
	private Object[][] getTestDataFor_PolicyCancle() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "CancelPolicy");
	}
	private Object[][] getTestDataFor_PolicyReinstatement() {
		return TestUtil.getData("TC002_Cyber_CancelPolicy", TestDataDragon, "ReinstatementPolicy");
	}
	@DataProvider
	private Object[][] getTestData() throws ParseException{
		return TestUtil.getAllData(getTestDataFor_Login(), getTestDataFor_AgencyDetails(),
				getTestDataFor_BasicInfo(),getTestDataFor_QuoteDetails(), getTestDataFor_LocationDetails(),
				getTestDataFor_PreviousClaims(),getTestDataFor_MemberInfo(), getTestDataFor_UnderwriterRefferal(),
				getTestDataFor_DocumentDelivery(),getTestDataFor_PolicyCancle(),getTestDataFor_PolicyReinstatement(),
				getTestDataFor_TestDetails());
	}
	@Test(dataProvider = "getTestData")
	public void TC002_Cyber_CancelReinstatement_Script(Hashtable<String, String> data) throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			String newPolicyNumber = new QuoteCreation(driver, reporter,testCyber).createQuote(data);	
			System.out.println("Policy Number::::::::::::" + newPolicyNumber );
			// policy Cancellation process started
			// policies tab navigation
			new HomeLib(driver, reporter).navigateToPolicies();
			// search & select policy
			PoliciesPageLib policiespage = new PoliciesPageLib(driver, reporter);
			policiespage.searchPolicy(enumSearchBy.PolicyNumber,newPolicyNumber );
			//Select policy
			policiespage.clickSearchedPolicy(newPolicyNumber);
			PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
			//navigate to endorsement tab
			policyEndorsement.navigateToTransationOrEndorsementTab();
			// click on new button
			policyEndorsement.clickNewBtn();
			// enter the all the required data select cancellation,cancelled
			// by,effective date,notes.
			policyEndorsement.policyCancellationFirstStep(data.get("ChooseTransactionType"), data.get("RequestedBy"),
					data.get("NewTransactionEffectiveDate"), data.get("Notes"));
			// Click on next button
			policyEndorsement.clickNextBtn();
			// enter the all the required data select cancellation method,reason.
			policyEndorsement.policyCancellationSecondStep(data.get("CancelMethod"), data.get("CancelReason"));
			policyEndorsement.clickProcessBtn();
			DiaryLib diary = new DiaryLib(driver, reporter);
			diary.clickExitTransaction();
			//diary check, Navigate to diary tab & get status
			diary.clickCancelEntry();
			diary.navigateToDairyTab();
			diary.verifyDiaryCancellationStatus();
			diary.clickExitTransaction();
			// policy Reinstatement process started
			// policies tab navigation
			new HomeLib(driver, reporter).navigateToPolicies();
			// Search policy
			policiespage.searchPolicy(enumSearchBy.PolicyNumber,newPolicyNumber );
			//Select policy
			policiespage.clickSearchedPolicy(newPolicyNumber);
			//navigate to endorsement tab
			policyEndorsement.navigateToTransationOrEndorsementTab();
			// Click on new button
			policyEndorsement.clickNewBtn();
			// enter the all the required data select Reinstatement method,notes,effectivedate....
			policyEndorsement.policyReinstatementFirstStep(data.get("ChooseTransactionTypeforRein"),
					data.get("NewTransactionEffectiveDateforRein"), data.get("NotesforRein"), data.get("DescriptionforRein"));
			// Click on next button
			policyEndorsement.clickNextBtn();
			// enter the all the required data select Reinstatement claim loss info,reason.
			policyEndorsement.policyReinstatementSecondStep(data.get("ClaimLossPostCancellation"), data.get("Reinstatement Reason"));
			//Click on Process button
			policyEndorsement.clickProcessBtn();
			policyEndorsement.navigateToTransationOrEndorsementTab();
			diary.clickReinstatementEntry();
			diary.navigateToDairyTab();
			diary.verifyDiaryReinStatementStatus();
			status = true;
		}
	}
	@AfterMethod
	public void afterTest() throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		//Qtest.UpdateQTestResults(status, "US4441_Cancel and Reinstate", startTime, endTime);
	}
}
