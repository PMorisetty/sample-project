package com.pure.test.cyber.commons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.*;
import com.pure.dragon.homeOwners.*;
import com.pure.dragon.excessLiability.AdditionalInsuredPageLib;
import com.pure.dragon.excessLiability.PropertyInformationPageLib;
import com.pure.dragon.excessLiability.SelectCoveragesPageLib;
import com.pure.dragon.excessLiability.WaterCraftInformationPageLib;
import com.pure.dragon.personalAuto.CoveragesPageLib;
import com.pure.dragon.personalAuto.DriverDetailPageLib;
import com.pure.dragon.personalAuto.MemberInformationAndPolicyHistoryPageLib;
import com.pure.dragon.personalAuto.NavigationPageLib;
import com.pure.dragon.personalAuto.OperatorsAndVehiclesPage;
import com.pure.dragon.personalAuto.OperatorsAndVehiclesPageLib;
import com.pure.dragon.personalAuto.VehicleDetailPageLib;
import com.pure.report.CReporter;
import com.pure.report.TestResult;
import com.pure.dragon.homeOwners.QuoteDetailsPage;


public class QuoteCreation extends ActionEngine{
	private boolean testCyber;
	public String BaseRateValue="";
	public String CreditScoreFactor="";
	public String AgeOfHomeFactor="";
	public String AOPDeductibleFactor="";
	public double OptionalCoveragePremium=0;	
	public double TotalPremiumValue =0;
	public double SurplusContributionValue =0;
	public double GrandTotalvalue =0;
	public double LocationPremiumValue=0;
	public double TotalLocationPremiumValue=0;
	public double AdditionalRatePerThousand=0;
	public String liabilityPremium="";
	public String liabilityExtensionRate="";
	public String liabilityExtensionNoOfPremises="";
	public String finalPartialLimitsFactorDwelling="";
	public String finalPartialLimitsFactorContents="";
	public String tierFactorV2BasePremium="";
	public String tierFactorV2WindPremium="";
	public String quoteNumber="";
	public String BaseRateNonHurricaneValue="";
	public String BaseRateHurricaneValue="";

	public HashMap<String, String> policyDetails;


	public QuoteCreation(EventFiringWebDriver webDriver, CReporter reporter, boolean testCyber) {
		// TODO Auto-generated constructor stub
		this.driver = webDriver;
		this.reporter = reporter;
		this.testCyber = testCyber;				

	}
	public QuoteCreation(EventFiringWebDriver webDriver, CReporter reporter) {
		// TODO Auto-generated constructor stub
		this.driver = webDriver;
		this.reporter = reporter;
		this.testCyber = false;				

	}
	/*Function to create quote with Cyber*/
	public String createQuote(Hashtable<String, String> data) throws Throwable {
		openApplication("UW");
		initiateQuoteCreation(data);		
		// Fill Quote details
		QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
		//Credit score
		if(data.get("Insurance_Score")!=null && data.get("Insurance_Score")!="0"){
			quoteDetailsPageLib.navigateToCustomer();
			new InsuranceScoreManagementLib(driver, reporter).addNewInsuranceScore(data);					
		}	
		quoteDetailsPageLib.fillDetails(data, testCyber);
		CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
		commonPageLib.clickNextBtn();

		// Location coverage section
		// Usage details
		LocationCoveragePageLib locationCoveragePageLib = new LocationCoveragePageLib(driver, reporter);
		locationCoveragePageLib.selectStandardizedAddress();
		commonPageLib.clickOrderPropertyDetailsBtn();

		locationCoveragePageLib.fillLocationCoverageInfo(data);
		// Construction details
		locationCoveragePageLib.fillConstructionDetails(data);
		//Wind Mitigation characteristics
		locationCoveragePageLib.selectWindMitigation(data);
		// protection details
		locationCoveragePageLib.fillProtectionInfo(data);
		// Flood info
		locationCoveragePageLib.selectFloodInfo(data);
		// Elevation Certificate
		locationCoveragePageLib.fillElevationCertificateDetails(data);
		//Order property details
		//		commonPageLib.clickOrderPropertyDetailsBtn();
		//		locationCoveragePageLib.selectProtectionClass_Backup();
		//only for KY
		//locationCoveragePageLib.setUwTax(data.get("uwTaxValue"));
		// Next to Optional Coverage section
		commonPageLib.clickNextBtn();

		OptionalCoveragesPageLib optionalCoveragesPageLib = new OptionalCoveragesPageLib(driver, reporter);
		optionalCoveragesPageLib.fillOptionalfloodCoverageDetails(data);
		optionalCoveragesPageLib.selectOptionalCoverage(data);
		optionalCoveragesPageLib.fillOptionalCoveragesOrExclusionsDetails(data);
		// Next to Earthquake section
		commonPageLib.clickNextBtnEarthquake(data);

		//Next to earthquake section - for CA state
		new EarthquakePageLib(driver, reporter).fillEarthquakeDetails(data);

		// Next to Wildfire section
		commonPageLib.clickNextBtnWildfire(data);

		// Next to Residence Employee section
		commonPageLib.clickNextBtnResidenceEmployee(data);

		// Next to section
		commonPageLib.clickNextBtnElevationCertificate(data);
		
		// Elevation Certificate
		locationCoveragePageLib.fillElevationCertificateDetails(data);
		
		// Next to Previous claim section
		commonPageLib.clickNextBtn();

		PreviousClaimDetailsPageLib previousClaimDetailsPageLib = new PreviousClaimDetailsPageLib(driver, reporter);
		previousClaimDetailsPageLib.selectPriorLoss(data);
		// Next to Wildfire section - CO state
		commonPageLib.clickNextBtnConditional(data);

		// Next to Additional interests section
		commonPageLib.clickNextBtn();

		//Inspection details
		commonPageLib.fillInspectionDetails(data);

		new AdditionalInterestsPageLib(driver, reporter).fillAdditionalInterestDetails(data);
		// Next to Member Information section
		new AdditionalInterestsPageLib(driver, reporter).setMortgageonProperty(data.get("mortgageOnTheProperty"));
		commonPageLib.clickNextBtn();

		// Member information
		MemberInformationPageLib memberInformationPageLib = new MemberInformationPageLib(driver, reporter);
		memberInformationPageLib.fillMemberInfo(data);

		// Next to Manuscript Endorsement section
		commonPageLib.clickNextBtn();
		new ManuscriptEndorsementsPageLib(driver, reporter).fillManuscriptEndorsementDetails(data);

		// Next to Application section
		commonPageLib.clickNextBtn();

		// Next to Subjectivities section
		commonPageLib.clickNextBtn();

		// Click rate button
		commonPageLib.clickRateBtn();

		//For CA State
		commonPageLib.handleErrorMsg(data);

		PremiumPageLib premiumPageLib = new PremiumPageLib(driver, reporter);
		TotalPremiumValue = premiumPageLib.getTotalPremiumValue();
		SurplusContributionValue = premiumPageLib.getSurplusContributionValue();
		GrandTotalvalue = premiumPageLib.getGrandTotalvalue();
		LocationPremiumValue = premiumPageLib.getLocationPremiumValue();
		TotalLocationPremiumValue = premiumPageLib.getTotalLocationPremiumValue();
		quoteNumber = premiumPageLib.getQuoteNumber();
		
		if(!testCyber){
			OptionalCoveragePremium = premiumPageLib.getOptionalCoveragePremium();
			premiumPageLib.clickPremiumDetailsButton();
			PremiumDetailsLib premiumDetailsLib = new PremiumDetailsLib(driver, reporter);
			if(data.get("state").equalsIgnoreCase("NJ")){
				BaseRateNonHurricaneValue= premiumDetailsLib.getBaseRateNonHurricaneValue();
				BaseRateHurricaneValue= premiumDetailsLib.getBaseRateHurricaneValue();
			}else{
				BaseRateValue= premiumDetailsLib.getBaseRateValue();
			}
			CreditScoreFactor=premiumDetailsLib.getCreditScoreFactor();
			AgeOfHomeFactor=premiumDetailsLib.getAgeOfHomeFactor();
			AOPDeductibleFactor=premiumDetailsLib.getAOPDeductibleFactor();
			AdditionalRatePerThousand = premiumPageLib.getAdditionalRatePerThousand();
			premiumDetailsLib.clickReturnToPremiumSummaryPage();
		}

		if(testCyber){
			// Verify Premium tab is active
			premiumPageLib.verifyPremiumTabActive();
			premiumPageLib.verifyCoverageExists("Fraud and Cyber Defense");
			premiumPageLib.verifyCyberFraudFirstCoverage();
			premiumPageLib.verifyCoverageLimits("Fraud and Cyber Defense", data.get("cyberLimit"));
			premiumPageLib.verifyCoverageDeductible("Fraud and Cyber Defense", data.get("cyberDeductible"));
		}
		// Navigate to underwriter referrals section
		UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
		uwReferralsPageLib.clickUnderwriterReferralsTab();

		if(testCyber){
			uwReferralsPageLib.verifyCyberUWRefferal(data);
		}
		uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
		uwReferralsPageLib.fillOverridden();
		// Select Underwriter
		if("PROD".equalsIgnoreCase(region)){
			uwReferralsPageLib.selectUnderwriter(data.get("uwName_prod"));
		}else{
			uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
		}
		//		uwReferralsPageLib.selectUnderwriter();

		// Comments for broker
		uwReferralsPageLib.setCommentsForBroker(data.get("uwComments"));
		// Accept quote
		uwReferralsPageLib.clickAcceptButton();

		// Summary Page - Request issue
		SummaryPageLib summaryPageLib = new SummaryPageLib(driver, reporter);
		summaryPageLib.clickRequestIssueButton();
		premiumPageLib.clickQuoteProposalButton();
		//		for FL LA and NC state
		uwReferralsPageLib.fillUnderwriterDetailsForFL(data);
		// Verify premium tab is active
		// premiumPageLib.verifyPremiumTabActive();
		locationCoveragePageLib.overrideUwTax(data);
		// Bind policy
		premiumPageLib.clickBindButton();

		// Select document delivery type
		PolicyDeliveryPageLib policyDeliveryPageLib = new PolicyDeliveryPageLib(driver, reporter);
		policyDeliveryPageLib.selectDocumentDeliveryType(data);

		// Click next to go to Billing section
		policyDeliveryPageLib.clickNextButton();

		// Select bill delivery
		BillingPageLib billingPageLib = new BillingPageLib(driver, reporter);
		billingPageLib.selectBillingAddress("SendToBilling");

		// click confirm button
		billingPageLib.clickConfirmBtn();

		// Request Bind
		FinalSummaryPageLib finalSummaryPageLib = new FinalSummaryPageLib(driver, reporter);
		finalSummaryPageLib.clickRequestBind();

		// Verify generated policy number
		PolicyCurrentSummaryPageLib policyCurrentSummaryPageLib = new PolicyCurrentSummaryPageLib(driver, reporter);
		policyCurrentSummaryPageLib.verifyPolicyGenerated();
		PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
		//navigate to endorsement tab
		policyEndorsement.navigateToTransationOrEndorsementTab();
		policyEndorsement.navigateToNBtransaction();
		policyEndorsement.navigateToSubjectivities();

		// override subjectivities
		SubjectivitiesPageLib subjectivitiesPageLib = new SubjectivitiesPageLib(driver, reporter);
		subjectivitiesPageLib.overrideValues();
		// Manually bind the policy
		subjectivitiesPageLib.clickManualBindButton();
		return policyCurrentSummaryPageLib.getPolicyNumber();
	}

	public String createQuoteAsAgent(Hashtable<String, String> data) throws Throwable {
		HomeLib homeLib = new HomeLib(driver, reporter);
		CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
		openApplication("UW");
		homeLib.loginAsAgent(data.get("brokerNumberAgent"), data.get("usernameAgent"), data.get("passwordAgent"));
		homeLib.navigateToQuotes_Agent();

		//Fill quote basic information
		NewQuoteBasicInformationLib newQuote_BasicInformationLib = new NewQuoteBasicInformationLib(driver, reporter);
		newQuote_BasicInformationLib.fillDetails(data);
		newQuote_BasicInformationLib.clickNxtBtn();

		// Fill Quote details
		QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
		quoteDetailsPageLib.fillDetails(data,testCyber);
		commonPageLib.clickNextBtn();

		// Location coverage section
		// Usage details
		LocationCoveragePageLib locationCoveragePageLib = new LocationCoveragePageLib(driver, reporter);
		locationCoveragePageLib.fillLocationCoverageInfo(data);
		// Construction details
		locationCoveragePageLib.fillConstructionDetails(data);
		// Wind Mitigation characteristics
		locationCoveragePageLib.selectWindMitigation(data);
		// protection details
		locationCoveragePageLib.fillProtectionInfo(data);
		// Flood info
		locationCoveragePageLib.selectFloodInfo(data);
		// Next to Optional Coverage section
		commonPageLib.clickNextBtn();

		// Next to Previous claim section
		commonPageLib.clickNextBtn();
		PreviousClaimDetailsPageLib previousClaimDetailsPageLib = new PreviousClaimDetailsPageLib(driver, reporter);
		previousClaimDetailsPageLib.selectPriorLoss(data);
		// Next to Additional interests section
		commonPageLib.clickNextBtn();
		//Inspection details
		commonPageLib.fillInspectionDetails(data);
		// Next to Member Information section
		commonPageLib.clickNextBtn();

		// Member information
		MemberInformationPageLib memberInformationPageLib = new MemberInformationPageLib(driver, reporter);
		memberInformationPageLib.fillMemberInfo(data);

		// Next to Manuscript Endorsement section
		commonPageLib.clickNextBtn();

		// Next to Application section
		commonPageLib.clickNextBtn();

		// Click rate button
		commonPageLib.clickRateBtn();

		PremiumPageLib premiumPageLib = new PremiumPageLib(driver, reporter);
		// Verify Premium tab is active
		premiumPageLib.verifyPremiumTabActive();
		premiumPageLib.verifyCoverageUnavailability("Fraud and Cyber Defense");
		String customerID = premiumPageLib.getCustomerID();
		premiumPageLib.clickReferToUWButton();

		premiumPageLib.clickContinueUWReferButton();
		premiumPageLib.fillCommentsSubmitToUW(data.get("brokerComments"));
		//Agent logout
		homeLib.agentLogout();
		return customerID;

	}

	/*Function to create quote with Cyber*/
	public String createQuotePredictiveAnalytics(Hashtable<String, String> data) throws Throwable {		
		openApplication("PA");
		initiateQuoteCreation(data);		
		Thread.sleep(2000);
		if(isVisibleOnly(NewQuoteBasicInformationPage.nxtBtn, "Next Button")){
			new NewQuoteBasicInformationLib(driver, reporter).clickNxtBtn();
		}


		// Fill Quote details
		QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
		quoteDetailsPageLib.fillDetailsPredictiveAnalytics(data);
		CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
		commonPageLib.clickNextBtn();

		// Location coverage section
		// Usage details
		LocationCoveragePageLib locationCoveragePageLib = new LocationCoveragePageLib(driver, reporter);
		locationCoveragePageLib.selectStandardizedAddress();
		commonPageLib.clickOrderPropertyDetailsBtn();

		locationCoveragePageLib.fillLocationCoverageInfo(data);
		// Construction details
		locationCoveragePageLib.fillConstructionDetails(data);
		//Wind Mitigation characteristics
		locationCoveragePageLib.selectWindMitigation(data);
		// protection details
		locationCoveragePageLib.fillProtectionInfo(data);
		//additional protection info
		locationCoveragePageLib.fillAdditionalProtectionInfo(data);
		// Flood info
		locationCoveragePageLib.selectFloodInfo(data);
		//Order property details
		//		commonPageLib.clickOrderPropertyDetailsBtn();
		//		locationCoveragePageLib.selectProtectionClass_Backup();
		//only for KY
		//locationCoveragePageLib.setUwTax(data.get("uwTaxValue"));
		// Next to Optional Coverage section
		commonPageLib.clickNextBtn();
		new OptionalCoveragesPageLib(driver, reporter).selectOptionalCoverage(data);
		// Next to Earthquake section
		commonPageLib.clickNextBtnEarthquake(data);

		//Next to earthquake section - for CA state
		new EarthquakePageLib(driver, reporter).fillEarthquakeDetails(data);

		// Next to Wildfire section
		commonPageLib.clickNextBtnWildfire(data);

		// Next to Residence Employee section
		commonPageLib.clickNextBtnResidenceEmployee(data);

		// Next to section
		commonPageLib.clickNextBtnElevationCertificate(data);
		// Next to Previous claim section
		commonPageLib.clickNextBtn();

		PreviousClaimDetailsPageLib previousClaimDetailsPageLib = new PreviousClaimDetailsPageLib(driver, reporter);
		previousClaimDetailsPageLib.selectPriorLoss(data);
		// Next to Wildfire section - CO state
		commonPageLib.clickNextBtnConditional(data);
		// Next to Additional interests section
		commonPageLib.clickNextBtn();

		//Inspection details
		commonPageLib.fillInspectionDetails(data);
		// Next to Member Information section
		commonPageLib.clickNextBtn();

		// Member information
		MemberInformationPageLib memberInformationPageLib = new MemberInformationPageLib(driver, reporter);
		memberInformationPageLib.fillMemberInfo(data);

		// Next to Manuscript Endorsement section
		commonPageLib.clickNextBtn();

		// Next to Application section
		commonPageLib.clickNextBtn();

		// Next to Subjectivities section
		commonPageLib.clickNextBtn();
		// Click save button
		commonPageLib.clickSaveChangesBtn();
		// Click rate button
		commonPageLib.clickRateBtn();
		PremiumPageLib premiumPageLib = new PremiumPageLib(driver, reporter);
		return premiumPageLib.getQuoteNumber();
		/*
		PremiumPageLib premiumPageLib = new PremiumPageLib(driver, reporter);
		// Navigate to underwriter referrals section
		UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
		uwReferralsPageLib.clickUnderwriterReferralsTab();
		uwReferralsPageLib.verifyCyberUWRefferal(data);
		uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
		uwReferralsPageLib.fillOverridden();
		// Select Underwriter
		uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
		// Comments for broker
		uwReferralsPageLib.setCommentsForBroker(data.get("uwComments"));
		// Accept quote
		uwReferralsPageLib.clickAcceptButton();

		// Summary Page - Request issue
		SummaryPageLib summaryPageLib = new SummaryPageLib(driver, reporter);
		summaryPageLib.clickRequestIssueButton();
//		for FL state
		uwReferralsPageLib.fillUnderwriterDetailsForFL(data);
		// Verify premium tab is active
		// premiumPageLib.verifyPremiumTabActive();
		locationCoveragePageLib.overrideUwTax(data);
		// Bind policy
		premiumPageLib.clickBindButton();

		// Select document delivery type
		PolicyDeliveryPageLib policyDeliveryPageLib = new PolicyDeliveryPageLib(driver, reporter);
		policyDeliveryPageLib.selectDocumentDeliveryType(data);

		// Click next to go to Billing section
		policyDeliveryPageLib.clickNextButton();

		// Select bill delivery
		BillingPageLib billingPageLib = new BillingPageLib(driver, reporter);
		billingPageLib.selectBillingAddress("SendToBilling");

		// click confirm button
		billingPageLib.clickConfirmBtn();

		// Request Bind
		FinalSummaryPageLib finalSummaryPageLib = new FinalSummaryPageLib(driver, reporter);
		finalSummaryPageLib.clickRequestBind();

		// Verify generated policy number
		PolicyCurrentSummaryPageLib policyCurrentSummaryPageLib = new PolicyCurrentSummaryPageLib(driver, reporter);
		policyCurrentSummaryPageLib.verifyPolicyGenerated();
		PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
		//navigate to endorsement tab
		policyEndorsement.navigateToTransationOrEndorsementTab();
		policyEndorsement.navigateToNBtransaction();
		policyEndorsement.navigateToSubjectivities();

		// override subjectivities
		SubjectivitiesPageLib subjectivitiesPageLib = new SubjectivitiesPageLib(driver, reporter);
		subjectivitiesPageLib.overrideValues();
		// Manually bind the policy
		subjectivitiesPageLib.clickManualBindButton();
		return policyCurrentSummaryPageLib.getPolicyNumber();*/
	}

	public List<HashMap<String, String>> createQuote_PersonalAuto(Hashtable<String, String> data) throws Throwable {
		openApplication("UW");
		initiateQuoteCreation_PA(data);
		// Credit Score
		QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
		if(data.get("Insurance_Score")!=null && data.get("Insurance_Score")!="0"){
			quoteDetailsPageLib.navigateToCustomer();
			new InsuranceScoreManagementLib(driver, reporter).addNewInsuranceScore(data);					
		}

		quoteDetailsPageLib.fillPersonalAutoCoverDetails(data);
		
		CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
		if(!getText(QuoteDetailsPage.headerText,"").contains("operators and vehicles")){		
		commonPageLib.clickNextBtn();}

		OperatorsAndVehiclesPageLib operatorsAndVehiclesPageLib = new OperatorsAndVehiclesPageLib(driver, reporter);
		operatorsAndVehiclesPageLib.selectDriverStatus(data.get("Driver_Status_Name_1"), 1);
		operatorsAndVehiclesPageLib.addAdditionalOperators(data);
		//data = operatorsAndVehiclesPageLib.addVehicles(data);		
		data = operatorsAndVehiclesPageLib.verifyVehiclePresentInOnlineReport(data);
		commonPageLib.clickSaveChangesBtn();
		commonPageLib.clickNextBtn();
		NavigationPageLib navigator = new NavigationPageLib(driver, reporter);
		int noOfOperators = Integer.valueOf(data.get("Drivers_Count"));
		for(int iterator = 1; iterator <= noOfOperators;iterator++){
			navigator.goToOperator(data.get("First_Name_Txt_" + iterator), data.get("Last_Name_Txt_" + iterator));
			new DriverDetailPageLib(driver, reporter).fillDriverDetails(data, iterator);
		}

		int noOfVehicles = Integer.valueOf(data.get("Vehicles_Count"));
		VehicleDetailPageLib vehicleDetailPageLib = new VehicleDetailPageLib(driver, reporter);
		for(int iterator = 1; iterator <= noOfVehicles;iterator++){
		//navigator.goToVehicle(data.get("Auto_Vehicle_Year_No_" + iterator), data.get("Auto_Vehicle_Make_Txt_" + iterator), data.get("Auto_Vehicle_Model_Txt_" + iterator));
		navigator.goToVehicle(data.get("VehicleName_" + iterator));
			vehicleDetailPageLib.fillVehicleDetails(data, iterator);
		}

		if((	!data.get("state").equalsIgnoreCase("PA"))
				&&(!data.get("state").equalsIgnoreCase("NY"))
				&&(!data.get("state").equalsIgnoreCase("TX"))
				&&(!data.get("state").equalsIgnoreCase("FL"))
				&&(!data.get("state").equalsIgnoreCase("AL"))
				&&(!data.get("state").equalsIgnoreCase("CO"))
				&&(!data.get("state").equalsIgnoreCase("CT"))
				&&(!data.get("state").equalsIgnoreCase("IL"))
				&&(!data.get("state").equalsIgnoreCase("SC"))
				&&(!data.get("state").equalsIgnoreCase("NJ"))
				){
			navigator.goToDriverAssignment();
			vehicleDetailPageLib.fillDriverAssignment(noOfOperators, noOfVehicles);
		}

		navigator.goToCoverages();
		new CoveragesPageLib(driver, reporter).fillCoverageDetails(data);

		navigator.goToManuscriptEndorsement();
		//Manuscript endorsement page
		new ManuscriptEndorsementsPageLib(driver, reporter).fillManuscriptEndorsementDetails(data);
		if((!data.get("state").equalsIgnoreCase("PA"))&&(!data.get("state").equalsIgnoreCase("NY"))&&(!data.get("state").equalsIgnoreCase("CA"))
				&&(!data.get("state").equalsIgnoreCase("TX"))&&(!data.get("state").equalsIgnoreCase("FL"))&&(!data.get("state").equalsIgnoreCase("AL"))&&(!data.get("state").equalsIgnoreCase("CO"))&&(!data.get("state").equalsIgnoreCase("CT"))&&(!data.get("state").equalsIgnoreCase("IL"))&&(!data.get("state").equalsIgnoreCase("SC"))&&(!data.get("state").equalsIgnoreCase("NJ"))&&(!data.get("state").equalsIgnoreCase("LA"))){
			navigator.goToAccountSummary();
		}
		navigator.goToMemberInformation();
		new MemberInformationAndPolicyHistoryPageLib(driver, reporter).fillMemberDetails(data);
		//For CT state it will be Forms
		navigator.goToApplicationOrForms(data);
		navigator.goToSubjectivities();
		// Click save button
		commonPageLib.clickSaveChangesBtn();

		// Click rate button
		commonPageLib.clickRateBtn();
		PremiumPageLib premiumPageLib = new PremiumPageLib(driver, reporter);
		TotalPremiumValue = premiumPageLib.getTotalPremiumValue();
		SurplusContributionValue = premiumPageLib.getSurplusContributionValue();
		GrandTotalvalue = premiumPageLib.getGrandTotalvalue();
		List<HashMap<String, String>> tempPolicyDetails = new ArrayList<HashMap<String, String>>();
		/*for(int driverCount = 1;driverCount<=noOfOperators; driverCount++){
			HashMap<String, String> tempDriverMap = new HashMap<String, String>();
			tempDriverMap.put("DriverName", data.get("First_Name_Txt_" + driverCount) + " " + data.get("Last_Name_Txt_" + driverCount));
			tempDriverMap.put("TotalPremiumValue", String.valueOf(TotalPremiumValue));
			tempDriverMap.put("SurplusContributionValue", String.valueOf(SurplusContributionValue));
			tempDriverMap.put("GrandTotalvalue", String.valueOf(GrandTotalvalue));
			tempPolicyDetails.add(tempDriverMap);
		}*/
		for(int vehicleCount = 1;vehicleCount<=noOfVehicles; vehicleCount++){
			HashMap<String, String> tempVehicleMap = new HashMap<String, String>();
			//			String vehicleName = data.get("Auto_Vehicle_Year_No_" + vehicleCount) +" "+ data.get("Auto_Vehicle_Make_Txt_" + vehicleCount) +" "+ data.get("Auto_Vehicle_Model_Txt_" + vehicleCount);
			String vehicleName = data.get("VehicleName_" + vehicleCount);
			tempVehicleMap.put("VehicleName", vehicleName);
			tempVehicleMap.put("LiabilityCSL", premiumPageLib.getPremium(vehicleName, "Liability CSL"));
			tempVehicleMap.put("MedicalPayments", premiumPageLib.getPremium(vehicleName, "Medical Payments"));
			tempVehicleMap.put("UninsuredMotorists(CSL)", premiumPageLib.getPremium(vehicleName, "Uninsured Motorists (CSL)"));
			tempVehicleMap.put("Uninsured Motorists BI (CSL)", premiumPageLib.getPremium(vehicleName, "Uninsured Motorists BI (CSL)"));
			tempVehicleMap.put("Underinsured Motorists BI (CSL)", premiumPageLib.getPremium(vehicleName, "Underinsured Motorists BI (CSL)"));
			tempVehicleMap.put("UninsuredMotorists", premiumPageLib.getPremium(vehicleName, "Uninsured Motorists"));
			tempVehicleMap.put("Comprehensive", premiumPageLib.getPremium(vehicleName, "Comprehensive"));
			tempVehicleMap.put("Collision", premiumPageLib.getPremium(vehicleName, "Collision"));
			tempVehicleMap.put("TowingAndLabor", premiumPageLib.getPremium(vehicleName, "Towing And Labor"));
			tempVehicleMap.put("VehiclePremium", premiumPageLib.getPremium(vehicleName, "Vehicle Premium"));
			tempVehicleMap.put("BodilyInjury", premiumPageLib.getPremium(vehicleName, "Bodily Injury"));
			tempVehicleMap.put("PropertyDamage", premiumPageLib.getPremium(vehicleName, "Property Damage"));
			tempVehicleMap.put("TotalPremiumValue", String.valueOf(TotalPremiumValue));
			tempVehicleMap.put("SurplusContributionValue", String.valueOf(SurplusContributionValue));
			tempVehicleMap.put("GrandTotalvalue", String.valueOf(GrandTotalvalue));
			tempPolicyDetails.add(tempVehicleMap);			
		}

		premiumPageLib.clickPremiumDetailsButton();
		PremiumDetailsLib premiumDetailsLib = new PremiumDetailsLib(driver, reporter);
		com.pure.dragon.watercraft.PremiumDetailsLib premiumDetails = new com.pure.dragon.watercraft.PremiumDetailsLib(driver, reporter);

		for(int vehicleCount = 1;vehicleCount<=noOfVehicles; vehicleCount++){
			int requiredMapLocation = 0;
			//			String vehicleName = data.get("Auto_Vehicle_Year_No_" + vehicleCount) +" "+ data.get("Auto_Vehicle_Make_Txt_" + vehicleCount) +" "+ data.get("Auto_Vehicle_Model_Txt_" + vehicleCount);
			String vehicleName = data.get("Auto_Vehicle_Year_No_" + vehicleCount) +" "+ data.get("Auto_Vehicle_Make_Txt_" + vehicleCount);
			for(int counter = 0; counter<tempPolicyDetails.size();counter++){
				if(tempPolicyDetails.get(counter).get("VehicleName")!= null && tempPolicyDetails.get(counter).get("VehicleName").contains(vehicleName)){
					requiredMapLocation = counter;
					break;
				}
			}
			HashMap<String, String> tempVehicleMap = tempPolicyDetails.get(requiredMapLocation);
			tempVehicleMap.put("BodilyInjuryRate",premiumDetails.getFactor(vehicleName, "Bodily Injury", "Base Rate"));
			tempVehicleMap.put("PropertyDamageRate",premiumDetails.getFactor(vehicleName, "Property Damage", "Base Rate"));
			tempVehicleMap.put("MedicalPaymentsRate",premiumDetails.getFactor(vehicleName, "Medical Payments", "Base Rate"));
			tempVehicleMap.put("UninsuredMotorists(CSL)Rate",premiumDetails.getFactor(vehicleName, "Uninsured Motorists (CSL)", "Base Rate"));
			tempVehicleMap.put("UninsuredMotoristsRate",premiumDetails.getFactor(vehicleName, "Uninsured Motorists", "Base Rate"));
			tempVehicleMap.put("ComprehensiveRate",premiumDetails.getFactor(vehicleName, "Comprehensive", "Base Rate"));
			tempVehicleMap.put("CollisionRate",premiumDetails.getFactor(vehicleName, "Collision", "Base Rate"));
			tempPolicyDetails.set(requiredMapLocation, tempVehicleMap);
		}
		BaseRateValue= premiumDetailsLib.getBaseRateValue();
		premiumDetailsLib.clickReturnToPremiumSummaryPage();

		// Navigate to underwriter referrals section
		UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
		uwReferralsPageLib.clickUnderwriterReferralsTab();
		uwReferralsPageLib.verifyCyberUWRefferal(data);
		uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
		uwReferralsPageLib.fillOverridden();
		// Select Underwriter
		if("PROD".equalsIgnoreCase(region)){
			uwReferralsPageLib.selectUnderwriter(data.get("uwName_prod"));
		}else{
			uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
		}
		// Comments for broker
		uwReferralsPageLib.setCommentsForBroker(data.get("uwComments"));
		// Accept quote
		uwReferralsPageLib.clickAcceptButton();

		// Summary Page - Request issue
		SummaryPageLib summaryPageLib = new SummaryPageLib(driver, reporter);
		summaryPageLib.clickRequestIssueButton();
		// quote proposal
		premiumPageLib.clickQuoteProposalButton();
		// Bind policy		
		premiumPageLib.clickBindButton();

		// Select document delivery type
		PolicyDeliveryPageLib policyDeliveryPageLib = new PolicyDeliveryPageLib(driver, reporter);
		policyDeliveryPageLib.selectDocumentDeliveryType(data);

		// Click next to go to Billing section
		policyDeliveryPageLib.clickNextButton();

		// Select bill delivery
		BillingPageLib billingPageLib = new BillingPageLib(driver, reporter);
		billingPageLib.selectBillingAddress("SendToBilling");

		// click confirm button
		billingPageLib.clickConfirmBtn();

		// Request Bind
		FinalSummaryPageLib finalSummaryPageLib = new FinalSummaryPageLib(driver, reporter);
		finalSummaryPageLib.clickRequestBind();

		// Verify generated policy number
		PolicyCurrentSummaryPageLib policyCurrentSummaryPageLib = new PolicyCurrentSummaryPageLib(driver, reporter);
		policyCurrentSummaryPageLib.verifyPolicyGenerated();
		PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
		//navigate to endorsement tab
		policyEndorsement.navigateToTransationOrEndorsementTab();
		policyEndorsement.navigateToNBtransaction();
		policyEndorsement.navigateToSubjectivities();

		// override subjectivities
		SubjectivitiesPageLib subjectivitiesPageLib = new SubjectivitiesPageLib(driver, reporter);
		subjectivitiesPageLib.overrideValues();
		// Manually bind the policy
		subjectivitiesPageLib.clickManualBindButton();
		//		return policyCurrentSummaryPageLib.getPolicyNumber();
		for(int i = 0; i< tempPolicyDetails.size(); i++){
			HashMap<String, String> temp = tempPolicyDetails.get(i);
			temp.put("OriginalPolicyNumber", data.get("Policy_Number_Txt"));
			temp.put("PolicyNumber", policyCurrentSummaryPageLib.getPolicyNumber());
			temp.put("RowNo", data.get("Row"));
			tempPolicyDetails.set(i, temp);
		}
		return tempPolicyDetails;
	}

	public String createQuote_ExcessLiability(Hashtable<String, String> data) throws Throwable {
		openApplication("UW");
		initiateQuoteCreation(data);
		new QuoteDetailsPageLib(driver, reporter).fillQuoteDetailsOfExcessLiability(data);	
		CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
		commonPageLib.clickNextBtn();
		//operator Information
		new OperatorsAndVehiclesPageLib(driver, reporter).fillOperatorInformationDetails(data);
		commonPageLib.clickNextBtn();
		//additional Insured
		commonPageLib.clickAddBtn();
		new AdditionalInsuredPageLib(driver, reporter).fillAdditionalInsuredPageDetails(data);
		commonPageLib.clickNextBtn();
		//property information
		new PropertyInformationPageLib(driver, reporter).fillPropertyInformation(data);
		commonPageLib.clickNextBtn();
		//watercraft
		commonPageLib.clickAddBtn();
		new WaterCraftInformationPageLib(driver, reporter).fillWaterCraftInformationDetails(data);
		commonPageLib.clickNextBtn();
		//select coverages
		new SelectCoveragesPageLib(driver, reporter).fillSelectCoverages(data);
		commonPageLib.clickNextBtn();
		//optional coverages
		new OptionalCoveragesPageLib(driver, reporter).fillOptionalCoverageDetailsOfExcessLiability(data);
		commonPageLib.clickNextBtn();
		//manuscript Endorsements
		new ManuscriptEndorsementsPageLib(driver, reporter).fillManuscriptEndorsementDetails(data);;
		commonPageLib.clickNextBtn();
		//subjectives
		commonPageLib.clickNextBtn();
		commonPageLib.clickRateBtn();
		UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
		uwReferralsPageLib.clickUnderwriterReferralsTab();
		/*uwReferralsPageLib.verifyCyberUWRefferal(data);*/
		uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
		uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
		uwReferralsPageLib.fillOverridden();
		uwReferralsPageLib.clickAcceptButton();
		new SummaryPageLib(driver, reporter).clickRequestIssueButton();
		// Bind policy
		new PremiumPageLib(driver, reporter).clickBindButton();
		// Select document delivery type
		PolicyDeliveryPageLib policyDeliveryPageLib = new PolicyDeliveryPageLib(driver, reporter);
		policyDeliveryPageLib.selectDocumentDeliveryType(data);
		policyDeliveryPageLib.clickNextButton();
		// Select bill delivery
		BillingPageLib billingPageLib = new BillingPageLib(driver, reporter);
		billingPageLib.selectBillingAddress("SendToBilling");
		// click confirm button
		billingPageLib.clickConfirmBtn();
		// Request Bind
		new FinalSummaryPageLib(driver, reporter).clickRequestBind();
		// Verify generated policy number
		PolicyCurrentSummaryPageLib policyCurrentSummaryPageLib = new PolicyCurrentSummaryPageLib(driver, reporter);
		policyCurrentSummaryPageLib.verifyPolicyGenerated();
		PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
		//navigate to endorsement tab
		policyEndorsement.navigateToTransationOrEndorsementTab();
		policyEndorsement.navigateToNBtransaction();
		policyEndorsement.navigateToSubjectivities();
		// override subjectivities
		SubjectivitiesPageLib subjectivitiesPageLib = new SubjectivitiesPageLib(driver, reporter);
		subjectivitiesPageLib.overrideValues();
		// Manually bind the policy
		subjectivitiesPageLib.clickManualBindButton();
		/*summaryPageLib.clickFormCode();*/
		return policyCurrentSummaryPageLib.getPolicyNumber();
	}
	public void initiateQuoteCreation(Hashtable<String, String> data) throws Throwable{
		HomeLib homeLib = new HomeLib(driver, reporter);		
		if(("PROD").equalsIgnoreCase(region)){
			//homeLib.loginOKTA("Pureonlineautomation1@mailinator.com", "Pr0dt3sting");
			homeLib.loginOKTA(data.get("username_prod"), data.get("password_prod"));
		}else{
			homeLib.login(data.get("username"), data.get("password"));
		}
		//TestResult.sessionDetails.put(data.get("SessionID"), getAttributeByValue(HomePage.dragonSessionID, "Dragon_Session_ID"));
		//openApplicationViaTiles();
		homeLib.navigateToQuotes();
		QuotesLib quotesLib = new QuotesLib(driver, reporter);
		quotesLib.clickNewQuoteButton();
		AgencyListLib agencyListLib = new AgencyListLib(driver, reporter);
		if("prod".equalsIgnoreCase(region)){
			agencyListLib.searchAgency(enumSearchBy.BrokerName,data.get("brokerName_prod"));
			agencyListLib.SelectAgency(data.get("brokerName_prod"));
		}else{
			agencyListLib.searchAgency(enumSearchBy.BrokerName,data.get("brokerName"));
			agencyListLib.SelectAgency(data.get("brokerName"));
		}
		
		NewQuoteBasicInformationLib newQuote_BasicInformationLib = new NewQuoteBasicInformationLib(driver, reporter);
		newQuote_BasicInformationLib.fillDetails(data);
		System.out.println(data.get("state")+":"+data.get("lobType")+":"+"---waiting for 5 secs");
		newQuote_BasicInformationLib.clickNxtBtn();
	}
	public void initiateQuoteCreation_PA(Hashtable<String, String> data) throws Throwable{
		HomeLib homeLib = new HomeLib(driver, reporter);		
		if(("PROD").equalsIgnoreCase(region)){
			//homeLib.loginOKTA("Pureonlineautomation1@mailinator.com", "Pr0dt3sting");
			homeLib.loginOKTA(data.get("username_prod"), data.get("password_prod"));
		}else{
			homeLib.login(data.get("username"), data.get("password"));
		}
			TestResult.sessionDetails.put(data.get("SessionID"), getAttributeByValue(HomePage.dragonSessionID, "Dragon_Session_ID"));
		// openApplicationViaTiles();
		homeLib.navigateToQuotes();
		QuotesLib quotesLib = new QuotesLib(driver, reporter);
		quotesLib.clickNewQuoteButton();
		AgencyListLib agencyListLib = new AgencyListLib(driver, reporter);
		if("prod".equalsIgnoreCase(region)){
			agencyListLib.searchAgency(enumSearchBy.BrokerName,data.get("brokerName_PROD"));
			agencyListLib.SelectAgency(data.get("brokerName_PROD"));
		}else{
			agencyListLib.searchAgency(enumSearchBy.BrokerName,data.get("brokerName"));
			agencyListLib.SelectAgency(data.get("brokerName"));
		}
		NewQuoteBasicInformationLib newQuote_BasicInformationLib = new NewQuoteBasicInformationLib(driver, reporter);
			newQuote_BasicInformationLib.fillDetails_PA(data);
		newQuote_BasicInformationLib.clickNxtBtn();
	}

	public void createQuote_HOPrefill(Hashtable<String, String> data) throws Throwable {
		openApplication("UW");
		initiateQuoteCreation(data);
		QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter); 
		int counter=0;
		while(!isVisibleOnly(QuoteDetailsPage.insuranceScore,"insuranceScore") && counter<4){
			new CommonPageLib(driver, reporter).clickSaveChangesBtn();
			counter++;			
		}
		//Credit score prefill validation
		assertText(QuoteDetailsPage.insuranceScore,data.get("insuranceScore"));		
	}
	public /*List<HashMap<String, String>>*/ String createQuote_PersonalAutoPrefill(Hashtable<String, String> data) throws Throwable {
		openApplication("UW");
		initiateQuoteCreation_PA(data);
		// Credit Score
		QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
		int counter=0;
		/***************
		while(!isVisibleOnly(QuoteDetailsPage.insuranceScore,"insuranceScore") && counter<4){
			new CommonPageLib(driver, reporter).clickSaveChangesBtn();
			counter++;			
		}
		//Credit score prefill validation
		assertText(QuoteDetailsPage.insuranceScore,data.get("insuranceScore"));
		**************/
		quoteDetailsPageLib.fillPersonalAutoCoverDetails(data);

		CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
		commonPageLib.clickNextBtn();

		OperatorsAndVehiclesPageLib operatorsAndVehiclesPageLib = new OperatorsAndVehiclesPageLib(driver, reporter);
		counter=0;
		while(operatorsAndVehiclesPageLib.errorMessage() &&(counter++)<4 ){
			commonPageLib.clickSaveChangesBtn();
		}
		operatorsAndVehiclesPageLib.selectDriverStatus(data.get("Driver_Status_Name_1"), 1);
		operatorsAndVehiclesPageLib.verifyVehiclesAdded(data);
		/*operatorsAndVehiclesPageLib.addAdditionalOperators(data);
		data = operatorsAndVehiclesPageLib.addVehicles(data);

		commonPageLib.clickSaveChangesBtn();
		commonPageLib.clickNextBtn();
		NavigationPageLib navigator = new NavigationPageLib(driver, reporter);
		int noOfOperators = Integer.valueOf(data.get("Drivers_Count"));
		for(int iterator = 1; iterator <= noOfOperators;iterator++){
			navigator.goToOperator(data.get("First_Name_Txt_" + iterator), data.get("Last_Name_Txt_" + iterator));
			new DriverDetailPageLib(driver, reporter).fillDriverDetails(data, iterator);
		}

		int noOfVehicles = Integer.valueOf(data.get("Vehicles_Count"));
		VehicleDetailPageLib vehicleDetailPageLib = new VehicleDetailPageLib(driver, reporter);
		for(int iterator = 1; iterator <= noOfVehicles;iterator++){
			//			navigator.goToVehicle(data.get("Auto_Vehicle_Year_No_" + iterator), data.get("Auto_Vehicle_Make_Txt_" + iterator), "");
			navigator.goToVehicle(data.get("VehicleName_" + iterator));
			vehicleDetailPageLib.fillVehicleDetails(data, iterator);
		}

		if((	!data.get("state").equalsIgnoreCase("PA"))
				&&(!data.get("state").equalsIgnoreCase("NY"))
				&&(!data.get("state").equalsIgnoreCase("TX"))
				&&(!data.get("state").equalsIgnoreCase("FL"))){
			navigator.goToDriverAssignment();
			vehicleDetailPageLib.fillDriverAssignment(noOfOperators, noOfVehicles);
		}

		navigator.goToCoverages();
		new CoveragesPageLib(driver, reporter).fillCoverageDetails(data);

		navigator.goToManuscriptEndorsement();
		//Manuscript endorsement page
		new ManuscriptEndorsementsPageLib(driver, reporter).fillManuscriptEndorsementDetails(data);
		if((!data.get("state").equalsIgnoreCase("PA"))&&(!data.get("state").equalsIgnoreCase("NY"))&&(!data.get("state").equalsIgnoreCase("CA"))
				&&(!data.get("state").equalsIgnoreCase("TX"))&&(!data.get("state").equalsIgnoreCase("FL"))){
			navigator.goToAccountSummary();
		}
		navigator.goToMemberInformation();
		new MemberInformationAndPolicyHistoryPageLib(driver, reporter).fillMemberDetails(data);
		navigator.goToApplication(data);
		navigator.goToSubjectivities();
		// Click save button
		commonPageLib.clickSaveChangesBtn();

		// Click rate button
		commonPageLib.clickRateBtn();
		PremiumPageLib premiumPageLib = new PremiumPageLib(driver, reporter);
		TotalPremiumValue = premiumPageLib.getTotalPremiumValue();
		SurplusContributionValue = premiumPageLib.getSurplusContributionValue();
		GrandTotalvalue = premiumPageLib.getGrandTotalvalue();
		List<HashMap<String, String>> tempPolicyDetails = new ArrayList<HashMap<String, String>>();
		for(int driverCount = 1;driverCount<=noOfOperators; driverCount++){
			HashMap<String, String> tempDriverMap = new HashMap<String, String>();
			tempDriverMap.put("DriverName", data.get("First_Name_Txt_" + driverCount) + " " + data.get("Last_Name_Txt_" + driverCount));
			tempDriverMap.put("TotalPremiumValue", String.valueOf(TotalPremiumValue));
			tempDriverMap.put("SurplusContributionValue", String.valueOf(SurplusContributionValue));
			tempDriverMap.put("GrandTotalvalue", String.valueOf(GrandTotalvalue));
			tempPolicyDetails.add(tempDriverMap);
		}
		for(int vehicleCount = 1;vehicleCount<=noOfVehicles; vehicleCount++){
			HashMap<String, String> tempVehicleMap = new HashMap<String, String>();
			//			String vehicleName = data.get("Auto_Vehicle_Year_No_" + vehicleCount) +" "+ data.get("Auto_Vehicle_Make_Txt_" + vehicleCount) +" "+ data.get("Auto_Vehicle_Model_Txt_" + vehicleCount);
			String vehicleName = data.get("VehicleName_" + vehicleCount);
			tempVehicleMap.put("VehicleName", vehicleName);
			tempVehicleMap.put("LiabilityCSL", premiumPageLib.getPremium(vehicleName, "Liability CSL"));
			tempVehicleMap.put("MedicalPayments", premiumPageLib.getPremium(vehicleName, "Medical Payments"));
			tempVehicleMap.put("UninsuredMotorists(CSL)", premiumPageLib.getPremium(vehicleName, "Uninsured Motorists (CSL)"));
			tempVehicleMap.put("Uninsured Motorists BI (CSL)", premiumPageLib.getPremium(vehicleName, "Uninsured Motorists BI (CSL)"));
			tempVehicleMap.put("Underinsured Motorists BI (CSL)", premiumPageLib.getPremium(vehicleName, "Underinsured Motorists BI (CSL)"));
			tempVehicleMap.put("UninsuredMotorists", premiumPageLib.getPremium(vehicleName, "Uninsured Motorists"));
			tempVehicleMap.put("Comprehensive", premiumPageLib.getPremium(vehicleName, "Comprehensive"));
			tempVehicleMap.put("Collision", premiumPageLib.getPremium(vehicleName, "Collision"));
			tempVehicleMap.put("TowingAndLabor", premiumPageLib.getPremium(vehicleName, "Towing And Labor"));
			tempVehicleMap.put("VehiclePremium", premiumPageLib.getPremium(vehicleName, "Vehicle Premium"));
			tempVehicleMap.put("BodilyInjury", premiumPageLib.getPremium(vehicleName, "Bodily Injury"));
			tempVehicleMap.put("PropertyDamage", premiumPageLib.getPremium(vehicleName, "Property Damage"));
			tempVehicleMap.put("TotalPremiumValue", String.valueOf(TotalPremiumValue));
			tempVehicleMap.put("SurplusContributionValue", String.valueOf(SurplusContributionValue));
			tempVehicleMap.put("GrandTotalvalue", String.valueOf(GrandTotalvalue));
			tempPolicyDetails.add(tempVehicleMap);			
		}

		premiumPageLib.clickPremiumDetailsButton();
		PremiumDetailsLib premiumDetailsLib = new PremiumDetailsLib(driver, reporter);
		com.pure.dragon.watercraft.PremiumDetailsLib premiumDetails = new com.pure.dragon.watercraft.PremiumDetailsLib(driver, reporter);

		for(int vehicleCount = 1;vehicleCount<=noOfVehicles; vehicleCount++){
			int requiredMapLocation = 0;
			//			String vehicleName = data.get("Auto_Vehicle_Year_No_" + vehicleCount) +" "+ data.get("Auto_Vehicle_Make_Txt_" + vehicleCount) +" "+ data.get("Auto_Vehicle_Model_Txt_" + vehicleCount);
			String vehicleName = data.get("Auto_Vehicle_Year_No_" + vehicleCount) +" "+ data.get("Auto_Vehicle_Make_Txt_" + vehicleCount);
			for(int counter = 0; counter<tempPolicyDetails.size();counter++){
				if(tempPolicyDetails.get(counter).get("VehicleName")!= null && tempPolicyDetails.get(counter).get("VehicleName").contains(vehicleName)){
					requiredMapLocation = counter;
					break;
				}
			}
			HashMap<String, String> tempVehicleMap = tempPolicyDetails.get(requiredMapLocation);
			tempVehicleMap.put("BodilyInjuryRate",premiumDetails.getFactor(vehicleName, "Bodily Injury", "Base Rate"));
			tempVehicleMap.put("PropertyDamageRate",premiumDetails.getFactor(vehicleName, "Property Damage", "Base Rate"));
			tempVehicleMap.put("MedicalPaymentsRate",premiumDetails.getFactor(vehicleName, "Medical Payments", "Base Rate"));
			tempVehicleMap.put("UninsuredMotorists(CSL)Rate",premiumDetails.getFactor(vehicleName, "Uninsured Motorists (CSL)", "Base Rate"));
			tempVehicleMap.put("UninsuredMotoristsRate",premiumDetails.getFactor(vehicleName, "Uninsured Motorists", "Base Rate"));
			tempVehicleMap.put("ComprehensiveRate",premiumDetails.getFactor(vehicleName, "Comprehensive", "Base Rate"));
			tempVehicleMap.put("CollisionRate",premiumDetails.getFactor(vehicleName, "Collision", "Base Rate"));
			tempPolicyDetails.set(requiredMapLocation, tempVehicleMap);
		}
		BaseRateValue= premiumDetailsLib.getBaseRateValue();
		premiumDetailsLib.clickReturnToPremiumSummaryPage();

		// Navigate to underwriter referrals section
		UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
		uwReferralsPageLib.clickUnderwriterReferralsTab();
		uwReferralsPageLib.verifyCyberUWRefferal(data);
		uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
		uwReferralsPageLib.fillOverridden();
		// Select Underwriter
		if("PROD".equalsIgnoreCase(region)){
			uwReferralsPageLib.selectUnderwriter(data.get("uwName_prod"));
		}else{
			uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
		}
		// Comments for broker
		uwReferralsPageLib.setCommentsForBroker(data.get("uwComments"));
		// Accept quote
		uwReferralsPageLib.clickAcceptButton();

		// Summary Page - Request issue
		SummaryPageLib summaryPageLib = new SummaryPageLib(driver, reporter);
		summaryPageLib.clickRequestIssueButton();
		// quote proposal
		premiumPageLib.clickQuoteProposalButton();
		// Bind policy		
		premiumPageLib.clickBindButton();

		// Select document delivery type
		PolicyDeliveryPageLib policyDeliveryPageLib = new PolicyDeliveryPageLib(driver, reporter);
		policyDeliveryPageLib.selectDocumentDeliveryType(data);

		// Click next to go to Billing section
		policyDeliveryPageLib.clickNextButton();

		// Select bill delivery
		BillingPageLib billingPageLib = new BillingPageLib(driver, reporter);
		billingPageLib.selectBillingAddress("SendToBilling");

		// click confirm button
		billingPageLib.clickConfirmBtn();

		// Request Bind
		FinalSummaryPageLib finalSummaryPageLib = new FinalSummaryPageLib(driver, reporter);
		finalSummaryPageLib.clickRequestBind();

		// Verify generated policy number
		PolicyCurrentSummaryPageLib policyCurrentSummaryPageLib = new PolicyCurrentSummaryPageLib(driver, reporter);
		policyCurrentSummaryPageLib.verifyPolicyGenerated();
		PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
		//navigate to endorsement tab
		policyEndorsement.navigateToTransationOrEndorsementTab();
		policyEndorsement.navigateToNBtransaction();
		policyEndorsement.navigateToSubjectivities();

		// override subjectivities
		SubjectivitiesPageLib subjectivitiesPageLib = new SubjectivitiesPageLib(driver, reporter);
		subjectivitiesPageLib.overrideValues();
		// Manually bind the policy
		subjectivitiesPageLib.clickManualBindButton();
		//		return policyCurrentSummaryPageLib.getPolicyNumber();
		for(int i = 0; i< tempPolicyDetails.size(); i++){
			HashMap<String, String> temp = tempPolicyDetails.get(i);
			temp.put("OriginalPolicyNumber", data.get("Policy_Number_Txt"));
			temp.put("PolicyNumber", policyCurrentSummaryPageLib.getPolicyNumber());
			temp.put("RowNo", data.get("Row"));
			tempPolicyDetails.set(i, temp);
		}
		return tempPolicyDetails;*/
		return "";
	}

}
