package com.pure.test.cyber;

import java.util.Date;
import java.util.Hashtable;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.HomeLib;
import com.pure.dragon.PolicyRenewalLib;
import com.pure.dragon.PolicyTransactionDocumentListPageLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.homeOwners.QuoteDetailsPageLib;
import com.pure.report.ConfigFileReadWrite;
import com.pure.test.cyber.commons.QuoteCreation;
import com.pure.utilities.TestUtil;

public class TC004_Cyber_RenewalPolicy extends ActionEngine {
	boolean status = false;
	Date startTime, endTime;
	int counter = 0;
	boolean executeMode = false;
	boolean testCyber = Boolean.parseBoolean(
			ConfigFileReadWrite.read("resources/framework.properties", "TEST_CYBER"));

	@BeforeMethod
	public void beforeTest() {
		startTime = new Date();
		reporter.setDetailedReportHeader(getTestDataFor_BasicInfo(), counter);
	}

	private Object[][] getTestDataFor_Login() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon,
				"LoginData");
	}

	private Object[][] getTestDataFor_AgencyDetails() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon,
				"AgencyDetails");
	}

	private Object[][] getTestDataFor_BasicInfo() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon,
				"BasicInfo");
	}

	private Object[][] getTestDataFor_QuoteDetails() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon,
				"QuoteDetails");
	}

	private Object[][] getTestDataFor_LocationDetails() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon,
				"LocationDetails");
	}

	private Object[][] getTestDataFor_PreviousClaims() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon,
				"PreviousClaims");
	}

	private Object[][] getTestDataFor_MemberInfo() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon,
				"MemberInfo");
	}

	private Object[][] getTestDataFor_UnderwriterRefferal() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon,
				"UnderwriterRefferal");
	}

	private Object[][] getTestDataFor_DocumentDelivery() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon,
				"DocumentDelivery");
	}
	private Object[][] getTestDataFor_Renewal() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon, "Renewal");
	}
	private Object[][] getTestDataFor_TestDetails() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon,
				"TestDetails");
	}
	private Object[][] getTestDataFor_ManuScriptEndorsements() {
		return TestUtil.getData("TC004_Cyber_RenewalPolicy", TestDataDragon,
				"ManuScriptEndorsement");
	}

	@DataProvider
	private Object[][] getTestData() throws ParseException {
		return TestUtil.getAllData(getTestDataFor_Login(),
				getTestDataFor_AgencyDetails(), getTestDataFor_BasicInfo(),
				getTestDataFor_QuoteDetails(),
				getTestDataFor_LocationDetails(),
				getTestDataFor_PreviousClaims(), getTestDataFor_MemberInfo(),
				getTestDataFor_UnderwriterRefferal(),
				getTestDataFor_DocumentDelivery(),getTestDataFor_TestDetails(),
				getTestDataFor_Renewal(),getTestDataFor_ManuScriptEndorsements());
	}

	@Test(dataProvider = "getTestData")
	public void TC004_Cyber_RenewalPolicy_Script(Hashtable<String, String> data)
			throws Throwable {
		if((data.get("execute")!=null) && (data.get("execute").equalsIgnoreCase("Y"))){
			executeMode = true;
			String newPolicyNumber = new QuoteCreation(driver, reporter,testCyber).createQuote(data);
			//String newPolicyNumber = "HO231456900";//HO231400900
			System.out.println("Policy Number::::::::::::" + newPolicyNumber);

			// policy Renewal process started
			// navigate to endorsement tab
			PolicyRenewalLib renewal = new PolicyRenewalLib(driver, reporter);
			renewal.navigateToTransationOrEndorsementTab();
			// click on create renewal button
			renewal.clickCreateRenewalBtn();
			// click on renewal entry
			renewal.clickRenewalEntry();
			// Process the renewal
			renewal.processRenewal(data, testCyber);
			//Exit transaction
			new PolicyTransactionDocumentListPageLib(driver, reporter).clickExitTransactionBtn();
			status = true;
		}
	}

	@AfterMethod
	public void afterTest() throws Throwable {
		System.out.println("Inside AfterMethod------------------------------------------\n");
		counter++;
		//Logout of application
		if(executeMode){new HomeLib(driver, reporter).logout();executeMode =false;}
		endTime = new Date();
		//		Qtest.UpdateQTestResults(status, "US4441_Cancel and Reinstate", startTime, endTime);
	}
}
