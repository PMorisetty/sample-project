package com.pure.dragon;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pure.report.CReporter;


public class AgencyListLib extends SearchFromList{
	public AgencyListLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void searchAgency(enumSearchBy enumSearchBy, String containsText) throws Throwable{
		search(enumSearchBy, containsText);		
	}
	//select nth item listed
	public void selectNthAgency(int n) throws Throwable{
		String nthItemXPATH = "(//table//input[@type='radio'])[" + n + "]";
		//waitForVisibilityOfElement(By.xpath(nthItemXPATH), "nth element");
		WebElement nthItem = driver.findElement(By.xpath(nthItemXPATH));
		if(nthItem.getAttribute("checked")==null){
			click(By.xpath(nthItemXPATH),"Select Agency radio");
		}
	}
	//select agency listed
	public void SelectAgency(String advisor) throws Throwable{
		Thread.sleep(3000);
		By itemXpath = By.xpath("//div[text()='"+advisor+"']/../preceding-sibling::td//input[@type='radio']");
		WebDriverWait wait = new WebDriverWait(driver, 180);
		wait.until(ExpectedConditions.numberOfElementsToBe(itemXpath, 1));
		click(itemXpath,"Select Agency radio");
		click(AgencyListPage.btnSelectAgency,"Select Agency button");
	}
	//select nth agency listed
	public void SelectAgency(int nthAgencyListed) throws Throwable{
		//List<WebElement> allListedAgencies = driver.findElements(AgencyListPage.btnRadioOfItemListed);
		selectNthAgency(nthAgencyListed);
		click(AgencyListPage.btnSelectAgency,"Select Agency button");
	}

}
