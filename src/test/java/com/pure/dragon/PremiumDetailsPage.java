package com.pure.dragon;

import org.openqa.selenium.By;

public class PremiumDetailsPage {
	
	static By
	baseRateValue,baseRateNonHurricaneValue, baseRateHurricaneValue,
	creditScoreFactor,
	ageOfHomeFactor, aopDeductibleFactor;
	public static By returnToSummaryPremiumPage;
	
	
	static{
		baseRateValue = By.xpath("//div[text()='Base Rate']/..//following-sibling::td[1]/div");
		baseRateNonHurricaneValue = By.xpath("//div[text()='Base Rate Non-Hurricane']/..//following-sibling::td[1]/div");
		baseRateHurricaneValue = By.xpath("//div[text()='Base Rate Hurricane']/..//following-sibling::td[1]/div");
		creditScoreFactor = By.xpath("//div[contains(text(),'Credit Score Factor')]/..//following-sibling::td[1]/div");
		ageOfHomeFactor = By.xpath("//div[text()='Age of Home Factor']/..//following-sibling::td[1]/div");
		aopDeductibleFactor = By.xpath("//div[text()='Deductible Factor']/..//following-sibling::td[1]/div");
		returnToSummaryPremiumPage= By.xpath("//a[text()='return to premium summary page']");
		
		
		//div[contains(text(), 'Hull Base Premium')]/../..//div[contains(text(), 'Base Rate')]/../../td[7]/div
	}

}
