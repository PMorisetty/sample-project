package com.pure.dragon;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class EndorsementModifiedPremiumPageLib extends ActionEngine{
	public EndorsementModifiedPremiumPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/*Function to click on Review Referrals button*/
	public void clickReviewReferralsButton() throws Throwable{
		click(EndorsementModifiedPremiumPage.reviewReferralsButton, "Review Referrals button");
	}
	/*Function to click on Premium Details button*/
	public void clickPremiumDetailsButton() throws Throwable{
		click(EndorsementModifiedPremiumPage.premiumDetailsButton, "Premium Details button");
	}
	/*Function to click on More Changes button*/
	public void clickMoreChangesButton() throws Throwable{
		click(EndorsementModifiedPremiumPage.moreChangesButton, "More Changes button");
	}
	/*Function to click on Discard Endorsement button*/
	public void clickDiscardEndorsementButton() throws Throwable{
		click(EndorsementModifiedPremiumPage.discardEndorsementButton, "Discard Endorsement button");
	}
	/*Function to click on Save and Exit button*/
	public void clickSaveAndExitButton() throws Throwable{
		click(EndorsementModifiedPremiumPage.saveAndExitButton, "Save and Exit button");
	}
	
	/*Function to click on Issue button*/
	public void clickIssueButton() throws Throwable{
		click(EndorsementModifiedPremiumPage.issueButton, "Issue button");
		waitForBodyTag();
	}
	public void clickCompleteRewriteTransaction() throws Throwable {
		click(EndorsementModifiedPremiumPage.completeRewriteTransactionBtn, "Complete Rewrite Transaction Button");
		waitForBodyTag();
	}
	
	
	/*Function to verify coverage exists in Premium details*/
	public void verifyCoverageExists(String coverage) throws Throwable{
		String tempXpath = xpathUpdator(EndorsementModifiedPremiumPage.coverageLabel, coverage);
		assertElementPresent(By.xpath(tempXpath), coverage + " label");
	}
	/*Function to verify coverage limits*/
	public void verifyCoverageLimits(String coverage, String limit) throws Throwable{
		String tempXpath = xpathUpdator(EndorsementModifiedPremiumPage.coverageLimitLabel, coverage);
		assertTrue(limit.equalsIgnoreCase(getText(By.xpath(tempXpath), coverage + " limit")),
				"Verifying limits '" + limit +"' for coverage '" +coverage +"'." );
	}
	/*Function to verify coverage deductible*/
	public void verifyCoverageDeductible(String coverage, String deductible) throws Throwable{
		String tempXpath = xpathUpdator(EndorsementModifiedPremiumPage.coverageDeductibleLabel, coverage);
		assertTrue(deductible.equalsIgnoreCase(getText(By.xpath(tempXpath), coverage + " Deductible")),
				"Verifying deductible '" + deductible +"' for coverage '" +coverage +"'." );
	}
	/*Function to verify coverage premium*/
	public void verifyCoveragePremium(String coverage, String premium) throws Throwable{
		String tempXpath = xpathUpdator(EndorsementModifiedPremiumPage.coveragePremiumLabel, coverage);
		assertTrue(premium.equalsIgnoreCase(getAttributeValue(By.xpath(tempXpath), "innerText")),
				"Verifying premium '" + premium +"' for coverage '" +coverage +"'." );
	}
	public void clickCompleteReviewTransactionBtn() throws Throwable {
		click(EndorsementModifiedPremiumPage.completeRewriteTransactionBtn, "Complete Rewrite Transaction Button");
	}
}
