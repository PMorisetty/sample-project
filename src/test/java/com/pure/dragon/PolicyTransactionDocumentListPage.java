package com.pure.dragon;

import org.openqa.selenium.By;

public class PolicyTransactionDocumentListPage {
	
	static By exitTransactionButton, clickPolicyLink, clickDocLink;

	static {
		exitTransactionButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.129205')]");
		clickPolicyLink = By.xpath("//div[@id='headerBar']//a[contains(text(),'Policy:')]");
		clickDocLink = By.cssSelector("a[title='Document Link']");
	}
}
