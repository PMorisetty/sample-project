package com.pure.dragon;

import org.openqa.selenium.By;

public class CustomerListPage {
	static By btnRadioOfItemListed, customerLink;
	static{
		btnRadioOfItemListed = By.xpath("//input[@type='radio']");
		customerLink = By.xpath("//input[@type='radio']/ancestor::tr[1]/td[2]//a");
	}
}
