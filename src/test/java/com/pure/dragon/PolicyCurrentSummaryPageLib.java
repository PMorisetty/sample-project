package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PolicyCurrentSummaryPageLib extends ActionEngine{
	public PolicyCurrentSummaryPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/*Function to get policy number from label*/
	public String getPolicyNumber() throws Throwable{
		return getAttributeValue(PolicyCurrentSummaryPage.policyNumberLabel, "innerText");
	}
	/*Function to click on Document link*/
	public void clickDocLink() throws Throwable{
		click(PolicyCurrentSummaryPage.docLink, "Document Link");
	}
	/*Function to verify policy is generated*/
	public void verifyPolicyGenerated() throws Throwable{
		waitForBodyTag();
		assertTrue(!getPolicyNumber().isEmpty(),"Policy generated");
	}
	/*Function to verify Cyber form is generated in NB*/
	public void downloadVerifyNBPolicy() throws Throwable{
		deleteDownloadedPdfs();
		click(PolicyCurrentSummaryPage.pdfDocLink, "Document download Link");
		verifyForm("PHVH-ENDGEN-029", "NB");
	}
	/*Function to verify Cyber form is generated in Endorsement*/
	public void downloadVerifyEndorsementPolicy() throws Throwable{
		deleteDownloadedPdfs();
		click(PolicyCurrentSummaryPage.endorsementPdfDocLink, "Document download Link");
		verifyForm("PHVH-ENDGEN-029", "Endorsement");
	}
	/*Function to verify Cyber form is generated in Renewal*/
	public void downloadVerifyRenewalPolicy() throws Throwable{
		deleteDownloadedPdfs();
		click(PolicyCurrentSummaryPage.endorsementPdfDocLink, "Document download Link");
		verifyForm("PHVH-ENDGEN-029", "Renewal");
	}
}
