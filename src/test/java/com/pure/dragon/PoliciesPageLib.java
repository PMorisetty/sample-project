package com.pure.dragon;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pure.report.CReporter;

public class PoliciesPageLib extends SearchFromList{
	public PoliciesPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void searchPolicy(enumSearchBy enumSearchBy, String containsText) throws Throwable{
		search(enumSearchBy, containsText);
		//Thread.sleep(1000);
	}
	public void clickSearchedPolicy(String containsText) throws Throwable{
		By itemXpath = By.cssSelector("input[type='radio']");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.numberOfElementsToBe(itemXpath, 1));
		By selectedPolicy = By.xpath("//a[.='"+ containsText +"']");
		waitForVisibilityOfElement(selectedPolicy, "Searched Policy");
		click(selectedPolicy, "selectedPolicy");
	}
}
