package com.pure.dragon;

import org.openqa.selenium.By;

public class PolicyRenewalPage {
	static By createRenewalBtn;
	static By renewalEntry;
	static By policyImageTab;
	static By summaryTab;
	static By premiumSummaryTab;
	static By underwritingAlertsTab;
	static By documentsTab;
	static By memberFlagTab;
	static By reviewChangesBtn;
	static By renewedPremiumBtn;
	static By exitBtn;
	public static By reviewReferals;
	static By subjectivitiesTab;
	static By diaryTab;
	static By acceptBtn;
	static By processRenewal;
	static By processCompleted;
	static By exitTransactionBtn;
	public static By transactionOrEndorsementTab;
	static {
		createRenewalBtn = By.xpath("//a[contains(text(),'create renewal')]");
		renewalEntry = By.xpath("//a[contains(text(),'Renewal')]");
		policyImageTab = By.xpath("//div[@id='tabs']//a[contains(@href,'Action.568307')]");
		summaryTab = By.xpath("//div[@id='tabs']//a[contains(@href,'Action.118402')]");
		premiumSummaryTab = By.xpath("//div[@id='tabs']//a[contains(@href,'Action.117502')]");
		underwritingAlertsTab = By.xpath("//div[@id='tabs']//a[contains(@href,'Action.433605')]");
		reviewChangesBtn = By.xpath("//div[@id='actionsButtons']//a[contains(@href,'Action.745233')]");
		renewedPremiumBtn = By.xpath("//div[@id='actionsButtons']//a[contains(@href,'Action.548107')]");
		exitBtn = By.xpath("//div[@id='actionsButtons']//a[contains(@href,'Action.144605')]");
		reviewReferals = By.xpath("//div[@id='actionsButtons']//a[contains(@href,'Action.433605')]");
		documentsTab = By.xpath("//div[@id='tabs']//a[contains(@href,'Action.434305')]");
		memberFlagTab = By.xpath("//div[@id='tabs']//a[contains(@href,'Action.667733')]");
		subjectivitiesTab = By.xpath("//div[@id='tabs']//a[contains(@href,'Action.516406')]");
		diaryTab = By.xpath("//div[@id='tabs']//a[contains(@href,'Action.519406')]");
		acceptBtn = By.xpath("//div[@id='footer']//a[contains(@href,'Action.530307')]");
		processRenewal = By.xpath("//div[@id='actionsButtons']//a[contains(@href,'Action.541607')]");
		processCompleted = By.xpath("//div[contains(text(),'Completed')]");
		exitTransactionBtn = By.xpath("//div[@id='footer']//a[contains(@href,'Action.129205')]");
		//transactionOrEndorsementTab = By.xpath("//a/span[contains(text(),'transactions/endorsements')]");
		transactionOrEndorsementTab = By.xpath("//div[@id='tabs']//a[contains(@href,'Action.129205')]");
	}
}
