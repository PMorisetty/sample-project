package com.pure.dragon;

public enum enumSearchBy {
			
	Any("Any"),
	ID("ID"),
	PolicyQuote_UW_Group_ID("PolicyQuote UW Group ID"),
	PolicyQuote_UW_Group_Name("PolicyQuote_UW Group Name"),
	PolicyQuote_Broker_ID("PolicyQuote Broker ID"),
	PolicyQuote_Broker_Name("PolicyQuote Broker Name"),
	PolicyQuote_Producer_ID("PolicyQuote Producer ID"),
	PolicyQuote_Producer_Name("PolicyQuote Producer Name"),
	PolicyQuote_Product_Name("PolicyQuote Product Name"),
	PolicyQuote_Insured_First_Name(""),
	PolicyQuote_Insured_Last_Name("PolicyQuote Insured Last Name"),
	PolicyQuote_Insured_ZIP("PolicyQuote Insured ZIP"),
	PolicyQuote_Insured_City("PolicyQuote Insured City"),
	PolicyQuote_Effective_Date("PolicyQuote Effective Date"),
	PolicyQuote_Expiration_Date("PolicyQuote Expiration Date"),
	PolicQuote_State_ID("PolicQuote State ID"),
	PolicyQuote_State_Name("PolicyQuote State Name"),
	PolicyQuote_Underwriter_Name("PolicyQuote Underwriter Name"),
	PolicyQuote_Creation_Date("PolicyQuote Creation Date"),
	Last_Update_Date("Last Update Date"),
	PolicyQuote_Name("PolicyQuote Name"),
	EDW_Extract_Schema_1("EDW Extract Schema 1"),
	PolicyQuote_Advisor_Servicer("PolicyQuote Advisor Servicer"),
	PolicyQuote_Resulting_Policy_Number("PolicyQuote Resulting Policy Number"),
	PolicyQuote_UTemplate_ID("PolicyQuote UTemplate ID"),
	PolicyQuote_Advisor_Servicer_ID("PolicyQuote Advisor Servicer ID"),
	PolicyQuote_Jurisdiction("PolicyQuote Jurisdiction"),
	
	/*-----------------------------------------------------*/
	/*--------------- agency select type enums ------------*/
	BrokerName("Broker Name"),
	BranchName("Branch Name"),
	Broker("Broker #"),	
	UWGroupName("UW Group Name"),
	BrokerID("Broker ID"),
	BrokerPhone("Broker Phone"),
	BrokerCity("Broker City"),
	BrokerState("Broker State"),
	BrokerZip("Broker Zip"),
	MarketingManager("Marketing Manager"),
	BrokerPrimaryContact("Broker Primary Contact"),
	BrokerCounty("Broker County"),
	ServiceAssociate("Service Associate"),
	RequestApprover("Request Approver"),
	AssignedTier("Assigned Tier"),
	EntityType("Entity Type"),
	BrokerClass("Broker Class"),
	AgencyDownloadEligibility("Agency Download Eligibility"),
	AgencyDownloadHistoricalData("Agency Download Historical Data"),
	AgencyDownloadEligibilityDate("Agency Download Eligibility Date"),
	AgencyDownloadHistoricalDataDate("Agency Download Historical Data Date"),
	UWPrimaryUnderwriter("UW - Primary Underwriter"),
	UWUnderwritingTech("UW - Underwriting Tech"),
	UWServiceAssociate("UW - Service Associate"),
	LastUpdateDate("Last Update Date"),
	EDWExtractSchema1("EDW Extract Schema 1"),
	PartnerTaxID("Partner Tax ID"),
	PartnerPUREConnectInd("Partner PURE Connect Ind"),
	BrokerStatus("Broker Status"),
	
	/*-----------------------------------------------------*/
	/*--------------- policy select type enums ------------*/
	PolicyNumber("Policy #"),
	Agency_Number("Agency Number"),
	City("City"),
	EffectiveDate("Eff.Date"),
	Policy_Holder("Policy Holder"),
	Policy_Status("Policy Status"),
	Producer("Producer"),
	SSN("SSN"),
	Type("Type"),
	LicenseNumber("License #"),
	ExpiryDate("Exp.Date"),
	License_State("License State"),
	FirstName("First Name"),
	LastName("Last Name"),
	Middle_Initial("Middle Initial"),
	Policy_Lines("Policy Lines"),
	Agency_Name("Agency Name"),
	Advisor_Servicer("Advisor/Servicer"),
	Policy_Skip_Renewal("Policy Skip Renewal"),
	Assigned_Underwriter("Assigned Underwriter"),
	Renewal_Creation_Prior_Days("Renewal Creation Prior Days"),
	Renewal_Process_Prior_Days("Renewal Process Prior Days"),
	Renewal_Review_Task_Prior_Days("Renewal Review Task Prior Days"),
	Commissioning_Broker("Commissioning Broker"),	
	EDW_Extract_Schema1("EDW Extract Schema 1"),
	Flags("Flags"),
	PolicyPreRenewal_UW_date("Policy Pre-renewal UW date");
	
	private String value;
	
	enumSearchBy(String value){
		this.value = value;
	}
	public String value(){
		return value;
	}
}
