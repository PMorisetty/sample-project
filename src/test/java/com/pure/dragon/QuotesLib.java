package com.pure.dragon;
import java.util.Hashtable;

import org.apache.xpath.operations.Quo;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pure.dragon.enumSearchBy;
import com.pure.report.CReporter;


public class QuotesLib extends SearchFromList{
	public QuotesLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/*Function to search quote depending on paramerters passed*/
	public void searchQuote(enumSearchBy enumSearchBy, String containsText) throws Throwable{
		search(enumSearchBy, containsText);	
		waitUntilLoadingMaskDisappear();
		waitUntilJQueryReady();
	}
	/*Function to click on New Quote button*/
	public void clickNewQuoteButton() throws Throwable{
		click(QuotesPage.newquote,"New Quote");
	}

	public void openQuote(String quoteName) throws Throwable{
		ExpectedCondition<Boolean> bodyTag = driver -> !((Boolean) driver.findElement(By.tagName("body")).getAttribute("class").contains("x-masked"));
		long wait_Time = 20;
		new WebDriverWait(driver, wait_Time).until(bodyTag);
		
		By quote = By.xpath("//div[text()='"+quoteName+"']/../preceding-sibling::td[1]//a");
		click(quote, "Quote Name");
		waitUntilJQueryReady();
	}

	public void createCopyQuote(String quoteToSearchLastName ,String newQuoteName) throws Throwable{
		//Search for the quote		
		QuotesLib quotesLib = new QuotesLib(driver, reporter);
		quotesLib.searchQuote(enumSearchBy.PolicyQuote_Name,quoteToSearchLastName );
		quotesLib.openQuote(quoteToSearchLastName);
		new SummaryPageLib(driver, reporter).clickCopyQuoteButton();
		type(QuotesPage.quoteCopyName, newQuoteName,"new quote copy name");
		click(QuotesPage.createCopyBtn,"createCopyBtn");
	}
	
	public void uncheck90DaysQuotes() throws Throwable{
		if(isCheckBoxSelected(QuotesPage.quotes90Days)){
			click(QuotesPage.quotes90Days, "uncheck 90DaysQuotes");
			waitUntilLoadingDisappear();
		}
	}
}
