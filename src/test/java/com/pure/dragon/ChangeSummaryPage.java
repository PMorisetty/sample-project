package com.pure.dragon;

import org.openqa.selenium.By;

public class ChangeSummaryPage {
	static By moreChangesButton;
	static By rateButton;
	static By exitButton;
	
	static {
		moreChangesButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.404605')]");
		rateButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.548007')]");
		exitButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.129205')]");
	}
}
