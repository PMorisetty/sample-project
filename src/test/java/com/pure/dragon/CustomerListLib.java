package com.pure.dragon;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pure.report.CReporter;


public class CustomerListLib extends SearchFromList{
	public CustomerListLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void searchCustomer(String containsText) throws Throwable{
		search(containsText);		
	}
	//select nth item listed
	public void selectNthCustomer(int n) throws Throwable{
		String nthItemXPATH = "(//table//input[@type='radio'])[" + n + "]";
		//waitForVisibilityOfElement(By.xpath(nthItemXPATH), "nth element");
		WebElement nthItem = driver.findElement(By.xpath(nthItemXPATH));
		if(nthItem.getAttribute("checked")==null){
			click(By.xpath(nthItemXPATH),"Select Customer radio");
			//Thread.sleep(2000);
		}
	}

	//select Customer listed
	public void SelectCustomer(String containsText) throws Throwable{
		this.searchCustomer(containsText);
		new WebDriverWait(driver, 60)
		.until(ExpectedConditions.numberOfElementsToBe(CustomerListPage.customerLink, 1));
		click(CustomerListPage.customerLink,"Select Customer");
	}
	//select nth Customer listed
	public void SelectCustomer(int nthCustomerListed) throws Throwable{
		selectNthCustomer(nthCustomerListed);
		click(CustomerListPage.customerLink,"Select Customer");
	}

}
