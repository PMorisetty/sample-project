package com.pure.dragon;

import org.openqa.selenium.By;

public class UnderwriterReferralsPage {
	static By underwriterReferralTab;
	static By underwriterReferralTabDiv;
	static By underwriterNotes;
	static By overriddenDropDown;
	static By commentsForBroker;
	static By underwriterName;
	
	static By saveChangesButton;
	static By orderCreditScoreButton;
	static By conditionalButton;
	static By endSubmissionButton;
	static By acceptButton;
	static By reassignReferButton;
	static By exitButton;
	
	static By uwReferralQuestions;
	static By approvedBtn;

	static {
		underwriterReferralTab = By.xpath("//span[contains(text(), 'underwriting referrals')]/..");
		underwriterReferralTabDiv = By.xpath("//a[contains(@href,'Action.488105')]/..");
		underwriterNotes = By.xpath("//table[@class='objectListTable']//textarea[@class='unFilledMandatoryField']");
		overriddenDropDown = By.xpath("//table[@class='objectListTable']//select[@title='Overridden?']");
		commentsForBroker = By.xpath("//div[@title='Underwriter Comments to Broker']/..//textarea");
		underwriterName = By.cssSelector("select[title='Assigned Underwriter']");
		
		saveChangesButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.488205')]");
		orderCreditScoreButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.511906')]");
		conditionalButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.630614')]");
		endSubmissionButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.411305')]");
		acceptButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.401305')]");
		reassignReferButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.491706')]");
		exitButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.88301')]");
		
		uwReferralQuestions = By.xpath("//table[@class='objectListTable']//textarea[@class='unFilledMandatoryField']/ancestor::tr[1]/td[2]//label");
		approvedBtn = By.xpath("//a[text()='>>>approved']");
	}
}
