package com.pure.dragon;

import org.openqa.selenium.By;

public class AgencyListPage {
	static By btnRadioOfItemListed, btnSelectAgency;
	static{
		btnRadioOfItemListed = By.xpath("//input[@type='radio']");
		btnSelectAgency = By.xpath("//div[@class='button']//a[contains(text(),'select agency')]");
	}
}
