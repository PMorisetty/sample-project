package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NewQuoteInformationPageLib extends ActionEngine{
	public NewQuoteInformationPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/* Function to click on create quote button*/
	public void clickCreateQuoteBtn() throws Throwable {
		click(NewQuoteInformationPage.createQuoteButton, "Create Quote");
	}
	/* Function to click on exit button*/
	public void clickExitBtn() throws Throwable {
		click(NewQuoteInformationPage.exitButton, "Exit");
	}
	/* Function to select Insurance Line*/
	public void selectInsuranceLine(String insuranceLine) throws Throwable {
		selectByVisibleText(NewQuoteInformationPage.insuranceLine, insuranceLine, "Insurance Line");
	}
	/* Function to select Risk State */
	public void selectRiskState(String riskState) throws Throwable {
		selectByVisibleText(NewQuoteInformationPage.riskState, riskState, "Risk State");
	}
	/* Function to set Quote name */
	public void setQuoteName(String quoteName) throws Throwable {
		type(NewQuoteInformationPage.quoteName, quoteName, "Quote Name");
	}
	/* Function to select licensed producer */
	public void selectLicensedProducer(String licensedProducer) throws Throwable {
		selectByVisibleText(NewQuoteInformationPage.licensedProducer, licensedProducer, "Licensed Producer");
	}
	/* Function to select advisor */
	public void selectAdvisor(String advisor) throws Throwable {
		selectByVisibleText(NewQuoteInformationPage.advisor, advisor, "advisor");
	}
	/* Function to fill required info */
	public void fillDetails(String insuranceLine, String riskState, String quoteName, String licensedProducer, String advisor) throws Throwable {
		this.selectInsuranceLine(insuranceLine);
		this.selectRiskState(riskState);
		this.setQuoteName(quoteName);
		this.selectLicensedProducer(licensedProducer);
		this.selectAdvisor(advisor);
	}
}
