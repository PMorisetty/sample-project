package com.pure.dragon;

import org.openqa.selenium.By;

public class MatchingCustomersPage {
	static By createNewCustomer, useExistingCustomer;
	static{
		createNewCustomer = By.xpath("//a[contains(text(),'create')]");
		useExistingCustomer = By.xpath("//a[contains(@href, 'Action.405705')]");
	}
}
