package com.pure.dragon;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PremiumPageLib extends ActionEngine{
	public PremiumPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/*Function to click on Premium details button*/
	public void clickPremiumDetailsButton() throws Throwable{
		click(PremiumPage.premiumDetailsButton, "Premium Details button");
	}
	/*Function to click on Deductible scenario's button*/
	public void clickDeductibleScenariosButton() throws Throwable{
		click(PremiumPage.deductibleScenariosButton, "Deductible Scenarios button");
	}
	/*Function to click on Exit button*/
	public void clickExitButton() throws Throwable{
		click(PremiumPage.exitButton, "Exit button");
	}
	/*Function to click on Create required forms button*/
	public void clickCreateRequiredFormsButton() throws Throwable{
		click(PremiumPage.createRequiredFormsButton, "Create Required Forms button");
	}
	/*Function to click on Bind button*/
	public void clickBindButton() throws Throwable{
		click(PremiumPage.bindButton, "Bind button");
	}
	/*Function to click on Quote proposal button*/
	public void clickQuoteProposalButton() throws Throwable{
		if(isVisibleOnly(PremiumPage.quoteProposalButton, "Quote Proposal button")){
			click(PremiumPage.quoteProposalButton, "Quote Proposal button");
			waitForBodyTag();
		}
	}
	/*Function to verify Premium tab is active tab*/
	public void verifyPremiumTabActive() throws Throwable{
		String value = getAttributeValue(PremiumPage.premiumTabDiv, "class");
		assertTrue(value.equalsIgnoreCase("selected"), "Verifying Premium tab is active tab");
	}

	/*Function to verify coverage exists in Premium details*/
	public void verifyCoverageExists(String coverage) throws Throwable{
		String tempXpath = xpathUpdator(PremiumPage.coverageLabel, coverage);
		assertElementPresent(By.xpath(tempXpath), coverage + " label");
	}
	/*Function to verify coverage limits*/
	public void verifyCoverageLimits(String coverage, String limit) throws Throwable{
		String tempXpath = xpathUpdator(PremiumPage.coverageLimitLabel, coverage);
		assertTrue(limit.equalsIgnoreCase(getText(By.xpath(tempXpath), coverage + " limit")),
				"Verifying limits '" + limit +"' for coverage '" +coverage +"'" );
	}
	/*Function to verify coverage deductible*/
	public void verifyCoverageDeductible(String coverage, String deductible) throws Throwable{
		String tempXpath = xpathUpdator(PremiumPage.coverageDeductibleLabel, coverage);
		assertTrue(deductible.equalsIgnoreCase(getText(By.xpath(tempXpath), coverage + " deductible")),
				"Verifying deductible '" + deductible +"' for coverage '" +coverage +"'" );
	}
	/*Function to verify coverage premium*/
	public void verifyCoveragePremium(String coverage, String premium) throws Throwable{
		String tempXpath = xpathUpdator(PremiumPage.coveragePremiumLabel, coverage);
		assertTrue(premium.equalsIgnoreCase(getText(By.xpath(tempXpath), coverage + " premium")),
				"Verifying premium '" + premium +"' for coverage '" +coverage +"'" );
	}	

	/*Function to verify underwriter referral message*/
	public void verifyUwReferralMessage() throws Throwable{
		String uwMessage = "Refer to Underwriting";
		assertTrue(uwMessage.equalsIgnoreCase(getText(PremiumPage.uwReferralMessageLabel,"Referal message label").trim()),
				"Verifying UW Referral message" );
		String uwMessage2 = "This submission requires an Underwriting review.";
		assertTrue(uwMessage2.equalsIgnoreCase(getText(PremiumPage.uwReferralMessageLabel2,"Referal message label").trim()),
				"Verifying UW Referral message" );
		String uwMessage3 = "Please click the \"refer to underwriter\" button to view the underwriting alerts and provide the requested information.";
		assertTrue(uwMessage3.equalsIgnoreCase(getText(PremiumPage.uwReferralMessageLabel3,"Referal message label").trim()),
				"Verifying UW Referral message" );
	}
	/*Function to verify Fraud and Cyber is first coverage irrespective of optional coverages added*/
	public void verifyCyberFraudFirstCoverage() throws Throwable{
		String tempXpath = xpathUpdator(PremiumPage.coverageLabel, "Optional Coverages");
		String coverageName = "Fraud and Cyber Defense";
		tempXpath = tempXpath + "/ancestor::tr[1]/following-sibling::tr[1]/td[2]//label";
		assertTrue(coverageName.equalsIgnoreCase(getText(By.xpath(tempXpath), "Coverage below optional coverage tag")),
				"Verifying coverageName is first coverage in optional coverage section" );
	}

	/*Function to click on Refer To Underwriting button*/
	public void clickReferToUWButton() throws Throwable{
		click(PremiumPage.referToUWButton, "Refer To Underwriting");
	}
	/*Function to click on Continue Refer To Underwriting button*/
	public void clickContinueUWReferButton() throws Throwable{
		click(PremiumPage.quoteCheckBox, "Quote checkbox");
		click(PremiumPage.continueReferToUWButton, "Continue Refer To Underwriting");
	}
	/*Function to enter broker comments*/
	public void setBrokerComments(String brokerComments) throws Throwable{
		type(PremiumPage.brokerComments, brokerComments, "Broker comments to UW");
	}
	/*Function to click on Refer To Underwriting button*/
	public void clickSubmitToUWButton() throws Throwable{
		click(PremiumPage.referToUwConfirmationButton, "Refer To Underwriting");
	}
	/*Function to click on Continue Refer To Underwriting button*/
	public void fillCommentsSubmitToUW(String brokerComments) throws Throwable{
		this.setBrokerComments(brokerComments);
		this.clickSubmitToUWButton();
	}


	/*Function to click on Quote Additional Lines button*/
	public void clickQuoteAdditionalLinesButton() throws Throwable{
		click(PremiumPage.quoteAdditionalLinesButton, "Quote Additional Lines");
	}

	/*Function to click on Quote Additional Lines button*/
	public String getCustomerID() throws Throwable{
		String customerID = "";
		customerID =  driver.findElement(PremiumPage.customerNameLink).getAttribute("href");
		customerID = customerID.split(",")[2];
		if(customerID.length() > 2) customerID = customerID.substring(2,customerID.length()-1);
		return customerID;
	}

	/*Function to verify coverage doesn't exists in Premium details*/
	public void verifyCoverageUnavailability(String coverage) throws Throwable{
		String tempXpath = xpathUpdator(PremiumPage.coverageLabel, coverage);
		isElementNotPresent(By.xpath(tempXpath), coverage + " label");
	}

	// navigate to quote details tab
	public void navigateToPremiumDetailsTab() throws Throwable {
		click(PremiumPage.premiumTab, "Premium Details Tab");
	}

	//To retrieve quote number
	public String getQuoteNumber(){
		String quoteNumber = "";
		try {
			String hrefValue = getAttributeValue(PremiumPage.quoteNumberLink, "href");
			quoteNumber = hrefValue.split(";")[1].split(",")[2].replace("\"", "").trim();
		} catch (Throwable e) {
			LOG.info(e.getMessage());
		}
		return quoteNumber;
	}

	//Retrieve premium values
	public double getOptionalCoveragePremium() throws Throwable{
		String totalPremiumString = getText(PremiumPage.optionalCoveragePremium, "Optional Coverage Premium");
		if(!totalPremiumString.equals("")){
			totalPremiumString = totalPremiumString.replace(",", "");
		}else if(totalPremiumString.equals("")){
			return 0;
		}
		return Double.parseDouble(totalPremiumString);
	}


	public double getTotalPremiumValue() throws Throwable{
		String totalPremiumString = getText(PremiumPage.totalPremiumValueLabel, "Total Premium");
		totalPremiumString = totalPremiumString.replace(",", "");
		return Double.parseDouble(totalPremiumString);
	}

	public double getLocationPremiumValue() throws Throwable{
		String locationPremiumString = getText(PremiumPage.locationPremium, "Location Premium");
		locationPremiumString = locationPremiumString.replace(",", "");
		return Double.parseDouble(locationPremiumString);
	}

	public double getTotalLocationPremiumValue() throws Throwable{
		String totalLocationPremiumString = getText(PremiumPage.totalLocationPremium, "Total Location Premium");
		totalLocationPremiumString = totalLocationPremiumString.replace(",", "");
		return Double.parseDouble(totalLocationPremiumString);
	}

	public double getSurplusContributionValue()throws Throwable{
		String surplusValueString = getText(PremiumPage.surplusContributionValueLabel, "Surplus Contribution");
		surplusValueString = surplusValueString.replace(",", "");
		return Double.parseDouble(surplusValueString);
	}

	public double getGrandTotalvalue() throws Throwable{
		String grandTotalPremiumString = getText(PremiumPage.grandTotalValueLabel, "Grand Total Premium");
		grandTotalPremiumString = grandTotalPremiumString.replace(",", "");
		return Double.parseDouble(grandTotalPremiumString);
	}

	public double getAdditionalRatePerThousand() throws Throwable{
		double additionalRatePerThousand = 0;
		if(isElementPresent(PremiumPage.additionalRatePerThousand, "Additional Rate Per Thousand")){
			String additionalRatePerThousandString = getText(PremiumPage.additionalRatePerThousand, "Additional Rate Per Thousand");
			additionalRatePerThousandString = additionalRatePerThousandString.replace(",", "");
			additionalRatePerThousand =  Double.parseDouble(additionalRatePerThousandString);
		}
		return additionalRatePerThousand;
	}


	public int extractNumberFromString(String label){		
		StringBuffer sBuffer = new StringBuffer();
		Pattern p = Pattern.compile("[0-9]+.[0-9]*|[0-9]*.[0-9]+|[0-9]+");
		Matcher m = p.matcher(label);
		while (m.find()) {
			sBuffer.append(m.group());
		}
		return Integer.parseInt(sBuffer.toString());
	}
	
	public String getPremium(String vehicleName, String coverageName){
		String premiumValue="";
		List<WebElement> rows = driver.findElements(By.xpath("//div[text()='details']/../..//table[@class='objectListTable']/tbody/tr"));
		int startRow = 0, lastRow = 0;
		for(int i = 1; i<rows.size(); i++){
			if(rows.get(i).findElement(By.cssSelector("td")).getAttribute("innerText").trim().contains(vehicleName)){
				startRow = i;
				continue;
			}
			if(startRow != 0){
				if(!rows.get(i).findElement(By.cssSelector("td")).getAttribute("innerText").trim().equalsIgnoreCase("")){
					lastRow = i-1;
					break;
				}else if(i == rows.size()-1){
					lastRow = i;
				}
			}
		}
		for(int i = startRow; i<=lastRow; i++){
//			if(rows.get(i).findElements(By.cssSelector("td")).get(1).getAttribute("innerText").trim().contains(coverageName)){
			if(rows.get(i).findElements(By.cssSelector("td")).get(1).getAttribute("innerText").trim().equalsIgnoreCase(coverageName)){
				premiumValue = rows.get(i).findElements(By.cssSelector("td")).get(4).getAttribute("innerText").trim();
				break;
			}
		}
		return premiumValue;
	}

}
