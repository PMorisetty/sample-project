package com.pure.dragon;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CustomerSummaryPageLib extends ActionEngine{
	public CustomerSummaryPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/* Function to click on new quote button*/
	public void clickNewQuoteBtn() throws Throwable {
		click(CustomerSummaryPage.newQuoteButton, "New Quote");
	}
	/* Function to click on first available quote*/
	public void selectQuote() throws Throwable {
		/*List<WebElement> elems = driver.findElements(CustomerSummaryPage.quoteLink);*/
		boolean status = false;
		try{
			/*for(int i= 0; i<elems.size(); i++){
			if(elems.get(i).isDisplayed()){
				((JavascriptExecutor)this.driver).executeScript("arguments[0].click();", elems.get(i));
				status = true;
				break;
			}
		}*/
			((JavascriptExecutor)this.driver).executeScript("arguments[0].click();", driver.findElement(CustomerSummaryPage.quoteLink));
			status = true;
		}catch(Exception ex){
			LOG.info(ex.getMessage());
		}finally{
			if (!status) {
				reporter.failureReport("Click : quoteLink", "Unable To Click On quoteLink" , driver);
			} else {
				reporter.SuccessReport("Click : quoteLink", "Successfully Clicked On quoteLink");
			}
		}

	}
	public void selectQuote(String quoteName) throws Throwable {
		By quoteLink = By.xpath("//td[contains(@style,'true')]//div[text()='"+ quoteName +"']/../preceding-sibling::td[1]//a");
		if(isElementPresent(quoteLink, "Quote Link")){	
			JSClick(quoteLink, "Quote Link");
		}else{
			click(SummaryPage.nextButton, "next button");
			selectQuote(quoteName);
		}
	}
}
