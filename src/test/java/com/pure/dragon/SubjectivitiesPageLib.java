package com.pure.dragon;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class SubjectivitiesPageLib extends ActionEngine{
	public SubjectivitiesPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/*Function to override dropdown values in Subjectivites Page*/
	public void overrideValues() throws Throwable{
		List<WebElement> elems = driver.findElements(SubjectivitiesPage.overRiddenDropdown);
		for(WebElement elem: elems){
			Select s = new Select(elem);
			s.selectByVisibleText("Yes");
		}
	}
	/*Function to click on Manual Bind button*/
	public void clickManualBindButton() throws Throwable{
		click(SubjectivitiesPage.manualBindButton, "Manual Bind button");
		waitForBodyTag();
	}
	/*Function to click on Save Changes button*/
	public void clickSaveChangesButton() throws Throwable{
		click(SubjectivitiesPage.manualBindButton, "Save Changes button");
	}
	/*Function to click on Exit button*/
	public void clickExitButton() throws Throwable{
		click(SubjectivitiesPage.manualBindButton, "Exit button");
	}	
}
