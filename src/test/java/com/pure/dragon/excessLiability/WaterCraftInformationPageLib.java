package com.pure.dragon.excessLiability;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class WaterCraftInformationPageLib extends ActionEngine{
	public WaterCraftInformationPageLib(EventFiringWebDriver driver, CReporter reporter) {
		this.driver = driver;
		this.reporter = reporter;
	}
	private void enterWaterCraftMake(String waterCraftMake) throws Throwable{
		type(WaterCraftInformationPage.waterCraftMake, waterCraftMake, " WaterCraft Make");
	}
	private void enterWaterCraftModel(String waterCraftModel) throws Throwable{
		type(WaterCraftInformationPage.waterCraftModel, waterCraftModel, " WaterCraft Model");
	}
	private void enterwaterCraftHullValue(String waterCraftHullValue) throws Throwable{
		type(WaterCraftInformationPage.waterCraftHullValue, waterCraftHullValue, " WaterCraft HullValue");
	}
	private void enterwaterCraftLength(String waterCraftLength) throws Throwable{
		type(WaterCraftInformationPage.waterCraftLength, waterCraftLength, " WaterCraft Length in feet");
	}
	private void enterwaterCraftHorsePower(String waterCraftHorsePower) throws Throwable{
		type(WaterCraftInformationPage.waterCraftHorsePower, waterCraftHorsePower, " WaterCraft Horse Power");
	}
	private void enterwaterCraftMaxDesignSpeed(String waterCraftMaxDesignSpeed) throws Throwable{
		selectByVisibleText(WaterCraftInformationPage.waterCraftMaxDesignSpeed, waterCraftMaxDesignSpeed, " WaterCraft Max Design Speed");
	}
	public void fillWaterCraftInformationDetails(Hashtable<String, String> data) throws Throwable{
		switch(data.get("state")){
		case "CA":
			enterwaterCraftMaxDesignSpeed(data.get("waterCraftMaxDesignSpeed"));
			break;
		case "NY":
		case "TX":
		case "FL":
			break;
		}
		enterWaterCraftMake(data.get("waterCraftMake"));
		enterWaterCraftModel(data.get("waterCraftModel"));
		enterwaterCraftHullValue(data.get("waterCraftHullValue"));
		enterwaterCraftLength(data.get("waterCraftLength"));
		enterwaterCraftHorsePower(data.get("waterCraftHorsePower"));
	}
}
