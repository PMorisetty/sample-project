package com.pure.dragon.excessLiability;

import org.openqa.selenium.By;

public class SelectCoveragesPage {

	static By excessLiabilityLimit,UIMLiabilityLimit,employmentPracticesLiabilityLimit,uninsuredUnderinsuredLiability,
	howManyEmployees;
	
	static{
		excessLiabilityLimit = By.xpath("//select[@title='Excess Liability Limit']");
		UIMLiabilityLimit= By.xpath("//select[@title='UIM  Liability Limit']");
		employmentPracticesLiabilityLimit = By.cssSelector("select[title='Employment Practices Liability Limit']");
		uninsuredUnderinsuredLiability = By.cssSelector("select[title='Uninsured/Underinsured Liability']");
		howManyEmployees = By.cssSelector("select[title='How many employees?']");
	}
	
}
