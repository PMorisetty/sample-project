package com.pure.dragon.excessLiability;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class SelectCoveragesPageLib extends ActionEngine{
	public SelectCoveragesPageLib(EventFiringWebDriver driver, CReporter reporter) {
		this.driver = driver;
		this.reporter = reporter;
	}
	public void fillSelectCoverages(Hashtable<String, String> data) throws Throwable{
		selectExcessLiabilityLimit(data.get("excessLiabilityLimit"));
		selectUIMCoverage(data.get("UIMCoverage"));
		selectEmploymentPracticesLiability(data.get("employmentPracticesLiability"));
		switch (data.get("state")) {
		case "NY":
			break;
		case "CA":
		case "TX":	
		case "FL":
			selectUninsuredLiability(data.get("uninsuredunderinsuredliability"));
			break;
		
		}
		selectHowManyEmployees(data.get("howManyEmployees"));
	}
	private void selectExcessLiabilityLimit(String excessLiabilityLimit)throws Throwable{
		selectByVisibleText(SelectCoveragesPage.excessLiabilityLimit, excessLiabilityLimit, "Excess Liability Limit");
	}
	private void selectUIMCoverage(String UIMLiabilityLimit)throws Throwable{
		selectByVisibleText(SelectCoveragesPage.UIMLiabilityLimit, UIMLiabilityLimit, "UIM Liability Limit");
	}
	private void selectEmploymentPracticesLiability(String employmentPracticesLiabilityLimit) throws Throwable{
		selectByVisibleText(SelectCoveragesPage.employmentPracticesLiabilityLimit, employmentPracticesLiabilityLimit, "Employment Practices Liability");
	}
	private void selectUninsuredLiability(String uninsuredUnderinsuredLiability)throws Throwable{
		selectByVisibleText(SelectCoveragesPage.uninsuredUnderinsuredLiability, uninsuredUnderinsuredLiability, "Uninsured Underinsured Liability");
	}
	private void selectHowManyEmployees(String howManyEmployees)throws Throwable{
		selectByVisibleText(SelectCoveragesPage.howManyEmployees, howManyEmployees, "No. Of Employees");
	}
}
