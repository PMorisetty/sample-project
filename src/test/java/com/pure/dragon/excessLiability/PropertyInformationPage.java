package com.pure.dragon.excessLiability;

import org.openqa.selenium.By;

public class PropertyInformationPage {
    
	static By swimmingPool,dwellingType;
	
	static{
		
		swimmingPool = By.xpath("//select[@title='Swimming Pool?']");
		dwellingType = By.xpath("//select[@title='Dwelling Type']");
	}
	
	
}
