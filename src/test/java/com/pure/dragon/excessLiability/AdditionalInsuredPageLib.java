package com.pure.dragon.excessLiability;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class AdditionalInsuredPageLib extends ActionEngine{
	public AdditionalInsuredPageLib(EventFiringWebDriver driver, CReporter reporter) {
		this.driver = driver;
		this.reporter = reporter;
	}	
	private void selectAdditionalInsuredType(String insuredType) throws Throwable{
		selectByVisibleText(AdditionalInsuredPage.insuredType, insuredType, "Insured Type");		
	}
	private void enterInsuredName(String insuredName) throws Throwable{
		type(AdditionalInsuredPage.insuredName, insuredName, "Insured Name");
		type(AdditionalInsuredPage.insuredName, Keys.TAB, "Tab");
	}
	private void enterInsuredStreetAddress(String InsuredStreetAddress) throws Throwable{
		type(AdditionalInsuredPage.InsuredStreetAddress, InsuredStreetAddress, "Street Address");
		type(AdditionalInsuredPage.InsuredStreetAddress, Keys.TAB, " Tab");
	}
	private void enterInsuredCityName(String insuredCity) throws Throwable {
		type(AdditionalInsuredPage.insuredCity, insuredCity, "Insured City Name");
		type(AdditionalInsuredPage.insuredCity, Keys.TAB, "Tab");
	}
	private void selectInsuredState(String insuredState) throws Throwable {
		selectByVisibleText(AdditionalInsuredPage.insuredState, insuredState, "Insured State");
	}
	private void enterInsuredZipCode(String insuredZip) throws Throwable {
		type(AdditionalInsuredPage.insuredZip, insuredZip, "Insured Zip Code");
		type(AdditionalInsuredPage.insuredZip, Keys.TAB, "Tab");
	}
	public void fillAdditionalInsuredPageDetails(Hashtable<String,String> data) throws Throwable{
		selectAdditionalInsuredType(data.get("additionalInsuredType"));
		enterInsuredName(data.get("additionalInsuredName"));
		enterInsuredStreetAddress(data.get("additionalInsuredStreetAddress"));
		enterInsuredCityName(data.get("additionalInsuredCity"));
		selectInsuredState(data.get("additionalInsuredState"));
		enterInsuredZipCode(data.get("additionalInsuredZip"));
	}
}
