package com.pure.dragon.excessLiability;

import org.openqa.selenium.By;

public class AdditionalInsuredPage {
	
	static By insuredType,insuredName, InsuredStreetAddress, insuredCity,insuredState, insuredZip;
	
	static{
		insuredType = By.xpath("//th[contains(text(),'Type')]/../following-sibling::tr//select");
		insuredName = By.cssSelector("input[title='Name']");
		InsuredStreetAddress =  By.cssSelector("input[title='Address Line 1']");
		insuredCity =  By.cssSelector("input[title='City']");
		insuredState = By.cssSelector("select[title='State']");
		insuredZip = By.cssSelector("input[title='Zip']");
	}
	

}
