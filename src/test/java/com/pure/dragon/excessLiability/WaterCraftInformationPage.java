package com.pure.dragon.excessLiability;

import org.openqa.selenium.By;

public class WaterCraftInformationPage {

	static By waterCraftMake, waterCraftModel, waterCraftHullValue, waterCraftLength,waterCraftHorsePower,waterCraftMaxDesignSpeed;

	static{
		waterCraftMake = By.cssSelector("input[title='Make']");
		waterCraftModel = By.cssSelector("input[title='Model']");
		waterCraftHullValue = By.cssSelector("input[title='Hull Value ($)']");
		waterCraftLength = By.cssSelector("input[title='Length (feet)']");
		waterCraftHorsePower = By.cssSelector("input[title='Horsepower']");
		waterCraftMaxDesignSpeed = By.cssSelector("select[title ='Max Design Speed']");
	}
}
