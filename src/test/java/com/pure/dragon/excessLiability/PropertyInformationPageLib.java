package com.pure.dragon.excessLiability;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PropertyInformationPageLib extends ActionEngine {
	public PropertyInformationPageLib(EventFiringWebDriver driver, CReporter reporter) {
		this.driver = driver;
		this.reporter = reporter;
	}
	public void fillPropertyInformation(Hashtable<String, String> data) throws Throwable{
		selectSwimmingPool(data.get("swimmingPool"));
		switch (data.get("state")) {
		case "NY":
		case "TX":	
		case "CA":
		case "FL":
			selectDwellingType(data.get("dwellingType"));
			break;
			
		}
	}
	private void selectSwimmingPool(String swimmingPool)throws Throwable{
		selectByVisibleText(PropertyInformationPage.swimmingPool, swimmingPool, "Swimming pool?");
	}
	private void selectDwellingType(String dwellingType)throws Throwable{
		selectByVisibleText(PropertyInformationPage.dwellingType, dwellingType, "Dwelling Type");
	}
}
