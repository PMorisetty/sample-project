package com.pure.dragon;

import org.openqa.selenium.By;

public class CustomersTABnavigationsPage {
	
	static By
	summary,coveragesummary,customerDetails,policyDeliveryPreferences,
	billingDeliveryPreferences,requiredforms,documents,proposals,diary,
	billingDashboard,history,brokerHistory,insuranceScoreManagement,
	CLUE_MVRreports,memberFlag,accountReferral
	;
	
	static{
		summary = By.xpath("//a[contains(@href,'Action.118101')]");
		coveragesummary = By.xpath("//a[contains(@href,'Action.622514')]");
		customerDetails = By.xpath("//a[contains(@href,'Action.550207')]");
		policyDeliveryPreferences = By.xpath("//a[contains(@href,'Action.548707')]");
		billingDeliveryPreferences = By.xpath("//a[contains(@href,'Action.550307')]");
		requiredforms= By.xpath("//a[contains(@href,'Action.703333')]");
		documents=By.xpath("//a[contains(@href,'Action.171305')]");
		proposals=By.xpath("//a[contains(@href,'Action.608946')]");
		diary=By.xpath("//a[contains(@href,'Action.477705')]");
		billingDashboard=By.xpath("//a[contains(@href,'Action.556409')]");
		history=By.xpath("//a[contains(@href,'Action.508706')]");
		brokerHistory=By.xpath("//a[contains(@href,'Action.577018')]");
		insuranceScoreManagement=By.xpath("//a[contains(@href,'Action.589914')]");
		CLUE_MVRreports = By.xpath("//a[contains(@href,'Action.634517')]");
		memberFlag=By.xpath("//a[contains(@href,'Action.657733')]");
		accountReferral=By.xpath("//a[contains(@href,'Action.660833')]");
		
	}
}
