package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class QuotesTABnavigationsLib extends ActionEngine{

	public QuotesTABnavigationsLib(EventFiringWebDriver driver,
			CReporter reporter) {
		this.driver = driver;
		this.reporter=reporter;
	}
	public void navigateToSummary() throws Throwable{
		click(QuotesTABnavigationsPage.summary,"summary");
	}
	public void navigateToQuoteDetails() throws Throwable{
		click(QuotesTABnavigationsPage.quoteDetails,"summary");
	}
	public void navigateToUnderwritingReferrals() throws Throwable{
		click(QuotesTABnavigationsPage.underwritingReferrals,"summary");
	}
	public void navigateToSubjectivities() throws Throwable{
		click(QuotesTABnavigationsPage.subjectivities,"summary");
	}
	public void navigateToDocuments() throws Throwable{
		click(QuotesTABnavigationsPage.documents,"summary");
	}
	public void navigateToDiary() throws Throwable{
		click(QuotesTABnavigationsPage.diary,"summary");
	}
	public void navigateToMemberFlag() throws Throwable{
		click(QuotesTABnavigationsPage.memberFlag,"summary");
	}
}
