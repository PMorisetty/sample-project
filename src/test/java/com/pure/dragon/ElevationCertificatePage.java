package com.pure.dragon;

import org.openqa.selenium.By;

public class ElevationCertificatePage {

	//elevation certificate
	static By baseFloodElevation, buildingDiagramNumber, topOfBottomFloor, topOfNextFloor, 
	bottomOfAttachedGarage, totalSquareFeet, permanentFloodOpenings, totalAreaOfPermanentOpenings;
	static{
		//elevation certificate
		baseFloodElevation = By.xpath("//input[@title='Base Flood Elevation']");
		buildingDiagramNumber = By.xpath("//select[@title='What is the building diagram number?']");
		topOfBottomFloor = By.xpath("//input[contains(@title, 'Top of bottom floor')]");
		topOfNextFloor = By.xpath("//input[contains(@title, 'Top of the next floor')]");
		bottomOfAttachedGarage = By.xpath("//input[contains(@title, 'Bottom of the attached garage')]");
		totalSquareFeet = By.xpath("//input[contains(@title, 'total square feet ')]");
		permanentFloodOpenings = By.xpath("//input[contains(@title, 'No. of permanent flood openings')]");
		totalAreaOfPermanentOpenings = By.xpath("//input[contains(@title, 'Total area of all permanent openings')]");
	}	
}
