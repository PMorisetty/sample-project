package com.pure.dragon;

import org.openqa.selenium.By;

public class SubjectivitiesPage {
	static By overRiddenDropdown;
	static By saveChangesButton;
	static By manualBindButton;
	static By exitButton;	

	static {
		overRiddenDropdown = By.cssSelector("select[title='Overridden?']");
		saveChangesButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.516606')]");
		manualBindButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.516806')]");
		exitButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.129205')]");
	}
}
