package com.pure.dragon;

import org.openqa.selenium.By;

public class QuotesPage {
	
	static By newquote, quotenottake,deletequote;	
	static By btnGo,btnFirst,btnPrev,btnNext,btnLast,btnExport;	
	static By quoteCopyName,createCopyBtn,quotes90Days;
	
	static{
		newquote 	= By.xpath("//*[contains(@href,'Action.393302')]");
		quotenottake= By.xpath("//*[contains(@href,'Action.524607')]");
		deletequote = By.xpath("//*[contains(@href,'Action.96701')]");				
		btnFirst 	= By.xpath("//div[.]//a[contains(text(),'first')]");
		btnPrev 	= By.xpath("//div[.]//a[contains(text(),'prev')]");
		btnNext 	= By.xpath("//div[.]//a[contains(text(),'next')]");
		btnLast 	= By.xpath("//div[.]//a[contains(text(),'last')]");
		btnExport 	= By.xpath("//div[.]//a[contains(text(),'export')]");
		quoteCopyName = By.xpath("//div[contains(text(),'Quote Copy Name')]/following-sibling::div/input");
		createCopyBtn = By.xpath("//a[contains(text(), 'create copy')]");
		quotes90Days=By.xpath("//div[contains(@title,'Show last 90 days only')]/../span/..//input");
	}
	
}
