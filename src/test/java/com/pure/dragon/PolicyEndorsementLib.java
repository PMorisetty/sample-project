
package com.pure.dragon;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PolicyEndorsementLib extends ActionEngine {
	public PolicyEndorsementLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/* Function to get text in notes text box */
	public String getNotes() throws Throwable {
		return getText(PolicyEndorsement.notes, "Notes Text Box");
	}

	/* Function to enter data in notes text box */
	public void setNotes(String notes) throws Throwable {
		type(PolicyEndorsement.notes, notes, "Notes Text Box");
	}

//	remove this code	
/*	 Function to select transaction type 
	// This is generic function 
	public void selectTransactionType(String chooseTransactionType)
			throws Throwable {
		selectByVisibleText(PolicyEndorsement.chooseTransactionType,
				chooseTransactionType, "Transaction Type dropdown");
	}
*/
	
	/* Function to select transaction type Endorsement */
	public void selectTransactionTypeEndorsement(String transactionTypeEndorsement)
			throws Throwable {
		if(transactionTypeEndorsement.equalsIgnoreCase("Yes")){
			selectByVisibleText(PolicyEndorsement.chooseTransactionType,
					"Endorsement", "Transaction Type dropdown: Endorsement");
		}
	}

	/* Function to select transaction type Cancellation */
	public void selectTransactionTypeCancellation(String transactionTypeEndorsementCancellation)
			throws Throwable {
		if(transactionTypeEndorsementCancellation.equalsIgnoreCase("Yes")){
			selectByVisibleText(PolicyEndorsement.chooseTransactionType,
					"Cancellation", "Transaction Type dropdown: Cancellation");
		}
	}

	/* Function to select transaction type Rewrite */
	public void selectTransactionTypeRewrite(String transactionTypeEndorsementRewrite)
			throws Throwable {
		if(transactionTypeEndorsementRewrite.equalsIgnoreCase("Yes")){
			selectByVisibleText(PolicyEndorsement.chooseTransactionType,
					"New Business Rewrite", "Transaction Type dropdown: Rewrite");
		}
	}

	/* Function to select transaction type ReInstatement */
	public void selectTransactionTypeReInstatement(String transactionTypeEndorsementReeInstatement)
			throws Throwable {
		if(transactionTypeEndorsementReeInstatement.equalsIgnoreCase("Yes")){
			selectByVisibleText(PolicyEndorsement.chooseTransactionType,
					"Reinstatement", "Transaction Type dropdown: Reinstatement");
		}
	}

	/* Function to select endorsement type */
	public void selectEndorsementType(String endorsementType) throws Throwable {
		selectByVisibleText(PolicyEndorsement.selectEndorsementType,
				endorsementType, "Endorsement Type Dropdown");
	}

	/* Function to get effective date populated in application */
	public String getEffectiveDate() throws Throwable {
		return getText(PolicyEndorsement.newTransEffectiveDate,
				"Effective Date Field");
	}

	/* Function to set effective date as per date passed in param */
    public void setEffectiveDate(String newTransEffectiveDate) throws Throwable {
          if(newTransEffectiveDate!=null && !newTransEffectiveDate.equalsIgnoreCase("")){
                type(PolicyEndorsement.newTransEffectiveDate, newTransEffectiveDate,
                      "Effective Date Field");
          }else{
                //Below code picks system date. If you want to test with date(current date + 1), then
                //don't enter any date in excel
                
                String pattern = "MM-dd-yyyy";
                int incrementBy = 10;
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                newTransEffectiveDate = simpleDateFormat.format(new Date());
                newTransEffectiveDate=getNextDateByIncrement(newTransEffectiveDate,incrementBy);
                
                type(PolicyEndorsement.newTransEffectiveDate, newTransEffectiveDate,
                            "new Effective Date Field incremented by "+incrementBy);
          }
    }     
	/* Function to click on Next button */
	public void clickNextBtn() throws Throwable {
		click(PolicyEndorsement.nextBtn, "next Button");
	}

	/* Function to select cancellation requested by from drop down */
	public void selectCancellationRequestedBy(String requestedBy)
			throws Throwable {
		selectByVisibleText(PolicyEndorsement.cancellationRequestedBy, requestedBy,
				"Requested By Dropdown");
	}

	/* Function to set description */
	public void setDescription(String description) throws Throwable {
		type(PolicyEndorsement.description, description, "Description Field");
	}

	/* Function to select cancel method from drop down */
	public void selectCancelMethod(String cancelMethod) throws Throwable {
		selectByVisibleText(PolicyEndorsement.cancelMethod, cancelMethod,
				"Cancellation Method Dropdown");
	}

	/* Function to select cancellation reason from drop down */
	public void selectCancelReason(String cancelReason) throws Throwable {
		selectByVisibleText(PolicyEndorsement.cancelReason, cancelReason,
				"Cancellation Reason Dropdown");
	}

	/* Function to click on modify out of sequence button */
	public void clickModifyOutofSequenceBtn() throws Throwable {
		click(PolicyEndorsement.modifySequenceBtn,
				"Modify out of Sequence Button");
	}

	/* Function to click on Process button */
	public void clickProcessBtn() throws Throwable {
		click(PolicyEndorsement.proceessBtn, "Process Button");
		acceptAlert();// Are you sure you wish to process this
		waitForBodyTag();				// cancellation?:accept
	}

	/* Function to get document generation status */
	public String getDocGenStatus() throws Throwable {
		return getText(PolicyEndorsement.docGenStatus,
				"Documents Generation Status");
	}

	public void clickNewBtn() throws Throwable {
		click(PolicyEndorsement.newBtn, "New Button");
		//Thread.sleep(3000);
	}
	/* Function to select Reinstatement loss/postcanellation Yes/No from drop down */
	public void selectClaimsLostPostCancellation(String claimLossPostCancellation) throws Throwable {
		selectByVisibleText(PolicyEndorsement.claimLossPostCancellation, claimLossPostCancellation,
				"ClaimLoss/PostCancellation Dropdown");
	}
	/* Function to select Reinstatement Reason from drop down */
	public void selectReinstmtReason(String reinstmtReason) throws Throwable {
		selectByVisibleText(PolicyEndorsement.reinstmtReason, reinstmtReason,
				"Reinstmt Reason Dropdown");
	}
	/*
	 * This method is to select the Endorsement type from the policy Transaction
	 * Type is "Endorsement"
	 */
	public void policyEndorsement(String transactionTypeEndorsement,
			String endorsementType, String newTransEffectiveDate, String notes)
					throws Throwable {
		selectTransactionTypeEndorsement(transactionTypeEndorsement);
		selectEndorsementType(endorsementType);
		setEffectiveDate(newTransEffectiveDate);
		setNotes(notes);
	}

	/*
	 * This method is to select the Cancellation type from the policy
	 * Transaction Type is "Cancellation"
	 */
	public void policyCancellationFirstStep(String transactionTypeCancel,
			String requestedBy, String newTransEffectiveDate, String notes)
					throws Throwable {
		selectTransactionTypeCancellation(transactionTypeCancel);
		setEffectiveDate(newTransEffectiveDate);
		selectCancellationRequestedBy(requestedBy);
		setNotes(notes);
	}

	/*
	 * This method is to select the Cancellation type from the policy
	 * Transaction Type is "Cancellation" Process the cancel request by accept
	 * the
	 */
	public void policyCancellationSecondStep(String cancelMethod,
			String cancelReason) throws Throwable {
		selectCancelMethod(cancelMethod);
		selectCancelReason(cancelReason);
	}

	/*
	 * This method is to select the Reinstatement type from the policy
	 * Transaction Type is "Reinstatement"
	 */
	public void policyReinstatementFirstStep(String transactionTypeReinstatement,
			String newTransEffectiveDate, String notes, String description)
					throws Throwable {
		selectTransactionTypeReInstatement(transactionTypeReinstatement);
		setEffectiveDate(newTransEffectiveDate);
		setNotes(notes);
		setDescription(description);
	}

	public void policyNewBusinessRewrite(String transactionTypeRewrite,	String newTransEffectiveDate, String notes)
			throws Throwable {
		selectTransactionTypeRewrite(transactionTypeRewrite);
		setEffectiveDate(newTransEffectiveDate);
		setNotes(notes);
	}
	/*
	 * This method is to select the Reinstatement type from the policy
	 * Transaction Type is "Reinstatement"
	 */
	public void policyReinstatementSecondStep(String claimLossPostCancellation,
			String reinstmtReason)
					throws Throwable {
		selectClaimsLostPostCancellation(claimLossPostCancellation);
		selectReinstmtReason(reinstmtReason);

	}
	/* Function to click on Next button */
	public void navigateToTransationOrEndorsementTab() throws Throwable {
		waitUntilJQueryReady();
		click(PolicyEndorsement.transactionOrEndorsementTab, "TransactionOrEndorsementTab");
	}

	/* Function to click on Customer name link */
	public void navigateToCustomerDetails() throws Throwable {
		waitUntilJQueryReady();
		click(PolicyEndorsement.customerNameLink, "Customer Name Link");
	}

	public void navigateToNBtransaction() throws Throwable{
		click(PolicyEndorsement.nbTransLink, "NB Link");
	}
	public void navigateToSubjectivities() throws Throwable{
		click(PolicyEndorsement.subjectivitiesTab, "Subjectivities Tab");
	}
	
	public void completeEndosementIfErrorOccur(Hashtable<String, String> data) throws Throwable{
		if(isElementPresent(PolicyEndorsement.errorMesage, "Error message"))
		{
			UnderwriterReferralsPageLib underwriterReferralsPageLib=new UnderwriterReferralsPageLib(driver, reporter);
			EndorsementModifiedPremiumPageLib EndorsementModifiedPremiumPageLib= new EndorsementModifiedPremiumPageLib(driver, reporter);
			EndorsementModifiedPremiumPageLib.clickReviewReferralsButton();
			underwriterReferralsPageLib.completeUnderwriterQuestions(data.get("underwriterNote"));
			underwriterReferralsPageLib.selectUnderwriter(data.get("uwName"));
			underwriterReferralsPageLib.clickApprovedButton();
		}
	}
	}
	
	

