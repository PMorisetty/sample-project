package com.pure.dragon;

import org.openqa.selenium.By;

public class FinalSummaryPage {
	static By requestBindButton;
	static By backButton;
	static By exitButton;

	static {
		requestBindButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.724433')]");
		backButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.566507')]");
		exitButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.118101')]");
	}
}
