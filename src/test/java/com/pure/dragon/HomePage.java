package com.pure.dragon;

import org.openqa.selenium.By;

public class HomePage {
	static By userNameLabel;
	static By logout;
	static By homePageLink;
	static By tasksPageLink;
	static By agentResourcesPageLink;
	static By customersPageLink;
	static By quotesPageLink;
	static By policiesPageLink;
	static By locationsPageLink;
	static By partersPageLink;
	static By automationPageLink;
	static By userName,userNamePUREOKTA;
	static By password,passwordPUREOKTA;
	static By brokerNumber;
	static By loginButton,signinButton;
	public static By dragonSessionID;
	//For agent
	static By newQuoteButton;
	static By customersButton;
	static By agentResourcesButton;
	
	
	static {
		userNameLabel = By.xpath("//div[@id='controls']/span");
		logout = By.xpath("//div[@id='controls']//a[contains(@href,'\"Action.7\"')]");
		homePageLink = By.xpath("//a[.=\"home\"]");
		tasksPageLink = By.xpath("//a[.=\"tasks\"]");	
		agentResourcesPageLink = By.xpath("//a[.=\"agent resources\"]");	
		customersPageLink = By.xpath("//a[.=\"customers\"]");	
		quotesPageLink = By.xpath("//a[.=\"quotes\"]");	
		policiesPageLink = By.xpath("//a[.=\"policies\"]");	
		locationsPageLink = By.xpath("//a[.=\"locations\"]");	
		partersPageLink = By.xpath("//a[.=\"partners\"]");	
		automationPageLink = By.xpath("//a[.=\"automation\"]");	
		
		userName = By.cssSelector("input[title='Username']");
		brokerNumber = By.cssSelector("input[title='Carrier assigned agency number.']");
		password = By.cssSelector("input[title='Password']");
		
		userNamePUREOKTA =  By.cssSelector("input[id='okta-signin-username']");
		passwordPUREOKTA =  By.cssSelector("input[id='okta-signin-password']");
		signinButton = By.cssSelector("input[id='okta-signin-submit']");
		
		loginButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.3')]");
		dragonSessionID = By.xpath("//input[@name='DRAGON_SESSION_ID']");
		
		newQuoteButton = By.cssSelector("a[href*='Action.393302']");
		customersButton = By.cssSelector("a[href*='Action.392802'][title='search customers']");
		agentResourcesButton = By.cssSelector("a[href*='Action.618914'][title='agent resources']");
		
	}
}
