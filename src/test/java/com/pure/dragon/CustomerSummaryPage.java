package com.pure.dragon;

import org.openqa.selenium.By;

public class CustomerSummaryPage {
	static By newQuoteButton, quoteLink;
	static{
		newQuoteButton = By.cssSelector("a[href*='Action.492306']");
		quoteLink = By.cssSelector("a[title='Quote Name']");
	}
}
