package com.pure.dragon;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.homeOwners.QuoteDetailsPageLib;
import com.pure.report.CReporter;

public class InsuranceScoreManagementLib extends ActionEngine{
	public InsuranceScoreManagementLib(EventFiringWebDriver driver, CReporter reporter) throws Throwable {
		this.driver = driver;
		this.reporter = reporter;
		new CustomersTABnavigationsLib(driver, reporter).NavigateToInsuranceScoreManagement();
	}

	public void addNewInsuranceScore(Hashtable<String, String> data) throws Throwable{
		//For CA and HI state insurance score model is excluded. Include more excluded states,if any.
		if(data.get("state").equalsIgnoreCase("CA") || data.get("state").equalsIgnoreCase("HI")){
			new CustomersTABnavigationsLib(driver, reporter).NavigateTosummary();
			new CustomerSummaryPageLib(driver, reporter).selectQuote();
			new QuotesTABnavigationsLib(driver, reporter).navigateToQuoteDetails();
			return;
		}
		int numOfScoresAdded = 0;
		String selectValue = null;
		newReport();
		String memberToSelect;
		if( data.get("First_Name_Txt_1")!=null){
			memberToSelect = data.get("First_Name_Txt_1").trim() + " " + data.get("Last_Name_Txt_1").trim();
		}else{
			memberToSelect = data.get("insuredFirstName").trim() + " " + data.get("insuredLastName").trim();
		}
		String insuranceScore = data.get("Insurance_Score");

		//Refer states and credit score models.xlsx file attached to framework 
		selectByVisibleText(InsuranceScoreManagementPage.member,memberToSelect, "member");
		String state=data.get("state");
		switch(state)
		{	
		case "FL":
		case "NY":
		case "RI":
		case "DC":
		case "NJ":
		case "AL":
		case "TX":
		case "IL":
		case "PA":
		case "MI":	
		case "DE":
		case "OH":
		case "MA":		
		case "OK":
		case "IA":
		case "WY":
		case "OR":
		case "AR":	
		case "LA":
		case "WI":
		case "MS":
		case "UT":
		case "MN":
		case "NM":
		case "WV":
		case "CO":
		case "SD":
		case "NE":
		case "KY":			
		case "VT":
		case "ND":
		case "VA":
		case "NH":
		case "TN":
		case "HI":
		case "CA":
			selectValue = "Attract One - CW";
			selectByVisibleText(InsuranceScoreManagementPage.insuranceScoreModel, selectValue, "insuranceScoreModel");
			break;
		case "SC":
			selectValue = "Attract Standard Auto - CW";
		    selectByVisibleText(InsuranceScoreManagementPage.insuranceScoreModel, selectValue, "insuranceScoreModel");
		    break;
		case "CT":
		case "AZ":
		case "GA":
		case "IN":
		case "KS":
		case "ME":
		case "MD":
		case "MT":
		case "NV":
		case "NC":
		case "WA":
			selectValue = "Attract One - "+ state;
			selectByVisibleText(InsuranceScoreManagementPage.insuranceScoreModel, selectValue, "insuranceScoreModel");
			break;		
		
		case "ID":
		case "AK":
		case "MO":break;
		}

		type(InsuranceScoreManagementPage.insuranceScore,insuranceScore,"Insurance Score");
		type(InsuranceScoreManagementPage.insuranceScore,Keys.TAB,"TAB key");

		type(InsuranceScoreManagementPage.comments,"-","Comments");
		type(InsuranceScoreManagementPage.comments,Keys.TAB,"TAB key");

		click(InsuranceScoreManagementPage.saveChanges,"save changes");

		new CustomersTABnavigationsLib(driver, reporter).NavigateTosummary();
		new CustomerSummaryPageLib(driver, reporter).selectQuote();
		new QuotesTABnavigationsLib(driver, reporter).navigateToQuoteDetails();			
		new QuoteDetailsPageLib(driver, reporter).navigateToCustomer();
		new CustomersTABnavigationsLib(driver, reporter).NavigateToInsuranceScoreManagement();
		String scoreApplied = getText(InsuranceScoreManagementPage.scoreCurentlyApplied, "Score currently applied");

		if(numOfScoresAdded<3 && !scoreApplied.equals(insuranceScore)){
			addNewInsuranceScore(data);
			numOfScoresAdded++;
		}else{
			new CustomersTABnavigationsLib(driver, reporter).NavigateTosummary();
			new CustomerSummaryPageLib(driver, reporter).selectQuote();
			new QuotesTABnavigationsLib(driver, reporter).navigateToQuoteDetails();
		}
	}
	public void newReport() throws Throwable{
		click(InsuranceScoreManagementPage.newReport, "new report");
	}
	public void exitCustomer(){
	}
	
}
