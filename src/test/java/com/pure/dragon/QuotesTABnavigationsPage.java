package com.pure.dragon;

import org.openqa.selenium.By;

public class QuotesTABnavigationsPage {

	static By summary,
	quoteDetails,underwritingReferrals,subjectivities,documents,
	diary,memberFlag;
	
	static{
		summary = By.cssSelector("a[href*='Action.479905']");
		quoteDetails = By.cssSelector("a[href*='Action.479805']");
		underwritingReferrals = By.cssSelector("a[href*='Action.488105']");
		subjectivities = By.cssSelector("a[href*='Action.769433']");
		documents = By.cssSelector("a[href*='Action.494106']");
		diary = By.cssSelector("a[href*='Action.504006']");
		memberFlag = By.cssSelector("a[href*='Action.659733']");
		
		
		
	}
}
