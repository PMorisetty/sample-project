package com.pure.dragon.personalAuto;

import org.openqa.selenium.By;

public class AutomobilePolicyPage {
	static By termDropDown, effectiveDateText;
	static By pureHomeownersPolicyCheckBox, pureExcessPolicyCheckBox;
	static By priorCarrierDropDown, premiumText, lapseInCoverageDropdown, forDropDown;

	static{
		termDropDown = By.cssSelector("select[title='Term']");
		effectiveDateText = By.cssSelector("input[title='Effective Date']");
		pureHomeownersPolicyCheckBox = By.cssSelector("input[id$='_29294207']");
		pureExcessPolicyCheckBox = By.cssSelector("input[id$='_29294307']");
		priorCarrierDropDown = By.cssSelector("select[id$='_28921105']");
		premiumText = By.cssSelector("input[id$='28921305']");
		lapseInCoverageDropdown = By.cssSelector("select[id$='_28921205']");
		forDropDown = By.cssSelector("select[id$='_28921405']");
	}
}
