package com.pure.dragon.personalAuto;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class DriverDetailPageLib extends ActionEngine{
	public DriverDetailPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void selectGender(String gender) throws Throwable {
		if(gender!=null && !gender.equalsIgnoreCase("Null") && !gender.equalsIgnoreCase(""))
			if(gender.equalsIgnoreCase("M"))
				selectByVisibleText(DriverDetailPage.gender, "Male", "Gender");
			else
				selectByVisibleText(DriverDetailPage.gender, "Female", "Gender");
	}

	public void selectMaritalStatus(String maritalStatus) throws Throwable {
		if(maritalStatus!=null && !maritalStatus.equalsIgnoreCase("Null") && !maritalStatus.equalsIgnoreCase(""))
			selectByVisibleText(DriverDetailPage.maritalStatus, maritalStatus, "Marital Status");
	}

	public void selectRelationToInsured(String relation) throws Throwable {
		if(relation!=null && !relation.equalsIgnoreCase(""))
			selectByVisibleText(DriverDetailPage.relationToInsured, relation, "Relation to Insured");
		else
			selectByIndex(DriverDetailPage.relationToInsured, 1, "Relation to Insured");//set to 1 for UAT
	}

	public void selectLicenseStatus(String licenseStatus) throws Throwable {
		if(licenseStatus!=null && !licenseStatus.equalsIgnoreCase("Null") && !licenseStatus.equalsIgnoreCase(""))
			selectByVisibleText(DriverDetailPage.licenseStatus, licenseStatus, "LicensStatus");
		waitUntilJQueryReady();
	}

	public void selectHowLongLicensedInUS(String howlonglicensedInUS) throws Throwable {
		waitUntilJQueryReady();
		if(howlonglicensedInUS!=null && !howlonglicensedInUS.equalsIgnoreCase("Null") && !howlonglicensedInUS.equalsIgnoreCase("") && isVisibleOnly(DriverDetailPage.licensedInUS, "How Long Licensed In US"))
			selectByVisibleText(DriverDetailPage.licensedInUS, howlonglicensedInUS, "How Long Licensed In US");
	}

	public void accidentPreventionCourse(String dob, String accidentPreventionCourse, String preventionCourseDate)
			throws Throwable {
		int yyyy = Integer.valueOf(dob.split("/")[2]);
		int currentYear = new Date().getYear() + 1900;
		if((currentYear-yyyy)>=55){
			if (accidentPreventionCourse.equalsIgnoreCase("Yes")) {
				click(DriverDetailPage.accidentPreventionCourseYes,
						"accidentPreventionCourse Yes");
				type(DriverDetailPage.preventionCourseDate, preventionCourseDate, "PreventionCourseDate");
			} else {
				click(DriverDetailPage.accidentPreventionCourseNo,
						"accidentPreventionCourse NO");
			}
		}
	}

	public void driverTrainingCourse(String dob, String driverTrainingCourse, String driverTrainingDate)
			throws Throwable {		
		int yyyy = Integer.valueOf(dob.split("/")[2]);
		int currentYear = new Date().getYear() + 1900;
		if((currentYear-yyyy)<=18){
			if(isVisibleOnly(DriverDetailPage.driverTrainingYes, "Driving trainig")){
				if (driverTrainingCourse.equalsIgnoreCase("Yes")) {
					click(DriverDetailPage.driverTrainingYes,
							"Driver Training - Yes");
					type(DriverDetailPage.preventionCourseDate, driverTrainingDate, "Driver Training Date");
				} else {
					click(DriverDetailPage.driverTrainingNo,
							"Driver Training - NO");
				}
			}
		}

	}
	public void driverImprovementCourse(String driverTrainingCourse, String driverImprovementCourseDate)
			throws Throwable {		
		if(isVisibleOnly(DriverDetailPage.driverImprovementCourseYes, "Driving improvement course")){
			if (driverTrainingCourse.equalsIgnoreCase("Yes")) {
				click(DriverDetailPage.driverImprovementCourseYes,
						"Driver Training - Yes");
				type(DriverDetailPage.improvementCourseDate, driverImprovementCourseDate, "Driver Training Date");
			} else {
				click(DriverDetailPage.driverImprovementCourseNo,
						"Driver Training - NO");
			}	
		}else if(isVisibleOnly(DriverDetailPage.driverTrainigCourseCompletedIn5YrsYes, "Driver Training course in 5 years")){
			if (driverTrainingCourse.equalsIgnoreCase("Yes")) {
				click(DriverDetailPage.driverTrainigCourseCompletedIn5YrsYes,
						"Driver Training course in 5 years - Yes");
				type(DriverDetailPage.trainingCourseDate, driverImprovementCourseDate, "Driver Training Date");
			} else {
				click(DriverDetailPage.driverTrainigCourseCompletedIn5YrsNo,
						"Driver Training course in 5 years - NO");
			}
		}
	}

	private void driverWithDiffState(String driverWithDiffStateValue) throws Throwable {
		if (driverWithDiffStateValue.equalsIgnoreCase("Yes")) {
			click(DriverDetailPage.driverWithDiffStateYes,
					"Is driver assigned to another vehicle insured under a separate PURE policy in a different state? Yes");
					} else{
			click(DriverDetailPage.driverWithDiffStateNo,
					"Is driver assigned to another vehicle insured under a separate PURE policy in a different state? NO");
		}
	}
	
	private void activeMilitaryPersonnal(String militarypersonnal) throws Throwable {
		if (militarypersonnal.equalsIgnoreCase("Yes")) {
			click(DriverDetailPage.militaryPersonnalYes,
					"Active military personnel? Yes");
					} else{
			click(DriverDetailPage.militaryPersonnalNo,
					"Active military personnel? No");
		}
	}
	
	private void driverSpend3orMoremonths(String driverSpend3orMoreMonthsValue, String driverTookListedAutosValue)
			throws Throwable {
		if (driverSpend3orMoreMonthsValue.equalsIgnoreCase("Yes")) {
			click(DriverDetailPage.driverSpend3orMoremonthsYes,
					"Does this driver spend 3 or more months out of state each year? Yes");
			driverTookListedAutos(driverTookListedAutosValue);
		} else {
			click(DriverDetailPage.driverSpend3orMoremonthsNo,
					"Does this driver spend 3 or more months out of state each year? NO");
		}
	}
	

	private void driverTookListedAutos(String driverTookListedAutosValue) throws Throwable {
		if (driverTookListedAutosValue.equalsIgnoreCase("Yes")) {
			click(DriverDetailPage.driverTookListedAutosYes,
					"Does this driver take any autos listed on the policy with him/ her? Yes");
		} else {
			click(DriverDetailPage.driverTookListedAutosNo,
					"Does this driver take any autos listed on the policy with him/ her? NO");
		}
	}

	public void vehicleWithStudentAtSchool(String licenseStatus, String value) throws Throwable {
		if(licenseStatus.contains("Away at school")){
			if (value != null && value.equalsIgnoreCase("Yes")) {
				click(DriverDetailPage.vehicleWithStudentAtSchoolYes, "Vehicle with Student at School? Yes");
			} else {
				click(DriverDetailPage.vehicleWithStudentAtSchoolNo, "Vehicle with Student at School? NO");
			}
		}
	}

	public void selectFullTimeStudent(String dob, String fullTimeStudent, String betterGrade) throws Throwable {
		int yyyy = Integer.valueOf(dob.split("/")[2]);
		int currentYear = new Date().getYear() + 1900;
		if((currentYear-yyyy)<=25){
			if (fullTimeStudent != null && fullTimeStudent.equalsIgnoreCase("Yes")) {
				click(DriverDetailPage.fullTimeStudentYes, "Full-Time Student? Yes");
				selectBetterGrade(betterGrade);
			} else {
				click(DriverDetailPage.fullTimeStudentNo, "Full-Time Student? NO");
			}
		}
	}

	public void selectBetterGrade(String betterGrade) throws Throwable {
		if (betterGrade != null && betterGrade.equalsIgnoreCase("Yes")) {
			click(DriverDetailPage.gradeAverageYes, "Better Grade? Yes");
		} else {
			click(DriverDetailPage.gradeAverageNo, "Better Grade? NO");
		}
	}



	public void licensedInOtherState(String licensedInOtherState, String priorLicensedState, String priorDriverLicenseNumber)
			throws Throwable {
		if (licensedInOtherState.equalsIgnoreCase("Yes")) {
			click(DriverDetailPage.licensedInOtherStateYes,
					"licensedInOtherStateYes Yes");
			if(priorLicensedState != null)
				selectByVisibleText(DriverDetailPage.priorLicensedState, priorLicensedState, "PriorLicensedState");
			if(priorDriverLicenseNumber != null)
				type(DriverDetailPage.priorDriverLicenseNumber, priorDriverLicenseNumber, "PriorDriverLicenseNumber");
		} else {
			click(DriverDetailPage.licensedInOtherStateNo,
					"licensedInOtherStateYes NO");
		}

	}

	public void selectdriverLicenseState(String driverLicenseState) throws Throwable {
		if(driverLicenseState!=null && !driverLicenseState.equalsIgnoreCase("Null") && !driverLicenseState.equalsIgnoreCase(""))
			selectByVisibleText(DriverDetailPage.driverLicenseState, driverLicenseState, "DriverLicenseState");
	}

	public void setLicenseNumber(String licenseNumber) throws Throwable {
		if(licenseNumber!=null && !licenseNumber.equalsIgnoreCase("Null") && !licenseNumber.equalsIgnoreCase(""))
			type(DriverDetailPage.licenseNumber, licenseNumber, "LicenseNumber");
	}

	public void setOccupation(String occupation) throws Throwable {
		if(occupation!=null && !occupation.equalsIgnoreCase("Null") && !occupation.equalsIgnoreCase(""))
			type(DriverDetailPage.occupation, occupation, "Occupation");
	}

	public void setFirstYearLicensedInUS(String firstYearLicensedInUS) throws Throwable {
		type(DriverDetailPage.firstYearLicensedInUS, firstYearLicensedInUS, "FirstYearLicensedInUS");
	}

	public void driverLicenseSuspendedRevoked(String driverLicenseSuspendedRevoked)
			throws Throwable {
		if (driverLicenseSuspendedRevoked.equalsIgnoreCase("Yes")) {
			click(DriverDetailPage.driverLicenseSuspendedRevokedYes,
					"driverLicenseSuspendedRevoked Yes");
		} else {
			click(DriverDetailPage.driverLicenseSuspendedRevokedNo,
					"driverLicenseSuspendedRevoked NO");
		}

	}

	public void driverLicenseLapsed(String driverLicenseLapsed)
			throws Throwable {
		if (driverLicenseLapsed.equalsIgnoreCase("Yes")) {
			click(DriverDetailPage.driverLicenseLapsedYes,
					"driverLicenseLapsed Yes");
		} else {
			click(DriverDetailPage.driverLicenseLapsedNo,
					"driverLicenseLapsed NO");
		}

	}

	public void matureDriverTrainingCourseCompleted(String drivertrainingcoursecompleted)
			throws Throwable {
		//Don't use isVisibleOnly as there is some logic for the appearance of this field, yet to analyze.
		if (drivertrainingcoursecompleted.equalsIgnoreCase("No")) {
			click(DriverDetailPage.maturedDriverCourseCompletedNo,
					"drivertrainingcoursecompleted No");
		} else {
			click(DriverDetailPage.maturedDriverCourseCompletedYes,
					"drivertrainingcoursecompleted Yes");
		}

	}

	private void defensiveDriverTrainingCourseCompleted(String defensivedrivertrainingcoursecompleted) throws Throwable {
		//Don't use isVisibleOnly as there is some logic for the appearance of this field, yet to analyze.
		if (defensivedrivertrainingcoursecompleted.equalsIgnoreCase("No")) {
			click(DriverDetailPage.defensiveDriverCourseCompletedNo,
					"defensivedrivertrainingcoursecompleted No");
		} else {
			click(DriverDetailPage.defensiveDriverCourseCompletedYes,
					"defensivedrivertrainingcoursecompleted Yes");
		}
	}

	public void SR22CertificateInsurance(String SR22CertificateInsurance)
			throws Throwable {
		if (SR22CertificateInsurance.equalsIgnoreCase("Yes")) {
			click(DriverDetailPage.SR22CertificateInsuranceYes,
					"SR22CertificateInsurance Yes");
		} else {
			click(DriverDetailPage.SR22CertificateInsuranceNo,
					"SR22CertificateInsurance NO");
		}

	}

	public void fillDriverDetails(Hashtable<String, String> data, int count) throws Throwable {
		this.selectGender(data.get("Gender_Cd_" + count));
		this.selectRelationToInsured(data.get("Relation_To_Insured_" + count));
		this.selectMaritalStatus(data.get("Marital_Status_Name_" + count));
		this.selectLicenseStatus(data.get("Driver_License_Status_Name_" + count));
		switch (data.get("state")) {							
		case "TX":
			//effective date is considered as driver training effective date
			this.driverTrainingCourse(data.get("insuredDOB_" + count), data.get("Driver_Training_Course_Ind_" + count), data.get("effectiveDate"));
			//*
		case "FL":
			this.selectHowLongLicensedInUS(data.get("Driver_Years_Licensed_Name_" + count));
			this.SR22CertificateInsurance(data.get("SR22CertificateInsurance"));
			break;
		case "SC":
		case "CT":
			this.driverSpend3orMoremonths(data.get("Three_Months_Out_of_State_Ind_" + count), data.get("Take_Autos_Out_of_State_Ind_" + count));
			this.selectHowLongLicensedInUS(data.get("Driver_Years_Licensed_Name_" + count));
			this.SR22CertificateInsurance(data.get("SR22CertificateInsurance"));
			break;
		case "NY":			
			this.accidentPreventionCourse(data.get("insuredDOB_" + count), data.get("Accident_Prevention_Course_Ind_" + count), data.get("Accident_Prevention_Course_Date_" + count));
			this.selectHowLongLicensedInUS(data.get("Driver_Years_Licensed_Name_" + count));
			break;
		case "IL":
			this.matureDriverTrainingCourseCompleted(data.get("matureDriverTrainingCourseCompleted"));
		case "AL":
		case "CO":
			this.selectHowLongLicensedInUS(data.get("Driver_Years_Licensed_Name_" + count));
			break;
		case "NJ":
			this.selectHowLongLicensedInUS(data.get("Driver_Years_Licensed_Name_" + count));
			this.defensiveDriverTrainingCourseCompleted(data.get("defensiveDriverTrainingCourseCompleted"));
            break;
		case "CA":
			this.setFirstYearLicensedInUS(data.get("firstYearLicensedInUS"));
			this.driverLicenseSuspendedRevoked(data.get("driverLicenseSuspendedRevoked"));
			this.driverLicenseLapsed(data.get("driverLicenseLapsed"));
			this.matureDriverTrainingCourseCompleted(data.get("matureDriverTrainingCourseCompleted"));			
			break;		
		case "MA":
		case "NC":
			this.setFirstYearLicensedInUS(data.get("firstYearLicensedInUS_"+ count));
		case "VA":
			this.driverWithDiffState(data.get("Driver In Diff State_"+ count));
		case "WA":
		case "OK":
			this.accidentPreventionCourse(data.get("insuredDOB_" + count), data.get("Accident_Prevention_Course_Ind_" + count), data.get("Accident_Prevention_Course_Date_" + count));
		case "PA":
			//this.accidentPreventionCourse(data.get("insuredDOB_" + count), data.get("Accident_Prevention_Course_Ind_" + count), data.get("Accident_Prevention_Course_Date_" + count));
			this.selectHowLongLicensedInUS(data.get("Driver_Years_Licensed_Name_" + count));
			this.driverSpend3orMoremonths(data.get("Three_Months_Out_of_State_Ind_" + count), data.get("Take_Autos_Out_of_State_Ind_" + count));
			//effective date is considered as driver training effective date
			this.driverImprovementCourse(data.get("Driver_Training_Course_Ind_" + count), data.get("Effective_Date_" + count));
			break;
		case "LA":
			//this.accidentPreventionCourse(data.get("insuredDOB_" + count), data.get("Accident_Prevention_Course_Ind_" + count), data.get("Accident_Prevention_Course_Date_" + count));
			this.selectHowLongLicensedInUS(data.get("Driver_Years_Licensed_Name_" + count));
			this.driverSpend3orMoremonths(data.get("Three_Months_Out_of_State_Ind_" + count), data.get("Take_Autos_Out_of_State_Ind_" + count));
			this.activeMilitaryPersonnal(data.get("Active Military Personnal_" + count));
			break;
		}
		this.vehicleWithStudentAtSchool(data.get("Driver_Status_Name_" + count), data.get("driverLicenseLapsed"));
		this.selectFullTimeStudent(data.get("insuredDOB_" + count), data.get("Good_Student_Ind_" + count), data.get("Driver_Better_Grade_" + count));
		this.licensedInOtherState(data.get("Driver_Licensed_In_Another_State_Ind_" + count), data.get("priorLicensedState"), data.get("priorDriverLicenseNumber"));
		this.selectdriverLicenseState(data.get("Driver_License_State_Cd_" + count));
		this.setLicenseNumber(data.get("Driver_License_" + count) + count);
		this.setOccupation(data.get("Driver_Occupation_" + count));
		addIncidents(data, count);
	}

	private void clickAddIncident() throws Throwable {
		click(DriverDetailPage.incidentAddButton, "incidentAddButton");
	}

	private void selectSource(String source) throws Throwable {
		if(source!=null && !source.equalsIgnoreCase("Null") && !source.equalsIgnoreCase(""))
			selectByVisibleText(DriverDetailPage.source, source, "Source");
	}

	private void selectAccidentOrViolation(String accidentOrViolation) throws Throwable {
		if(accidentOrViolation!=null && !accidentOrViolation.equalsIgnoreCase("Null") && !accidentOrViolation.equalsIgnoreCase(""))
			selectByVisibleText(DriverDetailPage.accidentOrViolation, accidentOrViolation, "AccidentOrViolation");
	}

	private void setAccOrConvictionDate(String accOrConvictionDate) throws Throwable {
		if(accOrConvictionDate!=null && !accOrConvictionDate.equalsIgnoreCase("Null") && !accOrConvictionDate.equalsIgnoreCase(""))
			type(DriverDetailPage.accOrConvictionDate, accOrConvictionDate + Keys.TAB, "AccOrConvictionDate");
	}

	public void addIncidents(Hashtable<String, String> data, int count) throws Throwable {
		String aaf60Months = data.get("Incidents_AAF_60_Month_No_" + count);
		if(!aaf60Months.equalsIgnoreCase("Null")){
			int aaf60MonthsValue = (aaf60Months!=null)?Integer.valueOf(aaf60Months):0;
			String aaf36Months = data.get("Incidents_AAF_36_Month_No_" + count);
			int aaf36MonthsValue = (aaf36Months!=null && !aaf36Months.equalsIgnoreCase("Null"))?Integer.valueOf(aaf36Months):0;
			for(int counter = 1; counter <= aaf60MonthsValue; counter++){
				if(counter <= aaf36MonthsValue)
					fillAAFIncident36Months();
				else
					fillAAFIncident60Months();
			}
		}
		String maj36Months = data.get("Incidents_MAJ_36_Month_No_" + count);
		int maj36MonthsValue = (maj36Months!=null)?Integer.valueOf(maj36Months):0;
		for(int counter = 1; counter <= maj36MonthsValue; counter++){
			fillMAJIncident36Months();
		}
		String dui36Months = data.get("Incidents_MAJ_36_Month_No_" + count);
		int dui36MonthsValue = (dui36Months!=null)?Integer.valueOf(dui36Months):0;
		for(int counter = 1; counter <= dui36MonthsValue; counter++){
			fillDUI36Months();
		}

		String min18Months = data.get("Incidents_AAF_60_Month_No_" + count);
		int min18MonthsValue = (min18Months!=null)?Integer.valueOf(min18Months):0;
		String min36Months = data.get("Incidents_AAF_36_Month_No_" + count);
		int min36MonthsValue = (min36Months!=null)?Integer.valueOf(min36Months):0;
		for(int counter = 1; counter <= min36MonthsValue; counter++){
			if(counter <= min18MonthsValue)
				fillMINIncident18Months();
			else
				fillMINIncident18To36Months();
		}

		String naf36Months = data.get("Incidents_AAF_60_Month_No_" + count);
		int naf36MonthsValue = (naf36Months!=null)?Integer.valueOf(naf36Months):0;
		String naf60Months = data.get("Incidents_AAF_36_Month_No_" + count);
		int naf60MonthsValue = (naf60Months!=null)?Integer.valueOf(naf60Months):0;
		for(int counter = 1; counter <= naf60MonthsValue; counter++){
			if(counter <= naf36MonthsValue)
				fillNAF36Months();
			else
				fillNAF60Months();
		}
	}
	private void fillIncidentDetails(String source, String accidentName, String accidentDate) throws Throwable{
		selectSource(source);
		selectAccidentOrViolation(accidentName);
		setAccOrConvictionDate(accidentDate);
	}
	private String getDate(int value){
		Calendar c1 = Calendar.getInstance();
		c1.add(Calendar.MONTH, value);
		return c1.get(Calendar.MONTH) + "/"+ c1.get(Calendar.DATE) + "/" + c1.get(Calendar.YEAR);
	}
	private void fillAAFIncident60Months()throws Throwable{
		clickAddIncident();
		fillIncidentDetails("PURE", "At-fault accident", getDate(-40));
	}
	private void fillAAFIncident36Months()throws Throwable{
		clickAddIncident();
		fillIncidentDetails("PURE", "At-fault accident", getDate(-20));
	}
	private void fillDUI36Months()throws Throwable{
		clickAddIncident();
		fillIncidentDetails("PURE", "Driving Under the Influence", getDate(-20));
	}
	private void fillMovingVoliation()throws Throwable{

	}
	private void fillMAJIncident36Months()throws Throwable{
		clickAddIncident();
		fillIncidentDetails("PURE", "Drag Racing", getDate(-20));
	}
	private void fillMINIncident18Months()throws Throwable{
		clickAddIncident();
		fillIncidentDetails("PURE", "Improper backing", getDate(-10));
	}
	private void fillMINIncident18To36Months()throws Throwable{
		clickAddIncident();
		fillIncidentDetails("PURE", "Improper backing", getDate(-20));
	}
	private void fillNAF60Months()throws Throwable{
		clickAddIncident();
		fillIncidentDetails("PURE", "Not-at-fault accident", getDate(-40));
	}
	private void fillNAF36Months()throws Throwable{
		clickAddIncident();
		fillIncidentDetails("PURE", "Not-at-fault accident", getDate(-20));
	}
	private void fillSurchargePoints()throws Throwable{

	}
	private void fillIncidentsWaived36Months()throws Throwable{

	}
	private void fillIncidentsWaived18Months()throws Throwable{

	}
}