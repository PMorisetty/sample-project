package com.pure.dragon.personalAuto;

import org.openqa.selenium.By;

public class OperatorsAndVehiclesPage {
	static By additionalVehiclesAddBtn, year, make, model, shortVIN, VIN, yearPP, makePP, modelPP;
	static By addDriver, addVehicle;
	//driver details
	static By prefixDropdown, firstNameText, middleNameText, lastNameText, suffixDropdown, dobText, driverStatusDropdown, nonDriverReasonDropdown, deleteButton;
	//vehicle details
	static By vehicleType, vehicleVIN, vehicleTag, vehiclesLocator, vinOverrideCheckbox;
	static By numberOfVehiclesAdded,getVehicleType,getYear,getMake,getModel,getVIN,getBodyType;
	static By errorMessage;
	//Online Vehicles
	static By vehiclesObtainedThroughOnline;
	
	static{
		additionalVehiclesAddBtn = By.xpath("//div[text()='additional vehicles']/following-sibling::div//a");
		//------------------
		//For Private Passenger vehicle type
		//year, make, model are auto-populated after entering VIN number and these are dropdowns.
		//For remaining vehicle types, year, make, and model are input fields and need to enter the data.
		year = By.xpath("//div[@class='headerLabel'][contains(text(),'$')]/../..//input[@title='Year'][@style='display: inline;']");
		make = By.xpath("//div[@class='headerLabel'][contains(text(),'$')]/../..//input[@title='Make'][@style='display: inline;']");
		model = By.xpath("//div[@class='headerLabel'][contains(text(),'$')]/../..//input[@title='Model'][@style='display: inline;']");
		
		yearPP = By.xpath("//div[@class='headerLabel'][contains(text(),'$')]/../..//input[@title='Year'][@class='unFilledMandatoryField']");
		makePP = By.xpath("//div[@class='headerLabel'][contains(text(),'$')]/../..//input[@title='Make'][@class='unFilledMandatoryField']");
		modelPP = By.xpath("//div[@class='headerLabel'][contains(text(),'$')]/../..//input[@title='Model'][@class='unFilledMandatoryField']");
		
		
		shortVIN = By.xpath("//select[@title='Short VIN']");
		VIN = By.xpath("//div[@class='labelsTop labelMandatory']/following-sibling::div/input[@value='']");
		addDriver = By.xpath("//div[@class='headerLabel'][text() = 'additional operators']/..//a[contains(@onclick,'Action.189')]");
		addVehicle = By.xpath("//div[@class='headerLabel'][text() = 'additional vehicles']/..//a[contains(@onclick,'Action.189')]");
		
		prefixDropdown = By.xpath("//div[text()='additional operators']/../..//tr[@style][$]//select[@title='Prefix']");
		firstNameText = By.xpath("//div[text()='additional operators']/../..//tr[@style][$]//input[@title='First Name']");
		middleNameText = By.xpath("//div[text()='additional operators']/../..//tr[@style][$]//input[@title='MI']");
		lastNameText = By.xpath("//div[text()='additional operators']/../..//tr[@style][$]//input[@title='Last Name']");
		suffixDropdown = By.xpath("//div[text()='additional operators']/../..//tr[@style][$]//select[@title='Suffix']");
		dobText = By.xpath("//div[text()='additional operators']/../..//tr[@style][$]//input[@title='Date of Birth']");
		driverStatusDropdown = By.xpath("//div[text()='additional operators']/../..//tr[@style][$]//select[@title='Driver Status']");

		nonDriverReasonDropdown = By.xpath("//div[text()='additional operators']/../..//tr[@style][$]//select[@title='Non-Driver Reason']");
		deleteButton = By.xpath("//div[text()='additional operators']/../..//tr[@style][$]//a[@title='Delete the selected item.']");
		
		//vehicle details
		vehicleType = By.xpath("//div[@class='headerLabel'][text()='$']/../..//select[@title='Vehicle Type']");
		vehicleVIN = By.xpath("//div[@class='headerLabel'][text()='$']/../..//input[@title='VIN'][@class='unFilledMandatoryField']");
		vehicleTag = By.xpath("//div[@class='headerLabel'][text()='additional vehicles']/../..//tr[@style][$]//div[@class='headerLabel']");
		vehiclesLocator = By.xpath("//div[@class='headerLabel'][text()='additional vehicles']/../..//tr[@style]");
		vinOverrideCheckbox = By.xpath("//div[@class='headerLabel'][contains(text(),'$')]/../..//input[@type='checkbox'][@title='VIN Override']");
		numberOfVehiclesAdded = By.xpath("//div[text()='vehicles obtained through online reports']/../..//select[@title='Include the vehicle on the policy']");
		
		//Below are for auto-populate validations: from num of vehicle listed, pass $ int value to get particular vehicle listed
		getVehicleType = By.xpath("(//select[@title='Vehicle Type'])[$]");
		getYear = By.xpath("(//input[@title='Year'])[$]");
		getMake = By.xpath("(//input[@title='Make'])[$]");
		getModel = By.xpath("(//input[@title='Model'])[$]");
		getVIN = By.xpath("(//input[@title='VIN'])[$]");
		getBodyType = By.xpath("(//input[@title='Body-Type'])[$]");
		errorMessage=By.xpath("//ul[@id='messages']/li");
		
		//Online Vehicles
		vehiclesObtainedThroughOnline = By.xpath("//div[@class='headerLabel'][text()='vehicles obtained through online reports']/../..//table[@class='objectListTable']/tbody/tr[@style]");
		
	}
	
	
	//***************Excess Liability*******************
	static By licenseState, licenseNumber, operatorDOB, accidentOrViolationSource,accidentOrViolation, accOrConvictionDate, addIncidentBtn ;
	

	static{
	licenseState = By.cssSelector("select[title='License State']");
	licenseNumber = By.cssSelector("input[title='License Number']");
	operatorDOB  = By.cssSelector("input[title='Date of Birth']");
	accidentOrViolationSource = By.cssSelector("select[title='Accident/ Violation Source']");
	accidentOrViolation = By.cssSelector("select[title='Accident or Violation (within last 5 years)']");
	accOrConvictionDate = By.cssSelector("input[title='Acc. or Conviction Date']");
	addIncidentBtn = By.xpath("//div[contains(text(),'incidents')]/..//a");
	}
	
}
