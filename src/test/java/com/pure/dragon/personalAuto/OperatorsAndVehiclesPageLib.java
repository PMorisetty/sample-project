package com.pure.dragon.personalAuto;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.apache.tools.ant.taskdefs.Exit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.report.CReporter;

public class OperatorsAndVehiclesPageLib extends ActionEngine{
	public OperatorsAndVehiclesPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void selectPrefixDropdown(String value, int... count) throws Throwable{
		int num = count.length >0? count[0]:1;
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.prefixDropdown, String.valueOf(num));
		selectByVisibleText(By.xpath(tempLocator), value,"Prefix");
	}
	public void setFirstNameText(String value, int... count) throws Throwable{
		int num = count.length >0? count[0]:1;
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.firstNameText, String.valueOf(num));
		type(By.xpath(tempLocator), value,"First Name");
	}
	public void setMiddleNameText(String value, int... count) throws Throwable{
		int num = count.length >0? count[0]:1;
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.middleNameText, String.valueOf(num));
		type(By.xpath(tempLocator), value,"Middle Name");
	}
	public void setLastNameText(String value, int... count) throws Throwable{
		int num = count.length >0? count[0]:1;
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.lastNameText, String.valueOf(num));
		type(By.xpath(tempLocator), value,"Last Name");
	}
	public void selectSuffixDropdown(String value, int... count) throws Throwable{
		int num = count.length >0? count[0]:1;
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.suffixDropdown, String.valueOf(num));
		selectByVisibleText(By.xpath(tempLocator), value,"Suffix");
	}
	public void setDOBText(String value, int... count) throws Throwable{
		int num = count.length >0? count[0]:1;
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.dobText, String.valueOf(num));
		type(By.xpath(tempLocator), value,"DOB");
	}
	public void selectDriverStatus(String driverStatus, int... count) throws Throwable {
		int num = count.length >0? count[0]:1;
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.driverStatusDropdown, String.valueOf(num));
		selectByVisibleText(By.xpath(tempLocator), driverStatus,"Driver Status");
	}

	public void selectNonDriverReasonDropdown(String reason, int... count) throws Throwable {
		int num = count.length >0? count[0]:1;
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.nonDriverReasonDropdown, String.valueOf(num));
		selectByVisibleText(By.xpath(tempLocator), reason,
				"Non-Driver Reason");
	}
	public void clickAddDriver() throws Throwable{
		click(OperatorsAndVehiclesPage.addDriver, "Add Driver");
	}
	public void clickAddVehicle() throws Throwable{
		click(OperatorsAndVehiclesPage.addVehicle, "Add Vehicle");
	}
	public boolean errorMessage() throws Throwable{
		if(isVisibleOnly(OperatorsAndVehiclesPage.errorMessage, "Error message")){
			reporter.SuccessReport("Error message thrown", getText(OperatorsAndVehiclesPage.errorMessage, "error message"));
			return true;
		}
		return false;
	}
	public int getNoOfOperatorsPresent(){
		By locator = By.xpath("//div[@class='headerLabel'][text()='additional operators']/../..//table[@class='objectListTable']/tbody/tr[@style]");
		int size = this.driver.findElements(locator).size();
		return size;
	}
	private int getNoOfOnlineVehicles(){
		int size = 0;
		try{
			size = this.driver.findElements(OperatorsAndVehiclesPage.vehiclesObtainedThroughOnline).size();		
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return size;
	}
	public String getVehicleTag() throws Throwable{
		int size = this.driver.findElements(OperatorsAndVehiclesPage.vehiclesLocator).size();
		String updatedLocator = xpathUpdator(OperatorsAndVehiclesPage.vehicleTag, String.valueOf(size));
		return getText(By.xpath(updatedLocator), "New Vehicle Tag");
	}
	public void addAdditionalOperators(Hashtable<String, String> data) throws Throwable{
		if(data.get("Drivers_Count") != null){
			int noOfOperators = Integer.valueOf(data.get("Drivers_Count"));
			for(int i = 1; i < noOfOperators;i++){
				int noOfOperatorsPresent= this.getNoOfOperatorsPresent();
				this.clickAddDriver();
				this.fillNewDriverDetails(noOfOperatorsPresent + 1, data);
				this.waitForMask();
			}
		}
	}
	public void fillNewDriverDetails(int count, Hashtable<String, String> data) throws Throwable{
		//		this.selectPrefixDropdown(data.get("operator" + count + "_prefix"), count);
		this.setFirstNameText(data.get("First_Name_Txt_" + count), count);
		//		this.setMiddleNameText(data.get("operator" + count + "_middleName"), count);
		this.setLastNameText(data.get("Last_Name_Txt_" + count), count);
		//		this.selectSuffixDropdown(data.get("operator" + count + "_suffix"), count);
		this.setDOBText(data.get("insuredDOB_" + count), count);
		this.selectDriverStatus(data.get("Driver_Status_Name_" + count), count);
		if("Non-Driver".equalsIgnoreCase(data.get("Driver_Status_Name_" + count))){
			this.selectNonDriverReasonDropdown(data.get("NonDriverReason_" + count), count);
		}
	}
	/*One vehicle is added by default*/
	public Hashtable<String, String> addVehicles(Hashtable<String, String> data) throws Throwable{
		if(data.get("Vehicles_Count")!= null){
			int noOfVehicles = Integer.valueOf(data.get("Vehicles_Count"));
			int noOfVehiclesAdded = 0;
			for(int iterator = 0; iterator < noOfVehicles;iterator++){
				if(noOfVehiclesAdded < noOfVehicles)this.clickAddVehicle();
				noOfVehiclesAdded++;
				String vehicleTag = this.getVehicleTag();
				this.fillNewVehicleDetails(vehicleTag, data, noOfVehiclesAdded);
				vehicleTag = this.getVehicleTag();
				data.put("VehicleName_" + noOfVehiclesAdded, vehicleTag);
			}
		}
		return data;
	}
	/*verify vehicle present in online vehicle reports, if not, add*/
	public Hashtable<String, String> verifyVehiclePresentInOnlineReport(Hashtable<String, String> data) throws Throwable{
		String vehicleTag = null;
		int noOfVehicles = Integer.valueOf(data.get("Vehicles_Count"));

		if(noOfVehicles == getNoOfOnlineVehicles()){
			return data;
		}
		if( getNoOfOnlineVehicles() > 0){
			List<WebElement> onlineVINs = getWebElementList(CoveragesPage.vehiclesObtainedThroughOnline,"VIN");
			for(int i = 1; i <= noOfVehicles;i++){
				vehicleTag = data.get("Auto_Vehicle_Year_No_" + i)+" "+data.get("Auto_Vehicle_Make_Txt_" + i)+" "+data.get("Auto_Vehicle_Model_Txt_" + i);
				data.put("VehicleName_" + i, vehicleTag);

				boolean vehicleispresent = false;
				for(WebElement vin : onlineVINs){
					String vehicleVIN =vin.getAttribute("value");
					if(vehicleVIN.equalsIgnoreCase(data.get("VIN_Txt_"+i))){
						vehicleispresent = true;
					}		
				}
				if(vehicleispresent == false){				
					addAdditionalVehicles(data,i);
				}
			}
		}
		else{
			for(int i=1;i <= noOfVehicles;i++){
				vehicleTag = data.get("Auto_Vehicle_Year_No_" + i)+" "+data.get("Auto_Vehicle_Make_Txt_" + i)+" "+data.get("Auto_Vehicle_Model_Txt_" + i);
				data.put("VehicleName_" + i, vehicleTag);
				addAdditionalVehicles(data,i);
			}
		}
		return data;
	}
	private void addAdditionalVehicles(Hashtable<String, String> data,int count) throws Throwable{
		String vehicleTag = null;
		this.clickAddVehicle();
		vehicleTag = this.getVehicleTag();
		this.fillNewVehicleDetails(vehicleTag, data, count);
	}


	public void fillNewVehicleDetails(String vehicleTag, Hashtable<String, String> data, int count) throws Throwable{
		selectVehicleType(data.get("Auto_Vehicle_Type_Name_" + count), vehicleTag);
		if(data.get("Auto_Vehicle_Type_Name_" + count).equalsIgnoreCase("Private Passenger")){
			setVIN(data.get("VIN_Txt_" + count), vehicleTag);
			new CommonPageLib(driver, reporter).clickSaveChangesBtn();
			vehicleTag = this.getVehicleTag();
			String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.vinOverrideCheckbox, vehicleTag);
			if(isCheckBoxSelected(By.xpath(tempLocator))){
				setYearPP(data.get("Auto_Vehicle_Year_No_" + count), vehicleTag);
				setMakePP(data.get("Auto_Vehicle_Make_Txt_" + count), vehicleTag);
				setModelPP(data.get("Auto_Vehicle_Model_Txt_" + count), vehicleTag);
				new CommonPageLib(driver, reporter).clickSaveChangesBtn();
			}
		}else{
			setYear(data.get("Auto_Vehicle_Year_No_" + count), vehicleTag);
			setMake(data.get("Auto_Vehicle_Make_Txt_" + count), vehicleTag);
			setModel(data.get("Auto_Vehicle_Model_Txt_" + count), vehicleTag);
			setVIN(data.get("VIN_Txt_" + count), vehicleTag);
			new CommonPageLib(driver, reporter).clickSaveChangesBtn();
		}
	}

	public void selectVehicleType(String value,String vehicleTag) throws Throwable{
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.vehicleType, vehicleTag);
		selectByVisibleText(By.xpath(tempLocator), value,"Vehicle Type");
	}
	public void setYearPP(String year, String vehicleTag) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.yearPP, vehicleTag);
		if(isVisibleOnly(By.xpath(tempLocator), "Year"))
			type(By.xpath(tempLocator), year, vehicleTag + " Year");
	}
	public void setMakePP(String make, String vehicleTag) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.makePP, vehicleTag);
		if(isVisibleOnly(By.xpath(tempLocator), "Make"))
			type(By.xpath(tempLocator), make, vehicleTag + " Make");
	}
	public void setModelPP(String model, String vehicleTag) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.modelPP, vehicleTag);
		if(isVisibleOnly(By.xpath(tempLocator), "Model"))
			type(By.xpath(tempLocator), model, vehicleTag + " Model");
	}
	public int getNumOfVehiclesAdded(){
		//Get the vehicles added/populated by Auto prefill
		List<WebElement> numOfVehiclesAdded = null;
		try {
			numOfVehiclesAdded = driver.findElements(OperatorsAndVehiclesPage.numberOfVehiclesAdded);
			return numOfVehiclesAdded.size();
		} catch (Exception e) {
			//do nothing
		}
		return 0;
	}
	public boolean verifyVehiclesAdded(Hashtable<String, String> data) throws Throwable{
		int count=getNumOfVehiclesAdded();
		for(int i=1;i<=count;i++){
			assertTextStringMatching(data.get("Auto_Vehicle_Year_No_"+i), getYearPP(i));
			assertTextStringMatching(data.get("Auto_Vehicle_Make_Txt_"+i), getMakePP(i));
			assertTextStringMatching(data.get("Auto_Vehicle_Model_Txt_"+i), getModelPP(i));
			assertTextStringMatching(data.get("Auto_Vehicle_Year_No_"+i), getYearPP(i));
		}
		return false;
	}
	public String getYearPP(int count) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.getYear, count+"");
		if(isVisibleOnly(By.xpath(tempLocator), "Year")){			
			return getAttributeByValue(By.xpath(tempLocator)," Year");
		}return "";
	}
	public String getMakePP(int count) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.getMake, count+"");
		if(isVisibleOnly(By.xpath(tempLocator), "Make")){			
			return getAttributeByValue(By.xpath(tempLocator)," Make");
		}return "";
	}
	public String getModelPP(int count) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.getModel, count+"");
		if(isVisibleOnly(By.xpath(tempLocator), "Model")){			
			return getAttributeByValue(By.xpath(tempLocator)," Model");
		}return "";
	}
	public String getVIN(int count) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.getVIN, count+"");
		if(isVisibleOnly(By.xpath(tempLocator), "VIN")){			
			return getAttributeByValue(By.xpath(tempLocator)," VIN");
		}return "";
	}
	public String getBodyType(int count) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.getBodyType, count+"");
		if(isVisibleOnly(By.xpath(tempLocator), "VIN")){			
			return getAttributeByValue(By.xpath(tempLocator)," VIN");
		}return "";
	}
	public void setYear(String year, String vehicleTag) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.year, vehicleTag);
		type(By.xpath(tempLocator), year, vehicleTag + " Year");
	}
	public void setMake(String make, String vehicleTag) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.make, vehicleTag);
		type(By.xpath(tempLocator), make, vehicleTag + " Make");
	}
	public void setModel(String model, String vehicleTag) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.model, vehicleTag);
		type(By.xpath(tempLocator), model, vehicleTag + " Model");
	}
	public void setVIN(String VIN, String vehicleTag) throws Throwable {
		String tempLocator = xpathUpdator(OperatorsAndVehiclesPage.vehicleVIN, vehicleTag);
		type(By.xpath(tempLocator), VIN, vehicleTag + " VIN");
		type(By.xpath(tempLocator), Keys.TAB, "TAB Key");
		waitForMask();
	}

	//****Excess Liability****
	private void selectLicenseState(String licensedState) throws Throwable{
		selectByVisibleText(OperatorsAndVehiclesPage.licenseState, licensedState, "Licensed State");
	}
	private void enterLicenseNumber(String licenseNumber) throws Throwable{
		type(OperatorsAndVehiclesPage.licenseNumber, licenseNumber, "License Number");
		type(OperatorsAndVehiclesPage.licenseNumber, Keys.TAB, "Tab");
	}
	private void enterOperatorDOB(String operatorDOB) throws Throwable{
		type(OperatorsAndVehiclesPage.operatorDOB, operatorDOB, "DOB");
		type(OperatorsAndVehiclesPage.operatorDOB, Keys.TAB, " Tab");
	}
	private void clickAddIncidentButton() throws Throwable{
		click(OperatorsAndVehiclesPage.addIncidentBtn, "Add Incident");
	}
	private void enterAccidentOrViolationSource(String accidentOrViolationSource) throws Throwable{
		selectByVisibleText(OperatorsAndVehiclesPage.accidentOrViolationSource, accidentOrViolationSource, "Accident or violation source");
	}
	private void enterAccidentOrViolationWithin5Years(String accidentOrViolation) throws Throwable{
		selectByVisibleText(OperatorsAndVehiclesPage.accidentOrViolation, accidentOrViolation, "Accident or violation within last 5 years");
	}
	private void enterAccOrConvictionDate(String accOrConvictionDate) throws Throwable{
		type(OperatorsAndVehiclesPage.accOrConvictionDate, accOrConvictionDate, "Acc Or Conviction Date");
		type(OperatorsAndVehiclesPage.accOrConvictionDate, Keys.TAB, "Tab");
	}

	public void fillOperatorInformationDetails(Hashtable<String, String> data) throws Throwable{
		enterLicenseNumber(data.get("licenseNumber"));
		selectLicenseState(data.get("licensedState"));
		enterOperatorDOB(data.get("operatorDOB"));
		clickAddIncidentButton();
		enterAccidentOrViolationSource(data.get("accidentOrViolationSource"));
		enterAccidentOrViolationWithin5Years(data.get("accidentOrViolationWithing5years"));
		enterAccOrConvictionDate(data.get("accOrConvictionDate"));
	}

}