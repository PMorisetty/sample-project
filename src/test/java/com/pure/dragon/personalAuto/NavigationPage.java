package com.pure.dragon.personalAuto;

import org.openqa.selenium.By;

public class NavigationPage {
	static By personalAutoPolicyPage, subjectivities;
	static By vehicleDetail, operatorDetail;
	static By driverAssignment, coverages, manuscriptEndorsement, accountSummary, memberInformation, application, forms;
	
	static{
		personalAutoPolicyPage = By.xpath("//div[@id='accountsColumn']//a[@title='Automobile Policy']");
		subjectivities = By.xpath("//div[@id='accountsColumn']//a[@title='Subjectivities']");
		vehicleDetail = By.xpath("//div[@id='accountsColumn']//a[contains(@title,'$')]");
		operatorDetail = By.xpath("//div[@id=\"accountsColumn\"]//a[@title=\"$\"]");
		
		driverAssignment = By.xpath("//div[@id='accountsColumn']//a[@title='Driver Assignment']");
		coverages = By.xpath("//div[@id='accountsColumn']//a[@title='Coverages']");
		manuscriptEndorsement = By.xpath("//div[@id='accountsColumn']//a[contains(@title,'Manuscript Endorsements')]");
		accountSummary = By.xpath("//div[@id='accountsColumn']//a[@title='Account Summary']");
		memberInformation = By.xpath("//div[@id='accountsColumn']//a[@title='Member Information & Policy History']");
		application = By.xpath("//div[@id='accountsColumn']//a[@title='Application']");
		forms = By.xpath("//div[@id='accountsColumn']//a[@title='Forms']");
	}
	
}
