package com.pure.dragon.personalAuto;

import org.openqa.selenium.By;

public class MemberInformationAndPolicyHistoryPage {
	static By employer, existingAgencyClientNo, existingAgencyClientYes,
	companyCancelledRefusedToInsureNo, companyCancelledRefusedToInsureYes,
	coverageNonrenewedOrDeclinedNo, coverageNonrenewedOrDeclinedYes,
	memberOfHouseholdNo, memberOfHouseholdYes; 
	
	
	static{
		employer = By.xpath("//div[contains(text(), 'Employer')]/following-sibling::div//input");
		existingAgencyClientNo = By.xpath("//div[@title='Existing Agency Client?']/following-sibling::div//input[@title='No']");
		existingAgencyClientYes = By.xpath("//div[@title='Existing Agency Client?']/following-sibling::div//input[@title='Yes']");
		companyCancelledRefusedToInsureNo = By.xpath("//div[@title='Has any company cancelled or refused to insure in the past 3 years?']/following-sibling::div//input[@title='No']");
		companyCancelledRefusedToInsureYes = By.xpath("//div[@title='Has any company cancelled or refused to insure in the past 3 years?']/following-sibling::div//input[@title='Yes']");
		coverageNonrenewedOrDeclinedNo = By.xpath("//div[@title='Has your coverage been non-renewed/Declined?']/following-sibling::div//input[@title='No']");
		coverageNonrenewedOrDeclinedYes = By.xpath("//div[@title='Has your coverage been non-renewed/Declined?']/following-sibling::div//input[@title='Yes']");
		memberOfHouseholdNo = By.xpath("//div[contains(@title, 'member of your household')]/following-sibling::div//input[@title='No']");
		memberOfHouseholdYes = By.xpath("//div[contains(@title, 'member of your household')]/following-sibling::div//input[@title='Yes']");
		
		
	}
}

