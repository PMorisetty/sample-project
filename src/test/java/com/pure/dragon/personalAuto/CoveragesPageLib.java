package com.pure.dragon.personalAuto;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CoveragesPageLib extends ActionEngine{
	public CoveragesPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void selectCombinedOrSplitLimits(Hashtable<String, String> data) throws Throwable {
		String uiOptions = data.get("Underinsured Options_1");

		if(uiOptions.equalsIgnoreCase("NULL")){
			uiOptions=(!data.get("BI Limit_1").equalsIgnoreCase("NULL")?"BI":
				(!data.get("CSL Limit_1").equalsIgnoreCase("NULL")?"CSL":"NULL"));
		}

		switch(uiOptions){
		case "CSL With PD":
			selectByVisibleText(CoveragesPage.combinedOrSplitLimits, "Combined Single Limit (CSL)",
					"Combined Single Limit (CSL)");
			selectByVisibleText(CoveragesPage.CSLLimits, data.get("CSL Limit_1"),
					"CSLLimits");
			selectByIndex(CoveragesPage.PIPDeductible, Integer.valueOf(data.get("PIP Limit_1")),"PIPDeductible");
			selectUIM_CSLLimit(data.get("UIM Limit_1"), "YES");
			break;
		case "CSL Without PD":
			selectByVisibleText(CoveragesPage.combinedOrSplitLimits, "Combined Single Limit (CSL)",
					"Combined Single Limit (CSL)");
			selectByVisibleText(CoveragesPage.CSLLimits, data.get("CSL Limit_1"),
					"CSLLimits");
			selectByIndex(CoveragesPage.PIPDeductible, Integer.valueOf(data.get("PIP Limit_1")),"PIPDeductible");
			selectUIM_CSLLimit(data.get("UIM Limit_1"), "NO");
			break;
		case "BI":
			if(data.get("state").equals("NC")){
				selectByVisibleText(CoveragesPage.splitLimits, "Split limits", "Split limits");}
			else if(!(data.get("state").equals("MA")||data.get("state").equals("NC"))){
				selectByVisibleText(CoveragesPage.combinedOrSplitLimits, "Split limits", "Split limits");}
			selectSplitBILimits(data.get("BI Limit_1"));
			switch(data.get("state")){
			case "MA":
				selectUIMBILimit(data.get("UIM Limit_1"));
			case "AL":
			case "LA":
			case "CO":
				selectUMBIlimit(data.get("UM Limit_1"));
				break;	
			case "CT":
				selectUMUIMSplitLimitCoverage(data.get("UM Limit_1"));
				selectUIMStandard(data.get("UIM Limit_1"));
				break;
			case "WA":
				selectPIPMedicalFuneralExpense(data.get("PIPMedicalFuneralExpense_1"));	
				selectUIMBILimit(data.get("UIM Limit_1"));
				selectUIMPDLimit(data.get("UIM PD Limit_1"));				
				break;			
			case "VA":
				selectIncomeLoss(data.get("AFPB Income/Work Loss_1"));
				selectMedicalExpense(data.get("AFPB Medical Expenses_1"));	
				selectUMBIlimit(data.get("UM Limit_1"));
				selectUMPDLimit(data.get("PD Limits_1"));
				break;
			case "NJ":
				selectPIPDeductible(data.get("PIP Deductibe"));
				selectDoYouWishToPurchaseLimitedTortOrFullTortselect(data.get("Tort Option_1"));
				break;
			case "NC":
				selectUMUIMLimit(data.get("UIM Limit_1"));
				break;
			case "IL":
				selectUMPDLimit(data.get("PD Limits_1"));
			case "OK":
			case "PA":
				selectUMBILimits(data.get("UIM Limit_1"));
				break;
			case "SC":
				selectUMBIlimit(data.get("UM Limit_1"));
				selectUIMCSLLimit(data.get("UIM Limit_1"));
				break;
			case "NY":
				selectPIPDeductible(data.get("PIP Deductibe"));
				break;
			case "CA":
				selectCSLUMBI(data.get("CSLUMBI"));
				selectUMLimit(data.get("UM Limit_1"));
				break;
			case "TX":
				selectPIPLimits(data.get("PIP Limit_1"));
				selectUMLimit(data.get("UM Limit_1"));
				selectautomobileDeathIndemnity(data.get("Automobile Death Indemnity"));
				break;
			case "FL":
				selectPIPDeductible(data.get("PIP Deductibe"));
				selectUMBILimits(data.get("UIM Limit_1"));
				selectAdditionalPIPLimits(data.get("Additional PIPLimits"));
				selectPIPOptions(data.get("PIPOptions"));
				selectPDLimits(data.get("PD Limits_1"));
				break;								
			}
			if(!(data.get("state").equalsIgnoreCase("NJ")|| data.get("state").equalsIgnoreCase("VA"))){
				selectMedPayLimits(data.get("MP Limit_1"));}
			break;
		case "CSL":
			if(!data.get("CSL Limit_1").equalsIgnoreCase("NULL")){
				selectByVisibleText(CoveragesPage.combinedOrSplitLimits, "Combined Single Limit (CSL)",
						"Combined Single Limit (CSL)");
				selectByVisibleText(CoveragesPage.CSLLimits, data.get("CSL Limit_1"),
						"CSLLimits");
			}
			switch(data.get("state")){
			case "AL":
			case "OK":
				selectUMCSLLimit(data.get("UM Limit_1"));
				break;
			case "CT":
				selectUMUIMCSLLimitCoverage(data.get("UM Limit_1"));
				//selectUIMStandard(data.get("UIM Limit_1"));
				break;
			case "IL":
				selectUMBILimits(data.get("UM Limit_1"));
				break;			
			case "WA":
				selectPIPMedicalFuneralExpense(data.get("PIPMedicalFuneralExpense_1"));			    
				selectUIMCSLLimit(data.get("UIM Limit_1"));
				break;	
			case "VA":
				selectIncomeLoss(data.get("AFPB Income/Work Loss_1"));				
				selectMedicalExpense(data.get("AFPB Medical Expenses_1"));	
			case "LA":
			case "CO":
				selectUMBIlimit(data.get("UM Limit_1"));
				break;	
			case "MA":
			case "SC":
				selectUMBIlimit(data.get("UM Limit_1"));
				selectUIMCSLLimit(data.get("UIM Limit_1"));
				break;
			case "NJ":
				selectPIPDeductible(data.get("PIP Deductibe"));
				selectDoYouWishToPurchaseLimitedTortOrFullTortselect(data.get("Tort Option_1"));
				break;
			case "PA":
				selectUMBICSL(data.get("UIM Limit_1"));break;
			case "NY":
				selectPIPDeductible(data.get("PIP Deductibe"));
				break;
			case "CA":
				selectCSLUMBI(data.get("CSLUMBI"));
				selectUMLimit(data.get("UM Limit_1"));
				break;			
			case "TX":
				selectPIPLimits(data.get("PIP Limit_1"));
				selectUMLimit(data.get("UM Limit_1"));
				selectautomobileDeathIndemnity(data.get("Automobile Death Indemnity"));
				break;
			case "FL":
				selectPIPDeductible(data.get("PIP Deductibe"));
				selectUMBILimits(data.get("UIM Limit_1"));
				selectAdditionalPIPLimits(data.get("Additional PIPLimits"));
				selectPIPOptions(data.get("PIPOptions"));
				selectPDLimits(data.get("PD Limits_1"));
				break;			
			}
			if(!(data.get("state").equalsIgnoreCase("NJ")|| data.get("state").equalsIgnoreCase("VA"))){
				selectMedPayLimits(data.get("MP Limit_1"));}
			break;
		case "OK":
		case "IA":
			selectMedPayLimits(data.get("MP Limit_1"));
			break;		
		case "NULL":
			selectByVisibleText(CoveragesPage.combinedOrSplitLimits, "No Coverage", "CSL - No Coverage");
		}

		switch (data.get("state")) {
		case "PA":	
			selectDoYouWishToPurchaseLimitedTortOrFullTortselect(data.get("Tort Option_1"));
			selectBasicFirstPartyBenefitsCoverage(data.get("Basic FPB_1"));
			selectAddedFirstPartyBenefitsCoverage(data);
			selectcombinationFirstPartyBenefitsCoverageselect(data);
			selectExtraOrdinaryMedicalBenefitsCoverage(data.get("Extraordinary Medical Benefits PA_1"));
			break;
		}
	}
	public void selectExtraOrdinaryMedicalBenefitsCoverage(String value) throws Throwable {
		if(value.equalsIgnoreCase("NULL"))
			selectByVisibleText(CoveragesPage.extraordinaryMedicalBenefitsCoverageselect, "No Coverage", "Extra ordinary Medical BenefitsCoverage");
		else{
			selectByVisibleText(CoveragesPage.extraordinaryMedicalBenefitsCoverageselect, value, "Extra ordinary Medical BenefitsCoverage");
		}
	}
	public void selectDoYouWishToPurchaseLimitedTortOrFullTortselect(String value) throws Throwable {
		if(!value.equalsIgnoreCase("NULL"))
			selectByVisibleText(CoveragesPage.doYouWishToPurchaseLimitedTortOrFullTortselect, value, "tort option");		
	}

	public void selectBasicFirstPartyBenefitsCoverage (String value) throws Throwable {
		if(value.equalsIgnoreCase("NULL"))
			click(CoveragesPage.basicFirstPartyBenefitsCoverageNO,"Basic First Party Benefits Coverage NO");
		else 
			click(CoveragesPage.basicFirstPartyBenefitsCoverageYES, "Basic First Party Benefits Coverage YES");
	}
	public void selectAddedFirstPartyBenefitsCoverage (Hashtable<String, String>data) throws Throwable {
		//For PA Auto, accident/death benefit is null, shall make AFPBC is NO, Yes otherwise.
		String AFPBC = (data.get("AFPB Accident/Death Benefit_1").equalsIgnoreCase("NULL"))?"NULL":"";
		if(data.get("Basic FPB_1").equalsIgnoreCase("NULL")){
			if(AFPBC.equalsIgnoreCase("NULL")){
				click(CoveragesPage.addedFirstPartyBenefitsCoverageNO,"Added First Party Benefits Coverage NO");			
			}else{ 
				click(CoveragesPage.addedFirstPartyBenefitsCoverageYES, "Added First Party Benefits Coverage YES");
				selectByVisibleText(CoveragesPage.medicalExpenseselect, data.get("AFPB Medical Expenses_1"), "AFPB Medical Expenses option");
				selectByVisibleText(CoveragesPage.workLossselect, data.get("AFPB Income/Work Loss_1"), "AFPB Income/Work Loss option");
				selectByVisibleText(CoveragesPage.funeralExpenseselect, data.get("AFPB Funeral Expenses PA_1"), "AFPB Funeral Expenses PA option");
				selectByVisibleText(CoveragesPage.accidentalDeathBenefitselect, data.get("AFPB Accident/Death Benefit_1"), "AFPB Accident/Death Benefit option");
			}
		}
	}
	public void selectcombinationFirstPartyBenefitsCoverageselect(Hashtable<String, String>data) throws Throwable {
		//Select " Combination First Party Benefits Coverage" when
		//both "Basic First Party Benefits Coverage ($5,000 Medical Expense)" and "Added First Party Benefits Coverage" are NO		

		if( data.get("AFPB Accident/Death Benefit_1").equalsIgnoreCase("NULL")//this ensures "Added First Party Benefits Coverage" is NO
				&& data.get("Basic FPB_1").equalsIgnoreCase("NULL")//this ensures "Basic First Party Benefits Coverage" is NO
				){
			List<WebElement> elems = driver.findElement(CoveragesPage.combinationFirstPartyBenefitsCoverageselect).findElements(By.tagName("option"));
			String cFPBC = data.get("Combined First Party Benefits_1");
			Select s = new Select(driver.findElement(CoveragesPage.combinationFirstPartyBenefitsCoverageselect));
			for(int i = 0; i < elems.size(); i++){
				if(elems.get(i).getText().contains(cFPBC)){
					s.selectByVisibleText(elems.get(i).getText());
				}
			}			
		}
	}
	public void selectUIMBILimit(String value) throws Throwable {
		if(value.equalsIgnoreCase("NULL"))
			selectByVisibleText(CoveragesPage.UIMBILimits, "Rejection","UIMBILimits - Rejection");
		else 
			selectByVisibleText(CoveragesPage.UIMBILimits, value,"UIMBILimits");
	}

	public void selectUMCSLLimit(String value) throws Throwable {
		if(value.equalsIgnoreCase("NULL") || value.equalsIgnoreCase(""))
			selectByVisibleText(CoveragesPage.UMCSLLimit, "No Coverage","UIMBILimits - No Coverage");
		else 
			selectByVisibleText(CoveragesPage.UMCSLLimit, value,"UM CSL Limit");
	}
	public void selectUMBICSL(String value) throws Throwable {
		if(!value.equalsIgnoreCase("NULL"))			 
			selectByVisibleText(CoveragesPage.UMBICSL, value,"UIMBI CSL");
	}

	public void selectUIMPDLimit(String value) throws Throwable {
		if(value.equalsIgnoreCase("NULL"))
			selectByVisibleText(CoveragesPage.UIMPDLimits, "Rejection","UIMPDLimits - Rejection");
		else 
			selectByVisibleText(CoveragesPage.UIMPDLimits, value,"UIMPDLimits");
	}

	public void selectUIM_CSLLimit(String value, String includePD) throws Throwable {
		if(includePD.equalsIgnoreCase("NULL"))
			selectByVisibleText(CoveragesPage.UIM_CSLLimit, "Rejection","UIM - Rejection");
		else if(includePD.equalsIgnoreCase("YES"))
			selectByVisibleText(CoveragesPage.UIM_CSLLimit, value + " UIMBI/PD","CSL With PD");
		else 
			selectByVisibleText(CoveragesPage.UIM_CSLLimit, value + " UIMBI only","CSL Without PD");
	}
	/*public void selectUIM_CSLLimit(String value) throws Throwable {
		if(!value.equalsIgnoreCase("NULL"))			
			selectByVisibleText(CoveragesPage.UIM_CSLLimit, value + " UIMBI only","CSL Without PD");
	}*/

	public void selectMedPayLimits(String medPayLimits) throws Throwable {

		if(medPayLimits.equalsIgnoreCase("NULL"))
			selectByVisibleText(CoveragesPage.medPayLimits, "No Coverage","MedPayLimits");
		else 
			selectByVisibleText(CoveragesPage.medPayLimits, medPayLimits,"MedPayLimits");

	}

	public void selectPIPDeductible(String medPayLimits) throws Throwable {
		selectByVisibleText(CoveragesPage.PIPDedctible, medPayLimits,"PIPDeductible");
	}
	public void selectOTCDeductible(String otcDeductible, int num) throws Throwable {
		//For varying otcDeductible, replace index by values from excel and use visible text
		//Along with this for multiple vehicles, use number of vehicles as a counter for excel keys.
		//hi
		/*String otcDeductibleXPTH= (num==1)?"(//div[text()='vehicles']/../..//select)["+ num+"]":
									(num>1)?"(//div[text()='vehicles']/../..//select)["+ (num+3*(num-1))+"]":null;*/
		String otcDeductibleXPTH = "(//select[@title='OTC Deductible'])["+ num+"]";
		By otcDeductibleBY = By.xpath(otcDeductibleXPTH);
		if(otcDeductible.equalsIgnoreCase("NULL")||otcDeductible.equalsIgnoreCase("")){
			selectByVisibleText(otcDeductibleBY, "No Coverage","OTCDeductible");
		}else{
			selectByVisibleText(otcDeductibleBY, otcDeductible,"OTCDeductible");
		//		scroll(CoveragesPage.otcDeductible,	"OTCDeductible");
		//		int index = 1;
		//		selectByIndex(CoveragesPage.otcDeductible, index,
		//				"OTCDeductible");
		}
	}
	public void selectCOLLDeductible(String collDeductible, int num) throws Throwable {
		//For varying collDeductible, replace index by values from excel and use visible text
		//Along with this for multiple vehicles, use number of vehicles as a counter for excel keys.
		/*String collDeductibleXPTH= (num==2)?"(//div[text()='vehicles']/../..//select)["+ num+"]":
									(num>2)?"(//div[text()='vehicles']/../..//select)["+ (num+3*(num-1))+"]":null;*/
		String collDeductibleXPTH = "(//select[@title='COLL Deductible'])["+ num+"]";
		By collDeductibleBY = By.xpath(collDeductibleXPTH);
		if(collDeductible.equalsIgnoreCase("NULL")||collDeductible.equalsIgnoreCase("")){
			selectByVisibleText(collDeductibleBY, "No Coverage","COLLDeductible");
		}else{
			selectByVisibleText(collDeductibleBY, collDeductible, "COLLDeductible");
		//		scroll(CoveragesPage.collDeductible,	"COLLDeductible");
		//		int index = 1;
		//		selectByIndex(CoveragesPage.collDeductible, index,"COLLDeductible");
		}

	}

	public void selectCOLLOption(String collOption, int num) throws Throwable {
		//For varying colloption, replace index by values from excel and use visible text
		//Along with this for multiple vehicles, use number of vehicles as a counter for excel keys.
		/*String collOptionXPTH= (num==2)?"(//div[text()='vehicles']/../..//select)["+ num+"]":
									(num>2)?"(//div[text()='vehicles']/../..//select)["+ (num+3*(num-1))+"]":null;*/
		String collOptionXPTH = "(//select[@title='COLL Option'])["+ num+"]";
		By collOptionBY = By.xpath(collOptionXPTH);
		if(collOption.equalsIgnoreCase("NULL")||collOption.equalsIgnoreCase("")){
			selectByVisibleText(collOptionBY, "No Coverage","COLLOption");
		}else{
			selectByVisibleText(collOptionBY, collOption, "COLLOption");
		}

	}

	public void selectWaiverOfCOLLDed(String waiverCollDed, int num) throws Throwable {
		//For varying waivercollded, replace index by values from excel and use visible text
		//Along with this for multiple vehicles, use number of vehicles as a counter for excel keys.
		/*String collOptionXPTH= (num==2)?"(//div[text()='vehicles']/../..//select)["+ num+"]":
									(num>2)?"(//div[text()='vehicles']/../..//select)["+ (num+3*(num-1))+"]":null;*/
		String waiverCollDedXPTH = "(//select[@title='Waiver of COLL Ded'])["+ num+"]";
		By waiverCollDedBY = By.xpath(waiverCollDedXPTH);
		if(waiverCollDed.equalsIgnoreCase("NULL")||waiverCollDed.equalsIgnoreCase("")){
			selectByVisibleText(waiverCollDedBY, "No Coverage","WaiverCOLLDed");
		}else{
			selectByVisibleText(waiverCollDedBY, waiverCollDed, "WaiverCOLLDed");
		}

	}

	public void selectOptions(String options) throws Throwable {
		selectByVisibleText(CoveragesPage.options, options,
				"Options");

	}
	private void selectAdditionalPIPLimits(String additionalPIPLimits) throws Throwable {
		selectByVisibleText(CoveragesPage.additionalPIPLimits, additionalPIPLimits,
				"AdditionalPIPLimits");

	}
	private void selectPIPOptions(String PIPOptions) throws Throwable {
		if(PIPOptions!=null && !PIPOptions.equalsIgnoreCase("") && isVisibleOnly(CoveragesPage.PIPOptions, "PIPOptions")){
			selectByVisibleText(CoveragesPage.PIPOptions, PIPOptions,
					"PIPOptions");
		}
	}
	private void selectPDLimits(String PDLimits) throws Throwable {
		if(PDLimits!=null && !PDLimits.equalsIgnoreCase("") && isVisibleOnly(CoveragesPage.PDLimits, "PDLimits")){
			selectByVisibleText(CoveragesPage.PDLimits, PDLimits,
					"PDLimits");
		}
	}
	public void selectUMBICSLLimits(String UMBICSLLimits) throws Throwable {
		if(UMBICSLLimits!=null && !UMBICSLLimits.equalsIgnoreCase("") && isVisibleOnly(CoveragesPage.UMBICSLLimit, "UMBILimits")){
			if(UMBICSLLimits.equalsIgnoreCase("NULL"))
				selectByVisibleText(CoveragesPage.UMBICSLLimit, "No Coverage","UMBILimits");
			else				
				selectByVisibleText(CoveragesPage.UMBICSLLimit, UMBICSLLimits,"UMBILimits");
		}
	}
	public void selectUIMBICSLLimits(String UIMBICSLLimits) throws Throwable {
		if(UIMBICSLLimits!=null && !UIMBICSLLimits.equalsIgnoreCase("") && isVisibleOnly(CoveragesPage.UIMBICSLLimits, "UMBILimits")){
			if(UIMBICSLLimits.equalsIgnoreCase("NULL"))
				selectByVisibleText(CoveragesPage.UIMBICSLLimits, "No Coverage","UMBILimits");
			else				
				selectByVisibleText(CoveragesPage.UIMBICSLLimits, UIMBICSLLimits,"UMBILimits");
		}
	}
	private void selectUMUIMLimit(String UMUIMLimit) throws Throwable {
		if(UMUIMLimit!=null && !UMUIMLimit.equalsIgnoreCase("")){
			if(UMUIMLimit.equalsIgnoreCase("NULL"))
				selectByVisibleText(CoveragesPage.UMUIMLimit, "No Coverage","UM/UIMLimit");
			else				
				selectByVisibleText(CoveragesPage.UMUIMLimit, UMUIMLimit,"UM/UIMLimit");
		}
	}
	private void selectUMBILimits(String UMBICSLLimits) throws Throwable {
		if(UMBICSLLimits!=null && !UMBICSLLimits.equalsIgnoreCase("") && isVisibleOnly(CoveragesPage.UMBILimits, "UMBILimits")){
			if(UMBICSLLimits.equalsIgnoreCase("NULL"))
				selectByVisibleText(CoveragesPage.UMBILimits, "No Coverage","UMBILimits");
			else				
				selectByVisibleText(CoveragesPage.UMBILimits, UMBICSLLimits,"UMBILimits");
		}
	}
	private void selectUMBIlimit(String umbilimit) throws Throwable {
		if(umbilimit!=null && !umbilimit.equalsIgnoreCase("")){
			selectByVisibleText(CoveragesPage.UMBIlimit, umbilimit,"UMBILimits");
		}
	}
	private void selectUIMCSLLimit(String uimcsllimit) throws Throwable {
		if(uimcsllimit!=null && !uimcsllimit.equalsIgnoreCase("")){
			selectByVisibleText(CoveragesPage.UIMCSLLimit, uimcsllimit,"UIMCSLLimit");
		}
	}
	private void selectUMPDLimit(String umpdlimit) throws Throwable {
		if(umpdlimit!=null && !umpdlimit.equalsIgnoreCase("")){
			selectByVisibleText(CoveragesPage.umpdLimit, umpdlimit,"UMPDLimits");
		}
	}
	private void selectIncomeLoss(String incomeloss) throws Throwable {
		if(incomeloss!=null && !incomeloss.equalsIgnoreCase("")){

			selectByVisibleText(CoveragesPage.incomeLoss, incomeloss,"IncomeLoss");
		}
	}
	private void selectPIPMedicalFuneralExpense(String pipmedicalfuneralexpense) throws Throwable {
		if(pipmedicalfuneralexpense!=null && !pipmedicalfuneralexpense.equalsIgnoreCase("")){

			selectByVisibleText(CoveragesPage.pipMedicalFuneralExpense, pipmedicalfuneralexpense,"PIPMedicalFuneralExpense");
		}
	}
	private void selectMedicalExpense(String medicalexpense) throws Throwable {
		if(medicalexpense!=null && !medicalexpense.equalsIgnoreCase("")){

			selectByVisibleText(CoveragesPage.medicalExpenseselect, medicalexpense,"MedicalExpense");
		}
	}
	private void selectUIMStandard(String uimcslstandard) throws Throwable {
		if(uimcslstandard!=null && !uimcslstandard.equalsIgnoreCase("")){
			if(uimcslstandard.equalsIgnoreCase("NULL"))
				selectByVisibleText(CoveragesPage.UIMCSLStandard, "No Coverage","UIMCSLStandard");
			else				
				selectByVisibleText(CoveragesPage.UIMCSLStandard, uimcslstandard,"UIMCSLStandard");
		}
	}
	private void selectUMUIMSplitLimitCoverage(String umuimsplitlimitcoverage) throws Throwable {
		if(umuimsplitlimitcoverage!=null && !umuimsplitlimitcoverage.equalsIgnoreCase("")){
			selectByVisibleText(CoveragesPage.UMUIMSplitLimitCoverage, umuimsplitlimitcoverage,"UMUIMSplitLimitCoverage");
		}
	}
	
	private void selectUMUIMCSLLimitCoverage(String umuimcsllimitcoverage) throws Throwable {
		if(umuimcsllimitcoverage!=null && !umuimcsllimitcoverage.equalsIgnoreCase("")){
			selectByVisibleText(CoveragesPage.UMUIMCSLLimitCoverage, umuimcsllimitcoverage,"UMUIMCSLLimitCoverage");
		}
	}
	public void selectUMBIOptions(String UMBICSLOptions) throws Throwable {
		if(UMBICSLOptions!=null && !UMBICSLOptions.equalsIgnoreCase("") && isVisibleOnly(CoveragesPage.UMBIOptions, "UMBIOptions")){
			selectByVisibleText(CoveragesPage.UMBIOptions, UMBICSLOptions,
					"UMBIOptions");
		}
	}
	private void selectCSLUMBI(String CSLUMBI) throws Throwable {
		if(CSLUMBI!=null && !CSLUMBI.equalsIgnoreCase("") && isVisibleOnly(CoveragesPage.CSLUMBI, "CSLUMBI")){
			selectByVisibleText(CoveragesPage.CSLUMBI, CSLUMBI,
					"CSLUMBI");
		}
	}
	private void selectPIPLimits(String PIPLimits) throws Throwable {
		if(PIPLimits!=null && !PIPLimits.equalsIgnoreCase("") && isVisibleOnly(CoveragesPage.PIPLimits, "PIPLimits")){
			selectByVisibleText(CoveragesPage.PIPLimits, PIPLimits, "PIPLimits");
		}
	}
	private void selectUMLimit(String UMLimit) throws Throwable {
		if(UMLimit!=null && !UMLimit.equalsIgnoreCase("") && isVisibleOnly(CoveragesPage.UMLimit, "UMLimit")){
			selectByVisibleText(CoveragesPage.UMLimit, UMLimit, "UMLimit");
		}
	}
	private void selectautomobileDeathIndemnity(String automobileDeathIndemnity) throws Throwable {
		selectByVisibleText(CoveragesPage.automobileDeathIndemnity, automobileDeathIndemnity,
				"AutomobileDeathIndemnity");

	}
	public void selectSupplementalSpousalLiability(String supplementalSpousalLiability) throws Throwable {
		if(supplementalSpousalLiability!=null && !supplementalSpousalLiability.equalsIgnoreCase("") && supplementalSpousalLiability.equalsIgnoreCase("Yes"))
			click(CoveragesPage.supplementalSpousalLiability, "SupplementalSpousalLiability");
	}
	public void selectNoFaultWorkReduction(String noFaultWorkReduction) throws Throwable {
		if(noFaultWorkReduction!=null && !noFaultWorkReduction.equalsIgnoreCase("") && noFaultWorkReduction.equalsIgnoreCase("Yes"))
			click(CoveragesPage.noFaultWorkReduction, "noFaultWorkReduction");
	}
	public void selectOptionalBasicEconomicLoss(String optionalBasicEconomicLoss) throws Throwable {
		if(optionalBasicEconomicLoss!=null && !optionalBasicEconomicLoss.equalsIgnoreCase("") && optionalBasicEconomicLoss.equalsIgnoreCase("Yes"))
			click(CoveragesPage.optionalBasicEconomicLoss, "OptionalBasicEconomicLoss");
	}
	public void selectCSLLimits(String selectCSLLimits) throws Throwable {
		if(selectCSLLimits.equalsIgnoreCase("Yes"))
			click(CoveragesPage.selectCSLLimits, "selectCSLLimits");
	}
	public void selectCSLUMBILimits(String selectCSLUMBILimits) throws Throwable {
		if(selectCSLUMBILimits.equalsIgnoreCase("Yes"))
			click(CoveragesPage.selectCSLUMBILimits, "selectCSLUMBILimits");
	}
	public void selectUMBILimit(String UMBILimit) throws Throwable {
		if(UMBILimit!=null && !UMBILimit.equalsIgnoreCase("") && isVisibleOnly(CoveragesPage.UMBILimit, "UMBILimit")){
			selectByVisibleText(CoveragesPage.UMBILimit, UMBILimit,"UMBILimit");
		}
	}
	public void selectSplitBILimits(String BILimits) throws Throwable {
		selectByVisibleText(CoveragesPage.BILimits,BILimits, "BILimits");
	}
	public void selectSplitUMBILimits(String selectSplitUMBILimits) throws Throwable {
		if(selectSplitUMBILimits.equalsIgnoreCase("Yes"))
			click(CoveragesPage.selectSplitUMBILimits, "selectSplitUMBILimits");
	}

	public void fillCoverageDetails(Hashtable<String, String> data) throws Throwable {
		selectCombinedOrSplitLimits(data);
		int noOfVehicles = Integer.valueOf(data.get("Vehicles_Count"));
		List<WebElement> rows = driver.findElements(By.xpath("//div[@class='headerLabel'][text()='vehicles']/../..//table[@class='objectListTable']//tr"));
		for(int iterator = 1; iterator <= noOfVehicles;iterator++){
			selectOTCDeductible(data.get("OTC Ded_" + iterator),iterator);
			selectCOLLDeductible(data.get("Coll Ded_" + iterator),iterator);
			if(data.get("state").equals("MA")){ 
				selectCOLLOption(data.get("Collision Options_" + iterator),iterator);
				selectWaiverOfCOLLDed(data.get("Waiver Coll Ded_" + iterator),iterator);
			}
			if(data.get("OTC Ded_" + iterator).equalsIgnoreCase("NULL")||data.get("OTC Ded_" + iterator).equalsIgnoreCase("")||
					data.get("OTC Ded_" + iterator).equalsIgnoreCase("NULL")||data.get("OTC Ded_" + iterator).equalsIgnoreCase(""))
				new Select(rows.get(iterator).findElement(By.cssSelector("select[title='Towing and Labor Limit']"))).selectByVisibleText("No Coverage");
		}
		/*switch (data.get("state")) {
		case "NY":
			//selectOptions(data.get("options"));
			selectSupplementalSpousalLiability(data.get("supplementalSpousalLiability"));
			selectNoFaultWorkReduction(data.get("noFaultWorkReduction"));
			selectOptionalBasicEconomicLoss(data.get("optionalBasicEconomicLoss"));
			break;
		case "TX":
			selectautomobileDeathIndemnity(data.get("automobileDeathIndemnity"));
			selectPIPLimits(data.get("PIPLimits"));
			selectUMLimit(data.get("UMLimit"));
			break;
		case "FL":
			selectPDLimits(data.get("PDLimits"));
			selectPIPOptions(data.get("PIPOptions"));
			selectAdditionalPIPLimits(data.get("additionalPIPLimits"));
			selectUMBILimits(data.get("UMBILimits"));
			selectUMBIOptions(data.get("UMBIOptions"));
			break;
		case "CA":
			if(data.get("combinedOrSplitLimits").equalsIgnoreCase("Combined Single Limit (CSL)")){
				selectCSLUMBI(data.get("CSLUMBI"));
				selectCSLLimits(data.get("selectCSLLimits"));
				selectCSLUMBILimits(data.get("selectCSLUMBILimits"));
			}else if(data.get("combinedOrSplitLimits").equalsIgnoreCase("Split limits")){
				selectUMBILimit(data.get("UMBILimit"));
				selectSplitBILimits(data.get("selectSplitBILimits"));
				selectSplitUMBILimits(data.get("selectSplitUMBILimits"));
			}
			break;
		}*/

	}



}