package com.pure.dragon.personalAuto;

import org.openqa.selenium.By;

public class CommonPage {
	static By nextButton;
	static By prevButton;
	static By nextRedButton;
	static By rateButton;
	static By saveChangesButton;
	static By exitButton;
	static By reviewChangesButton;
	static By obtainOperatorsAndVehiclesButton;
	static By geocodeButton;
	
	static{
		prevButton = By.xpath("//div[@id='headerBar']//a[contains(@href,'Action.114')]");
		nextButton = By.xpath("//div[@id='headerBar']//a[contains(@href,'Action.113')]");
		nextRedButton = By.xpath("//div[@id='headerBar']//a[contains(@href,'Action.606246')]");
		rateButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.506506')]");
		saveChangesButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.128505')]");
		exitButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.479905')]");
		reviewChangesButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.745233')]");
		obtainOperatorsAndVehiclesButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.601614')]");
		geocodeButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.611714')]");
	}
}

