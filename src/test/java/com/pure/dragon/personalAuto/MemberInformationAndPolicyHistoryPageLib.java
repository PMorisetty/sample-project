package com.pure.dragon.personalAuto;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class MemberInformationAndPolicyHistoryPageLib extends ActionEngine{
	public MemberInformationAndPolicyHistoryPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void setEmployer(String employer) throws Throwable {
		type(MemberInformationAndPolicyHistoryPage.employer, employer, "Employer");
	}
	
	public void existingAgencyClient(String existingAgencyClient)
			throws Throwable {
		if (existingAgencyClient.equalsIgnoreCase("Yes")) {
			click(MemberInformationAndPolicyHistoryPage.existingAgencyClientYes,
					"existingAgencyClient Yes");
		} else {
			click(MemberInformationAndPolicyHistoryPage.existingAgencyClientNo,
					"existingAgencyClient NO");
		}

	}
	
	public void companyCancelledRefusedToInsure(String companyCancelledRefusedToInsure)
			throws Throwable {
		if (companyCancelledRefusedToInsure.equalsIgnoreCase("Yes")) {
			click(MemberInformationAndPolicyHistoryPage.companyCancelledRefusedToInsureYes,
					"companyCancelledRefusedToInsure Yes");
		} else {
			click(MemberInformationAndPolicyHistoryPage.companyCancelledRefusedToInsureNo,
					"companyCancelledRefusedToInsure NO");
		}

	}
	
	public void coverageNonrenewedOrDeclined(String coverageNonrenewedOrDeclined)
			throws Throwable {
		if (coverageNonrenewedOrDeclined.equalsIgnoreCase("Yes")) {
			click(MemberInformationAndPolicyHistoryPage.coverageNonrenewedOrDeclinedYes,
					"coverageNonrenewedOrDeclined Yes");
		} else {
			click(MemberInformationAndPolicyHistoryPage.coverageNonrenewedOrDeclinedNo,
					"coverageNonrenewedOrDeclined NO");
		}

	}
	
	public void memberOfHousehold(String memberOfHousehold)
			throws Throwable {
		if (memberOfHousehold.equalsIgnoreCase("Yes")) {
			click(MemberInformationAndPolicyHistoryPage.memberOfHouseholdYes,
					"memberOfHousehold Yes");
		} else {
			click(MemberInformationAndPolicyHistoryPage.memberOfHouseholdNo,
					"memberOfHousehold No");
		}

	}
	
	public void fillMemberDetails(Hashtable<String, String> data)
			throws Throwable {
		this.setEmployer(data.get("employer"));
		this.existingAgencyClient(data.get("existingAgencyClient"));
		this.companyCancelledRefusedToInsure(data.get("companyCancelledRefusedToInsure"));
		this.coverageNonrenewedOrDeclined(data.get("coverageNonrenewedOrDeclined"));
	}
}
