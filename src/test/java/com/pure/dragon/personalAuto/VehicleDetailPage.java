package com.pure.dragon.personalAuto;

import org.openqa.selenium.By;

public class VehicleDetailPage {
		static By customOrAdditionalEquipmentInsuredNo, customOrAdditionalEquipmentInsuredYes,
				vehicleRegisteredInStateNo, vehicleRegisteredInStateYes, vacationUseVehicleYes, vacationUseVehicleNo,
				vehicleUse, ownership, titledName, annualMileage,annualMileageInput, vehicleDriven, vehicleKeptInGarageYes, vehicleKeptInGarageNo,
				privatePassengerVehicleYes, privatePassengerVehicleNo, pureDirectRepairProgramYes, pureDirectRepairProgramNo, annualMileageSelect, premiumTownCode,
				currentTermOverrideCheckbox;

		static By additionalCoverage;
		static By ABS, airbag, daytimeRunningLights;
		static By VINEtching, vehicleRecovery, disablingDevice, passive, active;
		static By vehicleType, drivenBy, agreedValue, agreedValuePP;
		static By interestType, lossPayeeName, streetAddress, city, state, zip;
		static By costNew, engineSize, yearFirstLicensed;
		
		static By motorcycleAccidentPreventionYes, motorcycleAccidentPreventionNo, motorcycleAccidentPreventionDate;

	
	
	
	static{
		customOrAdditionalEquipmentInsuredNo = By.xpath("//div[contains(text(), 'Does the vehicle have Custom or Additional Equipment to be insured?')]/following-sibling::div//input[@title='No']");
		customOrAdditionalEquipmentInsuredYes = By.xpath("//div[contains(text(), 'Does the vehicle have Custom or Additional Equipment to be insured?')]/following-sibling::div//input[@title='Yes']");
		vehicleRegisteredInStateNo = By.xpath("//div[contains(text(), 'Is this vehicle registered in')]/following-sibling::div//input[@title='No']");
		vehicleRegisteredInStateYes = By.xpath("//div[contains(text(), 'Is this vehicle registered in')]/following-sibling::div//input[@title='Yes']");
		vacationUseVehicleNo = By.xpath("//div[contains(text(), 'Is this a vacation use vehicle?') or contains(text(), 'Is this a Vacation Use Vehicle?')]/following-sibling::div//input[@title='Yes']");
		vacationUseVehicleYes = By.xpath("//div[contains(text(), 'Is this a vacation use vehicle?') or contains(text(), 'Is this a Vacation Use Vehicle?')]/following-sibling::div//input[@title='Yes']");
		vehicleUse = By.xpath("//select[@title='Vehicle Use']");
		ownership = By.xpath("//select[@title='Ownership']");
		titledName = By.xpath("//select[@title='Vehicle Titled in the Name of']");
		
		annualMileage = By.xpath("//div[contains(text(), 'Annual Mileage')]/following-sibling::div//input");
		annualMileageInput = By.xpath("//div[contains(text(), 'Annual Mileage')]/following-sibling::div//select");
		annualMileageSelect = By.cssSelector("select[title='Mileage Band/Year']");
		vehicleDriven = By.xpath("//select[@title='Vehicle Driven'][contains(@class,'unFilledMandatoryField')]");
		vehicleKeptInGarageYes = By.xpath("//div[contains(text(), 'Is the vehicle kept in a garage with 24 hour security?')]/following-sibling::div//input[@title='Yes']");
		vehicleKeptInGarageNo = By.xpath("//div[contains(text(), 'Is the vehicle kept in a garage with 24 hour security?')]/following-sibling::div//input[@title='No']");
		privatePassengerVehicleYes = By.xpath("//div[contains(text(), 'Is there a private passenger vehicle insured by another PURE policy?')]/following-sibling::div//input[@title='Yes']");
		privatePassengerVehicleNo = By.xpath("//div[contains(text(), 'Is there a private passenger vehicle insured by another PURE policy?')]/following-sibling::div//input[@title='No']");
		pureDirectRepairProgramNo = By.xpath("//div[contains(text(), 'Agree to use PURE Direct Repair Program?')]/following-sibling::div//input[@title='No']");
		pureDirectRepairProgramYes = By.xpath("//div[contains(text(), 'Agree to use PURE Direct Repair Program?')]/following-sibling::div//input[@title='Yes']");
		premiumTownCode = By.cssSelector("select[title='Premium Town - Code']");
		
		additionalCoverage = By.xpath("//input[@title='Estimated Value']");

		//Discounts
		ABS = By.xpath("//div[contains(text(), 'ABS')]/preceding-sibling::span//input");
		airbag = By.xpath("//div[contains(text(), 'Airbag')]/preceding-sibling::span//input");
		daytimeRunningLights = By.xpath("//div[contains(text(), 'Daytime Running Lights?')]/preceding-sibling::span//input");
		
		//Anti-Theft Types
		VINEtching = By.xpath("//div[contains(text(), 'VIN Etching')]/preceding-sibling::span//input");
		vehicleRecovery = By.xpath("//div[contains(text(), 'Vehicle Recovery')]/preceding-sibling::span//input");
		disablingDevice = By.xpath("//div[contains(text(), 'Disabling Device')]/preceding-sibling::span//input");
		passive = By.xpath("//input[@title='Passive']");
		active = By.xpath("//input[@title='Active']");
		
		vehicleType = By.xpath("//select[@title='Vehicle Type']");
		drivenBy = By.xpath("//select[@title='Driven By']");
		agreedValue = By.xpath("//input[@title='Agreed Value of Vehicle$']");
		
		interestType = By.xpath("//select[@title='Interest Type']");
		lossPayeeName = By.xpath("//input[@title='Name']");
		streetAddress = By.xpath("//input[@title='Address Line 1']");
		city = By.xpath("//input[@title='City']");
		state = By.xpath("//select[@title='State']");
		zip = By.xpath("//input[@title='ZIP']");
		
		costNew = By.xpath("//input[@title='Cost new value']");
		engineSize = By.xpath("//input[@title='Engine Size']");
		yearFirstLicensed = By.xpath("//input[@title='Year first licensed to operate a motorcycle?']");
		
		currentTermOverrideCheckbox = By.cssSelector("input[type='checkbox'][title='Current Term Agreed Value']");
		agreedValuePP = By.cssSelector("input[type='text'][title='Current Term Agreed Value']");
		
		motorcycleAccidentPreventionYes = By.xpath("//div[contains(text(), 'Accident Prevention Course')]/following-sibling::div//input[@title='Yes']");
		motorcycleAccidentPreventionNo = By.xpath("//div[contains(text(), 'Accident Prevention Course')]/following-sibling::div//input[@title='No']");
		motorcycleAccidentPreventionDate = By.cssSelector("input[title*='Accident Prevention Course'][type='text']");
		
	}
}
