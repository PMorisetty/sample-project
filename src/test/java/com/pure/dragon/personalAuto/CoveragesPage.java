package com.pure.dragon.personalAuto;

import org.openqa.selenium.By;

public class CoveragesPage {
		static By combinedOrSplitLimits, splitLimits, medPayLimits, otcDeductible, collDeductible, options,
					automobileDeathIndemnity, additionalPIPLimits;
		static By CSLLimits, PIPDeductible, BILimits, UIM_CSLLimit, UIMBILimits, UIMPDLimits, UMCSLLimit,PIPDedctible; 
		static By supplementalSpousalLiability, noFaultWorkReduction, optionalBasicEconomicLoss;
		static By PIPOptions, UMBILimits, UMBIOptions, PDLimits, PIPLimits, UMLimit, UMBICSL;
		static By CSLUMBI, selectCSLLimits, selectCSLUMBILimits, UMBIlimit, UIMCSLLimit, incomeLoss, umpdLimit, UIMCSLCoverage, UIMCSLStandard, UMUIMSplitLimitCoverage,UMUIMCSLLimitCoverage, vehiclesObtainedThroughOnline;
		static By UMBILimit,UMBICSLLimit,UIMBICSLLimits, UMUIMLimit, selectSplitBILimits, selectSplitUMBILimits;
		static By basicFirstPartyBenefitsCoverageNO,basicFirstPartyBenefitsCoverageYES,addedFirstPartyBenefitsCoverageNO,addedFirstPartyBenefitsCoverageYES;
		
		static By combinationFirstPartyBenefitsCoverageselect,extraordinaryMedicalBenefitsCoverageselect;
		static By medicalExpenseselect, pipMedicalFuneralExpense, workLossselect, funeralExpenseselect, accidentalDeathBenefitselect;
		static By doYouWishToPurchaseLimitedTortOrFullTortselect;
	
	
	static{
		combinedOrSplitLimits = By.xpath("//select[@title='Combined or Split Limits']");
		splitLimits = By.xpath("//select[@title='Split Limits']");
		medPayLimits = By.cssSelector("select[title='MedPay Limits']");
		//otcDeductible = By.xpath("//select[@title='OTC Deductible' and @class='unFilledMandatoryField'] | //select[@title='OTC Deductible']");
		//otcDeductible = By.xpath("//select[@title='OTC Deductible' and @class='unFilledMandatoryField']");
		otcDeductible = By.xpath("(//div[text()='vehicles']/../..//select)[1]");
		//collDeductible = By.xpath("//select[@title='COLL Deductible' and @class='unFilledMandatoryField'] | //select[@title='COLL Deductible']");
		//collDeductible = By.xpath("//select[@title='COLL Deductible' and @class='unFilledMandatoryField']");
		collDeductible = By.xpath("(//div[text()='vehicles']/../..//select)[2]");
		options = By.xpath("//select[@title='OTC Options' and @class='unFilledMandatoryField']");
		automobileDeathIndemnity = By.xpath("//select[@title='Automobile Death Indemnity and Total Disability Coverage']");
		additionalPIPLimits = By.xpath("//select[@title='Additional PIP Limits']");
		
		CSLLimits = By.xpath("//div[contains(text(), 'CSL Limits')]/following-sibling::div//select");
		PIPDeductible = By.xpath("//select[contains(@title,'PIP')]");
		PIPDedctible = By.cssSelector("select[title='PIP Deductible']");
		BILimits = By.xpath("//div[contains(text(), 'BI Limits')]/following-sibling::div//select");
		UIM_CSLLimit = By.xpath("//select[contains(@title,'UIM CSL Limit')]");
		UIMBILimits = By.cssSelector("select[title='UIMBI']");
		UIMPDLimits = By.cssSelector("select[title='UIMPD']");
		UMCSLLimit = By.cssSelector("select[title='UM Combined Single Limit']");
		supplementalSpousalLiability = By.xpath("//div[contains(text(), 'Supplemental Spousal Liability')]/preceding-sibling::span//input");
		noFaultWorkReduction = By.xpath("//div[contains(text(), 'No-Fault Work Reduction')]/preceding-sibling::span//input");
		optionalBasicEconomicLoss = By.xpath("//div[contains(text(), 'Optional Basic Economic Loss')]/preceding-sibling::span//input");
	    
		UIMCSLStandard = By.xpath("(//select[contains(@title, 'UM/UIM') and contains(@title, 'Standard')])[2]");
		UMUIMSplitLimitCoverage = By.xpath("//select[@title='UM/UIM Split Limit Coverage Selection']");
		UMUIMCSLLimitCoverage = By.xpath("//select[@title='UM/UIM CSL Coverage Selection']");
	    			
		PIPOptions = By.xpath("//select[@title='PIP Options']");
		UMBILimits = By.xpath("//select[contains(@title, 'UMBI') and contains(@title, 'Limits') and @class='unFilledMandatoryField']");
		UMBIOptions = By.xpath("//select[contains(@title, 'UMBI') and contains(@title, 'Options') and @class='unFilledMandatoryField']");
		PDLimits = By.xpath("//select[@title='PD Limits']");
		PIPLimits = By.xpath("//select[@title='PIP Limits']");
		UMLimit = By.xpath("//select[contains(@title, 'UM') and contains(@title, 'Limit') and contains(@style, 'inline')]");
		
		CSLUMBI = By.xpath("//select[@title='CSL UMBI/PD Limit']");
		selectCSLLimits = By.xpath("//div[contains(text(), '$1,000,000 CSL limits')]/preceding-sibling::span//input");
		selectCSLUMBILimits = By.xpath("//div[contains(text(), '$1,000,000 CSL UMBI limits')]/preceding-sibling::span//input");
	    UMBIlimit = By.xpath("//select[contains(@title, 'UM') and contains(@style, 'inline')]");
		UIMCSLLimit = By.xpath("//select[contains(@title, 'UIM') and contains(@style, 'inline')]");
		incomeLoss = By.xpath("//div[@title='Income Loss']/..//select");
		umpdLimit = By.xpath("//div[contains(@title,'UMPD')]/..//select");
		vehiclesObtainedThroughOnline =  By.xpath("//input[@title='VIN']");
		
		UMBICSL = By.xpath("//select[@title='UMBI CSL']");
		
		UMBILimit = By.xpath("//select[@title='UMBI Limit']");
		UMBICSLLimit = By.xpath("//select[@title='UM combined single limit']");
		UIMBICSLLimits = By.xpath("//select[@title='Uninsured Motorist Split Limit ']");
		UMUIMLimit = By.xpath("//select[contains(@title,'Underinsured Motorist Split Limit ')]");
		selectSplitBILimits = By.xpath("//div[contains(text(), '$1,000,000 split BI limits')]/preceding-sibling::span//input");
		selectSplitUMBILimits = By.xpath("//div[contains(text(), '$1,000,000 split UMBI limits')]/preceding-sibling::span//input");
		
		basicFirstPartyBenefitsCoverageNO=By.xpath("//div[@title='Basic First Party Benefits Coverage ($5,000 Medical Expense)']/..//input[@title='No']");
		basicFirstPartyBenefitsCoverageYES=By.xpath("//div[@title='Basic First Party Benefits Coverage ($5,000 Medical Expense)']/..//input[@title='Yes']");		
		addedFirstPartyBenefitsCoverageNO=By.xpath("//div[@title='Added First Party Benefits Coverage']/..//input[@title='No']");
		addedFirstPartyBenefitsCoverageYES=By.xpath("//div[@title='Added First Party Benefits Coverage']/..//input[@title='Yes']");
		
		combinationFirstPartyBenefitsCoverageselect=By.xpath("//div[@title='Combination First Party Benefits Coverage (Medical Expense and Work Loss)']/..//select");
		extraordinaryMedicalBenefitsCoverageselect=By.xpath("//div[@title='Extraordinary Medical Benefits Coverage']/..//select");
		medicalExpenseselect=By.xpath("//div[@title='Medical Expense']/..//select");
		pipMedicalFuneralExpense=By.xpath("//div[@title='PIP: Aggregate Medical Expense/Funeral Expense/Income Continuation/Loss of Services']/..//select");
		workLossselect=By.xpath("//div[@title='Work Loss']/..//select");
		funeralExpenseselect=By.xpath("//div[@title='Funeral Expense']/..//select");
		accidentalDeathBenefitselect=By.xpath("//div[@title='Accidental Death Benefit']/..//select");
		doYouWishToPurchaseLimitedTortOrFullTortselect=By.xpath("//div[@title='Do you wish to purchase Limited Tort or Full Tort?']/..//select");
		
		
	}
}
