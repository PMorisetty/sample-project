package com.pure.dragon.personalAuto;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class NavigationPageLib extends ActionEngine{
	public NavigationPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void goToPersonalAutoPolicyPage() throws Throwable{
		click(NavigationPage.personalAutoPolicyPage, "Personal Auto Cover Page Link");
	}
	public void goToSubjectivities() throws Throwable{
		click(NavigationPage.subjectivities, "Subjectivities Link");
	}
	public void goToVehicle(String year, String make, String model) throws Throwable{
		String vehicleName = year + " " + make + " " + model;
		String locator = this.xpathUpdator(NavigationPage.vehicleDetail, vehicleName);
		click(By.xpath(locator), vehicleName + " - vehicle description link");
	}
	public void goToVehicle(String vehicleName) throws Throwable{
		String locator = this.xpathUpdator(NavigationPage.vehicleDetail, vehicleName);
		click(By.xpath(locator), vehicleName + " - vehicle description link");
	}
	public void goToOperator(String firstName, String lastName) throws Throwable{
		String operatorName = firstName + " " + lastName;
		String locator = this.xpathUpdator(NavigationPage.operatorDetail, operatorName);
		click(By.xpath(locator), operatorName + " - link");
	}
	public void goToDriverAssignment() throws Throwable{
		click(NavigationPage.driverAssignment, "Driver Assignment Link");
	}
	public void goToCoverages() throws Throwable{
		click(NavigationPage.coverages, "Coverages Link");
	}
	public void goToManuscriptEndorsement() throws Throwable{
		click(NavigationPage.manuscriptEndorsement, "Manuscript Endorsement Link");
	}
	public void goToAccountSummary() throws Throwable{
		click(NavigationPage.accountSummary, "Account Summary Link");
	}
	public void goToMemberInformation() throws Throwable{
		click(NavigationPage.memberInformation, "Member Information Link");
	}
	public void goToApplicationOrForms(Hashtable<String, String>data) throws Throwable{
		if (!(data.get("state").equalsIgnoreCase("FL")||data.get("state").equalsIgnoreCase("CT")||data.get("state").equalsIgnoreCase("SC")||data.get("state").equalsIgnoreCase("VA")||data.get("state").equalsIgnoreCase("NC"))) {		
			click(NavigationPage.application, "Application Link");
		}
		else if(data.get("state").equalsIgnoreCase("CT")){
			click(NavigationPage.forms, "Forms Link");
		}
		
	}
}
