package com.pure.dragon.personalAuto;

import java.util.Hashtable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class VehicleDetailPageLib extends ActionEngine{
	public VehicleDetailPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void customOrAdditionalEquipmentInsured(String customOrAdditionalEquipmentInsured, String additionalCoverage)
			throws Throwable {
		if (customOrAdditionalEquipmentInsured.equalsIgnoreCase("Yes")) {
			click(VehicleDetailPage.customOrAdditionalEquipmentInsuredYes,
					"customOrAdditionalEquipmentInsured Yes");
			type(VehicleDetailPage.additionalCoverage, additionalCoverage, "AdditionalCoverage");
		} else {
			click(VehicleDetailPage.customOrAdditionalEquipmentInsuredNo,
					"customOrAdditionalEquipmentInsured No");
		}
	}

	public void vehicleRegisteredInState(String vehicleRegisteredInState)
			throws Throwable {
		if(isVisibleOnly(VehicleDetailPage.vehicleRegisteredInStateYes, "Vehicle registered option")){
			if (vehicleRegisteredInState.equalsIgnoreCase("Yes")) {
				click(VehicleDetailPage.vehicleRegisteredInStateYes,
						"vehicleRegisteredInState Yes");
			} else {
				click(VehicleDetailPage.vehicleRegisteredInStateNo,
						"vehicleRegisteredInState No");
			}
		}
	}

	public void vacationUseVehicle(String vacationUseVehicle)
			throws Throwable {

		//
		if(isVisibleOnly(VehicleDetailPage.vacationUseVehicleYes,
				"vacationUseVehicle")){
			if (vacationUseVehicle.equalsIgnoreCase("Yes")) {
				click(VehicleDetailPage.vacationUseVehicleYes,
						"vacationUseVehicle Yes");
			} else {
				click(VehicleDetailPage.vacationUseVehicleNo,
						"vacationUseVehicle NO");
			}
		}		
	}

	public void selectVehicleUse(String vehicleUse) throws Throwable {
		if(isVisibleOnly(VehicleDetailPage.vehicleUse,
				"VehicleUse"))
			selectByVisibleText(VehicleDetailPage.vehicleUse, vehicleUse,
					"VehicleUse");
	}
	public void selectOwnership(Hashtable<String, String> data, int count) throws Throwable {
		selectByVisibleText(VehicleDetailPage.ownership, data.get("Auto_Vehicle_Ownership_Type_Name_" + count),
				"Ownership");
		if(data.get("Auto_Vehicle_Ownership_Type_Name_" + count).equalsIgnoreCase("Financed")||data.get("Auto_Vehicle_Ownership_Type_Name_" + count).equalsIgnoreCase("Leased")){
			fillLossPayeeDetails(data, count);
		}
	}
	public void fillLossPayeeDetails(Hashtable<String, String> data, int count) throws Throwable {
		selectInterestType(data.get("Interest_Type_" + count));
		setLossPayeeName(data.get("First_Name_Txt_1"));
		setStreetAddress(data.get("insuredAddress1"));
		setCity(data.get("insuredCity"));
		selectState(data.get("state"));
		setZip(data.get("insuredZip"));
	}


	public void selectTitledName() throws Throwable {
		//Always titledName is listed at index 1, no other values listed apart from this.
		selectByIndex(VehicleDetailPage.titledName, 1,
				"TitledName");
	}

	public void setAnnualMileage(String annualMileage) throws Throwable {
		if(isVisibleOnly(VehicleDetailPage.annualMileage, "AnnualMileage")){
			type(VehicleDetailPage.annualMileage, annualMileage, "AnnualMileage");
		}else if(isVisibleOnly(VehicleDetailPage.annualMileageInput, "AnnualMileageInput")){
			selectByVisibleText(VehicleDetailPage.annualMileageInput, annualMileage, "AnnualMileageInput");
		}
	}

	public void selectVehicleDriven() throws Throwable {
		//Always vehicleDriven is listed at index 1, no other values listed apart from this.
		java.util.List<WebElement> elems = driver.findElements(VehicleDetailPage.vehicleDriven);
		for(WebElement e: elems)
			new Select(e).selectByIndex(1);
		//			selectByIndex(VehicleDetailPage.vehicleDriven, 1,"vehicleDriven");
	}

	public void vehicleKeptInGarage(String vehicleKeptInGarage)
			throws Throwable {
		if(isVisibleOnly(VehicleDetailPage.vehicleKeptInGarageYes,
				"vehicleKeptInGarage")){
			if (vehicleKeptInGarage.equalsIgnoreCase("Yes")) {
				click(VehicleDetailPage.vehicleKeptInGarageYes,
						"vehicleKeptInGarage Yes");
			} else {
				click(VehicleDetailPage.vehicleKeptInGarageNo,
						"vehicleKeptInGarage NO");
			}
		}
	}

	public void privatePassengerVehicle(String privatePassengerVehicle)
			throws Throwable {
		if(isVisibleOnly(VehicleDetailPage.privatePassengerVehicleYes,
				"privatePassengerVehicle")){
			if (privatePassengerVehicle.equalsIgnoreCase("Yes")) {
				click(VehicleDetailPage.privatePassengerVehicleYes,
						"privatePassengerVehicle Yes");
			} else {
				click(VehicleDetailPage.privatePassengerVehicleNo,
						"privatePassengerVehicle NO");
			}
		}

	}

	public void pureDirectRepairProgram(String pureDirectRepairProgram)
			throws Throwable {
		if(isVisibleOnly(VehicleDetailPage.pureDirectRepairProgramYes,
				"pureDirectRepairProgram")){
			if (pureDirectRepairProgram.equalsIgnoreCase("Yes")) {
				click(VehicleDetailPage.pureDirectRepairProgramYes,
						"pureDirectRepairProgram Yes");
			} else {
				click(VehicleDetailPage.pureDirectRepairProgramNo,
						"pureDirectRepairProgram NO");
			}
		}
	}

	public void selectAnnualMileage(String annualMileage) throws Throwable {
		selectByVisibleText(VehicleDetailPage.annualMileageSelect, annualMileage, "AnnualMileage");
	}

	public void selectPremiumTownCode(String premiumtown) throws Throwable {
		selectByVisibleText(VehicleDetailPage.premiumTownCode, premiumtown, "PremiumTownCode");
	}


	public void fillVehicleDetails(Hashtable<String, String> data, int count) throws Throwable {
		//fill
		vehicleRegisteredInState("YES");
		additionVehicleDetails(data, count);
		String vehicleType = getTextOfSelectedOption(VehicleDetailPage.vehicleType, "Vehicle Type");
		switch(vehicleType){
		case "Private Passenger":
			if(!data.get("state").equals("VA")){
				customOrAdditionalEquipmentInsured(data.get("Additional_Equipment_Ind_" + count), data.get("Additional_Equipment_Amt_" + count));
			}
			selectVehicleUse(data.get("Auto_Vehicle_Usage_Type_Name_" + count));
			setAgreedValuePP(data.get("Agreed_Value_Amt_" + count));
			break;
		case "Travel Trailer":
		case "Recreational Trailer":
			selectVehicleUse(data.get("Auto_Vehicle_Usage_Type_Name_" + count));
		case "Antique Collectors Car":
		case "Classic Collectors Car":
		case "Exotic Collectors Car":
		case "Golf Carts":
		case "Collectors Trailer":
		case "All Terrain Vehicles":
		case "Snowmobiles":
		case "Dune Buggies":
			setAgreedValue(data.get("Agreed_Value_Amt_" + count));
			break;
		case "Motorcycles/Mopeds/Scooters":
			selectDrivenBy();
			setCostNew(data.get("MSRP_Amt_" + count));
			setEngineSize(data.get("Motorcycle_Engine_Size_CC_No_" + count));
			setAgreedValue(data.get("Agreed_Value_Amt_" + count));
			setYearFirstLicensed(data.get("Motorcycle_Years_First_Licensed_No_" + count));
			switch(data.get("state")){
			case "OK":
				selectMotorcycleAccidentPrevention("No", "");//No since sheet doesn't contain this value
				break;
			}
			break;
		case "Motor Home":
			customOrAdditionalEquipmentInsured(data.get("customOrAdditionalEquipmentInsured"), data.get("additionalCoverage"));
			selectDrivenBy();
			setAgreedValue(data.get("agreedValue"));
			selectVehicleUse(data.get("vehicleUse"));
			break;
		}
		selectDiscounts(data, count);
		selectAntiTheftTypes(data, count);
		selectOwnership(data, count);
		selectTitledName();
		//new CommonPageLib(driver, reporter).clickNextBtn();
	}

	public void additionVehicleDetails(Hashtable<String, String> data, int count) throws Throwable {
		switch (data.get("state")) {		
		case "FL":
			this.privatePassengerVehicle(data.get("privatePassengerVehicle"));
			this.pureDirectRepairProgram(data.get("pureDirectRepairProgram"));
			/*
			//Annual Mileage field for FL state is disabled after 09/15/2018 
			//this.setAnnualMileage(data.get("Annual_Mileage_List_Name_" + count));
			 */

		case "NY":
			this.vacationUseVehicle(data.get("vacationUseVehicle"));
			break;
		case "NJ":
			this.vacationUseVehicle(data.get("vacationUseVehicle"));		
		case "CA":
			this.setAnnualMileage(data.get("Annual_Mileage_List_Name_" + count));
			break;
		case "TX":			
			this.vacationUseVehicle(data.get("vacationUseVehicle"));
			this.vehicleKeptInGarage(data.get("vehicleKeptInGarage"));
			this.privatePassengerVehicle(data.get("privatePassengerVehicle"));
			break;
		case "MA":
			this.selectPremiumTownCode(data.get("Premium Town Code_" + count));
			break;		
		case "CO":
		case "IL":
		case "AL":
			this.vacationUseVehicle(data.get("vacationUseVehicle"));
			break;
		case "WA":
		case "IA":
		case "NC":
		case "OK":
		case "LA":
			this.selectAnnualMileage(data.get("Annual_Mileage_List_Name_" + count));
			break;
		case "CT":
			this.selectAnnualMileage(data.get("Annual_Mileage_List_Name_" + count));
		case "SC":
		case "PA":
			this.vacationUseVehicle(data.get("vacationUseVehicle"));
			this.privatePassengerVehicle(data.get("Auto_Vehicle_Insured_By_Another_PURE_Policy_Ind_"+ count));
			break;
		}
	}
	public void fillDriverAssignment(int noOfOperators, int noOfVehicles)throws Throwable {
		int noOfIterations = (noOfOperators<noOfVehicles)?noOfOperators:noOfVehicles;
		while(noOfIterations>0){
			this.selectVehicleDriven();
			noOfIterations--;
		}
	}

	public void selectABSDiscount(String ABSDiscounts)throws Throwable {
		if (ABSDiscounts!=null && ABSDiscounts!="" && ABSDiscounts.equalsIgnoreCase("Yes") && isVisibleOnly(VehicleDetailPage.ABS, "ABS")) {
			if(!isCheckBoxSelected(VehicleDetailPage.ABS))
				click(VehicleDetailPage.ABS,"ABS");
		}
	}
	public void selectAirbagDiscount(String airbag)throws Throwable {
		if (airbag!=null && airbag!="" && airbag.equalsIgnoreCase("Yes") && isVisibleOnly(VehicleDetailPage.airbag, "airbag")) {
			if(!isCheckBoxSelected(VehicleDetailPage.airbag))
				click(VehicleDetailPage.airbag,"Airbag");
		}
	}
	public void selectDaytimeRunningLightsDiscount(String daytimeRunningLights)throws Throwable {
		if (daytimeRunningLights!=null && daytimeRunningLights!="" && daytimeRunningLights.equalsIgnoreCase("Yes")) {
			click(VehicleDetailPage.daytimeRunningLights,"DaytimeRunningLights");
		}
	}
	public void selectDiscounts(Hashtable<String, String> data, int count)throws Throwable {
		selectABSDiscount(data.get("Anti_Lock_Brakes_Ind_" + count));
		selectAirbagDiscount(data.get("Airbag_Ind_" + count));
		//		selectDaytimeRunningLightsDiscount(data.get(vehicleType+"_daytimeRunningLights"));
	}

	public void selectVINEtching(String VINEtching)throws Throwable {
		if (VINEtching!=null && VINEtching!="" && VINEtching.equalsIgnoreCase("Yes") && isVisibleOnly(VehicleDetailPage.VINEtching,"VINEtching")) {
			if(!isCheckBoxSelected(VehicleDetailPage.VINEtching))
				JSClick(VehicleDetailPage.VINEtching,"VINEtching");
		}
	}
	public void selectVehicleRecovery(String vehicleRecovery)throws Throwable {
		if (vehicleRecovery!=null && vehicleRecovery!="" && vehicleRecovery.equalsIgnoreCase("Yes") && isVisibleOnly(VehicleDetailPage.vehicleRecovery,"VehicleRecovery")) {
			if(!isCheckBoxSelected(VehicleDetailPage.vehicleRecovery))
				JSClick(VehicleDetailPage.vehicleRecovery,"VehicleRecovery");
		}
	}
	public void selectDisablingDevice(String disablingDevice, String type)throws Throwable {
		if (disablingDevice!=null && disablingDevice!="" && disablingDevice.equalsIgnoreCase("Yes") && isVisibleOnly(VehicleDetailPage.disablingDevice,"DisablingDevice")) {
			if(!isCheckBoxSelected(VehicleDetailPage.disablingDevice))
				JSClick(VehicleDetailPage.disablingDevice,"DisablingDevice");

			if(type.equalsIgnoreCase("Passive")){
				JSClick(VehicleDetailPage.passive, "Passive");
			}else if(type.equalsIgnoreCase("Active")){
				JSClick(VehicleDetailPage.active, "Active");
			}	
		}

	}
	public void selectAntiTheftTypes(Hashtable<String, String> data, int count)throws Throwable {
		selectVINEtching(data.get("VIN_Etching_Ind_" + count));
		selectVehicleRecovery(data.get("Vehicle_Recovery_System_Ind_" + count));
		selectDisablingDevice(data.get("Disabling_Device_Ind_" + count), data.get("Anti_Theft_Disabling_Device_Type_Name_" + count));
	}

	public void selectDrivenBy() throws Throwable {
		if(isVisibleOnly(VehicleDetailPage.drivenBy, "DrivenBy")){
			selectByIndex(VehicleDetailPage.drivenBy, 1,"DrivenBy");
		}
	}
	public void setAgreedValue(String agreedValue) throws Throwable {
		type(VehicleDetailPage.agreedValue, agreedValue, "AgreedValue");
	}

	public void setAgreedValuePP(String agreedValue) throws Throwable {
		waitUntilJQueryReady();
		waitUntilLoadingDisappear();
		if(!isCheckBoxSelected(VehicleDetailPage.currentTermOverrideCheckbox))
			click(VehicleDetailPage.currentTermOverrideCheckbox, "Current Term Override");
		type(VehicleDetailPage.agreedValuePP, agreedValue, "AgreedValue");
	}

	public void selectInterestType(String interestType) throws Throwable {
		selectByVisibleText(VehicleDetailPage.interestType, interestType,
				"InterestType");
	}
	public void setLossPayeeName(String lossPayeeName) throws Throwable {
		type(VehicleDetailPage.lossPayeeName, lossPayeeName, "LossPayeeName");
	}
	public void setStreetAddress(String streetAddress) throws Throwable {
		type(VehicleDetailPage.streetAddress, streetAddress, "StreetAddress");
	}
	public void setCity(String city) throws Throwable {
		type(VehicleDetailPage.city, city, "City");
	}
	public void selectState(String state) throws Throwable {
		selectByVisibleText(VehicleDetailPage.state, state,
				"state");
	}
	public void setZip(String zip) throws Throwable {
		type(VehicleDetailPage.zip, zip, "zip");
	}
	public void setCostNew(String costNew) throws Throwable {
		if(costNew!= null && !costNew.equalsIgnoreCase("NULL"))
			type(VehicleDetailPage.costNew, costNew, "Cost New");
	}
	public void setEngineSize(String engineSize) throws Throwable {
		if(engineSize!= null && !engineSize.equalsIgnoreCase("NULL"))
			type(VehicleDetailPage.engineSize, engineSize, "engineSize$");
	}
	public void setYearFirstLicensed(String yearFirstLicensed) throws Throwable {
		if(yearFirstLicensed!= null && !yearFirstLicensed.equalsIgnoreCase("NULL") && isVisibleOnly(VehicleDetailPage.yearFirstLicensed, "yearFirstLicensed")){
			type(VehicleDetailPage.yearFirstLicensed, yearFirstLicensed, "yearFirstLicensed");
		}
	}

	public void selectMotorcycleAccidentPrevention(String value, String date)
			throws Throwable {
		if (value.equalsIgnoreCase("Yes")) {
			click(VehicleDetailPage.motorcycleAccidentPreventionYes,
					"MotorcycleAccidentPrevention Yes");
			type(VehicleDetailPage.motorcycleAccidentPreventionDate, date, "Motorcycle Accident Prevention Date");
		} else {
			click(VehicleDetailPage.motorcycleAccidentPreventionNo,
					"MotorcycleAccidentPrevention No");
		}
	}
}