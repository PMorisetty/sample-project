package com.pure.dragon.personalAuto;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CommonPageLib extends ActionEngine{
	public CommonPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void clickNextBtn() throws Throwable {
		click(CommonPage.nextButton, "Next Button");
	}
	public void clickPreviousBtn() throws Throwable {
		click(CommonPage.prevButton, "Previous Button");
	}
	public void clickNextRedBtn() throws Throwable {
		click(CommonPage.nextRedButton, "Next Red Button");
	}
	public void clickRateBtn() throws Throwable {
		click(CommonPage.rateButton, "Rate Button");
	}
	public void clickSaveChangesBtn() throws Throwable {
		click(CommonPage.saveChangesButton, "Save Changes Button");
	}
	public void clickExitBtn() throws Throwable {
		click(CommonPage.exitButton, "Exit Button");
	}
	public void clickObtainOperatorsAndVehiclesBtn() throws Throwable {
		if(isVisibleOnly(CommonPage.obtainOperatorsAndVehiclesButton, "Obtain Operators and Vehicles"))
			click(CommonPage.obtainOperatorsAndVehiclesButton, "Obtain Operators and Vehicles");
	}
	public void clickReviewChangesBtn() throws Throwable {
		click(CommonPage.reviewChangesButton, "Review Changes Button");
	}
	public void clickGeocodeBtn() throws Throwable {
		click(CommonPage.geocodeButton, "Geocode Button");
	}
}
