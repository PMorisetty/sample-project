package com.pure.dragon.personalAuto;

import org.openqa.selenium.By;

public class DriverDetailPage {
	static By gender, maritalStatus, licenseStatus, licensedInUS, 
	accidentPreventionCourseYes, accidentPreventionCourseNo, preventionCourseDate,driverTrainingYes, driverTrainingNo,driverTrainingDate,
	driverSpend3orMoremonthsYes, driverSpend3orMoremonthsNo,vehicleWithStudentAtSchoolYes,vehicleWithStudentAtSchoolNo,
	driverTookListedAutosYes, driverTookListedAutosNo,driverImprovementCourseYes,driverImprovementCourseNo,improvementCourseDate, driverWithDiffStateYes, driverWithDiffStateNo, militaryPersonnalYes, militaryPersonnalNo , 
	driverTrainigCourseCompletedIn5YrsYes,driverTrainigCourseCompletedIn5YrsNo,trainingCourseDate,
	licensedInOtherStateYes, licensedInOtherStateNo, priorLicensedState, priorDriverLicenseNumber, maturedDriverCourseCompletedYes,maturedDriverCourseCompletedNo , defensiveDriverCourseCompletedYes, defensiveDriverCourseCompletedNo;
	static By relationToInsured, fullTimeStudentYes, fullTimeStudentNo, gradeAverageYes, gradeAverageNo;
	
	static By driverLicenseState, licenseNumber, occupation;
	static By firstYearLicensedInUS, driverLicenseSuspendedRevokedYes, driverLicenseSuspendedRevokedNo,
	 driverLicenseLapsedYes, driverLicenseLapsedNo, SR22CertificateInsuranceYes, SR22CertificateInsuranceNo;
	
	static By incidentAddButton, source, accidentOrViolation, accOrConvictionDate;
	static{
		gender = By.xpath("//select[@title='Gender']");
		relationToInsured = By.xpath("//select[@title='Relation to Insured']");
		maritalStatus = By.xpath("//select[@title='Marital Status']");
		licenseStatus = By.xpath("//select[@title='License Status']");
		licensedInUS = By.xpath("//select[@title='How long have you been licensed in the US?']");
		driverSpend3orMoremonthsYes = By.xpath("//div[contains(text(), 'Does this driver spend 3 or more months out of state each year?')]/following-sibling::div//input[@title='Yes']");
		driverSpend3orMoremonthsNo= By.xpath("//div[contains(text(), 'Does this driver spend 3 or more months out of state each year?')]/following-sibling::div//input[@title='No']");
		driverTookListedAutosYes = By.xpath("//div[contains(text(), 'Does this driver take any autos listed on the policy with him/ her?')]/following-sibling::div//input[@title='Yes']");
		driverTookListedAutosNo = By.xpath("//div[contains(text(), 'Does this driver take any autos listed on the policy with him/ her?')]/following-sibling::div//input[@title='No']");
		vehicleWithStudentAtSchoolYes = By.xpath("//div[contains(text(), 'Vehicle with Student at School?')]/following-sibling::div//input[@title='Yes']");
		vehicleWithStudentAtSchoolNo = By.xpath("//div[contains(text(), 'Vehicle with Student at School?')]/following-sibling::div//input[@title='No']");
		driverTrainingYes = By.xpath("//div[contains(text(), 'Driver Training Course')]/following-sibling::div//input[@title='Yes']");
		driverTrainingNo = By.xpath("//div[contains(text(), 'Driver Training Course')]/following-sibling::div//input[@title='No']");
		driverTrainingDate = By.xpath("//input[contains(@title,'Driver Training Course Completion Date')]");
		accidentPreventionCourseYes = By.xpath("//div[contains(text(), 'Accident Prevention Course')]/following-sibling::div//input[@title='Yes']");
		accidentPreventionCourseNo = By.xpath("//div[contains(text(), 'Accident Prevention Course')]/following-sibling::div//input[@title='No']");
		preventionCourseDate = By.xpath("//input[contains(@title,'Accident Prevention Course Completion Date')|| contains(@title,'Mature Driver Training course completion date')]");
		maturedDriverCourseCompletedYes = By.xpath("//div[contains(text(), 'Mature Driver Training Course completed?')]/following-sibling::div//input[@title='Yes']");
		maturedDriverCourseCompletedNo = By.xpath("//div[contains(text(), 'Mature Driver Training Course completed?')]/following-sibling::div//input[@title='No']");
		
		defensiveDriverCourseCompletedYes = By.xpath("//div[contains(text(), 'Defensive Driver Course Completed?')]/following-sibling::div//input[@title='Yes']");
		defensiveDriverCourseCompletedNo = By.xpath("//div[contains(text(), 'Defensive Driver Course Completed?')]/following-sibling::div//input[@title='No']");
		
		driverImprovementCourseYes = By.xpath("//div[contains(text(), 'Motor Vehicle Driver Improvement Course Completed?')]/following-sibling::div//input[@title='Yes']");
		driverImprovementCourseNo = By.xpath("//div[contains(text(), 'Motor Vehicle Driver Improvement Course Completed?')]/following-sibling::div//input[@title='No']");
		improvementCourseDate = By.xpath("//input[@title='Motor Vehicle Driver Improvement Course Completion Date?']");
		
		driverWithDiffStateYes = By.xpath("//div[contains(text(), 'Is driver assigned to another vehicle insured under a separate PURE policy')]/following-sibling::div//input[@title='Yes']");
		driverWithDiffStateNo = By.xpath("//div[contains(text(), 'Is driver assigned to another vehicle insured under a separate PURE policy')]/following-sibling::div//input[@title='No']");
		
		militaryPersonnalYes = By.xpath("//div[contains(text(), 'Active military personnel?')]/following-sibling::div//input[@title='Yes']");
		militaryPersonnalNo = By.xpath("//div[contains(text(), 'Active military personnel?')]/following-sibling::div//input[@title='No']");
		
		driverTrainigCourseCompletedIn5YrsYes = By.xpath("//div[contains(text(), 'Driver Training Course completed in last 5 years?')]/following-sibling::div//input[@title='Yes']");
		driverTrainigCourseCompletedIn5YrsNo = By.xpath("//div[contains(text(), 'Driver Training Course completed in last 5 years?')]/following-sibling::div//input[@title='No']");
		trainingCourseDate = By.xpath("//input[@title='Driver Training Course Completion Date']");
		
		licensedInOtherStateYes = By.xpath("//div[contains(text(), 'Have you been licensed in another state in the past 2 years?')]/following-sibling::div//input[@title='Yes']");
		licensedInOtherStateNo = By.xpath("//div[contains(text(), 'Have you been licensed in another state in the past 2 years?')]/following-sibling::div//input[@title='No']");
		priorLicensedState = By.xpath("//select[@title='Prior Licensed State']");
		priorDriverLicenseNumber = By.xpath("//input[@title='Prior Driver License Number']");
		
		fullTimeStudentYes = By.xpath("//div[contains(text(), 'Full-Time Student')]/following-sibling::div//input[@title='Yes']");
		fullTimeStudentNo = By.xpath("//div[contains(text(), 'Full-Time Student')]/following-sibling::div//input[@title='No']");
		gradeAverageYes = By.xpath("//div[contains(text(), 'current grade')]/following-sibling::div//input[@title='Yes']");
		gradeAverageNo = By.xpath("//div[contains(text(), 'current grade')]/following-sibling::div//input[@title='No']");
		
		driverLicenseState = By.xpath("//select[@title='License State']");
		licenseNumber = By.xpath("//div[@title='License Number']/following-sibling::div/input");
		occupation = By.xpath("//div[@title='Occupation']/following-sibling::div/input");
		
		firstYearLicensedInUS = By.xpath("//input[contains(@title,'The first year you were licensed')]");
		driverLicenseSuspendedRevokedNo = By.xpath("//div[contains(text(), 'license been suspended or revoked within the previous 3 years?')]/following-sibling::div//input[@title='No']");
		driverLicenseSuspendedRevokedYes = By.xpath("//div[contains(text(), 'license been suspended or revoked within the previous 3 years?')]/following-sibling::div//input[@title='Yes']");
		driverLicenseLapsedYes = By.xpath("//div[contains(text(), 'license lapsed within the previous 3 years?')]/following-sibling::div//input[@title='Yes']");
		driverLicenseLapsedNo = By.xpath("//div[contains(text(), 'license lapsed within the previous 3 years?')]/following-sibling::div//input[@title='No']");
		SR22CertificateInsuranceYes = By.xpath("//div[contains(text(), 'SR-22/ Certificate of Insurance Required?')]/following-sibling::div//input[@title='Yes']");
		SR22CertificateInsuranceNo = By.xpath("//div[contains(text(), 'SR-22/ Certificate of Insurance Required?')]/following-sibling::div//input[@title='No']");
		
		//Add incident
		incidentAddButton = By.xpath("//div[text()='incidents']/..//a[text()='add']");
		source = By.xpath("//select[@title='Source'][@class='unFilledMandatoryField']");
		accidentOrViolation = By.xpath("//select[contains(@title, 'Accident or Violation')][@class='unFilledMandatoryField']");
		accOrConvictionDate = By.xpath("//input[@title='Acc. or Conviction Date'][contains(@class,'unFilledMandatoryField')]");
	}
}
