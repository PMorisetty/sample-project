package com.pure.dragon.personalAuto;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.homeOwners.LocationCoveragePage;
import com.pure.report.CReporter;

public class AutomobilePolicyPageLib extends ActionEngine {
	public AutomobilePolicyPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void selectPureHomeownersPolicyCheckBox(String value) throws Throwable{
		boolean status = checkBoxStatus(AutomobilePolicyPage.pureHomeownersPolicyCheckBox);
		if(value.equalsIgnoreCase("YES")){
			if(!status)
			click(AutomobilePolicyPage.pureHomeownersPolicyCheckBox, "Pure Homeowners Policy - Yes");
		}else{
			if(status)
				click(AutomobilePolicyPage.pureHomeownersPolicyCheckBox, "Pure Homeowners Policy - No");
		}
	}
	public void selectPureExcessPolicyCheckBox(String value) throws Throwable{
		boolean status = checkBoxStatus(AutomobilePolicyPage.pureExcessPolicyCheckBox);
		if(value.equalsIgnoreCase("YES")){
			if(!status)
			click(AutomobilePolicyPage.pureExcessPolicyCheckBox, "Pure Excess Policy - Yes");
		}else{
			if(status)
				click(AutomobilePolicyPage.pureExcessPolicyCheckBox, "Pure Excess Policy - No");
		}
	}
	public void selectPriorCarrierDropDown(String value) throws Throwable{
		selectByVisibleText(AutomobilePolicyPage.priorCarrierDropDown, value, "Prior Carrier Select");
	}
	public void setPremium(String value) throws Throwable{
		type(AutomobilePolicyPage.premiumText, value, "Premium");
	}
	public void selectLapseInCoverageDropDown(String value) throws Throwable{
		selectByVisibleText(AutomobilePolicyPage.lapseInCoverageDropdown, value, "Lapse in Coverage Select");
	}
	public void selectForDropDown(String value) throws Throwable{
		selectByVisibleText(AutomobilePolicyPage.forDropDown, value, "For Select");
	}
}
