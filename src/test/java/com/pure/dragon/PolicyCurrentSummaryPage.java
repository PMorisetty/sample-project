package com.pure.dragon;

import org.openqa.selenium.By;

public class PolicyCurrentSummaryPage {
	static By policyNumberLabel;
	static By docLink;
	static By pdfDocLink;
	static By endorsementPdfDocLink;

	static {
		policyNumberLabel = By.xpath("//div[@title='Policy Number']/..//label");
		docLink = By.cssSelector("a[title='Subjectivity']");
		pdfDocLink = By.xpath("//a[@title='Form Code'][contains(text(), 'Home Owners Package')]");
		endorsementPdfDocLink = By.xpath("//a[@title='Document Link'][contains(text(), 'Home Owners Package')]");
		
	}
}
