package com.pure.dragon.watercraft;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.report.CReporter;

public class NavigationPageLib extends ActionEngine{
	public NavigationPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	public void goToWatercraftCoverpage() throws Throwable{
		click(NavigationPage.watercraftCoverpage, "Watercraft Cover Page Link");
	}
	public void goToSubjectivities() throws Throwable{
		click(NavigationPage.subjectivities, "Subjectivities Link");
	}
	public void goToPreviousClaims() throws Throwable{
		click(NavigationPage.previousClaims, "Previous Claims/MVR Activity Link");
	}
	public void goToVessel(String year, String manufacturer, String model) throws Throwable{
		String vesselName = year + " " + manufacturer + " " + model;
		String locator = this.xpathUpdator(NavigationPage.vesselDescription, vesselName);
		click(By.xpath(locator), vesselName + " - vessel description link");
		//set underwritter referral eligibility w.r.t year 
		if(convertDOBtoAge(year)>15){
			UnderwriterReferralsPageLib.uwReferralEligibilityForWCMorethan15yearsOld=true;
		}
	}
	public void goToVesselUW(String year, String manufacturer, String model) throws Throwable{
		String vesselName = year + " " + manufacturer + " " + model;
		String locator = this.xpathUpdator(NavigationPage.vesselUnderwriting, vesselName);
		click(By.xpath(locator), vesselName + " - vessel underwriting Link");
	}
	public void goToVesselCoverage(String year, String manufacturer, String model) throws Throwable{
		String vesselName = year + " " + manufacturer + " " + model;
		String locator = this.xpathUpdator(NavigationPage.vesselCoverage, vesselName);
		click(By.xpath(locator), vesselName + " - vessel coverage link");
	}
	public void goToVesselManuscript(String year, String manufacturer, String model) throws Throwable{
		String vesselName = year + " " + manufacturer + " " + model;
		String locator = this.xpathUpdator(NavigationPage.vesselManuscript, vesselName);
		click(By.xpath(locator), vesselName + " - vessel manuscript link");
	}
	public void goToVesselManuscriptIndividual(String year, String manufacturer, String model, String title) throws Throwable{
		String vesselName = year + " " + manufacturer + " " + model;
		String locator = this.xpathUpdator(NavigationPage.vesselManuscriptIndividual, vesselName);
		locator = this.xpathUpdator(By.xpath(locator), title, "%");
		click(By.xpath(locator), vesselName + " - " + title + " manuscript link");
	}
	//verify vessel underwriting and coverage present or not w.r.t. length of boat
	public void verifyVesselUWDisplayed(String lengthOfBoat, String vesselYear, String vesselManufacturer, String vesselModel)throws Throwable{
		String vesselDescription = vesselYear + " " + vesselManufacturer + " " + vesselModel;
		String uwXpath = xpathUpdator(NavigationPage.vesselUnderwriting, vesselDescription);
		String covXpath = xpathUpdator(NavigationPage.vesselCoverage, vesselDescription);
		boolean value = (Integer.valueOf(lengthOfBoat) > 26)?true:false;
		verifyElementPresent(By.xpath(uwXpath), vesselDescription + " - vessel underwriting", value);
		verifyElementPresent(By.xpath(covXpath), vesselDescription + " - vessel coverage", value);
	}
}
