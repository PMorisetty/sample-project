package com.pure.dragon.watercraft;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;


import com.pure.accelerators.ActionEngine;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.report.CReporter;

public class PreviousClaimsPageLib extends ActionEngine{
	public PreviousClaimsPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}	
	int noOfLossesForPriorVessels = 0;
	public void selectWatercraftLossHistory(String value)throws Throwable{
		if("YES".equalsIgnoreCase(value))
			click(PreviousClaimsPage.watercraftLossHistoryYes, "Watercraft Loss History - Yes");
		else
			click(PreviousClaimsPage.watercraftLossHistoryNo, "Watercraft Loss History - No");
	}
	public void selectAutoIncidents(String value)throws Throwable{
		if("YES".equalsIgnoreCase(value))
			click(PreviousClaimsPage.autoIncidentsYes, "Auto Incidents - Yes");
		else
			click(PreviousClaimsPage.autoIncidentsNo, "Auto Incidents - No");
	}
	public void selectWatercraftDropdown(String value)throws Throwable{
		selectByVisibleText(PreviousClaimsPage.selectWatercraftDropdown, value, "Select Watercraft");
	}
	public void clickAddWatercraftLossButton()throws Throwable{
		click(PreviousClaimsPage.addWatercraftButton, "Add Loss button");
	}
	
	public void clickAddWatercraftLossPriorVesselButton()throws Throwable{
		click(PreviousClaimsPage.addWatercraftLossPriorVesselButton, "Add Vessel Loss button");
	}
	public void clickAddOperatorLossButton()throws Throwable{
		click(PreviousClaimsPage.addOperatorButton, "Add Loss button");
	}
	public void selectWatercraftSourceDropdown(String waterCraftName, String value)throws Throwable{
		By locator = By.xpath(xpathUpdator(PreviousClaimsPage.sourceDropdown, waterCraftName));
		selectByVisibleText(locator, value, "Select Source");
	}
	public void selectLossTypeDropdown(String waterCraftName,String value)throws Throwable{
		By locator = By.xpath(xpathUpdator(PreviousClaimsPage.lossTypeDropdown, waterCraftName));
		selectByVisibleText(locator, value, "Loss type");
	}
	public void setLossDateText(String waterCraftName,String value)throws Throwable{
		By locator = By.xpath(xpathUpdator(PreviousClaimsPage.lossDateText, waterCraftName));
		type(locator, value, "Loss Date");
		type(locator, Keys.TAB, "Tab key");
		//Underwriting referral eligibility
		UnderwriterReferralsPageLib.priorLossDate=value;
		
	}
	public void setLossAmountText(String waterCraftName,String value)throws Throwable{
		By locator = By.xpath(xpathUpdator(PreviousClaimsPage.lossAmountText, waterCraftName));
		type(locator, value, "Loss Amount");
	}
	public void fillWatercraftLossHistory(Hashtable<String, String> data)throws Throwable{
		if("YES".equalsIgnoreCase(data.get("watercraft_previousLossHistory"))){
			click(PreviousClaimsPage.watercraftLossHistoryYes, "prior watercraft losses - Yes");
			int noOfVessels = Integer.valueOf(data.get("NoOfVessels"));
			for(int count = 1; count<=noOfVessels;count++){
				String temp = data.get("watercraft" + count + "_noOfLosses");
				if(temp != null && temp.trim() != ""){
					int noOfLosses = Integer.valueOf(temp);
					for(int lossCount = 1; lossCount<=noOfLosses;lossCount++){
						String year 		= data.get("watercraft" + count + "_year");
						String manufacturer = data.get("watercraft" + count + "_manufacturer");
						String model 		= data.get("watercraft" + count + "_model");
						String watercraftName = year + " " + manufacturer + " " + model;
						selectWatercraftDropdown(watercraftName);
						clickAddWatercraftLossButton();
						selectWatercraftSourceDropdown(watercraftName	,data.get("watercraft" + count + "_loss" + lossCount + "_source"));
						selectLossTypeDropdown(watercraftName			,data.get("watercraft" + count + "_loss" + lossCount + "_lossType"));
						setLossDateText(watercraftName					,data.get("watercraft" + count + "_loss" + lossCount + "_lossDate"));
						setLossAmountText(watercraftName				,data.get("watercraft" + count + "_loss" + lossCount + "_lossAmount"));
						new CommonPageLib(driver, reporter).clickSaveChangesBtn();
					}
				}
			}
		}else{
			click(PreviousClaimsPage.watercraftLossHistoryNo, "prior watercraft losses - No");
		}
	}
	
	//--------------- Watercraft loss for prior vessel---------------------------------
	public void selectWatercraftForPriorVesselSourceDropdown(String value)throws Throwable{
		By locator = By.xpath(xpathUpdator(PreviousClaimsPage.sourceDropdownPriorVessel,
							(noOfLossesForPriorVessels)+""));
		selectByVisibleText(locator, value, "Prior Vessel Select Source");
	}
	public void selectForPriorVesselLossTypeDropdown(String value)throws Throwable{
		By locator = By.xpath(xpathUpdator(PreviousClaimsPage.lossTypeDropdownPriorVessel,
							(noOfLossesForPriorVessels)+""));
		selectByVisibleText(locator, value, "Prior Vessel Loss type");
	}
	public void setForPriorVesselLossDateText(String value)throws Throwable{
		By locator = By.xpath(xpathUpdator(PreviousClaimsPage.lossDateTextPriorVessel,
							(noOfLossesForPriorVessels)+""));
		type(locator, value, "Prior Vessel Loss Date");
	}
	public void setForPriorVesselLossAmountText(String value)throws Throwable{
		By locator = By.xpath(xpathUpdator(PreviousClaimsPage.lossAmountTextPriorVessel,
							(noOfLossesForPriorVessels)+""));
		type(locator, value, "Prior Vessel Loss Amount");
	}
	public void fillWatercraftLossHistoryForPriorVessels(Hashtable<String, String> data)throws Throwable{
		if("YES".equalsIgnoreCase(data.get("lossesForPriorVessels"))){
			click(PreviousClaimsPage.watercraftLossHistoryForPriorVesselYes, "prior watercraft losses - Yes");			
				String temp = data.get("noOfLossesForPriorVessels");				
				if(temp != null && temp.trim() != ""){
					noOfLossesForPriorVessels = Integer.valueOf(temp);
					for(int lossCountPriorVessel = 1; lossCountPriorVessel<=noOfLossesForPriorVessels;lossCountPriorVessel++){					
						clickAddWatercraftLossPriorVesselButton();
						selectWatercraftForPriorVesselSourceDropdown(data.get("lossForPriorVessel" + lossCountPriorVessel + "_source"));
						selectForPriorVesselLossTypeDropdown		(data.get("lossForPriorVessel" + lossCountPriorVessel + "_lossType"));
						setForPriorVesselLossDateText				(data.get("lossForPriorVessel" + lossCountPriorVessel + "_lossDate"));
						setForPriorVesselLossAmountText				(data.get("lossForPriorVessel" + lossCountPriorVessel + "_lossAmount"));
						new CommonPageLib(driver, reporter).clickSaveChangesBtn();
					}
				}			
		}else{
			click(PreviousClaimsPage.watercraftLossHistoryForPriorVesselNo, "Watercraft losse history of prior vessel - No");
		}
	}
	//---------------------------------------------------------------------------------
	
		
	public void selectOperatorDropdown(String value)throws Throwable{
		selectByVisibleText(PreviousClaimsPage.selectOperatorDropdown, value, "Select Operator");
	}
	public void clickAddIncidentButton()throws Throwable{
		click(PreviousClaimsPage.addOperatorButton, "Add Incident button");
	}
	public void selectIncidentSourceDropdown(String operatorName, String value)throws Throwable{
		By locator = By.xpath(xpathUpdator(PreviousClaimsPage.incidentSourceDropdown, operatorName));
		selectByVisibleText(locator, value, "Select Source");
	}
	public void selectIncidentAccidentDropdown(String operatorName,String value)throws Throwable{
		By locator = By.xpath(xpathUpdator(PreviousClaimsPage.accidentDropdown, operatorName));
		selectByVisibleText(locator, value, "Accident/Violation");
	}
	public void setAccidentDateText(String operatorName,String value)throws Throwable{
		By locator = By.xpath(xpathUpdator(PreviousClaimsPage.accidentDateText, operatorName));
		type(locator, value, "Accident Date");
		type(locator, Keys.TAB, "Tab");
	}

	public void fillAutoIncidents(Hashtable<String, String> data)throws Throwable{
		if("YES".equalsIgnoreCase(data.get("operator_incidentHistory"))){
			click(PreviousClaimsPage.autoIncidentsYes, "prior incidents - Yes");
			int noOfOperators = Integer.valueOf(data.get("NoOfOperators"));
			for(int count = 1; count<=noOfOperators;count++){
				String temp = data.get("operator" + count + "_noOfIncidents");
				if(temp != null && temp.trim() != ""){
					int noOfIncidents = Integer.valueOf(temp);
					String firstName = "";
					String lastName = "";
					for(int incidentCount = 1; incidentCount<=noOfIncidents;incidentCount++){
						if(count == 1){
							firstName = data.get("insuredFirstName");
							lastName = data.get("insuredLastName");
						}else{
							firstName = data.get("operator" + count + "_firstName");
							lastName = data.get("operator" + count + "_lastName");
						}

						String operatorName = firstName + " " + lastName;
						selectOperatorDropdown(operatorName);
						clickAddOperatorLossButton();
						
						String incidentSourceDropdown = data.get("operator" + count + "_incident" + incidentCount + "_source");
						String accidentType= data.get("operator" + count + "_incident" + incidentCount + "_accidentType");						
						String accidentDate=data.get("operator" + count + "_incident" + incidentCount + "_incidentDate");
						
						selectIncidentSourceDropdown(operatorName,incidentSourceDropdown);
						selectIncidentAccidentDropdown(operatorName,accidentType);
						setAccidentDateText(operatorName, accidentDate);
						new CommonPageLib(driver, reporter).clickSaveChangesBtn();
						
						//underwriter referral rule eligibility;
						UnderwriterReferralsPageLib.accidentType=accidentType;
						UnderwriterReferralsPageLib.accidentDate=accidentDate;
					}
					//underwriter referral rule Eligibility
					if(noOfIncidents>=3){
						UnderwriterReferralsPageLib.uwReferralEligibilityForIncidents3AndAbove=true;
					}
				}

			}
		}else{
			click(PreviousClaimsPage.autoIncidentsNo, "prior incidents - No");
		}
	}
}
