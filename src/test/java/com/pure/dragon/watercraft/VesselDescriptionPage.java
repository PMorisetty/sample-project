package com.pure.dragon.watercraft;

import org.openqa.selenium.By;

public class VesselDescriptionPage {
	static By vesselYearText, vesselLengthText, vesselManufacturerText, vesselModelText, vesselHullLimitText, vesselHullIdentificationText;
	static By vesselHullMaterialDropDown, vesselHullDesignDropDown, vesselYearOfLastSurveyText, vesselYearPurchasedText, vesselUsageDropDown;
	static By vesselTypeDropDown, vesselBoatInWaterDropDown, vesselRegisteredStateDropDown, vesselNameText;
	static By vesselHullLimitOverridden, vesselYearOfEngine;
	static By vesselPeriodBoatInWater;
	static By hullmachineryAgreedValue;	

	static By engineNoOfEngines, engineTotalHP, engineFuelType, engineManufacturerText, engineYearBuiltText, engineTypeDropDown, engineMaxDesignSpeed;

	static By berthingDropDown, berthingMarinaOrBuilding, berthingStreet,
	berthingCity, berthingStateDropDown, berthingZip;
	
	static By territoryZone;
	
	static By safetyEquipmentGPSCheckbox, safetyEquipmentDepthSounderCheckbox, safetyEquipmentCOCheckbox, safetyEquipmentBurglarAlarmCheckbox,
	safetyEquipmentShipRadioCheckbox, safetyEquipmentRadarCheckbox;
	

	//waters navigated section
	static By navigationalLimitCheckbox, navigationalLimitText;
	static By coastalInlandWatersCheckbox,coastalInlandWatersCheckbox2, carribeanWatersCheckbox, mediterraneanWatersCheckbox, 
							southEastAlaskaWatersCheckbox, otherWatersCheckbox, otherWatersTextbox;
	static By coastalInlandWatersText, carribeanWatersText, mediterraneanWatersText, southEastAlaskaWatersText,otherWatersText;
	
	//Severe weather plan
	static By seasonalNavigationRestrictionYes,seasonalNavigationRestrictionNo,
	isHurricaneYes, isHurricaneNo,
	planEntailDropdown,overrideGradeDropdown, reviewDateText,
	hurricanePlanBuilding, hurricanePlanStreet, hurricanePlanCity, hurricanePlanState, hurricanePlanZip;
	

	static {
		vesselYearText = By.cssSelector("input[id*='_29650414'][title='Year']");
		vesselLengthText = By.cssSelector("input[id*='_29650414'][title='Length(ft)']");
		vesselManufacturerText = By.cssSelector("input[id*='_29650414'][title='Manufacturer']");
		vesselModelText = By.cssSelector("input[id*='_29650414'][title='Model']");
		vesselHullLimitText = By.cssSelector("input[id*='_30669433']");
		vesselHullLimitOverridden = By.cssSelector("input[id*='_29651814']");
		vesselYearOfEngine = By.cssSelector("input[id*='_29655014'][title='Year of Engine']");
		vesselTypeDropDown = By.cssSelector("select[id*='_29653414']");
		vesselRegisteredStateDropDown = By.cssSelector("select[id*='_29667914']");
		vesselNameText = By.cssSelector("input[id*='_29650414'][title='Vessel Name']");
		vesselYearOfLastSurveyText = By.cssSelector("input[id*='_29650414'][title='Year of last survey']");
		engineNoOfEngines = By.cssSelector("input[id*='_29654814']");
		engineTotalHP = By.cssSelector("input[id*='_29654914']");
		engineFuelType = By.cssSelector("select[id*='_29655314']");
		engineManufacturerText = By.cssSelector("input[id*='_29655114']");
		engineYearBuiltText = By.cssSelector("input[id*='_29655014'][title='Engine Year Built']");
		engineTypeDropDown = By.cssSelector("select[id*='_29655214']");
		engineMaxDesignSpeed = By.cssSelector("select[id*='_29655414']");

		hullmachineryAgreedValue = By.xpath("//input[@title='Hull and Machinery Limit - Agreed Value']");
		
		vesselHullIdentificationText = By.cssSelector("input[id*='_29650414'][title='Hull Identification']");
		vesselHullMaterialDropDown = By.cssSelector("select[id*='_29653814']");
		vesselHullDesignDropDown = By.cssSelector("select[id*='_29653914']");
		vesselYearPurchasedText = By.cssSelector("input[id*='_29650414'][title='Year Purchased']");
		vesselUsageDropDown = By.cssSelector("select[id*='_29654014']");
		vesselPeriodBoatInWater = By.cssSelector("select[id*='_29654214']");

		berthingDropDown = By.cssSelector("select[id*='_29656214']");
		berthingMarinaOrBuilding = By.cssSelector("input[id*='_29664014']");
		berthingStreet = By.cssSelector("input[id*='_29668014']");
		berthingCity = By.cssSelector("input[id*='_29668114']");
		berthingStateDropDown = By.cssSelector("select[id*='_29668214']");
		berthingZip = By.cssSelector("input[id*='_29668314']");

		territoryZone = By.cssSelector("input[title= 'Territory Zone']");
		
		navigationalLimitCheckbox = By.xpath("//div[@class='headerLabel'][contains(text(), 'navigational limits warranted and provided to include')]/../..//input[@type='checkbox']");
		coastalInlandWatersCheckbox = By.cssSelector("input[id*='_30707233']");
		coastalInlandWatersCheckbox2 = By.cssSelector("input[id*='_30707333']");
		carribeanWatersCheckbox = By.cssSelector("input[id*='_30693733']");
		mediterraneanWatersCheckbox = By.cssSelector("input[id*='_30694233']");
		southEastAlaskaWatersCheckbox = By.cssSelector("input[id*='_30694333']");
		otherWatersCheckbox = By.cssSelector("input[id*='_30694433']");
		otherWatersTextbox = By.cssSelector("textarea[id*='_18489633']");
		
		navigationalLimitText = By.xpath("//div[@class='headerLabel'][contains(text(), 'navigational limits warranted and provided to include')]/../..//div[@class='rightAlignCheckLabel']");
		/*coastalInlandWatersText = By.cssSelector("div[id*='_30693633'][class='rightAlignCheckLabel']");*/		
		coastalInlandWatersText = By.cssSelector("div[id*='_30707233'][class='rightAlignCheckLabel']");
		carribeanWatersText = By.cssSelector("div[id*='_18488633'][class*='label']");
		mediterraneanWatersText = By.cssSelector("div[id*='_18488533'][class*='label']");
		southEastAlaskaWatersText = By.cssSelector("div[id*='_30694333'][class*='label']");
		otherWatersText = By.xpath("//div[contains(@id,'_30694433')]//br.");
		
		seasonalNavigationRestrictionYes = By.cssSelector("input[name*='_29660814'][title='Yes']");
		seasonalNavigationRestrictionNo = By.cssSelector("input[name*='_29660814'][title='No']");
		isHurricaneYes = By.cssSelector("input[name*='_29654314'][title='Yes']");
		isHurricaneNo = By.cssSelector("input[name*='_29654314'][title='No']");
		planEntailDropdown = By.cssSelector("select[name*='_29654414'][title='What does your plan entail?']");
		hurricanePlanBuilding = By.cssSelector("input[name*='_29668914'][title='Marina/Building']");
		hurricanePlanStreet = By.cssSelector("input[name*='_29669014'][title='Street']");
		hurricanePlanCity = By.cssSelector("input[name*='_29669114'][title='City']");
		hurricanePlanState = By.cssSelector("select[name*='_29669214'][title='State']");
		hurricanePlanZip = By.cssSelector("input[name*='_29669314'][title='Zip']");
		overrideGradeDropdown = By.cssSelector("select[title='Override Hurr Plan Grade']");
		reviewDateText = By.cssSelector("input[title='Review Date']");
	}

}
