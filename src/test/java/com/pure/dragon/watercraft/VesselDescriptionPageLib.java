package com.pure.dragon.watercraft;

import java.util.Calendar;
import java.util.Hashtable;

import org.apache.commons.io.input.SwappedDataInputStream;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.dragon.homeOwners.QuoteDetailsPage;
import com.pure.report.CReporter;

public class VesselDescriptionPageLib extends ActionEngine{
	boolean swapedCoastalInland = false;
	public VesselDescriptionPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public boolean UWInternationalEligibility=false;
	//For Vessel Details
	public void setVesselYear(String value) throws Throwable{
		type(VesselDescriptionPage.vesselYearText, value, "Vessel Year");
	}	
	public void setVesselLength(String value) throws Throwable{
		type(VesselDescriptionPage.vesselLengthText, value, "Vessel Length");
	}
	public void setVesselManufacturer(String value) throws Throwable{
		type(VesselDescriptionPage.vesselManufacturerText, value, "Vessel Manufacturer");
	}	
	public void setVesselModel(String value) throws Throwable{
		type(VesselDescriptionPage.vesselModelText, value, "Vessel Model");
	}
	public void setVesselHullLimit(String value) throws Throwable{
		type(VesselDescriptionPage.vesselHullLimitText, value, "Vessel Hull Limit");
	}	
	private void setHullMachineryLimitAgreedValue(String value) throws Throwable {
		if(value != null && value.trim() != "")
			type(VesselDescriptionPage.hullmachineryAgreedValue, value, "Hull machinery Agreed value");
	}
	public void setVesselHullLimitOverridden(String value) throws Throwable{
		if(value != null && value.trim() != "")
			type(VesselDescriptionPage.vesselHullLimitOverridden, value, "Vessel Hull Limit Overriden");
	}	
	public void setVesselYearOfEngine(String value) throws Throwable{
		if(value != null)
			type(VesselDescriptionPage.vesselYearOfEngine, value, "Vessel Year of Engine");
	}
	public void selectVesselType(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.vesselTypeDropDown, value, "Vessel Type");
	}
	public void selectVesselRegisteredState(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.vesselRegisteredStateDropDown, value, "Vessel Registered State");
		//Underwriter referral eligibility
		UnderwriterReferralsPageLib.brokersLicensedState=value;
	}
	public void setVesselName(String value) throws Throwable{
		type(VesselDescriptionPage.vesselNameText, value, "Vessel Name");
	}
	public void setVesselYearOfLastSurvey(String value) throws Throwable{
		type(VesselDescriptionPage.vesselYearOfLastSurveyText, value, "Vessel Year of last Survey");
		//Eligibility for underwriter referral
		UnderwriterReferralsPageLib.yearOfLastSurvey = value;
	}	
	public void setVesselHullIdentification(String value) throws Throwable{
		type(VesselDescriptionPage.vesselHullIdentificationText, value, "Vessel Hull Identification");
	}
	public void setVesselYearPurchased(String value) throws Throwable{
		type(VesselDescriptionPage.vesselYearPurchasedText, value, "Vessel Year Purchased");
		//underwriter referral eligibility
		UnderwriterReferralsPageLib.yearOfPurchage=value;
	}
	public void selectVesselHullMaterialDropDown(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.vesselHullMaterialDropDown, value, "Vessel Hull Material");
	}
	public void selectVesselHullDesignDropDown(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.vesselHullDesignDropDown, value, "Vessel Hull Design");
	}
	public void selectVesselUsage(String value) throws Throwable{		
		selectByVisibleText(VesselDescriptionPage.vesselUsageDropDown, value, "Vessel Usage");
		// Underwriter referral eligibility
		if(value.replace(" ", "").equalsIgnoreCase("Antique/Collectors")){
			UnderwriterReferralsPageLib.uwReferralEligibilityForUsageAntiqueCollectors=true;
		}
	}
	public void selectVesselPeriodBoatInWater(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.vesselPeriodBoatInWater, value, "Period Boat in Water");
	}

	//For Engine details
	public void setEngineNoOfEngines(String value) throws Throwable{
		type(VesselDescriptionPage.engineNoOfEngines, value, "No of Engines");
	}
	public void setEngineTotalHP(String value) throws Throwable{
		type(VesselDescriptionPage.engineTotalHP, value, "Engine Total HP");
	}
	public void setEngineManufacturer(String value) throws Throwable{
		type(VesselDescriptionPage.engineManufacturerText, value, "Engine Manufacturer");
	}
	public void setEngineYearBuilt(String value) throws Throwable{
		type(VesselDescriptionPage.engineYearBuiltText, value, "Engine Year Built");
	}
	public void selectEngineFuelType(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.engineFuelType, value, "Engine Fuel Type");
	}
	public void selectEngineType(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.engineTypeDropDown, value, "Engine Type");
	}
	public void selectEngineMaxDesignSpeed(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.engineMaxDesignSpeed, value, "Engine Max Design Speed");
		//Underwriter referral eligibility 
		if(value.contains("61")){
			UnderwriterReferralsPageLib.eligibilityMaxEngineSpeed=true;
		}
	}

	//Berthing Location
	public void selectBerthing(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.berthingDropDown, value, "Berthing Location");
	}
	public void setBerthingMarinaOrBuilding(String value) throws Throwable{
		type(VesselDescriptionPage.berthingMarinaOrBuilding, value, "Berthing Marina or Building");
	}
	public void setBerthingStreet(String value) throws Throwable{
		type(VesselDescriptionPage.berthingStreet, value, "Berthing Street");
	}
	public void setBerthingCity(String value) throws Throwable{
		type(VesselDescriptionPage.berthingCity, value, "Berthing City");
	}
	public void selectBerthingState(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.berthingStateDropDown, value, "Berthing State");
	}
	public void setBerthingZip(String value) throws Throwable{
		type(VesselDescriptionPage.berthingZip, value, "Berthing Zip");
		type(VesselDescriptionPage.berthingZip, Keys.TAB, "Tab key");
	}

	//Verification for under 27 boat
	public void verifyUnder27(Hashtable<String, String> data, int counter) throws Throwable{
		//verify elements not present
		verifyElementPresent(VesselDescriptionPage.vesselHullIdentificationText, "Vessel Hull Identification", true);//as per new UI specs
		verifyElementPresent(VesselDescriptionPage.vesselHullMaterialDropDown, "Vessel Hull Material", false);
		verifyElementPresent(VesselDescriptionPage.vesselHullDesignDropDown, "Vessel Hull Design", false);
		verifyElementPresent(VesselDescriptionPage.vesselPeriodBoatInWater, "Period Boat in Water", false);
		verifyElementPresent(VesselDescriptionPage.engineYearBuiltText, "Engine Year Built", false);

		verifyElementPresent(VesselDescriptionPage.engineNoOfEngines, "Number of Engines", true);
		/*//remove state names
		String validStates = "FL, AL, MS, TX, MO, GA, CT, IA, KS, AZ, VA, AR, WI, OH, KY, OR, ND, NM, NE, ME";
		if(validStates.contains(data.get("state"))){
			verifyElementPresent(VesselDescriptionPage.engineNoOfEngines, "Number of Engines", true);
		}else{
			verifyElementPresent(VesselDescriptionPage.engineNoOfEngines, "Number of Engines", false);
		}*/

		verifyElementPresent(VesselDescriptionPage.engineTypeDropDown, "Engine Type", false);
		verifyElementPresent(VesselDescriptionPage.engineMaxDesignSpeed, "Engine Max Design Speed", false);
		//verify elements present
		verifyElementPresent(VesselDescriptionPage.vesselYearText, "Vessel Year", true);
		verifyElementPresent(VesselDescriptionPage.vesselLengthText, "Vessel Length", true);
		verifyElementPresent(VesselDescriptionPage.vesselManufacturerText, "Vessel Manufacturer", true);
		verifyElementPresent(VesselDescriptionPage.vesselModelText, "Vessel Model", true);
		switch (data.get("watercraft" + counter + "_policyType")){
		case "Hull and Liability": 
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitText, "Vessel Hull Limit", true);
			verifyElementEnable(VesselDescriptionPage.vesselHullLimitText, "Vessel Hull Limit", false);
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitOverridden, "Vessel Hull Limit Overridden", true);
			verifyElementEnable(VesselDescriptionPage.vesselHullLimitOverridden, "Vessel Hull Limit Overridden", true);
			break;
		case"Liability Only":
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitText, "Vessel Hull Limit", false);
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitOverridden, "Vessel Hull Limit Overridden", false);
			break;
		case"Property Only":
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitText, "Vessel Hull Limit", true);
			verifyElementEnable(VesselDescriptionPage.vesselHullLimitText, "Vessel Hull Limit", false);
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitOverridden, "Vessel Hull Limit Overridden", true);
			verifyElementEnable(VesselDescriptionPage.vesselHullLimitOverridden, "Vessel Hull Limit Overridden", true);
			break;
		}
		verifyElementPresent(VesselDescriptionPage.vesselYearOfEngine, "Vessel Year of Engine", true);
		verifyElementPresent(VesselDescriptionPage.vesselTypeDropDown, "Vessel Type", true);
		if(Integer.valueOf(data.get("watercraft" + counter + "_year")) <= 1969)
			verifyElementEnable(VesselDescriptionPage.vesselUsageDropDown, "Vessel Usage", true);
		else{
			verifyElementEnable(VesselDescriptionPage.vesselUsageDropDown, "Vessel Usage", false);
			String value = getTextOfSelectedOption(VesselDescriptionPage.vesselUsageDropDown, "Vessel Usage");
			assertTrue("Regular Use".equalsIgnoreCase(value), "Default value of Usage type");
		}
		verifyElementPresent(VesselDescriptionPage.vesselRegisteredStateDropDown, "Vessel Registered State", true);
		verifyElementPresent(VesselDescriptionPage.vesselNameText, "Vessel Name", true);
		verifyElementPresent(VesselDescriptionPage.vesselYearOfLastSurveyText, "Vessel Year of Last Survey", true);
		verifyElementPresent(VesselDescriptionPage.engineTotalHP, "Engine Total HP", true);
		verifyElementPresent(VesselDescriptionPage.engineFuelType, "Engine Fuel", true);
		//verify disabled elements
		verifyElementEnable(VesselDescriptionPage.vesselYearText, "Vessel Year", false);
		verifyElementEnable(VesselDescriptionPage.vesselLengthText, "Vessel Length", false);
		verifyElementEnable(VesselDescriptionPage.vesselManufacturerText, "Vessel Manufacturer", false);
		verifyElementEnable(VesselDescriptionPage.vesselModelText, "Vessel Model", false);

		//verify enabled elements

		verifyElementEnable(VesselDescriptionPage.vesselYearOfEngine, "Vessel Year of Engine", true);
		verifyElementEnable(VesselDescriptionPage.vesselTypeDropDown, "Vessel Type", true);
		verifyElementEnable(VesselDescriptionPage.vesselRegisteredStateDropDown, "Vessel Registered State", true);
		verifyElementEnable(VesselDescriptionPage.vesselNameText, "Vessel Name", true);
		verifyElementEnable(VesselDescriptionPage.vesselYearOfLastSurveyText, "Vessel Year of Last Survey", true);
		verifyElementEnable(VesselDescriptionPage.engineTotalHP, "Engine Total HP", true);
		verifyElementEnable(VesselDescriptionPage.engineFuelType, "Engine Fuel", true);
	}
	//Verification for above 27 boat
	public void verifyAbove27(Hashtable<String, String> data, int counter, String role) throws Throwable{
		//verify elements not present
		verifyElementPresent(VesselDescriptionPage.vesselYearOfEngine, "Vessel Year of Engine", false);
		//verify element present
		verifyElementPresent(VesselDescriptionPage.vesselYearText, "Vessel Year", true);
		verifyElementPresent(VesselDescriptionPage.vesselLengthText, "Vessel Length", true);
		verifyElementPresent(VesselDescriptionPage.vesselManufacturerText, "Vessel Manufacturer", true);
		verifyElementPresent(VesselDescriptionPage.vesselModelText, "Vessel Model", true);
		switch (data.get("watercraft" + counter + "_policyType")){
		case "Hull and Liability": 
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitText, "Vessel Hull Limit", true);
			verifyElementEnable(VesselDescriptionPage.vesselHullLimitText, "Vessel Hull Limit", false);
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitOverridden, "Vessel Hull Limit Overridden", true);
			verifyElementEnable(VesselDescriptionPage.vesselHullLimitOverridden, "Vessel Hull Limit Overridden", true);
			break;
		case "Liability Only":
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitText, "Vessel Hull Limit", false);
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitOverridden, "Vessel Hull Limit Overridden", false);
			break;
		case "Property Only":
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitText, "Vessel Hull Limit", true);
			verifyElementEnable(VesselDescriptionPage.vesselHullLimitText, "Vessel Hull Limit", false);
			verifyElementPresent(VesselDescriptionPage.vesselHullLimitOverridden, "Vessel Hull Limit Overridden", true);
			verifyElementEnable(VesselDescriptionPage.vesselHullLimitOverridden, "Vessel Hull Limit Overridden", true);
			break;
		}

		verifyElementPresent(VesselDescriptionPage.vesselHullMaterialDropDown, "Vessel Hull Material", true);
		verifyElementPresent(VesselDescriptionPage.vesselHullDesignDropDown, "Vessel Hull Design", true);
		verifyElementPresent(VesselDescriptionPage.vesselYearOfLastSurveyText, "Vessel Year of Last Survey", true);
		verifyElementPresent(VesselDescriptionPage.vesselYearPurchasedText, "Vessel Year Purchased", true);
		verifyElementPresent(VesselDescriptionPage.vesselUsageDropDown, "Vessel Usage", true);
		verifyElementPresent(VesselDescriptionPage.vesselTypeDropDown, "Vessel Type", true);
		if(role.equalsIgnoreCase("UW"))
			verifyElementPresent(VesselDescriptionPage.vesselPeriodBoatInWater, "Period Boat in Water", true);
		else if(role.equalsIgnoreCase("Agent"))
			verifyElementPresent(VesselDescriptionPage.vesselPeriodBoatInWater, "Period Boat in Water", false);
		verifyElementPresent(VesselDescriptionPage.vesselRegisteredStateDropDown, "Vessel Registered State", true);
		verifyElementPresent(VesselDescriptionPage.vesselNameText, "Vessel Name", true);

		verifyElementPresent(VesselDescriptionPage.engineNoOfEngines, "Number of Engines", true);
		verifyElementPresent(VesselDescriptionPage.engineTotalHP, "Engine Total HP", true);
		verifyElementPresent(VesselDescriptionPage.engineManufacturerText, "Engine Manufacturer", true);
		verifyElementPresent(VesselDescriptionPage.engineYearBuiltText, "Engine Year Built", true);
		verifyElementPresent(VesselDescriptionPage.engineTypeDropDown, "Engine Type", true);
		verifyElementPresent(VesselDescriptionPage.engineFuelType, "Engine Fuel Type", true);
		verifyElementPresent(VesselDescriptionPage.engineMaxDesignSpeed, "Engine Max Design Speed", true);
		//verify enabled elements
		verifyElementEnable(VesselDescriptionPage.vesselYearText, "Vessel Year", false);
		verifyElementEnable(VesselDescriptionPage.vesselLengthText, "Vessel Length", false);
		verifyElementEnable(VesselDescriptionPage.vesselManufacturerText, "Vessel Manufacturer", false);
		verifyElementEnable(VesselDescriptionPage.vesselModelText, "Vessel Model", false);
		verifyElementEnable(VesselDescriptionPage.vesselHullMaterialDropDown, "Vessel Hull Material", true);
		verifyElementEnable(VesselDescriptionPage.vesselHullDesignDropDown, "Vessel Hull Design", true);
		verifyElementEnable(VesselDescriptionPage.vesselYearOfLastSurveyText, "Vessel Year of Last Survey", true);
		verifyElementEnable(VesselDescriptionPage.vesselYearPurchasedText, "Vessel Year Purchased", true);
		if(Integer.valueOf(data.get("watercraft" + counter + "_year")) <= 1969)
			verifyElementEnable(VesselDescriptionPage.vesselUsageDropDown, "Vessel Usage", true);
		else{
			verifyElementEnable(VesselDescriptionPage.vesselUsageDropDown, "Vessel Usage", false);
			String value = getTextOfSelectedOption(VesselDescriptionPage.vesselUsageDropDown, "Vessel Usage");
			assertTrue("Regular Use".equalsIgnoreCase(value), "Default value of Usage type");
		}
		verifyElementEnable(VesselDescriptionPage.vesselTypeDropDown, "Vessel Type", true);
		verifyElementEnable(VesselDescriptionPage.vesselPeriodBoatInWater, "Period Boat in Water", false);
		verifyElementEnable(VesselDescriptionPage.vesselRegisteredStateDropDown, "Vessel Registered State", true);
		verifyElementEnable(VesselDescriptionPage.vesselNameText, "Vessel Name", true);

		verifyElementEnable(VesselDescriptionPage.engineNoOfEngines, "Number of Engines", true);
		verifyElementEnable(VesselDescriptionPage.engineTotalHP, "Engine Total HP", true);
		verifyElementEnable(VesselDescriptionPage.engineManufacturerText, "Engine Manufacturer", true);
		verifyElementEnable(VesselDescriptionPage.engineYearBuiltText, "Engine Year Built", true);
		verifyElementEnable(VesselDescriptionPage.engineTypeDropDown, "Engine Type", true);
		verifyElementEnable(VesselDescriptionPage.engineFuelType, "Engine Fuel Type", true);
		verifyElementEnable(VesselDescriptionPage.engineMaxDesignSpeed, "Engine Max Design Speed", true);
	}

	public void clickCoastalInlandWatersCheckbox(String value)throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.coastalInlandWatersCheckbox);
		if("YES".equalsIgnoreCase(value) && !status){
			click(VesselDescriptionPage.coastalInlandWatersCheckbox, "Checkbox checked - Coastal and Inland Waters");
			UWInternationalEligibility=true;
		}else if("NO".equalsIgnoreCase(value) && status){
			click(VesselDescriptionPage.coastalInlandWatersCheckbox, "Checkbox unchecked - Coastal and Inland Waters");
		}
	}
	public void clickCoastalInlandWatersCheckbox2(String value)throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.coastalInlandWatersCheckbox2);
		if("YES".equalsIgnoreCase(value) && !status){
			click(VesselDescriptionPage.coastalInlandWatersCheckbox2, "Checkbox checked - Coastal and Inland Waters");
		}else if("NO".equalsIgnoreCase(value) && status){
			click(VesselDescriptionPage.coastalInlandWatersCheckbox2, "Checkbox unchecked - Coastal and Inland Waters");
		}
	}
	public void clickCarribeanWatersCheckbox(String value)throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.carribeanWatersCheckbox);
		if("YES".equalsIgnoreCase(value) && !status){
			click(VesselDescriptionPage.carribeanWatersCheckbox, "Checkbox checked - Carribean Waters");
			//UW referral elegibility
			UWInternationalEligibility=true;
		}else if("NO".equalsIgnoreCase(value) && status){
			click(VesselDescriptionPage.carribeanWatersCheckbox, "Checkbox unchecked - Carribean Waters");
		}
	}
	public void clickMediterraneanWatersCheckbox(String value)throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.mediterraneanWatersCheckbox);
		if("YES".equalsIgnoreCase(value) && !status){
			click(VesselDescriptionPage.mediterraneanWatersCheckbox, "Checkbox checked - European rivers and inland waters");
			UWInternationalEligibility=true;
		}else if("NO".equalsIgnoreCase(value) && status){
			click(VesselDescriptionPage.mediterraneanWatersCheckbox, "Checkbox unchecked - European rivers and inland waters");
		}
	}
	public void clickSouthEastAlaskaWatersCheckbox(String value)throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.southEastAlaskaWatersCheckbox);
		if("YES".equalsIgnoreCase(value) && !status){
			click(VesselDescriptionPage.southEastAlaskaWatersCheckbox, "Checkbox checked - Southeast Alaska Waters");
			UWInternationalEligibility=true;
		}else if("NO".equalsIgnoreCase(value) && status){
			click(VesselDescriptionPage.southEastAlaskaWatersCheckbox, "Checkbox unchecked - Southeast Alaska Waters");
		}
	}
	public void clickOtherWatersCheckbox(String value, String otherWatersDetail)throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.otherWatersCheckbox);
		if("YES".equalsIgnoreCase(value) && !status){
			click(VesselDescriptionPage.otherWatersCheckbox, "Checkbox checked - Other waters");
			type(VesselDescriptionPage.otherWatersTextbox, otherWatersDetail, "Other waters");
		}else if("NO".equalsIgnoreCase(value) && status){
			click(VesselDescriptionPage.otherWatersCheckbox, "Checkbox unchecked - Other waters");
		}
	}

	public void verifyWaterNavigatesCheckboxText(String role)throws Throwable{
		String defaultTerritoryZone = "1,2,3,4,13";
		String territoryZone = getAttributeByValue(VesselDescriptionPage.territoryZone, "territoryZone value").trim();
		String text;
		if(!defaultTerritoryZone.contains(territoryZone)){
			text = "Coastal and inland waters of the continental United States and Canada , from Newfoundland Island, to Brownsville, TX, and from Imperial Beach, CA to Cape Scott, BC, including Bermuda, the Bahamas, Turks & Caicos, but warranted not south and not navigating coastal waters which are south of Cumberland Island, GA and east of Browsville, TX between June 30th and November 1st.";
			String text1 = "Coastal and inland waters of the continental United States and canada, from Newfoundland Island, to Brownsville, TX, and from Imperial Beach, CA to Cape Scott, BC , including Bermuda, the Bahamas, Turks & Caicos.";
			assertTextStringContains(getText(VesselDescriptionPage.coastalInlandWatersText, "Coastal and Inland Waters"), text1);
		}else{
			text = "Coastal and inland waters of the continental United States and canada, from Newfoundland Island, to Brownsville, TX, and from Imperial Beach, CA to Cape Scott, BC , including Bermuda, the Bahamas, Turks & Caicos.";
			swapedCoastalInland = true;
		}
		//assertTextStringContains(getText(VesselDescriptionPage.navigationalLimitText, "Navigational limits warranted"), text);
		text = "Caribbean Sea not South of Trinidad and Tobago, excluding the coastal waters of Cuba, Venezuala, Haiti & Colombia.";
		assertTextStringContains(getText(VesselDescriptionPage.carribeanWatersText, "Carribean Waters"), text);
		text = "European coastal and inland tributary waters of Mediterranean Sea not east of 35 degrees East Longitude, but excluding the coastal waters of Tunisia, Algeria, Libya, and Egypt.";
		assertTextStringContains(getText(VesselDescriptionPage.mediterraneanWatersText, "European rivers and inland waters"), text);		
		text = "Coastal and inland waters of British Colombia and Southeast Alaska not North of Hooper Bay, Alaska, and warranted not North of 57 degrees North latitude between September 1st and June 15th.";
		assertTextStringContains(getText(VesselDescriptionPage.southEastAlaskaWatersText, "Southeast Alaska Waters"), text);
		if(role.equalsIgnoreCase("UW")){
			text = "Warranted and confined to the use and navigation of coastal and inland waters and tributaries thereto of the:";
			assertTextStringContains(getText(VesselDescriptionPage.otherWatersText, "Other waters").trim(), text);
		}
	}

	public void fillVesselEngineDetails(Hashtable<String, String> data, int counter) throws Throwable{
		setVesselHullLimitOverridden(data.get("watercraft" + counter + "_hullLimitOverride"));
		setVesselYearOfEngine(data.get("watercraft" + counter + "_yearOfEngine"));
		selectVesselType(data.get("watercraft" + counter + "_type"));
		selectVesselRegisteredState(data.get("watercraft" + counter + "_registeredState"));
		setVesselName(data.get("watercraft" + counter + "_name"));
		setVesselYearOfLastSurvey(data.get("watercraft" + counter + "_yearOfLastSurvey"));
		setEngineTotalHP(data.get("watercraft" + counter + "_totalHP"));
		selectEngineFuelType(data.get("watercraft" + counter + "_fuelType"));
		setEngineNoOfEngines(data.get("watercraft" + counter + "_noOfEngines"));
	}
	public void fillVesselPriceLookUp(Hashtable<String, String> data, int counter) throws Throwable{		
		if(!"Liability Only".equalsIgnoreCase(data.get("watercraft" + counter + "_policyType"))){
			setHullMachineryLimitAgreedValue(data.get("watercraft" + counter + "_hullmachineryAgreedValue"));		
		}
	}

	public void fillVesselDetails(Hashtable<String, String> data, int counter) throws Throwable{
		setVesselHullLimitOverridden(data.get("watercraft" + counter + "_hullLimitOverride"));
		selectVesselHullMaterialDropDown(data.get("watercraft" + counter + "_hullMaterial"));
		selectVesselHullDesignDropDown(data.get("watercraft" + counter + "_hullDesign"));
		setVesselYearOfLastSurvey(data.get("watercraft" + counter + "_yearOfLastSurvey"));
		setVesselYearPurchased(data.get("watercraft" + counter + "_yearPurchased"));
		if(Integer.valueOf(data.get("watercraft" + counter + "_year")) <= 1969)
			selectVesselUsage(data.get("watercraft" + counter + "_usage"));
		selectVesselType(data.get("watercraft" + counter + "_type"));
		selectVesselRegisteredState(data.get("watercraft" + counter + "_registeredState"));
		setVesselName(data.get("watercraft" + counter + "_name"));
	}
	public void fillEngineDetails(Hashtable<String, String> data, int counter) throws Throwable{
		setEngineNoOfEngines(data.get("watercraft" + counter + "_noOfEngines"));
		setEngineTotalHP(data.get("watercraft" + counter + "_totalHP"));
		setEngineManufacturer(data.get("watercraft" + counter + "_engineManufacturer"));
		setEngineYearBuilt(data.get("watercraft" + counter + "_engineYearBuilt"));
		selectEngineType(data.get("watercraft" + counter + "_engineType"));
		selectEngineFuelType(data.get("watercraft" + counter + "_fuelType"));
		selectEngineMaxDesignSpeed(data.get("watercraft" + counter + "_engineMaxDesignSpeed"));
	}
	public void fillBerthingLocation(Hashtable<String, String> data, int counter) throws Throwable{
		selectBerthing(data.get("watercraft" + counter + "_berthing"));
		setBerthingMarinaOrBuilding(data.get("watercraft" + counter + "_building"));
		setBerthingStreet(data.get("watercraft" + counter + "_street"));
		setBerthingCity(data.get("watercraft" + counter + "_city"));
		selectBerthingState(data.get("watercraft" + counter + "_state"));
		setBerthingZip(data.get("watercraft" + counter + "_zip"));
		waitUntilJQueryReady();	
		//String validStates = "FL, AL, MS, LA, TX";
		//if(validStates.contains(data.get("watercraft" + counter + "_state")))	
		//fillSeverWeatherPlanDetails(data, counter);
	}
	public void fillAdditionalWatersNavigated(Hashtable<String, String> data, int counter, String role) throws Throwable{		
		/*String validStates = "FL, AL, MS, LA, TX";
//		if(!validStates.contains(data.get("watercraft" + counter + "_state")))
		String defaultTerritoryZone = "1,2,3,4,13";
		String territoryZone = getAttributeByValue(VesselDescriptionPage.territoryZone, "territoryZone value").trim();
		String text;*/
		boolean status1 = false;
		boolean status2 = false;
		try{
			boolean visibilityStatus = isVisibleOnly(VesselDescriptionPage.coastalInlandWatersCheckbox, "CoastalInlandWatersCheckbox");
			if (visibilityStatus && this.driver.findElement(VesselDescriptionPage.coastalInlandWatersCheckbox).isEnabled()) {
				status1 = true;
			}
		}catch(Exception ex){

		}
		if(status1)
			clickCoastalInlandWatersCheckbox(data.get("watercraft" + counter + "_coastalInlandWaters"));		
		try{
			boolean visibilityStatus = isVisibleOnly(VesselDescriptionPage.coastalInlandWatersCheckbox2, "CoastalInlandWatersCheckbox");
			if (visibilityStatus && this.driver.findElement(VesselDescriptionPage.coastalInlandWatersCheckbox2).isEnabled()) {
				status2 = true;
			}
		}catch(Exception ex){

		}
		if(status2)
			clickCoastalInlandWatersCheckbox2(data.get("watercraft" + counter + "_coastalInlandWaters"));
		clickCarribeanWatersCheckbox(data.get("watercraft" + counter + "_caribbeanWaters"));
		clickMediterraneanWatersCheckbox(data.get("watercraft" + counter + "_mediterraneanWaters1"));
		clickSouthEastAlaskaWatersCheckbox(data.get("watercraft" + counter + "_southEastAlaskaWaters"));
		verifyWaterNavigatesCheckboxText(role);
		if(role.equalsIgnoreCase("UW"))
			clickOtherWatersCheckbox(data.get("watercraft" + counter + "_otherWaters"), data.get("watercraft" + counter + "_otherWatersDesc"));
		
		//Underwriter referral eligibility
		if(UWInternationalEligibility){
			UnderwriterReferralsPageLib.uwReferralEligibilityForAdditionalWaterNavigated = true;
		}
	}
	public void fillSeverWeatherPlanDetails(Hashtable<String, String> data, int counter) throws Throwable{
		/*		String seasonalNavigationRestriction = data.get("watercraft" + counter + "_seasonalNavigationRestriction");
		selectSeasonalNavigationRestriction(seasonalNavigationRestriction);		
		if("NO".equalsIgnoreCase(seasonalNavigationRestriction)){ 
			String isHurricane = data.get("watercraft" + counter + "_isHurricane");
			String HurricaneState = data.get("watercraft" + counter + "_isHurricane_State");
			selectIsHurricane(HurricaneState, isHurricane);
		 */
		if(swapedCoastalInland){
			new CommonPageLib(driver, reporter).clickSaveChangesBtn();
		}
		String PrimaryBerthingTerritory="5";
		if(!("X-Wind".equalsIgnoreCase(data.get("watercraft" + counter + "_coverage_HurricaneValue"))) &&
				(checkBoxStatus(VesselDescriptionPage.coastalInlandWatersCheckbox) ||
						(swapedCoastalInland)||
						PrimaryBerthingTerritory.equalsIgnoreCase(getAttributeByValue(VesselDescriptionPage.territoryZone, "territoryZone value").trim()))){

			selectPlanEntail(data.get("watercraft" + counter + "_isHurricane_PlanEntail"));
			if(
					("YES".equalsIgnoreCase(data.get("watercraft" + counter + "_HurricaneGradeOverride")))
					&& data.get("usernameAgent")==null){
				selectHurricaneOverrideGrade(data.get("watercraft" + counter + "_HurricaneGradeOverrideValue"));
				setHurricaneReviewDate(data.get("watercraft" + counter + "_HurricaneGradeOverrideReviewDate"));
				//Eligibility for UW referral
				UnderwriterReferralsPageLib.uwReferralEligibilityNoHurricanSeverPlan = true;
			}
			setHurricaneBuilding(data.get("watercraft" + counter + "_isHurricane_Building"));
			setHurricaneStreet(data.get("watercraft" + counter + "_isHurricane_Street"));
			setHurricaneCity(data.get("watercraft" + counter + "_isHurricane_City"));
			selectHurricaneState(data.get("watercraft" + counter + "_isHurricane_State"));
			setHurricaneZip(data.get("watercraft" + counter + "_isHurricane_Zip"));
		}
		new CommonPageLib(driver, reporter).clickSaveChangesBtn();
	}	
	public void selectHurricaneOverrideGrade(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.overrideGradeDropdown, value, "Grade override");
		waitUntilJQueryReady();
		//Underwriter refferal eligibilty
		UnderwriterReferralsPageLib.hurricaneGradePlan=value;
	}
	public void selectHurricaneOverrideGradeIfVisible(String value) throws Throwable{
		if(isVisibleOnly(VesselDescriptionPage.overrideGradeDropdown, "Grade override")){
			selectByVisibleText(VesselDescriptionPage.overrideGradeDropdown, value, "Grade override");
			waitUntilJQueryReady();
		}
	}
	public void setHurricaneReviewDate(String value) throws Throwable{
		type(VesselDescriptionPage.reviewDateText, value, "Review date");
	}
	public void setHurricaneReviewDateIfVisible(String value) throws Throwable{
		if(isVisibleOnly(VesselDescriptionPage.reviewDateText, "Review date"))
			type(VesselDescriptionPage.reviewDateText, value, "Review date");
	}
	public void selectSeasonalNavigationRestriction(String value) throws Throwable{		
		if(value.equalsIgnoreCase("YES")){
			click(VesselDescriptionPage.seasonalNavigationRestrictionYes, "Seasonal Navigation Restriction - Yes");
		}else if(value.equalsIgnoreCase("NO")){
			click(VesselDescriptionPage.seasonalNavigationRestrictionNo, "Seasonal Navigation Restriction - No");
		}		
	}
	public void selectIsHurricane(String state, String value) throws Throwable{		
		if(value.equalsIgnoreCase("YES")){
			click(VesselDescriptionPage.isHurricaneYes, "Hurricane / Severe weather plan - Yes");
		}else if(value.equalsIgnoreCase("NO")){
			click(VesselDescriptionPage.isHurricaneNo, "Hurricane / Severe weather plan - No");
		}

	}
	public void selectPlanEntail(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.planEntailDropdown, value, "Plan entail");
	}
	public void setHurricaneBuilding(String value) throws Throwable{
		type(VesselDescriptionPage.hurricanePlanBuilding, value, "Hurricane Building");
	}
	public void setHurricaneStreet(String value) throws Throwable{
		type(VesselDescriptionPage.hurricanePlanStreet, value, "Hurricane Street");
	}
	public void setHurricaneCity(String value) throws Throwable{
		type(VesselDescriptionPage.hurricanePlanCity, value, "Hurricane City");
	}
	public void selectHurricaneState(String value) throws Throwable{
		selectByVisibleText(VesselDescriptionPage.hurricanePlanState, value, "Hurricane State");
	}
	public void setHurricaneZip(String value) throws Throwable{
		type(VesselDescriptionPage.hurricanePlanZip, value, "Hurricane Zip");
		type(VesselDescriptionPage.hurricanePlanZip, Keys.TAB, "Tab key");
		waitUntilJQueryReady();
	}

	public void selectSafetyEquipmentGPSCheckbox(String value) throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.safetyEquipmentGPSCheckbox);
		if(!status && value.equalsIgnoreCase("YES")){
			click(VesselDescriptionPage.safetyEquipmentGPSCheckbox, "Safety Equipment GPS - Checked");
		}else if(status && value.equalsIgnoreCase("NO")){
			click(VesselDescriptionPage.safetyEquipmentGPSCheckbox, "Safety Equipment GPS - Unchecked");
		}
	}
	public void selectSafetyEquipmentDepthSounderCheckbox(String value) throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.safetyEquipmentDepthSounderCheckbox);
		if(!status && value.equalsIgnoreCase("YES")){
			click(VesselDescriptionPage.safetyEquipmentDepthSounderCheckbox, "Safety Equipment DepthSounder - Checked");
		}else if(status && value.equalsIgnoreCase("NO")){
			click(VesselDescriptionPage.safetyEquipmentDepthSounderCheckbox, "Safety Equipment DepthSounder - Unchecked");
		}
	}
	public void selectSafetyEquipmentCOCheckbox(String value) throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.safetyEquipmentCOCheckbox);
		if(!status && value.equalsIgnoreCase("YES")){
			click(VesselDescriptionPage.safetyEquipmentCOCheckbox, "Safety Equipment CO - Checked");
		}else if(status && value.equalsIgnoreCase("NO")){
			click(VesselDescriptionPage.safetyEquipmentCOCheckbox, "Safety Equipment CO - Unchecked");
		}
	}
	public void selectSafetyEquipmentBurglarAlarmCheckbox(String value) throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.safetyEquipmentBurglarAlarmCheckbox);
		if(!status && value.equalsIgnoreCase("YES")){
			click(VesselDescriptionPage.safetyEquipmentBurglarAlarmCheckbox, "Safety Equipment BurglarAlarm - Checked");
		}else if(status && value.equalsIgnoreCase("NO")){
			click(VesselDescriptionPage.safetyEquipmentBurglarAlarmCheckbox, "Safety Equipment BurglarAlarm - Unchecked");
		}
	}
	public void selectSafetyEquipmentShipRadioCheckbox(String value) throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.safetyEquipmentShipRadioCheckbox);
		if(!status && value.equalsIgnoreCase("YES")){
			click(VesselDescriptionPage.safetyEquipmentShipRadioCheckbox, "Safety Equipment ShipRadio - Checked");
		}else if(status && value.equalsIgnoreCase("NO")){
			click(VesselDescriptionPage.safetyEquipmentShipRadioCheckbox, "Safety Equipment ShipRadio - Unchecked");
		}
	}
	public void selectSafetyEquipmentRadarCheckbox(String value) throws Throwable{
		boolean status = checkBoxStatus(VesselDescriptionPage.safetyEquipmentRadarCheckbox);
		if(!status && value.equalsIgnoreCase("YES")){
			click(VesselDescriptionPage.safetyEquipmentRadarCheckbox, "Safety Equipment Radar - Checked");
		}else if(status && value.equalsIgnoreCase("NO")){
			click(VesselDescriptionPage.safetyEquipmentRadarCheckbox, "Safety Equipment Radar - Unchecked");
		}
	}
	public void fillSafetyEquipmentDetails(Hashtable<String, String> data, int counter)throws Throwable{
		selectSafetyEquipmentGPSCheckbox(data.get("watercraft" + counter + "_safetyEquipment_GPS"));
		selectSafetyEquipmentDepthSounderCheckbox(data.get("watercraft" + counter + "_safetyEquipment_DepthSounder"));
		selectSafetyEquipmentCOCheckbox(data.get("watercraft" + counter + "_safetyEquipment_CO"));
		selectSafetyEquipmentBurglarAlarmCheckbox(data.get("watercraft" + counter + "_safetyEquipment_BurglarAlarm"));
		selectSafetyEquipmentShipRadioCheckbox(data.get("watercraft" + counter + "_safetyEquipment_ShipRadio"));
		selectSafetyEquipmentRadarCheckbox(data.get("watercraft" + counter + "_safetyEquipment_Radar"));
	}
}
