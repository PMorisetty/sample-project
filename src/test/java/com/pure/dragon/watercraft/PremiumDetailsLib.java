package com.pure.dragon.watercraft;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.PremiumDetailsPage;
import com.pure.report.CReporter;

public class PremiumDetailsLib extends ActionEngine{
//	RaterBook rbook;
	public PremiumDetailsLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
//		rbook = new RaterBook(System.getProperty("user.dir") + "/TestData/WC_Rating/WC_Manual_Rater.xlsx", "Manual Rater");
	}
	public void clickReturnToPremiumSummaryPage() throws Throwable{
		click(PremiumDetailsPage.returnToSummaryPremiumPage,"Return To Summary Premium Page");
	}
	public void verifyBasePremium(Hashtable<String, String> data) throws Throwable{
		String watercraftName = data.get("watercraft1_year") + " " + data.get("watercraft1_manufacturer") + " " + data.get("watercraft1_model");
		Double HullAdjustedBaseRate = Double.valueOf(data.get("Hull Adjusted Base Rate").replace(",", ""));
		Double app_HullAdjustedBaseRate = Double.valueOf(getFactor(watercraftName, "Hull Base Premium", "ADJUSTED BASE RATE").replace(",", ""));
		reporter.SuccessReport("HullAdjustedBaseRate", HullAdjustedBaseRate.toString());
		reporter.SuccessReport("app_HullAdjustedBaseRate:", app_HullAdjustedBaseRate.toString());
//		assertTrue(HullAdjustedBaseRate == app_HullAdjustedBaseRate, "HullAdjustedBaseRate premium");
		
		Double PIAdjustedBaseRate = Double.valueOf(data.get("P&I Adjusted Base Rate").replace(",", ""));
		Double app_PIAdjustedBaseRate = Double.valueOf(getFactor(watercraftName, "P&I Base Premium", "ADJUSTED BASE RATE").replace(",", ""));
		reporter.SuccessReport("PIAdjustedBaseRate", PIAdjustedBaseRate.toString());
		reporter.SuccessReport("app_PIAdjustedBaseRate:", app_PIAdjustedBaseRate.toString());
//		assertTrue(PIAdjustedBaseRate == app_PIAdjustedBaseRate, "PIAdjustedBaseRate premium");
		
		Double HullNonWindPremium = Double.valueOf(data.get("Hull Non-Wind Premium").replace(",", ""));
		Double app_HullNonWindPremium = Double.valueOf(getFactor(watercraftName, "Hull Non-Wind Base Premium", "NON WIND BASE PREMIUM").replace(",", ""));
		reporter.SuccessReport("HullNonWindPremium", HullNonWindPremium.toString());
		reporter.SuccessReport("app_HullNonWindPremium:", app_HullNonWindPremium.toString());
//		assertTrue(HullNonWindPremium == app_HullNonWindPremium, "HullNonWindPremium premium");
		
		Double PINonWindPremium = Double.valueOf(data.get("P&I Non-Wind Premium").replace(",", ""));
		Double app_PINonWindPremium = Double.valueOf(getFactor(watercraftName ,"P&I Non-Wind Base Premium", "NON WIND BASE PREMIUM").replace(",", ""));
		reporter.SuccessReport("PINonWindPremium", PINonWindPremium.toString());
		reporter.SuccessReport("app_PINonWindPremium:", app_PINonWindPremium.toString());
//		assertTrue(PINonWindPremium == app_PINonWindPremium, "PINonWindPremium premium");
		
		Double VesselBasePremium = Double.valueOf(data.get("Vessel Base Premium").replace(",", ""));
		Double app_hullWindBasePremium = Double.valueOf(getFactor(watercraftName, "Hull Wind Base Premium", "WIND BASE PREMIUM").replace(",", ""));
		Double app_vesselBasePremium = app_HullNonWindPremium + app_PINonWindPremium + app_hullWindBasePremium;
		reporter.SuccessReport("VesselBasePremium", VesselBasePremium.toString());
		reporter.SuccessReport("app_VesselBasePremium:", app_vesselBasePremium.toString());
//		assertTrue(VesselBasePremium == app_VesselBasePremium, "VesselBasePremium premium");
		
		/*Double OptionalCoveragesPremium = Double.valueOf(data.get("Optional Coverages Premium").replace(",", ""));
		Double app_OptionalCoveragesPremium = Double.valueOf(getFactor(watercraftName ,"Grand Total Premium", "Final Base Premium").replace(",", ""));
		reporter.SuccessReport("OptionalCoveragesPremium", OptionalCoveragesPremium.toString());
		reporter.SuccessReport("app_OptionalCoveragesPremium:", app_OptionalCoveragesPremium.toString());
		assertTrue(OptionalCoveragesPremium == app_OptionalCoveragesPremium, "OptionalCoveragesPremium premium");
		*/
		Double appPremium = Double.valueOf(getFactor("","Grand Total Premium", "Final Base Premium").replace(",", ""));
		reporter.SuccessReport("Grand Total Premium", appPremium.toString());
	}
	@SuppressWarnings("finally")
	public String getFactor(String ratedObject, String coverageName, String factorName) throws Throwable{
		String factor = "//div[contains(text(), '$ratedObject')]/../..//div[text() = '$coverageName']/../..//div[contains(text(), '$factorName')]/../../td[7]/div";
		factor = factor.replace("$ratedObject", ratedObject);
		factor = factor.replace("$coverageName", coverageName);
		factor = factor.replace("$factorName", factorName);
		By factorLocator = By.xpath(factor);
		String premium="0.0";
		try{
//			premium = getText(factorLocator, ratedObject +" " + coverageName +" " + factorName);
			if(driver.findElement(factorLocator) != null)
				premium = driver.findElement(factorLocator).getText().trim();
		}catch(Exception ex){
			
		}finally{
			return premium;
		}
	}
}
