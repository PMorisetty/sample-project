package com.pure.dragon.watercraft;

import org.openqa.selenium.By;

public class VesselUnderwritingPage {
	//select coverage section
	static By coverageProtectionIndemnityDropDown,
	coverageMedicalExpOverrideCheckbox,
	coverageMedicalExpOverrideText,
	coverageAOP_deductibleOverrideCheckbox,
	coverageUninsuredBoatersDropDown,
	coverageHurricane_deductibleOverrideCheckbox,
	coverageAOP_DeductibleDropDown,
	coverageHurricane_DeductibleDropDown,
	coverageAOP_DeductibleOverrideDropDown,
	coverageHurricane_DeductibleOverrideDropDown,
	coverageProtectionIndemnityText,
	coverageUninsuredBoatersText;
	//additional coverage section
	static By additionalCoveragePersonalEffects,
	additionalCoveragePersonalEffectsOverrideText,
	additionalCoverageIncreasedTrailerLimit,
	additionalCoverageEmergencyExpense,
	additionalCoverageEmergencyExpenseOverrideText,
	additionalCoverageEmergencyExpenseOverrideCheckbox,
	additionalCoveragePersonalEffectsOverrideCheckbox,
	additionalCoverageTendersYES,additionalCoverageTendersNO,
	additionalCoverageTrailerYES,additionalCoverageTrailerNO,
	additionalCoverageTender_ValueText,additionalCoverageTender_ManufacturerText, additionalCoverageTender_Length,
	additionalCoverageTrailer_ValueText, additionalCoverageTrailer_ManufacturerText, additionalCoverageTrailer_Length,
	additionalCoverageTrailer_YearText, additionalCoverageTrailer_SerialNumberText;
	static By additionalCoverageBlock;
	
	//vessel underwriting section
	static By vesselUnderWritingDetailsBusinessUseYES,
	vesselUnderWritingDetailsBusinessUseNO,
	vesselUnderWritingDetailsRacingUseYES,
	vesselUnderWritingDetailsForSaleNO,
	vesselUnderWritingDetailsForSaleYES,
	vesselUnderWritingDetailsRacingUseNO,
	vesselUnderWritingDetailsWaterSkiingUseYES,
	vesselUnderWritingDetailsWaterSkiingUseNO,
	vesselUnderWritingDetailsPreviousDamageYES,
	vesselUnderWritingDetailsPreviousDamageNO,
	vesselUnderWritingDetailsEmployedCrewYES,
	vesselUnderWritingDetailsEmployedCrewNO,
	vesselUnderWritingDetailsExistingDamageYES,
	vesselUnderWritingDetailsExistingDamageNO,
	vesselUnderWritingDetailsEmployeedCaptainYES,
	vesselUnderWritingDetailsEmployeedCaptainNO,
	vesselUnderWritingDetailsPreviousDamageText,
	vesselUnderWritingDetailsExistingDamageText,
	vesselUnderWritingDetailsNoofCrewText,
	vesselUnderWritingDetailsFullTimeCrewText,
	vesselUnderWritingDetailsPartTimeCrewText;
	
	//optional coverages section
	static By optCoverage_WarPI_YES, optCoverage_WarPI_NO,
	optCoverage_WarHullYES, optCoverage_WarHullNO,
	optCoverage_FineArtsYES, optCoverage_FineArtsNO,
	optCoverage_CaptainWarrantyYES,
	optCoverage_CaptainWarrantyNO,
	optCoverage_WarrantyBreachYES,
	optCoverage_WarrantyBreachNO,
	optCoverage_OperatorWarrantyYES,
	optCoverage_OperatorWarrantyNO,
	optCoverage_OperatorExclusionYES,
	optCoverage_OperatorExclusionNO,
	optCoverage_ACVforMachineryYES,
	optCoverage_ACVforMachineryNO,
	optCoverage_CharterCoverageYES,
	optCoverage_CharterCoverageNO,
	optCoverage_MachineryExclusionYES,
	optCoverage_MachineryExclusionNO,
	optCoverage_TowingDeductibleYES,
	optCoverage_TowingDeductibleNO,
	optCoverage_TheftDeductibleYES,
	optCoverage_TheftDeductibleNO,
	optCoverage_FishingGearYES,
	optCoverage_FishingGearNO,
	optCoverage_AdditionalTendersYES,
	optCoverage_AdditionalTendersNO;

	static By optCoverage_WarPI_Limit, optCoverage_WarPI_Rate;
	static By optCoverage_WarHull_Limit, optCoverage_WarHull_Rate;
	static By optCoverage_FineArts_Limit, optCoverage_FineArts_Rate;
	static By optCoverage_FishingGear_Limit, optCoverage_FishingGearRate;
	static By optCoverage_WarrantyBreach_Limit, optCoverage_WarrantyBreach_Rate;
	static By optCoverage_NameOfCaptain;
	static By optCoverage_NamedOperatorWarranty;
	static By optCoverage_NamedOperatorExclusion;
	static By optCoverage_TowingDeductible_ded;
	static By optCoverage_TheftDeductible_ded;
	static By optCoverage_AdditionalTenders_manufacturer,
	optCoverage_AdditionalTenders_length,
	optCoverage_AdditionalTenders_value,
	optCoverage_AdditionalTenders_ded,
	optCoverage_WarPI_Rate_Label, optCoverage_WarHull_Rate_Label, optCoverage_FineArts_Rate_Label, optCoverage_WarrantyBreach_Rate_Label;

	//Operator experience
	static By operatorExp_PrimaryOperator,
	operatorExp_3yearExpYes, operatorExp_3yearExpNo,
	operatorExp_USCoastGuardAcademyCheckbox,
	operatorExp_LicensedCaptainCheckbox,
	operatorExp_USPowerSquadronsCheckbox,
	operatorExp_OtherCertificateCheckbox,
	operatorExp_OtherCertificateText;
	
	//Optional for quote, required for bind
	static By vesselTitleInNameOf,ownership,additionalInterestADDbtn,
	additionalInterestDropDown,additionalOwnerName,additionalOwnerAddress,
	additionalOwnerCity,additionalOwnerStateDropDown,additionalOwnerZip;

	//Agent message for select coverage additional options
	static By selectCoverageAgentMessage;

	static {

		coverageProtectionIndemnityDropDown = By.cssSelector("select[id*='_29660414']");
		coverageUninsuredBoatersDropDown = By.cssSelector("select[id*='_29660614']");
		coverageAOP_DeductibleDropDown = By.cssSelector("select[title='AOP Deductible']");
		coverageHurricane_DeductibleDropDown = By.xpath("//select[@title='Hurricane Deductible' and not(contains(@style,'none'))]");
		coverageMedicalExpOverrideCheckbox = By.cssSelector("input[name*='_30689333']");
		coverageMedicalExpOverrideText = By.cssSelector("input[name*='_30689433'][title='Medical Expenses']");
		coverageAOP_deductibleOverrideCheckbox = By.cssSelector("input[name*='_30689733']");
		coverageHurricane_deductibleOverrideCheckbox = By.cssSelector("input[name*='_30689833']");
		coverageAOP_DeductibleOverrideDropDown = By.cssSelector("select[name*='_30689633']");
		coverageHurricane_DeductibleOverrideDropDown = By.cssSelector("select[name*='_30692533']");
		coverageProtectionIndemnityText = By.cssSelector("input[name*='_30684433']");
		coverageUninsuredBoatersText = By.cssSelector("input[name*='_30690033']");		

		additionalCoveragePersonalEffects = By.cssSelector("select[name*='_29675514']");
		additionalCoverageIncreasedTrailerLimit = By.cssSelector("input[name*='_30692733']");
		additionalCoverageEmergencyExpense = By.cssSelector("select[name*='_29660514']");
		additionalCoverageEmergencyExpenseOverrideCheckbox = By.cssSelector("input[name*='_30690133']");
		additionalCoveragePersonalEffectsOverrideCheckbox = By.cssSelector("input[name*='_30672833']");
		additionalCoverageTendersYES = By.cssSelector("input[name*='_29661014'][title='Yes']");
		additionalCoverageTendersNO = By.cssSelector("input[name*='_29661014'][title='Yes']");
		additionalCoverageTrailerYES = By.cssSelector("input[name*='_29661114'][title='Yes']");
		additionalCoverageTrailerNO = By.cssSelector("input[name*='_29661114'][title='No']");
		additionalCoveragePersonalEffectsOverrideText = By.cssSelector("input[name*='_30681333'][title='Personal Effects']");
		additionalCoverageEmergencyExpenseOverrideText = By.cssSelector("input[name*='_30690233'][title='Emergency Expense']");
		additionalCoverageTender_ValueText = By.cssSelector("input[name*='_29662314'][title='Value']");
		additionalCoverageTender_ManufacturerText = By.cssSelector("input[name*='_29662414'][title='Manufacturer']");
		additionalCoverageTender_Length = By.cssSelector("input[name*='_29662514'][title='Length(ft)']");
		additionalCoverageTrailer_ValueText = By.cssSelector("input[title='Total Trailer Limit']");
		additionalCoverageTrailer_ManufacturerText = By.cssSelector("input[name*='_29662814'][title='Manufacturer']");
		additionalCoverageTrailer_Length = By.cssSelector("input[name*='_29662914'][title='Length(ft)']");
		additionalCoverageTrailer_YearText = By.cssSelector("input[name*='_29663014'][title='Year']");
		additionalCoverageTrailer_SerialNumberText = By.cssSelector("input[name*='_29663114'][title='Serial Number']");

		vesselUnderWritingDetailsBusinessUseYES = By.cssSelector("input[name*='_29652414'][title='Yes']");
		vesselUnderWritingDetailsBusinessUseNO = By.cssSelector("input[name*='_29652414'][title='No']");
		vesselUnderWritingDetailsRacingUseYES = By.cssSelector("input[name*='_29652614'][title='Yes']");
		vesselUnderWritingDetailsRacingUseNO = By.cssSelector("input[name*='_29652614'][title='No']");
		vesselUnderWritingDetailsWaterSkiingUseYES = By.cssSelector("input[name*='_29652714'][title='Yes']");
		vesselUnderWritingDetailsWaterSkiingUseNO = By.cssSelector("input[name*='_29652714'][title='No']");
		vesselUnderWritingDetailsPreviousDamageYES = By.cssSelector("input[name*='_29652814'][title='Yes']");
		vesselUnderWritingDetailsPreviousDamageNO = By.cssSelector("input[name*='_29652814'][title='No']");
		vesselUnderWritingDetailsEmployedCrewYES = By.cssSelector("input[name*='_30688433'][title='Yes']");
		vesselUnderWritingDetailsEmployedCrewNO = By.cssSelector("input[name*='_30688433'][title='No']");
		vesselUnderWritingDetailsExistingDamageYES = By.cssSelector("input[name*='_29652914'][title='Yes']");
		vesselUnderWritingDetailsExistingDamageNO = By.cssSelector("input[name*='_29652914'][title='No']");
		vesselUnderWritingDetailsEmployeedCaptainYES = By.cssSelector("input[name*='_30691233'][title='Yes']");
		vesselUnderWritingDetailsEmployeedCaptainNO = By.cssSelector("input[name*='_30691233'][title='No']");
		vesselUnderWritingDetailsPreviousDamageText = By.cssSelector("textarea[name*='_29653014'][title='Please describe previous damage']");
		vesselUnderWritingDetailsExistingDamageText = By.cssSelector("textarea[name*='_29653114'][title='Please describe existing damage']");
		vesselUnderWritingDetailsNoofCrewText = By.cssSelector("input[name*='_30691633'][title='Vessel Underwriting - Small Boats - Total number of crew']");
		vesselUnderWritingDetailsFullTimeCrewText = By.cssSelector("input[name*='_30688533'][title*='Number of full-time crew']");
		vesselUnderWritingDetailsPartTimeCrewText = By.cssSelector("input[name*='_30688633'][title*='Number of part-time crew']");
		vesselUnderWritingDetailsForSaleNO = By.xpath("//div[@title='Is the vessel currently for sale?']/..//input[@title='No']");
		vesselUnderWritingDetailsForSaleYES = By.xpath("//div[@title='Is the vessel currently for sale?']/..//input[@title='Yes']");
		
		vesselUnderWritingDetailsForSaleNO = By.xpath("//div[@title='Is the vessel currently for sale?']/..//input[@title='No']");
		vesselUnderWritingDetailsForSaleYES = By.xpath("//div[@title='Is the vessel currently for sale?']/..//input[@title='Yes']");
		
		optCoverage_WarPI_YES = By.cssSelector("input[name*='_29661214'][title='Yes']");
		optCoverage_WarPI_NO = By.cssSelector("input[name*='_29661214'][title='No']");
		optCoverage_WarPI_Limit = By.cssSelector("input[name*='_30691733']");
		optCoverage_WarPI_Rate = By.cssSelector("input[name*='_30690333']");
		optCoverage_WarHullYES = By.cssSelector("input[name*='_29661314'][title='Yes']");
		optCoverage_WarHullNO = By.cssSelector("input[name*='_29661314'][title='No']");
		optCoverage_WarHull_Limit = By.cssSelector("input[name*='_30691833']");
		optCoverage_WarHull_Rate = By.cssSelector("input[name*='_30690833']");
		optCoverage_FineArtsYES = By.cssSelector("input[name*='_29661414'][title='Yes']");
		optCoverage_FineArtsNO = By.cssSelector("input[name*='_29661414'][title='No']");
		optCoverage_FineArts_Limit = By.cssSelector("input[name*='_29663314']");
		optCoverage_FineArts_Rate = By.cssSelector("input[name*='_30690933']");
		optCoverage_WarrantyBreachYES = By.cssSelector("input[name*='_29661714'][title='Yes']");
		optCoverage_WarrantyBreachNO = By.cssSelector("input[name*='_29661714'][title='No']");
		optCoverage_WarrantyBreach_Limit = By.cssSelector("input[name*='_30691133']");
		optCoverage_WarrantyBreach_Rate = By.cssSelector("input[name*='_30691033']");

		optCoverage_WarPI_Rate_Label = By.xpath("//input[contains(@name,'_30690333')]/../..//b");
		optCoverage_WarHull_Rate_Label = By.xpath("//input[contains(@name,'_30690833')]/../..//b");
		optCoverage_FineArts_Rate_Label = By.xpath("//input[contains(@name,'_30690933')]/../..//b");
		optCoverage_WarrantyBreach_Rate_Label = By.xpath("//input[contains(@name,'_30691033')]/../..//b");
		
		optCoverage_CaptainWarrantyYES = By.cssSelector("input[name*='_29661614'][title='Yes']");
		optCoverage_CaptainWarrantyNO = By.cssSelector("input[name*='_29661614'][title='No']");
		optCoverage_NameOfCaptain = By.cssSelector("input[name*='_30691933']");

		optCoverage_OperatorWarrantyYES = By.cssSelector("input[name*='_29661914'][title='Yes']");
		optCoverage_OperatorWarrantyNO = By.cssSelector("input[name*='_29661914'][title='No']");
		optCoverage_NamedOperatorWarranty = By.cssSelector("input[name*='_30692033']");

		optCoverage_OperatorExclusionYES = By.cssSelector("input[name*='_29662014'][title='Yes']");
		optCoverage_OperatorExclusionNO = By.cssSelector("input[name*='_29662014'][title='No']");
		optCoverage_NamedOperatorExclusion = By.cssSelector("input[name*='_30692233']");
		optCoverage_ACVforMachineryYES = By.cssSelector("input[name*='_29662114'][title='Yes']");
		optCoverage_ACVforMachineryNO = By.cssSelector("input[name*='_29662114'][title='No']");

		optCoverage_CharterCoverageYES = By.cssSelector("input[name*='_29661814'][title='Yes']");
		optCoverage_CharterCoverageNO = By.cssSelector("input[name*='_29661814'][title='No']");

		optCoverage_MachineryExclusionYES = By.cssSelector("input[name*='_30690433'][title='Yes']");
		optCoverage_MachineryExclusionNO = By.cssSelector("input[name*='_30690433'][title='No']");
		
		optCoverage_TowingDeductibleYES = By.cssSelector("input[name*='_30690633'][title='Yes']");
		optCoverage_TowingDeductibleNO = By.cssSelector("input[name*='_30690633'][title='No']");
		optCoverage_TowingDeductible_ded = By.cssSelector("select[name*='_30690733']");

		optCoverage_TheftDeductibleYES = By.cssSelector("input[name*='_30672733'][title='Yes']");
		optCoverage_TheftDeductibleNO = By.cssSelector("input[name*='_30672733'][title='No']");
		optCoverage_TheftDeductible_ded = By.cssSelector("select[name*='_30672933']");

		optCoverage_FishingGearYES = By.cssSelector("input[name*='_30690533'][title='Yes']");
		optCoverage_FishingGearNO = By.cssSelector("input[name*='_30690533'][title='No']");
		optCoverage_FishingGear_Limit = By.cssSelector("input[name*='_30692133']");

		operatorExp_PrimaryOperator = By.cssSelector("select[name*='_29672514']");
		operatorExp_3yearExpYes = By.cssSelector("input[name*='_30691333'][title='Yes']");
		operatorExp_3yearExpNo = By.cssSelector("input[name*='_30691333'][title='No']");
		operatorExp_USCoastGuardAcademyCheckbox = By.xpath("//div[@title='USCGA']/..//input[@type='checkbox']");
		operatorExp_LicensedCaptainCheckbox = By.xpath("//div[@title='Licensed Captain']/..//input[@type='checkbox']");
		operatorExp_USPowerSquadronsCheckbox = By.xpath("//div[@title='USPS']/..//input[@type='checkbox']");
		operatorExp_OtherCertificateCheckbox = By.xpath("//div[@title='Other']/..//input[@type='checkbox']");
		operatorExp_OtherCertificateText = By.cssSelector("input[title='Other']");
		
		vesselTitleInNameOf = By.cssSelector("select[id*='_29677114']");
		ownership = By.cssSelector("select[id*='_29677214']");
		additionalInterestADDbtn = By.xpath("//div[text()='loss payee/ additional insured/ additional owners']/..//a[@title='Add another item.']");
		additionalInterestDropDown = By.xpath("(//select[@title='Interest Type'])[$]");
		additionalOwnerName = By.xpath("(//input[contains(@id,'_29669514')])[$]");
		additionalOwnerAddress = By.xpath("(//input[contains(@id,'_29669614')])[$]");
		additionalOwnerCity = By.xpath("(//input[contains(@id,'_29669714')])[$]");
		additionalOwnerStateDropDown = By.xpath("(//select[contains(@id,'_29669814')])[$]");
		additionalOwnerZip = By.xpath("(//input[contains(@id,'_29669914')])[$]");
		
		
		

		selectCoverageAgentMessage = By.xpath("//div[contains(text(), 'Please contact underwriting for Additional Options.')]");
		additionalCoverageBlock = By.xpath("//div[@class='headerLabel'][contains(text(), 'additional coverage')]/../..");
		
		optCoverage_AdditionalTendersYES = By.cssSelector("input[name*='_30711033'][title='Yes']");
		optCoverage_AdditionalTendersNO = By.cssSelector("input[name*='_30711033'][title='No']");
		optCoverage_AdditionalTenders_manufacturer = By.cssSelector("input[title='Manufacturer'][type='text']");
		optCoverage_AdditionalTenders_length = By.cssSelector("input[title='Length'][type='text']");
		optCoverage_AdditionalTenders_value = By.cssSelector("input[title='Value'][type='text']");
		optCoverage_AdditionalTenders_ded = By.cssSelector("select[name$='_30716233']");
	}

}
