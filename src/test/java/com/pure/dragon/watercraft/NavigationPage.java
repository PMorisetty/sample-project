package com.pure.dragon.watercraft;

import org.openqa.selenium.By;

public class NavigationPage {
	//a[@title='2010 honda honda']/..//a[@title='Vessel Underwriting']
	//a[@title='2010 honda honda']/..//a[@title='Vessel Coverage']
	static By watercraftCoverpage, subjectivities, previousClaims;
	static By vesselDescription, vesselUnderwriting, vesselCoverage, vesselManuscript, vesselManuscriptIndividual;
	
	static{
		watercraftCoverpage = By.xpath("//div[@id='accountsColumn']//a[@title='Watercraft Policy']");
		previousClaims = By.xpath("//div[@id='accountsColumn']//a[@title='Previous Claims/MVR Activity']");
		subjectivities = By.xpath("//div[@id='accountsColumn']//a[@title='Subjectivities']");
		vesselDescription = By.xpath("//div[@id='accountsColumn']//a[@title='$']");
		vesselUnderwriting = By.xpath("//div[@id='accountsColumn']//a[@title='$']/..//a[@title='Vessel Underwriting']");
		vesselCoverage = By.xpath("//div[@id='accountsColumn']//a[@title='$']/..//a[@title='Vessel Coverage']");
		vesselManuscript = By.xpath("//div[@id='accountsColumn']//a[@title='$']/..//a[@title='Manuscript Endorsements']");
		vesselManuscriptIndividual = By.xpath("//div[@id='accountsColumn']//a[@title='$']/..//a[@title='%']");
	}
	
}
