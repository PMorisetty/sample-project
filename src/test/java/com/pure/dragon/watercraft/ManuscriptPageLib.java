package com.pure.dragon.watercraft;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.report.CReporter;

public class ManuscriptPageLib extends ActionEngine{
	public ManuscriptPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	//Manuscript details
	public void selectManuscriptTypeDropdown(String value) throws Throwable{
		selectByVisibleText(ManuscriptPage.manuscriptTypeDropdown, value, "Manuscript type");
	}
	public void setManuscriptTitleText(String value) throws Throwable{
		type(ManuscriptPage.manuscriptTitleText, value, "Manuscript title");
	}
	public void setManuscriptPremiumText(String value) throws Throwable{
		type(ManuscriptPage.manuscriptPremiumText, value, "Manuscript annual manual premium");
	}
	public void selectManuscriptApplyToDropdown(String value) throws Throwable{
		selectByVisibleText(ManuscriptPage.manuscriptApplyToDropdown, value, "Apply to");
	}
	public void clickAddManuscript() throws Throwable{
		click(ManuscriptPage.addManuscriptButton, "Add Manuscript");
	}
	public void selectLifeTimePremiumAdjustment(String value, String tempAdjustment) throws Throwable{
		if("YES".equalsIgnoreCase(value))
			click(ManuscriptPage.lifeTimePremiumAdjustmentYes, "LifeTimePremiumAdjustment - Yes");
		else{
			click(ManuscriptPage.lifeTimePremiumAdjustmentNo, "LifeTimePremiumAdjustment - No");
			if("YES".equalsIgnoreCase(tempAdjustment))
				click(ManuscriptPage.temporaryAdjustmentCheckbox, "Temporary adjustment");
		}
	}
	public void setEndorsementText(String value) throws Throwable{
		type(ManuscriptPage.endrosementText, value, "Manuscript Endorsement text");
	}
	public void fillManuscriptDetails(Hashtable<String, String> data, int counter)throws Throwable{
		selectManuscriptTypeDropdown(data.get("watercraft" + counter + "_manuscriptType"));
		setManuscriptTitleText(data.get("watercraft" + counter + "_manuscriptTitle"));
		setManuscriptPremiumText(data.get("watercraft" + counter + "_manuscriptPremium"));
		selectManuscriptApplyToDropdown(data.get("watercraft" + counter + "_manuscriptApplyTo"));
		new CommonPageLib(this.driver, this.reporter).clickSaveChangesBtn();
	}
	public void fillManuscriptIndividualDetails(Hashtable<String, String> data, int counter)throws Throwable{
		selectLifeTimePremiumAdjustment(data.get("watercraft" + counter + "_manuscriptLifeTimePremiumAdjustment"),data.get("watercraft" + counter + "_manuscriptTempAdjustment") );
		setEndorsementText(data.get("watercraft" + counter + "_manuscriptEndorsementText"));
	}
}
