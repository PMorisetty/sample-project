package com.pure.dragon.watercraft;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.pure.utilities.Xls_Reader;

public class RaterBook {
	Xls_Reader xls;
	String sheetName;
	JSONParser parser;
	JSONObject jsonObject;
	JSONObject readObject, writeObject;
	public RaterBook(String path, String sheetName){
		xls = new Xls_Reader(path);
		this.sheetName = sheetName;
		 parser = new JSONParser();
		 try {
			jsonObject =  (JSONObject)  parser.parse(new FileReader(System.getProperty("user.dir") + "/src/test/java/com/pure/dragon/watercraft/RatingMapping.json"));
			readObject = (JSONObject) jsonObject.get("readFactor");
			writeObject = (JSONObject) jsonObject.get("writeValue");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
//		xls.createNamedInstances("FR_Tier", "'FR Tier'!$A$5:$E$14");
	}
	public void setData(int row, String value){
		xls.setCellData(sheetName, "Value", row, value);
//		xls.evaluateFormula();
	}
	public String getData(String colName, int rowNum) {
		//return xls.getCellData(sheetName, colName, rowNum);//updated to remove error from repo
		return null;
	}
	public String getHullBaseRate(){
		return getData("Factor", Integer.valueOf((String) readObject.get("Hull Base Rate")));
	}
	public String getPIBaseRate(){
		return getData("Factor", Integer.valueOf((String) readObject.get("P&I Base Rate")));
	}
	public String getFRTier(){
		return getData("Factor", Math.toIntExact((long) writeObject.get("FR Tier")));
	}
	public void setFRTier(String value){
		setData(Math.toIntExact((long) writeObject.get("FR Tier")), value);
	}
	
}
