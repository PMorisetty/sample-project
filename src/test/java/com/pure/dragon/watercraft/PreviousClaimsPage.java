package com.pure.dragon.watercraft;

import org.openqa.selenium.By;

public class PreviousClaimsPage {
	static By watercraftLossHistoryYes, watercraftLossHistoryNo,
	watercraftLossHistoryForPriorVesselYes,watercraftLossHistoryForPriorVesselNo,
	selectWatercraftDropdown, addWatercraftButton,addWatercraftLossPriorVesselButton,
	sourceDropdown, lossTypeDropdown, lossDateText, lossAmountText, deleteButton,
	sourceDropdownPriorVessel, lossTypeDropdownPriorVessel,lossDateTextPriorVessel, lossAmountTextPriorVessel; 
	
	static By autoIncidentsYes, autoIncidentsNo,
	selectOperatorDropdown, addOperatorButton,
	incidentSourceDropdown, accidentDropdown, accidentDateText, deleteAccidentButton;

	static{
		watercraftLossHistoryYes = By.xpath("//div[contains(@title,'prior watercraft losses')]/..//input[@type='radio'][@title='Yes']");
		watercraftLossHistoryNo = By.xpath("//div[contains(@title,'prior watercraft losses')]/..//input[@type='radio'][@title='No']");
		selectWatercraftDropdown = By.cssSelector("select[title='List of Vessels']");
		addWatercraftButton = By.xpath("//div[@title='List of Vessels']/../../..//a[@title='Add']");
		addWatercraftLossPriorVesselButton = By.xpath("//a[contains(@href,'Action.789433')]");
		sourceDropdown = By.xpath("//label[contains(text(), '$')]/../../../..//select[@title='Source'][@class='unFilledMandatoryField']");
		lossTypeDropdown = By.xpath("//label[contains(text(), '$')]/../../../..//select[@title='Loss Type'][@class='unFilledMandatoryField']");
		lossDateText = By.xpath("//label[contains(text(), '$')]/../../../..//input[@title='Loss Date'][contains(@class,'unFilledMandatoryField')]");
		lossAmountText = By.xpath("//label[contains(text(), '$')]/../../../..//input[@title='Loss Amount'][@class='unFilledMandatoryField']");
		
		//-----Watercraft loss for prior Vessel
		sourceDropdownPriorVessel = 	By.xpath("(//div[text()='watercraft loss history for prior vessels not listed on the quote (policy)']/ancestor::tr[position()=1]/following-sibling::tr[2]//select[@title='Source'])[$]");
		lossTypeDropdownPriorVessel = 	By.xpath("(//div[text()='watercraft loss history for prior vessels not listed on the quote (policy)']/ancestor::tr[position()=1]/following-sibling::tr[2]//select[@title='Loss Type'])[$]");
		lossDateTextPriorVessel = 		By.xpath("(//div[text()='watercraft loss history for prior vessels not listed on the quote (policy)']/ancestor::tr[position()=1]/following-sibling::tr[2]//input[@title='Loss Date'])[$]");
		lossAmountTextPriorVessel = 	By.xpath("(//div[text()='watercraft loss history for prior vessels not listed on the quote (policy)']/ancestor::tr[position()=1]/following-sibling::tr[2]//input[@title='Loss Amount'])[$]");
		
		deleteButton = By.xpath("//label[contains(text(), '$')]/../../../..//a[@title='Delete the selected item.']");
		
		autoIncidentsYes = By.xpath("//div[contains(@title,'prior incidents')]/..//input[@type='radio'][@title='Yes']");
		autoIncidentsNo = By.xpath("//div[contains(@title,'prior incidents')]/..//input[@type='radio'][@title='No']");
		selectOperatorDropdown = By.cssSelector("select[title='List of Operators']");
		addOperatorButton = By.xpath("//div[@title='List of Operators']/../../..//a[@title='Add']");
		incidentSourceDropdown = By.xpath("//label[contains(text(), '$')]/../../../..//select[@title='Source'][@class='unFilledMandatoryField']");
		accidentDropdown = By.xpath("//label[contains(text(), '$')]/../../../..//select[@title='Accident / Violation'][contains(@class,'unFilledMandatoryField')]");
		accidentDateText = By.xpath("//label[contains(text(), '$')]/../../../..//input[@title='Date'][contains(@class,'unFilledMandatoryField')]");
		deleteAccidentButton = By.xpath("//label[contains(text(), '$')]/../../../..//a[@title='Delete the selected item.']");
		watercraftLossHistoryForPriorVesselYes = By.xpath("//div[text()='watercraft loss history for prior vessels not listed on the quote (policy)']/../..//input[@title='Yes']");
		watercraftLossHistoryForPriorVesselNo = By.xpath("//div[text()='watercraft loss history for prior vessels not listed on the quote (policy)']/../..//input[@title='No']");
	}
}
