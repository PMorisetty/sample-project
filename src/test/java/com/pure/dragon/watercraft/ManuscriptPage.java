package com.pure.dragon.watercraft;

import org.openqa.selenium.By;

public class ManuscriptPage {
	
	static By manuscriptTypeDropdown, manuscriptTitleText, manuscriptPremiumText, manuscriptApplyToDropdown;
	static By addManuscriptButton;
	
	static By lifeTimePremiumAdjustmentYes, lifeTimePremiumAdjustmentNo; 
	static By temporaryAdjustmentCheckbox, endrosementText;
	
	static{
		manuscriptTypeDropdown = By.cssSelector("select[name*='_29014006'][class='unFilledMandatoryField']");
		manuscriptTitleText = By.cssSelector("input[name*='_29014206'][class='unFilledMandatoryField']");
		manuscriptPremiumText = By.cssSelector("input[name*='_29014306'][class='unFilledMandatoryField']");
		manuscriptApplyToDropdown = By.cssSelector("select[name*='_29678214'][class='unFilledMandatoryField']");
		addManuscriptButton = By.cssSelector("a[onclick*='Action.189'][title='Add another item.']");
		
		lifeTimePremiumAdjustmentYes = By.cssSelector("input[name*='_29694714'][title='Yes']");
		lifeTimePremiumAdjustmentNo = By.cssSelector("input[name*='_29694714'][title='No']");
		temporaryAdjustmentCheckbox = By.cssSelector("input[title='Temporary Adjustment'][type='checkbox']");
		endrosementText = By.cssSelector("textarea[title='Endorsement Text/ Premium Adjustment Rationale']");
	}
}
