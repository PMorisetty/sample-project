package com.pure.dragon.watercraft;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.report.CReporter;

public class VesselUnderwritingPageLib extends ActionEngine{
	public VesselUnderwritingPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	private boolean eligibiliy=false;
	
	//Select Coverage
	public void selectCoverageProtectionIndemnity(String value, String additionalValue, String role) throws Throwable{
		selectByVisibleText(VesselUnderwritingPage.coverageProtectionIndemnityDropDown, value, "Coverage Protection Indemnity");
		if(value.equalsIgnoreCase("Additional Options") && role.equalsIgnoreCase("UW")){
			type(VesselUnderwritingPage.coverageProtectionIndemnityText, additionalValue, "Protection Indemnity value");
			//Underwriter referral eligibility
			if(Integer.valueOf(additionalValue)>5){
				UnderwriterReferralsPageLib.uwReferralEligibilityProtectionIndemnityExceeds5000000 = true;
			}		
		}else if(value.equalsIgnoreCase("Additional Options") && role.equalsIgnoreCase("Agent")){
			verifyElementPresent(VesselUnderwritingPage.selectCoverageAgentMessage, "Additional Options Message", true);
			verifyElementEnable(VesselUnderwritingPage.coverageProtectionIndemnityText, "Protection Indemnity value", false);
		}


	}
	public void selectCoverageUninsuredBoaters(String value, String additionalValue, String role) throws Throwable{
		selectByVisibleText(VesselUnderwritingPage.coverageUninsuredBoatersDropDown, value, "Coverage Uninsured Boaters");
		if(value.equalsIgnoreCase("Additional Options") && role.equalsIgnoreCase("UW")){
			type(VesselUnderwritingPage.coverageUninsuredBoatersText, additionalValue, "Uninsured Boaters value");
			//Underwriter referral eligibility
			if(Integer.valueOf(additionalValue)>5){
				UnderwriterReferralsPageLib.uwReferralEligibilityCoverageUninsuredBoatersExceeds5000000 = true;
			}
		}else if(value.equalsIgnoreCase("Additional Options") && role.equalsIgnoreCase("Agent")){
			verifyElementPresent(VesselUnderwritingPage.selectCoverageAgentMessage, "Additional Options Message", true);
			verifyElementEnable(VesselUnderwritingPage.coverageProtectionIndemnityText, "Protection Indemnity value", false);

		}

	}
	public void selectCoverageAOP_Deductible(String value) throws Throwable{
		selectByVisibleText(VesselUnderwritingPage.coverageAOP_DeductibleDropDown, value, "Coverage AOP Deductible");
	}
	public void selectCoverageAOPOverrideDropdown(String value) throws Throwable{
		selectByVisibleText(VesselUnderwritingPage.coverageAOP_DeductibleOverrideDropDown, value, "Coverage AOP Deductible Override");
	}
	public void selectCoverageHurricane_DeductibleDropDown(String value) throws Throwable{
		selectByVisibleText(VesselUnderwritingPage.coverageHurricane_DeductibleDropDown, value, "Coverage Hurricane Deductible");
		//Select s = new Select(driver.findElement(VesselUnderwritingPage.coverageHurricane_DeductibleDropDown));
		//selectByIndex(VesselUnderwritingPage.coverageHurricane_DeductibleDropDown, s.getOptions().size() - 1, "Coverage Hurricane Deductible");
	}
	public void selectCoverageHurricaneOverrideDropDown(String value) throws Throwable{
		selectByVisibleText(VesselUnderwritingPage.coverageHurricane_DeductibleOverrideDropDown, value, "Coverage Hurricane Deductible Override");
	}
	public void clickCoverageMedicalExpOverrideCheckbox() throws Throwable{
		click(VesselUnderwritingPage.coverageMedicalExpOverrideCheckbox, "Coverage Medical Expenses Override");
	}
	public void setCoverageMedicalExpensesText(String value) throws Throwable{
		type(VesselUnderwritingPage.coverageMedicalExpOverrideText, value, "Coverage Medical Expenses Text");
	}
	public void clickCoverageAOP_deductibleOverrideCheckbox() throws Throwable{
		click(VesselUnderwritingPage.coverageAOP_deductibleOverrideCheckbox, "Coverage AOP_deductible Override");
	}
	public void clickCoverageHurricane_deductibleOverrideCheckbox() throws Throwable{
		click(VesselUnderwritingPage.coverageHurricane_deductibleOverrideCheckbox, "Coverage Hurricane_deductible Override");
	}
	public void fillSelectCoverage(Hashtable<String, String> data,int counter, String role) throws Throwable{
		if("Property Only".equalsIgnoreCase(data.get("watercraft" + counter + "_policyType"))){
			verifyElementPresent(VesselUnderwritingPage.coverageProtectionIndemnityDropDown, "Protection Indemnity dropdown", false);
			verifyElementPresent(VesselUnderwritingPage.coverageProtectionIndemnityText, "Protection Indemnity - Additional Options text", false);
			verifyElementPresent(VesselUnderwritingPage.coverageUninsuredBoatersDropDown, "Uninsured Boaters dropdown", false);
			verifyElementPresent(VesselUnderwritingPage.coverageUninsuredBoatersText, "Uninsured Boaters - Additional Options text", false);
		}else{
			selectCoverageProtectionIndemnity(data.get("watercraft" + counter + "_coverage_ProtectionIndemnity"),data.get("watercraft" + counter + "_coverage_ProtectionIndemnity_Additional"), role);
			selectCoverageUninsuredBoaters(data.get("watercraft" + counter + "_coverage_UninsuredBoaters"), data.get("watercraft" + counter + "_coverage_UninsuredBoaters_Additional"), role);
		}
		String watercratPolicyType;
		if("Liability Only".equalsIgnoreCase(watercratPolicyType = data.get("watercraft" + counter + "_policyType"))){
			verifyElementPresent(VesselUnderwritingPage.coverageAOP_DeductibleDropDown, "AOP Deductible", true);
			verifyElementPresent(VesselUnderwritingPage.coverageAOP_deductibleOverrideCheckbox, "AOP Deductible override checkbox", false);
			verifyElementPresent(VesselUnderwritingPage.coverageAOP_DeductibleOverrideDropDown, "AOP Deductible override dropdown", false);
			verifyElementPresent(VesselUnderwritingPage.coverageHurricane_DeductibleDropDown, "Hurricane Deductible", true);
			verifyElementPresent(VesselUnderwritingPage.coverageHurricane_deductibleOverrideCheckbox, "Hurricane override checkbox", false);
			verifyElementPresent(VesselUnderwritingPage.coverageHurricane_DeductibleOverrideDropDown, "Hurricane override dropdown", false);
			
		}
		if(role.equalsIgnoreCase("UW") || role.equalsIgnoreCase("Agent")){
			if("YES".equalsIgnoreCase(data.get("watercraft" + counter + "_coverage_MedicalExpensesOverride"))){
				clickCoverageMedicalExpOverrideCheckbox();
				setCoverageMedicalExpensesText(data.get("watercraft" + counter + "_coverage_MedicalExpensesValue"));
			}
			if(!"Liability Only".equalsIgnoreCase(watercratPolicyType)){
				if("YES".equalsIgnoreCase(data.get("watercraft" + counter + "_coverage_AOPOverride"))){
					selectCoverageAOP_Deductible(data.get("watercraft" + counter + "_coverage_AOPValue"));
					waitUntilJQueryReady();
					clickCoverageAOP_deductibleOverrideCheckbox();
					selectCoverageAOPOverrideDropdown(data.get("watercraft" + counter + "_coverage_AOPOverrideValue"));
				}else{
					selectCoverageAOP_Deductible(data.get("watercraft" + counter + "_coverage_AOPValue"));
					waitUntilJQueryReady();
				}
				waitUntilJQueryReady();
				if("YES".equalsIgnoreCase(data.get("watercraft" + counter + "_coverage_HurricaneOverride"))){
					selectCoverageHurricane_DeductibleDropDown(data.get("watercraft" + counter + "_coverage_HurricaneValue"));
					waitUntilJQueryReady();
					clickCoverageHurricane_deductibleOverrideCheckbox();
					selectCoverageHurricaneOverrideDropDown(data.get("watercraft" + counter + "_coverage_HurricaneOverrideValue"));
				}else{
					selectCoverageHurricane_DeductibleDropDown(data.get("watercraft" + counter + "_coverage_HurricaneValue"));
					waitUntilJQueryReady();
				}
			}else{
				selectCoverageAOP_Deductible(data.get("watercraft" + counter + "_coverage_AOPValue"));
				selectCoverageHurricane_DeductibleDropDown(data.get("watercraft" + counter + "_coverage_HurricaneValue"));
			}
		}
		if(role.equalsIgnoreCase("Agent")){
			verifyElementPresent(VesselUnderwritingPage.coverageAOP_DeductibleDropDown, "AOP_DeductibleDropDown", true);
			verifyElementPresent(VesselUnderwritingPage.coverageAOP_DeductibleOverrideDropDown, "AOP_DeductibleOverrideDropDown", false);
			verifyElementPresent(VesselUnderwritingPage.coverageHurricane_DeductibleDropDown, "Hurricane_DeductibleDropDown", true);
			verifyElementPresent(VesselUnderwritingPage.coverageHurricane_DeductibleOverrideDropDown, "Hurricane_DeductibleOverrideDropDown", false);
			verifyElementPresent(VesselUnderwritingPage.coverageMedicalExpOverrideCheckbox, "MedicalExpOverrideCheckbox", false);
			verifyElementPresent(VesselUnderwritingPage.coverageMedicalExpOverrideText, "MedicalExpOverrideText", false);
			verifyElementPresent(VesselUnderwritingPage.coverageAOP_deductibleOverrideCheckbox, "AOP_deductibleOverrideCheckbox", false);
			verifyElementPresent(VesselUnderwritingPage.coverageHurricane_deductibleOverrideCheckbox, "Hurricane_deductibleOverrideCheckbox", false);
		}
	}

	//additional coverage
	public void selectAdditionalCoveragePersonalEffects(String value) throws Throwable{
		selectByVisibleText(VesselUnderwritingPage.additionalCoveragePersonalEffects, value, "Additional Coverage Personal Effects");
	}
	public void setAdditionalCoveragePersonalEffectsOverride(String value) throws Throwable{
		type(VesselUnderwritingPage.additionalCoveragePersonalEffectsOverrideText, value, "Additional Coverage Personal Effects Override");
	}
	public void setAdditionalCoverageIncreasedTrailerLimit(String value) throws Throwable{
		if(value != null && value.trim() != ""){
			type(VesselUnderwritingPage.additionalCoverageIncreasedTrailerLimit, value, "Additional Coverage Increased Trailer Limit");
			type(VesselUnderwritingPage.additionalCoverageIncreasedTrailerLimit, Keys.TAB, "tab key");
			int actualVal = Integer.parseInt(
					getAttributeByValue(VesselUnderwritingPage.additionalCoverageTrailer_ValueText, "additionalCoverageTrailer_ValueText")
					.replaceAll("\\.0*$", ""));
assertTrue(actualVal==(Integer.parseInt(value) + 5000), "trailerLimit and actualVal");

		}
	}
	public void selectAdditionalCoverageEmergencyExpense(String value) throws Throwable{
		selectByVisibleText(VesselUnderwritingPage.additionalCoverageEmergencyExpense, value, "Additional Coverage Personal Effects");
	}
	public void setAdditionalCoverageEmergencyExpenseOverride(String value) throws Throwable{
		type(VesselUnderwritingPage.additionalCoverageEmergencyExpenseOverrideText, value, "Additional Coverage Emergency Expense Override");
	}
	public void clickAdditionalCoveragePersonalEffectsOverrideCheckbox() throws Throwable{
		click(VesselUnderwritingPage.additionalCoveragePersonalEffectsOverrideCheckbox, "Additional Coverage Personal Effects Override");
	}
	public void clickAdditionalCoverageEmergencyExpenseOverrideCheckbox() throws Throwable{
		click(VesselUnderwritingPage.additionalCoverageEmergencyExpenseOverrideCheckbox, "Additional Coverage Emergency Expense Override");
	}
	public void fillAdditionalCoverageUnder27(Hashtable<String, String> data,int counter) throws Throwable{
		if("YES".equalsIgnoreCase(data.get("watercraft" + counter + "_additionalCoverage_PersonalEffectOverride"))){
			clickAdditionalCoveragePersonalEffectsOverrideCheckbox();
			setAdditionalCoveragePersonalEffectsOverride(data.get("watercraft" + counter + "_additionalCoverage_PersonalEffectOverrideValue"));
		}else{
			selectAdditionalCoveragePersonalEffects(data.get("watercraft" + counter + "_additionalCoverage_PersonalEffectValue"));
		}
		setAdditionalCoverageIncreasedTrailerLimit(data.get("watercraft" + counter + "_additionalCoverage_IncreasedTrailerLimit"));
		if("YES".equalsIgnoreCase(data.get("watercraft" + counter + "_additionalCoverage_EmergecyExpenseOverride"))){
			clickAdditionalCoverageEmergencyExpenseOverrideCheckbox();
			setAdditionalCoverageEmergencyExpenseOverride(data.get("watercraft" + counter + "_additionalCoverage_EmergecyExpenseOverrideValue"));
		}else{
			selectAdditionalCoverageEmergencyExpense(data.get("watercraft" + counter + "_additionalCoverage_EmergecyExpenseValue"));
		}
	}
	public void fillAdditionalCoverageUnder27Agent(Hashtable<String, String> data, int counter) throws Throwable{
		selectAdditionalCoveragePersonalEffects(data.get("watercraft" + counter + "_additionalCoverage_PersonalEffectValue"));
		setAdditionalCoverageIncreasedTrailerLimit(data.get("watercraft" + counter + "_additionalCoverage_IncreasedTrailerLimit"));
		selectAdditionalCoverageEmergencyExpense(data.get("watercraft" + counter + "_additionalCoverage_EmergecyExpenseValue"));
	}
	/*public void selectAdditionalCoverageTenders(Hashtable<String, String> data, int counter) throws Throwable{
		if("YES".equalsIgnoreCase(data.get("watercraft" + counter + "_additionalCoverage_Tenders"))){
			click(VesselUnderwritingPage.additionalCoverageTendersYES, "Vessel Under Writing Tenders YES");
			type(VesselUnderwritingPage.additionalCoverageTender_ValueText, data.get("watercraft" + counter + "_additionalCoverage_TenderValue"), "Tender value");
			type(VesselUnderwritingPage.additionalCoverageTender_ManufacturerText, data.get("watercraft" + counter + "_additionalCoverage_TenderManufacturer"), "Tender Manufacturer");
			type(VesselUnderwritingPage.additionalCoverageTender_Length, data.get("watercraft" + counter + "_additionalCoverage_TenderLength"), "Tender Length");
		}else if("NO".equalsIgnoreCase(data.get("watercraft" + counter + "_additionalCoverage_Tenders")))
			click(VesselUnderwritingPage.additionalCoverageTendersNO, "Vessel Under Writing Tenders NO");
	}*/
	public void selectAdditionalCoverageTrailer(Hashtable<String, String> data, int counter) throws Throwable{
		setAdditionalCoverageIncreasedTrailerLimit(data.get("watercraft" + counter + "_additionalCoverage_IncreasedTrailerLimit"));
		//		commented since seperate text box not present now
		//		if(data.get("watercraft" + counter + "_additionalCoverage_TrailerValue") != null)
		//		type(VesselUnderwritingPage.additionalCoverageTrailer_ValueText, data.get("watercraft" + counter + "_additionalCoverage_TrailerValue"), "Trailer value");
		/*if(data.get("watercraft" + counter + "_additionalCoverage_TrailerManufacturer") != null)
			type(VesselUnderwritingPage.additionalCoverageTrailer_ManufacturerText, data.get("watercraft" + counter + "_additionalCoverage_TrailerManufacturer"), "Trailer Manufacturer");
		if(data.get("watercraft" + counter + "_additionalCoverage_TrailerLength") != null)
			type(VesselUnderwritingPage.additionalCoverageTrailer_Length, data.get("watercraft" + counter + "_additionalCoverage_TrailerLength"), "Trailer Length");
		if(data.get("watercraft" + counter + "_additionalCoverage_TrailerYear") != null)
			type(VesselUnderwritingPage.additionalCoverageTrailer_YearText, data.get("watercraft" + counter + "_additionalCoverage_TrailerYear"), "Trailer Year");
		if(data.get("watercraft" + counter + "_additionalCoverage_TrailerSerialNumber") != null)
			type(VesselUnderwritingPage.additionalCoverageTrailer_SerialNumberText, data.get("watercraft" + counter + "_additionalCoverage_TrailerSerialNumber"), "Trailer Serial Number");*/
	}

	public void fillAdditionalCoverageAbove27(Hashtable<String, String> data, int counter) throws Throwable{
		if("YES".equalsIgnoreCase(data.get("watercraft" + counter + "_additionalCoverage_PersonalEffectOverride"))){
			clickAdditionalCoveragePersonalEffectsOverrideCheckbox();
			setAdditionalCoveragePersonalEffectsOverride(data.get("watercraft" + counter + "_additionalCoverage_PersonalEffectOverrideValue"));
		}else{
			selectAdditionalCoveragePersonalEffects(data.get("watercraft" + counter + "_additionalCoverage_PersonalEffectValue"));
		}
		String trailerLimit;
		setAdditionalCoverageIncreasedTrailerLimit(trailerLimit = data.get("watercraft" + counter + "_additionalCoverage_IncreasedTrailerLimit"));		
		/*int actualVal = Integer.parseInt(
				getText(VesselUnderwritingPage.additionalCoverageTrailer_ValueText, "additionalCoverageTrailer_ValueText")
				.replaceAll("\\.0*$", ""));
		assertTrue(actualVal==Integer.parseInt(trailerLimit+5000), "trailerLimit and actualVal wont match");*/

		if("YES".equalsIgnoreCase(data.get("watercraft" + counter + "_additionalCoverage_EmergecyExpenseOverride"))){
			clickAdditionalCoverageEmergencyExpenseOverrideCheckbox();
			setAdditionalCoverageEmergencyExpenseOverride(data.get("watercraft" + counter + "_additionalCoverage_EmergecyExpenseOverrideValue"));
		}else{
			selectAdditionalCoverageEmergencyExpense(data.get("watercraft" + counter + "_additionalCoverage_EmergecyExpenseValue"));
		}
//			selectAdditionalCoverageTenders(data, counter);
//			selectAdditionalCoverageTrailer(data,counter);
	}

	public void fillAdditionalCoverageAbove27Agent(Hashtable<String, String> data, int counter) throws Throwable{
		selectAdditionalCoveragePersonalEffects(data.get("watercraft" + counter + "_additionalCoverage_PersonalEffectValue"));
		selectAdditionalCoverageEmergencyExpense(data.get("watercraft" + counter + "_additionalCoverage_EmergecyExpenseValue"));
//		selectAdditionalCoverageTenders(data, counter);
		selectAdditionalCoverageTrailer(data,counter);
	}
	//Vessel Underwriting
	public void selectVesselUWDetailsBusinessUse(String value) throws Throwable{
		if("YES".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsBusinessUseYES, "Vessel Under Writing Details BusinessUse YES");
		else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsBusinessUseNO, "Vessel Under Writing Details BusinessUse NO");
	}
	public void selectVesselUWDetailsRacingUse(String value) throws Throwable{
		if("YES".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsRacingUseYES, "Vessel Under Writing Details RacingUse YES");
		else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsRacingUseNO, "Vessel Under Writing Details RacingUse NO");
	}
	public void selectVesselUWDetailsWaterSkiingUse(String value) throws Throwable{
		if("YES".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsWaterSkiingUseYES, "Vessel Under Writing Details WaterSkiingUse YES");
		else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsWaterSkiingUseNO, "Vessel Under Writing Details WaterSkiingUse NO");
	}
	
	public void selectVesselUWDetailsForSale(String value) throws Throwable{
		if("YES".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsForSaleYES, "Vessel Under Writing Details For Sale YES");
		else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsForSaleNO, "Vessel Under Writing Details For Sale NO");
	}
	
	public void selectVesselUWDetailsPreviousDamage(String value, String... text) throws Throwable{
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.vesselUnderWritingDetailsPreviousDamageYES, "Vessel Under Writing Details PreviousDamage YES");
			String input = (text[0] != null)?text[0]:"";
			type(VesselUnderwritingPage.vesselUnderWritingDetailsPreviousDamageText, input, "Previous Damage");
		}
		else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsPreviousDamageNO, "Vessel Under Writing Details PreviousDamage NO");
	}
	public void selectVesselUWDetailsExistingDamage(String value, String... text) throws Throwable{
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.vesselUnderWritingDetailsExistingDamageYES, "Vessel Under Writing Details ExistingDamage YES");
			String input = (text[0] != null)?text[0]:"";
			type(VesselUnderwritingPage.vesselUnderWritingDetailsExistingDamageText, input, "Existing Damage");
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsExistingDamageNO, "Vessel Under Writing Details ExistingDamage NO");
	}

	public void selectVesselUWDetailsDetailsEmployedCrew(String value, String...text) throws Throwable{
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.vesselUnderWritingDetailsEmployedCrewYES, "Vessel Under Writing Details Employed Crew YES");
			String input = (text[0] != null)?text[0]:"1";//1 since field is mandatory, can't be blank
			typeText(VesselUnderwritingPage.vesselUnderWritingDetailsFullTimeCrewText, input, "Full time crew");
			input = (text[1] != null)?text[0]:"1";//1 since field is mandatory, can't be blank
			typeText(VesselUnderwritingPage.vesselUnderWritingDetailsPartTimeCrewText, input, "Part time crew");
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsEmployedCrewNO, "Vessel Under Writing Details Employed Crew NO");
	}
	public void selectVesselUWDetailsDetailsEmployedCaptain(String value, String... text) throws Throwable{
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.vesselUnderWritingDetailsEmployeedCaptainYES, "Vessel Under Writing Details Employed Captain YES");
			String input = (text[0] != null)?text[0]:"1";//1 since field is mandatory, can't be blank
			type(VesselUnderwritingPage.vesselUnderWritingDetailsNoofCrewText, input, "Number of Crew");
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.vesselUnderWritingDetailsEmployeedCaptainNO, "Vessel Under Writing Details Employed Captain NO");
	}

	public void fillVesselUWDetailsUnder27(Hashtable<String, String> data, int counter)throws Throwable{
		selectVesselUWDetailsBusinessUse(data.get("watercraft" + counter + "_VesselUW_isBusinessPurpose"));
		selectVesselUWDetailsRacingUse(data.get("watercraft" + counter + "_VesselUW_isRacing"));
		selectVesselUWDetailsPreviousDamage(data.get("watercraft" + counter + "_VesselUW_PreviousDamage"), data.get("watercraft" + counter + "_VesselUW_PreviousDamageReason"));
		selectVesselUWDetailsWaterSkiingUse(data.get("watercraft" + counter + "_VesselUW_isWaterSkiing"));
		selectVesselUWDetailsDetailsEmployedCaptain(data.get("watercraft" + counter + "_VesselUW_EmployedCaptain"), data.get("watercraft" + counter + "_VesselUW_NoOfCrew"));
		selectVesselUWDetailsExistingDamage(data.get("watercraft" + counter + "_VesselUW_ExistingDamage"), data.get("watercraft" + counter + "_VesselUW_ExistingDamageReason"));
	}
	public void fillVesselUWDetailsAbove27(Hashtable<String, String> data, int counter)throws Throwable{
		selectVesselUWDetailsBusinessUse(data.get("watercraft" + counter + "_VesselUW_isBusinessPurpose"));
		selectVesselUWDetailsRacingUse(data.get("watercraft" + counter + "_VesselUW_isRacing"));
		selectVesselUWDetailsPreviousDamage(data.get("watercraft" + counter + "_VesselUW_PreviousDamage"), data.get("watercraft" + counter + "_VesselUW_PreviousDamageReason"));
		selectVesselUWDetailsDetailsEmployedCrew(data.get("watercraft" + counter + "_VesselUW_EmployedCrew"), data.get("watercraft" + counter + "_VesselUW_EmployedCrew_FullTimeCrew"), data.get("watercraft" + counter + "_VesselUW_EmployedCrew_PartTimeCrew"));
		selectVesselUWDetailsWaterSkiingUse(data.get("watercraft" + counter + "_VesselUW_isWaterSkiing"));
		selectVesselUWDetailsExistingDamage(data.get("watercraft" + counter + "_VesselUW_ExistingDamage"), data.get("watercraft" + counter + "_VesselUW_ExistingDamageReason"));
		selectVesselUWDetailsForSale(data.get("watercraft" + counter + "_ForSale"));
		
	}


	//Optional coverage section
	public void fillOptionalCoverageDetails(Hashtable<String, String> data, int counter)throws Throwable{
		selectOptCoverage_WarPI(data, counter);
		selectOptCoverage_WarHull(data, counter);
		selectOptCoverage_FineArts(data, counter);
		selectOptCoverage_WarrantyBreach(data, counter);
		selectOptCoverage_FishingGear(data, counter);
		selectOptCoverage_CaptainWarranty(data, counter);
		selectOptCoverage_OperatorWarranty(data, counter);
		selectOptCoverage_OperatorExclusion(data, counter);
		selectOptCoverage_ACVforMachinery(data.get("watercraft" + counter + "_optCoverage_ACV"));
		selectOptCoverage_CharterCoverage(data.get("watercraft" + counter + "_optCoverage_Charter"));
		selectOptCoverage_MachineryExclusion(data.get("watercraft" + counter + "_optCoverage_MachineryExclusion"));
		selectOptCoverage_TowingDeductible(data, counter);
		selectOptCoverage_TheftDeductible(data, counter);
		if(data.get("watercraft" + counter + "_length") != null && data.get("watercraft" + counter + "_length").trim() != "" && Integer.valueOf(data.get("watercraft" + counter + "_length")) >= 27)
		selectOptCoverage_AdditionalTenders(data, counter);
		if(eligibiliy){
			UnderwriterReferralsPageLib.eligibilityOptionalCoverage=true;
		}
	}

	public void selectOptCoverage_WarPI(Hashtable<String, String> data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_optional_WarPI");
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_WarPI_YES, "Optional Coverage WarPI YES");
			type(VesselUnderwritingPage.optCoverage_WarPI_Limit, data.get("watercraft" + counter + "_optCoverage_WarPI_Limit"), "War PI - Limit");
			type(VesselUnderwritingPage.optCoverage_WarPI_Rate, data.get("watercraft" + counter + "_optCoverage_WarPI_Rate"), "War PI - Rate");
			assertTextStringContains(getText(VesselUnderwritingPage.optCoverage_WarPI_Rate_Label, "WarPI Rate Label"), "Rate per $1000(1.00)");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_WarPI_NO, "Optional Coverage WarPI NO");
	}
	public void selectOptCoverage_WarHull(Hashtable<String, String> data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_optional_WarHull");
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_WarHullYES, "Optional Coverage War Hull YES");
			type(VesselUnderwritingPage.optCoverage_WarHull_Limit, data.get("watercraft" + counter + "_optCoverage_WarHull_Limit"), "War PI - Limit");
			type(VesselUnderwritingPage.optCoverage_WarHull_Rate, data.get("watercraft" + counter + "_optCoverage_WarHull_Rate"), "War PI - Rate");
			assertTextStringContains(getText(VesselUnderwritingPage.optCoverage_WarHull_Rate_Label, "WarHull Rate Label"), "Rate per $1000(4.25)");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_WarHullNO, "Optional Coverage War Hull NO");
	}
	public void selectOptCoverage_FineArts(Hashtable<String, String> data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_optional_FineArts");
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_FineArtsYES, "Optional Coverage Fine Arts YES");
			type(VesselUnderwritingPage.optCoverage_FineArts_Limit, data.get("watercraft" + counter + "_optCoverage_FineArts_Limit"), "FineArts - Limit");
			type(VesselUnderwritingPage.optCoverage_FineArts_Rate, data.get("watercraft" + counter + "_optCoverage_FineArts_Rate"), "FineArts - Rate");
			assertTextStringContains(getText(VesselUnderwritingPage.optCoverage_FineArts_Rate_Label, "FineArts Rate Label"), "Rate per $1000(2.50)");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_FineArtsNO, "Optional Coverage Fine Arts NO");
	}
	public void selectOptCoverage_WarrantyBreach(Hashtable<String, String> data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_optional_WarrantyBreach");
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_WarrantyBreachYES, "Optional Coverage Warranty Breach YES");
			type(VesselUnderwritingPage.optCoverage_WarrantyBreach_Limit, data.get("watercraft" + counter + "_optCoverage_WarrantyBreach_Limit"), "WarrantyBreach - Limit");
			type(VesselUnderwritingPage.optCoverage_WarrantyBreach_Rate, data.get("watercraft" + counter + "_optCoverage_WarrantyBreach_Rate"), "WarrantyBreach - Rate");
			assertTextStringContains(getText(VesselUnderwritingPage.optCoverage_WarrantyBreach_Rate_Label, "Warranty Breach Rate Label"), "Rate per 1MIL(375.00)");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_WarrantyBreachNO, "Optional Coverage Warranty Breach NO");
	}
	public void selectOptCoverage_CaptainWarranty(Hashtable<String, String> data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_optional_CaptainWarranty");
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_CaptainWarrantyYES, "Optional Coverage Captain Warranty YES");
			type(VesselUnderwritingPage.optCoverage_NameOfCaptain, data.get("watercraft" + counter + "_optCoverage_NameOfCaptain"), "NameOfCaptain");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_CaptainWarrantyNO, "Optional Coverage Captain Warranty NO");
	}
	public void selectOptCoverage_OperatorWarranty(Hashtable<String, String> data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_optional_OperatorWarranty");
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_OperatorWarrantyYES, "Optional Coverage Operator Warranty YES");
			type(VesselUnderwritingPage.optCoverage_NamedOperatorWarranty, data.get("watercraft" + counter + "_optCoverage_NamedOperatorWarranty"), "NamedOperatorWarranty");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_OperatorWarrantyNO, "Optional Coverage Operator Warranty NO");
	}
	public void selectOptCoverage_CharterCoverage(String value) throws Throwable{
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_CharterCoverageYES, "Optional Coverage Charter Coverage YES");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_CharterCoverageNO, "Optional Coverage Charter Coverage NO");
		}
	}
	public void selectOptCoverage_OperatorExclusion(Hashtable<String, String> data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_optional_OperatorExclusion");
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_OperatorExclusionYES, "Optional Coverage Operator Exclusion YES");
			type(VesselUnderwritingPage.optCoverage_NamedOperatorExclusion, data.get("watercraft" + counter + "_optCoverage_NamedOperatorExclusion"), "NamedOperatorExclusion");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_OperatorExclusionNO, "Optional Coverage Operator Exclusion NO");
	}
	public void selectOptCoverage_ACVforMachinery(String value) throws Throwable{
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_ACVforMachineryYES, "Optional Coverage ACV for Machinery YES");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_ACVforMachineryNO, "Optional Coverage ACV for Machinery NO");
	}
	public void selectOptCoverage_MachineryExclusion(String value) throws Throwable{
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_MachineryExclusionYES, "Optional Coverage Machinery Exclusion YES");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_MachineryExclusionNO, "Optional Coverage Machinery Exclusion NO");
	}
	public void selectOptCoverage_TowingDeductible(Hashtable<String, String> data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_optional_TowingDeductible");
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_TowingDeductibleYES, "Optional Coverage Towing Deductible YES");
			selectByVisibleText(VesselUnderwritingPage.optCoverage_TowingDeductible_ded, data.get("watercraft" + counter + "_optCoverage_TowingDeductible_ded"), "TowingDeductible_ded");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_TowingDeductibleNO, "Optional Coverage Towing Deductible NO");
	}
	public void selectOptCoverage_TheftDeductible(Hashtable<String, String> data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_optional_TheftDeductible");
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_TheftDeductibleYES, "Optional Coverage Theft Deductible YES");
			selectByVisibleText(VesselUnderwritingPage.optCoverage_TheftDeductible_ded, data.get("watercraft" + counter + "_optCoverage_TheftDeductible_ded"), "TheftDeductible_ded");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_TheftDeductibleNO, "Optional Coverage Theft Deductible NO");
	}
	public void selectOptCoverage_FishingGear(Hashtable<String, String> data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_optional_FishingGear");
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_FishingGearYES, "Optional Coverage Fishing Gear YES");
			type(VesselUnderwritingPage.optCoverage_FishingGear_Limit, data.get("watercraft" + counter + "_optCoverage_FishingGear_Limit"), "FishingGear_Limit");
			eligibiliy=true;
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_FishingGearNO, "Optional Coverage Fishing Gear NO");
	}
	public void selectOptCoverage_AdditionalTenders(Hashtable<String, String> data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_optCoverage_AdditionalTenders");
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.optCoverage_AdditionalTendersYES, "Optional Coverage Additional Tenders YES");
			type(VesselUnderwritingPage.optCoverage_AdditionalTenders_manufacturer, data.get("watercraft" + counter + "_optCoverage_AdditionalTenders_Manufacturer"), "AdditionalTenders_Manufacturer");
			type(VesselUnderwritingPage.optCoverage_AdditionalTenders_length, data.get("watercraft" + counter + "_optCoverage_AdditionalTenders_Length"), "AdditionalTenders_Length");
			type(VesselUnderwritingPage.optCoverage_AdditionalTenders_value, data.get("watercraft" + counter + "_optCoverage_AdditionalTenders_Value"), "AdditionalTenders_Value");
			selectByVisibleText(VesselUnderwritingPage.optCoverage_AdditionalTenders_ded, data.get("Watercraft" + counter + "_TenderDeductible"), "Tender Deductible");
		}else if("NO".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.optCoverage_AdditionalTendersNO, "Optional Coverage Additional Tenders NO");
	}
	

	//Optional for Quote
	public void selectVesselTitleInNameOf(String value) throws Throwable{
		selectByVisibleText(VesselUnderwritingPage.vesselTitleInNameOf, value, "Vessel Title In Name Of");
	}
	public void selectOwnership(Hashtable<String, String>data, int counter) throws Throwable{
		String value = data.get("watercraft" + counter + "_ownership");
		selectByVisibleText(VesselUnderwritingPage.ownership, value, "Ownership");
		waitUntilJQueryReady();		
		if(value.equalsIgnoreCase("Add Additional Owner")){
			int noOfAdditionalOwners=Integer.valueOf(data.get("watercraft" + counter + "_numberofAdditionalOwners"));
			for(int i=1;i<=noOfAdditionalOwners;i++){
				click(VesselUnderwritingPage.additionalInterestADDbtn,"additionalInterest ADD btn");waitUntilJQueryReady();				
				String interestType = data.get("watercraft" + counter + "_AdditionalOwner"+i+"_Type");
				String name = data.get("watercraft" + counter + "_AdditionalOwner"+i+"_Name");
				String address = data.get("watercraft" + counter + "_AdditionalOwner"+i+"_Address");
				String city = data.get("watercraft" + counter + "_AdditionalOwner"+i+"_City");
				String state = data.get("watercraft" + counter + "_AdditionalOwner"+i+"_State");
				String zip = data.get("watercraft" + counter + "_AdditionalOwner"+i+"_zip");
				
				selectByVisibleText(VesselUnderwritingPage.additionalInterestDropDown, interestType, "interestType");
				type(VesselUnderwritingPage.additionalOwnerName, name, "interestType");
				type(VesselUnderwritingPage.additionalOwnerAddress, address, "interestType");
				type(VesselUnderwritingPage.additionalOwnerCity, city, "interestType");
				selectByVisibleText(VesselUnderwritingPage.additionalOwnerStateDropDown, state, "interestType");waitUntilJQueryReady();
				type(VesselUnderwritingPage.additionalOwnerZip, zip, "interestType");waitUntilJQueryReady();
			}
			//Underwriter referral rule			
			UnderwriterReferralsPageLib.eligibilityMoreThan2AdditionalOwners=noOfAdditionalOwners;
		}
	}
	//operator experience details
	public void selectOperatorExpPrimaryOperator(String value) throws Throwable{
		selectByVisibleText(VesselUnderwritingPage.operatorExp_PrimaryOperator, value, "Operator Exp - Primary Operator");
	}
	public void selectOperatorExp3yearsExp(String value)throws Throwable{
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.operatorExp_3yearExpYes, "Primary operator - 3years exp YES");					
		}else{
			click(VesselUnderwritingPage.operatorExp_3yearExpNo, "Primary operator - 3years exp NO");
			//Underwriter referral eligibility
			UnderwriterReferralsPageLib.uwReferralEligibilityForOperatorExp3yearsExp=true;
		}

	}
	public void selectOperatorExpUSCGA(String value) throws Throwable{
		if("YES".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.operatorExp_USCoastGuardAcademyCheckbox, "Operator Exp - US Coast Guard Academy");
	}
	public void selectOperatorExpLicensedCaptain(String value) throws Throwable{
		if("YES".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.operatorExp_LicensedCaptainCheckbox, "Operator Exp - Licensed Captain");
	}
	public void selectOperatorExpUSPS(String value) throws Throwable{
		if("YES".equalsIgnoreCase(value))
			click(VesselUnderwritingPage.operatorExp_USPowerSquadronsCheckbox,"Operator Exp - US PowerSquadrons");
	}
	public void selectOperatorExpOther(String value, String details) throws Throwable{
		if("YES".equalsIgnoreCase(value)){
			click(VesselUnderwritingPage.operatorExp_OtherCertificateCheckbox, "Operator Exp - Other Certificate");
			type(VesselUnderwritingPage.operatorExp_OtherCertificateText, details, "Operator Exp - Other Certificate details");
		}
	}

	public void fillOperatorExperienceDetails(Hashtable<String, String> data, int counter) throws Throwable{
		selectOperatorExpPrimaryOperator(data.get("watercraft" + counter + "_OperatorExp_PrimaryOperator"));
		selectOperatorExp3yearsExp(data.get("watercraft" + counter + "_OperatorExp_3yearsExp"));
		selectOperatorExpUSCGA(data.get("watercraft" + counter + "_OperatorExp_USCGA"));
		selectOperatorExpLicensedCaptain(data.get("watercraft" + counter + "_OperatorExp_LicensedCaptain"));
		selectOperatorExpUSPS(data.get("watercraft" + counter + "_OperatorExp_USPS"));
		//selectOperatorExpOther(data.get("watercraft" + counter + "_OperatorExp_OtherCertificate"),data.get("watercraft" + counter + "_OperatorExp_OtherCertificateDetails"));
	}
}
