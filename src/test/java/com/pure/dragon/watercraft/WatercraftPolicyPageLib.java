package com.pure.dragon.watercraft;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.report.CReporter;

public class WatercraftPolicyPageLib extends ActionEngine {
	public WatercraftPolicyPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void selectPoliticalFigureOption(String value) throws Throwable{
		if(value.equalsIgnoreCase("YES")){
			click(WatercraftPolicyPage.politicalFigureYes, "Political Figure - Yes");
		}else if(value.equalsIgnoreCase("NO")){
			click(WatercraftPolicyPage.politicalFigureNo, "Political Figure - No");
		}
	}
	private void selectNamedInsuredType(String value) throws Throwable{
		selectByVisibleText(WatercraftPolicyPage.namedInsuredDropdown,value,"Named Insured Type");
		if(value.equalsIgnoreCase("Other Legal Entity")){
			type(WatercraftPolicyPage.trustLLCOtherLegalEntity,"Pure Insurance","Trust,LLC, Other legal entity");
			//underwriter referral eligibility
			UnderwriterReferralsPageLib.uwReferralEligibilityForNamedInsuredOtherLegalEntity=true;
		}
	}
	public void selectPriorCarrierDropDown(String value) throws Throwable{
		selectByVisibleText(WatercraftPolicyPage.priorCarrierDropDown, value, "Prior Carrier Select");
	}
	public void setPremium(String value) throws Throwable{
		type(WatercraftPolicyPage.premiumText, value, "Premium");
	}
	public void selectTermDropDown(String value) throws Throwable{
		selectByVisibleText(WatercraftPolicyPage.termDropDown, value, "Term Select");
	}
	public void setEffectiveDateText(String value) throws Throwable{
		type(WatercraftPolicyPage.effectiveDateText, value, "Effective Date");
	}
	public void selectPolicyTypeDropDown(String value) throws Throwable{
		selectByVisibleText(WatercraftPolicyPage.policyType, value, "Policy Type");
	}
	public void setYearText(String value) throws Throwable{
		type(WatercraftPolicyPage.yearText, value, "Year");
		
	}
	public void setManufacturerText(String value) throws Throwable{
		type(WatercraftPolicyPage.manufacturerText, value, "Manufacturer");
	}
	public void setModelText(String value) throws Throwable{
		type(WatercraftPolicyPage.modelText, value, "Model");
	}
	public void setHullLimitText(String value) throws Throwable{
		
		//type(WatercraftPolicyPage.hullLimitText, value, "Hull Limit");
		//Underwriter referral eligibility
		UnderwriterReferralsPageLib.uwReferralEligibilityForHullLimit = value;
	}
	public void setLengthText(String value) throws Throwable{
		type(WatercraftPolicyPage.lengthText, value, "Length");
		if(Integer.valueOf(value)>27){
			UnderwriterReferralsPageLib.uwReferralEligibilityForVesselLength=true;			
		}
		UnderwriterReferralsPageLib.vesselLength=value;
	}
	public void selectLiabilityDropDown(String value) throws Throwable{
		selectByVisibleText(WatercraftPolicyPage.liabilityDropDown, value, "Liability");
	}
	public void clickAddWatercraft() throws Throwable{
		click(WatercraftPolicyPage.addWatercraftButton, "Add Watercraft");
	}
	public void clickDeleteWatercraft() throws Throwable{
		click(WatercraftPolicyPage.deleteWatercraftButton, "Delete Watercraft");
	}
	public void deleteAllWatercraft() throws Throwable{
		while(isVisibleOnly(WatercraftPolicyPage.deleteWatercraftButton, "Delete Watercraft button")){
			this.clickDeleteWatercraft();
		}
	}
	//It would also deselect watercraft with partial info
	public void deleteWatercraftObtainedThroughReports() throws Throwable{
		List<WebElement> elems = driver.findElements(WatercraftPolicyPage.listOfWCObtainedViaReports);
		for(int iterator = 2; iterator <= elems.size(); iterator++){
			String year = elems.get(iterator).findElements(By.cssSelector("")).get(iterator).getAttribute("innerText");
		}
	}

	public void setFirstNameText(String value) throws Throwable{
		type(WatercraftPolicyPage.firstNameText, value, "First Name");
	}
	public void setLastNameText(String value) throws Throwable{
		type(WatercraftPolicyPage.lastNameText, value, "Last Name");
	}
	public void setDobText(String value) throws Throwable{
		type(WatercraftPolicyPage.dobText, value, "DOB");
		//Underwriter referral eligibility
		if(convertDOBtoAge(value)<25){
			UnderwriterReferralsPageLib.uwReferralEligibilityForAge=true;
		}
		UnderwriterReferralsPageLib.operatorDOB=value;
	}
	public void selectRelationToInsured(String value) throws Throwable{
		selectByVisibleText(WatercraftPolicyPage.relationToInsured, value, "Relation To Insured");
		//underwriter referral eligibility
		if(value.contains("Captain")){
			UnderwriterReferralsPageLib.uwReferralEligibilityForRelationToCaptain = true;
		}
	}
	public void clickAddOperatorButton() throws Throwable{
		click(WatercraftPolicyPage.addOperatorButton, "Add Operator");
	}
	public void clickDeleteOperatorButton() throws Throwable{
		click(WatercraftPolicyPage.deleteOperatorButton, "Delete Operator");
	}
	public void fillAdditionalWatercraftDetails(Hashtable<String, String> data) throws Throwable{
		int noOfVessels = Integer.valueOf(data.get("NoOfVessels"));
		for(int i = 1; i<=noOfVessels;i++){
			selectPolicyTypeDropDown(data.get("watercraft" + i + "_policyType"));
			setYearText(data.get("watercraft" + i + "_year"));
			setManufacturerText(data.get("watercraft" + i + "_manufacturer"));
			setModelText(data.get("watercraft" + i + "_model"));
			setLengthText(data.get("watercraft" + i + "_length"));
			if(!"Liability Only".equalsIgnoreCase(data.get("watercraft" + i + "_policyType")))
				//setHullLimitText(data.get("watercraft" + i + "_hullLimit"));
				//this has been moved to VesselDescriptionPageLib.fillVesselPriceLookUp as setHullMachineryLimitAgreedValue
			if(i<noOfVessels)clickAddWatercraft();
		}

	}
	public void fillOperatorDetails(Hashtable<String, String> data) throws Throwable{
		int noOfOperators = Integer.valueOf(data.get("NoOfOperators"));
		for(int i = 2; i<=noOfOperators;i++){
			clickAddOperatorButton();
			setFirstNameText(data.get("operator" + i + "_firstName"));
			setLastNameText(data.get("operator" + i + "_lastName"));
			setDobText(data.get("operator" + i + "_dob"));
			selectRelationToInsured(data.get("operator" + i + "_relation"));
		}
	}
	public void fillInsuranceInfo(Hashtable<String, String> data)throws Throwable{
		if(data.get("priorCarrier") != null && data.get("priorCarrier").trim() != "")selectPriorCarrierDropDown(data.get("priorCarrier"));
		if(data.get("priorPremium") != null && data.get("priorPremium").trim() != "")setPremium("priorPremium");
		if(data.get("namedInsuredType")!=null) selectNamedInsuredType(data.get("namedInsuredType"));
	}
	public void fillWatercraftPolicyPageDetails(Hashtable<String, String> data)throws Throwable{
		//Do you intend to quote and insure any vessel greater than or equal to 27 ft?
		selectLexisNexisQuestion("No");
		fillInsuranceInfo(data);
		fillAdditionalWatercraftDetails(data);
		fillOperatorDetails(data);
	}
	public void selectLexisNexisQuestion(String value)throws Throwable{
		if(value.equalsIgnoreCase("YES"))
			click(WatercraftPolicyPage.lexisNexisQuestionYes, "Do you intend to quote and insure any vessel greater than or equal to 27 ft? : Yes");
		else
			click(WatercraftPolicyPage.lexisNexisQuestionNo, "Do you intend to quote and insure any vessel greater than or equal to 27 ft? : No");
	}
	public void verifyOperatorTagNotPresent(Hashtable<String, String> data)throws Throwable{
		int noOfOperators = Integer.valueOf(data.get("NoOfOperators"));
		List<WebElement> invalidElems = driver.findElements(By.xpath("//div[@id='accountsColumn']//a"));
		boolean status = false;
		outer:
			for(WebElement e : invalidElems){
				for(int i = 2; i<=noOfOperators;i++){
					if(e.getAttribute("innerText").contains(data.get("operator" + i + "_firstName"))){
						status = true; break outer;
					}
				}
			}
		if(status)
			this.reporter.failureReport("Operator name node", "Operator named node present.", this.driver);
		else
			this.reporter.SuccessReport("Operator name node", "Operator named node not present.");
	}
	public void verifyManuscriptTagNotPresent()throws Throwable{
		List<WebElement> invalidElems = driver.findElements(By.xpath("//div[@id='accountsColumn']//a"));
		boolean status = false;
		for(WebElement e : invalidElems){
			if(e.getAttribute("innerText").contains("Manuscript Endorsements")){
				status = true; break;
			}
		}
		if(status)
			this.reporter.failureReport("Operator name node", "Operator named node present.", this.driver);
		else
			this.reporter.SuccessReport("Manuscript Endorsements node", "Manuscript Endorsements node not present.");
	}
	public void verifyDefaults() throws Throwable{
		String[] priorValues = {"AIG Private Client Group", "AIG", "Chubb","Lloyd's","Metropolitan Property and Casualty Ins", "Progressive", "Travelers", "Other", "Boat US", "Geico", "Great American", "Ironshore", "Pantaenius"};
		Select s = new Select(driver.findElement(WatercraftPolicyPage.priorCarrierDropDown));
		List<WebElement> options = s.getOptions();
		//for(WebElement e: options)System.out.println(e.getText());
		for(int counter = 0; counter < options.size()-1; counter++)
			//assertTrue(Arrays.asList(priorValues).contains(options.get(counter+1).getText()), "Verify " + priorValues[counter] + " in Prior Carriers");
			assertTrue(Arrays.asList(priorValues).contains(options.get(counter+1).getText()), "Verify " + options.get(counter+1).getText() + " in Prior Carriers");
		String[] relationToInsured = {"Captain Fulltime", "Captain Part-time", "Crew Member Fulltime", "Crew Member Part-time",
				"Daughter", "Domestic Partner", "Domestic Staff", "Fiancee", "Friend", "Named Insured", "Other", "Other Family Member",
				"Son","Spouse"};
		s = new Select(driver.findElement(WatercraftPolicyPage.relationToInsuredDropdown));
		options = s.getOptions();
		for(int counter = 0; counter < options.size()-1; counter++)
			assertTrue(Arrays.asList(relationToInsured).contains(options.get(counter+1).getText()), "Verify " + options.get(counter+1).getText() + " in Relation to Insured");
		String[] policyType = {"Hull and Liability", "Liability Only", "Property Only"};
		s = new Select(driver.findElement(WatercraftPolicyPage.policyType));
		options = s.getOptions();
		for(int counter = 0; counter < options.size()-1; counter++)
			assertTrue(policyType[counter].equalsIgnoreCase(options.get(counter).getText()), "Verify " + policyType[counter] + " in Policy Type");
	}

}
