package com.pure.dragon.watercraft;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.AgencyListLib;
import com.pure.dragon.BillingPageLib;
import com.pure.dragon.FinalSummaryPageLib;
import com.pure.dragon.HomeLib;
import com.pure.dragon.InsuranceScoreManagementLib;
import com.pure.dragon.NewQuoteBasicInformationLib;
import com.pure.dragon.NewQuoteBasicInformationPage;
import com.pure.dragon.PolicyCurrentSummaryPageLib;
import com.pure.dragon.PolicyDeliveryPageLib;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.PremiumPageLib;
import com.pure.dragon.QuotesLib;
import com.pure.dragon.SubjectivitiesPageLib;
import com.pure.dragon.SummaryPageLib;
import com.pure.dragon.UnderwriterReferralsPageLib;
import com.pure.dragon.enumSearchBy;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.dragon.homeOwners.QuoteDetailsPageLib;
import com.pure.report.CReporter;

public class QuoteCreation extends ActionEngine{
	public QuoteCreation(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public String createQuote(Hashtable<String, String> data) throws Throwable {
		openApplication("UW");
		initiateQuoteCreation(data);	
		if(isVisibleOnly(NewQuoteBasicInformationPage.nxtBtn, "Next Button")){
			new NewQuoteBasicInformationLib(driver, reporter).clickNxtBtn();
		}
		//Credit score
		QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
		if(data.get("Insurance_Score")!=null && data.get("Insurance_Score")!="0" && !data.get("Insurance_Score").contains("No Hit")){
			quoteDetailsPageLib.navigateToCustomer();
			new InsuranceScoreManagementLib(driver, reporter).addNewInsuranceScore(data);					
			//Unerwriter referral
			UnderwriterReferralsPageLib.insuranceScore=data.get("Insurance_Score");
		}
		WatercraftPolicyPageLib policyPage = new WatercraftPolicyPageLib(driver, reporter);
		policyPage.verifyDefaults();
		policyPage.fillWatercraftPolicyPageDetails(data);
		new CommonPageLib(driver, reporter).clickSaveChangesBtn();
		policyPage.verifyOperatorTagNotPresent(data);
		NavigationPageLib navigator = new NavigationPageLib(driver, reporter);
		int noOfVessels = Integer.valueOf(data.get("NoOfVessels"));
		for(int count = 1; count<=noOfVessels;count++){
			if(Integer.valueOf(data.get("watercraft" + count + "_length")) <27){
				fillWatercraftDetailsUnder27(data, count, "UW");
			}else{
				fillWatercraftDetailsAbove27(data, count, "UW");
			}
		}
		navigator.goToPreviousClaims();
		PreviousClaimsPageLib previousClaims = new PreviousClaimsPageLib(driver, reporter);
		previousClaims.fillWatercraftLossHistory(data);		
		previousClaims.fillAutoIncidents(data);
		previousClaims.fillWatercraftLossHistoryForPriorVessels(data);

		navigator.goToSubjectivities();
		// Click rate button
		CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
		commonPageLib.clickRateBtn();
		//Verify Coverage for additional tenders on premium details tab
		
		//Premium details
		new PremiumPageLib(driver, reporter).clickPremiumDetailsButton();
		//verify final premium
		PremiumDetailsLib premiumDetailsLib =  new PremiumDetailsLib(driver, reporter);
		premiumDetailsLib.verifyBasePremium(data);
		premiumDetailsLib.clickReturnToPremiumSummaryPage();

		// Navigate to underwriter referrals section
		UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
		uwReferralsPageLib.clickUnderwriterReferralsTab();
		//uwReferralsPageLib.verifyUWReferralsForWC(data); Use this for UW referral verifications

		uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
		uwReferralsPageLib.fillOverridden();
		// Select Underwriter
		uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
		// Comments for broker
		uwReferralsPageLib.setCommentsForBroker(data.get("uwComments"));
		// Accept quote
		uwReferralsPageLib.clickAcceptButton();

		// Summary Page - Request issue
		SummaryPageLib summaryPageLib = new SummaryPageLib(driver, reporter);
		summaryPageLib.clickRequestIssueButton();
		PremiumPageLib premiumPageLib = new PremiumPageLib(driver, reporter);
		premiumPageLib.clickQuoteProposalButton();
		//		for FL state
		uwReferralsPageLib.fillUnderwriterDetailsForFL(data);
		// Bind policy
		premiumPageLib.clickBindButton();

		// Select document delivery type
		PolicyDeliveryPageLib policyDeliveryPageLib = new PolicyDeliveryPageLib(driver, reporter);
		policyDeliveryPageLib.selectDocumentDeliveryType(data);

		// Click next to go to Billing section
		policyDeliveryPageLib.clickNextButton();

		// Select bill delivery
		BillingPageLib billingPageLib = new BillingPageLib(driver, reporter);
		billingPageLib.selectBillingAddress("SendToBilling");

		// click confirm button
		billingPageLib.clickConfirmBtn();

		// Request Bind
		FinalSummaryPageLib finalSummaryPageLib = new FinalSummaryPageLib(driver, reporter);
		finalSummaryPageLib.clickRequestBind();

		// Verify generated policy number
		PolicyCurrentSummaryPageLib policyCurrentSummaryPageLib = new PolicyCurrentSummaryPageLib(driver, reporter);
		policyCurrentSummaryPageLib.verifyPolicyGenerated();
		PolicyEndorsementLib policyEndorsement = new PolicyEndorsementLib(driver, reporter);
		//navigate to endorsement tab
		policyEndorsement.navigateToTransationOrEndorsementTab();
		policyEndorsement.navigateToNBtransaction();
		policyEndorsement.navigateToSubjectivities();

		// override subjectivities
		SubjectivitiesPageLib subjectivitiesPageLib = new SubjectivitiesPageLib(driver, reporter);
		subjectivitiesPageLib.overrideValues();
		// Manually bind the policy
		subjectivitiesPageLib.clickManualBindButton();
		return policyCurrentSummaryPageLib.getPolicyNumber();
	}

	public void initiateQuoteCreation(Hashtable<String, String> data) throws Throwable{
		HomeLib homeLib = new HomeLib(driver, reporter);
		homeLib.login(data.get("username"), data.get("password"));
		// openApplicationViaTiles();
		homeLib.navigateToQuotes();
		QuotesLib quotesLib = new QuotesLib(driver, reporter);
		quotesLib.clickNewQuoteButton();
		AgencyListLib agencyListLib = new AgencyListLib(driver, reporter);
		agencyListLib.searchAgency(enumSearchBy.BrokerName,
				data.get("brokerName"));
		agencyListLib.SelectAgency(data.get("brokerName"));
		NewQuoteBasicInformationLib newQuote_BasicInformationLib = new NewQuoteBasicInformationLib(driver, reporter);
		newQuote_BasicInformationLib.fillDetails(data);
		newQuote_BasicInformationLib.clickNxtBtn();
	}
	public void initiateQuoteCreationAgent(Hashtable<String, String> data) throws Throwable{
		HomeLib homeLib = new HomeLib(driver, reporter);
		openApplication("UW");
		homeLib.loginAsAgent(data.get("brokerNumber"), data.get("usernameAgent"), data.get("passwordAgent"));
		homeLib.navigateToQuotes_Agent();
		NewQuoteBasicInformationLib newQuote_BasicInformationLib = new NewQuoteBasicInformationLib(driver, reporter);
		newQuote_BasicInformationLib.fillDetails(data);
		newQuote_BasicInformationLib.clickNxtBtn();
	}

	public void fillWatercraftDetailsUnder27(Hashtable<String, String> data, int counter, String role)throws Throwable{
		String year = data.get("watercraft" + counter + "_year");
		String manufacturer = data.get("watercraft" + counter + "_manufacturer");
		String model = data.get("watercraft" + counter + "_model");
		NavigationPageLib navigator = new NavigationPageLib(driver, reporter);
		//Vessel Description page
		navigator.goToVessel(year, manufacturer, model);
		//---------------underwritter referral enabler
		UnderwriterReferralsPageLib.yearOfHull=year;		
		//---------------
		VesselDescriptionPageLib vesselDesc = new VesselDescriptionPageLib(driver, reporter);
		VesselUnderwritingPageLib vesselUW = new VesselUnderwritingPageLib(driver, reporter);
		//vesselDesc.verifyUnder27(data, counter);
		vesselDesc.fillVesselPriceLookUp(data, counter);
		vesselDesc.fillVesselEngineDetails(data, counter);		
		vesselDesc.fillBerthingLocation(data, counter);	
		vesselDesc.fillAdditionalWatersNavigated(data,counter, role);
		vesselDesc.fillSeverWeatherPlanDetails(data, counter);
		vesselUW.fillSelectCoverage(data, counter, role);
		if(!"Liability Only".equalsIgnoreCase(data.get("watercraft" + counter + "_policyType")))
			vesselUW.fillAdditionalCoverageUnder27(data, counter);
		else{
			verifyElementPresent(VesselUnderwritingPage.additionalCoverageBlock, "Additional Coverage Block", false);
		}
		vesselUW.fillVesselUWDetailsUnder27(data, counter);
		if(role.equalsIgnoreCase("UW"))
			vesselUW.fillOptionalCoverageDetails(data, counter);
		vesselUW.selectVesselTitleInNameOf(data.get("watercraft" + counter + "_inNameOf"));
		vesselUW.selectOwnership(data,counter);
		//Vessel manuscript page
		if(role.equalsIgnoreCase("UW") && "YES".equalsIgnoreCase(data.get("watercraft" + counter + "_manuscript"))){
			navigator.goToVesselManuscript(year, manufacturer, model);
			ManuscriptPageLib manuscriptPage = new ManuscriptPageLib(driver, reporter);
			manuscriptPage.clickAddManuscript();
			manuscriptPage.fillManuscriptDetails(data, counter);
			navigator.goToVesselManuscriptIndividual(year, manufacturer, model, data.get("watercraft" + counter + "_manuscriptTitle"));
			manuscriptPage.fillManuscriptIndividualDetails(data, counter);
			new CommonPageLib(driver, reporter).clickSaveChangesBtn();
		}
	}

	public void fillWatercraftDetailsAbove27(Hashtable<String, String> data, int counter, String role)throws Throwable{
		String year = data.get("watercraft" + counter + "_year");
		String manufacturer = data.get("watercraft" + counter + "_manufacturer");
		String model = data.get("watercraft" + counter + "_model");
		NavigationPageLib navigator = new NavigationPageLib(driver, reporter);
		navigator.goToVessel(year, manufacturer, model);
		//---------------underwritter referral enabler
		UnderwriterReferralsPageLib.yearOfHull=year;
		//---------------
		VesselDescriptionPageLib vesselDesc = new VesselDescriptionPageLib(driver, reporter);
		VesselUnderwritingPageLib vesselUW = new VesselUnderwritingPageLib(driver, reporter);
		//vesselDesc.verifyAbove27(data, counter, role);
		vesselDesc.fillVesselPriceLookUp(data, counter);
		vesselDesc.fillVesselDetails(data, counter);
		vesselDesc.fillEngineDetails(data, counter);		
		vesselDesc.fillBerthingLocation(data, counter);	

		vesselDesc.fillAdditionalWatersNavigated(data,counter, role);
		vesselDesc.fillSeverWeatherPlanDetails(data, counter);			
		navigator.goToVesselUW(year, manufacturer, model);
		vesselUW.selectVesselTitleInNameOf(data.get("watercraft" + counter + "_inNameOf"));
		vesselUW.selectOwnership(data, counter);
		vesselUW.fillSelectCoverage(data, counter, role);
		if(!"Liability Only".equalsIgnoreCase(data.get("watercraft" + counter + "_policyType"))){
			if(role.equalsIgnoreCase("UW")){
				vesselUW.fillAdditionalCoverageAbove27(data, counter);
			}else{
				vesselUW.fillAdditionalCoverageAbove27Agent(data, counter);
			}
		}else{
			verifyElementPresent(VesselUnderwritingPage.additionalCoverageBlock, "Additional Coverage Block", false);
		}
		vesselUW.fillVesselUWDetailsAbove27(data, counter);
		if(role.equalsIgnoreCase("UW"))
			vesselUW.fillOptionalCoverageDetails(data, counter);
		vesselUW.fillOperatorExperienceDetails(data, counter);

		//Vessel manuscript page
		if(role.equalsIgnoreCase("UW") && "YES".equalsIgnoreCase(data.get("watercraft" + counter + "_manuscript"))){
			navigator.goToVesselManuscript(year, manufacturer, model);
			ManuscriptPageLib manuscriptPage = new ManuscriptPageLib(driver, reporter);
			manuscriptPage.clickAddManuscript();
			manuscriptPage.fillManuscriptDetails(data, counter);
			navigator.goToVesselManuscriptIndividual(year, manufacturer, model, data.get("watercraft" + counter + "_manuscriptTitle"));
			manuscriptPage.fillManuscriptIndividualDetails(data, counter);
			new CommonPageLib(driver, reporter).clickSaveChangesBtn();
		}
	}

	public String createQuoteAgent(Hashtable<String, String> data) throws Throwable {
		openApplication("UW");
		initiateQuoteCreationAgent(data);
		if(isVisibleOnly(NewQuoteBasicInformationPage.nxtBtn, "Next Button")){
			new NewQuoteBasicInformationLib(driver, reporter).clickNxtBtn();
		}
		WatercraftPolicyPageLib policyPage = new WatercraftPolicyPageLib(driver, reporter);
		policyPage.fillWatercraftPolicyPageDetails(data);
		new CommonPageLib(driver, reporter).clickSaveChangesBtn();
		policyPage.verifyOperatorTagNotPresent(data);
		policyPage.verifyManuscriptTagNotPresent();
		policyPage.verifyDefaults();
		NavigationPageLib navigator = new NavigationPageLib(driver, reporter);
		int noOfVessels = Integer.valueOf(data.get("NoOfVessels"));
		for(int count = 1; count<=noOfVessels;count++){
			if(Integer.valueOf(data.get("watercraft" + count + "_length")) <27){
				fillWatercraftDetailsUnder27(data, count, "Agent");
			}else{
				fillWatercraftDetailsAbove27(data, count, "Agent");
			}
		}
		navigator.goToPreviousClaims();
		PreviousClaimsPageLib previousClaims = new PreviousClaimsPageLib(driver, reporter);
		previousClaims.fillWatercraftLossHistory(data);
		previousClaims.fillAutoIncidents(data);
		previousClaims.fillWatercraftLossHistoryForPriorVessels(data);
		//		navigator.goToSubjectivities();
		// Click rate button
		CommonPageLib commonPageLib = new CommonPageLib(driver, reporter);
		commonPageLib.clickRateBtn();
		//Premium details
		PremiumPageLib premiumPageLib = new PremiumPageLib(driver, reporter);
		/*premiumPageLib.clickPremiumDetailsButton();
		//verify final premium
		PremiumDetailsLib premiumDetailsLib =  new PremiumDetailsLib(driver, reporter);
		premiumDetailsLib.verifyBasePremium(data);
		premiumDetailsLib.clickReturnToPremiumSummaryPage();*/

		// Verify Premium tab is active
		premiumPageLib.verifyPremiumTabActive();
		String customerID = premiumPageLib.getCustomerID();
		premiumPageLib.clickReferToUWButton();

		premiumPageLib.clickContinueUWReferButton();
		premiumPageLib.fillCommentsSubmitToUW(data.get("brokerComments"));
		//Agent logout
		new HomeLib(driver, reporter).agentLogout();

		return customerID;
	}
}
