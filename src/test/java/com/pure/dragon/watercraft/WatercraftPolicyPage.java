package com.pure.dragon.watercraft;

import org.openqa.selenium.By;

public class WatercraftPolicyPage {
	static By termDropDown, effectiveDateText;
	static By politicalFigureYes, politicalFigureNo;
	static By priorCarrierDropDown, premiumText;
	static By addWatercraftButton, deleteWatercraftButton;
	static By policyType, yearText, manufacturerText, modelText, hullLimitText, lengthText, liabilityDropDown;
	static By addOperatorButton, deleteOperatorButton;
	static By firstNameText, lastNameText, dobText, relationToInsured, relationToInsuredDropdown,
	lexisNexisQuestionYes, lexisNexisQuestionNo;
	static By listOfWCObtainedViaReports;
	static By namedInsuredDropdown, trustLLCOtherLegalEntity;
	static{
		termDropDown = By.cssSelector("select[title='Term']");
		effectiveDateText = By.cssSelector("input[title='Effective Date']");
		
		politicalFigureYes = By.cssSelector("input[name$='_29719614'][title='Yes']");
		politicalFigureNo = By.cssSelector("input[name$='_29719614'][title='No']");
		
		priorCarrierDropDown = By.cssSelector("select[id$='_29675614']");
		premiumText = By.cssSelector("input[id$='28921305']");
		
		policyType = By.cssSelector("select[title='Policy Type']");
		yearText = By.cssSelector("input[id*='_29650414'][title='Year']");
		manufacturerText = By.cssSelector("input[id*='_29650414'][title='Manufacturer']");
		modelText = By.cssSelector("input[id*='_29650414'][title='Model']");
		hullLimitText = By.cssSelector("input[id*='_18428933'][title='Hull and Machinery Limit']");
		lengthText = By.cssSelector("input[id*='_29650414'][title='Length(ft)']");
		liabilityDropDown = By.cssSelector("select[id*='_29650414'][title='Liability only (Y/N)']");
		
		addWatercraftButton = By.xpath("//div[text()='additional watercrafts']/..//a[contains(@onclick,'Action.189')]");
		deleteWatercraftButton = By.xpath("//div[text()='additional watercrafts']/../..//a[contains(@onclick, 'Action.190')]");
		
		firstNameText = By.cssSelector("input[id*='_29646214'][title='First Name'][class='unFilledMandatoryField']");
		lastNameText = By.cssSelector("input[id*='_29646214'][title='Last Name'][class='unFilledMandatoryField']");
		dobText = By.cssSelector("input[id*='_29646214'][title='Date of Birth'][class*='unFilledMandatoryField']");
		relationToInsured = By.cssSelector("select[id$='_29653514'][title='Relationship to Insured'][class='unFilledMandatoryField']");
		relationToInsuredDropdown = By.cssSelector("select[id$='_29653514'][title='Relationship to Insured']");
		addOperatorButton = By.xpath("//div[text()='list of operators (including captain)']/..//a[contains(@onclick,'Action.189')]");
		deleteOperatorButton = By.xpath("//div[text()='list of operators (including captain)']/../..//a[contains(@onclick, 'Action.190')]");
		
		lexisNexisQuestionNo = By.xpath("//div[@title='Do you intend to quote and insure any vessel greater than or equal to 27 ft?']/..//input[@title='No']");
		lexisNexisQuestionYes = By.xpath("//div[@title='Do you intend to quote and insure any vessel greater than or equal to 27 ft?']/..//input[@title='Yes']");
		
		listOfWCObtainedViaReports = By.xpath("//div[@class='headerLabel'][contains(text(),'watercraft obtained through online reports')]/../..//table[@class='objectListTable']/tbody/tr");
		namedInsuredDropdown = By.xpath("//div[@title='Policyholder types']/..//select");
		trustLLCOtherLegalEntity = By.xpath("//input[@title='Trust, LLC or Other Legal Entity']");
	}
}
