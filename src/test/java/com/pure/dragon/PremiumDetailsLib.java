package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PremiumDetailsLib extends ActionEngine{

	public PremiumDetailsLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public String getBaseRateNonHurricaneValue() throws Throwable{
		if(isVisibleOnly(PremiumDetailsPage.baseRateNonHurricaneValue, "baseRateNonHurricaneValue")){
			String baseRate = getText(PremiumDetailsPage.baseRateNonHurricaneValue, "baseRateNonHurricaneValue");
			return baseRate.replace(",", "");
		}else{
			return "Not Available";
		}
	}
	public String getBaseRateHurricaneValue() throws Throwable{
		if(isVisibleOnly(PremiumDetailsPage.baseRateHurricaneValue, "baseRateHurricaneValue")){
			String baseRate = getText(PremiumDetailsPage.baseRateHurricaneValue, "baseRateHurricaneValue");
			return baseRate.replace(",", "");
		}else{
			return "Not Available";
		}
	}
	public String getBaseRateValue() throws Throwable{
		if(isVisibleOnly(PremiumDetailsPage.baseRateValue, "Base RateValue")){
			String baseRate = getText(PremiumDetailsPage.baseRateValue, "Base RateValue");
			return baseRate.replace(",", "");
		}else{
			return "Not Available";
		}
	}

	public String getCreditScoreFactor() throws Throwable{
		if(isVisibleOnly(PremiumDetailsPage.creditScoreFactor, "Credit score factor")){
			String creditScore = getText(PremiumDetailsPage.creditScoreFactor, "Credit score factor");
			return creditScore.replace(",", "");			
		}else{
			return "Not Available";
		}
	}

	public String getAgeOfHomeFactor() throws Throwable{
		if(isVisibleOnly(PremiumDetailsPage.ageOfHomeFactor, "Age of Home factor")){
			String homeFactor = getText(PremiumDetailsPage.ageOfHomeFactor, "Age of Home factor");
			return homeFactor.replace(",", "");
		}else{
			return "Not Available";
		}
	}

	public String getAOPDeductibleFactor() throws Throwable{
		if(isVisibleOnly(PremiumDetailsPage.aopDeductibleFactor, "AOP deductible factor")){
			String AOPFactor = getText(PremiumDetailsPage.aopDeductibleFactor, "AOP deductible factor");
			return AOPFactor.replace(",", "");
		}else{
			return "Not Available";
		}
	}
	public void clickReturnToPremiumSummaryPage() throws Throwable{
		click(PremiumDetailsPage.returnToSummaryPremiumPage,"returnToSummaryPremiumPage");
	}


}
