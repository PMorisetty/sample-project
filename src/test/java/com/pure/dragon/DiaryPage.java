package com.pure.dragon;

import org.openqa.selenium.By;

public class DiaryPage {

	
	public static By diaryCancellation;
	public static By diaryTab;
	public static By diaryStatus;
	public static By cancellationEntity;
	public static By reinstatementEntity;
	public static By exitTransactionBtn;
	public static By exitPolicyBtn;
	
	static{
		diaryTab = By.xpath(".//a/span[contains(text(),'diary')]");
		diaryStatus = By.xpath(".//table[@id='bodyFrame']//tr[2]/td[4]/div/a");
		cancellationEntity = By.xpath("//a[contains(text(),'Cancellation')]");
		reinstatementEntity = By.xpath("//a[contains(text(),'Reinstatement')]");
		exitTransactionBtn = By.xpath("//div[@id='footer']//a[contains(@href,'Action.129205')]");
		exitPolicyBtn = By.xpath("//div[@id='footer']//a[contains(@href,'Action.88501')]");
	}
}
