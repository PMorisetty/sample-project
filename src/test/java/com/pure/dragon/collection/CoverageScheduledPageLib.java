package com.pure.dragon.collection;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CoverageScheduledPageLib extends ActionEngine {

	public CoverageScheduledPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void selectCollectionClass(String value) throws Throwable {
		selectByVisibleText(CoverageScheduledPage.collectionClass, value, "CollectionClass");
	}

	public void setNoOfItems(String value) throws Throwable {
		type(CoverageScheduledPage.noOfItems, value, "NoOfItems");
	}

	public void clickAdd() throws Throwable {
		click(CoverageScheduledPage.addCollectionClass, "AddCollectionClass");
	}

	public void setDescriptionOfItem(String collectionClass, String value) throws Throwable {
		String[] itemList = value.split(",");
		for (int i = 0; i < itemList.length; i++) {
			int x = i + 1;
			By locator = By.xpath("//table[@class='objectListTable']//tr[@style][" + x + "]//div/label[text()='"
					+ collectionClass + "']/../../../..//textarea[contains(@id,'_28971906')]");
			type(locator, itemList[i], "DescriptionOfItem");
		}
	}

	public void setLimitOfItem(String collectionClass, String value) throws Throwable {
		String[] limitList = value.split(",");
		for (int i = 0; i < limitList.length; i++) {
			int x = i + 1;
			By locator = By.xpath("//table[@class='objectListTable']//tr[@style][" + x + "]//div/label[text()='"
					+ collectionClass + "']/../../../..//input[contains(@id,'_28972006')]");
			type(locator, limitList[i], "LimitOfItem");
		}
	}

	public void setAppraisalDate(String collectionClass, String value, int... count) throws Throwable {
		String[] dateList = value.split(",");
		for (int i = 0; i < dateList.length; i++) {
			int x = i + 1;
			By locator = By.xpath("//table[@class='objectListTable']//tr[@style][" + x + "]//div/label[text()='"
					+ collectionClass + "']/../../../..//input[contains(@id,'_28972106')]");
			type(locator, dateList[i], "AppraisalDate");
			driver.findElement(locator).sendKeys(Keys.TAB);
			Thread.sleep(500);
		}
	}

	public void checkboxDelete(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(CoverageScheduledPage.deleteCheckbox, String.valueOf(num));
		click(By.xpath(tempLocator), "DeleteCheckbox");
	}

	public int getNoOfItemsPresent() {
		By locator = By.xpath("//div[text()='worldwide jewelry']/../..//table[@class='objectListTable']//tr[@style]");
		int size = this.driver.findElements(locator).size();
		return size;

	}

	public void addAnotheItem(Hashtable<String, String> data) throws Throwable {
		int noOfcollections = Integer.valueOf(data.get("NoOfCollectons"));
		for (int iterator = 1; iterator <= noOfcollections ; iterator++) {
			if(!(data.get("CollectionClass_" + iterator).equals("") || data.get("CollectionClass_" + iterator).equals("NULL"))){
			
				selectCollectionClass(data.get("CollectionClass_" + iterator));
				setNoOfItems(data.get("NoOfItems_" + iterator));
				clickAdd();
				fillNewItemDetails(iterator, data);
			}
		}
	}

	public void fillNewItemDetails(int count, Hashtable<String, String> data) throws Throwable {
		setDescriptionOfItem(data.get("CollectionClass_" + count), data.get("DescriptionOfItem_" + count));
		setLimitOfItem(data.get("CollectionClass_" + count), data.get("LimitOfItem_" + count));
		setAppraisalDate(data.get("CollectionClass_" + count), data.get("AppraisalDate_" + count));
	}
}
