package com.pure.dragon.collection;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.dragon.watercraft.VesselDescriptionPage;
import com.pure.report.CReporter;

public class CollectionPolicyPageLib extends ActionEngine {

	public CollectionPolicyPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	
	// Policy Information
	public void selectTerm(String value) throws Throwable {
		selectByVisibleText(CollectionPolicyPage.term, value, "Term");
	}

	public void setQuoteName(String value) throws Throwable {
		type(CollectionPolicyPage.quoteName, value, "quoteName");
	}

	public void selectLicensedProducer(String value) throws Throwable {
		selectByVisibleText(CollectionPolicyPage.licensedProducer, value, "licensedProducer");
	}

	public void selectCurrentCollInsuranceCompany(String value) throws Throwable {
		selectByVisibleText(CollectionPolicyPage.currentCollInsuranceCompany, value, "currentCollInsuranceCompany");
	}

	public void setEffectiveDate(String value) throws Throwable {
		type(CollectionPolicyPage.effectiveDate, value, "effectiveDate");
	}

	public void selectAdvisorOrServicer(String value) throws Throwable {
		selectByVisibleText(CollectionPolicyPage.advisorOrServicer, value, "advisorOrServicer");
	}

	public void setCurrentPremium(String value) throws Throwable {
		type(CollectionPolicyPage.currentPremium, value, "currentPremium");
	}

	public void selectNamedInsuredType(String value) throws Throwable {
		selectByVisibleText(CollectionPolicyPage.namedInsuredType, value, "namedInsuredType");
	}

	// Named Insured - Individual
	public void selectNamedInsured(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(CollectionPolicyPage.namedInsured, String.valueOf(num));
		selectByVisibleText(By.xpath(tempLocator), value, "namedInsured");
	}

	public void selectPrefix(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(CollectionPolicyPage.prefix, String.valueOf(num));
		selectByVisibleText(By.xpath(tempLocator), value, "prefix");
	}

	public void setFirstName(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(CollectionPolicyPage.firstName, String.valueOf(num));
		type(By.xpath(tempLocator), value, "firstName");
	}

	public void setLastName(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(CollectionPolicyPage.lastName, String.valueOf(num));
		type(By.xpath(tempLocator), value, "lastName");
	}

	public void selectSuffix(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(CollectionPolicyPage.suffix, String.valueOf(num));
		selectByVisibleText(By.xpath(tempLocator), value, "suffix");
	}

	public void setSocialSecurityNumber(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(CollectionPolicyPage.socialSecurityNumber, String.valueOf(num));
		type(By.xpath(tempLocator), value, "socialSecurityNumber");
	}

	public void clickDeleteBtn(int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(CollectionPolicyPage.deleteBtn, String.valueOf(num));
		click(By.xpath(tempLocator), "deleteBtn");
	}

	public void clickAdd() throws Throwable {
		click(CollectionPolicyPage.addAnotherItem, "Add another item");
	}

	// Risk location Characteristics
	public void radioBtn_BurglarAlarm(String value) throws Throwable {
		driver.findElement(CollectionPolicyPage.burglarAlarmYES).sendKeys(Keys.PAGE_DOWN);
		if ("YES".equalsIgnoreCase(value))
			click(CollectionPolicyPage.burglarAlarmYES, "burglarAlarmYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CollectionPolicyPage.burglarAlarmNO, "burglarAlarmNO");
	}

	public void radioBtn_FireAlarm(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CollectionPolicyPage.fireAlarmYES, "fireAlarmYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CollectionPolicyPage.fireAlarmNO, "fireAlarmNO");
	}

	public void radioBtn_HomeUnoccupied(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CollectionPolicyPage.homeUnoccupiedYES, "homeUnoccupiedYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CollectionPolicyPage.homeUnoccupiedNO, "homeUnoccupiedNO");
	}

	public void radioBtn_Generator(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CollectionPolicyPage.generatorYES, "generatorYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CollectionPolicyPage.generatorNO, "generatorNO");
	}

	public void radioBtn_InsuredRisk(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CollectionPolicyPage.insuredRiskYES, "insuredRiskYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CollectionPolicyPage.insuredRiskNO, "insuredRiskNO");
	}

	public void selectHomeConstructionType(String value) throws Throwable {
		selectByVisibleText(CollectionPolicyPage.homeConstructionType, value, "homeConstructionType");
	}

	public void selectProtectionClassCode(String value) throws Throwable {
		selectByVisibleText(CollectionPolicyPage.protectionClassCode, value, "protectionClassCode");
	}
	
	public void set_YearBuilt(String value) throws Throwable {
		type(CollectionPolicyPage.yearBuilt, value, "YearBuilt");
	}
	
	public void set_NoOfFloorsAtRiskLocation(String value) throws Throwable {
		type(CollectionPolicyPage.noOfFloorsAtRiskLocation, value, "NoOfFloorsAtRiskLocation");
	}
	
	public void radioBtn_HomeLocatedOnSlope(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CollectionPolicyPage.homeLocatedOnSlopeYES, "homeLocatedOnSlopeYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CollectionPolicyPage.homeLocatedOnSlopeNO, "homeLocatedOnSlopeNO");
	}

	public void radioBtn_HomeSupported(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CollectionPolicyPage.homeSupportedYES, "homeSupportedYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CollectionPolicyPage.homeSupportedNO, "homeSupportedNO");
	}

	public void radioBtn_AutoSeismicValve(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CollectionPolicyPage.autoSeismicValveYES, "AutoSeismicValveYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CollectionPolicyPage.autoSeismicValveNO, "AutoSeismicValveNO");
	}

	public int getNoOfItemsPresent() {
		By locator = By.xpath("//table[@class='objectListTable']//tr[@style]");
		int size = driver.findElements(locator).size();
		return size;

	}

	public void addAnotheItem(Hashtable<String, String> data) throws Throwable {
		int noOfItems = Integer.valueOf(data.get(""));
		for (int iterator = 0; iterator < noOfItems; iterator++) {
			int noOfItemsPresent = getNoOfItemsPresent();
			clickAdd();
			fillNewItemDetails(noOfItemsPresent + 1, data);
		}
		;
	}

	public void fillNewItemDetails(int count, Hashtable<String, String> data) throws Throwable {
		selectNamedInsured(data.get("operator" + count + "_prefix"), count);
		selectPrefix(data.get("operator" + count + "_firstName"), count);
		setFirstName(data.get("operator" + count + "_middleName"), count);
		setLastName(data.get("operator" + count + "_lastName"), count);
		selectSuffix(data.get("operator" + count + "_lastName"), count);
		clickDeleteBtn(count);
	}

	public void fillAllFields(Hashtable<String, String> data) throws Throwable {
		switch(data.get("state")){
		case "CA":
			radioBtn_BurglarAlarm(data.get("BurglarAlarm"));
			radioBtn_FireAlarm(data.get("FireAlarm"));
			radioBtn_Generator(data.get("Generator"));
			radioBtn_HomeUnoccupied(data.get("HomeUnoccupied"));
			selectHomeConstructionType(data.get("HomeConstructionType"));
			selectProtectionClassCode(data.get("ProtectionClassCode"));
			set_YearBuilt(data.get("YearBuilt"));
			set_NoOfFloorsAtRiskLocation(data.get("NoOfFloorsAtRiskLocation"));
			radioBtn_HomeLocatedOnSlope(data.get("HomeLocatedOnSlope"));
			radioBtn_HomeSupported(data.get("HomeSupported"));
			radioBtn_AutoSeismicValve(data.get("AutoSeismicValve"));
			break;
		default:
			radioBtn_BurglarAlarm(data.get("BurglarAlarm"));
			radioBtn_FireAlarm(data.get("FireAlarm"));
			radioBtn_Generator(data.get("Generator"));
			radioBtn_HomeUnoccupied(data.get("HomeUnoccupied"));
			radioBtn_InsuredRisk(data.get("InsuredRisk"));
			selectHomeConstructionType(data.get("HomeConstructionType"));
			selectProtectionClassCode(data.get("ProtectionClassCode"));
			break;
		}
	}

}
