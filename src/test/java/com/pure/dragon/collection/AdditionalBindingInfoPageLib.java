package com.pure.dragon.collection;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class AdditionalBindingInfoPageLib extends ActionEngine {
	
	public AdditionalBindingInfoPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void set_Occupation(String value) throws Throwable {
		type(AdditionalBindingInfoPage.occupation, value, "Occupation");
	}

	public void set_Employer(String value) throws Throwable {
		type(AdditionalBindingInfoPage.employer, value, "Employer");
	}

	public void radioBtn_ExistingAgencyClient(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(AdditionalBindingInfoPage.existingAgencyClientYES, "ExistingAgencyClientYES");
		else if ("NO".equalsIgnoreCase(value))
			click(AdditionalBindingInfoPage.existingAgencyClientNO, "ExistingAgencyClientNO");
	}

	public void radioBtn_AnyCompanyRefusedToInsure(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(AdditionalBindingInfoPage.anyCompanyRefusedToInsureYES, "AnyCompanyRefusedToInsureYES");
		else if ("NO".equalsIgnoreCase(value))
			click(AdditionalBindingInfoPage.anyCompanyRefusedToInsureNO, "AnyCompanyRefusedToInsureNO");
	}

	public void radioBtn_CoverageRenewed(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(AdditionalBindingInfoPage.coverageRenewedYES, "CoverageRenewedYES");
		else if ("NO".equalsIgnoreCase(value))
			click(AdditionalBindingInfoPage.coverageRenewedNO, "CoverageRenewedNO");
	}

}
