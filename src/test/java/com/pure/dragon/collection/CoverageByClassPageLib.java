package com.pure.dragon.collection;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CoverageByClassPageLib extends ActionEngine {

	public CoverageByClassPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	// Worldwide Jewelry
	public void setWorldwide_TotalScheduledJewelryCoverage(String value) throws Throwable {
		type(CoverageByClassPage.Worldwide_TotalScheduledJewelryCoverage, value,
				"Worldwide_TotalScheduledJewelryCoverage");
	}

	public void setWorldwide_TotalBlanketJewelryCoverage(String value) throws Throwable {
		type(CoverageByClassPage.Worldwide_TotalBlanketJewelryCoverage, value, "Worldwide_TotalBlanketJewelryCoverage");
	}

	public void setWorldwide_TotalJewelryCoverage(String value) throws Throwable {
		type(CoverageByClassPage.Worldwide_TotalJewelryCoverage, value, "Worldwide_TotalJewelryCoverage");
	}

	public void setWorldwide_ItemValue(String value) throws Throwable {
		type(CoverageByClassPage.Worldwide_ItemValue, value, "Worldwide_ItemValue");
	}

	public void setWorldwide_ItemDescription(String value) throws Throwable {
		type(CoverageByClassPage.Worldwide_ItemDescription, value, "Worldwide_ItemDescription");
	}

	public void selectWorldwide_BlanketCovSingleArtLimit(String value) throws Throwable {
		selectByVisibleText(CoverageByClassPage.Worldwide_BlanketCovSingleArtLimit, value,
				"Worldwide_BlanketCovSingleArtLimit");
	}

	public void radioBtnWorldwide_CollectionAppraisal(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.Worldwide_CollectionAppraisalYES, "Worldwide_CollectionAppraisalYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.Worldwide_CollectionAppraisalNO, "Worldwide_CollectionAppraisalNO");
	}

	public void radioBtnWorldwide_HomeSafe(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.Worldwide_HomeSafeYES, "Worldwide_HomeSafeYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.Worldwide_HomeSafeNO, "Worldwide_HomeSafeNO");
	}

	public void setWorldwide_VaultMaker(String value) throws Throwable {
		type(CoverageByClassPage.Worldwide_VaultMaker, value, "Worldwide_VaultMaker");
	}

	public void setWorldwide_VaultModel(String value) throws Throwable {
		type(CoverageByClassPage.Worldwide_VaultModel, value, "Worldwide_VaultModel");
	}

	public void radioBtnWorldwide_VaultSafeBolted(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.Worldwide_VaultSafeBoltedYES, "Worldwide_VaultSafeBoltedYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.Worldwide_VaultSafeBoltedNO, "Worldwide_VaultSafeBoltedNO");
	}

	public void radioBtnWorldwide_AddToCredit(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.Worldwide_AddToCreditYES, "Worldwide_AddToCreditYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.Worldwide_AddToCreditNO, "Worldwide_AddToCreditNO");
	}

	public void setWorldwide_MaxAmtOFJewelry(String value) throws Throwable {
		type(CoverageByClassPage.Worldwide_MaxAmtOFJewelry, value, "Worldwide_MaxAmtOFJewelry");
	}

	// Bank Vaulted Jewelry
	public void setBankVaulted_TotalScheduledJewelryCoverage(String value) throws Throwable {
		type(CoverageByClassPage.BankVaulted_TotalScheduledJewelryCoverage, value,
				"BankVaulted_TotalScheduledJewelryCoverage");
	}

	public void clickToExpandTable() throws Throwable {
		click(CoverageByClassPage.clickToExpandTable, "clickToExpandTable");
	}

	public void setBankVaulted_ItemValue(String value) throws Throwable {
		type(CoverageByClassPage.BankVaulted_ItemValue, value, "BankVaulted_ItemValue");
	}

	public void setBankVaulted_ItemDescription(String value) throws Throwable {
		type(CoverageByClassPage.BankVaulted_ItemDescription, value, "BankVaulted_ItemDescription");
	}

	public void radioBtnBankVaulted_CollectionAppraisal(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.BankVaulted_CollectionAppraisalYES, "BankVaulted_CollectionAppraisalYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.BankVaulted_CollectionAppraisalNO, "BankVaulted_CollectionAppraisalNO");
	}

	public void radioBtnBankVaulted_InsuredJewelryToCurrentValue(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.BankVaulted_InsuredJewelryToCurrentValueYES,
					"BankVaulted_InsuredJewelryToCurrentValueYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.BankVaulted_InsuredJewelryToCurrentValueNO,
					"BankVaulted_InsuredJewelryToCurrentValueNO");
	}

	// Fine Arts/ Collectibles
	public void setFineArtsCollectibles_TotalScheduledCoverage(String value) throws Throwable {
		type(CoverageByClassPage.FineArtsCollectibles_TotalScheduledCoverage, value, "FineArtsCollectibles_TotalScheduledCoverage");
	}

	public void setFineArtsCollectibles_TotalBlanketCoverage(String value) throws Throwable {
		type(CoverageByClassPage.FineArtsCollectibles_TotalBlanketCoverage, value, "FineArtsCollectibles_TotalBlanketCoverage");
	}

	public void setFineArtsCollectibles_TotalCoverage(String value) throws Throwable {
		type(CoverageByClassPage.FineArtsCollectibles_TotalCoverage, value, "FineArtsCollectibles_TotalCoverage");
	}

	public void setFineArtsCollectibles_ItemValue(String value) throws Throwable {
		type(CoverageByClassPage.FineArtsCollectibles_ItemValue, value, "FineArtsCollectibles_ItemValue");
	}

	public void setFineArtsCollectibles_ItemDescription(String value) throws Throwable {
		type(CoverageByClassPage.FineArtsCollectibles_ItemDescription, value, "FineArtsCollectibles_ItemDescription");
	}

	public void selectFineArtsCollectibles_BlanketCovSingleArtLimit(String value) throws Throwable {
		selectByVisibleText(CoverageByClassPage.FineArtsCollectibles_BlanketCovSingleArtLimit, value,
				"FineArtsCollectibles_BlanketCovSingleArtLimit");
	}

	public void radioBtnFineArtsCollectibles_CollectionAppraisal(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.FineArtsCollectibles_CollectionAppraisalYES, "FineArtsCollectibles_CollectionAppraisalYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.FineArtsCollectibles_CollectionAppraisalNO, "FineArtsCollectibles_CollectionAppraisalNO");
	}

	public void radioBtnFineArtsCollectibles_InsuredToCurrentValue(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.FineArtsCollectibles_InsuredToCurrentValueYES, "FineArtsCollectibles_InsuredToCurrentValueYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.FineArtsCollectibles_InsuredToCurrentValueNO, "FineArtsCollectibles_InsuredToCurrentValueNO");
	}
	
	// Fine Arts
		public void setFineArts_TotalScheduledCoverage(String value) throws Throwable {
			type(CoverageByClassPage.FineArts_TotalScheduledCoverage, value, "FineArts_TotalScheduledCoverage");
		}

		public void setFineArts_TotalBlanketCoverage(String value) throws Throwable {
			type(CoverageByClassPage.FineArts_TotalBlanketCoverage, value, "FineArts_TotalBlanketCoverage");
		}

		public void setFineArts_TotalCoverage(String value) throws Throwable {
			type(CoverageByClassPage.FineArts_TotalCoverage, value, "FineArts_TotalCoverage");
		}

		public void setFineArts_ItemValue(String value) throws Throwable {
			type(CoverageByClassPage.FineArts_ItemValue, value, "FineArts_ItemValue");
		}

		public void setFineArts_ItemDescription(String value) throws Throwable {
			type(CoverageByClassPage.FineArts_ItemDescription, value, "FineArts_ItemDescription");
		}

		public void selectFineArts_EarthquakeDeductible(String value) throws Throwable {
			selectByVisibleText(CoverageByClassPage.FineArts_EarthquakeDeductible, value,
					"FineArts_EarthquakeDeductible");
		}

		public void radioBtnFineArts_CollectionAppraisal(String value) throws Throwable {
			if ("YES".equalsIgnoreCase(value))
				click(CoverageByClassPage.FineArts_CollectionAppraisalYES, "FineArts_CollectionAppraisalYES");
			else if ("NO".equalsIgnoreCase(value))
				click(CoverageByClassPage.FineArts_CollectionAppraisalNO, "FineArts_CollectionAppraisalNO");
		}

		public void radioBtnFineArts_InsuredToCurrentValue(String value) throws Throwable {
			if ("YES".equalsIgnoreCase(value))
				click(CoverageByClassPage.FineArts_InsuredToCurrentValueYES, "FineArts_InsuredToCurrentValueYES");
			else if ("NO".equalsIgnoreCase(value))
				click(CoverageByClassPage.FineArts_InsuredToCurrentValueNO, "FineArts_InsuredToCurrentValueNO");
		}
		
		// Collectibles
		public void setCollectibles_TotalScheduledCoverage(String value) throws Throwable {
			type(CoverageByClassPage.Collectibles_TotalScheduledCoverage, value, "Collectibles_TotalScheduledCoverage");
		}

		public void setCollectibles_TotalBlanketCoverage(String value) throws Throwable {
			type(CoverageByClassPage.Collectibles_TotalBlanketCoverage, value, "Collectibles_TotalBlanketCoverage");
		}

		public void setCollectibles_TotalCoverage(String value) throws Throwable {
			type(CoverageByClassPage.Collectibles_TotalCoverage, value, "Collectibles_TotalCoverage");
		}

		public void setCollectibles_ItemValue(String value) throws Throwable {
			type(CoverageByClassPage.Collectibles_ItemValue, value, "Collectibles_ItemValue");
		}

		public void setCollectibles_ItemDescription(String value) throws Throwable {
			type(CoverageByClassPage.Collectibles_ItemDescription, value, "Collectibles_ItemDescription");
		}

		public void selectCollectibles_EarthquakeDeductible(String value) throws Throwable {
			selectByVisibleText(CoverageByClassPage.Collectibles_EarthquakeDeductible, value,
					"Collectibles_EarthquakeDeductible");
		}

		public void radioBtnCollectibles_CollectionAppraisal(String value) throws Throwable {
			if ("YES".equalsIgnoreCase(value))
				click(CoverageByClassPage.Collectibles_CollectionAppraisalYES, "Collectibles_CollectionAppraisalYES");
			else if ("NO".equalsIgnoreCase(value))
				click(CoverageByClassPage.Collectibles_CollectionAppraisalNO, "Collectibles_CollectionAppraisalNO");
		}

		public void radioBtnCollectibles_InsuredToCurrentValue(String value) throws Throwable {
			if ("YES".equalsIgnoreCase(value))
				click(CoverageByClassPage.Collectibles_InsuredToCurrentValueYES, "Collectibles_InsuredToCurrentValueYES");
			else if ("NO".equalsIgnoreCase(value))
				click(CoverageByClassPage.Collectibles_InsuredToCurrentValueNO, "Collectibles_InsuredToCurrentValueNO");
		}

	// Coins Silver Stamps
	public void setCoinsStamps_TotalScheduledCoverage(String value) throws Throwable {
		type(CoverageByClassPage.CoinsStamps_TotalScheduledJewelryCoverage, value,
				"CoinsStamps_TotalScheduledJewelryCoverage");
	}

	public void setCoinsStamps_TotalBlanketCoverage(String value) throws Throwable {
		type(CoverageByClassPage.CoinsStamps_TotalBlanketJewelryCoverage, value,
				"CoinsStamps_TotalBlanketJewelryCoverage");
	}

	public void setCoinsStamps_ItemValue(String value) throws Throwable {
		type(CoverageByClassPage.CoinsStamps_ItemValue, value, "CoinsStamps_ItemValue");
	}

	public void setCoinsStamps_ItemDescription(String value) throws Throwable {
		type(CoverageByClassPage.CoinsStamps_ItemDescription, value, "CoinsStamps_ItemDescription");
	}

	public void selectCoinsStamps_BlanketCovSingleArtLimit(String value) throws Throwable {
		selectByVisibleText(CoverageByClassPage.CoinsStamps_BlanketCovSingleArtLimit, value,
				"CoinsStamps_BlanketCovSingleArtLimit");
	}

	public void radioBtnCoinsStamps_CollectionAppraisal(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.CoinsStamps_CollectionAppraisalYES, "CoinsStamps_CollectionAppraisalYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.CoinsStamps_CollectionAppraisalNO, "CoinsStamps_CollectionAppraisalNO");
	}

	public void radioBtnCoinsStamps_InsuredToCurrentValue(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.CoinsStamps_InsuredToCurrentValueYES, "CoinsStamps_InsuredToCurrentValueYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.CoinsStamps_InsuredToCurrentValueNO, "CoinsStamps_InsuredToCurrentValueNO");
	}

	// Wine
	public void setWine_TotalScheduledCoverage(String value) throws Throwable {
		type(CoverageByClassPage.Wine_TotalScheduledJewelryCoverage, value, "Wine_TotalScheduledJewelryCoverage");
	}

	public void setWine_TotalBlanketCoverage(String value) throws Throwable {
		type(CoverageByClassPage.Wine_TotalBlanketJewelryCoverage, value, "Wine_TotalBlanketJewelryCoverage");
	}

	public void setWine_ItemValue(String value) throws Throwable {
		type(CoverageByClassPage.Wine_ItemValue, value, "Wine_ItemValue");
	}

	public void setWine_ItemDescription(String value) throws Throwable {
		type(CoverageByClassPage.Wine_ItemDescription, value, "Wine_ItemDescription");
	}

	public void selectWine_BlanketCovSingleArtLimit(String value) throws Throwable {
		selectByVisibleText(CoverageByClassPage.Wine_BlanketCovSingleArtLimit, value, "Wine_BlanketCovSingleArtLimit");
	}

	public void radioBtnWine_TemperatureAlarm(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.Wine_TemperatureAlarmYES, "Wine_TemperatureAlarmYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.Wine_TemperatureAlarmNO, "Wine_TemperatureAlarmNO");
	}

	public void radioBtnWine_CollectionAppraisal(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.Wine_CollectionAppraisalYES, "Wine_CollectionAppraisalYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.Wine_CollectionAppraisalNO, "Wine_CollectionAppraisalNO");
	}

	public void radioBtnWine_InsuredToCurrentValue(String value) throws Throwable {
		if ("YES".equalsIgnoreCase(value))
			click(CoverageByClassPage.Wine_InsuredToCurrentValueYES, "Wine_InsuredToCurrentValueYES");
		else if ("NO".equalsIgnoreCase(value))
			click(CoverageByClassPage.Wine_InsuredToCurrentValueNO, "Wine_InsuredToCurrentValueNO");
	}

	/*
	 * public void fillAllWorldwideJewelryFields(Hashtable<String, String> data)
	 * throws Throwable { set_TotalScheduledCoverageFor(EnumCoverageByClass.
	 * TotalScheduled_JewelryCoverage.value(), data.get(""));
	 * set_TotalBlanketJewelryCoverage
	 * (EnumCoverageByClass.TotalBlanket_JewelryCoverage.value(),data.get(""));
	 * // setWorldwide_TotalJewelryCoverage(data.get(""));
	 * setWorldwide_ItemValue(data.get(""));
	 * set_ItemDescription(EnumCoverageByClass.Description_Jewelry.value(),
	 * data.get("")); radioBtnWorldwide_CollectionAppraisal(data.get(""));
	 * radioBtnWorldwide_HomeSafe(data.get("")); }
	 * 
	 * public void fillAllBankVaultedJewelryFields(Hashtable<String, String>
	 * data) throws Throwable {
	 * set_TotalScheduledCoverageFor(EnumCoverageByClass
	 * .TotalScheduled_BankVaultCoverage.value(), data.get("")); //
	 * set_ItemValue(EnumCoverageByClass.ItemValue_JewelryCoverage.value(),
	 * data.get(""));
	 * set_ItemDescription(EnumCoverageByClass.Description_Jewelry.value(),
	 * data.get("")); radioBtnBankVaulted_CollectionAppraisal(data.get(""));
	 * radioBtnBankVaulted_InsuredJewelryToCurrentValue(data.get("")); }
	 */
	public void fillAllWorldwideJewelryFields(Hashtable<String, String> data) throws Throwable {
		if(!(data.get("WW_TotalScheduledJewelryCoverage").equals("") || data.get("WW_TotalScheduledJewelryCoverage").equals("NULL") || data.get("WW_TotalScheduledJewelryCoverage").equals("0"))){
			setWorldwide_TotalScheduledJewelryCoverage(data.get("WW_TotalScheduledJewelryCoverage"));
			setWorldwide_TotalBlanketJewelryCoverage(data.get("WW_TotalBlanketJewelryCoverage"));
			// setWorldwide_TotalJewelryCoverage(data.get(""));
			setWorldwide_ItemValue(data.get("WW_ItemValue"));
			// selectWorldwide_BlanketCovSingleArtLimit(data.get("WW_BlanketCovSingleArtLimit"));
			setWorldwide_ItemDescription(data.get("WW_ItemDescription"));
			radioBtnWorldwide_CollectionAppraisal(data.get("WW_CollectionAppraisal"));
			radioBtnWorldwide_HomeSafe(data.get("WW_HomeSafe"));
			// if Home safe Yes
			if ("Yes".equalsIgnoreCase(data.get("WW_HomeSafe"))) {
				setWorldwide_VaultMaker(data.get("WW_VaultMaker"));
				setWorldwide_VaultModel(data.get("WW_VaultModel"));
				radioBtnWorldwide_VaultSafeBolted(data.get("WW_VaultSafeBolted"));
				// if vaultSafeBolted yes
				if ("Yes".equalsIgnoreCase(data.get("WW_VaultSafeBolted"))) {
					radioBtnWorldwide_AddToCredit(data.get("WW_AddToCredit"));
					// if Add to credit yes
					if ("Yes".equalsIgnoreCase(data.get("WW_AddToCredit")))
						setWorldwide_MaxAmtOFJewelry(data.get("WW_MaxAmtOFJewelry"));
				}
			}
		}
	}

	public void fillAllBankVaultedJewelryFields(Hashtable<String, String> data) throws Throwable {
		if(!(data.get("BV_TotalScheduledJewelryCoverage").equals("") || data.get("BV_TotalScheduledJewelryCoverage").equals("NULL") || data.get("BV_TotalScheduledJewelryCoverage").equals("0"))){
			setBankVaulted_TotalScheduledJewelryCoverage(data.get("BV_TotalScheduledJewelryCoverage"));
			clickToExpandTable();
			setBankVaulted_ItemValue(data.get("BV_ItemValue"));
			setBankVaulted_ItemDescription(data.get("BV_ItemDescription"));
			radioBtnBankVaulted_CollectionAppraisal(data.get("BV_CollectionAppraisal"));
			radioBtnBankVaulted_InsuredJewelryToCurrentValue(data.get("BV_InsuredJewelryToCurrentValue"));
		}
	}

	public void fillAllFineArtsCollectiblesFields(Hashtable<String, String> data) throws Throwable {
		if(!(data.get("FA_TotalScheduledCoverage").equals("") || data.get("FA_TotalScheduledCoverage").equals("NULL") || data.get("FA_TotalScheduledCoverage").equals("0"))){
			setFineArtsCollectibles_TotalScheduledCoverage(data.get("FA_TotalScheduledCoverage"));
			setFineArtsCollectibles_TotalBlanketCoverage(data.get("FA_TotalBlanketCoverage"));
			setFineArtsCollectibles_ItemValue(data.get("FA_ItemValue"));
			//		selectFineArtsCollectibles_BlanketCovSingleArtLimit(data.get("FA_BlanketCovSingleArtLimit"));
			setFineArtsCollectibles_ItemDescription(data.get("FA_ItemDescription"));
			radioBtnFineArtsCollectibles_CollectionAppraisal(data.get("FA_CollectionAppraisal"));
			radioBtnFineArtsCollectibles_InsuredToCurrentValue(data.get("FA_InsuredToCurrentValue"));
		}
	}

	public void fillAllCoinsSilverStampsFields(Hashtable<String, String> data) throws Throwable {
		if(!(data.get("CS_TotalScheduledCoverage").equals("") || data.get("CS_TotalScheduledCoverage").equals("NULL") || data.get("CS_TotalScheduledCoverage").equals("0"))){
			setCoinsStamps_TotalScheduledCoverage(data.get("CS_TotalScheduledCoverage"));
			setCoinsStamps_TotalBlanketCoverage(data.get("CS_TotalBlanketCoverage"));
			setCoinsStamps_ItemValue(data.get("CS_ItemValue"));
			//		selectCoinsStamps_BlanketCovSingleArtLimit(data.get("CS_BlanketCovSingleArtLimit"));
			setCoinsStamps_ItemDescription(data.get("CS_ItemDescription"));
			radioBtnCoinsStamps_CollectionAppraisal(data.get("CS_CollectionAppraisal"));
			radioBtnCoinsStamps_InsuredToCurrentValue(data.get("CS_InsuredToCurrentValue"));
		}
	}

	public void fillAllWineFields(Hashtable<String, String> data) throws Throwable {
		if(!(data.get("W_TotalScheduledCoverage").equals("") || data.get("W_TotalScheduledCoverage").equals("NULL") || data.get("W_TotalScheduledCoverage").equals("0"))){
			setWine_TotalScheduledCoverage(data.get("W_TotalScheduledCoverage"));
			setWine_TotalBlanketCoverage(data.get("W_TotalBlanketCoverage"));
			setWine_ItemValue(data.get("W_ItemValue"));
			//		selectWine_BlanketCovSingleArtLimit(data.get("W_BlanketCovSingleArtLimit"));
			setWine_ItemDescription(data.get("W_ItemDescription"));
			radioBtnWine_TemperatureAlarm(data.get("W_TemperatureAlarm"));
			radioBtnWine_CollectionAppraisal(data.get("W_CollectionAppraisal"));
			radioBtnWine_InsuredToCurrentValue(data.get("W_InsuredToCurrentValue"));
		}
	}

	public void fillAllFineArtsFields(Hashtable<String, String> data) throws Throwable {
		if(!(data.get("FA_TotalScheduledCoverage").equals("") || data.get("FA_TotalScheduledCoverage").equals("NULL") || data.get("FA_TotalScheduledCoverage").equals("0"))){
			setFineArts_TotalScheduledCoverage(data.get("FA_TotalScheduledCoverage"));
			setFineArts_TotalBlanketCoverage(data.get("FA_TotalBlanketCoverage"));
			setFineArts_ItemValue(data.get("FA_ItemValue"));
			selectFineArts_EarthquakeDeductible(data.get("FA_EarthquakeDeductible"));
			setFineArts_ItemDescription(data.get("FA_ItemDescription"));
			radioBtnFineArts_CollectionAppraisal(data.get("FA_CollectionAppraisal"));
			radioBtnFineArts_InsuredToCurrentValue(data.get("FA_InsuredToCurrentValue"));
		}
	}

	public void fillAllCollectiblesFields(Hashtable<String, String> data) throws Throwable {
		if(!(data.get("C_TotalScheduledCoverage").equals("") || data.get("C_TotalScheduledCoverage").equals("NULL") || data.get("C_TotalScheduledCoverage").equals("0"))){
			setCollectibles_TotalScheduledCoverage(data.get("C_TotalScheduledCoverage"));
			setCollectibles_TotalBlanketCoverage(data.get("C_TotalBlanketCoverage"));
			setCollectibles_ItemValue(data.get("C_ItemValue"));
			selectCollectibles_EarthquakeDeductible(data.get("C_EarthquakeDeductible"));
			setCollectibles_ItemDescription(data.get("C_ItemDescription"));
			radioBtnCollectibles_CollectionAppraisal(data.get("C_CollectionAppraisal"));
			radioBtnCollectibles_InsuredToCurrentValue(data.get("C_InsuredToCurrentValue"));
		}
	}
}
