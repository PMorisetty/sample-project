package com.pure.dragon.collection;

import org.openqa.selenium.By;

public class CoverageScheduledPage {

	static By collectionClass, noOfItems, addCollectionClass, descriptionOfItem, limitOfItem, appraisalDate,
			deleteCheckbox;

	static {
		collectionClass = By.cssSelector("select[name*='_29956214']");
		noOfItems = By.cssSelector("input[name*='_29956314']");
		addCollectionClass = By.cssSelector("a[id*='_15975514']");
		descriptionOfItem = By
				.xpath("//td[contains(@id,'_558806')]//table[@class='objectListTable']//tr[@style][$]//textarea[contains(@id,'_28971906')]");
		limitOfItem = By
				.xpath("//td[contains(@id,'_558806')]//table[@class='objectListTable']//tr[@style][$]//input[contains(@id,'_28972006')]");
		appraisalDate = By
				.xpath("//td[contains(@id,'_558806')]//table[@class='objectListTable']//tr[@style][$]//input[contains(@id,'_28972106')]");
		deleteCheckbox = By
				.xpath("//td[contains(@id,'_558806')]//table[@class='objectListTable']//tr[@style][$]//input[contains(@id,'_29956414')]");
	}

}
