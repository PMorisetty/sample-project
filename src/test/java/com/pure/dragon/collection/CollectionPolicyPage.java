package com.pure.dragon.collection;

import org.openqa.selenium.By;

public class CollectionPolicyPage {

	static By term, quoteName, licensedProducer, currentCollInsuranceCompany, effectiveDate, advisorOrServicer,
			currentPremium, namedInsuredType;
	// trustLLCOrOtherLegalEntity;

	static By namedInsured, prefix, firstName, lastName, suffix, socialSecurityNumber, deleteBtn, addAnotherItem;

	static By burglarAlarmYES, burglarAlarmNO, fireAlarmYES, fireAlarmNO, homeUnoccupiedYES, homeUnoccupiedNO,
			generatorYES, generatorNO, insuredRiskYES, insuredRiskNO, homeConstructionType, protectionClassCode,
			yearBuilt, noOfFloorsAtRiskLocation, homeLocatedOnSlopeYES, homeLocatedOnSlopeNO, homeSupportedYES,
			homeSupportedNO, autoSeismicValveYES, autoSeismicValveNO;

	static {
		term = By.cssSelector("select[title='Term']");
		quoteName = By.cssSelector("input[title='Quote Name']");
		licensedProducer = By.cssSelector("select[title='Licensed Producer']");
		currentCollInsuranceCompany = By.cssSelector("select[title='Current Collections Insurance Company']");
		effectiveDate = By.cssSelector("input[title='Effective Date']");
		advisorOrServicer = By.cssSelector("select[title='Advisor / Servicer']");
		currentPremium = By.cssSelector("input[title='Current Insurance Premium']");
		namedInsuredType = By.cssSelector("select[title='Named Insured Type']");
		// trustLLCOrOtherLegalEntity = By.cssSelector("input[title='Year']");

		/*
		 * namedInsured = By.cssSelector("select[title='Named Insured']");
		 * prefix = By.cssSelector("select[title='Prefix']"); firstName =
		 * By.cssSelector("input[title='First Name']"); lastName =
		 * By.cssSelector("input[title='Last Name']"); suffix =
		 * By.cssSelector("select[title='Suffix']");
		 */

		namedInsured = By.xpath("//table[@class='objectListTable']//tr[@style][$]//select[@title='Named Insured']");
		prefix = By.xpath("//table[@class='objectListTable']//tr[@style][$]//select[@title='Prefix']");
		firstName = By.xpath("//table[@class='objectListTable']//tr[@style][$]//input[@title='First Name']");
		lastName = By.xpath("//table[@class='objectListTable']//tr[@style][$]//input[@title='Last Name']");
		suffix = By.xpath("//table[@class='objectListTable']//tr[@style][$]//select[@title='Suffix']");
		socialSecurityNumber = By
				.xpath("//table[@class='objectListTable']//tr[@style][$]//input[@title='Social Security Number']");
		deleteBtn = By.xpath("//table[@class='objectListTable']//tr[@style][$]//a[@title='Delete the selected item.']");
		addAnotherItem = By.cssSelector("a[title='Add another item.']");

		burglarAlarmYES = By.cssSelector("input[name*='_28963806'][title='Yes']");
		burglarAlarmNO = By.cssSelector("input[name*='_28963806'][title='No']");
		fireAlarmYES = By.cssSelector("input[name*='_28963706'][title='Yes']");
		fireAlarmNO = By.cssSelector("input[name*='_28963706'][title='No']");
		homeUnoccupiedYES = By.cssSelector("input[name*='_28963406'][title='Yes']");
		homeUnoccupiedNO = By.cssSelector("input[name*='_28963406'][title='No']");
		generatorYES = By.cssSelector("input[name*='_28963606'][title='Yes']");
		generatorNO = By.cssSelector("input[name*='_28963606'][title='No']");
		insuredRiskYES = By.cssSelector("input[name*='_28963206'][title='Yes']");
		insuredRiskNO = By.cssSelector("input[name*='_28963206'][title='No']");
		homeConstructionType = By.cssSelector("select[id*='_28963306']");
		protectionClassCode = By.cssSelector("select[id*='_28963006']");
		
		yearBuilt = By.cssSelector("input[title='Year Built']");
		noOfFloorsAtRiskLocation = By.cssSelector("input[title='Number of Floors at the risk location']");
		homeLocatedOnSlopeYES = By.cssSelector("input[name*='_30066314'][title='Yes']");
		homeLocatedOnSlopeNO = By.cssSelector("input[name*='_30066314'][title='No']");
		homeSupportedYES = By.cssSelector("input[name*='_30066414'][title='Yes']");
		homeSupportedNO = By.cssSelector("input[name*='_30066414'][title='No']");
		autoSeismicValveYES = By.cssSelector("input[name*='_30066614'][title='Yes']");
		autoSeismicValveNO = By.cssSelector("input[name*='_30066614'][title='No']");
	}

}
