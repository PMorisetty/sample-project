package com.pure.dragon.collection;

import org.openqa.selenium.By;

public class CoverageByClassPage {

	static By Worldwide_TotalScheduledJewelryCoverage, Worldwide_TotalBlanketJewelryCoverage,
			Worldwide_TotalJewelryCoverage, Worldwide_ItemValue, Worldwide_ItemDescription,
			Worldwide_BlanketCovSingleArtLimit, Worldwide_CollectionAppraisalYES, Worldwide_CollectionAppraisalNO,
			Worldwide_HomeSafeYES, Worldwide_HomeSafeNO, Worldwide_VaultMaker, Worldwide_VaultModel,
			Worldwide_VaultSafeBoltedYES, Worldwide_VaultSafeBoltedNO, Worldwide_AddToCreditYES,
			Worldwide_AddToCreditNO, Worldwide_MaxAmtOFJewelry;

	static By BankVaulted_TotalScheduledJewelryCoverage, clickToExpandTable, BankVaulted_ItemValue, BankVaulted_ItemDescription,
			BankVaulted_CollectionAppraisalYES, BankVaulted_CollectionAppraisalNO,
			BankVaulted_InsuredJewelryToCurrentValueYES, BankVaulted_InsuredJewelryToCurrentValueNO;

	static By FineArtsCollectibles_TotalScheduledCoverage, FineArtsCollectibles_TotalBlanketCoverage, FineArtsCollectibles_TotalCoverage,
			FineArtsCollectibles_CollectionAppraisalYES, FineArtsCollectibles_CollectionAppraisalNO, FineArtsCollectibles_InsuredToCurrentValueYES,
			FineArtsCollectibles_ItemValue, FineArtsCollectibles_ItemDescription, FineArtsCollectibles_BlanketCovSingleArtLimit,
			FineArtsCollectibles_InsuredToCurrentValueNO;
	
	static By FineArts_TotalScheduledCoverage, FineArts_TotalBlanketCoverage, FineArts_TotalCoverage,
			FineArts_CollectionAppraisalYES, FineArts_CollectionAppraisalNO, FineArts_InsuredToCurrentValueYES,
			FineArts_ItemValue, FineArts_ItemDescription, FineArts_EarthquakeDeductible,
			FineArts_InsuredToCurrentValueNO;
	
	static By Collectibles_TotalScheduledCoverage, Collectibles_TotalBlanketCoverage, Collectibles_TotalCoverage,
			Collectibles_CollectionAppraisalYES, Collectibles_CollectionAppraisalNO, Collectibles_InsuredToCurrentValueYES,
			Collectibles_ItemValue, Collectibles_ItemDescription, Collectibles_EarthquakeDeductible,
			Collectibles_InsuredToCurrentValueNO;

	static By CoinsStamps_TotalScheduledJewelryCoverage, CoinsStamps_TotalBlanketJewelryCoverage,
			CoinsStamps_ItemValue, CoinsStamps_ItemDescription, CoinsStamps_BlanketCovSingleArtLimit, CoinsStamps_CollectionAppraisalYES,
			CoinsStamps_CollectionAppraisalNO, CoinsStamps_InsuredToCurrentValueYES,
			CoinsStamps_InsuredToCurrentValueNO;

	static By Wine_TotalScheduledJewelryCoverage, Wine_TotalBlanketJewelryCoverage, Wine_ItemValue,
			Wine_ItemDescription, Wine_BlanketCovSingleArtLimit, Wine_TemperatureAlarmYES, Wine_TemperatureAlarmNO, Wine_CollectionAppraisalYES,
			Wine_CollectionAppraisalNO, Wine_InsuredToCurrentValueYES, Wine_InsuredToCurrentValueNO;

	static {
		Worldwide_TotalScheduledJewelryCoverage = By.cssSelector("input[title='Total Scheduled Jewelry Coverage ($)']");
		Worldwide_TotalBlanketJewelryCoverage = By.cssSelector("input[title='Total Blanket Jewelry Coverage ($)']");
//		Worldwide_TotalJewelryCoverage = By.cssSelector("input[title='" + titleText + "']");
		Worldwide_ItemValue = By.cssSelector("input[id*='28959906']");
		Worldwide_ItemDescription = By.cssSelector("textarea[id*='28960006']");
		Worldwide_BlanketCovSingleArtLimit = By.cssSelector("select[id*='30023114']");
		Worldwide_CollectionAppraisalYES = By.cssSelector("input[name*='29000106'][title='Yes']");
		Worldwide_CollectionAppraisalNO = By.cssSelector("input[name*='29000106'][title='No']");
		Worldwide_HomeSafeYES = By.cssSelector("input[name*='28960106'][title='Yes']");
		Worldwide_HomeSafeNO = By.cssSelector("input[name*='28960106'][title='No']");
		Worldwide_VaultMaker = By.cssSelector("input[id*='28960206']");
		Worldwide_VaultModel = By.cssSelector("input[id*='28960306']");
		Worldwide_VaultSafeBoltedYES = By.cssSelector("input[name*='28960406'][title='Yes']");
		Worldwide_VaultSafeBoltedNO = By.cssSelector("input[name*='28960406'][title='No']");
		Worldwide_AddToCreditYES = By.cssSelector("input[name*='28977006'][title='Yes']");
		Worldwide_AddToCreditNO = By.cssSelector("input[name*='28977006'][title='No']");
		Worldwide_MaxAmtOFJewelry = By.cssSelector("input[id*='28960506']");
		
		BankVaulted_TotalScheduledJewelryCoverage = By.cssSelector("input[id*='28960606']");
		clickToExpandTable = By.cssSelector("div[id*='28960606']");
		BankVaulted_ItemValue = By.cssSelector("input[id*='28960706']");
		BankVaulted_ItemDescription = By.cssSelector("textarea[name*='28960806']");
		BankVaulted_CollectionAppraisalYES = By.cssSelector("input[name*='28960906'][title='Yes']");
		BankVaulted_CollectionAppraisalNO = By.cssSelector("input[name*='28960906'][title='No']");
		BankVaulted_InsuredJewelryToCurrentValueYES = By.cssSelector("input[name*='28961006'][title='Yes']");
		BankVaulted_InsuredJewelryToCurrentValueNO = By.cssSelector("input[name*='28961006'][title='No']");
		
		FineArtsCollectibles_TotalScheduledCoverage = By.cssSelector("input[id*='28961106']");
		FineArtsCollectibles_TotalBlanketCoverage = By.cssSelector("input[id*='28961206']");
//		FineArts_TotalCoverage = By.cssSelector("input[id*='28960606']");
		FineArtsCollectibles_ItemValue = By.cssSelector("input[id*='28961306']");
		FineArtsCollectibles_ItemDescription = By.cssSelector("textarea[name*='28961406']");
		FineArtsCollectibles_BlanketCovSingleArtLimit = By.cssSelector("select[id*='30023314']");
		FineArtsCollectibles_CollectionAppraisalYES = By.cssSelector("input[name*='28961506'][title='Yes']");
		FineArtsCollectibles_CollectionAppraisalNO = By.cssSelector("input[name*='28961506'][title='No']");
		FineArtsCollectibles_InsuredToCurrentValueYES = By.cssSelector("input[name*='28961606'][title='Yes']");
		FineArtsCollectibles_InsuredToCurrentValueNO = By.cssSelector("input[name*='28961606'][title='No']");
		
		FineArts_TotalScheduledCoverage = By.cssSelector("input[id*='30051614']");
		FineArts_TotalBlanketCoverage = By.cssSelector("input[id*='30051314']");
		FineArts_ItemValue = By.cssSelector("input[id*='30050514']");
		FineArts_ItemDescription = By.cssSelector("textarea[name*='30050614']");
		FineArts_EarthquakeDeductible = By.cssSelector("select[id*='30080414']");
		FineArts_CollectionAppraisalYES = By.cssSelector("input[name*='30050114'][title='Yes']");
		FineArts_CollectionAppraisalNO = By.cssSelector("input[name*='30050114'][title='No']");
		FineArts_InsuredToCurrentValueYES = By.cssSelector("input[name*='30051014'][title='Yes']");
		FineArts_InsuredToCurrentValueNO = By.cssSelector("input[name*='30051014'][title='No']");
		
		Collectibles_TotalScheduledCoverage = By.cssSelector("input[id*='30054014']");
		Collectibles_TotalBlanketCoverage = By.cssSelector("input[id*='30053714']");
		Collectibles_ItemValue = By.cssSelector("input[id*='30052914']");
		Collectibles_ItemDescription = By.cssSelector("textarea[name*='30053014']");
		Collectibles_EarthquakeDeductible = By.cssSelector("select[id*='30080114']");
		Collectibles_CollectionAppraisalYES = By.cssSelector("input[name*='30052514'][title='Yes']");
		Collectibles_CollectionAppraisalNO = By.cssSelector("input[name*='30052514'][title='No']");
		Collectibles_InsuredToCurrentValueYES = By.cssSelector("input[name*='30053414'][title='Yes']");
		Collectibles_InsuredToCurrentValueNO = By.cssSelector("input[name*='30053414'][title='No']");
		
		CoinsStamps_TotalScheduledJewelryCoverage = By.cssSelector("input[id*='28961706']");
		CoinsStamps_TotalBlanketJewelryCoverage = By.cssSelector("input[id*='28961806']");
		CoinsStamps_ItemValue = By.cssSelector("input[id*='28961906']");
		CoinsStamps_ItemDescription = By.cssSelector("textarea[name*='28962006']");
		CoinsStamps_BlanketCovSingleArtLimit = By.cssSelector("select[id*='30023214']");
		CoinsStamps_CollectionAppraisalYES = By.cssSelector("input[name*='28962106'][title='Yes']");
		CoinsStamps_CollectionAppraisalNO = By.cssSelector("input[name*='28962106'][title='No']");
		CoinsStamps_InsuredToCurrentValueYES = By.cssSelector("input[name*='28962206'][title='Yes']");
		CoinsStamps_InsuredToCurrentValueNO = By.cssSelector("input[name*='28962206'][title='No']");
		
		Wine_TotalScheduledJewelryCoverage = By.cssSelector("input[id*='28962306']");
		Wine_TotalBlanketJewelryCoverage = By.cssSelector("input[id*='28962406']");
		Wine_ItemValue = By.cssSelector("input[id*='28962506']");
		Wine_ItemDescription = By.cssSelector("textarea[name*='28962606']");
		Wine_BlanketCovSingleArtLimit = By.cssSelector("select[id*='30023414']");
		Wine_TemperatureAlarmYES = By.cssSelector("input[name*='28962706'][title='Yes']");
		Wine_TemperatureAlarmNO = By.cssSelector("input[name*='28962706'][title='No']");
		Wine_CollectionAppraisalYES = By.cssSelector("input[name*='28962806'][title='Yes']");
		Wine_CollectionAppraisalNO = By.cssSelector("input[name*='28962806'][title='No']");
		Wine_InsuredToCurrentValueYES = By.cssSelector("input[name*='28962906'][title='Yes']");
		Wine_InsuredToCurrentValueNO = By.cssSelector("input[name*='28962906'][title='No']");
	}

}
