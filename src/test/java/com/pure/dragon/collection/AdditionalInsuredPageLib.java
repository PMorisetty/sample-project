package com.pure.dragon.collection;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class AdditionalInsuredPageLib extends ActionEngine{
	
	public AdditionalInsuredPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void clickAdd() throws Throwable{
		click(AdditionalInsuredPage.addAnotherItem, "AddAnotherItem Button");
	}
	
	public void selectType(String value, int... count) throws Throwable{
		int num = count != null? count[0]:1;
		String tempLocator = xpathUpdator(AdditionalInsuredPage.type, String.valueOf(num));
		selectByVisibleText(By.xpath(tempLocator), value, "Type");
	}
	
	public void setName(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(AdditionalInsuredPage.name, String.valueOf(num));
		type(By.xpath(tempLocator), value, "Name");
	}

	public void setAddress(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(AdditionalInsuredPage.address, String.valueOf(num));
		type(By.xpath(tempLocator), value, "Address");
	}

	public void setCity(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(AdditionalInsuredPage.city, String.valueOf(num));
		type(By.xpath(tempLocator), value, "City");
	}
	
	public void selectState(String value, int... count) throws Throwable{
		int num = count != null? count[0]:1;
		String tempLocator = xpathUpdator(AdditionalInsuredPage.state, String.valueOf(num));
		selectByVisibleText(By.xpath(tempLocator), value, "State");
	}
	
	public void setZip(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(AdditionalInsuredPage.zip, String.valueOf(num));
		type(By.xpath(tempLocator), value, "Zip");
	}
	
	public void selectApplicableClass(String value, int... count) throws Throwable{
		int num = count != null? count[0]:1;
		String tempLocator = xpathUpdator(AdditionalInsuredPage.applicableClass, String.valueOf(num));
		selectByVisibleText(By.xpath(tempLocator), value, "ApplicableClass");
	}

	public void clickDelete(String value, int... count) throws Throwable {
		int num = count != null ? count[0] : 1;
		String tempLocator = xpathUpdator(AdditionalInsuredPage.deleteBtn, String.valueOf(num));
		click(By.xpath(tempLocator), "DeleteBtn");
	}

	public int getNoOfItemsPresent() {
		By locator = By.xpath("//table[@class='objectListTable']//tr[@style]");
		int size = this.driver.findElements(locator).size();
		return size;

	}

	public void addAnotheItem(Hashtable<String, String> data) throws Throwable {
		int noOfItems = Integer.valueOf(data.get("NoOfAdditionalInsuredItems"));
		for (int iterator = 1; iterator < noOfItems; iterator++) {
//			int noOfItemsPresent = this.getNoOfItemsPresent();
			this.clickAdd();
			this.fillNewItemDetails(iterator, data);
		}
		;
	}

	public void fillNewItemDetails(int count, Hashtable<String, String> data) throws Throwable {
		this.selectType(data.get("Type_" + count), count);
		this.setName(data.get("Name_" + count), count);
		this.setAddress(data.get("Address_" + count), count);
		this.setCity(data.get("City_" + count), count);
		this.selectState(data.get("State_" + count), count);
		this.setZip(data.get("Zip_" + count), count);
		this.selectApplicableClass(data.get("ApplicableClass_" + count), count);
//		this.clickDelete(data.get("Type_" + count), count);
	}
	
}
