package com.pure.dragon.collection;

import org.openqa.selenium.By;

public class AdditionalBindingInfoPage {

	static By occupation, employer, existingAgencyClientYES, existingAgencyClientNO, anyCompanyRefusedToInsureYES,
			anyCompanyRefusedToInsureNO, coverageRenewedYES, coverageRenewedNO;
	
	static {
		occupation = By.cssSelector("input[title='Occupation']");
		employer = By.cssSelector("input[title='Employer']");
		existingAgencyClientYES = By.cssSelector("input[name*='_28964406'][title='Yes']");
		existingAgencyClientNO = By.cssSelector("input[name*='_28964406'][title='No']");
		anyCompanyRefusedToInsureYES = By.cssSelector("input[name*='_28964206'][title='Yes']");
		anyCompanyRefusedToInsureNO = By.cssSelector("input[name*='_28964206'][title='No']");
		coverageRenewedYES = By.cssSelector("input[name*='_28964306'][title='Yes']");
		coverageRenewedNO = By.cssSelector("input[name*='_28964306'][title='No']");
	}
}
