package com.pure.dragon.collection;

import org.openqa.selenium.By;

public class AdditionalInsuredPage {

	static By addAnotherItem, type, name, address, city, state, zip, applicableClass, deleteBtn;
	
	static {
		addAnotherItem = By.cssSelector("a[title='Add another item.']");
		type = By.cssSelector("select[id*='_28975406']");
		name = By.cssSelector("input[id*='_28975506']");
		address = By.cssSelector("input[id*='_28973306']");
		city = By.cssSelector("input[id*='_28973706']");
		state = By.cssSelector("select[id*='_28973606']");
		zip = By.cssSelector("input[id*='_28973806']");
		applicableClass = By.cssSelector("select[id*='_28973606']");
		deleteBtn = By.cssSelector("a[title='Delete the selected item.']");
	}
}
