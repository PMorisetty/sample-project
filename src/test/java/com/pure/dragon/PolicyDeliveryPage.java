package com.pure.dragon;

import org.openqa.selenium.By;

public class PolicyDeliveryPage {
	static By electronicDeliveryYes;
	static By electronicDeliveryNo;
	static By printMailByPure;
	static By agencyEmailDelivery;
	static By agencyEmail;
	static By memberEmail;
	static By sameAddressCheckBox;
	static By memberPhoneNumber;
	static By sendPrintedDocsToMember;
	static By sendPrintedDocsToAgent;
	static By agentInvolvedNo;
	static By agentInvolvedYes;
	
	static By nextButton;
	static By backButton;
	static By saveChangesButton;
	static By exitButton;

	static {
		electronicDeliveryNo = By.xpath("//div[@title='Did the member opt out of electronic delivery (Applicant)?']/..//input[@title='No']");
		electronicDeliveryYes = By.xpath("//div[@title='Did the member opt out of electronic delivery (Applicant)?']/..//input[@title='Yes']");
		printMailByPure = By.cssSelector("input[title='Print and Mail by PURE']");
		agencyEmailDelivery = By.cssSelector("input[title='Agency Email delivery (New Business only).']");
		agencyEmail = By.cssSelector("input[title='Agency Email Address']");
		memberEmail = By.cssSelector("input[title=\"Member's Email\"]");
		sameAddressCheckBox = By.xpath("//div[@title=\"Use Member's Primary Mailing Address\"]/..//input[@type='checkbox']");
		memberPhoneNumber = By.cssSelector("input[title=\"Member's Primary Phone Number\"]");
		sendPrintedDocsToMember = By.cssSelector("[title='Send the printed documents to the Member']");
		sendPrintedDocsToAgent = By.cssSelector("[title='Agency Email (New Business Only)']");
		agentInvolvedNo = By.xpath("//div[@title='Does the Agent want to be involved in new business delivery?']/..//input[@title='No']");
		agentInvolvedYes = By.xpath("//div[@title='Does the Agent want to be involved in new business delivery?']/..//input[@title='Yes']");
		
		nextButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.554407')]");
		backButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.479905')]");
		saveChangesButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.569107')]");
		exitButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.118101')]");
	}
}
