package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PolicyTransactionDocumentListPageLib extends ActionEngine{
	public PolicyTransactionDocumentListPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/*Function to click on Exit Transaction Button*/
	public void clickExitTransactionBtn() throws Throwable{
		WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.visibilityOfElementLocated(PolicyTransactionDocumentListPage.exitTransactionButton));
		wait.until(ExpectedConditions.elementToBeClickable(PolicyTransactionDocumentListPage.exitTransactionButton));
		click(PolicyTransactionDocumentListPage.exitTransactionButton, "Exit Transaction Button");
	}
	public void clickOnPolicyLink() throws Throwable {
		click(PolicyTransactionDocumentListPage.clickPolicyLink, "Policy Link");
	}
	public void clickDocumentLink() throws Throwable{
		click(PolicyTransactionDocumentListPage.clickDocLink, "Document link");
	}
}
