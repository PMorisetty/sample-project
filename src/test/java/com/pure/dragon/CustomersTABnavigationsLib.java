package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CustomersTABnavigationsLib extends ActionEngine{
	public CustomersTABnavigationsLib(EventFiringWebDriver driver, CReporter reporter) {
		this.driver=driver;
		this.reporter = reporter;
				
	}

	public void NavigateTosummary() throws Throwable {
		click(CustomersTABnavigationsPage.summary, "summary tab");
	}
	public void NavigateToCustomerDetails() throws Throwable{
		click(CustomersTABnavigationsPage.customerDetails,"customerDetails tab");
	}
	public void NavigateToPolicyDeliveryPreferences() throws Throwable{
		click(CustomersTABnavigationsPage.policyDeliveryPreferences,"policyDeliveryPreferences tab");
	}
	public void NavigateToBillingDeliveryPreferences() throws Throwable{
		click(CustomersTABnavigationsPage.billingDeliveryPreferences,"billingDeliveryPreferences tab");
	}
	public void NavigateToRequiredforms() throws Throwable{
		click(CustomersTABnavigationsPage.requiredforms,"requiredforms tab");
	}
	public void NavigateToDocuments() throws Throwable{
		click(CustomersTABnavigationsPage.documents,"documents tab");
	}
	public void NavigateToProposals() throws Throwable{
		click(CustomersTABnavigationsPage.proposals,"proposals tab");
	}
	public void NavigateToDiary() throws Throwable{
		click(CustomersTABnavigationsPage.diary,"diary tab");
	}
	public void NavigateToBillingDashboard() throws Throwable{
		click(CustomersTABnavigationsPage.billingDashboard,"billingDashboard tab");
	}
	public void NavigateToHistory() throws Throwable{
		click(CustomersTABnavigationsPage.history,"history tab");
	}
	public void NavigateToBrokerHistory() throws Throwable{
		click(CustomersTABnavigationsPage.brokerHistory,"brokerHistory tab");
	}
	public void NavigateToInsuranceScoreManagement() throws Throwable{
		click(CustomersTABnavigationsPage.insuranceScoreManagement,"insuranceScoreManagement tab");		
	}
	public void NavigateToCLUE_MVRreports() throws Throwable{
		click(CustomersTABnavigationsPage.CLUE_MVRreports,"CLUE_MVRreports tab");
	}
	public void NavigateToMemberFlag() throws Throwable{
		click(CustomersTABnavigationsPage.memberFlag,"memberFlag tab");
	}
	public void NavigateToAccountReferral() throws Throwable{
		click(CustomersTABnavigationsPage.accountReferral,"accountReferral tab");
	}

	

}
