package com.pure.dragon;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.personalAuto.MemberInformationAndPolicyHistoryPage;
import com.pure.report.CReporter;


public class NewQuoteBasicInformationLib extends ActionEngine {
	public NewQuoteBasicInformationLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}

	public void setSpousePartnerOtherToBeNamed(String option) throws Throwable{
		if(option.equalsIgnoreCase("yes")){
			selectByVisibleText(NewQuoteBasicInformationPage.spousePartnerOtherToBeNamed, option, "spousePartnerOtherToBeNamed");
		}else{
			selectByVisibleText(NewQuoteBasicInformationPage.spousePartnerOtherToBeNamed, "No", "spousePartnerOtherToBeNamed");
		}
	}
	// function to Set EffectiveDate
	public void setEffectiveDate(String effectiveDate) throws Throwable {
		if(effectiveDate!=null && !effectiveDate.equalsIgnoreCase("")){
			typeText(NewQuoteBasicInformationPage.effectiveDate, effectiveDate,
					"effectiveDate");
			type(NewQuoteBasicInformationPage.effectiveDate, Keys.TAB,
					"effectiveDate");
			waitUntilJQueryReady();

		}else{
			//Below code picks system date. If you want to test with current effective date, then
			//don't enter any date in excel
			String pattern = "MM-dd-yyyy";
			int incrementBy = 1;
			effectiveDate = new SimpleDateFormat(pattern).format(new Date());
			effectiveDate=getNextDateByIncrement(effectiveDate,incrementBy);
			typeText(NewQuoteBasicInformationPage.effectiveDate, effectiveDate, "effectiveDate");
			type(NewQuoteBasicInformationPage.effectiveDate, Keys.TAB, "effectiveDate");
			waitUntilJQueryReady();
		}
	}
	// function to Set DOB
	public void setDOB(String DOB) throws Throwable {
		type(NewQuoteBasicInformationPage.dob, DOB, "DOB");
		UnderwriterReferralsPageLib.insuredDOB=DOB;
	}
	// function to Set FirstName
	public void setFirstName(String FirstName) throws Throwable {
		type(NewQuoteBasicInformationPage.firstName, FirstName, "FirstName");
	}
	// function to Set MiddleName
	public void setMiddleName(String middleName) throws Throwable {
		if(middleName != null && middleName.length() > 0)
			type(NewQuoteBasicInformationPage.middleName, middleName, "MiddleName");
	}

	// function to Set LastName
	public void setLastName(String LastName) throws Throwable {
		type(NewQuoteBasicInformationPage.lastName, LastName, "LastName");
	}
	// function to select RiskState
	public void selectRiskState(String state) throws Throwable {
		selectByVisibleText(NewQuoteBasicInformationPage.selectRiskState,
				state, "Risk State");
	}
	// function to select Home owner
	public void selectHomeowner() throws Throwable {
		click(NewQuoteBasicInformationPage.homeOwner, "Homeowner");
	}
	// function to select PersonalAuto
	public void selectPersonalAuto() throws Throwable {
		click(NewQuoteBasicInformationPage.personalAuto, "personalAuto");
	}
	// function to select Collections
	public void selectCollections() throws Throwable {
		click(NewQuoteBasicInformationPage.collections, "collections");
	}
	// function to select ExcessLiability
	public void selectExcessLiability() throws Throwable {
		click(NewQuoteBasicInformationPage.excessLiability, "ExcessLiability");
	}
	public void selectWaterCraft() throws Throwable {
		click(NewQuoteBasicInformationPage.waterCraft, "WaterCraft");
	}
	public void setPrimaryRiskAddress1(String address1) throws Throwable {
		if(address1 != null && !address1.equalsIgnoreCase("")){
			type(NewQuoteBasicInformationPage.primaryAdrressLine1, address1,
					"address1");
		}

	}
	public void setPrimaryRiskAddress2(String address2) throws Throwable {
		if(address2 != null && !address2.equalsIgnoreCase("")){
			type(NewQuoteBasicInformationPage.primaryAdrressLine2, address2,
					"address2");
		}

	}

	public void setSSN(String ssn) throws Throwable {
		if(ssn != null && ssn.length() > 0)
			type(NewQuoteBasicInformationPage.ssn, ssn, "SSN");
	}
	public void setCity(String city) throws Throwable {
		type(NewQuoteBasicInformationPage.city, city, "City");
	}
	public void selectState(String state) throws Throwable {
		selectByVisibleText(NewQuoteBasicInformationPage.selectState, state,
				"State");
	}

	public void setZIP(String zip) throws Throwable {
		type(NewQuoteBasicInformationPage.zip, zip, "zip");
	}

	// Has the client lived at this address for more than six months?
	public void sixMothsStayed(Hashtable<String, String> data) throws Throwable {
		if (data.get("sixMonthsStayed").equalsIgnoreCase("Yes")) {
			click(NewQuoteBasicInformationPage.sixMonthsYes,
					"Stayed Six month YES");
		} else {
			click(NewQuoteBasicInformationPage.sixMonthsNo,
					"Stayed Six month NO");
			type(NewQuoteBasicInformationPage.priorAddressLine1, data.get("priorAddressLine1"), "PriorAddressLine1");
			type(NewQuoteBasicInformationPage.priorCity, data.get("insuredCity"), "PriorCity");
			selectByVisibleText(NewQuoteBasicInformationPage.priorState, data.get("state"), "PriorState");
			type(NewQuoteBasicInformationPage.priorZip, data.get("insuredZip"), "PriorZIP");
		}
	}
	// Are you quoting only California, Hawaii or Massachusetts business or
	// Maryland Homeowners? No disclosure is required?
	public void disclosureRequiredOrNot(String disclosureRequired)
			throws Throwable {
		if (disclosureRequired.equalsIgnoreCase("Yes")) {
			click(NewQuoteBasicInformationPage.disclosureRequiredYes,
					"disclosureRequired Yes");
		} else {
			click(NewQuoteBasicInformationPage.disclosureRequiredNo,
					"disclosureRequired NO");
		}

	}

	// Have you disclosed the use of consumer reports to your client?
	public void disclosedConsumerReportToClient(String discloseReportOrNOT)
			throws Throwable {
		/*
		 * if(!driver.findElement(NewQuoteBasicInformationPage.disclosureRequiredYes
		 * ).isSelected()){
		 * click(NewQuoteBasicInformationPage.disclosureRequiredYes,
		 * "disclosureRequired Yes");  }
		 */
		if (discloseReportOrNOT.equalsIgnoreCase("Yes")) {
			click(NewQuoteBasicInformationPage.disclosureMadeToClientYes,
					"disclosureMadeToClientYes");
		} else {
			click(NewQuoteBasicInformationPage.disclosureMadeToClientNo,
					"disclosureMadeToClientNo");
		}
	}

	public void setLicensedProducer(String licensedProducer) throws Throwable {
		selectByVisibleText(NewQuoteBasicInformationPage.licensedProducer,
				licensedProducer, "licensedProducer");
		//Thread.sleep(4000);
	}
	public void setAdvisor(String advisor) throws Throwable {
		selectByVisibleText(NewQuoteBasicInformationPage.advisor,
				advisor, "Advisor/Servicer");
	}

	public void clickNext() throws Throwable {
		click(NewQuoteBasicInformationPage.nextButton, "Next");
		// If the customer name is already exists, Matching customer page opens.
		// For now, create new customer is considered.
		if (driver.findElement(MatchingCustomersPage.createNewCustomer) != null) {
			driver.findElement(MatchingCustomersPage.createNewCustomer).click();
			//Thread.sleep(3000);
		}
	}
	// navigate to Homeowners policy tree link
	public void clickHomeownersPolicyLink() throws Throwable {
		click(NewQuoteBasicInformationPage.homeOwnersPolicyTreeLink, "HomeOwners Policy Link");
	}
	// navigate to quote details tab
	public void navigateToQuoteDetailsTab() throws Throwable {
		click(NewQuoteBasicInformationPage.navigateToQuotedetailsTab, "Quote Details Tab");
	}
	public void clickNxtBtn() throws Throwable {
		System.out.println("Initated clicking next button");
		try{
			driver.findElement(NewQuoteBasicInformationPage.nxtBtn).isDisplayed();
			driver.findElement(NewQuoteBasicInformationPage.nxtBtn).sendKeys(Keys.ENTER);
			waitUntilJQueryReady();
			if(driver.findElement(MatchingCustomersPage.createNewCustomer).isDisplayed()){
				reporter.SuccessReport("IsElementPresent",NewQuoteBasicInformationPage.nxtBtn+" is visible/present.");
			}
		} catch (Exception e) {
			reporter.failureReport("IsElementNotPresent",  NewQuoteBasicInformationPage.nxtBtn+" is NOT visible/present.", driver);
		}
		click(MatchingCustomersPage.createNewCustomer,"createNewCustomer");		
	}

	public void clickOnlyNxtBtn() throws Throwable {
		click(NewQuoteBasicInformationPage.nxtBtn, "Next Button");
		click(NewQuoteBasicInformationPage.existingUserRadio, "Radio button");
		click(MatchingCustomersPage.useExistingCustomer, "Select existing customer button");
	}
	public void selectExistingCustomer() throws Throwable{
		click(NewQuoteBasicInformationPage.existingUserRadio, "Radio button");
		//		click(NewQuoteBasicInformationPage.existingUserRadio, "Radio button");
	}
	public void memberOfHousehold(String memberOfHousehold)
			throws Throwable {
		if (memberOfHousehold.equalsIgnoreCase("Yes")) {
			click(NewQuoteBasicInformationPage.memberOfHouseholdYes,
					"memberOfHousehold Yes");
			//underwriter referral rule eligibility
			UnderwriterReferralsPageLib.memberOHouseHold=true;

		} else {
			click(NewQuoteBasicInformationPage.memberOfHouseholdNo,
					"memberOfHousehold No");
		}
	}

	public void fillDetails(Hashtable<String, String> data) throws Throwable{
		this.setSpousePartnerOtherToBeNamed("No");
		this.setEffectiveDate(data.get("effectiveDate"));
		this.setFirstName(data.get("First_Name_Txt_1"));
		//		this.setMiddleName(data.get("insuredMiddleName"));
		this.setLastName(data.get("Last_Name_Txt_1"));		
		this.setPrimaryRiskAddress1(data.get("insuredAddress1"));
		this.setPrimaryRiskAddress2(data.get("insuredAddress2"));
		this.setDOB(data.get("insuredDOB_1"));
		this.selectRiskState(data.get("insuredRiskState"));
		this.selectState(data.get("state"));

		switch(data.get("state")){
		case "FL":
		case "NC":
		case "SC":
		case "NY"://added for STG
		case "MA":
		case "CA":
		case "NJ":
			if(!getTextOfSelectedOption(NewQuoteBasicInformationPage.admittedSurplusLines,"admittedSurplusLines").equalsIgnoreCase(data.get("admittedSurplusLines")))
				selectByVisibleText(NewQuoteBasicInformationPage.admittedSurplusLines, data.get("admittedSurplusLines"), "Admitted Surplus Lines");
		}

		switch(data.get("lobType")){
		case "HO":
			this.selectHomeowner();	
			this.selectExWindHomeOwner(data.get("ExWind"));
			break;
		case "PA":
			this.selectPersonalAuto();
			break;
		case "EX":
			this.selectExcessLiability();break;
		case "WC":this.selectWaterCraft();break;
		case "CO":
			this.selectCollections();
			break;
		}				
		this.setSSN(data.get("insuredSSN"));
		this.setCity(data.get("insuredCity"));
		this.setZIP(data.get("insuredZip"));
		this.sixMothsStayed(data);
		this.memberOfHousehold(data.get("memberOfHousehold"));
		this.disclosureRequiredOrNot(data.get("disclosureRequired"));
		this.disclosedConsumerReportToClient(data.get("disclosedConsumerReportToClient"));
		String advisor = null;
		switch(region){
		case "QA":
			this.setLicensedProducer(data.get("licensedProducer"));
			this.setAdvisor(data.get("advisor_QA"));break;
		case "STG":
			this.setLicensedProducer(data.get("licensedProducer"));
			this.setAdvisor(data.get("advisor_STG"));break;
		case "PROD":
			this.setLicensedProducer(data.get("licensedProducer_prod"));
			this.setAdvisor(data.get("advisor_PROD"));break;
		}

	}
	public void fillDetails_PA(Hashtable<String, String> data) throws Throwable{
		this.setSpousePartnerOtherToBeNamed("No");
		this.setEffectiveDate(data.get("effectiveDate"));
		this.setFirstName(data.get("First_Name_Txt_1"));
		this.setMiddleName(data.get("insuredMiddleName"));
		this.setLastName(data.get("Last_Name_Txt_1"));		
		this.setPrimaryRiskAddress1(data.get("insuredAddress1"));
		this.setPrimaryRiskAddress2(data.get("insuredAddress2"));
		this.setDOB(data.get("insuredDOB_1"));
		this.selectRiskState(data.get("insuredRiskState"));
		this.selectState(data.get("state"));

		switch(data.get("state")){
		case "FL":
		case "NC":
		case "SC":
		case "NY"://added for STG
		case "MA":
		case "NJ":
			if(!getTextOfSelectedOption(NewQuoteBasicInformationPage.admittedSurplusLines,"admittedSurplusLines").equalsIgnoreCase(data.get("admittedSurplusLines"))){
				selectByVisibleText(NewQuoteBasicInformationPage.admittedSurplusLines, data.get("admittedSurplusLines"), "Admitted Surplus Lines");
			}break;
		case "CA"://added for PROD
			if(region.equalsIgnoreCase("PROD")){
				if(!getTextOfSelectedOption(NewQuoteBasicInformationPage.admittedSurplusLines,"admittedSurplusLines").equalsIgnoreCase(data.get("admittedSurplusLines"))){
					selectByVisibleText(NewQuoteBasicInformationPage.admittedSurplusLines, data.get("admittedSurplusLines"), "Admitted Surplus Lines");
				}
			}
		}
		switch(data.get("lobType")){
		case "HO":
			this.selectHomeowner();	
			this.selectExWindHomeOwner(data.get("ExWind"));
			break;
		case "PA":
			this.selectPersonalAuto();
			break;
		case "EX":
			this.selectExcessLiability();break;
		case "WC":this.selectWaterCraft();break;
		case "CO":
			this.selectCollections();
			break;
		}				
		this.setSSN(data.get("insuredSSN"));
		this.setCity(data.get("insuredCity"));
		this.setZIP(data.get("insuredZip"));
		this.sixMothsStayed(data);
		this.memberOfHousehold(data.get("memberOfHousehold"));
		this.disclosureRequiredOrNot(data.get("disclosureRequired"));
		this.disclosedConsumerReportToClient(data.get("disclosedConsumerReportToClient"));
		String advisor = null;
		switch(region){
		case "QA":
			this.setLicensedProducer(data.get("licensedProducer"));
			advisor = data.get("advisor_QA");break;
		case "STG":
			this.setLicensedProducer(data.get("licensedProducer"));
			advisor = data.get("advisor_STG");break;
		case "PROD":
			this.setLicensedProducer(data.get("licensedProducer_PROD"));
			advisor = data.get("advisor_PROD");break;
		}
		this.setAdvisor(advisor);
	}

	public void selectExWindHomeOwner(String value) throws Throwable {
		if(isVisibleOnly(NewQuoteBasicInformationPage.exWindYes, "Ex-Wind")){
			if(value != null){
				if (value.equalsIgnoreCase("Yes")) {
					click(NewQuoteBasicInformationPage.exWindYes,
							"Ex-Wind - YES");
				} else {
					click(NewQuoteBasicInformationPage.exWindNo,
							"Ex-Wind - NO");
				}
			}
		}
	}	


}
