package com.pure.dragon;

import org.openqa.selenium.By;

public class BillingPage {
	static By billedToMortgageeYes;
	static By billedToMortgageeNo;
	static By sendToBillingAddress;
	static By sendToOtherAddress;
	
	static By confirmButton;
	static By backButton;
	static By saveChangesButton;
	static By exitButton;

	static {
		billedToMortgageeYes = By.xpath("//div[contains(text(), 'Should this be billed to a Mortgagee?')]/..//input[@title='Yes']");
		billedToMortgageeNo = By.xpath("//div[contains(text(), 'Should this be billed to a Mortgagee?')]/..//input[@title='No']");
		sendToBillingAddress = By.xpath("//div[@title=\"Please indicate the member's choice for Bill delivery - options\"]/..//input[@title=\"Send Bill to Member's primary billing Address (For Consolidated billing)\"]");
		sendToOtherAddress = By.xpath("//div[@title=\"Please indicate the member's choice for Bill delivery - options\"]/..//input[@title=\"Send Bill to Other Billing Address (For Individual Policy billing)\"]");
		
		confirmButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.567207')]");
		backButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.551707')]");
		saveChangesButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.569307')]");
		exitButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.118101')]");
	}
}
