package com.pure.dragon;

import org.openqa.selenium.By;

public class InsuranceScoreManagementPage {

	static By newReport,
	
	
	
	//add new report
	member,insuranceScoreModel,insuranceScore,comments,
	saveChanges,scoreCurentlyApplied;
	
	
	static {
		newReport = By.xpath("//a[text()='new report']");
		
		//add new report
		member = By.xpath("//select[@title='Member']");
		insuranceScoreModel = By.xpath("//select[@title='Insurance Score Model']");
		insuranceScore = By.xpath("//input[@title='Insurance Score']");
		comments = By.xpath("//textarea[@title='Comments']");
		saveChanges = By.xpath("//a[text()='save changes']");
		
		//list of scores
		scoreCurentlyApplied = By.xpath("//span[text()='End Date']/ancestor::*[5]//tr//td[5]//div[string-length(text())<4]/../following-sibling::td[1]/div");
	}
}
