package com.pure.dragon;

import org.openqa.selenium.By;

public class PremiumPage {
	static By premiumTab;
	static By premiumTabDiv;
	static By premiumDetailsButton;
	static By deductibleScenariosButton;
	static By exitButton;
	static By createRequiredFormsButton;
	static By bindButton;
	static By quoteProposalButton;
	
	static By quoteCheckBox;
	
	static By referToUWButton;
	static By quoteAdditionalLinesButton;
	static By continueReferToUWButton;
		
	static By coverageLabel;
	static By coverageLimitLabel;
	static By coverageDeductibleLabel;
	static By coveragePremiumLabel;
	
	static By totalPremiumValueLabel,
	grandTotalValueLabel, surplusContributionValueLabel;
	
	static By uwReferralMessageLabel;
	static By uwReferralMessageLabel2;
	static By uwReferralMessageLabel3;
	
	static By customerNameLink;
	
	static By brokerComments, referToUwConfirmationButton;
	
	static By quoteNumberLink;
	
	static By optionalCoveragePremium, locationPremium, totalLocationPremium;
	static By additionalRatePerThousand;
	

	static {
		premiumTab = By.xpath("//a[contains(@href,'Action.103301')]");
		premiumTabDiv = By.xpath("//a[contains(@href,'Action.103301')]/..");
		premiumDetailsButton = By.xpath("//div[@id='footer']//a[contains(text(),'premium detail')]");
		deductibleScenariosButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.574718')]");
		exitButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.88301')]");
		createRequiredFormsButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.734433')]");
		bindButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.481805')]");
		quoteProposalButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.580314')]");
		
		//generic locators for all coverage
		coverageLabel = By.xpath("//table[@class='objectListTable']//label[text()='$']");
		coverageLimitLabel = By.xpath("//table[@class='objectListTable']//label[text()='$']/ancestor::tr[1]/td[3]//label");
		coverageDeductibleLabel = By.xpath("//table[@class='objectListTable']//label[text()='$']/ancestor::tr[1]/td[4]//label");
		coveragePremiumLabel = By.xpath("//table[@class='objectListTable']//label[text()='$']/ancestor::tr[1]/td[5]//label");
		
		totalPremiumValueLabel = By.xpath("//div[@class='headerLabel'][text()='premium summary']/ancestor::td[1]//div[contains(text(), 'Total Premium') or contains(text(), 'Total Annualized Premium')]/../div[2]");
		grandTotalValueLabel = By.xpath("//div[@class='headerLabel'][text()='premium summary']/ancestor::td//div[contains(text(), 'Grand Total')]/../div[2]");
		surplusContributionValueLabel = By.xpath("//div[@class='headerLabel'][text()='premium summary']/ancestor::td//div[contains(text(), 'Surplus Contribution')]/../div[2]");
		
		
		uwReferralMessageLabel = By.cssSelector("div[id^='Label_'][id$='_16460014']");
		uwReferralMessageLabel2 = By.cssSelector("div[id^='Label_'][id$='_16460114']");
		uwReferralMessageLabel3 = By.cssSelector("div[id^='Label_'][id$='_16460214']");
		
		referToUWButton = By.id("660833");
		quoteAdditionalLinesButton = By.id("668733");
		continueReferToUWButton = By.id("667633");
			
		customerNameLink = By.xpath("//div[@id='headerBar']//a[contains(@href,'118101')]");
		quoteCheckBox = By.cssSelector("input[type='Checkbox']");
		
		brokerComments = By.cssSelector("textarea[title*='Comments from Broker' i]");
		referToUwConfirmationButton = By.id("662033");
		
		quoteNumberLink = By.xpath("//a[contains(text(),'Quote: ')]");//By.cssSelector("a[href*='\"537307\"']");
		
		optionalCoveragePremium = By.xpath("//label[text()='Optional Coverages Premium']/../ancestor::*[2]/..//label[string-length(text())>0 and not(contains(text(),'Optional Coverages Premium'))]");
		locationPremium = By.xpath("//label[text()='Location Premium']/../ancestor::*[2]/..//label[string-length(text())>0 and not(contains(text(),'Location Premium'))]");
		totalLocationPremium = By.xpath("//label[text()='Total Location Premium']/../ancestor::*[2]/..//label[string-length(text())>0 and not(contains(text(),'Total Location Premium'))]");
		additionalRatePerThousand = By.xpath("//div[text()='Additional rate per thousand']/../following-sibling::td[1]/div");
	}
}
