package com.pure.dragon;

import org.openqa.selenium.By;

import com.pure.accelerators.ActionEngine;

public class NewQuoteInformationPage extends ActionEngine{
	static By insuranceLine, riskState, quoteName, licensedProducer, advisor;
	static By createQuoteButton, exitButton;
	static {
		insuranceLine = By.cssSelector("select[id$='28932005']");
		riskState = By.cssSelector("select[id$='8733007']");
		quoteName = By.cssSelector("input[title='Quote Name']");
		licensedProducer = By.cssSelector("select[title='Licensed Producer']");
		advisor = By.cssSelector("select[title='Advisor / Servicer']");
		createQuoteButton = By.id("538707");
		exitButton = By.id("88301");
	}
}
