package com.pure.dragon;

import org.openqa.selenium.By;

import com.pure.accelerators.ActionEngine;

public abstract class SearchFromList extends ActionEngine{
	
	By searchBy	= By.xpath("//div[@class='searchPanel']//*[contains(@id,'List')]");
	By contains	= By.xpath("//*[label = 'contains']//input");
	By btnGo 	= By.xpath("//div[@class]//a[contains(text(),'go')]");
	
	/*Function to search agency from list depending on parameters passed*/
	public void search(enumSearchBy enumSearchBy, String containsText) throws Throwable {		
		selectByVisibleText(searchBy, enumSearchBy.value(),"Search By");		
		type(contains, containsText,"contains text");
		click(btnGo, "btnGO");
		waitUntilJQueryReady();
		
	}
	/*Function to search from list depending on parameters passed*/
	public void search(String containsText) throws Throwable {		
		type(contains, containsText,"Search By text");
		click(btnGo, "Go");
	}
}
