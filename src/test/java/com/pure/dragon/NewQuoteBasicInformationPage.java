package com.pure.dragon;

import org.openqa.selenium.By;

public class NewQuoteBasicInformationPage {

	static By effectiveDate;
	static By firstName, middleName, lastName, dob, city, ssn;
	static By primaryAdrressLine1, primaryAdrressLine2;
	static By selectRiskState, selectState, zip;
	static By sixMonthsYes, sixMonthsNo;
	static By disclosureRequiredNo, disclosureRequiredYes;
	static By disclosureMadeToClientNo, disclosureMadeToClientYes;
	static By licensedProducer, advisor, nextButton;
	public static By nxtBtn;
	static By homeOwner, personalAuto,collections,excessLiability,waterCraft;
	static By homeOwnersPolicyTreeLink;
	static By navigateToQuotedetailsTab;
	static By spousePartnerOtherToBeNamed;
	static By admittedSurplusLines;
	static By exWindYes, exWindNo;
	static By memberOfHouseholdNo, memberOfHouseholdYes;
	static By priorAddressLine1, priorCity, priorState, priorZip;
	
	static By existingUserRadio;
	
	static{
		effectiveDate = By.xpath("//input[contains(@title ,'Effective Date')]");
		firstName = By.xpath("//input[contains(@title ,'First Name')]");
		middleName = By.cssSelector("input[title='MI']");
		lastName = By.xpath("//input[contains(@title ,'Last Name')]");
		dob = By.xpath("//input[contains(@title ,'DOB')]");
		primaryAdrressLine1 = By.xpath("//input[contains(@title ,'Primary Address Line 1')]");
		primaryAdrressLine2 = By.xpath("//input[contains(@title ,'Primary Address Line 2')]");
		city = By.xpath("//input[contains(@title ,'City')]");
		ssn = By.xpath("//input[contains(@title ,'SSN')]");
		selectRiskState = By.xpath("//Select[contains(@title ,'Select Risk State')]");
		selectState = By.xpath("//Select[contains(@title ,'State')]");
		zip = By.xpath("//input[contains(@title ,'ZIP')]");		
		sixMonthsYes = By.xpath("//div[contains(@title,'six months')]//..//input[@type='radio' and @title='Yes']");
		sixMonthsNo = By.xpath("//div[contains(@title,'six months')]//..//input[@type='radio' and @title='No']");
		disclosureRequiredNo = By.xpath("//div[contains(text(),'No disclosure')]//..//input[@type='radio' and @title='No']");
		disclosureRequiredYes = By.xpath("//div[contains(text(),'No disclosure')]//..//input[@type='radio' and @title='Yes']");
		disclosureMadeToClientNo = By.xpath("//input[@title='No disclosure has been made.']");
		disclosureMadeToClientYes = By.xpath("//input[@title='Yes, a disclosure has been made.']");
		licensedProducer = By.xpath("//Select[contains(@title ,'Licensed Producer')]");
		advisor = By.xpath("//Select[contains(@title ,'Advisor / Servicer')]");
		homeOwner = By.xpath("//span[contains(text(),'Homeowner')]//..//input");
		personalAuto = By.xpath("//span[contains(text(),'Personal')]//..//input");
		collections = By.xpath("//span[contains(text(),'Collections')]//..//input");
		excessLiability = By.xpath("//span[contains(text(),'Excess')]//..//input");
		waterCraft = By.xpath("//span[contains(text(),'Water')]//..//input");
		nextButton = By.xpath("//a[@title='Click to go to household page']");
		nxtBtn = By.xpath("//a[contains(text(),'>>> next')]");
		homeOwnersPolicyTreeLink = By.xpath("//a[@title='Homeowners Policy']");
		navigateToQuotedetailsTab = By.xpath("//span[contains(text(),'quote details')]");
		spousePartnerOtherToBeNamed = By.xpath("//select[@title='Spouse/Partner/Other to be named on the policy']");
		admittedSurplusLines = By.xpath("//select[@title='Select Admitted or Surplus Lines']");
		exWindYes = By.xpath("//div[@title='Ex-wind Homeowner']/..//input[@title='Yes']");
		exWindNo = By.xpath("//div[@title='Ex-wind Homeowner']/..//input[@title='No']");
		
		memberOfHouseholdNo = By.xpath("//div[contains(@title, 'member of your household')]/following-sibling::div//input[@title='No']");
		memberOfHouseholdYes = By.xpath("//div[contains(@title, 'member of your household')]/following-sibling::div//input[@title='Yes']");
		
		priorAddressLine1 = By.xpath("//input[@title='Prior Address Line 1']");
		priorCity = By.xpath("//div[@title='Prior Address']/../../../following-sibling::tr//input[@title='City']");
		priorState = By.xpath("//div[@title='Prior Address']/../../../following-sibling::tr//select[@title='State']");
		priorZip = By.xpath("//div[@title='Prior Address']/../../../following-sibling::tr//input[contains(@title, 'ZIP')]");
		
		existingUserRadio = By.xpath("//div[contains(text(), 'DOB + Zip/LName')]/../../td//input[@type = 'radio']");
	}
}
