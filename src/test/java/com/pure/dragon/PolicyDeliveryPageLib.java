package com.pure.dragon;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PolicyDeliveryPageLib extends ActionEngine{
	public PolicyDeliveryPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/*Function to select Electronic Delivery depending on parameter*/
	public void selectElectronicDelivery(String value) throws Throwable {
		if(value.equalsIgnoreCase("YES")){
			try {
				if(driver.findElement(PolicyDeliveryPage.electronicDeliveryYes).isEnabled())
				{
				click(PolicyDeliveryPage.electronicDeliveryYes, "Electronic Delivery - Yes");
				}
			} catch (Exception e) {
				//Do Nothing
			}
		}else{
			click(PolicyDeliveryPage.electronicDeliveryNo, "Electronic Delivery - No");
		}
	}
	/*Function to select Document Delivery depending on parameter*/
	public void selectDocumentDelivery(String value) throws Throwable {
		if(value.equalsIgnoreCase("AGENCY EMAIL")){
			click(PolicyDeliveryPage.agencyEmailDelivery, "Document Delivery - Yes");
		}else{
			if(isVisibleOnly(PolicyDeliveryPage.printMailByPure, "Print Mail By Pure")){
				click(PolicyDeliveryPage.printMailByPure, "Document Delivery - No");
			}
		}
	}
	/*Function to determine whether to involve agent in delivery or not depending on parameter passed*/
	public void selectAgentInvolvement(String value) throws Throwable {
		if(value.equalsIgnoreCase("YES") && isVisibleOnly(PolicyDeliveryPage.agentInvolvedYes,"agentInvolvedYes")){
			click(PolicyDeliveryPage.agentInvolvedYes, "Agent Involvement - Yes");
		}else if(isVisibleOnly(PolicyDeliveryPage.agentInvolvedNo,"agentInvolvedNo")){
			click(PolicyDeliveryPage.agentInvolvedNo, "Agent Involvement - No");
		}
	}
	/*Function to determine whether to send policy documents to agent or member in delivery depending on parameter passed*/
	public void selectPrintedDocs(String value) throws Throwable {
		if(value.equalsIgnoreCase("Agent") && isVisibleOnly(PolicyDeliveryPage.sendPrintedDocsToAgent,"sendPrintedDocsToAgent")){
			click(PolicyDeliveryPage.sendPrintedDocsToAgent, "Send Printed Docs to Agent");
		}else if(isVisibleOnly(PolicyDeliveryPage.sendPrintedDocsToMember,"sendPrintedDocsToMember")){
			click(PolicyDeliveryPage.sendPrintedDocsToMember, "Send Printed Docs to Member");
		}
	}
	/*Function to fill agency email*/
	public void setAgencyEmail(String agencyEmail) throws Throwable {
		type(PolicyDeliveryPage.agencyEmail, agencyEmail, "Agency Email type");
	}
	/*Function to fill member email*/
	public void setMemberEmail(String memberEmail) throws Throwable {
		type(PolicyDeliveryPage.memberEmail, memberEmail, "Member Email type");
	}
	/*Function to fill member phone*/
	public void setMemberPhone(String memberPhoneNumber) throws Throwable {
		scrollToWebElement(PolicyDeliveryPage.memberPhoneNumber);
		click(PolicyDeliveryPage.memberPhoneNumber,"memberPhoneNumber");
		type(PolicyDeliveryPage.memberPhoneNumber, memberPhoneNumber, "Member Phone type");
	}
	/*Function to click same adress checkbox*/
	public void clickSameAddress() throws Throwable {
		click(PolicyDeliveryPage.sameAddressCheckBox, "Same Address CheckBox");
	}
	/*Customised Function to electronic delivery without agent involvement*/
	public void setElectronicDelivery(String memberEmail, String phoneNumber) throws Throwable{
		selectElectronicDelivery("No");
		selectAgentInvolvement("No");
		setMemberEmail(memberEmail);
		setMemberPhone(phoneNumber);
	}
	/*Customised Function to electronic delivery with agent involvement*/
	public void setElectronicAgencyDelivery(String sentPrintedDocsTo,String agentEmail, String memberEmail, String phoneNumber) throws Throwable{
		selectElectronicDelivery("No");
		selectAgentInvolvement("Yes");
		selectPrintedDocs(sentPrintedDocsTo);
		setAgencyEmail(agentEmail);
		setMemberEmail(memberEmail);
		setMemberPhone(phoneNumber);
	}
	/*Customised Function to non electronic delivery with document delivery via Pure*/
	public void setNonElectronicDelivery(String sentPrintedDocsTo, String memberEmail, String phoneNumber) throws Throwable{
		selectElectronicDelivery("Yes");
		selectDocumentDelivery("Pure");
		selectPrintedDocs(sentPrintedDocsTo);
		setMemberEmail(memberEmail);
		setMemberPhone(phoneNumber);
	}
	/*Customised Function to non electronic delivery with document delivery via Agency mail*/
	public void setNonElectronicAgencyDelivery(String agentEmail, String memberEmail, String phoneNumber) throws Throwable{
		selectElectronicDelivery("Yes");
		selectDocumentDelivery("AGENCY EMAIL");
		setAgencyEmail(agentEmail);
		setMemberEmail(memberEmail);
		setMemberPhone(phoneNumber);
	}
	/*Function to click on back button*/
	public void clickBackButton() throws Throwable {
		click(PolicyDeliveryPage.backButton, "Back Button");
	}
	/*Function to click on save changes button*/
	public void clickSaveChangesButton() throws Throwable {
		click(PolicyDeliveryPage.saveChangesButton, "Save Changes Button");
	}
	/*Function to click on next button*/
	public void clickNextButton() throws Throwable {
		click(PolicyDeliveryPage.nextButton, "Next Button");
	}
	/*Function to click on exit button*/
	public void clickExitButton() throws Throwable {
		click(PolicyDeliveryPage.exitButton, "Exit Button");
	}
	
	/*Function to select document delivery type*/
	public void selectDocumentDeliveryType(Hashtable<String, String> data) throws Throwable{
		switch(data.get("documentDeliveryType").toUpperCase()){
			case "ELECTRONICDELIVERY": setElectronicDelivery(data.get("memberEmail"), data.get("phoneNumber")); break;
			case "ELECTRONICAGENCYDELIVERY": setElectronicAgencyDelivery(data.get("sendDocsTo"),data.get("agentEmail"),data.get("memberEmail"), data.get("phoneNumber"));break;
			case "NONELECTRONICDELIVERY": setNonElectronicDelivery(data.get("sendDocsTo"),data.get("memberEmail"), data.get("phoneNumber"));break;
			case "NONELECTRONICAGENCYDELIVERY": setNonElectronicAgencyDelivery(data.get("agentEmail"),data.get("memberEmail"), data.get("phoneNumber"));break;
		}
	}
	
}
