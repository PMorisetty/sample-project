package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class BillingPageLib extends ActionEngine{
	public BillingPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	// function to click on BilledToMortgagee radio button Yes/No
	public void selectBilledToMortgagee(String value) throws Throwable {
		if(value.equalsIgnoreCase("YES")){
			click(BillingPage.billedToMortgageeYes, "Billed To Mortgagee - Yes");
		}else{
			click(BillingPage.billedToMortgageeNo, "Billed To Mortgagee - No");
		}
	}
	// function to select billing address
	public void selectBillingAddress(String value) throws Throwable {
		if(value.equalsIgnoreCase("SendToBilling")){
			click(BillingPage.sendToBillingAddress, "Send to Billing Address");
		}else{
			click(BillingPage.sendToOtherAddress, "Send to Other Address");
		}
	}
	// function to click confirm button
	public void clickConfirmBtn() throws Throwable{
		click(BillingPage.confirmButton, "Confirm Button");
	}
	// function to click back button
	public void clickBackBtn() throws Throwable{
		click(BillingPage.backButton, "Back Button");
	}
	// function to click SaveChanges button
	public void clickSaveChangesBtn() throws Throwable{
		click(BillingPage.saveChangesButton, "Save Changes Button");
	}
	//function to click Exit button
	public void clickExitBtn() throws Throwable{
		click(BillingPage.exitButton, "Exit Button");
	}
	
	
}
