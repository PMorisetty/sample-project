package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class DiaryLib extends ActionEngine{
	public DiaryLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/* Function to click onCancel entry */
	public void clickCancelEntry() throws Throwable {
		waitForVisibilityOfElement(DiaryPage.cancellationEntity, "waiting for Cancel entry");
		click(DiaryPage.cancellationEntity, "Cancel Entry clicked");
	}
	/* Function to click on Reinstatement entry */
	public void clickReinstatementEntry() throws Throwable {
		waitForVisibilityOfElement(DiaryPage.reinstatementEntity, "waiting for Reinstatement entry");
		JSClick(DiaryPage.reinstatementEntity, "Reinstatement Entry");
	}
	/* Function to navigate diary tab */
	public void navigateToDairyTab() throws Throwable {
		click(DiaryPage.diaryTab, "Diary Tab");
	}
	/* Function to get text from diary status */
	public String getDiaryStatus() throws Throwable {
		return getText(DiaryPage.diaryStatus, "Diary Status");
	}
	/* Function to verify the canellation message on Diary tab */
	public void verifyDiaryCancellationStatus() throws Throwable{
		String msg = getDiaryStatus();
		//msg.contains("was cancelled along with Fraud and Cyber Defense Coverage");
		assertTrue(msg.contains("was cancelled along with Fraud and Cyber Defense Coverage"), "Canellation Diary Message");
	}
	/* Function to verify the Reinstatement message on Diary tab */
	public void verifyDiaryReinStatementStatus() throws Throwable{
		String msg = getDiaryStatus();
		//msg.contains("along with Fraud and Cyber Defense Coverage and the policy is now reinstated");
		assertTrue(msg.contains("along with Fraud and Cyber Defense Coverage and the policy is now reinstated"), "ReinStatement Diary Message");
	}
	public void clickExitTransaction() throws Throwable {
		click(DiaryPage.exitTransactionBtn, "ExitTransaction Button");
	}
	
	public void clickExitPolicyBtn() throws Throwable {
		click(DiaryPage.exitPolicyBtn, "ExitPolicy Button");
	}
}
