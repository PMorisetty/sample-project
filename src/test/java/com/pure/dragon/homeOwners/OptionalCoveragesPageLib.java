package com.pure.dragon.homeOwners;

import java.util.Arrays;
import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class OptionalCoveragesPageLib extends ActionEngine{
	public OptionalCoveragesPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	private void selectEarthquakeExtension(String value, String deductible) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				if(isVisibleOnly(OptionalCoveragesPage.earthquakeExtensionYes, "Earthquake Extension - Yes")){
					click(OptionalCoveragesPage.earthquakeExtensionYes, "Earthquake Extension - Yes");
					selectByVisibleText(OptionalCoveragesPage.earthquakeExtensionDeductible, deductible, "deductible");
				}
			}else if(value.equalsIgnoreCase("NO")){
				if(isVisibleOnly(OptionalCoveragesPage.earthquakeExtensionNo, "Earthquake Extension - No"))
					click(OptionalCoveragesPage.earthquakeExtensionNo, "Earthquake Extension - No");
			}
		}
	}
	private void selectEarthquakeLossAssessment(String value, String limit) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				if(isVisibleOnly(OptionalCoveragesPage.earthquakeLossAssessmentYes, "Earthquake Loss Assessment - Yes")){
					click(OptionalCoveragesPage.earthquakeLossAssessmentYes, "Earthquake Loss Assessment - Yes");
					type(OptionalCoveragesPage.earthquakeLossAssessmentLimit, limit, "deductible");
				}
			}else if(value.equalsIgnoreCase("NO")){
				if(isVisibleOnly(OptionalCoveragesPage.earthquakeLossAssessmentNo, "Earthquake Loss Assessment - No")){
					click(OptionalCoveragesPage.earthquakeLossAssessmentNo, "Earthquake Loss Assessment - No");
				}	
			}
		}
	}
	private void selectHomeSystemProtection(Hashtable<String, String>data) throws Throwable{	
		String homesystemProtection = data.get("homesystemProtection");
		String limit = data.get("homesystemProtectionLimit");
		//String deductible = data.get("homesystemProtectionDeductible");
		try{
			if(homesystemProtection.equalsIgnoreCase("Yes") && isElementPresent(OptionalCoveragesPage.homesystemProtectionYes, "homesystemProtectionYes")){
				click(OptionalCoveragesPage.homesystemProtectionYes,"homesystem Protection-Yes");
				if((!limit.equalsIgnoreCase("NULL") && limit != null)){
					selectByVisibleText(OptionalCoveragesPage.homesystemProtectionLimit,limit,"homesystem Protection Limit");
					//type(OptionalCoveragesPage.homesystemProtectionDeductible,deductible,"homesystemProtection Deductible");//Disabled in UI
				}
			}else if(homesystemProtection.equalsIgnoreCase("No") && isElementPresent(OptionalCoveragesPage.homesystemProtectionNo, "homesystemProtectionNo")){					
			}else if(homesystemProtection.equalsIgnoreCase("No") && !isElementPresent(OptionalCoveragesPage.homesystemProtectionNo, "homesystemProtectionNo")){
				reporter.warningReport("homesystemProtection No", " Unable to locate homesystemProtectionNo: Check for "+data.get("insuredRiskState") + " its available.");
			}
		}catch(Exception e){				
		}
	}
	private void selectLiabilityExtension(String liabilityExtension, String liabilityExtensionNoOfPremises) throws Throwable{
		if(liabilityExtension != null && liabilityExtension != ""){
			if(liabilityExtension.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.liabilityExtentionYes,"liabilityExtention Yes");
				if(liabilityExtensionNoOfPremises!=null && !liabilityExtensionNoOfPremises.equalsIgnoreCase("NULL")){
					type(OptionalCoveragesPage.noOfPremises,liabilityExtensionNoOfPremises,"noOfPremises");
				}
			}
		}
	}
	private void selectLossAssessmentIncrease(String lossAssessmentIncrease, String lossAssessmentIncreaseLimit) throws Throwable{
		if(lossAssessmentIncrease != null && lossAssessmentIncrease != ""){
			if(lossAssessmentIncrease.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.lossAssessmentIncreaseYes,"lossAssessmentIncrease--Yes");
				if(lossAssessmentIncreaseLimit!=null && !lossAssessmentIncreaseLimit.equalsIgnoreCase("NULL")){
					type(OptionalCoveragesPage.lossAssessmenIncreaseLimit,lossAssessmentIncreaseLimit,"lossAssessmentIncreaseLimit");
				}
			}
		}
	}

	private void selectBusinessPropertyExtension(String value, String limit) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				if(isVisibleOnly(OptionalCoveragesPage.businessPropertyExtensionYes, "businessPropertyExtension - Yes")){
					click(OptionalCoveragesPage.businessPropertyExtensionYes, "businessPropertyExtension - Yes");
					type(OptionalCoveragesPage.businessPropertyExtensionLimit, limit, "Limit");
				}
			}else if(value.equalsIgnoreCase("NO")){
				if(isVisibleOnly(OptionalCoveragesPage.businessPropertyExtensionNo, "businessPropertyExtension - No")){
					click(OptionalCoveragesPage.businessPropertyExtensionNo, "businessPropertyExtension - No");
				}	
			}
		}
	}

	private void selectEnsuingFungiIncrease(String value, String limit) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				if(isVisibleOnly(OptionalCoveragesPage.ensuingFungiIncreaseYes, "ensuingFungiIncrease - Yes")){
					click(OptionalCoveragesPage.ensuingFungiIncreaseYes, "ensuingFungiIncrease - Yes");
					selectByVisibleText(OptionalCoveragesPage.ensuingFungiIncreaseLimit, limit, "Limit");
				}
			}else if(value.equalsIgnoreCase("NO")){
				if(isVisibleOnly(OptionalCoveragesPage.ensuingFungiIncreaseNo, "ensuingFungiIncrease - No")){
					click(OptionalCoveragesPage.ensuingFungiIncreaseNo, "ensuingFungiIncrease - No");
				}	
			}
		}
	}
	private void selectIncidentalBusiness(String value, String limit) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				if(isVisibleOnly(OptionalCoveragesPage.incidentalBusinessYes, "incidentalBusiness - Yes")){
					click(OptionalCoveragesPage.incidentalBusinessYes, "incidentalBusiness - Yes");
					type(OptionalCoveragesPage.incidentalBusinessLimit, limit, "Limit");
				}
			}else if(value.equalsIgnoreCase("NO")){
				if(isVisibleOnly(OptionalCoveragesPage.incidentalBusinessNo, "incidentalBusiness - No")){
					click(OptionalCoveragesPage.incidentalBusinessNo, "incidentalBusiness - No");
				}	
			}
		}
	}

	private void selectLandscapingIncreasedLimits(String value) throws Throwable{
		if(value!= null && value != ""){
			if(value.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.landscapingIncreasedLimitsYes,"landscapingIncreasedLimits--Yes");
			}else{
				click(OptionalCoveragesPage.landscapingIncreasedLimitsNo,"landscapingIncreasedLimits--No");
			}
		}
	}
	private void selectBusinessThreshold(String value) throws Throwable{
		if(value!= null && value != ""){
			if(value.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.businessThresholdYes,"businessThreshold--Yes");
			}else{
				click(OptionalCoveragesPage.businessThresholdNo,"businessThreshold--No");
			}
		}
	}
	private void selectGuaranteedReplacement(String value) throws Throwable{
		if(value!= null && value != ""){
			if(value.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.guaranteedReplacementYes,"guaranteedReplacement--Yes");
			}else{
				click(OptionalCoveragesPage.guaranteedReplacementNo,"guaranteedReplacement--No");
			}
		}
	}
	private void selectSinkholeCollapseExtension(String value) throws Throwable{
		if(value!= null && value!= ""){
			if(value.equalsIgnoreCase("Yes"))
				click(OptionalCoveragesPage.sinkholeCollapseExtensionYes,"sinkholeCollapseExtension--Yes");
			else
				click(OptionalCoveragesPage.sinkholeCollapseExtensionNo,"sinkholeCollapseExtension--No");
		}
	}
	private void selectFloodCoverageExtension(String value) throws Throwable{
		if(value!= null && value != ""){
			if(value.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.floodCoverageExtensionYes,"floodCoverageExtension--Yes");
			}else{
				click(OptionalCoveragesPage.floodCoverageExtensionNo,"floodCoverageExtension--No");
			}
		}
	}

	private void selectLawAndOrdinanceIncrease(String value, String option) throws Throwable{
		if(value!= null && value != ""){
			if(value.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.lawAndOrdinanceIncreaseYes,"LawAndOrdinanceIncrease--Yes");
				selectByVisibleText(OptionalCoveragesPage.lawAndOrdinanceIncreaseOption, option, "LawAndOrdinanceIncreaseOption");
			}else{
				click(OptionalCoveragesPage.lawAndOrdinanceIncreaseNo,"LawAndOrdinanceIncrease--No");
			}
		}
	}

	public void selectOptionalCoverage(Hashtable<String, String> data) throws Throwable{
		//String[] invalidStates = {"MN", "MO"};
		String[] invalidStates = {"MN"};
		if(Arrays.asList(invalidStates).contains(data.get("state"))){
			new LocationCoveragePageLib(this.driver, this.reporter).selectExcessFlood(data);
		}
		switch (data.get("state")) {
		case "NC":
			//need to add lines for NC state
			break;
		case "AR":
			selectLiabilityExtension(data.get("liabilityExtension"), data.get("liabilityExtensionNumberOfPremises"));
			selectHomeSystemProtection(data);
		case "WI":
		case "IA":
		case "WV":
		case "SD":
		case "HI":
			selectEarthquakeExtension(data.get("earthquakeExtension"), data.get("earthquakeExtensionDeductible"));
			break;
		case "FL":
			this.selectSinkholeCollapseExtension(data.get("sinkholeCollapseExtension"));
			this.selectLawAndOrdinanceIncrease(data.get("lawAndOrdinanceIncrease"), data.get("lawAndOrdinanceIncreaseOption"));
		case "CA":
		case "TX":
		case "DE":
		case "WY":
		case "OR":
		case "NY":
			selectGuaranteedReplacement(data.get("guaranteedReplacementCost"));
		case "PA":
		case "RI":	
			selectBusinessPropertyExtension(data.get("businessPropertyExtension"), data.get("businessPropertyExtensionLimit"));
			selectEnsuingFungiIncrease(data.get("ensuingFungiIncrease"), data.get("ensuingFungiIncreaseLimit"));
			selectIncidentalBusiness(data.get("incidentalBusiness"), data.get("incidentalBusinessLimit"));
			selectLandscapingIncreasedLimits(data.get("landscapingIncreasedLimits"));
			selectBusinessThreshold(data.get("incidentalBusinessThreshold"));
		case "VA":
			selectHomeSystemProtection(data);
			selectLiabilityExtension(data.get("liabilityExtension"), data.get("liabilityExtensionNumberOfPremises"));
			selectLossAssessmentIncrease(data.get("lossAssessmentIncrease"), data.get("lossAssessmentIncreaseLimit"));
			break;
		default:
			selectEarthquakeExtension(data.get("earthquakeExtension"), data.get("earthquakeExtensionDeductible"));
			selectEarthquakeLossAssessment(data.get("earthquakeLossAssessment"), data.get("earthquakeLossAssessmentLimit"));
		}
	}

	//optional flood coverage
	private void selectFloodExtensionDIC(String floodExtensionDIC) throws Throwable{
		if((floodExtensionDIC!= null) && (floodExtensionDIC != ""))
		{
			if(floodExtensionDIC.equalsIgnoreCase("Yes") && isVisibleOnly(OptionalCoveragesPage.floodExtensionDICYes,"floodExtensionDICYes")){
				click(OptionalCoveragesPage.floodExtensionDICYes,"floodExtensionDIC--Yes");
			}else if(floodExtensionDIC.equalsIgnoreCase("No") && isVisibleOnly(OptionalCoveragesPage.floodExtensionDICNo,"floodExtensionDICYes")){				
				if(getAttributeValue(OptionalCoveragesPage.floodExtensionDICNo,"checked") != null){
					click(OptionalCoveragesPage.floodExtensionDICNo,"floodExtensionDIC--No");
				}
			}
		}
	}
	public void selectfloodAdvantage(String floodAdvantage) throws Throwable{
		if((floodAdvantage!= null) && (floodAdvantage != "")){
			//if(isElementPresent(OptionalCoveragesPage.floodAdvantageYes,"floodAdvantage")){
			//Uncomment isElementPresent justifying which case it should be used. use switch or if for that criteria.
				if(floodAdvantage.equalsIgnoreCase("Yes")){
					click(OptionalCoveragesPage.floodAdvantageYes,"floodAdvantage--Yes");
				}else if(floodAdvantage.equalsIgnoreCase("No")){
					click(OptionalCoveragesPage.floodAdvantageNo,"floodAdvantage--No");
				}
			//}
		}
	}
	private void selectExcessFloodCoverage(String excessFloodCoverage) throws Throwable{
		if((excessFloodCoverage!= null) && (excessFloodCoverage != "")){
			if(excessFloodCoverage.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.excessFloodCoverageYes,"excessFloodCoverage--Yes");
			}else{
				click(OptionalCoveragesPage.excessFloodCoverageNo,"excessFloodCoverage--No");
			}
		}
	}
	private void selectDwelling(String dwelling) throws Throwable{
		if((dwelling!= null) && (dwelling != "")){
			//If value doesn't have %, 'Full Limits', then that value can entered by choosing
			// "Other Limits" and entering value in the input box next to it.
			if(!dwelling.contains("%") && !dwelling.contains("Full Limits")){
				selectByVisibleText(OptionalCoveragesPage.dwelling, "Other Limits", "Dwelling-Other Limits");
				type(OptionalCoveragesPage.dwellingOther,dwelling,"dwelling Other:"+dwelling);
				type(OptionalCoveragesPage.dwellingOther,Keys.TAB,"Tab");
			}else if(driver.findElement(OptionalCoveragesPage.dwelling).isEnabled()){
				selectByVisibleText(OptionalCoveragesPage.dwelling, dwelling, "Dwelling");
			}
		}
	}
	private void selectContents(String contents) throws Throwable{
		if((contents!= null) && (contents != "")){
			//If value doesn't have %, 'Full Limits', then that value can entered by choosing
			// "Other Limits" and entering value in the input box next to it.
			if(!contents.contains("%") && !contents.contains("Full Limits")){
				selectByVisibleText(OptionalCoveragesPage.contents, "Other Limits", "Contents-Other Limits");
				type(OptionalCoveragesPage.contentsOther,contents,"dwelling Other:"+contents);
				type(OptionalCoveragesPage.contentsOther,Keys.TAB,"Tab");
			}else if(driver.findElement(OptionalCoveragesPage.contents).isEnabled())
				selectByVisibleText(OptionalCoveragesPage.contents, contents, "Contents");
		}
	}

	public void fillOptionalfloodCoverageDetails(Hashtable<String, String> data) throws Throwable{
		switch (data.get("state")) {
		case "MI":
		case "RI":
			selectfloodAdvantage(data.get("floodAdvantage"));
		case "NY":
			selectFloodExtensionDIC(data.get("floodExtensionDIC"));
			break;
		case "CA":
		case "PA":
		case "TX":
			selectfloodAdvantage(data.get("floodAdvantage"));
			break;
		case "FL":
			//FloodAdvantage and FloodExtensionDIC are not applicable
			break;
		}
		if("Yes".equalsIgnoreCase(data.get("excessFloodCoverage"))){
			selectExcessFloodCoverage(data.get("excessFloodCoverage"));
			selectDwelling(data.get("dwelling"));
			selectContents(data.get("contents"));
		}else if("No".equalsIgnoreCase(data.get("excessFloodCoverage"))){
			selectExcessFloodCoverage(data.get("excessFloodCoverage"));
		}
	}

	//Optional Coverage/Exclusions
	private void selectContentsOffPremises(String contentsOffPremises) throws Throwable{
		if((contentsOffPremises!= null) && (contentsOffPremises != "")){
			if(contentsOffPremises.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.contentsOffPremisesYes,"contentsOffPremises--Yes");
			}else{
				click(OptionalCoveragesPage.contentsOffPremisesNo,"contentsOffPremises--No");
			}
		}
	}
	private void selectFineArtExclusion(String fineArtExclusion) throws Throwable{
		if((fineArtExclusion!= null) && (fineArtExclusion != "")){
			if(fineArtExclusion.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.fineArtExclusionYes,"fineArtExclusion--Yes");
			}else{
				click(OptionalCoveragesPage.fineArtExclusionNo,"fineArtExclusion--No");
			}
		}
	}
	private void selectPremisesLiabilityLimitation(String premisesLiabilityLimitation) throws Throwable{
		if((premisesLiabilityLimitation!= null) && (premisesLiabilityLimitation != "")){
			if(premisesLiabilityLimitation.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.premisesLiabilityLimitationYes,"premisesLiabilityLimitation--Yes");
			}else{
				click(OptionalCoveragesPage.premisesLiabilityLimitationNo,"premisesLiabilityLimitation--No");
			}
		}
	}
	private void selectDwellingReplacementCost(String dwellingReplacementCost) throws Throwable{
		if((dwellingReplacementCost!= null) && (dwellingReplacementCost != "")){
			if(dwellingReplacementCost.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.dwellingReplacementCostYes,"dwellingReplacementCost--Yes");
			}else{
				click(OptionalCoveragesPage.dwellingReplacementCostNo,"dwellingReplacementCost--No");
			}
		}
	}
	private void selectLibelSlanderExclusion(String libelSlanderExclusion) throws Throwable{
		if((libelSlanderExclusion!= null) && (libelSlanderExclusion != "")){
			if(libelSlanderExclusion.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.libelSlanderExclusionYes,"libelSlanderExclusion--Yes");
			}else{
				click(OptionalCoveragesPage.libelSlanderExclusionNo,"libelSlanderExclusion--No");
			}
		}
	}
	private void selectEliminationOfWaiver(String eliminationOfWaiver) throws Throwable{
		if((eliminationOfWaiver!= null) && (eliminationOfWaiver != "")){
			if(eliminationOfWaiver.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.eliminationOfWaiverYes,"eliminationOfWaiver--Yes");
			}else{
				click(OptionalCoveragesPage.eliminationOfWaiverNo,"eliminationOfWaiver--No");
			}
		}
	}
	private void selectFungiLiabilityExtension(String fungiLiabilityExtension) throws Throwable{
		if((fungiLiabilityExtension!= null) && (fungiLiabilityExtension != "")){
			if(fungiLiabilityExtension.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.fungiLiabilityExtensionYes,"fungiLiabilityExtension--Yes");
			}else{
				click(OptionalCoveragesPage.fungiLiabilityExtensionNo,"fungiLiabilityExtension--No");
			}
		}
	}

	private void selectRoofCoveringReconstructionCost(String value) throws Throwable{
		if((value!= null) && (value != "")){
			if(value.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.roofCoveringReconstructionCostYes,"roofCoveringReconstructionCost--Yes");
			}else{
				click(OptionalCoveragesPage.roofCoveringReconstructionCostNo,"roofCoveringReconstructionCost--No");
			}
		}
	}

	private void selectRemovalOfRequirement(String value) throws Throwable{
		if((value!= null) && (value != "")){
			if(value.equalsIgnoreCase("Yes")){
				click(OptionalCoveragesPage.removalOfRequirementYes,"removalOfRequirement--Yes");
			}else{
				click(OptionalCoveragesPage.removalOfRequirementNo,"removalOfRequirement--No");
			}
		}
	}
	public void fillOptionalCoveragesOrExclusionsDetails(Hashtable<String, String> data) throws Throwable{
		selectFineArtExclusion(data.get("fineArtExclusion"));
		selectPremisesLiabilityLimitation(data.get("premisesLiabilityLimitation"));
		selectLibelSlanderExclusion(data.get("libelSlanderExclusion"));
		selectEliminationOfWaiver(data.get("eliminationOfWaiver"));
		switch (data.get("state")) {
		case "NY":
			selectDwellingReplacementCost(data.get("dwellingReplacementCost"));
		case "CA":
			selectContentsOffPremises(data.get("contentsOffPremises"));
			break;
		case "FL":
			selectFungiLiabilityExtension(data.get("fungiLiabilityExtension"));
			selectDwellingReplacementCost(data.get("dwellingReplacementCost"));
			break;
		case "TX":
			selectRoofCoveringReconstructionCost(data.get("roofCoveringReconstructionCostYes"));
			selectRemovalOfRequirement(data.get("removalOfRequirement"));
			break;

		}

	}



	//*****************Excess Liability**********************
	private void selectLibelSlanderExclusionEx(String libelSlanderExclusion) throws Throwable{
		selectByVisibleText(OptionalCoveragesPage.libelSlanderExclusion, libelSlanderExclusion, "Political Activity Exclusion");
	}
	private void selectpoliticalActivityExclusion(String politicalActivityExclusion) throws Throwable{
		selectByVisibleText(OptionalCoveragesPage.politicalActivityExclusion, politicalActivityExclusion, "Political Activity Exclusion");
	}
	private void selectpremisesLiabilityLimitation(String premisesLiabilityLimitation) throws Throwable{
		selectByVisibleText(OptionalCoveragesPage.premisesLiabilityLimitation, premisesLiabilityLimitation, "Premises Liability Limitation");
	}
	private void selecthigherUnderlyingLimit(String higherUnderlyingLimit) throws Throwable{
		selectByVisibleText(OptionalCoveragesPage.higherUnderlyingLimit, higherUnderlyingLimit, "Higher Underlying Limit");
	}
	private void selecthomeOwnersLOB() throws Throwable{
		click(OptionalCoveragesPage.homeOwnersLOB, "HomeOwnersLOB");
	}
	private void selectautoLOB() throws Throwable{
		click(OptionalCoveragesPage.autoLOB, "AutoLOB");
	}
	private void selectUIMLOB() throws Throwable{
		click(OptionalCoveragesPage.UIMLOB, "UIMLOB");
	}
	private void selectwaterCraftLOB() throws Throwable{
		click(OptionalCoveragesPage.waterCraftLOB,  "WaterCraftLOB");
	}
	private void selectrecreationalVehiclesLOB() throws Throwable{
		click(OptionalCoveragesPage.recreationalVehiclesLOB, "Recreational VehiclesLOB");
	}
	private void selectincreaseLimitHomeOwners(String increaseLimitHomeOwners) throws Throwable{
		selectByVisibleText(OptionalCoveragesPage.increaseLimitHomeOwners, increaseLimitHomeOwners, "IncreaseLimit HomeOwners");
	}
	private void selectincreaseLimitAuto(String increaseLimitAuto) throws Throwable{
		selectByVisibleText(OptionalCoveragesPage.increaseLimitAuto, increaseLimitAuto, "IncreaseLimit Auto");
	}
	private void selectincreaseLimitUIM(String increaseLimitUIM) throws Throwable{
		selectByVisibleText(OptionalCoveragesPage.increaseLimitUIM, increaseLimitUIM, "IncreaseLimit UIM");
	}
	private void selectincreaseLimitWaterCraft(String increaseLimitWaterCraft) throws Throwable{
		selectByVisibleText(OptionalCoveragesPage.increaseLimitWaterCraft, increaseLimitWaterCraft, "IncreaseLimit WaterCraft");
	}
	private void selectincreaseLimitRecreationalVehicles(String increaseLimitRecreationalVehicles) throws Throwable{
		selectByVisibleText(OptionalCoveragesPage.increaseLimitRecreationalVehicles, increaseLimitRecreationalVehicles, "Increase Limit Recreational Vehicles");
	}
	public void fillOptionalCoverageDetailsOfExcessLiability(Hashtable<String,String> data) throws Throwable{
		selectLibelSlanderExclusionEx(data.get("libelSlanderExclusion"));
		selectpoliticalActivityExclusion(data.get("politicalActivityExclusion"));
		selectpremisesLiabilityLimitation(data.get("premisesLiabilityLimitation"));
		selecthigherUnderlyingLimit(data.get("higherUnderlyingLimit"));
		selecthomeOwnersLOB();
		selectUIMLOB();
		selectwaterCraftLOB();
		selectrecreationalVehiclesLOB();
		selectincreaseLimitHomeOwners(data.get("homeOwnersIncreaseLimit"));
		selectincreaseLimitUIM(data.get("UIMIncreaseLimit"));
		selectincreaseLimitWaterCraft(data.get("waterCratIncreaseLimit"));
		selectincreaseLimitRecreationalVehicles(data.get("recreationalValuesIncreaseLimit"));
		switch(data.get("state")){		  
		case "NY":
		case "CA":
		case "FL":
			selectautoLOB();
			selectincreaseLimitAuto(data.get("autoIncreaseLimit"));
			break;
		case "TX":
			break;
		}
	}


















}
