package com.pure.dragon.homeOwners;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class AdditionalInterestsPageLib extends ActionEngine {
	public AdditionalInterestsPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	private void clickAddInterestButton(String value) throws Throwable{
		if(value!= null && value != "")
		click(AdditionalInterestsPage.addInterestButton, "Add Interest button");
	}
	private void selectType(String value) throws Throwable{
		if(value!= null && value != "")
		selectByVisibleText(AdditionalInterestsPage.typeAI, value, "Type");
	}
	private void setName(String value) throws Throwable{
		if(value!= null && value != "")
			type(AdditionalInterestsPage.name, value, "Name");
	}
	private void setAddressLine1(String value) throws Throwable{
		if(value!= null && value != "")
			type(AdditionalInterestsPage.addressLine1, value, "AddressLine1");
	}
	private void setCity(String value) throws Throwable{
		if(value!= null && value != "")
			type(AdditionalInterestsPage.city, value, "City");
	}
	private void setState(String value) throws Throwable{
		if(value!= null && value != "")
			selectByVisibleText(AdditionalInterestsPage.state, value, "State");
	}
	private void setZip(String value) throws Throwable{
		if(value!= null && value != "")
			type(AdditionalInterestsPage.zip, value, "ZIP");
	}
	
	
	public void fillAdditionalInterestDetails(Hashtable<String, String> data) throws Throwable{
		String additionaInterest = "Mortgagee";//For PA-HO [6/15/2018]
		String mortgageType = "HELOC";
		/**Please cover the followings additional interest as & when needed
		 * Additional Insured
		 * Additional Interest
		 * Mortgagee
		 * Loss Payee
		 * LLC/Trust
		 * **/
		String interest = data.get(additionaInterest+"_Count");
		String type = additionaInterest;
		if(interest!=null){
			if(additionaInterest.contains("Insured")){
				type= "Additional Insured";
			}else if(additionaInterest.contains("Interest")){
				type= "Additional Interest";
			}else if(additionaInterest.equalsIgnoreCase("Mortgagee")){
				type= additionaInterest;
			}else if(additionaInterest.contains("Loss")){
				type= "Loss Payee";
			}else if(additionaInterest.contains("LLC")){
				type= "LLC/Trust";
			}
		
			//For now only Mortgagee is covered
			int numOfMortages = Integer.parseInt(interest);
			for(int i=1;i<=numOfMortages;i++){
				
				By typeAI = By.xpath("(//select[@title='Type'])["+i+"]");
				By name = By.xpath("(//input[@title='Name'])["+i+"]");
				By addressLine1 = By.xpath("(//input[@title='Address Line 1'])["+i+"]");
				By city = By.xpath("(//input[@title='City'])["+i+"]");
				By state = By.xpath("(//select[@title='State'])["+i+"]");
				By zip = By.xpath("(//input[@title='ZIP'])["+i+"]");
				By mortgageTypeAI = By.xpath("(//select[@title='Mortgage Type'])["+i+"]");
				By loan = By.xpath("(//input[@title='Loan #'])["+i+"]");
				
				click(AdditionalInterestsPage.addBtn,"Add additional interests");
				selectByVisibleText(typeAI, type, "Type");
				selectByVisibleText(mortgageTypeAI, mortgageType, "Type");
				type(name, data.get("insuredFirstName"), "Name");
				type(addressLine1,data.get("insuredAddress1"),"address");
				type(city, data.get("insuredCity"), "City");
				selectByVisibleText(state, data.get("state"), "State");
				type(zip, data.get("insuredZip"), "ZIP");
				type(loan, "0", "additional interests -mortage loan value=0");
			}
		}
	}
	
	public void setMortgageonProperty(String yesORno) throws Throwable{
		if(yesORno!=null && 
				yesORno!="" && 
					isVisibleOnly(AdditionalInterestsPage.mortgageOnPropertyYes, "mortgageOnPropertyYes")){
			if(yesORno.equalsIgnoreCase("Yes")){
				click(AdditionalInterestsPage.mortgageOnPropertyYes, "mortgageOnProperty Yes");
			}else if(yesORno.equalsIgnoreCase("No")){
				click(AdditionalInterestsPage.mortgageOnPropertyNo, "mortgageOnProperty No");
			}
		}
	}
	
}
