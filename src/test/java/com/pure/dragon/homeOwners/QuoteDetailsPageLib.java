package com.pure.dragon.homeOwners;

import java.util.Arrays;
import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.HasTouchScreen;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.PolicyEndorsement;
import com.pure.dragon.PolicyEndorsementLib;
import com.pure.report.CReporter;

public class QuoteDetailsPageLib extends ActionEngine{
	public QuoteDetailsPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	private void selectResidenceType(String residenceType) throws Throwable {
		selectByVisibleText(QuoteDetailsPage.residenceType, residenceType, "Residence Type Select");
	}

	private void selectPolicyType(String policyType) throws Throwable {
		selectByVisibleText(QuoteDetailsPage.policyType, policyType, "Policy Type Select");
	}

	public void setEffectiveDate(String value) throws Throwable {
		type(QuoteDetailsPage.effectiveDate, value, "Effective Date");
	}
	public String getAddress1() throws Throwable {
		return getAttributeByValue(QuoteDetailsPage.address1, "Address 1");
	}
	public String getCity() throws Throwable {
		return getAttributeByValue(QuoteDetailsPage.city, "City");
	}
	public String getZIP() throws Throwable {
		return getAttributeByValue(QuoteDetailsPage.zip, "ZIP");
	}
	public void setAddress1(String value) throws Throwable {
		type(QuoteDetailsPage.address1, value, "Address 1");
	}
	public void setCity(String value) throws Throwable {
		type(QuoteDetailsPage.city, value, "City");
	}
	public void setZIP(String value) throws Throwable {
		type(QuoteDetailsPage.zip, value, "ZIP");
	}
	public void setQuoteName(String quoteName) throws Throwable{
		if(quoteName!=null)
		type(QuoteDetailsPage.quoteName,quoteName,"Quote Name");
	}

	private void checkUncheckAutoCompanion(String autoCompanion) throws Throwable{
		//By default its checked
		if(autoCompanion !=null && autoCompanion.equalsIgnoreCase("No")){
			click(QuoteDetailsPage.auto_Companion,"auto comapion");
		}
	}
	private void checkUncheckCollectionCompanion(String collectionCompanion) throws Throwable{
		//By default its checked
		if(collectionCompanion !=null && collectionCompanion.equalsIgnoreCase("No")){
			click(QuoteDetailsPage.collection_Companion,"collection comapion");
		}
	}
	private void checkUncheckExcessCompanion(String excessCompanion) throws Throwable{
		//By default its checked
		if(excessCompanion != null && excessCompanion.equalsIgnoreCase("No")){
			scroll(QuoteDetailsPage.excess_Companion,"excess comapion");
			click(QuoteDetailsPage.excess_Companion,"excess comapion");
		}
	}
	private void checkUncheckHomeOwnersCompanion(String excessCompanion) throws Throwable{
		//By default its checked
		if(excessCompanion != null && excessCompanion.equalsIgnoreCase("No")){
			scroll(QuoteDetailsPage.home_companion,"home comapion");
			click(QuoteDetailsPage.home_companion,"home comapion");
		}
	}

	public void selectLossOfUse(String value) throws Throwable{
		selectByVisibleText(QuoteDetailsPage.allPerilDeductible, value, "Loss Of Use Select");
	}

	private void selectAllPerilDeductible(String allPerilDeductible) throws Throwable {
		selectByVisibleText(QuoteDetailsPage.allPerilDeductible, allPerilDeductible, "All Peril Deductible Select");
		waitUntilJQueryReady();
	}
	private void selectLoss_Of_Use_Pct(String Loss_Of_Use_Pct) throws Throwable {
		if(Loss_Of_Use_Pct != null)
			selectByVisibleText(QuoteDetailsPage.Loss_Of_Use_Pct, Loss_Of_Use_Pct, "Loss_Of_Use_Pct");
	}

	public void selectWindstormHailDeductible(String deductible) throws Throwable {
		//selectByIndex(QuoteDetailsPage.windstormHailDeductible, 1, "Windstorm or Hail Deductible");
		selectByVisibleText(QuoteDetailsPage.windstormHailDeductible, deductible, "Windstorm or Hail Deductible");
	}

	private void selectPersonalLiabilityLimit(String personalLiabilityLimit) throws Throwable {
		selectByVisibleText(QuoteDetailsPage.personalLiabilityLimit, personalLiabilityLimit, "Personal Liability Limit Select");
	}

	public void selectCyberLimit(String cyberLimit) throws Throwable {
		selectByVisibleText(QuoteDetailsPage.cyberLimit, cyberLimit, "Cyber Limit Select");
	}

	public void selectCyberDeductible(String cyberDeductible) throws Throwable {
		selectByVisibleText(QuoteDetailsPage.cyberDeductible, cyberDeductible, "Cyber Deductible Select");
	}

	private void setReplacementValue(String replacementValue) throws Throwable {
		type(QuoteDetailsPage.replacementValue, replacementValue, "Replacement Value type");
		type(QuoteDetailsPage.replacementValue, Keys.TAB, "tab key");		
		waitUntilJQueryReady();
	}

	public void setOtherStructureValue(String otherStructureValue) throws Throwable {		
		type(QuoteDetailsPage.otherStructureValue, otherStructureValue, "Other Structure Value type");
	}
		
	private void setContentValue(String contentValue) throws Throwable {
		type(QuoteDetailsPage.contentValue, contentValue, "Content Value type");
	}

	public void selectCyberDefenseCoverage(String value) throws Throwable {
		if(isVisibleOnly(QuoteDetailsPage.yesCyberDefenseCoverage, "Fraud and Cyber Defense Coverage")){
			if(!value.equalsIgnoreCase("null") && value.equalsIgnoreCase("YES")){
				click(QuoteDetailsPage.yesCyberDefenseCoverage, "Fraud and Cyber Defense Coverage - Yes");
			}else{
				//click(QuoteDetailsPage.noCyberDefenseCoverage, "Fraud and Cyber Defense Coverage - No");
				JSClick(QuoteDetailsPage.noCyberDefenseCoverage, "Fraud and Cyber Defense Coverage - No");
				checkBoxIsChecked(QuoteDetailsPage.noCyberDefenseCoverage, "Fraud and Cyber Defense Coverage - No", true);
			}
		}
	}

	public void selectMoneyStolen(String value) throws Throwable {
		if(value.equalsIgnoreCase("YES")){
			click(QuoteDetailsPage.moneyStolenYes, "Money Stolen - Yes");
		}else{
			click(QuoteDetailsPage.moneyStolenNo, "Money Stolen - No");
		}
	}

	public void selectProtectedByActiveSub(String value) throws Throwable {
		if(value.equalsIgnoreCase("YES")){
			click(QuoteDetailsPage.protectedByActiveSubscriptionYes, "Protected By Active Subscription - Yes");
		}else{
			click(QuoteDetailsPage.protectedByActiveSubscriptionNo, "Protected By Active Subscription - No");
		}
	}
	/*Function to verify deductible and available questions*/ 
	public void verifyCyberLimitQuestions() throws Throwable {
		//For 100,000 limit - 500 deductible
		selectByVisibleText(QuoteDetailsPage.cyberLimit, "100,000", "Cyber Limit Select");
		waitUntilJQueryReady();
		assertTrue(getText(QuoteDetailsPage.cyberDeductible, "Cyber Deductible").equalsIgnoreCase("500"), "Cyber Deductible");
		//For 250,000 limit - 1,000 deductible
		selectByVisibleText(QuoteDetailsPage.cyberLimit, "250,000", "Cyber Limit Select");
		waitUntilJQueryReady();
		assertTrue(getText(QuoteDetailsPage.cyberDeductible, "Cyber Deductible").equalsIgnoreCase("1,000"), "Cyber Deductible");
		//isVisible(QuoteDetailsPage.question1, "Question1#Within the last 24 months, has the Insured or family member had money stolen or been deceived into making a payment or parting with something of value?");
		String question1 = "Within the last 24 months, has the Insured or family member had money stolen or been deceived into making a payment or parting with something of value?";
		String question2 = "Are the Insured and family members protected by an active subscription to Rubica or Rubica-like Cyber Security Monitoring Service?";
		assertTrue(getAttributeValue(QuoteDetailsPage.question1, "innerText").equalsIgnoreCase(question1), "Verifying question1 text");

		//For 1,000,000 limit - 1,000 deductible
		selectByVisibleText(QuoteDetailsPage.cyberLimit, "1,000,000", "Cyber Limit Select");
		waitUntilJQueryReady();
		assertTrue(getText(QuoteDetailsPage.cyberDeductible, "Cyber Deductible").equalsIgnoreCase("1,000"), "Cyber Deductible");
		assertTrue(getAttributeValue(QuoteDetailsPage.question1, "innerText").equalsIgnoreCase(question1), "Verifying question1 text");
		assertTrue(getAttributeValue(QuoteDetailsPage.question2, "innerText").equalsIgnoreCase(question2), "Verifying question2 text");
	}
	public void fillDetailsPredictiveAnalytics(Hashtable<String, String> data) throws Throwable{
		switch (data.get("state")) {
		case "AZ":
		case "NV":
			selectPolicyType(data.get("policyType"));
			break;
		default:
			selectResidenceType(data.get("residenceType"));
			break;
		}
		setReplacementValue(data.get("replacementValue"));//other structure and contents will be autopopulated
		waitUntilJQueryReady();
		setOtherStructureValue(data.get("otherStructureValue"));
		setContentValue(data.get("contentValue"));
		waitUntilJQueryReady();
		//Save changes for hurricane deductible
		new CommonPageLib(driver, reporter).clickSaveChangesBtn();
		//additional state specific options
		switch (data.get("state")) {
		case "NC":
		case "VT":
		case "TN":
		case "HI":
		case "NY":
			selectAllPerilDeductible(data.get("allPerilDeductible"));break;
		case "TX":
		case "IN":
		case "AR":
		case "OH":
		case "KS":
		case "MT":
		case "CO":
		case "GA":
		case "RI":
		case "OK":
		case "MO":
		case "MN":
		case "MS":
			selectWindstormHailDeductible(data.get("windstormHailDeductible"));break;			
		case "KY":
		case "NH":
		case "ND":
		case "ME":
		case "NJ":
			selectAllPerilDeductible(data.get("allPerilDeductible"));
			selectWindstormHailDeductible(data.get("windstormHailDeductible"));break;
		case "WA":
			selectAllPerilDeductible(data.get("allPerilDeductible"));
			selectFamilyUnits(data.get("noOfFamilyUnits"));break;
		case "CT":
			selectHurricaneDeductible(data.get("hurricaneDeductible"));break;
		case "SC":
		case "MA":
		case "LA":
		case "AL":
		case "DE":
			selectHurricaneNamedDeductible(data.get("hurricaneDeductible"));break;
		}
		selectPersonalLiabilityLimit(data.get("personalLiabilityLimit"));
		numberOfEmployees(data);
	}


	public void fillDetails(Hashtable<String, String> data, boolean testCyber) throws Throwable{
		String type = "";
		switch (data.get("state")) {
		case "AZ":
		case "NV":
			selectPolicyType(data.get("policyType"));
			type = data.get("policyType");
			break;
		default:
			selectResidenceType(data.get("residenceType"));
			type = data.get("residenceType");
			break;
		}
		if(("Homeowner").equalsIgnoreCase(type)){
			setReplacementValue(data.get("replacementValue"));//other structure and contents will be autopopulated
			waitUntilJQueryReady();
			String currentOtherStructureValue = getAttributeByValue(QuoteDetailsPage.otherStructureValue, "Other Structure Value");
			if(currentOtherStructureValue.equals("") || 
					!currentOtherStructureValue.replace(",","").equals(data.get("otherStructureValue"))){
				setOtherStructureValue(data.get("otherStructureValue"));
				currentOtherStructureValue = getAttributeByValue(QuoteDetailsPage.otherStructureValue, "Other Structure Value");
				if(!currentOtherStructureValue.equals(data.get("otherStructureValue"))){
					setOtherStructureValue(data.get("otherStructureValue"));
				}
			}
			String currentContentValue = getAttributeByValue(QuoteDetailsPage.contentValue, "Content Value");
			if(currentContentValue.equals("") ||				 
					(!currentContentValue.equals(data.get("contentValue")))){			
				setContentValue(data.get("contentValue"));
				String newContentValue = getAttributeByValue(QuoteDetailsPage.contentValue, "Content Value").replace(",", "");
				if(newContentValue.equals(data.get("contentValue"))){
					setContentValue(data.get("contentValue"));
				}
			}
		}
		if(("Condo/Co-op").equalsIgnoreCase(type)||("Tenants").equalsIgnoreCase(type)){
			String currentContentValue = getAttributeByValue(QuoteDetailsPage.contentValue, "Content Value");
			if(currentContentValue.equals("") ||					
					(!currentContentValue.equals(data.get("contentValue")))){			
				setContentValue(data.get("contentValue"));
				String newContentValue = getAttributeByValue(QuoteDetailsPage.contentValue, "Content Value").replace(",", "");
				if(newContentValue.equals(data.get("contentValue"))){
					setContentValue(data.get("contentValue"));
				}
			}
		}
		waitUntilJQueryReady();
		//Save changes for hurricane deductible
		new CommonPageLib(driver, reporter).clickSaveChangesBtn();
		//additional state specific options
		switch (data.get("state")) {
		case "NC":
		case "VT":
		case "TN":
		case "HI":
		case "NY":
			selectAllPerilDeductible(data.get("allPerilDeductible"));break;
		case "RI":
			checkUncheckAutoCompanion(data.get("Auto_Companion"));
			checkUncheckExcessCompanion(data.get("Excess_Companion"));
			checkUncheckCollectionCompanion(data.get("Collections_Companion"));
			selectAllPerilDeductible(data.get("allPerilDeductible"));
			selectLoss_Of_Use_Pct(data.get("Loss_Of_Use"));
			/***************************************************************************
			 ***********Below code is only for RI HO UAT [6/5/2018] ********************
			 *********** Modify this for further RI executions according to requirements
			 ***************************************************************************/
			String[] listOptions = getOptionList(QuoteDetailsPage.hurricaneDeductible, "options in hurricaneDeductible");
			String toSelect=data.get("hurricaneDeductible");
			if(toSelect.equalsIgnoreCase("N/A - AOP Ded Applies") && Arrays.asList(listOptions).contains("Not Available")){
				selectHurricaneDeductible("Not Available");
			}else if(toSelect.equalsIgnoreCase("Not Available") && Arrays.asList(listOptions).contains("N/A - AOP Ded Applies")){
				selectHurricaneDeductible("N/A - AOP Ded Applies");
			}else{
				selectHurricaneDeductible(toSelect);
			}			
			break;
			
			//*******************************************//
		case "TX":
		case "IN":
		case "OH":
		case "KS":
		case "MT":
		case "CO":
		case "GA":
		case "OK":
		case "MN":
		case "MS":
		case "FL":
			selectWindstormHailDeductible(data.get("windstormHailDeductible"));break;	
		case "MO":
		case "WY":
		case "IL":
		case "NJ":
		case "AR":
			checkUncheckAutoCompanion(data.get("Auto_Companion"));
			checkUncheckExcessCompanion(data.get("Excess_Companion"));
			checkUncheckCollectionCompanion(data.get("Collections_Companion"));
			selectLoss_Of_Use_Pct(data.get("Loss_Of_Use"));
			selectAllPerilDeductible(data.get("allPerilDeductible"));
			selectWindstormHailDeductible(data.get("windstormHailDeductible"));break;
		case "KY":
		case "NH":
		case "ND":
		case "ME":
			selectAllPerilDeductible(data.get("allPerilDeductible"));
			selectWindstormHailDeductible(data.get("windstormHailDeductible"));break;
		case "WA":
			selectAllPerilDeductible(data.get("allPerilDeductible"));
			selectFamilyUnits(data.get("noOfFamilyUnits"));break;
		case "CT":
		case "LA":
			selectHurricaneDeductible(data.get("hurricaneDeductible"));break;
		case "SC":
			selectHurricaneNamedStormDeductible(data.get("hurricaneDeductible"));break;
		case "AL":
		case "MA":
		case "DE":			
			selectHurricaneNamedDeductible(data.get("hurricaneDeductible"));
		case "OR":
		case "PA":
		case "VA":			
			checkUncheckAutoCompanion(data.get("Auto_Companion"));
			checkUncheckExcessCompanion(data.get("Excess_Companion"));
			checkUncheckCollectionCompanion(data.get("Collections_Companion"));
			selectLoss_Of_Use_Pct(data.get("Loss_Of_Use"));
			selectAllPerilDeductible(data.get("allPerilDeductible")); break;
		}
		selectPersonalLiabilityLimit(data.get("personalLiabilityLimit"));
		numberOfEmployees(data);
		String cyberDefenseCoverage=data.get("cyberDefenseCoverage");
		if(testCyber){						
			selectCyberDefenseCoverage(cyberDefenseCoverage);
			verifyCyberLimitQuestions();		
			selectCyberLimit(data.get("cyberLimit"));
			switch (data.get("cyberLimit")) {
			case "250,000":
				selectMoneyStolen(data.get("isMoneyStolen"));
				break;

			case "1,000,000":
				selectMoneyStolen(data.get("isMoneyStolen"));
				selectProtectedByActiveSub(data.get("isProtectedByActiveSub"));
				break;
			}
		}else if(cyberDefenseCoverage!= null && cyberDefenseCoverage.equalsIgnoreCase("no")){
				selectCyberDefenseCoverage(cyberDefenseCoverage);			
		}
	}

	private void numberOfEmployees(Hashtable<String, String> data) throws Throwable{
		if(isVisibleOnly(QuoteDetailsPage.noOfEmployees, "No of employees")){
			selectByVisibleText(QuoteDetailsPage.noOfEmployees, data.get("noOfEmployees"), "No of employees");
		}
	}

	public void verifyCyberAvailable() throws Throwable{
		waitUntilJQueryReady();
		isVisible(QuoteDetailsPage.cyberLimit, "Cyber Limit");
		isEnabled(QuoteDetailsPage.cyberLimit, "Cyber Limit");
		isVisible(QuoteDetailsPage.yesCyberDefenseCoverage, "Cyber Defense Coverage");
		isEnabled(QuoteDetailsPage.yesCyberDefenseCoverage, "Cyber Defense Coverage");
	}

	public void verifyCyberNotAvailable(String policyNumber) throws Throwable{
		waitUntilJQueryReady();
		verifyElementPresent(QuoteDetailsPage.cyberLimit, "Cyber Limit", false);
		verifyElementPresent(QuoteDetailsPage.policyNumber, "Policy Number with Cyber", true);
		assertTextMatching(QuoteDetailsPage.policyNumber, policyNumber, "Policy Number with Cyber");

		//		verifyElementPresent(QuoteDetailsPage.yesCyberDefenseCoverage, "Cyber Defense Coverage", false);

	}
	private void selectFamilyUnits(String noOfFamilyUnits) throws Throwable {
		selectByVisibleText(QuoteDetailsPage.noOfFamilyUnits, noOfFamilyUnits, "No of Family Units");
	}
	private void selectHurricaneDeductible(String hurricaneDeductible) throws Throwable {
		//selectByIndex(QuoteDetailsPage.hurricaneDeductible, 1, "HurricaneDeductible Select");
		selectByVisibleText(QuoteDetailsPage.hurricaneDeductible, hurricaneDeductible, "HurricaneDeductible Select");
	}
	private void selectHurricaneNamedDeductible(String hurricaneNamedDeductible) throws Throwable {
		//selectByIndex(QuoteDetailsPage.hurricaneAndNamedDeductible, 1, "Hurricane And Named Deductible Select");
		selectByVisibleText(QuoteDetailsPage.hurricaneAndNamedDeductible, hurricaneNamedDeductible, "Hurricane And Named Deductible Select");
	}
	private void selectHurricaneNamedStormDeductible(String hurricaneNamedStormDeductible) throws Throwable {
		//selectByIndex(QuoteDetailsPage.hurricaneAndNamedDeductible, 1, "Hurricane And Named Deductible Select");
		selectByVisibleText(QuoteDetailsPage.hurricaneAndNamedStormDeductible, hurricaneNamedStormDeductible, "Hurricane And Named Storm Deductible Select");
	}
	public void navigateToCustomer() throws Throwable{
		waitUntilJQueryReady();
		new PolicyEndorsementLib(driver, reporter).navigateToCustomerDetails();
	}

	public void clickObtainOperatorsAndVehicles() throws Throwable{
		click(QuoteDetailsPage.obtainOperatorsAndVehiclesBtn, "obtainOperatorsAndVehiclesBtn");
	//	click(QuoteDetailsPage.cancelBtn, "Cancel Button");
		acceptAlert();
		waitUntilJQueryReady();
	}

	private void nonCATPropertyLoss(String nonCATPropertyLoss) throws Throwable {
		if(nonCATPropertyLoss.equalsIgnoreCase("Yes")){
			click(QuoteDetailsPage.nonCATPropertyLossYes, "nonCATPropertyLoss Yes");
		}else {
			click(QuoteDetailsPage.nonCATPropertyLossNo, "nonCATPropertyLoss No");
		}
	}

	private void insureTwoOrMoreProperties(String insureTwoOrMoreProperties) throws Throwable {
		if(insureTwoOrMoreProperties.equalsIgnoreCase("Yes")){
			click(QuoteDetailsPage.insureTwoOrMorePropertiesYes, "insureTwoOrMoreProperties Yes");
		}else {
			click(QuoteDetailsPage.insureTwoOrMorePropertiesNo, "insureTwoOrMoreProperties No");
		}
	}

	public void fillPersonalAutoCoverDetails(Hashtable<String, String> data) throws Throwable
	{
		switch (data.get("state")) {
		case "TX":			
		case "FL":
		case "NJ":
		case "AL":
		case "CO":
		case "CT":
		case "IL":
			nonCATPropertyLoss(data.get("nonCATPropertyLoss"));
			insureTwoOrMoreProperties(data.get("insureTwoOrMoreProperties"));
			break;
		}
		checkUncheckHomeOwnersCompanion(data.get("Homeowners_Companion_Credit_Ind"));
		checkUncheckExcessCompanion(data.get("Personal_Excess_Companion_Credit_Ind"));
		if(!data.get("state").equalsIgnoreCase("IA")){
			//For IA state Jewelry companion option is not listed.
			checkUncheckCollectionCompanion(data.get("Jewelry_Companion_Credit_Ind"));
		}
		
		if("".equalsIgnoreCase(getAddress1())){setAddress1(data.get("insuredAddress1"));}
		if("".equalsIgnoreCase(getCity())){setCity(data.get("insuredCity"));}
		if("".equalsIgnoreCase(getZIP())){setZIP(data.get("insuredZip"));}
		clickObtainOperatorsAndVehicles();
		setQuoteName(data.get("QuoteName"));

	}




	/*************************************************************************
	 *************  Excess Liability
	 *************************************************************************/
	private void selectHaveYouBeenDeclinedCancelledNonRenewed(String value) throws Throwable{	
		if(value.equalsIgnoreCase("Yes")){
			click(QuoteDetailsPage.anyDeclinedCancelledNonRenewedYes, "Declined Policy? - Yes");
		}else{
			click(QuoteDetailsPage.anyDeclinedCancelledNonRenewedNo, "Declined Policy?  - No");
		}
	}
	private void selectAnyHouseholdMemberInvolvedInLitigation(String value) throws Throwable{
		if(value.equalsIgnoreCase("Yes")){
			click(QuoteDetailsPage.anyHouseholdMemberInvolvedInLitigationYes, "Insurance for HouseholdMember - Yes");
		}else{
			click(QuoteDetailsPage.anyHouseholdMemberInvolvedInLitigationNo, "Insurance for HouseholdMember - No");
		}
	}
	private void selectDoYouEmployAnyDomesticEmployee(String value) throws Throwable{
		if(value.equalsIgnoreCase("Yes")){
			click(QuoteDetailsPage.anyDomesticEmployeeYes, "Do you employ any domestic employees  - Yes");
		}else{
			click(QuoteDetailsPage.anyDomesticEmployeeNo, "Do you employ any domestic employees  - No");
		}
	}
	private void selectAnyHouseholdMemberMadeAllegation(String value) throws Throwable {
		if(value.equalsIgnoreCase("YES")){
			click(QuoteDetailsPage.anyHouseholdMemberMadeAllegationAgainstEmployeeYes, "Household Member Eligation - Yes");
		}else{
			click(QuoteDetailsPage.anyHouseholdMemberMadeAllegationAgainstEmployeeNo, "Household Member Eligation - No");
		}
	}
	private void selectNoOfVehiclesOwnedOrLeased(String value) throws Throwable{
		selectByVisibleText(QuoteDetailsPage.numOfVehicles, value, "Number of vehicles");
	}
	public void fillQuoteDetailsOfExcessLiability(Hashtable<String, String> data) throws Throwable{
		selectHaveYouBeenDeclinedCancelledNonRenewed(data.get("haveYouBeenDeclinedCancelledNonRenewed"));
		selectAnyHouseholdMemberInvolvedInLitigation(data.get("anyHouseholdMemberInvolvedInLitigation"));
		selectDoYouEmployAnyDomesticEmployee(data.get("doYouEmployAnyDomesticEmployee"));
		selectAnyHouseholdMemberMadeAllegation(data.get("anyHouseholdMemberHadAllegation"));
		switch (data.get("state")) {
		case "NY":
			selectNoOfVehiclesOwnedOrLeased(data.get("noOfVehiclesOwnedOrLeased"));
			break;
		case "TX":
		case "CA":
		case "FL":
			break;
		}
	}

}
