package com.pure.dragon.homeOwners;

import org.openqa.selenium.By;

public class AdditionalInterestsPage {
	static By addInterestButton;
	static By typeAI, name, addressLine1, city, state, zip;
	static By mortgageOnPropertyNo,mortgageOnPropertyYes;
	static By addBtn;
	
	static{
		addInterestButton = By.xpath("//a[contains(@onclick,'Action.189')]");
		typeAI = By.xpath("//select[@title='Type']");
		name = By.xpath("//input[@title='Name']");
		addressLine1 = By.xpath("//input[@title='Address Line 1']");
		city = By.xpath("//input[@title='City']");
		state = By.xpath("//select[@title='State']");
		zip = By.xpath("//input[@title='ZIP']");
		mortgageOnPropertyNo = By.xpath("//div[contains(@title,'mortgage on the property')]/following-sibling::div//input[@title='No']");
		mortgageOnPropertyYes = By.xpath("//div[contains(@title,'mortgage on the property')]/following-sibling::div//input[@title='Yes']");
		addBtn = By.xpath("//a[@title='Add another item.']");
	}
}
