package com.pure.dragon.homeOwners;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.HasTouchScreen;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class LocationCoveragePageLib extends ActionEngine{
	public LocationCoveragePageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	private void selectPrimaryResidence(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.primaryResidenceYes, "Primary Residence - Yes");
			}else{
				click(LocationCoveragePage.primaryResidenceNo, "Primary Residence - No");
			}
		}
	}

	private void selectSecondaryResidence(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.secondaryResidenceYes, "Secondary Residence - Yes");
			}else{
				click(LocationCoveragePage.secondaryResidenceNo, "Secondary Residence - No");
			}
		}
	}
	private void selectMortgageeInfo(Hashtable<String, String> data) throws Throwable {
		if(isVisibleOnly(LocationCoveragePage.mortgageeInfoNO, "mortgageeInfoNO")){
			String v = data.get("Mortgagee_Count");
			if(v!=null || v!=""){
				int numOfCOunt = Integer.parseInt(v);
				if(numOfCOunt>0){					
					click(LocationCoveragePage.mortgageeInfoYES, "mortgageeInfoYES");
				}else if(numOfCOunt==0){
					click(LocationCoveragePage.mortgageeInfoNO, "mortgageeInfoNO");
				}
			}
		}
	}

	private void selectRentedResidence(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.rentedResidenceYes, "Rented Residence - Yes");
			}else{
				click(LocationCoveragePage.rentedResidenceNo, "Rented Residence - No");
			}
		}
	}

	public void selectInsuredByPure(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.insuredByPureYes, "Insured By Pure - Yes");
			}else{
				click(LocationCoveragePage.insuredByPureNo, "Insured By Pure - No");
			}
		}
	}

	private void selectVacantResidence(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.vacantResidenceYes, "Vacant Residence - Yes");
			}else{
				click(LocationCoveragePage.vacantResidenceNo, "Vacant Residence - No");
			}
		}
	}

	private void selectMajorRenovation(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.renovationYes, "Major Renovation - Yes");
			}else{
				click(LocationCoveragePage.renovationNo, "Major Renovation - No");
			}
		}
	}

	private void selectForSale(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.forSaleYes, "For Sale - Yes");
			}else{
				click(LocationCoveragePage.forSaleNo, "For Sale - No");
			}
		}
	}

	private void selectGroundUpConstruction(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.groundUpConstructionYes, "groundUpConstruction - Yes");
			}else{
				click(LocationCoveragePage.groundUpConstructionNo, "groundUpConstruction - No");
			}
		}
	}
	private void homeRatingcharsPrefill(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.homeRatingcharsPrefillYes,"homeRatingcharsPrefill - Yes");
			}else{
				click(LocationCoveragePage.homeRatingcharsPrefillNo, "homeRatingcharsPrefill - No");
			}
		}
	}

	private void setYearBuilt(String year) throws Throwable {
		if(year != null && year != ""){
			type(LocationCoveragePage.yearBuiltInput, year, "Year built type");
			type(LocationCoveragePage.yearBuiltInput, Keys.TAB, "Tab key");
		}
	}
	private void setYearBuiltID(String year) throws Throwable {
		if(year != null && year != ""){
			type(LocationCoveragePage.yearBuiltID, year, "Year built");
			type(LocationCoveragePage.yearBuiltID, Keys.TAB, "Tab key");
		}
	}

	private void setRenovationYear(String year) throws Throwable {
		if(!(year == null) && !year.equalsIgnoreCase("Null") && (!year.equals(""))){
			type(LocationCoveragePage.yearRenovatedInput, year, "Year renovated type");
			waitUntilJQueryReady();
			String yearRenovated = getAttributeByValue(LocationCoveragePage.yearRenovatedInput, "Year renovated type");
			if(!yearRenovated.equalsIgnoreCase(year)){
				type(LocationCoveragePage.yearRenovatedInput, year, "Year renovated type");
			}
		}
	}

	private void selectProtectionClass(String protectionClass) throws Throwable {
		if(protectionClass != null && protectionClass != ""){
			if(isVisibleOnly(LocationCoveragePage.protectionClass, "Protection Class Select")){
				if(!optionAvailable(LocationCoveragePage.protectionClass, protectionClass)){
					selectProtectionClassByIndex();
					click(LocationCoveragePage.managerPPCOverride, "managerPPCOverride");
					//waitUntilLoadingMaskDisappear();
					selectByVisibleText(LocationCoveragePage.protectionClassOverride, protectionClass, "Protection Class Override Select");					
				}else if(optionAvailable(LocationCoveragePage.protectionClass, protectionClass)){
					selectByVisibleText(LocationCoveragePage.protectionClass, protectionClass, "Protection Class Select");
				}
				/*else{
								selectProtectionClassByIndex();
							}*/
			}
		}
	}

	private void selectProtectionClassByIndex() throws Throwable {
		if(isVisibleOnly(LocationCoveragePage.protectionClass, "Protection Class Select")){
			selectByIndex(LocationCoveragePage.protectionClass, 1, "Protection Class Select");
		}
	}

	public void selectProtectionClass_Backup() throws Throwable {
		if(isVisibleOnly(LocationCoveragePage.protectionClass, "Protection Class Select"))
			selectByIndex(LocationCoveragePage.protectionClass, 1, "Protection Class Select");

		if(isVisibleOnly(LocationCoveragePage.openingProtectionType, "Opening Protection Type"))
			selectByIndex(LocationCoveragePage.openingProtectionType, 1, "Opening Protection Type");
	}

	private void setSquareFoot(String squareFootage) throws Throwable {
		if(squareFootage != null && squareFootage != ""){
			type(LocationCoveragePage.squareFootage, squareFootage, "Square Footage type");
		}
	}

	private void selectConstructionType(String constructionType) throws Throwable {
		if(constructionType != null && constructionType != ""){
			selectByVisibleText(LocationCoveragePage.constructionType, constructionType, "Construction Type Select");
		}
	}

	private void setNoOfFloors(String noOfFloors) throws Throwable {
		if(noOfFloors != null && noOfFloors != "")
			type(LocationCoveragePage.noOfFloors, noOfFloors, "Number of floors type");
	}

	private void selectRoofShape(String roofShape) throws Throwable {
		if(roofShape!=null && !roofShape.equalsIgnoreCase("null") && roofShape != "")
			selectByVisibleText(LocationCoveragePage.roofShape, roofShape, "Roof Shape Select");
	}

	private void selectRoofCoveringCredit(String roofCoveringCredit) throws Throwable {
		if(isVisibleOnly(LocationCoveragePage.roofCoveringCredit, "Roof Covering Credit")){
			if(roofCoveringCredit != null && roofCoveringCredit != ""){
				selectByVisibleText(LocationCoveragePage.roofCoveringCredit, roofCoveringCredit, "Roof Covering Credit Select");
			}
		}
	}

	private void selectRoofCoveringMaterial(String roofCoveringMaterial) throws Throwable {
		if(roofCoveringMaterial != null && roofCoveringMaterial != "")
			selectByVisibleText(LocationCoveragePage.roofCoveringMaterial, roofCoveringMaterial, "Roof Covering Material Select");
	}

	private void selectRoofCovering(String roofCovering) throws Throwable {
		if(roofCovering != null && !roofCovering.equalsIgnoreCase("NULL") && roofCovering != "")
			selectByVisibleText(LocationCoveragePage.roofCovering, roofCovering, "Roof Covering Select");
	}

	private void selectOpeningProtection(Hashtable<String, String> data) throws Throwable {
		if(data.get("openingProtection")!=null && !data.get("openingProtection").equalsIgnoreCase("null")&& data.get("openingProtection")!= "")
			selectByVisibleText(LocationCoveragePage.openingProtection, data.get("openingProtection"), "Opening Protection Select");
		if(data.get("openingProtection").equalsIgnoreCase("Impact Shutters")){
			selectByVisibleText(LocationCoveragePage.impactShuttersType, data.get("impactShutterType"), "ImpactShutterType");
		}
	}

	private void selectFireAlarm(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.fireAlarmYes, "Fire Alarm - Yes");
			}else{
				click(LocationCoveragePage.fireAlarmNo, "Fire Alarm - No");
			}
		}
	}

	private void selectBurglarAlarm(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.burglarAlarmYes, "Burglar Alarm - Yes");
			}else{
				click(LocationCoveragePage.burglarAlarmNo, "Burglar Alarm - No");
			}
		}
	}

	private void selectWaterLeakageSystem(String waterLeakageSystem) throws Throwable {
		if(waterLeakageSystem != null && waterLeakageSystem != ""){
			if(isVisibleOnly(LocationCoveragePage.waterLeakageSystem, "Water Leakage System Select"))
				selectByVisibleText(LocationCoveragePage.waterLeakageSystem, waterLeakageSystem, "Water Leakage System Select");
		}
	}

	private void selectPureNFIP(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.pureNfipYes, "Pure NFIP with Max limits - Yes");
			}else{
				click(LocationCoveragePage.pureNfipNo, "Pure NFIP with Max limits - No");
			}
		}
	}
	private void selectNFIPMaxLimits(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.NFIPMaxlimitsYes, "NFIP with Max limits - Yes");
			}else if(value.equalsIgnoreCase("No")){
				click(LocationCoveragePage.NFIPMaxlimitsNo, "NFIP with Max limits - No");
			}
		}
	}
	public void selectWindMitigation(Hashtable<String, String> data) throws Throwable {
		String[] invalidStates = {"MI"};
		String state = data.get("state");
		selectRoofShape(data.get("roofShape"));
		if(!Arrays.asList(invalidStates).contains(data.get("state"))){
			if(data.get("residenceType").equalsIgnoreCase("Homeowner") || data.get("residenceType").equalsIgnoreCase("Condo/Co-op") || data.get("residenceType").equalsIgnoreCase("Tenants")){
				switch(state){
				case "AR":				
					selectRoofCovering(data.get("roofCovering"));
					setYearRoofReplaced(data.get("Year_Roof_Replaced_No"));
					selectRoofCoveringCredit(data.get("roofCoveringCredit"));
					break;
				case "IN":
					selectRoofCoveringCredit(data.get("roofCoveringCredit"));
					selectRoofCoveringMaterial(data.get("roofCoveringMaterial"));
					break;
				case "KY":
				case "ME":
				case "CA":
				case "NH":
				case "ND":
				case "OR":
				case "SD":
				case "TN":
				case "VT":
				case "WV":
					selectRoofCovering(data.get("roofCovering"));
					break;	
				case "HI":
					selectRoofCovering(data.get("roofCovering"));
					selectOpeningProtection(data);
					selectWallConstructionType(data.get("wallConstructionType"));
					break;
				case "LA":
					selectRoofCovering(data.get("roofCovering"));
					selectOpeningProtection(data);
					//selectRoofDeck(data.get("roofDeck")); //Check if this is used in state SC
					selectRoofDeckAttachment(data.get("roofDeck"));
					selectWaterResist(data.get("secondaryWaterResist"));
					selectRoofWall(data.get("roofWall"));
					break;
				case "RI":
					setYearRoofReplaced(data.get("Year_Roof_Replaced_No"));
				case "MA":			
				case "MD":
				case "SC":
					selectRoofCovering(data.get("roofCovering"));
					selectOpeningProtection(data);
					selectRoofDeckAttachment(data.get("roofDeck"));
					selectWaterResist(data.get("secondaryWaterResist"));
					selectRoofWall(data.get("roofWall"));
					break;
					// For Florida state if ex-wind is yes or no, wind mitigation will come
				case "FL":
					selectOpeningProtection(data);
					selectRoofDeck(data.get("roofDeck"));
					selectWaterResist(data.get("secondaryWaterResist"));
					break;	
				}
			}
			if(data.get("residenceType").equalsIgnoreCase("Homeowner")){
				switch(state){				
				case "CO":				
				case "IL":
				case "KS":
				case "MN":
				case "MT":
				case "NE":
				case "OK":
					selectRoofCoveringCredit(data.get("roofCoveringCredit"));
					selectRoofCoveringMaterial(data.get("roofCoveringMaterial"));
					break;
				case "IA":
				case "NV":
				case "NM":
				case "UT":
				case "WA":
				case "WI":
				case "WY":
				case "MS":
					selectRoofCovering(data.get("roofCovering"));
					break;				
				case "MO":
				case "OH":
					selectRoofCoveringCredit(data.get("roofCoveringCredit"));
					selectRoofCoveringMaterial(data.get("roofCoveringMaterial"));
					setYearRoofReplaced(data.get("Year_Roof_Replaced_No"));
					break;		
				case "AL":
				case "NC":
					selectHurricaneFortification(data.get("hurricaneFortification"));
					selectOpeningProtection(data);
					selectRoofCovering(data.get("roofCovering"));
					break;			
				}
			}
			if(!data.get("residenceType").equals("Condo/Co-op")){
				switch(state){
				case "AK":
				case "AZ":
				case "CT":
				case "DC":					
				case "GA":
				case "NJ":
				case "NY":
				case "PA":
				case "VA":
					selectOpeningProtection(data);
					selectRoofCovering(data.get("roofCovering"));
					break;
				case "DE":
					setYearRoofReplaced(data.get("Year_Roof_Replaced_No"));
					selectRoofCovering(data.get("roofCovering"));
					selectOpeningProtection(data);
					break;
				}
			}				
			if(state.equals("TX")){
				String windStromVal = getText(LocationCoveragePage.windStormHailDed,"windStormHailDed");
				if(windStromVal.equals("Ex-wind") && data.get("residenceType").equals("Homeowner")){
					selectOpeningProtection(data);
					selectRoofCoveringCredit(data.get("roofCoveringCredit"));
					selectRoofCoveringMaterial(data.get("roofCoveringMaterial"));					
				}else if(!windStromVal.equals("Ex-wind")){
					selectOpeningProtection(data);
					selectRoofCoveringCredit(data.get("roofCoveringCredit"));
					selectRoofCoveringMaterial(data.get("roofCoveringMaterial"));					
				}

			}
		}
	}
	private void selectWallConstructionType(String value) throws Throwable{
		if(value != null && value != "")
			selectByVisibleText(LocationCoveragePage.wallConstructionType, value, "WallConstructionType");
	}

	private void selectHurricaneFortification(String value) throws Throwable{
		if(value != null && value != "")
			selectByVisibleText(LocationCoveragePage.hurricaneFortification, value, "HurricaneFortification");
	}

	private void selectExteriorWallsCovering(String value) throws Throwable{
		if(value != null && value != "")
			selectByVisibleText(LocationCoveragePage.exteriorWallsCovering, value, "Exterior Walls Covering");
	}

	private void selectSupportedHome(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.supportedHomeYes, "Supported Home - Yes");
			}else{
				click(LocationCoveragePage.supportedHomeNo, "Supported Home - No");
			}
		}
	}

	private void selectRetrofittedForEarthQuake(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.retrofittedForEarthQuakeYES, "retrofittedForEarthQuake - Yes");
			}else if(value.equalsIgnoreCase("NO")){
				click(LocationCoveragePage.retrofittedForEarthQuakeNO, "retrofittedForEarthQuake - No");
			}
		}
	}

	private void selectHomeBeachFront(Hashtable<String, String>data) throws Throwable{		
		String value = data.get("homeBeachFront");	
		if( !"Condo/Co-op".equalsIgnoreCase(data.get("residenceType")) &&
				value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.homeBeachFrontYes, "Home Beach Front - Yes");
			}else{
				click(LocationCoveragePage.homeBeachFrontNo, "Home Beach Front - No");
			}
		}
	}		


	private void setRiskLocatedFloor(String value) throws Throwable{
		if(value!= null && !value.equalsIgnoreCase("null") && value != ""){
			if(isVisibleOnly(LocationCoveragePage.riskLocatedFloor, "Risk Located Floor"))
				type(LocationCoveragePage.riskLocatedFloor, value, "Risk Located Floor");
		}
	}

	private void selectDistanceToBeach(String distanceValue) throws Throwable{
		if(distanceValue != null && distanceValue != "" && distanceValue != "N/A"){
			try {
				if(driver.findElement(LocationCoveragePage.distanceToBeach).isEnabled()){
					selectByVisibleText(LocationCoveragePage.distanceToBeach, distanceValue, "distanceToBeach");
				}else if(isEnabled(LocationCoveragePage.managerDTCOverride, "managerDTCOverride")){
					click(LocationCoveragePage.managerDTCOverride,"managerDTCOverride");
					selectByVisibleText(LocationCoveragePage.distanceToCoastOverride, distanceValue, "distanceToCoast override");
				}
			} catch (Exception e) {
			}
		}
	}

	public void selectExcessFlood(Hashtable<String, String> data) throws Throwable{
		if(isVisibleOnly(LocationCoveragePage.excessFloodYes, "Excess Flood")){
			if(data.get("excessFlood") != null && data.get("excessFlood") != "" && !data.get("excessFlood").equalsIgnoreCase("NULL")){
				if(data.get("excessFlood").equalsIgnoreCase("YES")){
					click(LocationCoveragePage.excessFloodYes, "Excess Flood - Yes");
					selectByVisibleText(LocationCoveragePage.dwelling, data.get("excessFloodDwellingLimits"), "ExcessFloodDwellingLimits");
					selectByVisibleText(LocationCoveragePage.contents, data.get("excessFloodContentsLimits"), "ExcessFloodContentsLimits");
				}else{
					click(LocationCoveragePage.excessFloodNo, "Excess Flood - No");
				}
			}
		}
	}

	private void select24HourSignalContinuity(String signal24Hour) throws Throwable{
		if(signal24Hour != null && !signal24Hour.equalsIgnoreCase("")){
			if(signal24Hour.equalsIgnoreCase("Yes")){
				click(LocationCoveragePage.signalContinuity24HourYes,"signalContinuity24Hour - Yes");
			}else if(signal24Hour.equalsIgnoreCase("No")){
				click(LocationCoveragePage.signalContinuity24HourNo,"signalContinuity24Hour - No");
			}
		}
	}

	private void selectCBRAZone(String value) throws Throwable{
		if(value!= null && value!= ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.CBRAZoneYes, "CBRA Zone - Yes");
			}else{
				click(LocationCoveragePage.CBRAZoneNo, "CBRA Zone - No");
			}
		}
	}
	public void selectGatedCommunityPatrol(String value) throws Throwable{
		if(value!= null && value!= ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.gatedCommunityPatrolYes, "gatedCommunityPatrol - Yes");
			}else{
				click(LocationCoveragePage.gatedCommunityPatrolNo, "gatedCommunityPatrol - No");
			}
		}
	}

	/*public void fillConstructionDetails(Hashtable<String, String> data) throws Throwable {
					setYearBuilt(data.get("yearBuilt"));
					selectProtectionClass(data.get("protectionClass"));
					setSquareFoot(data.get("squareFoot"));
					selectConstructionType(data.get("constructionType"));
					setNoOfFloors(data.get("noOfFloors"));
					switch(data.get("state")){
					case "NC":
					case "VA":
					case "TX":
						selectHomeBeachFront(data.get("homeBeachFront"));break;

					}
				}*/
	public void fillConstructionDetails(Hashtable<String, String> data) throws Throwable {
		switch(data.get("state")){
		case "NJ":
		case "RI":
		case "VA":
			setRenovationYear(data.get("yearRenovated"));
			selectDistanceToBeach(data.get("Distance_To_Coast"));
		case "NC":
		case "TX":		
		case "CT":
		case "MA":
		case "AL":				
		case "MD":
		case "MS":
		case "HI":
		case "NY":
			setYearBuilt(data.get("yearBuilt"));
			setSquareFoot(data.get("squareFoot"));
			selectHomeBeachFront(data);
			break;
		case "AR":
			setYearBuilt(data.get("yearBuilt"));
			setRenovationYear(data.get("yearRenovated"));
			setSquareFoot(data.get("squareFoot"));
			selectLeedCertifiedHome(data);
			break;
		case "WA":homeRatingcharsPrefill(data.get("homeRatingcharsPrefill"));
		case "IL":
		case "AZ":
		case "KS":
		case "VT":
		case "IA":
		case "WV":
		case "SD":
		case "NV":
		case "MT":
		case "TN":
		case "WI":
		case "OH":
		case "KY":
		case "IN":
		case "UT":
		case "OR":
		case "ND":
		case "NM":
		case "NE":
		case "NH":
		case "CO":
		case "WY":
		case "OK":
		case "PA":
		case "MI":
		case "MN":
		case "DC":
		case "MO":
			setYearBuilt(data.get("yearBuilt"));
			setRenovationYear(data.get("yearRenovated"));
			setSquareFoot(data.get("squareFoot"));break;
		case "FL":
			yearBuilt(data.get("yearBuilt"));
			selectBCEG(data.get("BCEG"));
			setSquareFootFL(data.get("squareFoot"));break;
		case "DE":
			setRenovationYear(data.get("yearRenovated"));
			selectDistanceToBeach(data.get("Distance_To_Coast"));
		case "LA":
		case "GA":
			setYearBuilt(data.get("yearBuilt"));
			setSquareFoot(data.get("squareFoot"));
			selectHomeBeachFront(data);
			selectLeedCertifiedHome(data);break;
		case "AK":
			setYearBuilt(data.get("yearBuilt"));
			setSquareFoot(data.get("squareFoot"));
			selectLeedCertifiedHome(data);
			selectCertifiedInspection(data.get("certifiedInspection"));break;
		case "ME":
			setYearBuilt(data.get("yearBuilt"));
			setSquareFoot(data.get("squareFoot"));
			selectHomeBeachFront(data);
			selectHandcraftedLogHome(data.get("handCraftedLogHome"));break;
		case "SC":
			setYearBuiltID(data.get("yearBuilt"));
			setSquareFoot(data.get("squareFoot"));
			selectBCEG(data.get("BCEG"));break;
		case "CA":
			setYearBuilt(data.get("yearBuilt"));
			setSquareFoot(data.get("squareFoot"));
			selectExteriorWallsCovering(data.get("exteriorWallsCovering"));
			selectSupportedHome(data.get("supportedHome"));
			selectRetrofittedForEarthQuake(data.get("retrofittedForEarthQuake"));
			break;
		}
		selectConstructionType(data.get("constructionType"));
		selectProtectionClass(data.get("protectionClass"));
		setNoOfFloors(data.get("noOfFloors"));
		setRiskLocatedFloor(data.get("riskLocatedFloor"));

	}

	public void selectFloodInfo(Hashtable<String, String> data) throws Throwable {
		//selectFloodZone(data.get("floodZone"));
		selectPureNFIP(data.get("pureNFIP"));
		if(data.get("pureNFIP").equalsIgnoreCase("No"))selectNFIPMaxLimits(data.get("NFIP_Max_Limit"));
		switch(data.get("state")){
		case "NC":
		case "AR":
		case "IA":
		case "WV":
		case "SD":
		case "UT":
		case "OR":
		case "NM":
		case "DE":
		case "CO":
			selectExcessFlood(data);break;
		case "FL":
			selectFloodZone(data.get("floodZone"));
			selectCBRAZone(data.get("CBRAZone"));
			selectExcessFlood(data);break;
		case "SC":
		case "RI":
		case "IL":
		case "PA":
			selectFloodZone(data.get("floodZone"));
		}
	}

	public void overrideUwTax(Hashtable<String, String> data) throws Throwable {
		switch(data.get("state")){
		case "KY":
			/*setCityCode(data.get("cityCode"));
						setCityName(data.get("cityName"));
						setCountyName(data.get("countyName"));*/
			//setUwTax(data.get("uwTaxValue"));
			click(CommonPage.muncipalTaxReportButton, "Muncipal Tax Report");		
		}

	}
	public void setUwTax(String value) throws Throwable{
		List<WebElement> list = driver.findElements(LocationCoveragePage.uwTaxOverride);
		for (WebElement elem : list) {
			type(LocationCoveragePage.uwTaxOverride, value, "UW Tax details");
		}
	}
	void selectOutOfResidence(String value) throws Throwable{
		if(value != null && value != "")
			selectByVisibleText(LocationCoveragePage.outOfResidence, value, "Construction_Member_Out_Of_Residence_Duration");
	}
	void setCostOfPlannedWork(String value) throws Throwable{
		if(value != null && value != "")
			type(LocationCoveragePage.costOfPlannedWork, value, "Cost of Planned Work");
	}
	void setYearRoofReplaced(String value) throws Throwable{
		if(value != null && !value.equalsIgnoreCase("NULL"))
			type(LocationCoveragePage.yearRoofReplaced, value, "Year Roof Replaced");
	}
	public void fillLocationCoverageInfo(Hashtable<String, String> data) throws Throwable {
		overrideUwTax(data);
		selectPrimaryResidence(data.get("isPrimaryResidence"));
		//selectSecondaryResidence(data.get("isSecondaryResidence"));
		//selectRentedResidence(data.get("isRentedResidence"));
		selectVacantResidence(data.get("isVacantResidence"));
		selectMajorRenovation(data.get("isMajorRenovation"));

		//Risk_Ground_Up_Construction_Ind=isMajorRenovation
		if(data.get("isMajorRenovation").equalsIgnoreCase("YES")){
			selectOutOfResidence(data.get("Construction_Member_Out_Of_Residence_Duration_Name"));
			setCostOfPlannedWork(data.get("Risk_Cost_Of_Planned_Work_Amt"));
		}
		//selectForSale(data.get("isForSale"));
		switch(data.get("state")){
		case "FL":
			selectMortgageeInfo(data);
			selectUnOccupiedTwoMonths(data.get("unOccupiedTwoMonths"));
			if(data.get("effectiveDate")!="" && ("7/1/2018").equalsIgnoreCase(data.get("effectiveDate"))){
				selectRentedForTenWeeks(data.get("isRentedResidence"));
			}else{//effective date is current
				selectRentedResidence(data.get("isRentedResidence"));
			}
			break;
		case "SC":
			selectSeasonalHome(data.get("seasonalHome"));
			selectRentedForTenWeeks(data.get("isRentedResidence"));break;
		case "CA":
			selectMortgageeInfo(data);
			selectGroundUpConstruction(data.get("groundUpConstruction"));
		case "IL":
		case "NJ":
		case "PA":
			selectForSale("No");
		case "NC":
			selectMortgageeInfo(data);
		default:
			selectMortgageeInfo(data);
			selectSecondaryResidence(data.get("isSecondaryResidence"));
			if(data.get("isSecondaryResidence").equalsIgnoreCase("YES"))
				selectPureInsurePrimaryHome(data.get("pureInsurePrimaryHome"));
			selectForSale(data.get("isForSale"));
			selectRentedResidence(data.get("isRentedResidence"));break;
		}
	}

	private void selectPureInsurePrimaryHome(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.pureInsurePrimaryHomeYes, "Pure Insure Primary Home - Yes");
			}else{
				click(LocationCoveragePage.pureInsurePrimaryHomeNo, "Pure Insure Primary Home - No");
			}
		}
	}

	public void selectStandardizedAddress() throws Throwable{
		String value = getAttributeByClass(LocationCoveragePage.standardizedAddress, "Standardized Address");
		if(value.indexOf("unFilledMandatoryField") > -1){
			int index = driver.findElement(LocationCoveragePage.standardizedAddress).findElements(By.tagName("option")).size();
			selectByIndex(LocationCoveragePage.standardizedAddress, index-1, "Standardized Address");
			waitUntilJQueryReady();
		}		
	}
	private void selectSeasonalHome(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.seasonalHomeYes, "Seasonal Home Radio - Yes");
			}else{
				click(LocationCoveragePage.seasonalHomeNo, "Seasonal Home Radio- No");
			}
		}
	}

	private void selectUnOccupiedTwoMonths(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.unOccupiedTwoMonthsYes, "UnOccupiedTwoMonths Radio - Yes");
			}else{
				click(LocationCoveragePage.unOccupiedTwoMonthsNo, "unOccupiedTwoMonths Radio- No");
			}
		}
	}
	private void selectRentedForTenWeeks(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.rentedForTenWeeksYes, "RentedForTenWeeksYes Radio - Yes");
			}else if(value.equalsIgnoreCase("No")){
				click(LocationCoveragePage.rentedForTenWeeksNo, "RentedForTenWeeksNo Radio- No");
			}
		}
	}
	private void yearBuilt(String year) throws Throwable {
		if(year != null && year != ""){
			type(LocationCoveragePage.yearBuilt, year, "Year built type");
			type(LocationCoveragePage.yearBuilt, Keys.TAB, "Tab key");
		}
	}
	private void setSquareFootFL(String squareFootageFL) throws Throwable {
		if(squareFootageFL != null && squareFootageFL != "")
			type(LocationCoveragePage.squareFootageFL, squareFootageFL, "Square Footage type");
	}
	private void selectBCEG(String value) throws Throwable{
		if(value != null && value != ""){
			if(isVisibleOnly(LocationCoveragePage.selectBCEG, "BCEG")){
				if(optionAvailable(LocationCoveragePage.selectBCEG, value)){
					selectByVisibleText(LocationCoveragePage.selectBCEG, value, "BCEG");
				}else if(isVisibleOnly(LocationCoveragePage.selectBCEG, "BCEG")){
					selectByIndex(LocationCoveragePage.selectBCEG, 1, "BCEG");
				}
			}
		}
	}
	private void selectRoofDeck(String value) throws Throwable{
		if(value != null && value != "")
			selectByVisibleText(LocationCoveragePage.selectRoofDeck, value, "Roof Deck");
	}
	private void selectRoofDeckAttachment(String value) throws Throwable{
		if(value != null && value != "")
			selectByVisibleText(LocationCoveragePage.selectRoofDeckAttachment, value, "Roof Deck");
	}	
	private void selectWaterResist(String value) throws Throwable{
		if(value != null && value != "")
			selectByVisibleText(LocationCoveragePage.selectSecondWaterResist, value, "Secondary Water Resist");
	}
	private void selectRoofWall(String value) throws Throwable{
		if(value != null && value != "")
			selectByVisibleText(LocationCoveragePage.selectRoofWall, value, "Roof Wall Resist");
	}
	public void fillAdditionalProtectionInfo(Hashtable<String, String> data) throws Throwable {
		selectLowTempMonitor(data.get("lowTempMonitoringSystem"));
		//selectFirewiseCommunity(data.get("firewiseCommunity")); included for selected states
		selectExternalPerimeterGate(data.get("extPerimeterGate"));
		selectFulltimeCareTaker(data.get("fullTimeCareTaker"));
		selectGasLeakDetector(data.get("gasLeakDetector"));
		selectGuardGatedCommunity(data.get("guardGatedCommunity"));
		//selectPerimeterSecurityProtection(data.get("perimeterSecProtection"));
		selectLightningProtectionSystem(data.get("lightningProtectionSystem"));
		selectPermanentInstGenerators(data.get("PermanentInstGenerators"));
		/*need more details on verifying county and territory
		 *verifyCounty(data.get("county"));
					verifyTerritory(data.get("territory"));*/	
	}
	public void fillProtectionInfo(Hashtable<String, String> data) throws Throwable {
		selectFireAlarm(data.get("fireAlarm"));
		selectBurglarAlarm(data.get("burglarAlarm"));
		selectWaterLeakageSystem(data.get("waterLeakageSystem"));		
		//		From here
		/*selectLowTempMonitor(data.get("lowTempMonitoringSystem"));
					selectFirewiseCommunity(data.get("firewiseCommunity"));
					selectExternalPerimeterGate(data.get("extPerimeterGate"));
					selectFulltimeCareTaker(data.get("fullTimeCareTaker"));
					selectGasLeakDetector(data.get("gasLeakDetector"));
					selectGuardGatedCommunity(data.get("guardGatedCommunity"));
					//		selectPerimeterSecurityProtection(data.get("perimeterSecProtection"));
					selectLightningProtectionSystem(data.get("lightningProtectionSystem"));
					selectPermanentInstGenerators(data.get("PermanentInstGenerators"));*/
		//		Till here
		if(("Homeowner").equalsIgnoreCase(data.get("residenceType"))){
			switch(data.get("state")){
			case "WA":
				selectAutomaticSeismicValue(data.get("automaticSeismic"));break;
			case "ME":
				selectStormShutter(data.get("stormShutter"));break;
			case "CO":
				selectFirewiseCommunity(data.get("firewiseCommunity")); break;
			case "CA":
				selectAutomaticSeismicValue(data.get("automaticSeismic"));
				selectGatedCommunityPatrol(data.get("gatedCommunityPatrol"));
			case "FL":
			case "NY":
			case "TX":
			case "MO":
			case "WY":
			case "DE":
			case "OR":
			case "RI":
			case "PA":
			case "IL":
			case "AR":
			case "NJ":
				selectSprinklerSystemWithWaterflow(data.get("sprinklerSystemWithWaterflow"));
				selectResidentialSprinklerSystem(data.get("residentialSprinklerSystem"));
				selectExternalPerimeterGate(data.get("extPerimeterGate"));
			case "VA":
				select24HourSignalContinuity(data.get("24_Hour_Signal_Continuity_Ind"));
				selectLowTempMonitor(data.get("lowTempMonitoringSystem"));						
				selectFulltimeCareTaker(data.get("fullTimeCareTaker"));
				selectGasLeakDetector(data.get("gasLeakDetector"));
				selectGuardGatedCommunity(data.get("guardGatedCommunity"));
				selectLightningProtectionSystem(data.get("lightningProtectionSystem"));
				selectPermanentInstGenerators(data.get("PermanentInstGenerators"));
				selectPerimeterSecurityProtection(data.get("perimeterSecProtection"));break;			
			}
		}else if(("Condo/Co-op").equalsIgnoreCase(data.get("residenceType")) || ("Tenants").equalsIgnoreCase(data.get("residenceType"))){
			switch(data.get("state")){
			case "WY":
			case "DE":
			case "OR":
				select24HourDoorMan(data);
			case "AR":
				selectResidentialSprinklerSystem(data.get("residentialSprinklerSystem"));
				selectGuardGatedCommunity(data.get("guardGatedCommunity"));
				selectFulltimeCareTaker(data.get("fullTimeCareTaker"));
				break;
			case "CO":
				selectFirewiseCommunity(data.get("firewiseCommunity")); break;
			}
		}else if(("Tenants").equalsIgnoreCase(data.get("residenceType"))){
			switch(data.get("state")){
			case "OR":
			case "WY":
				select24HourDoorMan(data);
				selectResidentialSprinklerSystem(data.get("residentialSprinklerSystem"));
				selectGuardGatedCommunity(data.get("guardGatedCommunity"));
				selectFulltimeCareTaker(data.get("fullTimeCareTaker"));
				break;
			case "CO":
				selectFirewiseCommunity(data.get("firewiseCommunity")); break;
			}
		}

		/*verifyCounty(data.get("county"));
					verifyTerritory(data.get("territory"));*/	
	}
	private void selectSprinklerSystemWithWaterflow(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES"))
				click(LocationCoveragePage.sprinklerSystemWithWaterflowYes, "SprinklerSystemWithWaterflow Radio - Yes");
			else
				click(LocationCoveragePage.sprinklerSystemWithWaterflowNo, "SprinklerSystemWithWaterflow Radio- No");
		}
	}
	private void selectResidentialSprinklerSystem(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES"))
				click(LocationCoveragePage.residentialSprinklerSystemYes, "ResidentialSprinklerSystem Radio - Yes");
			else
				click(LocationCoveragePage.residentialSprinklerSystemNo, "ResidentialSprinklerSystem Radio- No");
		}
	}
	private void selectFirewiseCommunity(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES"))
				click(LocationCoveragePage.firewiseCommunityYes, "FirewiseCommunity Radio - Yes");
			else
				click(LocationCoveragePage.firewiseCommunityNo, "FirewiseCommunity Radio- No");
		}
	}
	private void selectPerimeterSecurityProtection(String value) throws Throwable{
		if(value != null && value != "")
			selectByVisibleText(LocationCoveragePage.perimeterSecurityProtection, value, "Perimeter Security Protection");
	}
	/*Function to select Flood Zone*/
	private void selectFloodZone(String value) throws Throwable{
		try {
			if(isEnabled(LocationCoveragePage.managerFloodZoneOverRide, "managerFloodZoneOverRide")){
				click(LocationCoveragePage.managerFloodZoneOverRide,"managerFloodZoneOverRide");
				selectByVisibleText(LocationCoveragePage.floodZone, value, "Flood Zone");
			}
		} catch (Exception e) {
			//Do nothing. If floodzone is disabled, then selectByVisibleText throws error and marks case as failed
		}

	}
	/*Function to set county*/
	public void setCountyName(String countyName) throws Throwable {
		if(countyName != null && countyName != "")
			type(LocationCoveragePage.countyName, countyName, "CountyName type");
	}
	/*Function to set City Code*/
	public void setCityCode(String cityCode) throws Throwable {
		if(cityCode != null && cityCode != "")
			type(LocationCoveragePage.cityCode, cityCode, "CityCode type");
	}
	/*Function to set City Name*/
	public void setCityName(String cityName) throws Throwable {
		if(cityName != null && cityName != "")
			type(LocationCoveragePage.cityName, cityName, "CityName type");
	}
	/*Function to select Automatic Seismic Value*/
	private void selectAutomaticSeismicValue(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES"))
				click(LocationCoveragePage.automaticSeismicYes, "AutomaticSeismic- Yes");
			else
				click(LocationCoveragePage.automaticSeismicNo, "AutomaticSeismic - No");
		}
	}
	/*Function to seelect Leed Certified Home*/
	private void selectLeedCertifiedHome(Hashtable<String, String>data) throws Throwable{
		String value = data.get("leedCertifiedHome");
		if( !"Condo/Co-op".equalsIgnoreCase(data.get("residenceType")) &&
				value != null && value != ""){
			if(value.equalsIgnoreCase("YES"))
				click(LocationCoveragePage.leedCertifiedHomeYes, "Leed Certified Home - Yes");
			else
				click(LocationCoveragePage.leedCertifiedHomeNo, "Leed Certified Home - No");
		}
	}
	/*Function to seelect Certified Inspection*/
	private void selectCertifiedInspection(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.certifiedInspectionYes, "Certified Inspection- Yes");
			}else{
				click(LocationCoveragePage.certifiedInspectionNo, "Certified Inspection- No");
			}
		}
	}
	/*Function to seelect Storm Shutter*/
	private void selectStormShutter(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.stormShuttersYes, "Storm Shutters- Yes");
			}else{
				click(LocationCoveragePage.stormShuttersNo, "Storm Shutters- No");
			}
		}
	}
	/*Function to seelect HandcraftedLog Home*/
	private void selectHandcraftedLogHome(String value) throws Throwable{
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.handcraftedLogHomeYes, "HandcraftedLogHomeYes- Yes");
			}else{
				click(LocationCoveragePage.handcraftedLogHomeNo, "HandcraftedLogHome- No");
			}
		}
	}
	/*Function to select Lightning Protection System*/
	private void selectLightningProtectionSystem(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.lightningProtectionYes, "Lightning ProtectionYes - Yes");
			}else{
				click(LocationCoveragePage.lightningProtectionNo, "Lightning ProtectionNo - No");
			}
		}
	}
	/*Function to select External perimeter Gate*/
	private void selectExternalPerimeterGate(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.externalPerimeterGateYes, "External PerimeterGateYes - Yes");
			}else{
				click(LocationCoveragePage.externalPerimeterGateNo, "External PerimeterGateNo- No");
			}
		}
	}
	/*Function to select Full Time care taker*/
	private void selectFulltimeCareTaker(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.fulltimeCaretakerYes, "FulltimeCaretakerYes - Yes");
			}else{
				click(LocationCoveragePage.fulltimeCaretakerNo, "FulltimeCaretakerNo- No");
			}
		}
	}

	/*Function to select Gas Leak Detector*/
	private void selectGasLeakDetector(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.gasLeakDetectorYes, "GasLeakDetectorYes - Yes");
			}else{
				click(LocationCoveragePage.gasLeakDetectorNo, "GasLeakDetectorNo- No");
			}
		}		
	}
	/*Function to select Low Temperature Monitor*/
	private void selectLowTempMonitor(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.lowTempMonitoringSysYes, "LowTempMonitorYes - Yes");
			}else{
				click(LocationCoveragePage.lowTempMonitoringSysNo, "LowTempMonitorNo- No");
			}
		}
	}
	/*Function to select 24 Hour Door Man*/
	private void select24HourDoorMan(Hashtable<String, String> data) throws Throwable {
		if(data.get("24HourDoorMan") != null && data.get("24HourDoorMan") != ""){
			if(data.get("24HourDoorMan").equalsIgnoreCase("YES")){
				click(LocationCoveragePage.doorMan24HourYes, "24HourDoorMan- Yes");
				if(data.get("surveillanceCamera").equalsIgnoreCase("YES")){
					click(LocationCoveragePage.surveillanceCameraYes, "surveillanceCamera- Yes");
				}else{
					click(LocationCoveragePage.surveillanceCameraNo, "surveillanceCamera- No");
				}
				if(data.get("lockedOrMannedElevator").equalsIgnoreCase("YES")){
					click(LocationCoveragePage.lockedOrMannedElevatorYes, "lockedOrMannedElevator- Yes");
				}else{
					click(LocationCoveragePage.lockedOrMannedElevatorNo, "lockedOrMannedElevator- No");
				}
			}else{
				click(LocationCoveragePage.doorMan24HourNo, "24HourDoorMan- No");
			}
		}
	}

	/*Function to select guard gated community*/
	private void selectGuardGatedCommunity(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.guardGatedCommunityYes, "guardGatedCommunity- Yes");
			}else{
				click(LocationCoveragePage.guardGatedCommunityNo, "guardGatedCommunity- No");
			}
		}
	}

	/*Function to get County from label*/
	public String getCounty() throws Throwable{
		String county=null;
		if(isVisibleOnly(LocationCoveragePage.countyLabel, "County")){
			county = getAttributeValue(LocationCoveragePage.countyLabel, "innerText");
		}
		return county;
	}
	/*Function to get Territory from label*/
	public String getTerritory() throws Throwable{
		String territory=null;
		if(isVisibleOnly(LocationCoveragePage.territoryLabel, "Territory")){
			territory = getAttributeValue(LocationCoveragePage.territoryLabel, "innerText");
		}
		return territory;
	}
	/*Function to verify County is generated and as expected*/
	public boolean verifyCounty(String value) throws Throwable{
		if(getCounty()!=null){
			return assertTextStringMatching(getCounty(),value);
		}
		return false;
	}
	/*Function to verify Territory is generated and as expected*/
	public boolean verifyTerritory(String value) throws Throwable{
		if(getTerritory()!=null){
			return assertTextStringMatching(getTerritory(),value);
		}
		return false;
	}
	/*Function to select Permanent Instant Generators*/
	private void selectPermanentInstGenerators(String value) throws Throwable {
		if(value != null && value != ""){
			if(value.equalsIgnoreCase("YES")){
				click(LocationCoveragePage.permanentInstGeneratorYes, "Permanent Instant Generators- Yes");
			}else{
				click(LocationCoveragePage.permanentInstGeneratorNo, "Permanent Instant Generators- No");
			}
		}
	}

	//elevation certificate
	private void setBaseFloodElevation(String value) throws Throwable {
		if(value!=null && !value.equalsIgnoreCase("null")&& value != ""){
			clearData(LocationCoveragePage.baseFloodElevation);
			type(LocationCoveragePage.baseFloodElevation, value, "Base Flood Elevation");
			type(LocationCoveragePage.baseFloodElevation, Keys.TAB, "TAB Key");
			waitUntilJQueryReady();
		}
	}
	private void selectBuildingDiagramNumber(String value) throws Throwable {
		if(value != null && value != ""){
			selectByVisibleText(LocationCoveragePage.buildingDiagramNumber, value, "Building Diagram Number");
			waitUntilJQueryReady();
		}
	}
	private void setTopOfBottomFloor(String value) throws Throwable {
		if(value != null && value != ""){
			type(LocationCoveragePage.topOfBottomFloor, value, "TopOfBottomFloor");
			type(LocationCoveragePage.topOfBottomFloor, Keys.TAB, "TAB Key");
			waitUntilJQueryReady();
		}
	}
	private void setTopOfNextFloor(String value) throws Throwable {
		if(value != null && value != "")
			type(LocationCoveragePage.topOfNextFloor, value, "TopOfNextFloor");
	}
	private void setBottomOfLowest(String value) throws Throwable {
		if(value != null && value != "")
			type(LocationCoveragePage.bottomOfLowest, value, "BottomOfLowestHorizontalStructuralMember");
	}
	private void setBottomOfAttachedGarage(String value) throws Throwable {
		if(value != null && value != ""){
			type(LocationCoveragePage.bottomOfAttachedGarage, value, "BottomOfAttachedGarage");
			type(LocationCoveragePage.bottomOfAttachedGarage, Keys.TAB, "TAB Key");
			waitUntilJQueryReady();
		}
	}
	private void setLowestElevationOfMachinery(String value) throws Throwable {
		if(value != null && value != ""){
			type(LocationCoveragePage.lowestElevationOfMachinery, value, "LowestElevationOfMachinery");
			type(LocationCoveragePage.lowestElevationOfMachinery, Keys.TAB, "TAB Key");
			waitUntilJQueryReady();
		}
	}
	private void setTotalSquareFeet(String value) throws Throwable {
		if(value != null && value != "")
			type(LocationCoveragePage.totalSquareFeet, value, "TotalSquareFeet");
	}
	private void setPermanentFloodOpenings(String value) throws Throwable {
		if(value != null && value != "")
			type(LocationCoveragePage.permanentFloodOpenings, value, "PermanentFloodOpenings");
	}
	private void setTotalAreaOfPermanentOpenings(String value) throws Throwable {
		if(value != null && value != "")
			type(LocationCoveragePage.totalAreaOfPermanentOpenings, value, "TotalAreaOfPermanentOpenings");
	}
	public void fillElevationCertificateDetails(Hashtable<String, String> data) throws Throwable{
		if(isVisibleOnly(LocationCoveragePage.baseFloodElevation, "BaseFloodElevation")){
			switch(data.get("state")){
			case "DE":
			case "FL":
				setBottomOfLowest(data.get("bottomOfLowest"));
				setLowestElevationOfMachinery(data.get("lowestElevationOfMachinery"));
			case "NJ":
			case "SC":
				setBaseFloodElevation(data.get("baseFloodElevation"));
				selectBuildingDiagramNumber(data.get("buildingDiagramNumber"));
				setTopOfBottomFloor(data.get("topOfBottomFloor"));
				setTopOfNextFloor(data.get("topOfNextFloor"));
				setBottomOfAttachedGarage(data.get("bottomOfAttachedGarage"));
				setTotalSquareFeet(data.get("totalSquareFeet"));
				setPermanentFloodOpenings(data.get("permanentFloodOpenings"));
				setTotalAreaOfPermanentOpenings(data.get("totalAreaOfPermanentOpenings"));
				break;
			}
		}
	}


}

