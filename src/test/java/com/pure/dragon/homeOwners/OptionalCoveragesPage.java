package com.pure.dragon.homeOwners;

import org.openqa.selenium.By;

public class OptionalCoveragesPage {

	static By
	earthquakeExtensionYes,
	earthquakeExtensionNo,
	earthquakeLossAssessmentYes,
	earthquakeLossAssessmentNo,
	earthquakeExtensionDeductible,
	earthquakeLossAssessmentLimit,
	sinkholeCollapseExtensionNo,
	sinkholeCollapseExtensionYes,
	lawAndOrdinanceIncreaseNo,
	lawAndOrdinanceIncreaseYes,
	lawAndOrdinanceIncreaseOption,
	floodCoverageExtensionYes,
	floodCoverageExtensionNo,
	
	homesystemProtectionYes,
	homesystemProtectionNo,
	homesystemProtectionLimit,
	homesystemProtectionDeductible,
	
	liabilityExtentionYes,
	liabilityExtentionNo,
	noOfPremises,
	
	lossAssessmentIncreaseYes,
	lossAssessmentIncreaseNo,
	lossAssessmenIncreaseLimit,
	
	floodExtensionDICNo,
	floodExtensionDICYes,
	floodAdvantageYes,
	floodAdvantageNo,
	excessFloodCoverageNo,
	excessFloodCoverageYes,
	dwelling,dwellingOther,
	contents,contentsOther,
	
	businessPropertyExtensionNo,
	businessPropertyExtensionYes,
	businessPropertyExtensionLimit,
	ensuingFungiIncreaseNo,
	ensuingFungiIncreaseYes,
	ensuingFungiIncreaseLimit,
	incidentalBusinessNo,
	incidentalBusinessYes,
	incidentalBusinessLimit,
	landscapingIncreasedLimitsNo,
	landscapingIncreasedLimitsYes,
	businessThresholdNo,
	businessThresholdYes,
	guaranteedReplacementNo,
	guaranteedReplacementYes,
	
	contentsOffPremisesNo,
	contentsOffPremisesYes,
	fineArtExclusionNo,
	fineArtExclusionYes,
	premisesLiabilityLimitationNo,
	premisesLiabilityLimitationYes,
	dwellingReplacementCostNo,
	dwellingReplacementCostYes,
	libelSlanderExclusionNo,
	libelSlanderExclusionYes,
	eliminationOfWaiverNo,
	eliminationOfWaiverYes,
	fungiLiabilityExtensionYes,
	fungiLiabilityExtensionNo,
	roofCoveringReconstructionCostYes,
	roofCoveringReconstructionCostNo,
	removalOfRequirementYes,
	removalOfRequirementNo

	;
	

	static{
		earthquakeExtensionYes = By.xpath("//*[contains(text(),'Earthquake Extension')]/..//input[@title='Yes']");
		earthquakeExtensionNo = By.xpath("//*[contains(text(),'Earthquake Extension')]/..//input[@title='No']");
		earthquakeLossAssessmentYes = By.xpath("//*[contains(text(),'Earthquake Loss Assessment Extension')]/..//input[@title='Yes']");
		earthquakeLossAssessmentNo = By.xpath("//*[contains(text(),'Earthquake Loss Assessment Extension')]/..//input[@title='Yes']");
		earthquakeExtensionDeductible = By.xpath("//*[contains(text(),'Earthquake Extension')]/ancestor::tr[1]//select[@title='Deductible']");
		earthquakeLossAssessmentLimit = By.xpath("//*[contains(text(),'Earthquake Loss Assessment Extension')]/ancestor::tr[1]//input[@type='text']");
		
		homesystemProtectionYes = By.xpath("//*[contains(text(),'Home Systems Protection')]//..//input[@title='Yes']");
		homesystemProtectionNo = By.xpath("//*[contains(text(),'Home Systems Protection')]//..//input[@title='No']");
		homesystemProtectionLimit = By.xpath("//div[contains(text(), 'Home Systems Protection')]/../../..//select[contains(@title, 'Limit')]");
		homesystemProtectionDeductible = By.xpath("//div[@title='Deductible']//..//input[@title='Deductible']");
		
		liabilityExtentionYes = By.xpath("//*[contains(text(),'Liability Extension')]//..//input[@title='Yes']");
		liabilityExtentionNo = By.xpath("//*[contains(text(),'Liability Extension')]//..//input[@title='No']");
		noOfPremises = By.xpath("//input[@title='No. of Premises']");
		
		lossAssessmentIncreaseYes = By.xpath("//*[contains(text(),'Loss Assessment Increase')]//..//input[@title='Yes']");
		lossAssessmentIncreaseNo = By.xpath("//*[contains(text(),'Loss Assessment Increase')]//..//input[@title='No']");
		lossAssessmenIncreaseLimit = By.xpath("//div[contains(text(),'Loss Assessment Increase')]/../../..//input[@type='text']");
		
		//optional flood coverage
		floodExtensionDICNo = By.xpath("//div[contains(@title, 'Flood Extension')]/..//input[@title='No']");
		floodExtensionDICYes = By.xpath("//div[contains(@title, 'Flood Extension')]/..//input[@title='Yes']");
		floodAdvantageYes = By.xpath("//div[contains(text(), 'Flood Advantage')]/..//input[@title='Yes']");
		floodAdvantageNo = By.xpath("//div[contains(text(), 'Flood Advantage')]/..//input[@title='No']");
//		excessFloodCoverageNo = By.xpath("//div[contains(@title, 'Excess Flood Coverage')]/..//input[@title='No']");
//		excessFloodCoverageYes = By.xpath("//div[contains(@title, 'Excess Flood Coverage')]/..//input[@title='Yes']");
		excessFloodCoverageNo = By.xpath("//div[contains(text(), 'Excess Flood Coverage')]/..//input[@title='No']");
		excessFloodCoverageYes = By.xpath("//div[contains(text(), 'Excess Flood Coverage')]/..//input[@title='Yes']");

		dwelling = By.xpath("//select[contains(@title, 'Dwelling')]");
		contents = By.xpath("//select[contains(@title, 'Contents')]");		
		dwellingOther = By.xpath("//input[contains(@title, 'Dwelling Limit for Excess Flood')]");
		contentsOther = By.xpath("//input[contains(@title, 'Contents Limit for Excess Flood')]");
		
		//Optional Coverage
		businessPropertyExtensionNo = By.xpath("//div[contains(@title, 'Business Property')]/..//input[@title='No']");
		businessPropertyExtensionYes = By.xpath("//div[contains(@title, 'Business Property')]/..//input[@title='Yes']");
		businessPropertyExtensionLimit = By.xpath("//div[contains(text(), 'Business Property')]/../../..//input[@title='Limit']");
		ensuingFungiIncreaseNo = By.xpath("//div[contains(text(), 'Ensuing Fungi Increase')]/..//input[@title='No']");
		ensuingFungiIncreaseYes = By.xpath("//div[contains(text(), 'Ensuing Fungi Increase')]/..//input[@title='Yes']");
		ensuingFungiIncreaseLimit = By.xpath("//div[contains(text(), 'Ensuing Fungi Increase')]/../../..//select");
		incidentalBusinessNo = By.xpath("//div[contains(@title, 'Incidental Business')]/..//input[@title='No']");
		incidentalBusinessYes = By.xpath("//div[contains(@title, 'Incidental Business')]/..//input[@title='Yes']");
		incidentalBusinessLimit = By.xpath("//div[contains(text(), 'Incidental Business Prop')]/../../..//input[@title='Limit']");
		landscapingIncreasedLimitsNo = By.xpath("//div[contains(text(), 'Landscaping Increased Limits')]/..//input[@title='No']");
		landscapingIncreasedLimitsYes = By.xpath("//div[contains(text(), 'Landscaping Increased Limits')]/..//input[@title='Yes']");
		businessThresholdNo = By.xpath("//div[contains(text(), 'Business Threshold')]/..//input[@title='No']");
		businessThresholdYes = By.xpath("//div[contains(text(), 'Business Threshold')]/..//input[@title='Yes']");
		guaranteedReplacementNo = By.xpath("//div[contains(@title, 'Guaranteed Replacement Cost')]/..//input[@title='No']");
		guaranteedReplacementYes = By.xpath("//div[contains(@title, 'Guaranteed Replacement Cost')]/..//input[@title='Yes']");
		sinkholeCollapseExtensionNo = By.xpath("//div[contains(@title, 'Sinkhole Collapse Extension')]/..//input[@title='No']");
		sinkholeCollapseExtensionYes = By.xpath("//div[contains(@title, 'Sinkhole Collapse Extension')]/..//input[@title='Yes']");
		lawAndOrdinanceIncreaseNo = By.xpath("//div[contains(@title, 'Increase Limit')]/..//input[@title='No']");
		lawAndOrdinanceIncreaseYes = By.xpath("//div[contains(@title, 'Increase Limit')]/..//input[@title='Yes']");
		lawAndOrdinanceIncreaseOption = By.xpath("//div[contains(text(), 'Law and Ordinance Increase')]/../../..//select[@title='Option']");
		floodCoverageExtensionYes = By.xpath("//div[contains(@title, 'Flood Coverage Extension')]/..//input[@title='Yes']");
		floodCoverageExtensionNo = By.xpath("//div[contains(@title, 'Flood Coverage Extension')]/..//input[@title='No']");
		
		
		//Optional Coverage/Exclusions
		contentsOffPremisesNo = By.xpath("//div[contains(@title, 'Contents Off')]/..//input[@title='No']");
		contentsOffPremisesYes = By.xpath("//div[contains(@title, 'Contents Off')]/..//input[@title='Yes']");
		fineArtExclusionNo = By.xpath("//div[contains(@title, 'Fine Art Exclusion')]/..//input[@title='No']");
		fineArtExclusionYes = By.xpath("//div[contains(@title, 'Fine Art Exclusion')]/..//input[@title='Yes']");
		premisesLiabilityLimitationNo = By.xpath("//div[contains(@title, 'Premises Liability Limitation')]/..//input[@title='No']");
		premisesLiabilityLimitationYes = By.xpath("//div[contains(@title, 'Premises Liability Limitation')]/..//input[@title='Yes']");
		dwellingReplacementCostNo = By.xpath("//div[contains(@title, 'Dwelling Replacement')]/..//input[@title='No']");
		dwellingReplacementCostYes = By.xpath("//div[contains(@title, 'Dwelling Replacement')]/..//input[@title='Yes']");
		libelSlanderExclusionNo = By.xpath("//div[contains(@title, 'Libel Slander Exclusion')]/..//input[@title='No']");
		libelSlanderExclusionYes = By.xpath("//div[contains(@title, 'Libel Slander Exclusion')]/..//input[@title='Yes']");
		eliminationOfWaiverNo = By.xpath("//div[contains(@title, 'Elimination of Waiver')]/..//input[@title='No']");
		eliminationOfWaiverYes = By.xpath("//div[contains(@title, 'Elimination of Waiver')]/..//input[@title='Yes']");
		fungiLiabilityExtensionYes = By.xpath("//div[contains(@title, 'Fungi Liability Extension')]/..//input[@title='Yes']");
		fungiLiabilityExtensionNo = By.xpath("//div[contains(@title, 'Fungi Liability Extension')]/..//input[@title='No']");
		roofCoveringReconstructionCostYes = By.xpath("//div[contains(@title, 'Roof Covering Full Reconstruction Cost')]/..//input[@title='Yes']");
		roofCoveringReconstructionCostNo = By.xpath("//div[contains(@title, 'Roof Covering Full Reconstruction Cost')]/..//input[@title='No']");
		removalOfRequirementYes = By.xpath("//div[contains(@title, 'Removal of 20% of Requirement ')]/..//input[@title='Yes']");
		removalOfRequirementNo = By.xpath("//div[contains(@title, 'Removal of 20% of Requirement ')]/..//input[@title='No']");
	}
	
	
	//***********Excess Liability************
	
	static By libelSlanderExclusion, politicalActivityExclusion, premisesLiabilityLimitation, higherUnderlyingLimit,
	homeOwnersLOB, autoLOB, UIMLOB, waterCraftLOB, recreationalVehiclesLOB, increaseLimitHomeOwners,increaseLimitAuto,
	increaseLimitUIM, increaseLimitWaterCraft,increaseLimitRecreationalVehicles;
	
	
	static{
		libelSlanderExclusion = By.cssSelector("select[title='Libel/Slander Exclusion']");
		politicalActivityExclusion = By.cssSelector("select[title='Political Activity Exclusion']");
		premisesLiabilityLimitation = By.cssSelector("select[title='Premises Liability Limitation']");
		higherUnderlyingLimit = By.cssSelector("select[title='Higher Underlying Limit']");
		homeOwnersLOB = By.xpath("//div[@title='Homeowners']/..//input");
		autoLOB = By.xpath("//div[@title='Auto']/..//input");
		UIMLOB = By.xpath("//div[@title='UIM']/..//input");
		waterCraftLOB = By.xpath("//div[@title='Watercraft']/..//input");
		recreationalVehiclesLOB = By.xpath("//div[@title='Recreational Vehicles']/..//input");
		increaseLimitHomeOwners = By.cssSelector("select[title='Increase limit -Homeowners']");
		increaseLimitAuto = By.cssSelector("select[title='Increase limit - Auto']");
		increaseLimitUIM = By.cssSelector("select[title='Increase limit - UIM']");
		increaseLimitWaterCraft = By.cssSelector("select[title='Increase limit - Watercraft']");
		increaseLimitRecreationalVehicles = By.cssSelector("select[title='Increase limit - Recreational Vehicles']");
	}
}
