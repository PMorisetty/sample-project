package com.pure.dragon.homeOwners;

import org.openqa.selenium.By;

public class QuoteDetailsPage {
	
	static By
	//policy information
	auto_Companion,	collection_Companion, excess_Companion,home_companion,
	//risk address
	residenceType,address1,city,zip,
	policyType,	replacementValue,
	otherStructureValue, contentValue,
	Loss_Of_Use_Pct,allPerilDeductible,
	//liability coberage
	personalLiabilityLimit,

	//Cyber
	yesCyberDefenseCoverage,
	noCyberDefenseCoverage,
	cyberLimit,	cyberDeductible,
	moneyStolenYes,	moneyStolenNo,
	protectedByActiveSubscriptionYes,
	protectedByActiveSubscriptionNo,
	question1, question2,	
	windstormHailDeductible,
	policyNumber,noOfFamilyUnits,hurricaneDeductible,hurricaneAndNamedDeductible,hurricaneAndNamedStormDeductible,
	noOfEmployees, effectiveDate,
	customer,obtainOperatorsAndVehiclesBtn, cancelBtn,
	nonCATPropertyLossYes, nonCATPropertyLossNo, insureTwoOrMorePropertiesYes, insureTwoOrMorePropertiesNo;
	public static By headerText;
	
	//***************** Excess Laibility********
	static By  anyDeclinedCancelledNonRenewedYes,anyDeclinedCancelledNonRenewedNo,
	anyHouseholdMemberInvolvedInLitigationYes,anyHouseholdMemberInvolvedInLitigationNo,
	anyDomesticEmployeeYes,anyDomesticEmployeeNo,
	anyHouseholdMemberMadeAllegationAgainstEmployeeYes,anyHouseholdMemberMadeAllegationAgainstEmployeeNo,
	numOfVehicles ;
	
	
	public static By insuranceScore,unfilledMandatoryFields;

	public static By quoteName;

	static{
		//policy information
		auto_Companion = By.xpath("//div[contains(@title,'Auto Insurance policy')]/..//input");
		collection_Companion = By.xpath("//div[contains(@title,'Jewelry & Art policy') or contains(text(),'Jewelry & Art policy')]/..//input");
		excess_Companion = By.xpath("//div[contains(@title,'Personal Excess policy') or contains(text(),'Personal Excess policy') or contains(text(),'Personal Excess Liability policy')]/..//input");
		home_companion = By.xpath("//div[@title='Does the Member have a PURE Homeowners policy?']/..//input");
		//insurance score section
		insuranceScore = By.xpath("//th[contains(text(),'Applicant Insurance Score')]/../..//td[2]//label");

		//risk address
		residenceType = By.cssSelector("select[title='Residence Type']");
		policyType = By.cssSelector("select[title='Policy Type']");
		replacementValue = By.cssSelector("input[title='Replacement Value']");
		otherStructureValue = By.cssSelector("input[title='Other Structures Limit']");
		contentValue = By.cssSelector("input[title='Contents Limit']");
		Loss_Of_Use_Pct = By.cssSelector("select[title='Location - Loss of Use Limit']");
		allPerilDeductible = By.cssSelector("select[title='All Peril Deductible']");
		quoteName=By.xpath("//input[@title='Quote Name']");

		//Policy Information
		address1 = By.cssSelector("input[title='Street Address 1']");
		city = By.cssSelector("input[title='City']");
		zip = By.xpath("//input[starts-with(@title,'ZIP')]");

		//liability coberage
		personalLiabilityLimit = By.cssSelector("select[title='Personal Liability Limit']");

		//Cyber
		noCyberDefenseCoverage = By.xpath("//*[@title='Fraud and Cyber Defense Coverage']/..//input[@title='No']");
		yesCyberDefenseCoverage = By.xpath("//*[@title='Fraud and Cyber Defense Coverage']/..//input[@title='Yes']");
		cyberLimit = By.cssSelector("select[title='Limit']");
		cyberDeductible = By.cssSelector("select[id$='_30589733p30588833']");
		moneyStolenYes = By.xpath("//*[@title='Within the last 24 months, has the Insured or family member had money stolen or been deceived into making a payment or parting with something of value?']/..//input[@title='Yes']");
		moneyStolenNo = By.xpath("//*[@title='Within the last 24 months, has the Insured or family member had money stolen or been deceived into making a payment or parting with something of value?']/..//input[@title='No']");
		protectedByActiveSubscriptionYes = By.xpath("//*[@title='Are the Insured and family members protected by an active subscription to Rubica or Rubica-like Cyber Security Monitoring Service?']/..//input[@title='Yes']");
		protectedByActiveSubscriptionNo = By.xpath("//*[@title='Are the Insured and family members protected by an active subscription to Rubica or Rubica-like Cyber Security Monitoring Service?']/..//input[@title='No']");
		question1 = By.xpath("//div[@class='contentBlock']/*[@class='tableEnvelope']/*[@class='innerTable']//div[contains(@title, 'Within the last 24 months')]");
		question2 = By.xpath("//div[@class='contentBlock']/*[@class='tableEnvelope']/*[@class='innerTable']//div[contains(@title, 'Cyber Security Monitoring Service')]");

		windstormHailDeductible = By.cssSelector("select[id$='_27908702']");

		policyNumber = By.cssSelector("a[id$='18222733']");
		noOfFamilyUnits = By.xpath("//select[@title='Number of family units']");
		hurricaneDeductible = By.cssSelector("select[title*='Hurricane Deductible' i]");
		hurricaneAndNamedDeductible = By.cssSelector("select[id$='_27908702']");
		hurricaneAndNamedStormDeductible = By.cssSelector("select[id$='_29424109']");
		noOfEmployees = By.cssSelector("select[title='How many total full-time inside and outside employees?' i]");

		effectiveDate = By.cssSelector("input[title='Effective Date']");

		customer = By.xpath("//a[contains(text(),'Customer:')]");

		//************* PersonalAuto ****************************
		obtainOperatorsAndVehiclesBtn = By.xpath("//a[text()='obtain operators and vehicles']");
		cancelBtn = By.xpath("//a[text()='Cancel']");
		nonCATPropertyLossYes = By.xpath("//div[contains(@title,'Have you had a Non-CAT property loss in the last 3 years?')]/..//input[@title='Yes']");
		nonCATPropertyLossNo = By.xpath("//div[contains(@title,'Have you had a Non-CAT property loss in the last 3 years?')]/..//input[@title='No']");
		insureTwoOrMorePropertiesYes = By.xpath("//div[contains(@title,'Do you insure 2 or more properties with PURE?')]/..//input[@title='Yes']");
		insureTwoOrMorePropertiesNo = By.xpath("//div[contains(@title,'Do you insure 2 or more properties with PURE?')]/..//input[@title='No']");
		headerText = By.xpath("//div[@id='headerBar']/..//span[starts-with(text(),'quote')]");
		
		//***************** Excess Laibility********
		anyDeclinedCancelledNonRenewedYes = By.xpath("//div[@title = 'Have you been Declined, Canceled or Non-Renewed for any type of insurance coverage in the past 5 years?']/following-sibling::div//input[@type='radio' and @title='Yes']");
		anyDeclinedCancelledNonRenewedNo =  By.xpath("//div[@title = 'Have you been Declined, Canceled or Non-Renewed for any type of insurance coverage in the past 5 years?']/following-sibling::div//input[@type='radio' and @title='No']"); 
		anyHouseholdMemberInvolvedInLitigationYes = By.xpath("//div[contains(text(),'Household been involved in any Litigation')]/following-sibling::div//input[@title='Yes']");
		anyHouseholdMemberInvolvedInLitigationNo = By.xpath("//div[contains(text(),'Household been involved in any Litigation')]/following-sibling::div//input[@title='No']");
		anyDomesticEmployeeYes = By.xpath("//div[contains(text(),'Do you employ any Domestic Employees?')]/following-sibling::div//input[@title='Yes']");
		anyDomesticEmployeeNo  = By.xpath("//div[contains(text(),'Do you employ any Domestic Employees?')]/following-sibling::div//input[@title='No']");
		anyHouseholdMemberMadeAllegationAgainstEmployeeYes = By.xpath("//div[contains(text(),'allegation made')]/following-sibling::div//input[@title='Yes']");
		anyHouseholdMemberMadeAllegationAgainstEmployeeNo = By.xpath("//div[contains(text(),'allegation made')]/following-sibling::div//input[@title='No']");
		numOfVehicles = By.xpath("//div[contains(@title,'owned or leased')]/..//select");
		
		//****Unfilled mandatory fields
		unfilledMandatoryFields = By.xpath("//select[@class='unFilledMandatoryField']");

	}		




}
