package com.pure.dragon.homeOwners;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class MemberInformationPageLib extends ActionEngine {
	public MemberInformationPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	private void setOccupation(String occupation) throws Throwable {
		if(occupation!= null && occupation != "")
		type(MemberInformationPage.occupation, occupation, "Occupation type");
	}
	
	private void setEmployer(String employer) throws Throwable {
		if(employer!= null && employer != "")
		type(MemberInformationPage.employer, employer, "Employer type");
	}
	
	private void selectExistingAgencyClient(String value) throws Throwable {
		if(value!= null && value != ""){
		if(value.equalsIgnoreCase("YES")){
			click(MemberInformationPage.existingClientYes, "Existing Agency Client - Yes");
		}else{
			click(MemberInformationPage.existingClientNo, "Existing Agency Client - No");
		}
	}
	}
	
	private void selectRefusedInLast3Years(String value) throws Throwable {
		if(value!= null && value != ""){
		if(value.equalsIgnoreCase("YES")){
			click(MemberInformationPage.refusedInLast3YearsYes, "Refused In Last 3 Years - Yes");
		}else{
			click(MemberInformationPage.refusedInLast3YearsNo, "refused In Last 3 Years - No");
		}
	}
	}
	
	private void selectCoverageDeclined(String value) throws Throwable {
		if(value!= null && value != ""){
		if(value.equalsIgnoreCase("YES")){
			click(MemberInformationPage.coverageDeclinedYes, "Coverage declined/non-renewed - Yes");
		}else{
			click(MemberInformationPage.coverageDeclinedNo, "Coverage declined/non-renewed - No");
		}
	}
	}
	
	private void selectPolitcalFigure(String value) throws Throwable {
		if(value!= null && value != ""){
		if(value.equalsIgnoreCase("YES")){
			click(MemberInformationPage.politcalFigureYes, "Politcal Figure - Yes");
		}else{
			click(MemberInformationPage.politcalFigureNo, "Politcal Figure - No");
		}
	}
	}
	
	private void selectDangerousDogs(String value) throws Throwable {
		if(value!= null && value != ""){
		if(value.equalsIgnoreCase("YES")){
			click(MemberInformationPage.dangerousDogsYes, "Dangerous Dogs - Yes");
		}else{
			click(MemberInformationPage.dangerousDogsNo, "Dangerous Dogs - No");
		}
	}
	}
	
	private void setContactName(String contactName) throws Throwable {
		if(contactName!= null && contactName != ""){
		scroll(MemberInformationPage.contactName, "Contact Name type");
		type(MemberInformationPage.contactName, contactName, "Contact Name type");
	}
	}
	
	private void setContactEmail(String contactEmail) throws Throwable {
		if(contactEmail!= null && contactEmail != "")
		type(MemberInformationPage.contactEmail, contactEmail, "Contact Email type");
	}
	private void setContactPhone(String contactPhone) throws Throwable {
		if(contactPhone!= null && contactPhone != "")
		type(MemberInformationPage.contactPhone, contactPhone, "Contact Phone type");
	}
	public void fillMemberInfo(Hashtable<String, String> data) throws Throwable {
		//if(data.get("state").equalsIgnoreCase("IL")){this.clickMemberinfoItem();}
		setOccupation(data.get("occupation"));
		setEmployer(data.get("employer"));
		selectExistingAgencyClient(data.get("existingAgencyClient"));
		switch(data.get("state")){
		case "IL":
		case "NC":
		case "TX":
		case "AZ":
		case "VA":
		case "NV":
		case "KS":
		case "VT":
		case "IA":
		case "WV":
		case "MT":
		case "TN":
		case "SD":
		case "WI":
		case "KY":
		case "AR":
		case "OH":
		case "IN":
		case "WA":
		case "UT":
		case "OR":
		case "ND":
		case "NM":
		case "NE":
		case "DE":
		case "NH":
		case "CO":
		case "CT":
		case "NJ":
		case "WY":
		case "MA":
		case "LA":
		case "GA":
		case "CA":
		case "AL":
		case "RI":
		case "OK":
		case "PA":
		case "MD":
		case "MI":
		case "MN":
		case "HI":
		case "NY":
		case "ME":
		case "AK":
		case "DC":
			this.selectRefusedInLast3Years(data.get("refusedInLast3Years"));
			this.selectCoverageDeclined(data.get("coverageDeclined"));
			/*this.selectPolitcalFigure(data.get("politcalFigure"));*/
			this.selectDangerousDogs(data.get("dangerousDogs"));break;
		case "MO":
			/*this.selectPolitcalFigure(data.get("politcalFigure"));*/
			this.selectDangerousDogs(data.get("dangerousDogs"));break;
		default:
			selectRefusedInLast3Years(data.get("refusedInLast3Years"));
			selectCoverageDeclined(data.get("coverageDeclined"));break;
		}
		// Appraisal Info
		setContactName(data.get("contactName"));
		setContactEmail(data.get("contactEmail"));
		setContactPhone(data.get("contactPhone"));
	}
	
}
