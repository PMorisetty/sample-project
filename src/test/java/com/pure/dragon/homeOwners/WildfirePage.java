package com.pure.dragon.homeOwners;

import org.openqa.selenium.By;

public class WildfirePage {
	static By earthquakeCoverageOption;
	
	static{
		earthquakeCoverageOption = By.cssSelector("select[id$='_30029814']");
	}
}
