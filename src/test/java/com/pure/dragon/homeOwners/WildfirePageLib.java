package com.pure.dragon.homeOwners;

import java.util.Hashtable;

import com.pure.accelerators.ActionEngine;

public class WildfirePageLib extends ActionEngine{
	public void fillWildfireDetails(Hashtable<String, String> data) throws Throwable {
		switch(data.get("state")){
		case "CA":
			selectEarthquakeCoverageOption(data.get("inspectionCompName"));
			break;
		
		}		

	}
	
	private void selectEarthquakeCoverageOption(String value) throws Throwable{
		selectByVisibleText(EarthquakePage.earthquakeCoverageOption, value, "Earthquake Coverage Option");
	}
	
}
