package com.pure.dragon.homeOwners;

import org.openqa.selenium.By;

public class EarthquakePage {
	static By earthquakeCoverageOption;
	
	static{
		earthquakeCoverageOption = By.cssSelector("select[id$='_30029814']");
	}
}
