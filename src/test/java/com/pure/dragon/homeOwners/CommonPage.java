package com.pure.dragon.homeOwners;

import org.openqa.selenium.By;

public class CommonPage {
	static By nextButton;
	static By prevButton;
	static By nextRedButton;
	static By rateButton;
	static By saveChangesButton;
	static By exitButton;
	static By reviewChangesButton;
	static By muncipalTaxReportButton;
	static By inspectionCompanyName,inspectionDate;
	static By orderPropertyDetailsButton;
	static By addButton;
	static By deleteButton;
	
	static By errorMessage, errorPage,errorMessageHVIUpdatedPremiumImpacted;
	
	static{
		prevButton = By.xpath("//div[@id='headerBar']//a[contains(@href,'Action.114')]");
		nextButton = By.xpath("//div[@id='headerBar']//a[contains(@href,'Action.113')]");
		nextRedButton = By.xpath("//div[@id='headerBar']//a[contains(@href,'Action.606246')]");
		rateButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.506506')]");
		saveChangesButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.128505')]");
		exitButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.479905')]");
		reviewChangesButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.745233')]");
		muncipalTaxReportButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.572709')]");
		inspectionCompanyName = By.xpath("//input[@title='Inspection Company']");
		inspectionDate = By.xpath("//input[@title='Date of Inspection']");
		orderPropertyDetailsButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.618514')]");
		addButton = By.cssSelector("a[title='Add another item.']");
		deleteButton = By.cssSelector("a[title='Delete the selected item.']");
		errorMessage = By.xpath("//ul[@id='messages']");
		errorMessageHVIUpdatedPremiumImpacted = By.xpath("//div/ul[@id='messages']/li[contains(text(),'HVI was updated and premium may have been impacted')]");
		errorPage = By.xpath("//a[@class='redChild']");
	}
}