package com.pure.dragon.homeOwners;

import org.openqa.selenium.By;

public class MemberInformationPage {
	static By occupation;
	static By employer;
	static By existingClientYes;
	static By existingClientNo;
	static By refusedInLast3YearsYes;
	static By refusedInLast3YearsNo;
	static By coverageDeclinedYes;
	static By coverageDeclinedNo;
	static By politcalFigureYes;
	static By politcalFigureNo;
	static By dangerousDogsYes;
	static By dangerousDogsNo;
	//appraiser info
	static By contactName;
	static By contactEmail;
	static By contactPhone;
	
	static{
		occupation = By.cssSelector("input[title='Occupation']");
		employer = By.cssSelector("input[title='Employer']");
		existingClientYes = By.xpath("//*[contains(@title,'Existing Agency Client')]/..//input[@title='Yes']");
		existingClientNo = By.xpath("//*[contains(@title,'Existing Agency Client')]/..//input[@title='No']");
		refusedInLast3YearsYes = By.xpath("//*[@title='Has any company cancelled or refused to insure in the past 3 years?']/..//input[@title='Yes']");
		refusedInLast3YearsNo = By.xpath("//*[@title='Has any company cancelled or refused to insure in the past 3 years?']/..//input[@title='No']");
		coverageDeclinedYes = By.xpath("//*[@title='Has your coverage been non-renewed/Declined?']/..//input[@title='Yes']");
		coverageDeclinedNo = By.xpath("//*[@title='Has your coverage been non-renewed/Declined?']/..//input[@title='No']");
		politcalFigureYes = By.xpath("//*[contains(@title,'State political figure?')]/..//input[@title='Yes']");
		politcalFigureNo = By.xpath("//*[contains(@title,'State political figure?')]/..//input[@title='No']");
		dangerousDogsYes = By.xpath("//*[contains(@title,'Any dog that has a history of biting')]/..//input[@title='Yes']");
		dangerousDogsNo = By.xpath("//*[contains(@title,'Any dog that has a history of biting')]/..//input[@title='No']");
		
		contactName = By.cssSelector("input[title='Appraisal Contact Name']");
		contactEmail = By.cssSelector("input[title='Appraisal Contact Email']");
		contactPhone = By.cssSelector("input[title='Appraisal Contact Phone Number']");
		
	}
}
