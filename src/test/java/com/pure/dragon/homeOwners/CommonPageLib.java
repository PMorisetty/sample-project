package com.pure.dragon.homeOwners;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class CommonPageLib extends ActionEngine{
	public CommonPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void clickNextBtn() throws Throwable {
		click(CommonPage.nextButton, "Next Button");
		waitUntilJQueryReady();
	}
	public void clickAddBtn() throws Throwable {
		click(CommonPage.addButton, "Add Button");
	}
	public void clickDeletBtn() throws Throwable{
		click(CommonPage.deleteButton, "Delete Button");
	}
	public void clickNextBtnElevationCertificate(Hashtable<String, String> data) throws Throwable {
		switch (data.get("state")) {
		case "NJ":
		case "SC":
		click(CommonPage.nextButton, "Next Button");
		//Sometime above click next button is not needed, Pls find the logic & implement.
		}
	}
	public void clickNextBtnEarthquake(Hashtable<String, String> data) throws Throwable {
		switch (data.get("state")) {
		case "CA":
			click(CommonPage.nextButton, "Next Button");break;
		}
	}
	
	public void clickNextBtnResidenceEmployee(Hashtable<String, String> data) throws Throwable {
		switch (data.get("state")) {
		case "CA":
			click(CommonPage.nextButton, "Next Button");break;
		}
	}
	
	public void clickNextBtnWildfire(Hashtable<String, String> data) throws Throwable {
		switch (data.get("state")) {
		case "CA":
			click(CommonPage.nextButton, "Next Button");break;
		}
	}
	
	public void clickNextBtnConditional(Hashtable<String, String> data) throws Throwable {
		switch (data.get("state")) {
		case "CO":
			click(CommonPage.nextButton, "Next Button");break;
		}
	}
	public void clickPreviousBtn() throws Throwable {
		click(CommonPage.prevButton, "Previous Button");
	}
	public void clickNextRedBtn() throws Throwable {
		click(CommonPage.nextRedButton, "Next Red Button");
	}
	public void clickRateBtn() throws Throwable {
		click(CommonPage.rateButton, "Rate Button");
	}
	public void clickSaveChangesBtn() throws Throwable {
		click(CommonPage.saveChangesButton, "Save Changes Button");
		waitUntilJQueryReady();
		waitUntilLoadingDisappear();
	}
	public void clickExitBtn() throws Throwable {
		click(CommonPage.exitButton, "Exit Button");
	}
	public void clickOrderPropertyDetailsBtn() throws Throwable {
		if(isVisibleOnly(CommonPage.orderPropertyDetailsButton, "Order Property Details"))
			click(CommonPage.orderPropertyDetailsButton, "Order Property Details");
	}
	public void clickReviewChangesBtn() throws Throwable {
		click(CommonPage.reviewChangesButton, "Review Changes Button");
	}
	
	public void setInspectionCompanyName(String inspectionCompanyName) throws Throwable {
		if(inspectionCompanyName!=null)
		type(CommonPage.inspectionCompanyName, inspectionCompanyName, "Inspection Company Name");
	}
	public void setInspectionDate(String inspectionDate) throws Throwable {
		if(inspectionDate!=null)
		type(CommonPage.inspectionDate, inspectionDate, "Inspection Date");
	}
	public void fillInspectionDetails(Hashtable<String, String> data) throws Throwable {
		switch(data.get("state")){
		case "FL":
		case "SC":
			this.setInspectionCompanyName(data.get("inspectionCompName"));
			this.setInspectionDate(data.get("inspectionDate"));
			this.clickNextBtn();
			break;
			}

	}
	
	public void handleErrorMsg(Hashtable<String, String> data) throws Throwable
	{
		switch(data.get("state")){
		case "CA":
			if(isVisibleOnly(CommonPage.errorMessage, "Error Message")){
				click(CommonPage.errorPage, "Error Page");
				click(CommonPage.nextButton, "Next Button");
				click(CommonPage.rateButton, "Rate Button");
			}
			
		}
	}
	public boolean warningHVIUpdatedPremiumImpactedVisible() throws Throwable{
		return isVisibleOnly(CommonPage.errorMessageHVIUpdatedPremiumImpacted, "warning Message appeared: HVI was updated and premium may have been impacted. If calculated HVI is incorrect, please contact u/w manager.");
	}
}
