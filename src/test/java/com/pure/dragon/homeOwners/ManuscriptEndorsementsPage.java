package com.pure.dragon.homeOwners;

import org.openqa.selenium.By;

public class ManuscriptEndorsementsPage {
	static By addButton;
	static By manuScriptEndorsementType, manuScriptEndorsementTitle, annualManualPremium, manuScriptEndorsementText;
	static{
		addButton = By.xpath("//a[contains(@onclick,'Action.189')]");
		manuScriptEndorsementType = By.xpath("//select[@title='Type']");
		manuScriptEndorsementTitle = By.xpath("//input[@title='Title']");
		annualManualPremium = By.xpath("//input[@title='Annual Manual Premium']");
		manuScriptEndorsementText = By.xpath("//textarea[@title='Endorsement Text']");
	}

	//********Collection Manuscript
	static By addAnotherItem, type, title_CO, annualManualPremium_CO, deleteBtn, endorsementText_CO;
	static {
		addAnotherItem = By.cssSelector("a[title='Add another item.']");
		type = By.xpath("//tr[@style][$]//select[contains(@id,'29014006')]");
		title_CO = By.xpath("//tr[@style][$]//input[contains(@id,'29014206')]");
		annualManualPremium_CO = By.xpath("//tr[@style][$]//input[contains(@id,'29014306')]");
		deleteBtn = By.xpath("//tr[@style][$]//a[contains(@title,'Delete the selected item.')]");
		endorsementText_CO = By.cssSelector("textarea[title='Endorsement Text']");
	}
}
