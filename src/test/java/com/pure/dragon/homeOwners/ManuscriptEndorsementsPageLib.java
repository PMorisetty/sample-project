package com.pure.dragon.homeOwners;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ManuscriptEndorsementsPageLib extends ActionEngine{
	public ManuscriptEndorsementsPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void clickAddBtn() throws Throwable {
		click(ManuscriptEndorsementsPage.addButton, "Add Button");
	}
	private void selectManuScriptEndorsementType(String manuScriptEndorsementType) throws Throwable {
		selectByVisibleText(ManuscriptEndorsementsPage.manuScriptEndorsementType, manuScriptEndorsementType, "select Endorsement Type");
	}
	private void enterManuScriptEndorsementTitle(String manuScriptEndorsementTitle) throws Throwable {
		type(ManuscriptEndorsementsPage.manuScriptEndorsementTitle, manuScriptEndorsementTitle, "select Endorsement Title");
		type(ManuscriptEndorsementsPage.manuScriptEndorsementTitle, Keys.TAB, "click on Tab");
	}
	private void enterManuScriptAnnualManualPremium(String annualManualPremium) throws Throwable {
		type(ManuscriptEndorsementsPage.annualManualPremium, annualManualPremium, "select Annual Premium");
		type(ManuscriptEndorsementsPage.annualManualPremium, Keys.TAB, "click on Tab");
	}
	private void enterManuScriptEndorsementText(String manuScriptEndorsementText) throws Throwable {
		type(ManuscriptEndorsementsPage.manuScriptEndorsementText, manuScriptEndorsementText, "select Endorsement Text");
		type(ManuscriptEndorsementsPage.manuScriptEndorsementText, Keys.TAB, "click on Tab");
	}
	public void fillManuscriptEndorsementDetails(Hashtable<String, String> data) throws Throwable{
		if(data.get("addManuscriptEndorsements")!=null && data.get("addManuscriptEndorsements").equalsIgnoreCase("Yes")){
			clickAddBtn();
			selectManuScriptEndorsementType(data.get("endorsementTypeManuScript"));
			enterManuScriptEndorsementTitle(data.get("endorsementTitle"));
			enterManuScriptAnnualManualPremium(data.get("endorsementAnnualPremium"));
			new CommonPageLib(driver, reporter).clickNextBtn(); //traverse to submenu
			enterManuScriptEndorsementText(data.get("endorsementText")); //if not filled, can't complete rating
		}
	}
	
	// Under Collections
		public void clickAdd() throws Throwable {
			click(ManuscriptEndorsementsPage.addAnotherItem, "AddAnotherItem Button");
		}

		public void selectType(String value, int... count) throws Throwable {
			int num = count != null ? count[0] : 1;
			String tempLocator = xpathUpdator(ManuscriptEndorsementsPage.type, String.valueOf(num));
			selectByVisibleText(By.xpath(tempLocator), value, "Type");
		}

		public void setTitle(String value, int... count) throws Throwable {
			int num = count != null ? count[0] : 1;
			String tempLocator = xpathUpdator(ManuscriptEndorsementsPage.title_CO, String.valueOf(num));
			type(By.xpath(tempLocator), value, "title");
		}

		public void setAnnualManualPremium(String value, int... count) throws Throwable {
			int num = count != null ? count[0] : 1;
			String tempLocator = xpathUpdator(ManuscriptEndorsementsPage.annualManualPremium_CO, String.valueOf(num));
			type(By.xpath(tempLocator), value, "AnnualManualPremium");
		}

		public void clickDelete(String value, int... count) throws Throwable {
			int num = count != null ? count[0] : 1;
			String tempLocator = xpathUpdator(ManuscriptEndorsementsPage.deleteBtn, String.valueOf(num));
			click(By.xpath(tempLocator), "DeleteBtn");
		}

		public int getNoOfItemsPresent() {
			By locator = By.xpath("//table[@class='objectListTable']//tr[@style]");
			int size = this.driver.findElements(locator).size();
			return size;

		}

		public void addAnotheItem(Hashtable<String, String> data) throws Throwable {
			int noOfItems = Integer.valueOf(data.get("NoOfEndorsements"));
			for (int iterator = 1; iterator <= noOfItems; iterator++) {
				// int noOfItemsPresent = this.getNoOfItemsPresent();
				this.clickAdd();
				this.fillNewItemDetails(iterator, data);
			}
			;
		}

		public void fillNewItemDetails(int count, Hashtable<String, String> data) throws Throwable {
			this.selectType(data.get("Type_" + count), count);
			this.setTitle(data.get("Title_" + count), count);
			this.setAnnualManualPremium(data.get("AnnualManualPremium_" + count), count);
			// this.clickDelete(data.get("Delete_" + count), count);
		}

		public void setEndorsementText_CO(String value) throws Throwable {
			type(ManuscriptEndorsementsPage.endorsementText_CO, value, "EndorsementText");
		}
}