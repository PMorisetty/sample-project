package com.pure.dragon.homeOwners;

import java.util.Hashtable;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class EarthquakePageLib extends ActionEngine{
	public EarthquakePageLib(EventFiringWebDriver driver, CReporter reporter) {
		this.driver = driver;
		this.reporter = reporter;
	}

	public void fillEarthquakeDetails(Hashtable<String, String> data) throws Throwable {
		switch(data.get("state")){
		case "CA":
			this.selectEarthquakeCoverageOption(data.get("earthquakeCoverageOption"));
			break;
		
		}		

	}
	
	private void selectEarthquakeCoverageOption(String value) throws Throwable{
		selectByVisibleText(EarthquakePage.earthquakeCoverageOption, value, "Earthquake Coverage Option");
	}
	
}
