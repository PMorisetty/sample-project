package com.pure.dragon.homeOwners;

import org.openqa.selenium.By;

public class PreviousClaimDetailsPage {
	static By priorLossYes;
	static By priorLossNo;
	static By addClaimButton;
	
	static By sourceDropDown;
	static By lossDate;
	static By lossType;
	static By amountPaid;
	
	static By lossClaimHistoryYES, lossClaimHistoryNO, addLossDetails, collectionClass, source, lossType_CO, lossDate_CO,
			amountPaid_CO, claimClosed;
	
	static{
		priorLossYes = By.xpath("//*[contains(@title,'Any prior losses on this location over the past five years')]/..//input[@title='Yes']");
		priorLossNo = By.xpath("//*[contains(@title,'Any prior losses on this location over the past five years')]/..//input[@title='No']");
		addClaimButton = By.xpath("//a[contains(@onclick,'Action.189')]");
		
		sourceDropDown = By.cssSelector("select[title='Source']");
		lossDate = By.cssSelector("input[title='Loss Date']");
		lossType = By.cssSelector("select[title='Loss Type']");
		amountPaid = By.cssSelector("input[title='Amount Paid']");
		
		//Under Collections
				lossClaimHistoryYES = By.cssSelector("input[name*='_28964006'][title='Yes']");
				lossClaimHistoryNO = By.cssSelector("input[name*='_28964006'][title='No']");
				addLossDetails = By.cssSelector("a[title='Add another item.']");
				collectionClass = By
						.xpath("//table[@class='objectListTable']//tr[@style][$]//select[contains(@id,'_29022606')]");
				source = By.xpath("//table[@class='objectListTable']//tr[@style][$]//select[contains(@id,'_28971506')]");
				lossType_CO = By.xpath("//table[@class='objectListTable']//tr[@style][$]//select[contains(@id,'_27917102')]");
				lossDate_CO = By.xpath("//table[@class='objectListTable']//tr[@style][$]//input[contains(@title,'Loss Date')]");
				amountPaid_CO = By
						.xpath("//table[@class='objectListTable']//tr[@style][$]//input[contains(@title,'Amount Paid')]");
				claimClosed = By.xpath("//table[@class='objectListTable']//tr[@style][$]//select[contains(@id,'_28907905')]");
	}
}
