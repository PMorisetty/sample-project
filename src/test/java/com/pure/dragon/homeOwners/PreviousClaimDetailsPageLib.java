package com.pure.dragon.homeOwners;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class PreviousClaimDetailsPageLib extends ActionEngine{
	public PreviousClaimDetailsPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	private void selectPriorLossInFiveYear(String value) throws Throwable {
		if(value.equalsIgnoreCase("YES")){
			click(PreviousClaimDetailsPage.priorLossYes, "Prior Loss - Yes");
		}else{
			click(PreviousClaimDetailsPage.priorLossNo, "Prior Loss - No");
		}
	}
	
	private void clickAddClaimButton() throws Throwable{
		click(PreviousClaimDetailsPage.addClaimButton, "Add Claim button");
	}
	
	public void selectPriorLoss(Hashtable<String, String> data) throws Throwable{
		if((data.get("priorLoss") != null) || (data.get("priorLoss") != "")){
			selectPriorLossInFiveYear(data.get("priorLoss"));
			if(data.get("priorLoss").equalsIgnoreCase("YES")){
				clickAddClaimButton();
				selectLossSource(data.get("priorLossSource"));
				setLossDate(data.get("priorLossDate"));
				selectLossType(data.get("priorLossType"));
				setAmountPaid(data.get("priorLossAmount"));
			}
		}
	}
	
	public void selectLossSource(String value) throws Throwable{
		selectByVisibleText(PreviousClaimDetailsPage.sourceDropDown, value, "Source Drop Down");
	}
	public void setLossDate(String value) throws Throwable{
		type(PreviousClaimDetailsPage.lossDate, value, "Loss Date");
	}
	public void selectLossType(String value) throws Throwable{
		selectByVisibleText(PreviousClaimDetailsPage.lossType, value, "Loss Type Drop Down");
	}
	public void setAmountPaid(String value) throws Throwable{
		type(PreviousClaimDetailsPage.amountPaid, value, "Amount Paid");
	}
	public void verifyCyberFraudLossType(boolean value) throws Throwable{
		try{
		Select s = new Select(driver.findElement(PreviousClaimDetailsPage.lossType));
		boolean flag = false;
		List<WebElement> options = s.getOptions();
		for(WebElement option: options){
			if(option.getText().trim().contains("Fraud and Cyber Defense")) flag = true;
		}
		if(value){
			if(flag){
				reporter.SuccessReport("Verifying Fraud and Cyber Defense", "Fraud and Cyber Defense is present in loss type");
			}else{
				reporter.failureReport("Verifying Fraud and Cyber Defense", "Fraud and Cyber Defense is not present in loss type", driver);
			}
			
		}else{
			if(!flag){
				reporter.SuccessReport("Verifying Fraud and Cyber Defense", "Fraud and Cyber Defense is not present in loss type");
			}else{
				reporter.failureReport("Verifying Fraud and Cyber Defense", "Fraud and Cyber Defense is present in loss type", driver);
			}
			
		}
		}catch(NoSuchElementException ex){
			reporter.failureReport("Loss Type drop down", "Loss type drop down not found.", driver);
			throw new Exception("Loss type drop down not found.");
		}
	}
	
	//Under Collections
		public void radioBtn_LossClaimHistory(String value) throws Throwable{
			if("YES".equalsIgnoreCase(value))
	    		click(PreviousClaimDetailsPage.lossClaimHistoryYES, "LossClaimHistoryYES");
	    	else if("NO".equalsIgnoreCase(value))
	    		click(PreviousClaimDetailsPage.lossClaimHistoryNO, "LossClaimHistoryNO");
		}
		public void clickAdd() throws Throwable{
			click(PreviousClaimDetailsPage.addLossDetails, "AddLossDetails");
		}
		public void selectCollectionClass(String value, int... count) throws Throwable{
			int num = count != null? count[0]:1;
			String tempLocator = xpathUpdator(PreviousClaimDetailsPage.collectionClass, String.valueOf(num));
			selectByVisibleText(By.xpath(tempLocator), value, "collectionClass");
		}
		public void selectSource(String value, int... count) throws Throwable{
			int num = count != null? count[0]:1;
			String tempLocator = xpathUpdator(PreviousClaimDetailsPage.source, String.valueOf(num));
			selectByVisibleText(By.xpath(tempLocator), value, "Source");
		}
		public void selectLossType(String value, int... count) throws Throwable{
			int num = count != null? count[0]:1;
			String tempLocator = xpathUpdator(PreviousClaimDetailsPage.lossType_CO, String.valueOf(num));
			selectByVisibleText(By.xpath(tempLocator), value, "LossType");
		}
		public void setLossDate(String value, int... count) throws Throwable{
			int num = count != null? count[0]:1;
			String tempLocator = xpathUpdator(PreviousClaimDetailsPage.lossDate_CO, String.valueOf(num));
			type(By.xpath(tempLocator), value, "LossDate");
		}
		public void setAmountPaid(String value, int... count) throws Throwable{
			int num = count != null? count[0]:1;
			String tempLocator = xpathUpdator(PreviousClaimDetailsPage.amountPaid_CO, String.valueOf(num));
			type(By.xpath(tempLocator), value, "AmountPaid");
		}
		public void selectClaimClosed(String value, int... count) throws Throwable{
			int num = count != null? count[0]:1;
			String tempLocator = xpathUpdator(PreviousClaimDetailsPage.claimClosed, String.valueOf(num));
			selectByVisibleText(By.xpath(tempLocator), value, "ClaimClosed");
		}
		
		public int getNoOfItemsPresent(){
			By locator = By.xpath("//table[@class='objectListTable']//tr[@style]");
			int size = this.driver.findElements(locator).size();
			return size;
			
		}
		public void addAnotheItem(Hashtable<String, String> data) throws Throwable{
			int noOfItems = Integer.valueOf(data.get("NoOfLossItems"));
			for(int iterator = 1; iterator <= noOfItems; iterator++){
				this.clickAdd();
				this.fillNewItemDetails(iterator, data);
			};
		}
		public void fillNewItemDetails(int count, Hashtable<String, String> data) throws Throwable{
			this.selectCollectionClass(data.get("CollectionClass_" + count), count);
			this.selectSource(data.get("Source_" + count), count);
			this.selectLossType(data.get("LossType_" + count), count);
			this.setLossDate(data.get("LossDate_" + count), count);
			this.setAmountPaid(data.get("AmountPaid_" + count), count);
			this.selectClaimClosed(data.get("ClaimClosed_" + count), count);
		}
}
