package com.pure.dragon.homeOwners;

import org.openqa.selenium.By;

public class LocationCoveragePage {
	static By windStormHailDed;
	
	static By primaryResidenceYes;
	static By primaryResidenceNo;
	static By secondaryResidenceYes;
	static By secondaryResidenceNo;
	static By rentedResidenceYes;
	static By rentedResidenceNo;
	static By insuredByPureYes;
	static By insuredByPureNo;
	static By vacantResidenceYes;
	static By vacantResidenceNo;
	static By renovationYes;
	static By renovationNo;
	static By forSaleYes;
	static By forSaleNo;
	static By groundUpConstructionYes, groundUpConstructionNo; 
	static By mortgageeInfoNO,mortgageeInfoYES;
	//construction details
	static By homeRatingcharsPrefillYes,homeRatingcharsPrefillNo;
	static By yearBuiltInput;
	static By yearRenovatedInput;
	static By protectionClass;
	static By managerPPCOverride;
	static By protectionClassOverride;
	static By squareFootage;
	static By constructionType;
	static By noOfFloors;
	static By homeBeachFrontYes;
	static By homeBeachFrontNo;
	static By distanceToBeach;
	static By managerDTCOverride;
	static By distanceToCoastOverride;
	static By riskLocatedFloor;
	static By supportedHomeYes, supportedHomeNo;
	static By retrofittedForEarthQuakeYES, retrofittedForEarthQuakeNO;
	//roof characteristics
	static By roofShape;
	static By roofCoveringCredit;
	static By roofCoveringMaterial;	
	static By roofCovering;
	static By openingProtection;
	static By hurricaneFortification;
	static By yearRoofReplaced;
	static By impactShuttersType;
	//protection details
	static By fireAlarmYes;
	static By fireAlarmNo;
	static By burglarAlarmYes;
	static By burglarAlarmNo;
	static By waterLeakageSystem;
	static By signalContinuity24HourNo;
	static By signalContinuity24HourYes;
	static By gatedCommunityPatrolYes, gatedCommunityPatrolNo; 
	//flood details
	static By pureNfipYes;
	static By pureNfipNo;
	static By excessFloodYes;
	static By dwelling;
	static By contents;
	static By excessFloodNo;
	static By CBRAZoneNo, CBRAZoneYes;

	static By uwTaxOverride;
	static By selectBCEG;
	static By selectSecondWaterResist,rentedForTenWeeksYes,rentedForTenWeeksNo,yearBuilt;
	static By selectRoofDeck,selectRoofDeckAttachment,perimeterSecurityProtection,floodZone,managerFloodZoneOverRide,unOccupiedTwoMonthsYes,unOccupiedTwoMonthsNo,squareFootageFL;
	static By cityName,countyName,cityCode,automaticSeismicYes,automaticSeismicNo,leedCertifiedHomeYes,leedCertifiedHomeNo,certifiedInspectionYes,certifiedInspectionNo,stormShuttersYes,stormShuttersNo,handcraftedLogHomeYes,handcraftedLogHomeNo;
	static By lightningProtectionYes,lightningProtectionNo,externalPerimeterGateYes,externalPerimeterGateNo,fulltimeCaretakerYes,fulltimeCaretakerNo,fireWiseCommunityCreditYes,fireWiseCommunityCreditNo,gasLeakDetectorNo,gasLeakDetectorYes,lowTempMonitoringSysYes,lowTempMonitoringSysNo,guardGatedCommunityNo,guardGatedCommunityYes,countyLabel,territoryLabel;
	static By permanentInstGeneratorYes,permanentInstGeneratorNo;
	static By seasonalHomeYes,seasonalHomeNo;  
	static By firewiseCommunityYes,firewiseCommunityNo;
	static By yearBuiltID;
	static By selectRoofWall;
	static By standardizedAddress;
	static By openingProtectionType;
	static By wallConstructionType;
	static By exteriorWallsCovering;
	
	static By pureInsurePrimaryHomeYes, pureInsurePrimaryHomeNo;
	static By outOfResidence, costOfPlannedWork;
	static By sprinklerSystemWithWaterflowYes, sprinklerSystemWithWaterflowNo;
	static By residentialSprinklerSystemYes, residentialSprinklerSystemNo;
	static By NFIPMaxlimitsYes,NFIPMaxlimitsNo; 
	static By doorMan24HourYes, doorMan24HourNo, surveillanceCameraYes, surveillanceCameraNo, lockedOrMannedElevatorYes, lockedOrMannedElevatorNo;
	
	//elevation certificate
	static By baseFloodElevation, buildingDiagramNumber, topOfBottomFloor, topOfNextFloor, bottomOfLowest,
	bottomOfAttachedGarage, lowestElevationOfMachinery, totalSquareFeet, permanentFloodOpenings, totalAreaOfPermanentOpenings;
	static{
		windStormHailDed = By.xpath("//div[contains(text(),'Windstorm or Hail Ded')]/../div[@class='fieldAtRight']/div");
		primaryResidenceYes = By.xpath("//*[@title='Is this your Primary Residence?']/..//input[@title='Yes']");
		primaryResidenceNo = By.xpath("//*[@title='Is this your Primary Residence?']/..//input[@title='No']");
		secondaryResidenceYes = By.xpath("//*[@title='Is this a seasonal or secondary home?']/..//input[@title='Yes']");
		secondaryResidenceNo = By.xpath("//*[@title='Is this a seasonal or secondary home?']/..//input[@title='No']");
		rentedResidenceYes = By.xpath("//*[@title='Is the residence or part of the residence rented to others?']/..//input[@title='Yes']");
		rentedResidenceNo = By.xpath("//*[@title='Is the residence or part of the residence rented to others?']/..//input[@title='No']");
		insuredByPureYes = By.xpath("//*[@title='Does PURE insure the primary home?']/..//input[@title='Yes']");
		insuredByPureNo = By.xpath("//*[@title='Does PURE insure the primary home?']/..//input[@title='No']");
		vacantResidenceYes = By.xpath("//*[@title='Is the property vacant?']/..//input[@title='Yes']");
		vacantResidenceNo = By.xpath("//*[@title='Is the property vacant?']/..//input[@title='No']");
		renovationYes = By.xpath("//*[contains(@title ,'renovation?')]/..//input[@title='Yes']");
		renovationNo = By.xpath("//*[contains(@title ,'renovation?')]/..//input[@title='No']");
		forSaleYes = By.xpath("//*[contains(@title ,'for sale?')]/..//input[@title='Yes']");
		forSaleNo = By.xpath("//*[contains(@title ,'for sale?')]/..//input[@title='No']");
		groundUpConstructionYes = By.xpath("//*[contains(@title ,'ground-up construction')]/..//input[@title='Yes']");
		groundUpConstructionNo = By.xpath("//*[contains(@title ,'ground-up construction')]/..//input[@title='No']");
		mortgageeInfoNO= By.xpath("//div[@title='Is there, or will there be a mortgage on this location? (Excluding lines of credit)']/..//input[@title='No']");
		mortgageeInfoYES= By.xpath("//div[@title='Is there, or will there be a mortgage on this location? (Excluding lines of credit)']/..//input[@title='Yes']");
		homeRatingcharsPrefillYes = By.xpath("//div[@title='Do you want to have the information shown above prefilled into the quote?']/..//input[@title='Yes']");
		homeRatingcharsPrefillNo = By.xpath("//div[@title='Do you want to have the information shown above prefilled into the quote?']/..//input[@title='No']");
		yearBuiltInput = By.cssSelector("input[title='Year Built']");
		yearRenovatedInput = By.xpath("//div[contains(text(),'Year Renovated')]/..//input");
		protectionClass = By.cssSelector("select[title='Protection Class'][class='unFilledMandatoryField']");
		managerPPCOverride = By.xpath("//input[@title='Manager PPC override']");
		protectionClassOverride = By.xpath("//select[@title='Protection Class Override']");
		squareFootage = By.cssSelector("input[title*='square foot' i]");
		constructionType = By.cssSelector("select[title='Construction Type']");
		noOfFloors = By.cssSelector("input[title='Number of Floors at the risk location']");
		homeBeachFrontYes = By.xpath("//*[contains(@title ,'home beachfront')]/..//input[@title='Yes']");
		homeBeachFrontNo = By.xpath("//*[contains(@title ,'home beachfront')]/..//input[@title='No']");
		distanceToBeach = By.xpath("//select[@title='Distance to Coast override']");
		managerDTCOverride = By.xpath("//input[@title='Manager DTC override']");
		distanceToCoastOverride = By.xpath("//select[@title='Distance to Coast override']");
		riskLocatedFloor = By.xpath("//input[contains(@title, 'risk is located')]");


		roofShape = By.cssSelector("select[id$='_27879502']");
		roofCoveringCredit = By.cssSelector("select[title='Roof Covering Credit']");
		roofCoveringMaterial = By.cssSelector("select[title='Roof Covering Material']");
		roofCovering = By.cssSelector("select[id$='_28186305']");
		openingProtection = By.cssSelector("select[id$='_28186605']");
		hurricaneFortification = By.cssSelector("select[title='Hurricane Fortification']");
		yearRoofReplaced = By.cssSelector("input[id$='_29314507']");
		impactShuttersType = By.xpath("//select[@title='What type of Impact Shutters?']");

		fireAlarmYes = By.xpath("//*[contains(@title ,'Fire Alarm')]/..//input[@title='Yes']");
		fireAlarmNo = By.xpath("//*[contains(@title ,'Fire Alarm')]/..//input[@title='No']");
		burglarAlarmYes = By.xpath("//*[contains(@title ,'Burglar Alarm')]/..//input[@title='Yes']");
		burglarAlarmNo = By.xpath("//*[contains(@title ,'Burglar Alarm')]/..//input[@title='No']");
		waterLeakageSystem = By.cssSelector("select[title='Water Leak Detection System'i]");
		signalContinuity24HourNo = By.xpath("//div[@title='24 Hour Signal Continuity']/..//input[@title='No']");
		signalContinuity24HourYes = By.xpath("//div[@title='24 Hour Signal Continuity']/..//input[@title='Yes']");
		gatedCommunityPatrolYes = By.xpath("//div[@title='Gated Community Patrol Service']/..//input[@title='Yes']");
		gatedCommunityPatrolNo = By.xpath("//div[@title='Gated Community Patrol Service']/..//input[@title='No']");
		
		pureNfipYes = By.xpath("//*[contains(@title,'PURE NFIP Flood')]/..//input[@title='Yes']");
		pureNfipNo = By.xpath("//*[contains(@title,'PURE NFIP Flood')]/..//input[@title='No']");
		excessFloodYes = By.xpath("//*[contains(@title ,'excess flood coverage?')]/..//input[@title='Yes']");
		dwelling = By.xpath("//select[@title='Dwelling Limit Percent']");
		contents = By.xpath("//select[@title='Contents Limit Percent']");
		excessFloodNo = By.xpath("//*[contains(@title ,'excess flood coverage?')]/..//input[@title='No']");
		CBRAZoneNo = By.xpath("//*[contains(@title ,'home in CBRA Zone?')]/..//input[@title='No']");
		CBRAZoneYes = By.xpath("//*[contains(@title ,'home in CBRA Zone?')]/..//input[@title='Yes']");

		uwTaxOverride = By.cssSelector("input[title='Underwriting tax override'][class='unFilledMandatoryField']");
		selectBCEG = By.xpath("//select[@title='BCEG'][@class='unFilledMandatoryField']");
		selectSecondWaterResist = By.cssSelector("select[id $= '_28186805']");
		//selectSecondWaterResist = By.xpath("//select[@title = 'Secondary Water Resistance']"); - changed for SC State
		//selectRoofDeck = By.cssSelector("select[id$='_28186405']");		
		selectRoofDeckAttachment = By.xpath("//div[contains(text(),'Roof Deck Attachment')]/../div/select");	
		selectRoofDeck = By.xpath("//div[@title='Roof Deck']/../div/select");
		perimeterSecurityProtection = By.xpath("//select[@title = 'Perimeter Security Protection']");
		floodZone = By.xpath("//select[@title = 'Flood Zone']");
		managerFloodZoneOverRide = By.xpath("//input[@title='Manager Flood Zone override']");		
		unOccupiedTwoMonthsYes = By.xpath("//*[contains(@title ,'Is the home unoccupied for 2 or more consecutive months?')]/..//input[@title='Yes']");
		unOccupiedTwoMonthsNo = By.xpath("//*[contains(@title ,'Is the home unoccupied for 2 or more consecutive months?')]/..//input[@title='No']");
		rentedForTenWeeksYes = By.xpath("//*[@title='Is the property rented more than 10 weeks a year?']/..//input[@title='Yes']");
		rentedForTenWeeksNo = By.xpath("//*[@title='Is the property rented more than 10 weeks a year?']/..//input[@title='No']");
		yearBuilt = By.xpath("//input[@title='Year']");
		squareFootageFL = By.xpath("//input[@title='Square Footage']");
		cityName = By.cssSelector("input[title='City name']");
		countyName = By.cssSelector("input[title='County name']");
		cityCode = By.cssSelector("input[title='City Code']");
		automaticSeismicYes = By.xpath("//*[contains(@title ,'Automatic Seismic Shut-Off Valve')]/..//input[@title='Yes']");
		automaticSeismicNo = By.xpath("//*[contains(@title ,'Automatic Seismic Shut-Off Valve')]/..//input[@title='No']");
		leedCertifiedHomeYes = By.xpath("//*[contains(@title ,'LEED Certified Home')]/..//input[@title='Yes']");
		leedCertifiedHomeNo = By.xpath("//*[contains(@title ,'LEED Certified Home')]/..//input[@title='No']");
		certifiedInspectionYes = By.xpath("//*[contains(@title ,'Certified Inspection')]/..//input[@title='Yes']");
		certifiedInspectionNo = By.xpath("//*[contains(@title ,'Certified Inspection')]/..//input[@title='No']");
		stormShuttersYes = By.xpath("//*[contains(@title ,'Storm Shutters')]/..//input[@title='Yes']");
		stormShuttersNo = By.xpath("//*[contains(@title ,'Storm Shutters')]/..//input[@title='No']");
		handcraftedLogHomeYes = By.xpath("//*[contains(@title ,'Is this a handcrafted log home?')]/..//input[@title='Yes']");
		handcraftedLogHomeNo = By.xpath("//*[contains(@title ,'Is this a handcrafted log home?')]/..//input[@title='No']");
		firewiseCommunityYes = By.xpath("//*[contains(@title ,'Firewise Community')]/..//input[@title='Yes']");
		firewiseCommunityNo = By.xpath("//*[contains(@title ,'Firewise Community')]/..//input[@title='No']");
		externalPerimeterGateYes = By.xpath("//*[@title='External Perimeter Gate']/..//input[@title='Yes']");
		externalPerimeterGateNo = By.xpath("//*[@title='External Perimeter Gate']/..//input[@title='No']");
		fulltimeCaretakerYes = By.xpath("//*[@title='Full Time Live In Caretaker']/..//input[@title='Yes']");
		fulltimeCaretakerNo = By.xpath("//*[@title='Full Time Live In Caretaker']/..//input[@title='No']");
		fireWiseCommunityCreditYes = By.xpath("//*[@title='Credit is applicable for a dwelling located in a community which is recognized as a Firewise Community by the National Fire Protection Association.']/..//input[@title='Yes']");
		fireWiseCommunityCreditNo = By.xpath("//*[@title='Credit is applicable for a dwelling located in a community which is recognized as a Firewise Community by the National Fire Protection Association.']/..//input[@title='No']");
		gasLeakDetectorYes = By.xpath("//*[@title='Gas Leak Detector']/..//input[@title='Yes']");
		gasLeakDetectorNo = By.xpath("//*[@title='Gas Leak Detector']/..//input[@title='No']");
		lowTempMonitoringSysYes = By.xpath("//*[contains(@title,'Low Temperature')]/..//input[@title='Yes']");
		lowTempMonitoringSysNo = By.xpath("//*[contains(@title,'Low Temperature')]/..//input[@title='No']");
		guardGatedCommunityYes = By.xpath("//*[@title='Guard Gated Community']/..//input[@title='Yes']");
		guardGatedCommunityNo = By.xpath("//*[@title='Guard Gated Community']/..//input[@title='No']");
		countyLabel = By.xpath("//*[@title='County']/..//div/label");
		territoryLabel = By.xpath("//*[@title='Territory']/..//div/label");
		permanentInstGeneratorYes = By.xpath("//*[@title='Permanently Installed Generator']/..//input[@title='Yes']");
		permanentInstGeneratorNo = By.xpath("//*[@title='Permanently Installed Generator']/..//input[@title='No']");
		lightningProtectionYes = By.xpath("//*[@title='Lightning Protection System']/..//input[@title='Yes']");
		lightningProtectionNo = By.xpath("//*[@title='Lightning Protection System']/..//input[@title='No']");
		yearBuiltID = By.cssSelector("input[id$='_27861702']");
		seasonalHomeYes = By.xpath("//*[contains(@title ,'seasonal home')]/..//input[@title='Yes']");
		seasonalHomeNo = By.xpath("//*[contains(@title ,'seasonal home')]/..//input[@title='No']");
		selectRoofWall = By.cssSelector("select[id$='_28186505']");
		standardizedAddress = By.cssSelector("select[title='Google API Standardized Address']");
		openingProtectionType = By.cssSelector("select[id*='_12097014']");
		wallConstructionType = By.cssSelector("select[id*='_30002914']");

		exteriorWallsCovering = By.cssSelector("select[title='Exterior Wall Coverings']");
		supportedHomeYes = By.cssSelector("input[name$='_30037314'][title='Yes']");
		supportedHomeNo = By.cssSelector("input[name$='_30037314'][title='No']");
		retrofittedForEarthQuakeYES = By.cssSelector("input[name$='_30037414'][title='Yes']");
		retrofittedForEarthQuakeNO = By.cssSelector("input[name$='_30037414'][title='No']");
		
		pureInsurePrimaryHomeYes = By.xpath("//*[@title='Does PURE insure the primary home?']/..//input[@title='Yes']");
		pureInsurePrimaryHomeNo = By.xpath("//*[@title='Does PURE insure the primary home?']/..//input[@title='No']");
		
		outOfResidence = By.xpath("//select[contains(@title, 'out of the residence')]");
		costOfPlannedWork = By.xpath("//input[contains(@title, 'planned work')]");
		
		sprinklerSystemWithWaterflowYes = By.cssSelector("input[name$='_28905905'][title='Yes']");
		sprinklerSystemWithWaterflowNo = By.cssSelector("input[name$='_28905905'][title='No']");
		
		residentialSprinklerSystemYes = By.cssSelector("input[name$='_28903405'][title='Yes']");
		residentialSprinklerSystemNo = By.cssSelector("input[name$='_28903405'][title='No']");
		
		NFIPMaxlimitsYes = By.xpath("//div[@title='Does this location have a PURE NFIP Flood policy with maximum limits?']/..//input[@title='Yes']");
		NFIPMaxlimitsNo = By.xpath("//div[@title='Does this location have a PURE NFIP Flood policy with maximum limits?']/..//input[@title='No']");
		
		doorMan24HourYes = By.xpath("//div[@title='24 Hour Door Man']/..//input[@title='Yes']");
		doorMan24HourNo = By.xpath("//div[@title='24 Hour Door Man']/..//input[@title='No']");
		surveillanceCameraYes = By.xpath("//div[@title='Surveillance Camera']/..//input[@title='Yes']");
		surveillanceCameraNo = By.xpath("//div[@title='Surveillance Camera']/..//input[@title='No']");
		lockedOrMannedElevatorYes = By.xpath("//div[@title='Locked or Manned Elevator']/..//input[@title='Yes']");
		lockedOrMannedElevatorNo = By.xpath("//div[@title='Locked or Manned Elevator']/..//input[@title='No']");
		
		//elevation certificate
		baseFloodElevation = By.xpath("//input[@title='Base Flood Elevation']");
		buildingDiagramNumber = By.xpath("//select[@title='What is the building diagram number?']");
		topOfBottomFloor = By.xpath("//input[contains(@title, 'Top of bottom floor')]");
		topOfNextFloor = By.xpath("//input[contains(@title, 'Top of the next floor')]");
		bottomOfLowest = By.xpath("//input[contains(@title, 'Bottom of the lowest')]");
		bottomOfAttachedGarage = By.xpath("//input[contains(@title, 'Bottom of the attached garage')]");
		lowestElevationOfMachinery = By.xpath("//input[contains(@title, 'Lowest elevation of machinery')]");
		totalSquareFeet = By.xpath("//input[contains(@title, 'total square feet ')]");
		permanentFloodOpenings = By.xpath("//input[contains(@title, 'No. of permanent flood openings')]");
		totalAreaOfPermanentOpenings = By.xpath("//input[contains(@title, 'Total area of all permanent openings')]");
		
	}
}
