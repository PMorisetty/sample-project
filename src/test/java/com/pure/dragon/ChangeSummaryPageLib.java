package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ChangeSummaryPageLib extends ActionEngine{
	public ChangeSummaryPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void clickMoreChangesBtn() throws Throwable {
		click(ChangeSummaryPage.moreChangesButton, "More Changes Button");
	}
	public void clickRateBtn() throws Throwable {
		click(ChangeSummaryPage.rateButton, "Rate Button");
	}
	public void clickExitBtn() throws Throwable {
		click(ChangeSummaryPage.exitButton, "Exit Button");
	}
}
