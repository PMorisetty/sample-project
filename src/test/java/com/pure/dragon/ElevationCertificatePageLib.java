package com.pure.dragon;

import java.util.Hashtable;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class ElevationCertificatePageLib extends ActionEngine{
	
	public ElevationCertificatePageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	//elevation certificate
	private void setBaseFloodElevation(String value) throws Throwable {
		if(value!=null && !value.equalsIgnoreCase("null")&& value != ""){
			clearData(ElevationCertificatePage.baseFloodElevation);
			type(ElevationCertificatePage.baseFloodElevation, value, "Base Flood Elevation");
			type(ElevationCertificatePage.baseFloodElevation, Keys.TAB, "TAB Key");
			waitUntilJQueryReady();
		}
	}
	private void selectBuildingDiagramNumber(String value) throws Throwable {
		if(value != null && value != ""){
			selectByVisibleText(ElevationCertificatePage.buildingDiagramNumber, value, "Building Diagram Number");
			waitUntilJQueryReady();
		}
	}
	private void setTopOfBottomFloor(String value) throws Throwable {
		if(value != null && value != ""){
			type(ElevationCertificatePage.topOfBottomFloor, value, "TopOfBottomFloor");
			type(ElevationCertificatePage.topOfBottomFloor, Keys.TAB, "TAB Key");
			waitUntilJQueryReady();
		}
	}
	private void setTopOfNextFloor(String value) throws Throwable {
		if(value != null && value != "")
			type(ElevationCertificatePage.topOfNextFloor, value, "TopOfNextFloor");
	}
	private void setBottomOfAttachedGarage(String value) throws Throwable {
		if(value != null && value != ""){
			type(ElevationCertificatePage.bottomOfAttachedGarage, value, "BottomOfAttachedGarage");
			type(ElevationCertificatePage.bottomOfAttachedGarage, Keys.TAB, "TAB Key");
			waitUntilJQueryReady();
		}
	}
	private void setTotalSquareFeet(String value) throws Throwable {
		if(value != null && value != "")
			type(ElevationCertificatePage.totalSquareFeet, value, "TotalSquareFeet");
	}
	private void setPermanentFloodOpenings(String value) throws Throwable {
		if(value != null && value != "")
			type(ElevationCertificatePage.permanentFloodOpenings, value, "PermanentFloodOpenings");
	}
	private void setTotalAreaOfPermanentOpenings(String value) throws Throwable {
		if(value != null && value != "")
			type(ElevationCertificatePage.totalAreaOfPermanentOpenings, value, "TotalAreaOfPermanentOpenings");
	}
	public void fillElevationCertificateDetails(Hashtable<String, String> data) throws Throwable
	{
		if(isVisibleOnly(ElevationCertificatePage.baseFloodElevation, "BaseFloodElevation")){
			switch(data.get("state")){
			case "NJ":
				setBaseFloodElevation(data.get("baseFloodElevation"));
				selectBuildingDiagramNumber(data.get("buildingDiagramNumber"));
				setTopOfBottomFloor(data.get("topOfBottomFloor"));
				setTopOfNextFloor(data.get("topOfNextFloor"));
				setBottomOfAttachedGarage(data.get("bottomOfAttachedGarage"));
				setTotalSquareFeet(data.get("totalSquareFeet"));
				setPermanentFloodOpenings(data.get("permanentFloodOpenings"));
				setTotalAreaOfPermanentOpenings(data.get("totalAreaOfPermanentOpenings"));
				break;
			}
		}
	}
}
