package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.homeOwners.CommonPage;
import com.pure.dragon.homeOwners.CommonPageLib;
import com.pure.report.CReporter;

public class SummaryPageLib extends ActionEngine{
	public SummaryPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/*Function to click on Request Issue button*/
	public void clickRequestIssueButton() throws Throwable{
		click(SummaryPage.requestIssueButton, "Request Issue Button");
		waitUntilJQueryReady();
		CommonPageLib commonPageLib= new CommonPageLib(driver, reporter);
		if(commonPageLib.warningHVIUpdatedPremiumImpactedVisible()){
			commonPageLib.clickRateBtn();
			waitUntilJQueryReady();
		}
	}
	/*Function to click on Copy Quote button*/
	public void clickCopyQuoteButton() throws Throwable{
		click(SummaryPage.copyQuoteButton, "Copy Quote Button");
	}
	/*Function to click on Delete Tree button*/
	public void clickDeleteTreeButton() throws Throwable{
		click(SummaryPage.deleteTreeButton, "Delete Tree Button");
	}
	/*Function to click on Exit button*/
	public void clickExitButton() throws Throwable{
		click(SummaryPage.exitButton, "Exit Button");
	}

	public void clickCompleteQuote() throws Throwable{
		click(SummaryPage.completeQuote, "completeQuote");
	}
	public void clickFormCode() throws Throwable{
		click(SummaryPage.clickFormCode, "Form Code link");	
	}
	public void clickNextButton() throws Throwable{
		click(SummaryPage.nextButton, "Next Button");	
	}
}
