package com.pure.dragon;

import org.openqa.selenium.By;

public class SummaryPage {
	public static By requestIssueButton;//public bcoz, its being used @ script level. 
	static By copyQuoteButton;
	static By deleteTreeButton;
	static By exitButton, completeQuote;
	static By clickFormCode, nextButton;



	static {
		requestIssueButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.398705')]");
		copyQuoteButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.487405')]");
		deleteTreeButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.524507')]");
		exitButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.88301')]");
		completeQuote = By.xpath("//a[text()= '>>> complete quote']");
		clickFormCode = By.xpath("//a[@title='Form Code']");

		/********** All Quotes section******/
		nextButton = By.xpath("//div[contains(text(),'all quotes')]//following-sibling::div//a[contains(text(),'next')]");
	}
}
