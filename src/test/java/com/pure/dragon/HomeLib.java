package com.pure.dragon;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.HomePage;
import com.pure.report.CReporter;

public class HomeLib extends ActionEngine{

	public HomeLib(EventFiringWebDriver webDriver, CReporter reporter) {
		// TODO Auto-generated constructor stub
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void login(String userName, String password) throws Throwable{
		this.logout();
		type(HomePage.brokerNumber,"0", "Broker Number");
		type(HomePage.userName,userName, "User Name");
		type(HomePage.password, password, "Password");
		click(HomePage.loginButton, "Login Button");
		waitUntilJQueryReady();
		//Session_ID will be used by developers to debug the non-reproducible bugs.
		getAttributeByValue(HomePage.dragonSessionID, "Dragon_Session_ID");
	}
	public void loginOKTA(String userName, String password) throws Throwable{
		if(isVisibleOnly(By.xpath("//input[@value='Send Push']"), "OKTA send push")){
			click(By.xpath("//a[contains(text(),'Sign Out')]"),"Sign Out");
			waitUntilJQueryReady();
		}
		if(isVisibleOnly(HomePage.userNamePUREOKTA, "HomePage login userName")){
			type(HomePage.userNamePUREOKTA,userName, "User Name");
			type(HomePage.passwordPUREOKTA, password, "Password");
			click(HomePage.signinButton, "Sign In Button");
			waitUntilJQueryReady();
		}
		
		//Session_ID will be used by developers to debug the non-reproducible bugs.
		//getAttributeByValue(HomePage.dragonSessionID, "Dragon_Session_ID");
		By prodLink = By.xpath("//a[contains(@href,'pureinsuranceprod')]");
		click(prodLink,"Prodcution Tile");
		String tileHandle = driver.getWindowHandle();
		System.out.println(tileHandle);
		Set<String> handles = driver.getWindowHandles();
		for(String handle:handles){
			System.out.println(handle);
			if(!handle.equalsIgnoreCase(tileHandle)){
				driver.switchTo().window(handle);
				System.out.println(driver.getTitle());
			}
		}
	}
	
	public void loginAsAgent(String brokerNumber, String userName, String password) throws Throwable{
		this.logout();
		type(HomePage.brokerNumber,brokerNumber, "Broker Number");
		type(HomePage.userName,userName, "User Name");
		type(HomePage.password, password, "Password");
		click(HomePage.loginButton, "Login Button");
	}
	public void logout(){
		try {
			if("PROD".equalsIgnoreCase(region)){
				String productionHandle = driver.getWindowHandle();
				String tileHandle = null;
				System.out.println(productionHandle);
				Set<String> handles = driver.getWindowHandles();
			
				for(String handle:handles){
					System.out.println("tab window handle:"+handle);
					if(handle.equalsIgnoreCase(productionHandle)){
						if(isVisibleOnly(HomePage.logout, "Logout Button")){
							click(HomePage.logout, "Logout Button");
							acceptAlert();//accept and logout
						}
					
						driver.close();	
					}else{
						tileHandle = handle;
					}
				}
				if(tileHandle!=null){
					driver.switchTo().window(tileHandle);
				}
			}
			if(isVisibleOnly(HomePage.logout, "Logout Button")){
				click(HomePage.logout, "Logout Button");
				acceptAlert();//accept and logout
			}
		} catch (Throwable e) {
			//Catched to avoid configuration failure
		}

	}
	public void agentLogout() throws Throwable{
		click(HomePage.logout, "Logout Button");
	}
	public void navigateToHome() throws Throwable {
		click(HomePage.homePageLink,"Home tab");
	}
	public void navigateToTasks() throws Throwable {
		click(HomePage.tasksPageLink, "Tasks tab");
	}
	public void navigateToAgentResources() throws Throwable  {
		click(HomePage.agentResourcesPageLink, "Agent Resources tab");
	}
	public void navigateToCustomers() throws Throwable  {
		click(HomePage.customersPageLink, "Customers tab");
	}
	public void navigateToQuotes() throws Throwable  {
		click(HomePage.quotesPageLink, "Quotes tab");
	}
	public void navigateToPolicies() throws Throwable  {
		click(HomePage.policiesPageLink, "Policies tab");
	}
	public void navigateToLocations() throws Throwable  {
		click(HomePage.locationsPageLink, "Location tab");
	}
	public void navigateToPartners() throws Throwable  {
		click(HomePage.partersPageLink, "Partners tab");
	}
	public void navigateToAutomation() throws Throwable  {
		click(HomePage.automationPageLink, "Automation tab");
	}

	public void navigateToAgentResources_Agent() throws Throwable  {
		click(HomePage.agentResourcesButton, "Agent Resources");
	}
	public void navigateToCustomers_Agent() throws Throwable  {
		click(HomePage.customersButton, "Customers");
	}
	public void navigateToQuotes_Agent() throws Throwable  {
		click(HomePage.newQuoteButton, "New Quote");
	}
}
