package com.pure.dragon;

import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.dragon.homeOwners.QuoteDetailsPage;
import com.pure.dragon.homeOwners.QuoteDetailsPageLib;
import com.pure.report.CReporter;

public class PolicyRenewalLib extends ActionEngine {
	public PolicyRenewalLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public void clickCreateRenewalBtn() throws Throwable {
		click(PolicyRenewalPage.createRenewalBtn, "Create Renewal Button");
		//Thread.sleep(1000);
		acceptAlert();
	}

	public void clickRenewalEntry() throws Throwable {
		waitForVisibilityOfElement(PolicyRenewalPage.renewalEntry,
				"waiting for Renewal entry");
		click(PolicyRenewalPage.renewalEntry, "Renewal Entry clicked");
	}

	public void clickPolicyImageTab() throws Throwable {
		click(PolicyRenewalPage.policyImageTab, "Policy Image Tab");
	}

	public void clickReviewChangesBtn() throws Throwable {
		click(PolicyRenewalPage.reviewChangesBtn, "Review Changes Button");
	}

	public void clickExitBtn() throws Throwable {
		click(PolicyRenewalPage.exitBtn, "Exit Button");
	}

	public void clickRenewedPremiumBtn() throws Throwable {
		click(PolicyRenewalPage.renewedPremiumBtn, "Renewed Premium Button");
	}

	public void clickSummaryTab() throws Throwable {
		click(PolicyRenewalPage.summaryTab, "Summary Tab");
	}

	public void clickUnderwritingAlertsTab() throws Throwable {
		click(PolicyRenewalPage.underwritingAlertsTab,
				"Underwriting Alerts Tab");
	}

	public void clickReviewReferalsBtn() throws Throwable {
		waitForVisibilityOfElement(PolicyRenewalPage.reviewReferals, "Review Referals Button");
		click(PolicyRenewalPage.reviewReferals, "Review Referals Button");
	}

	public void clickDocumentsTab() throws Throwable {
		click(PolicyRenewalPage.documentsTab, "Documents Tab");
	}

	public void clickMemberFlagTab() throws Throwable {
		click(PolicyRenewalPage.memberFlagTab, "MemberFlag Tab");
	}

	public void clickSubjectivitiesTab() throws Throwable {
		click(PolicyRenewalPage.subjectivitiesTab, "Subjectivities Tab");
	}

	public void clickDiaryTab() throws Throwable {
		click(PolicyRenewalPage.diaryTab, "Diary Tab");
	}

	public void clickAcceptBtn() throws Throwable {
		click(PolicyRenewalPage.acceptBtn, "Accept Button");
	}
	Hashtable<String, String> data;
	boolean testCyber;
	public void processRenewal(Hashtable<String, String> data, boolean testCyber) throws Throwable {
		this.data= data;
		this.testCyber = testCyber;
		if(isVisibleOnly(PolicyRenewalPage.processRenewal,"Process Renewal Button")){
			click(PolicyRenewalPage.processRenewal, "Process Renewal Button");
			waitForBodyTag();
		}else{
			clickRenewalEntry();   // If underwriter Alerts page appears, then click on renewal link to come back to summary tab
			clickPolicyImageTab();
			//WindstormHailDeductible is available in HO Renewal for only FL state.
			if("HO".equalsIgnoreCase(data.get("lobType"))){
				switch (data.get("state")) {
				case "FL":
					new QuoteDetailsPageLib(driver, reporter).selectWindstormHailDeductible(data.get("windstormHailDeductible"));
				}
			}			
			/*if(testCyber){
				//change limit to 100,000 and rate and verify on premium tab
				QuoteDetailsPageLib quoteDetailsPageLib = new QuoteDetailsPageLib(driver, reporter);
				quoteDetailsPageLib.selectCyberLimit(data.get("renewalCyberLimit"));//100,000 in Renewal
			}*/
			clickReviewChangesBtn();
			clickRenewedPremiumBtn();
			if(isVisibleOnly(PolicyRenewalPage.reviewReferals,"Review Referals Button")){
				clickReviewReferalsBtn();

				// overriding the under writer notes			
				UnderwriterReferralsPageLib uwReferralsPageLib = new UnderwriterReferralsPageLib(driver, reporter);
				uwReferralsPageLib.fillUnderwriterNotes(data.get("underwriterOverriddenNote"));
				uwReferralsPageLib.fillOverridden();
				if(isVisibleOnly(UnderwriterReferralsPage.underwriterName, "Underwriter")){
					uwReferralsPageLib.selectUnderwriter(data.get("uwName"));
				}
				// Accept the renewal
				new PolicyRenewalLib(driver, reporter).clickAcceptBtn();
				//rerun
				processRenewal(data, testCyber);
			}
			else{
				click(PolicyRenewalPage.processRenewal, "Process Renewal Button");
				waitForBodyTag();
			}
		}
	}

	public void clickExitTransactionBtn() throws Throwable {
		waitForVisibilityOfElement(PolicyRenewalPage.processCompleted,
				"Process Completed");
		click(PolicyRenewalPage.exitTransactionBtn, "Exit Transaction Button");
	}

	/* Function to click on Endorsement Tab */
	public void navigateToTransationOrEndorsementTab() throws Throwable {
		click(PolicyRenewalPage.transactionOrEndorsementTab,
				"TransactionOrEndorsement Tab");
	}
	private String getPolicyRenewalStatus() throws Throwable{
		return getAttributeValue(PolicyRenewalPage.processCompleted, "innerText");
	}
	public void verifyPolicyRenewalSuccess() throws Throwable{
		assertTextStringContains(getPolicyRenewalStatus(),"Completed");
	}
}
