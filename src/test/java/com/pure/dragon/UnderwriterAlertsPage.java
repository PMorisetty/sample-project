package com.pure.dragon;

import org.openqa.selenium.By;

public class UnderwriterAlertsPage {

	static By underwriterNotes;
	static By overriddenDropDown;
	static By commentsForBroker;
	static By underwriterName;
	
	static By saveChangesButton;
	static By approvedButton;
	static By notApprovedButton;
	static By reassignReferButton;
	static By exitButton;

	static {
		underwriterNotes = By.xpath("//table[@class='objectListTable']//textarea[@class='unFilledMandatoryField']");
		overriddenDropDown = By.xpath("//table[@class='objectListTable']//select[@title='Overridden?']");
		commentsForBroker = By.xpath("//div[@title='Underwriter Comments to Broker']/..//textarea");
		underwriterName = By.cssSelector("select[title='Assigned Underwriter']");
		
		saveChangesButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.508006')]");
		approvedButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.409405')]");
		notApprovedButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.409005')]");
		reassignReferButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.610714')]");
		exitButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.129205')]");
	}

}
