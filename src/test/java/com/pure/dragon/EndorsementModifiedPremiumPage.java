package com.pure.dragon;

import org.openqa.selenium.By;

public class EndorsementModifiedPremiumPage {
	static By coverageLabel;
	static By coverageLimitLabel;
	static By coverageDeductibleLabel;
	static By coveragePremiumLabel;

	static By reviewReferralsButton;
	static By premiumDetailsButton;
	static By moreChangesButton;
	static By discardEndorsementButton;
	static By saveAndExitButton;
	
	static By issueButton;
	static By completeRewriteTransactionBtn;


	static {
		//generic locators for all coverage
		coverageLabel = By.xpath("//div[contains(@class,'x-grid-view')]//tr//td[3]/div[text()='$']");
		coverageLimitLabel = By.xpath("//div[contains(@class,'x-grid-view')]//tr//td[3]/div[text()='$']/ancestor::tr[1]/td[4]/div");
		coverageDeductibleLabel = By.xpath("//div[contains(@class,'x-grid-view')]//tr//td[3]/div[text()='$']/ancestor::tr[1]/td[5]/div");
		coveragePremiumLabel = By.xpath("//div[contains(@class,'x-grid-view')]//tr//td[3]/div[text()='$']/ancestor::tr[1]/td[7]/div");

		reviewReferralsButton= By.xpath("//div[@id='footer']//a[contains(@href,'Action.433605')]");
		premiumDetailsButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.528607')]");
		moreChangesButton= By.xpath("//div[@id='footer']//a[contains(@href,'Action.689033')]");
		discardEndorsementButton= By.xpath("//div[@id='footer']//a[contains(@href,'Action.603514')]");
		saveAndExitButton= By.xpath("//div[@id='footer']//a[contains(@href,'Action.129205')]");
		
		issueButton = By.xpath("//div[@id='footer']//a[contains(@href,'Action.599002')]");
		completeRewriteTransactionBtn = By.xpath("//a[text()='>>> complete rewrite transaction']");
	}
}
