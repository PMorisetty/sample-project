package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class UnderwriterAlertsPageLib extends ActionEngine{
	public UnderwriterAlertsPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	/*Function to click on Save changes button*/
	public void clickSaveChangesButton() throws Throwable{
		click(UnderwriterAlertsPage.saveChangesButton, "Save Changes Button");
	}
	/*Function to click on Approved button*/
	public void clickApprovedButton() throws Throwable{
		click(UnderwriterAlertsPage.approvedButton, "Approved Button");
	}
	/*Function to click on Not Approved button*/
	public void clickNotApprovedButton() throws Throwable{
		click(UnderwriterAlertsPage.notApprovedButton, "Not Approved Button");
	}
	/*Function to click on Reassign/refer button*/
	public void clickReassignReferButton() throws Throwable{
		click(UnderwriterAlertsPage.reassignReferButton, "Reassign Refer Button");
	}
	/*Function to click on Exit button*/
	public void clickExitButton() throws Throwable{
		click(UnderwriterAlertsPage.exitButton, "Exit Button");
	}
}
