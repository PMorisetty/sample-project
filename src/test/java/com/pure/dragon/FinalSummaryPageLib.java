package com.pure.dragon;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class FinalSummaryPageLib extends ActionEngine{
	public FinalSummaryPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	// function to click on Request bind button
	public void clickRequestBind() throws Throwable {
			click(FinalSummaryPage.requestBindButton, "Request Bind Button");
			waitForBodyTag();
	}
}
