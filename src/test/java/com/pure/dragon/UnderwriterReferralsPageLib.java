package com.pure.dragon;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.pure.accelerators.ActionEngine;
import com.pure.report.CReporter;

public class UnderwriterReferralsPageLib extends ActionEngine{
	public UnderwriterReferralsPageLib(EventFiringWebDriver webDriver, CReporter reporter) {
		this.driver = webDriver;
		this.reporter = reporter;
	}
	public int counter=0;
	/*Function to click on Underwriter Referrals Tab*/
	public void clickUnderwriterReferralsTab() throws Throwable{
		click(UnderwriterReferralsPage.underwriterReferralTab, "Underwriter Referral Tab");
	}
	/*Function to verify Underwriter Referrals Tab is active tab*/
	public void verifyUnderwriterReferralTabActive() throws Throwable{
		String value = getAttributeValue(UnderwriterReferralsPage.underwriterReferralTabDiv, "class");
		assertTrue(value.equalsIgnoreCase("selected"), "Verifying Premium tab is active tab");
	}
	/*Function to fill underwriters notes in Underwriter Referrals Tab*/
	@SuppressWarnings({ "deprecation" })
	public void fillUnderwriterNotes(String value) throws Throwable{
		List<WebElement> elems = driver.findElements(UnderwriterReferralsPage.underwriterNotes);
		LOG.info("Available textarea ::: " + elems.size());
		for(WebElement elem: elems){
			try {
				if(elem.isDisplayed()){
					elem.sendKeys(value  + " : " + new Date().getDate() + "/" +  (new Date().getMonth() + 1) + "/" + (new Date().getYear()+1900));
					elem.sendKeys(Keys.TAB);
					WebElement tempElem = elem.findElement(By.xpath("../../../..")).findElement(By.cssSelector("select[title='Overridden?']"));
					new Select(tempElem).selectByVisibleText("Yes");
				}
			} catch (Exception e) {
				LOG.info("++++++++++++++++++++++++++++Catch Block Start+++++++++++++++++++++++++++++++++++++++++++");
				LOG.info("Class name" + getCallerClassName() + "Method name : " + getCallerMethodName());
				LOG.info(e.getMessage());
				LOG.info("++++++++++++++++++++++++++++Catch Block End+++++++++++++++++++++++++++++++++++++++++++");
				reporter.warningReport("underwriter note :: " + elem,
						"Unable to find :: " + elem);
		
			}
			if(isVisibleOnly(UnderwriterReferralsPage.saveChangesButton, "save changes")){
				click(UnderwriterReferralsPage.saveChangesButton, "save changes");
			}
			
		}
		elems = driver.findElements(UnderwriterReferralsPage.underwriterNotes);
		int numOfInvisibles=0;
		if(!elems.isEmpty()){
			for(WebElement elem: elems){
				if(!elem.isDisplayed()){
					numOfInvisibles++;
				}
			}
		}if(elems.size()!=numOfInvisibles){this.fillUnderwriterNotes(value);}
	}
	/*Function to select Yes in overridden drop down on Underwriter Referrals Tab*/
	public void fillOverridden() throws Throwable{
		List<WebElement> elems = driver.findElements(UnderwriterReferralsPage.overriddenDropDown);
		LOG.info("Available dropdown ::: " + elems.size());
		for(WebElement elem: elems){
			if(elem.isDisplayed()){
				Select s = new Select(elem);
				s.selectByVisibleText("Yes");
			}
		}
	}
	/*Function to complete underwriter questions on Underwriter Referrals Tab*/
	public void completeUnderwriterQuestions(String value) throws Throwable{
		this.fillUnderwriterNotes(value);
		this.fillOverridden();
	}
	/*Function to enter underwriters comments for broker on Underwriter Referrals Tab*/
	public void setCommentsForBroker(String comments) throws Throwable{
		type(UnderwriterReferralsPage.commentsForBroker, comments, "Underwriter comments for broken");
	}
	/*Function to select underwriter on Underwriter Referrals Tab*/
	public void selectUnderwriter(String name) throws Throwable{
		selectByVisibleText(UnderwriterReferralsPage.underwriterName, name, "Underwriter name");
	}
	/*Function to select underwriter on Underwriter Referrals Tab*/
	public void selectUnderwriter() throws Throwable{
		selectByIndex(UnderwriterReferralsPage.underwriterName, 1, "Underwriter at index 1");
	}
	/*Function to click on Save changes button*/
	public void clickSaveChangesButton() throws Throwable{
		click(UnderwriterReferralsPage.saveChangesButton, "Save Changes Button");
	}
	/*Function to click on Order credit score button*/
	public void clickOrderCreditScoreButton() throws Throwable{
		click(UnderwriterReferralsPage.orderCreditScoreButton, "Order Credit Score Button");
	}
	/*Function to click on Conditional button*/
	public void clickConditionalButton() throws Throwable{
		click(UnderwriterReferralsPage.conditionalButton, "Conditional Button");
	}
	/*Function to click on End submission button*/
	public void clickEndSubmissionButton() throws Throwable{
		click(UnderwriterReferralsPage.endSubmissionButton, "End Submission Button");
	}
	/*Function to click on Accept button*/
	public void clickAcceptButton() throws Throwable{
		click(UnderwriterReferralsPage.acceptButton, "Accept Button");
		driver.switchTo().alert().accept();
	}
	/*Function to click on Reassign/refer button*/
	public void clickReassignReferButton() throws Throwable{
		click(UnderwriterReferralsPage.reassignReferButton, "Reassign Refer Button");
	}
	/*Function to click on Exit button*/
	public void clickExitButton() throws Throwable{
		click(UnderwriterReferralsPage.exitButton, "Exit Button");
	}
	/*Function to verify UW question*/
	public void verifyUwQuestion(String question, String logName) throws Throwable{
		boolean flag = false;
		List<WebElement> elems = driver.findElements(UnderwriterReferralsPage.uwReferralQuestions);
		loop:
			for(int index = 0; index< elems.size();index++){
				String text = elems.get(index).getAttribute("innerText");
				if(text.contains(question)){
					flag = true;
					break loop;
				}
			}
		if(flag){
			reporter.SuccessReport("Verifying UW Question is present", logName + " question was present in list.");
		}else{
			reporter.failureReport("Verifying UW Question is present", logName + " question was not present in list.", driver);
		}
	}

	//Only valid for NB transactions
	public void verifyCyberUWRefferal(Hashtable<String, String> data) throws Throwable{
		if(data.get("cyberLimit") != null){
			if((data.get("isMoneyStolen") != null) && (data.get("isMoneyStolen").equalsIgnoreCase("YES"))){
				this.verifyCyberUWRefferal3();
			}else{
				switch(data.get("cyberLimit")){
				case "250,000": this.verifyCyberUWRefferal1();break;
				case "1,000,000": this.verifyCyberUWRefferal2();break;
				}
			}
		}
	}
	/*Function to verify Cyber - UW Referral question1*/
	public void verifyCyberUWRefferal1() throws Throwable{
		String question = "You have selected a limit of $250,000 of Fraud and Cyber Defense coverage. To be eligible for this limit we must have confirmation, via an email from the Broker, that the insured and their family members have not had money stolen or been deceived into making a payment or parting with something of value.";
		this.verifyUwQuestion(question, "UW referral question 1");
	}
	/*Function to verify Cyber - UW Referral question2*/
	public void verifyCyberUWRefferal2() throws Throwable{
		String question = "You have selected a limit of $1 Million of Fraud and Cyber Defense coverage. To be eligible for this limit we must have confirmation, via an email from the Broker, that the insured and their family members have not had money stolen or been deceived into making a payment or parting with something of value. We must also have confirmation prior to bind of an active subscription to a Cyber Security Monitoring Service.";
		this.verifyUwQuestion(question, "UW referral question 2");
	}
	/*Function to verify Cyber - UW Referral question3*/
	public void verifyCyberUWRefferal3() throws Throwable{
		String question = "You have selected a higher limit of Fraud and Cyber Defense Coverage, and reported that a loss had previously occurred. Please provide details of the loss and submit to Underwriting for consideration.";
		this.verifyUwQuestion(question, "UW referral question 3");
	}

	//---------------Watercraft underwritter referrals---------------------------
	// New UW rules
	public static boolean uwReferralEligibilityForAdditionalWaterNavigated = false;
	public static boolean uwReferralEligibilityCoverageUninsuredBoatersExceeds5000000 = false;
	public static boolean uwReferralEligibilityProtectionIndemnityExceeds5000000 = false;
	public static boolean uwReferralEligibilityNoHurricanSeverPlan = false;
	public static boolean uwReferralEligibilityForAdditional$$$$$ = false;
	public static String uwReferralEligibilityForHullLimit = null;
	public static boolean uwReferralEligibilityForVesselLength = false;
	public static boolean uwReferralEligibilityForAge = false;
	public static boolean uwReferralEligibilityForRelationToCaptain = false;
	public static boolean uwReferralEligibilityForWCMorethan15yearsOld = false;
	public static String yearOfLastSurvey = null;	
	public static String yearOfHull = null;
	public static boolean uwReferralEligibilityForIncidents3AndAbove = false;
	public static boolean uwReferralEligibilityForNamedInsuredOtherLegalEntity = false;
	public static String insuranceScore = null;
	public static String priorLossDate = null;
	public static String hurricaneGradePlan = null;
	public static String vesselLength = null;
	public static boolean uwReferralEligibilityForUsageAntiqueCollectors = false;
	public static String brokersLicensedState=null;
	public static String yearOfPurchage = null;
	public static boolean uwReferralEligibilityForOperatorExp3yearsExp = false;
	public static String operatorDOB=null;
	public static String insuredDOB=null;
	public static boolean eligibilityOptionalCoverage=false;
	public static boolean eligibilityMaxEngineSpeed=false;
	public static int eligibilityMoreThan2AdditionalOwners=0;
	public static boolean memberOHouseHold;
	public static String accidentType,accidentDate;
	
	


	public void verifyUWReferralsForWC(Hashtable<String, String> data) throws Throwable{
		int YEAR = Calendar.getInstance().get(Calendar.YEAR);
		System.out.println(YEAR);

		ArrayList<String> uWReferralPresent = new ArrayList<String>();
		ArrayList<String> uWReferralNotPresent = new ArrayList<String>();
		/*============================================>*/
		//common
		uWReferralPresent.add("Watercraft mooring Zip Code not found. Please refer to Underwriting.");
		uWReferralPresent.add("Please refer to Underwriting to run CLUE and MVR and confirm other lines.");
		/*============================================>*/
		if(uwReferralEligibilityForAdditionalWaterNavigated){
			uWReferralPresent.add("An international navigational territory has been selected. Please refer to underwriting.");
			uwReferralEligibilityForAdditionalWaterNavigated = false;
		}else{
			uWReferralNotPresent.add("An international navigational territory has been selected. Please refer to underwriting.");
		}
		/*============================================>*/
		if(checkBackdated(data.get("effectiveDate"))){
			uWReferralPresent.add("You have requested a Back-dated policy effective date. Please refer to Underwriting.");
		}else{uWReferralNotPresent.add("You have requested a Back-dated policy effective date. Please refer to Underwriting.");}
		/*============================================>*/
		if(eligibilityOptionalCoverage){
			uWReferralPresent.add("An optional underwriter-only optional coverage or exclusion has been selected. Please confirm that the selected endorsement is accurate and appropriate.");
			eligibilityOptionalCoverage = false;
		}else{uWReferralNotPresent.add("An optional underwriter-only optional coverage or exclusion has been selected. Please confirm that the selected endorsement is accurate and appropriate.");	}
		/*============================================>*/
		if(uwReferralEligibilityCoverageUninsuredBoatersExceeds5000000){
			uWReferralPresent.add("Uninsured Boaters limits exceeds $5m. Please refer to underwriting.");
			uwReferralEligibilityCoverageUninsuredBoatersExceeds5000000 = false;
		}else{uWReferralNotPresent.add("Uninsured Boaters limits exceeds $5m. Please refer to underwriting.");}
		/*============================================>*/
		if(uwReferralEligibilityProtectionIndemnityExceeds5000000){
			uWReferralPresent.add("Protection and Indemnity limit exceeds $5m. Please refer to underwriting.");
			uwReferralEligibilityProtectionIndemnityExceeds5000000 = false;
		}else{uWReferralNotPresent.add("Protection and Indemnity limit exceeds $5m. Please refer to underwriting.");}
		/*============================================>*/
		if(uwReferralEligibilityNoHurricanSeverPlan){
			uWReferralPresent.add("Please submit a completed hurricane plan questionnaire to your underwriter for review.");
			uwReferralEligibilityNoHurricanSeverPlan = false;
		}else{
			/*Not applicable since UI changed.  US5178 created for this change request.
			uWReferral.add("A hurricane plan is required in this navigational area. Please submit a completed hurricane plan questionnaire to your underwriter for review.");
			*/
			uWReferralNotPresent.add("Please submit a completed hurricane plan questionnaire to your underwriter for review.");
		}
		/*============================================>*/
		if(uwReferralEligibilityForHullLimit!=null){
			if(Integer.valueOf(uwReferralEligibilityForHullLimit)>25000
					&&	Integer.valueOf(uwReferralEligibilityForHullLimit)<1000000 ){
				uWReferralPresent.add("Agreed Value of Crafts > $500k.");
				UnderwriterReferralsPageLib.uwReferralEligibilityForHullLimit = null;
			}else if(Integer.valueOf(uwReferralEligibilityForHullLimit) > 1000000){
				uWReferralPresent.add("The hull value of the watercraft is greater than 1,000,000.");
				UnderwriterReferralsPageLib.uwReferralEligibilityForHullLimit = null;
			}else{
				uWReferralNotPresent.add("Agreed Value of Crafts > $500k.");
				uWReferralNotPresent.add("The hull value of the watercraft is greater than 1,000,000.");
			}
		}
		/*============================================>*/
		if(uwReferralEligibilityForVesselLength 
				&& uwReferralEligibilityForAge){
			uWReferralPresent.add("Operator under 25 years of age is selected. Please refer to underwriting.");
			uwReferralEligibilityForVesselLength = uwReferralEligibilityForAge = false;
		}else{uWReferralNotPresent.add("Operator under 25 years of age is selected. Please refer to underwriting.");}
		/*============================================>*/
		if(uwReferralEligibilityForRelationToCaptain){
			uWReferralPresent.add("Please provide captain's resume.");
			uwReferralEligibilityForRelationToCaptain = false;
		}else{uWReferralNotPresent.add("Please provide captain's resume.");	}
		/*============================================>*/
		if(uwReferralEligibilityForWCMorethan15yearsOld){
			uWReferralPresent.add("The watercraft is more than 15 years old, refer to underwriting.");
			uwReferralEligibilityForWCMorethan15yearsOld = false;
		}else{uWReferralNotPresent.add("The watercraft is more than 15 years old, refer to underwriting.");}
		/*============================================>*/
		if(uwReferralEligibilityForNamedInsuredOtherLegalEntity){
			uWReferralPresent.add("Insured is Other Legal Entity. Please confirm who owns the entity, whether it is operated for profit, employs staff or owns other property.");
			uwReferralEligibilityForNamedInsuredOtherLegalEntity = false;
		}else{uWReferralNotPresent.add("Insured is Other Legal Entity. Please confirm who owns the entity, whether it is operated for profit, employs staff or owns other property.");}

		/*============================================>*/
		/*Prior Loss date less than 5 year*/
		if(priorLossDate!=null && (convertDOBtoAge(priorLossDate)<5)){
			uWReferralPresent.add("A member of the household has been involved in a watercraft loss/claim");
			priorLossDate = null;
		}else{uWReferralNotPresent.add("A member of the household has been involved in a watercraft loss/claim");}
		/*============================================>*/
		/* This has been moved to userstory US5178 
		if(brokersLicensedState!=null && (!data.get("state").equalsIgnoreCase(brokersLicensedState))){
			uWReferral.add("The broker is not licensed in the watercraft's registered state");
			brokersLicensedState = null;
		}else{
			uWReferralNotPresent.add("The broker is not licensed in the watercraft's registered state");
		}*/
		/*============================================>*/
		/*should be tested by manual QA. Rule appears only when Rate is not clicked
		if(data.get("state").equalsIgnoreCase("KY")){
			uWReferralPresent.add("Municipality tax reports were not received therefore taxes were not calculated. Please contact Member Services.");
		}else{uWReferralNotPresent.add("Municipality tax reports were not received therefore taxes were not calculated. Please contact Member Services.");}
		*/
		/*============================================>*/
		//Get Insurance score from UI and compare. Commented as updated setting insurance score logic is in UI side. 
		/*if(insuranceScore!=null && insuranceScore!=""
				&& Integer.valueOf(insuranceScore)<700){
				uWReferralPresent.add("Member may not qualify - please refer to underwriting.");
				insuranceScore = null;
		}else{uWReferralNotPresent.add("Member may not qualify - please refer to underwriting.");}
		*/
		//*============================================>*/
		if(	(yearOfLastSurvey!=null && yearOfHull!=null && vesselLength!=null) 
				&&(YEAR-Integer.valueOf(yearOfLastSurvey)>5 && convertDOBtoAge(yearOfHull)>15 && Integer.valueOf(vesselLength)>36)){
			uWReferralPresent.add("A survey is required for the watercraft.");
			yearOfLastSurvey = null;
			yearOfHull=null;
			//Script 2: 
		}else{uWReferralNotPresent.add("A survey is required for the watercraft.");}
		/*============================================>*/
		if(accidentDate!=null && accidentType!=null
				&& (accidentType.equalsIgnoreCase("Driving Under the Influence")) 
				&& (convertDOBtoAge(accidentDate))<5){
			uWReferralPresent.add("1 or more DUI's in last 5 years. Please refer to Underwriting.");
		}else{uWReferralNotPresent.add("1 or more DUI's in last 5 years. Please refer to Underwriting.");}
		/*============================================>*/
		if(uwReferralEligibilityForIncidents3AndAbove){
			uWReferralPresent.add("3 or more incidents, accidents or convictions in last 3 years. Please refer to Underwriting.");
			uwReferralEligibilityForIncidents3AndAbove = false;
		}else{uWReferralNotPresent.add("3 or more incidents, accidents or convictions in last 3 years. Please refer to Underwriting.");}
		/*============================================>*/
		if(uwReferralEligibilityForUsageAntiqueCollectors){
			uWReferralPresent.add("This is a collectors boat, refer to underwriting.");
			uwReferralEligibilityForUsageAntiqueCollectors = false;
			//row 14
		}else{uWReferralNotPresent.add("This is a collectors boat, refer to underwriting.");}	
		/*============================================>*/
		if(uwReferralEligibilityForOperatorExp3yearsExp){
			uWReferralPresent.add("Listed Operator may not have adequate boating experience for this size vessel.  Please confirm prior boats owned and operated and refer to underwriting.");
			uwReferralEligibilityForOperatorExp3yearsExp = false;
		}else{uWReferralNotPresent.add("Listed Operator may not have adequate boating experience for this size vessel.  Please confirm prior boats owned and operated and refer to underwriting.");}
		/*============================================>*/
		if(operatorDOB!=null &&  vesselLength!= null){
			if(Integer.valueOf(vesselLength)>=27 && (convertDOBtoAge(insuredDOB)>75 || convertDOBtoAge(operatorDOB)>75)){
				/*JP: This referral is disable in Production for all watercraft. We shouldn't enable it.*/
				uWReferralPresent.add("Operator over 75 years of age is selected. Please refer to underwriting.");
				operatorDOB = null;
			}else if(operatorDOB!=null	&& (convertDOBtoAge(operatorDOB)<25)){
				uWReferralPresent.add("Operator under 25 years of age is selected. Please refer to underwriting.");
				operatorDOB = null;
			}else{			
				uWReferralNotPresent.add("Operator under 25 years of age is selected. Please refer to underwriting.");
				uWReferralNotPresent.add("Operator over 75 years of age is selected. Please refer to underwriting.");
			}
		}
		/*============================================>*/
		if(eligibilityMaxEngineSpeed){
			uWReferralPresent.add("Max design speed of vessel selected exceeds 60mph.  Please refer to underwriting for review.");
			eligibilityMaxEngineSpeed = false;
		}else{uWReferralNotPresent.add("Max design speed of vessel selected exceeds 60mph.  Please refer to underwriting for review.");}
		/*============================================>*/
		if(eligibilityMoreThan2AdditionalOwners>=2){
			uWReferralPresent.add("More than one additional owner is listed.");
			eligibilityMoreThan2AdditionalOwners=0;
		}else{uWReferralNotPresent.add("More than one additional owner is listed.");}
		/*============================================>*/
		if(memberOHouseHold){
			uWReferralPresent.add("Named Insured has a high profile occupation. Please refer to Underwriting.");
			memberOHouseHold=false;
		}else{uWReferralNotPresent.add("Named Insured has a high profile occupation. Please refer to Underwriting.");}
				
		//*************************************************
		//***********Deleted rules*************************		
		String state=data.get("state");
		if(hurricaneGradePlan !=null &&("NC, SC, GA, FL, AL, MS, LA, TX").contains(state) && !hurricaneGradePlan.contains("A")){
			uWReferralNotPresent.add("Watercraft is located in a hurricane prone area and the hurricane plan may not be adequate - please refer to underwriting.");
			hurricaneGradePlan=null;
		}/*============================================>*/
		if(	vesselLength !=null	&& state.equalsIgnoreCase("MI") && Integer.valueOf(vesselLength)<25){
			uWReferralNotPresent.add("Watercraft in Michigan must be greater than 24 ft in length to be eligible.");
			vesselLength = null;
		}/*============================================>*/
		if((!uwReferralEligibilityForAdditionalWaterNavigated) && vesselLength !=null && !state.equalsIgnoreCase("WA")				
				&& Integer.valueOf(vesselLength)<26){
			uWReferralNotPresent.add("Navigational Waters selected include hurricane prone areas of Gulf and Southeast and the hurricane plan may not be adequate - please refer to underwriting");
			vesselLength = null;
		}/*============================================>*/
		if(	yearOfPurchage !=null && (YEAR-Integer.valueOf(yearOfPurchage))<2){
			uWReferralNotPresent.add("Please provide details of prior boats owned, and refer to underwriting.");
			vesselLength = null;
		}/*============================================>*/

		//Verify for present rules
		verifyUwQuestion(uWReferralPresent,true);
		//Verify for deleted referrals
		verifyUwQuestion(uWReferralNotPresent, false);
		verifyUwQuestion(deletedReferrals(), false);

	}
	
	public void verifyUWReferralForRenewal() throws Throwable{

		ArrayList<String> uWReferralPresent = new ArrayList<String>();
		/*============================================>*/
		//common
		uWReferralPresent.add("Premium Adjustment/Manuscript endorsement has been selected");
		uWReferralPresent.add("Claims flag set");		
		
		//Verify for present rules
		verifyUwQuestion(uWReferralPresent,true);
	}
	//---------------END WC underwritter referrals---------------------------
	/*function to fill FL, LA, & NC state */
	public void fillUnderwriterDetailsForFL(Hashtable<String, String> data) throws Throwable {
		switch(data.get("state")){
		case "LA":
		case "FL":
		case "NC":
		case "SC":
		case "AL":
			this.clickUnderwriterReferralsTab();
			this.fillUnderwriterNotes(data
					.get("underwriterOverriddenNote"));
			this.fillOverridden();
			// Select Underwriter
			if("PROD".equalsIgnoreCase(region)){
				this.selectUnderwriter(data.get("uwName_prod"));
			}else{
				this.selectUnderwriter(data.get("uwName"));
			}
			// Comments for broker
			this.setCommentsForBroker(data.get("uwComments"));
			// Accept quote
			this.clickAcceptButton();
		}

	}

	public void clickApprovedButton() throws Throwable{
		click(UnderwriterReferralsPage.approvedBtn, "Approved Button");
	}

	public ArrayList<String> deletedReferrals() throws Throwable{
		ArrayList<String> deletedUWReferrels=new ArrayList<String>();
		deletedUWReferrels.add("3 or more watercrafts policy.  Please refer to Underwriting.");
		deletedUWReferrels.add("Boat is <26ft in length and registered in WA but berthed outside of Puget Sound and Western Territories. Please refer to Underwriting");
		deletedUWReferrels.add("Boating Education is \"Other\" Please provide information.");
		deletedUWReferrels.add("Charter Coverage has been selected, please provide detail.");
		deletedUWReferrels.add("Charter Coverage has been selected, please provide detail.");
		deletedUWReferrels.add("Driver license status is revoked or suspended. Please refer to Underwriting.");
		deletedUWReferrels.add("Listed Operator has less than 5 years prior boating experience.  Please provide information.");
		deletedUWReferrels.add("Only one Primary Berthing Location allowed for the Vessel");
		deletedUWReferrels.add("Please refer to underwriting - member may not qualify");
		deletedUWReferrels.add("Please refer to underwriting - member may not qualify");
		deletedUWReferrels.add("Pull MVR for driver(s) < 25)");
		deletedUWReferrels.add("Pull MVR for driver(s) > 75");
		deletedUWReferrels.add("Pull MVR's at Renewal for Drivers with accidents/violations.");
		deletedUWReferrels.add("The P&I or Uninsured Boaters limit selections do not match for all watercrafts listed on the policy. Please revise coverage selections so these limits will match for all watercrafts listed on the policy.");
		deletedUWReferrels.add("The P&I or Uninsured Boaters limit selections do not match for all watercrafts listed on the policy. Please revise coverage selections so these limits will match for all watercrafts listed on the policy.");				

		return deletedUWReferrels;
	}

	/**
	 * Test requested rule displayed in UI or deleted 
	 * @param presentNOTPresent True->rule should be displayed, False->rule should not be displayed
	 * @return 
	 * @throws Throwable 
	 */
	public void verifyUwQuestion(ArrayList<String>rules, boolean presentNOTPresent) throws Throwable{
		ArrayList<String> listedRules = new ArrayList<String>();
		List<WebElement> elems = driver.findElements(UnderwriterReferralsPage.uwReferralQuestions);
		String allListedRules = "";
		for(int index = 0; index< elems.size();index++){
			String listedRule =elems.get(index).getAttribute("innerText"); 
			listedRules.add(listedRule.toLowerCase().replace(" ",""));
			allListedRules = allListedRules + "------"+listedRule + System.lineSeparator();
		}
		if(counter==0){
			reporter.SuccessReport("These are listed rules in UI", allListedRules);
			counter++;
		}

		/* Delete if not needed
		 * for(int i=0;i<rules.size();i++){
			String rule = rules.get(i);
			if(listedRules.contains(rule.toLowerCase())){
				//verifying presence of rule
				if(presentNOTPresent){
					reporter.SuccessReport("Verifying UW Question is present", rule + " question was present in list.");
				}else if(!presentNOTPresent){
					reporter.softFailureReport("Verifying UW Question is NOT present", rule + " question was present in list.", driver);
				}
			}else{
				//verifying deletion of rule from UI
				if(!presentNOTPresent){
					reporter.SuccessReport("Verifying UW Question is NOT present", rule + " question was NOT present in list.");
				}else if(presentNOTPresent){
					reporter.softFailureReport("Verifying UW Question is present", rule + " question was NOT present in list.", driver);
				}
			}
		}*/
		if(presentNOTPresent){
			for(int i=0;i<rules.size();i++){
				String rule = rules.get(i);
				if(listedRules.contains(rule.toLowerCase().replace(" ",""))){
					reporter.SuccessReport("Verifying UW Question is present", rule + " question was present in list.");
				}else
					reporter.softFailureReport("Verifying UW Question is present", rule + " question was NOT present in list.", driver);
			}
		}
		if(!presentNOTPresent){
			for(int i=0;i<rules.size();i++){
				String rule = rules.get(i);
				if(listedRules.contains(rule.toLowerCase().replace(" ",""))){
					reporter.softFailureReport("Verifying UW Question is NOT present", rule + " question was present in list.", driver);						
				}else
					reporter.SuccessReport("Verifying UW Question is NOT present", rule + " question was NOT present in list.");
			}
		}

	}
	public boolean checkBackdated(String sDate) throws Throwable {
		/*Calendar calendar = Calendar.getInstance();						
		calendar.add(Calendar.DATE,0);
		Date today=java.time;*/
		
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date today = new Date();
		Date todayWithZeroTime = formatter.parse(formatter.format(today));
		System.out.println("today:"+todayWithZeroTime);
		
		Date d=new SimpleDateFormat("MM/dd/yyyy").parse(sDate);
		System.out.println("effDate:"+d);
		
		return todayWithZeroTime.after(d);
		
	}
}
