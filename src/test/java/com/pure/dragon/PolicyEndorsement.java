 package com.pure.dragon;

import org.openqa.selenium.By;

public class PolicyEndorsement {

	public static By chooseTransactionType;
	public static By selectEndorsementType;
	public static By newTransEffectiveDate;
	public static By permiumYes;
	public static By permiumNo;
	public static By notes;
	public static By nextBtn;
	public static By cancellationRequestedBy;
	public static By description;
	public static By cancelMethod;
	public static By cancelReason;
	public static By proceessBtn;
	public static By modifySequenceBtn;
	public static By docGenStatus;
	public static By newBtn;
	public static By claimLossPostCancellation;
	public static By reinstmtReason;
	public static By reinStmtStatus;
	public static By transactionOrEndorsementTab;
	static By customerNameLink;
	static By nbTransLink;
	static By subjectivitiesTab;
	static By errorMesage;
	static {
		chooseTransactionType = By
				.xpath("//select[contains(@title,'Transaction Type')]");
		selectEndorsementType = By
				.xpath("//select[contains(@title,'Endorsement')]");
		newTransEffectiveDate = By
				.xpath("//input[contains(@title,'MM/DD/YYYY')]");
		notes = By.xpath("//input[contains(@title,'Notes')]");
		nextBtn = By.xpath("//a[contains(text(),'next')]");
		cancellationRequestedBy = By
				.xpath("//select[contains(@title,'Cancellation Requested by:')]");
		description = By
				.xpath("//input[contains(@title,'Description (THIS WILL APPEAR ON BILL)') and @class='unFilledMandatoryField']");
		cancelMethod = By
				.xpath("//select[contains(@title,'Cancellation Type')]");
		cancelReason = By
				.xpath("//select[contains(@title,'Cancellation Reason')]");
		modifySequenceBtn = By
				.xpath("//a[contains(text(),'modify out-of-sequence')]");
		proceessBtn = By.xpath("//a[contains(text(),'process')]");
		docGenStatus = By
				.xpath("//table[@class='x-grid-table x-grid-table-resizer']/tbody/tr/td[6]/div");
		newBtn = By.xpath("//a[contains(text(),'>> new')]");
		claimLossPostCancellation = By
				.xpath("//select[contains(@title,'Claims/Losses post cancellation?')]");
		reinstmtReason = By
				.xpath("//select[contains(@title,'Reinstatement Reason')]");
		reinStmtStatus = By
				.xpath("//div[contains(text(),'Transaction Status')]/following-sibling::div/div");
		transactionOrEndorsementTab = By.xpath(".//a/span[contains(text(),'transactions/endorsements')]");
		customerNameLink = By.xpath("//div[@id='headerBar']//a[contains(@href,'\"118101\"')]");
		nbTransLink = By.xpath("//a[@title='Transaction Type'][contains(text(), 'New Business')]");
		subjectivitiesTab = By.xpath("//span[contains(text(), 'subjectivities')]");
		errorMesage =By.xpath("//font[.='THIS IS NOT A BINDABLE QUOTE. This change requires Company review for final processing.']");
		
		
		
	}

}
