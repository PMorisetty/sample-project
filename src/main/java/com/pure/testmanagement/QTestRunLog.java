package com.pure.testmanagement; 

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.pure.testmanagement.QTestAPI_Bridge.tcResult;
import com.pure.utilities.Zip;

public class QTestRunLog {
		
	private JSONObject jsTargetTestRun = null;
	private JSONObject jsTRLog = new JSONObject();		
	private String logNotes = "";
	private final static Object lock = new Object();
	
	public QTestRunLog() {
		String jsonPayloadPath = System.getProperty("user.dir")
				+ "/src/main/java/com/pure/testmanagement/AddTestRunLog.json";
		JSONObject jsTRLog = readJsonFile(jsonPayloadPath);
	}
	
	public void setTargetTestRun(JSONObject jsTR) throws Exception {
		if(jsTR != null) {
			jsTargetTestRun = jsTR;
			updateTCVersionId();			
		}else {
			throw new NullPointerException();
		}
	}
	
	public void resetLogger() {		
		jsTRLog.put("test_case_version_id", 0);
		jsTRLog.put("note", "");
		jsTRLog.put("exe_start_date", "");
		jsTRLog.put("exe_end_date", "");
		jsTRLog.put("attachments", "[]");
		jsTRLog.put("status", "{}");
		
		logNotes="";
		jsTargetTestRun = null;
	}
	
	public JSONObject getTargetTestRun() {
		return jsTargetTestRun;
	}	
	
	public JSONObject getTestRunLog() {
		System.out.println(jsTRLog.toString());
		return jsTRLog;
	}
	
	private JSONObject updateTCVersionId(){
		Long tcId = (Long) jsTargetTestRun.get("test_case_version_id");
		jsTRLog.put("test_case_version_id", tcId);
		return jsTRLog;
	}
	
	public JSONObject setExecutionStatus(tcResult status){
		
		JSONObject jsStatus = new JSONObject();		
		switch (status){
			case PASSED: jsStatus.put("id" , QTestAPI_Bridge.PASSED); break;
			case FAILED: jsStatus.put("id" , QTestAPI_Bridge.FAILED); break;
			case INCOMPLETE: jsStatus.put("id" , QTestAPI_Bridge.INCOMPLETE); break;
			case BLOCKED: jsStatus.put("id" , QTestAPI_Bridge.BLOCKED); break;
			case UNEXECUTED: jsStatus.put("id" , QTestAPI_Bridge.UNEXECUTED); break;
		}
		
		jsTRLog.put("status", jsStatus);    			
		return jsTRLog;
	}
	
	public JSONObject setStartTime(String startTime) {
		jsTRLog.put("exe_start_date", startTime);
		return jsTRLog;
	}

	public JSONObject setEndTime(String startTime) {
		jsTRLog.put("exe_end_date", startTime);
		return jsTRLog;
	}
	
	public JSONObject appendNote(String exeNote) {
		logNotes = logNotes+exeNote;
		jsTRLog.put("note", logNotes);
		return jsTRLog;
	}	
	
	public JSONObject addAttachment(String filePath, String fileContentType) throws Exception{
		
		File attachFile = new File(filePath);
		
		if(!attachFile.exists()){
			//throw new Exception(filePath + " doesnot exists");
			System.out.println("File: " + filePath + " doesn't exist");
		}else{
    		JSONObject jsAttachment = new JSONObject();
			jsAttachment.put("name", attachFile.getName());
			jsAttachment.put("content_type", fileContentType); //jsAttachment.put("content_type", "application/octet-stream");    		
						
			FileInputStream fis = new FileInputStream(attachFile);
			byte imgBytes[] = new byte[(int) attachFile.length()];
			fis.read(imgBytes);
			String encodedFile = Base64.getEncoder().encodeToString(imgBytes);		
			jsAttachment.put("data", encodedFile);		

			JSONArray jsAttachs = new JSONArray();
			if(jsTRLog.containsKey("attachments")) {
				jsAttachs = (JSONArray) jsTRLog.get("attachments");
			}
			jsAttachs.add(jsAttachment);
			jsTRLog.put("attachments",jsAttachs);			
//			JSONArray jsAttachs = (JSONArray) jsTRLog.get("attachments");
//			jsAttachs.add(jsAttachment);
		}
		return jsTRLog;
	}
	
   	private JSONObject readJsonFile(String jsonFilePath) {
		JSONObject jsonObject = null;
		try {
			/**
			 * START-->>Reading input data from JSON file & Closing stream
			 */
			FileReader fr = new FileReader(jsonFilePath);
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(fr);
			jsonObject = (JSONObject) obj;
			fr.close();
			/**
			 * END-->>Reading input data from JSON file & Closing stream
			 */
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(jsonFilePath + ".json file not found");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}	
   	
   	public void addResultAttachment(String filePath, String fileContentType){
   		synchronized (lock) {
   			try {
   				String destFolder = "result.zip";
   				String outputFileName = "forQtestResultUpload/temp1";
   				File source = new File("forQtestResultUpload/temp");
   				File dest = new File(outputFileName);
   				File destFile = new File(outputFileName + "/result.html");
   				if(dest.exists()){
   					FileUtils.cleanDirectory(dest);
   					FileUtils.forceDelete(dest);
   					FileUtils.forceMkdir(dest);
   				}
   				FileUtils.copyDirectory(source, dest);
   				FileUtils.copyFile(new File(filePath), destFile);
   				Zip.zipFolder(outputFileName, destFolder);
   				addAttachment(destFolder, fileContentType);
   			} catch (Exception e) {
   				System.out.println("Add result attachement failed due to ::" + e.getMessage());
   			}
   		}

   	}
   	
}