/**
 * TestRail API binding for Java (API v2, available since TestRail 3.0)
 * <p>
 * Learn more:
 * <p>
 * http://docs.gurock.com/testrail-api2/start
 * http://docs.gurock.com/testrail-api2/accessing
 * <p>
 * Copyright Gurock Software GmbH. See license.md for details.
 */

package com.pure.testmanagement;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONObject;

public class QTestSamp {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("YYYY-MM-dd'T'HH:MM:ss.SSS'Z'");
		QTestAPI_Bridge qTest = new QTestAPI_Bridge("pureinsurance");
		qTest.authorize("sgajula@pureinsurance.com", "Priv0516@");
		qTest.loadProject("PURE Dragon Product Changes");
				
		//JSONObject jsCycleInfo = qTest.getTestcyclefromTestRunID("49226931");
		JSONObject jsCycleInfo = qTest.getProjectElement("releases| test-cycles", "201807 - July Release | qTest Smoke Integration");		
		System.out.println("Printing Pjson"+jsCycleInfo);
		
		//qTest.getExistingTestRun(jsCycleInfo, "TC-3");
		QTestRunLog tcLog = qTest.startTestRunLog(jsCycleInfo, "TC-2175","API Test run TC-2175");
		tcLog.setStartTime(dateFormatter.format(new Date()));
		tcLog.setExecutionStatus(QTestAPI_Bridge.tcResult.INCOMPLETE);
		tcLog.appendNote("Hello... FirstNote, ");
		tcLog.appendNote("SecondNote, ");
		tcLog.appendNote("ThirdNote, ");
		tcLog.appendNote("  ....Last Note");
		//tcLog.addAttachment("C:\\PureInsurance\\EclipseWorkspace\\pureInsurance\\SampleSS.jpg",
		//		"application/octet-stream");
		Thread.sleep(10000);
		tcLog.setEndTime(dateFormatter.format(new Date()));
		
		qTest.postTestRunLog(tcLog);
	}

	public static void TestQTest() throws Exception {

		// QTestAPI qTest = new QTestAPI("pureinsurance");
		// qTest.authorize("sgajula@pureinsurance.com", "Priv0516@");
		//
		// //First Approach: Individual methods and test run id
		// JSONObject jsProj =qTest.getProject("z_QConnect - Sample Project");
		// int ProjId = Integer.valueOf(jsProj.get("id").toString());
		// System.out.println("z_QConnect - Sample Project : " + jsProj);
		//
		// JSONObject jsRel = qTest.getRelease(ProjId, "Release 2.0");
		// int relId = Integer.valueOf(jsRel.get("id").toString());
		// System.out.println("z_QConnect - Sample Project | Release 2.0 : " +
		// jsRel.toString());
		//
		// JSONObject jsTestCycle = qTest.getTestCycle(ProjId, relId, "release",
		// "Hardening Sprint");
		// int cycleId = Integer.valueOf(jsTestCycle.get("id").toString());
		// System.out.println("Test Cycle : " + jsTestCycle.toString());
		//
		////// JSONObject jsTestSuite = qTest.getTestSuite(ProjId, cycleId,
		// "test-cycle", "Hardening Sprint");
		////// int suiteId = Integer.valueOf(jsTestSuite.get("id").toString());
		////// System.out.println("Test Cycle Id " + suiteId);
		//
		// JSONObject jsRunDetails = qTest.getTestRun(ProjId, cycleId, "test-cycle",
		// "Our Solutions section");
		// int runId = Integer.valueOf(jsRunDetails.get("id").toString());
		// int tcId =
		// Integer.valueOf(jsRunDetails.get("test_case_version_id").toString());
		// System.out.println("Test Run details- " + jsRunDetails.toString());
		//
		// QTestRunLog tcLog = new QTestRunLog(tcId);
		// tcLog.updateStatus(QTestAPI.tcResult.PASSED);
		// tcLog.updateExecutionDetails("2018-05-28T18:09:30.034Z",
		// "2018-05-28T18:15:30.034Z", "Automation Sample Log");
		// tcLog.addAttachment("C:\\Users\\E002258\\Downloads\\MemoryLeak_HeapSize_iMacSlave.png",
		// "application/octet-stream");
		// //System.out.println(tcLog.jsTCLog.toString());
		// JSONObject jsTCResult = qTest.addTestLog(ProjId, runId, tcLog);
		// System.out.println(jsTCResult.toString());
		//
		//
		// //Second Approach - individual methods & test case Id
		// //Get test case link
		// String testRunsURI = "";
		// JSONArray jsLinks = (JSONArray) jsTestCycle.get("links");
		// for(int lCntr=0; lCntr<jsLinks.size(); lCntr++){
		// JSONObject jsLink = (JSONObject) jsLinks.get(lCntr);
		// if (jsLink.get("rel").toString().equalsIgnoreCase("test-runs")){
		// testRunsURI = jsLink.get("href").toString();
		// break;
		// }
		// }
		//
		// JSONObject jsUpdateTestRun = qTest.getTestRun(testRunsURI, "TC-3");
		// int testCaseId =
		// Integer.valueOf(jsUpdateTestRun.get("test_case_version_id").toString()) ;
		// int testRunId = Integer.valueOf(jsUpdateTestRun.get("id").toString());
		// tcLog = new QTestRunLog(testCaseId);
		// tcLog.updateStatus(QTestAPI.tcResult.PASSED);
		// tcLog.updateExecutionDetails("2018-05-28T18:09:30.034Z",
		// "2018-05-28T18:12:30.034Z", "Automation API Sample Log");
		// tcLog.addAttachment("C:\\Users\\E002258\\Downloads\\MemoryLeak_HeapSize_iMacSlave.png",
		// "application/octet-stream");
		// //System.out.println(tcLog.jsTCLog.toString());
		// jsTCResult = qTest.addTestLog(ProjId, testRunId, tcLog);
		// System.out.println(jsTCResult.toString());

		/*SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-dd'T'HH:MM:ss.SSSZ");
		String endDate = format.format(new Date());

		// Third Approach - using getProjectElement method & test case Id
		QTestAPI_Bridge qTest = new QTestAPI_Bridge("pureinsurance");
		qTest.authorize("sgajula@pureinsurance.com", "Priv0516@");
		qTest.loadProject("z_QConnect - Sample Project");
		
		// JSONObject jsProjInfo = qTest.getProjectElement("z_QConnect - Sample
		// Project","", "");
		// System.out.println(jsProjInfo.toString());
		JSONObject jsRelInfo = qTest.getProjectElement("releases", "Release 2.0");
		// System.out.println(jsRelInfo.toString());
		JSONObject jsTestCyleInfo = qTest.getProjectElement("releases| test-cycles",
				"Release 2.0 | Hardening Sprint");
		System.out.println(jsTestCyleInfo.toString());
		JSONObject jsRunInfo = qTest.getProjectElement("releases| test-cycles | test-runs ", "Release 2.0 | Hardening Sprint | Our Solutions section");
		// System.out.println(jsRunInfo.toString());
		// int runId = Integer.valueOf(jsRunInfo.get("id").toString());
		// int tcId = Integer.valueOf(jsRunInfo.get("test_case_version_id").toString())
		// ;
		QTestRunLog tcLog = new QTestRunLog();
		tcLog.updateTestCaseId(Integer.valueOf(jsRunInfo.get("test_case_version_id").toString()));
		tcLog.updateStatus(QTestAPI.tcResult.PASSED);
		tcLog.updateExecutionDetails("2018-05-28T18:09:30.034Z", endDate, "Automation Sample Log");
		tcLog.addAttachment("C:\\Users\\E002258\\Downloads\\MemoryLeak_HeapSize_iMacSlave.png",
				"application/octet-stream");
		// System.out.println(tcLog.jsTCLog.toString());
		JSONObject jsTCResult = qTest.addTestLog(jsRunInfo, tcLog);
		System.out.println(jsTCResult.toString());*/
	}

}
