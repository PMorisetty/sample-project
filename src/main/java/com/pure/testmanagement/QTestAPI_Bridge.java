package com.pure.testmanagement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.HttpStatus;
import org.eclipse.jetty.util.log.Log;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.JsonObject;
import com.pure.accelerators.TestEngineWeb;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;

import bsh.StringUtil;

public class QTestAPI_Bridge {
    private String username;
    private String password;
    private String qTestSiteName;
    private String serverUrl;
    private String authHeader;
    
    private JSONObject jsProjInfo = null;
    //private JSONObject jsCurrTestRun = null;
    public QTestRunLog testRunLogger = new QTestRunLog();
    
    public final String loginUri = "oauth/token";
    public final String projectUri = "api/v3/projects";
    public final String releasesUri = "api/v3/projects/{projectId}/releases";
    public final String testcyclesUri= "api/v3/projects/{projectId}/test-cycles";
    public final String testcycleUri= "/api/v3/projects/{projectId}/test-cycles/{testCycleId}";
    public final String testsuitesUri= "api/v3/projects/{projectId}/test-suites";
    public final String testrunsUri= "api/v3/projects/{projectId}/test-runs";
    public final String testrunUri= "/api/v3/projects/{projectId}/test-runs/{testRunId}";
    public final String testlogsUri= "api/v3/projects/{projectId}/test-runs/{testRunId}/test-logs";
    public final String testcaseUri= "api/v3/projects/{projectId}/test-cases/{testcaseID}?versionId={versionID}";
    public final String testcasesUri="api/v3/projects/{projectId}/test-cases?size=20&page={pageNumber}";
    public final String testCaseWithNumberUri = "api/v3/projects/{projectId}/test-cases/{testCaseNumber}";
    public static String projectID;
    public static final int PASSED = 601;
    public static final int FAILED = 602;
    public static final int INCOMPLETE = 603;
    public static final int BLOCKED = 604;
    public static final int UNEXECUTED = 605;
    public static boolean qTestSessionLive=false;
    public static String timestamp = null;
    public static boolean qTestCycleCreated=false;
    public static JSONObject curr_CycleInfo=null;
    private final static Object lock = new Object();
    
    public enum tcResult {
    	PASSED, FAILED, INCOMPLETE, BLOCKED, UNEXECUTED
    }
    
    public QTestAPI_Bridge(String siteName)
    {
        if (siteName.length() > 0 ){
        	this.qTestSiteName = siteName.trim();
        	this.serverUrl = "https://" + siteName.trim() + ".qtestnet.com/";
        }else{
        	this.serverUrl = "Missing-QTestSiteName";
        }
    }
    
    /**
     * Get/Set User
     *
     * Returns the user used for authenticating the API requests.
     */
    public String getUsername()
    {
        return this.username;
    }

    /**
     * Get/Set Password
     *
     * Returns the password used for authenticating the API requests.
     */
    public String getPassword()
    {
        return this.password;
    }

    /**
     * Get Access Token
     *
     * Returns the access token to be used for authenticating the API requests.
     */
    public String getAccessToken()
    {
        return this.authHeader;
    }
    
    /**
     * Send Get
     *
     * Issues a GET request (read) against the API and returns the result (as Object, see below).
     *
     * Arguments:
     *
     * uri The API method to call including parameters (e.g. get_case/1)
     *
     * Returns the parsed JSON response as standard object which can either be an instance of JSONObject or JSONArray (depending on the API method). In most cases, this returns a JSONObject instance
     * which is basically the same as java.util.Map.
     */
    private String sendGet(String uri) throws MalformedURLException, IOException, QTestAPIException
    {
    	return this.sendRequest("GET", uri, "", null);    	
    }

    /**
     * Send POST
     *
     * Issues a POST request (write) against the API and returns the result (as Object, see below).
     *
     * Arguments:
     *
     * uri The API method to call including parameters (e.g. add_case/1) data The data to submit as part of the request (e.g., a map)
     *
     * Returns the parsed JSON response as standard object which can either be an instance of JSONObject or JSONArray (depending on the API method). In most cases, this returns a JSONObject instance
     * which is basically the same as java.util.Map.
     */
    private String sendPost(String uri, String contentType, Object data) throws MalformedURLException, IOException, QTestAPIException
    {
        return this.sendRequest("POST", uri, contentType, data);
    }

    private String sendRequest(String method, String uri, String contentType, Object data) throws MalformedURLException, IOException, QTestAPIException
    {
    	
    	String accessUrl = ( uri.trim().toLowerCase().contains(this.serverUrl.toLowerCase()) )? uri : this.serverUrl + uri; 
    	URL url = new URL(accessUrl);

        // Create the connection object, set required headers
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(method.trim().toUpperCase());
        conn.addRequestProperty("Content-Type", contentType);
        conn.addRequestProperty("Authorization", this.authHeader);
        
        // (GET/POST) and headers (content type and basic auth).
        if (method.equals("POST"))
        {
            // Add the POST arguments, if any. We just serialize the passed.
            // data object (i.e. a dictionary) and then add it to the request body.
            if (data != null)
            {
                byte[] block = JSONValue.toJSONString(data).getBytes("UTF-8");

                conn.setDoOutput(true);
                OutputStream ostream = conn.getOutputStream();
                ostream.write(block);
                ostream.flush();
            }
        }

        // Execute the actual web request (if it wasn't already initiated, by getOutputStream above) and 
        // record any occurred errors (we use the error stream in this case).
        int status = conn.getResponseCode();

        InputStream istream;
        if (status == HttpStatus.SC_OK || status == HttpStatus.SC_CREATED || status == HttpStatus.SC_ACCEPTED ){
        	istream = conn.getInputStream();
        }else{
        	istream = conn.getErrorStream();
//	        if (istream == null)
//	        {
//	        	throw new QTestAPIException("QTest API return HTTP " + status + " (No additional error message received)");
//	        }        	
        }

        // Read the response body, if any, and deserialize it from JSON.
        String respText = "";
        if (istream != null)
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(istream, "UTF-8"));
            String line;
            while ((line = reader.readLine()) != null)
            {
                line = line + System.getProperty("line.separator");
                respText += line;
            }
            reader.close();
        }
         
         return respText;
    }
    
    synchronized public void authorize() throws MalformedURLException, IOException, QTestAPIException{
    
	    	String userName =  ConfigFileReadWrite.read("resources/framework.properties", "USERNAME");
	    	String password =  ConfigFileReadWrite.read("resources/framework.properties", "PASSWORD");
	    	authorize(userName, password);
    }
    /**
     * Post login request and update access token
     * 
     * @param user
     * @param pwd
     * @return
     * @throws MalformedURLException
     * @throws IOException
     * @throws QTestAPIException
     */
    public boolean authorize(String user, String pwd) throws MalformedURLException, IOException, QTestAPIException{
    	boolean blnRes = false;
    	this.username = user;
    	this.password = pwd;
    	
    	String postLogin = loginUri + "?grant_type=password&username="+ user +"&password="+ pwd;
    	String loginReqContentType = "application/x-www-form-urlencoded";
    	this.authHeader = "Basic " + Base64.getEncoder().encodeToString((this.qTestSiteName+":").getBytes("UTF-8"));
    	System.out.println("base64 sitename: " + this.authHeader);
    	
    	
    	String loginResp =  sendPost(postLogin, loginReqContentType, null);
    	JSONObject authResp = (JSONObject) JSONValue.parse(loginResp);
    	
//    	if(authResp.containsKey("access_token") && !qTestSessionLive){
    	if(authResp.containsKey("access_token")){
    		this.authHeader = "bearer " + (String) authResp.get("access_token");
    		System.out.println(this.username + " access token - " + this.authHeader);
    		blnRes = true;
    		qTestSessionLive=true;
    	}
    	System.out.println("Authorization :" + blnRes + "\n Authorization Response" + authResp);
    	return blnRes;
    }
    
    /**
     * Get Project ID for given project
     * @param projName
     * @return
     */
    public JSONObject loadProject(String projName) {
    	try {
			JSONArray jsProjs = (JSONArray) JSONValue.parse(sendGet(projectUri));
			
			for(int projCntr=0; projCntr<jsProjs.size(); projCntr++){
				
				JSONObject jsProj = (JSONObject) jsProjs.get(projCntr);
				
				if(projName.equals(jsProj.get("name").toString().trim()) &&
						jsProj.containsKey("links")){
					jsProjInfo = jsProj;
					return jsProj;
				}				
			}
		} catch (IOException | QTestAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}    	
    	return null;
    }
    
   	
   	private JSONObject readJsonFile(String jsonFilePath) {
		JSONObject jsonObject = null;
		try {
			/**
			 * START-->>Reading input data from JSON file & Closing stream
			 */
			FileReader fr = new FileReader(jsonFilePath);
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(fr);
			jsonObject = (JSONObject) obj;
			fr.close();
			/**
			 * END-->>Reading input data from JSON file & Closing stream
			 */
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(jsonFilePath + ".json file not found");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
   	
	private JSONObject getJsonObject(String jsonFilePath, String jsonKeyPath) throws Exception {
		
   		JSONObject jsonObject = null;
		try {

			if (jsonFilePath != null) {
				jsonObject = readJsonFile(jsonFilePath);
			}

			if (jsonKeyPath.contains("|")) {
				String[] keys = jsonKeyPath.split("\\s*\\|\\s*");// Splitting key with '|' separate

				for (String key : keys) {
					if (jsonKeyPath.contains("[")) {// Finding index json arrays
						String numberOnly = key.replaceAll("[^0-9]", "");
						key = key.replaceAll("[^a-zA]", "");
						JSONArray arr = (JSONArray) jsonObject.get(key);
						jsonObject = (JSONObject) arr.get(Integer.parseInt(numberOnly));
					} // Arrays Handling -if
				} // For each loop
			} // Main-if
			else {
				jsonObject = (JSONObject) jsonObject.get(jsonKeyPath);
				if (!jsonObject.containsKey(jsonKeyPath)) {
					throw new Exception(
							jsonKeyPath + "-->> Key is not there in JSON File. Please check");
				}
			}
		} // Try-block
		catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}    
    
    /**
     * 
     * @param elementDepth
     * @param elementName
     * @return
     * @throws MalformedURLException
     * @throws IOException
     * @throws QTestAPIException
     */
    public JSONObject getProjectElement(String elementType, String elementName) throws MalformedURLException, IOException, QTestAPIException{
    	JSONObject jsEleInfo = null;
    	String eleTypes[] = elementType.split("\\|"); 
    	String eleNames[] = elementName.split("\\|");
    	   	
    	//Iterate through project nodes and get details
    	if(jsProjInfo != null){
    		
    		//Initialize target element info to proj info to start & update through input type/name
    		jsEleInfo = jsProjInfo;   
    		
        	//Iterate through each element type
        	for(int eCntr=0;eCntr<eleTypes.length;eCntr++){
        		
        		//Get target element type link uri
        		String targetLinkUri = getLinkUri(jsEleInfo.get("links"), eleTypes[eCntr]);
        		if(targetLinkUri.length() > 0 ){
        			
        			targetLinkUri = targetLinkUri.replace(this.serverUrl, "");
        			
        			//Make to get request to get current element details 
        			JSONArray jsElements = (JSONArray) JSONValue.parse(sendGet(targetLinkUri));        			
            		boolean blnEleFound = false;
            		
            		//Iterate through element type array to find matching element name
            		for(int iCntr=0; iCntr<jsElements.size(); iCntr++){            			
            			JSONObject jsElement = (JSONObject) jsElements.get(iCntr);
            			String actEleName = (String) jsElement.get("name");
            			String expEleName = eleNames[eCntr].trim().toLowerCase();
            			
            			if(actEleName.trim().toLowerCase().equals(expEleName)){
            				jsEleInfo = jsElement;
            				blnEleFound= true;
            				break;
            			}
            		}             			
        		}else{
        			jsEleInfo = null;
        			break;
        		}        		
        	}
    	}
    		
    	return jsEleInfo;
    }
        
    /**
     * Parse links json array and return target link uri
     * @param jsLinks
     * @param linkType
     * @return
     */
    private String getLinkUri(Object jsLinks, String linkType){
    	String targetLinkType = "";
    	String targetLinkUri = "";
    	
    	JSONArray jsInpLinks = (JSONArray) jsLinks;
    	linkType = linkType.trim().toLowerCase();
    	switch(linkType){
			case "releases":
    		case "release":
    			targetLinkType="release";
    			break;
    		case "test-cycles":
    		case "test-cycle":
    		case "testcycles":
    		case "testcycle":
    			targetLinkType="test-cycle";
    			break;
    		case "test-suites":
    		case "test-suite":
    		case "testsuites":
    		case "testsuite":
    			targetLinkType="test-suite";
    			break;
    		case "test-runs":
    		case "test-run":
    		case "testruns":
    		case "testrun":
    			targetLinkType="test-run";
    			break;
    		case "test-cases":
    		case "test-case":
    		case "testcases":
    		case "testcase":
    			targetLinkType="test-case";
    			break;
    	}
    	
		for(int lCntr=0; lCntr<jsInpLinks.size(); lCntr++){
			JSONObject jsLink = (JSONObject) jsInpLinks.get(lCntr); 
			if (jsLink.get("rel").toString().equalsIgnoreCase(targetLinkType) || 
					jsLink.get("rel").toString().equalsIgnoreCase(targetLinkType+"s")){
				targetLinkUri = jsLink.get("href").toString();
				break;
			}
		}    	
    	return targetLinkUri;
    }
    
//    /**
//     * 
//     * @param projId
//     * @param testRunId
//     * @param jsTCResult
//     * @return
//     */
//    public JSONObject addTestLog(JSONObject jsTestRunInfo, QTestRunLog tcLog){
//    	
//    	try{
//    		int testRunId = (int) jsTestRunInfo.get("id");
//    		
//    		String projId = jsProjInfo.get("id").toString();    		
//    		String reqUri = testlogsUri.replace("{projectId}", projId)
//					   .replace("{testRunId}", String.valueOf(testRunId));
//
//			JSONObject jsResult = (JSONObject) JSONValue.parse(sendPost(reqUri, "application/json", tcLog.jsTCLog)) ;
//			return jsResult;        			
//    	}catch(Exception e){
//    		e.printStackTrace();
//    	}
//    	return null;
//    }  
   	
	/**
	 * Purpose: To return the element/node type based on its Id
	 * @param jObject
	 * @return
	 */
    private String getElementTypebyPid(JSONObject jObject) {
		String otype = null;
		try {
			if (jObject.get("pid").toString().contains("CL")) {
				otype = "test-cycle";
			} else if (jObject.get("pid").toString().contains("RL")) {
				otype = "release";

			} else if (jObject.get("pid").toString().contains("TS")) {
				otype = "test-suite";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return otype;
	}    
	
	/**
	 * Purpose: Search given test case Pid under the project and return its details
	 * @param tcID
	 * @return
	 * @throws Exception
	 */
	public JSONObject getTCObject(String tcPID) throws Exception {
		try {
			Long projId = Long.valueOf(jsProjInfo.get("id").toString());			
			String tcasesUri = testcasesUri.replace("{projectId}", String.valueOf(projId));
			
			for(int pCntr=1;pCntr<200;pCntr++){
				tcasesUri = testcasesUri.replace("{projectId}", String.valueOf(projId)).
						replace("{pageNumber}", String.valueOf(pCntr));
				System.out.println("Searching for test case "+tcPID+ " in page number " + pCntr );
				//Get Test Case Info
				String strtc = sendGet(tcasesUri);			
				JSONParser parsering = new JSONParser();			
				JSONArray jsonTestcasesArr = (JSONArray) parsering.parse(strtc);			
				
				for (int i = 0; i < jsonTestcasesArr.size(); i++) {
					JSONObject jsonTestcase = (JSONObject) jsonTestcasesArr.get(i);
					String pid = jsonTestcase.get("pid").toString();
					if (pid.equalsIgnoreCase(tcPID.trim())) {
						System.out.println(" TC JSONObject ::::" + jsonTestcase);
						return jsonTestcase;
					}
				}				
			}
			/*
			//Get Test Case Info
			String strtc = sendGet(tcasesUri);			
			JSONParser parsering = new JSONParser();			
			JSONArray jsonTestcasesArr = (JSONArray) parsering.parse(strtc);			
			
			for (int i = 0; i < jsonTestcasesArr.size(); i++) {
				JSONObject jsonTestcase = (JSONObject) jsonTestcasesArr.get(i);
				String pid = jsonTestcase.get("pid").toString();
				if (pid.equalsIgnoreCase(tcPID.trim())) {
					System.out.println(" TC JSONObject ::::" + jsonTestcase);
					return jsonTestcase;
				}
			}*/
		} catch (IOException | QTestAPIException e) {
			e.printStackTrace();
		}

		return null;
	}	
    
		/**
		 * Purpose: Search given test case Pid under the project and return its details
		 * @param tcNumber
		 * @return
		 * @throws Exception
		 */
		public JSONObject getTCObjectFromTCNumber(String tcNumber) throws Exception {
			try {
				Long projId = Long.valueOf(jsProjInfo.get("id").toString());			
				String tcasesUri = "";//testCaseWithNumberUri.replace("{projectId}", String.valueOf(projId));
				

				tcasesUri = testCaseWithNumberUri.replace("{projectId}", String.valueOf(projId)).
						replace("{testCaseNumber}", String.valueOf(tcNumber));
				System.out.println("Searching for test case "+tcNumber);
				//Get Test Case Info
				String strtc = sendGet(tcasesUri);	
				System.out.println("jResponse : " + strtc);
				JSONParser parsering = new JSONParser();		
				JSONObject jsonTestcase = (JSONObject) parsering.parse(strtc);
				
						return jsonTestcase;
			
			} catch (IOException | QTestAPIException e) {
				e.printStackTrace();
			}

			return null;
		}
    /**
     * Purpose: To post/create a new test run with given name & run id, under a given test cycle
     * @param projectName
     * @param parentJson
     * @param tcId
     * @return
     * @throws MalformedURLException
     * @throws IOException
     * @throws QTestAPIException
     */
	public JSONObject createTestRun(JSONObject parentJson, String tcId, String inpRunName)
			throws MalformedURLException, IOException, QTestAPIException {
		JSONObject jsNewTestRun=null;
		try {			
			Long projId = Long.valueOf(jsProjInfo.get("id").toString());
		
			String trunsUri = testrunsUri.replace("{projectId}", String.valueOf(projId));						
			String filePath = "./src/main/java/com/pure/testmanagement/CreateTestRun.json";			
			JSONObject jsCreateTestRun = readJsonFile(filePath);
			
			String parentType = getElementTypebyPid(parentJson);			
			JSONObject crtcjson = (JSONObject) jsCreateTestRun.get("test_case");
			System.out.println(jsCreateTestRun.get("test_case"));
			
			// Test case Json Object			
			JSONObject tcJson = getTCObjectFromTCNumber(tcId);
			crtcjson.put("id", tcJson.get("id"));
			crtcjson.put("pid", tcJson.get("pid"));
			crtcjson.put("parent_id", tcJson.get("parent_id"));
			jsCreateTestRun.put("parentId", Long.valueOf(parentJson.get("id").toString()));
			jsCreateTestRun.put("parentType", parentType);
			
			String trName = (inpRunName.trim().length()>0)? inpRunName: "AutoRun " + tcJson.get("name").toString();
			jsCreateTestRun.put("name", trName);
			
			jsCreateTestRun.put("test_case_version_id", tcJson.get("test_case_version_id"));
			jsCreateTestRun.put("test_case", crtcjson);
			
			//Post Test Run to QTest
			String trJson = sendPost(trunsUri, "application/json", jsCreateTestRun);
			jsNewTestRun = (JSONObject) new JSONParser().parse(trJson);
			if(jsNewTestRun != null) {				
				//testRunLogger.setTargetTestRun(jsNewTestRun);
			}else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsNewTestRun;
	}
	/*
	*//**
	 * Purpose: Check and return the details of a given test run (using its id & name), under given test cycle
	 * @param jsTestCycle
	 * @param TCID
	 * @param inpRunName
	 * @return
	 *//*
	public JSONObject getExistingTestRun(JSONObject jsTestCycle, String TCID, String inpRunName) {
		
		JSONObject jsTargetTestRun = null;		
		try {
			
			//Get Test Runs Info for given test cycle
			String tcRunsUri = getLinkUri(jsTestCycle.get("links"), "test-run");
			String testrunsParams ="";
			//Temp Workaround: for QTest API issue
			if(tcRunsUri.indexOf("?parentId=") >0)
			{
				testrunsParams = tcRunsUri.substring(tcRunsUri.indexOf("?parentId="), tcRunsUri.length());
			}
			tcRunsUri = testrunsUri.replace("{projectId}", String.valueOf(jsProjInfo.get("id"))) + testrunsParams;
			//Temp Workaround:
			
			String tcruns = sendGet(tcRunsUri.trim());
			JSONParser parsering = new JSONParser();
			JSONArray jsonTestrunsArr = (JSONArray) parsering.parse(tcruns);
			
			//Iterate through existing test runs
			for (int k = 0; k < jsonTestrunsArr.size(); k++) {
				
				boolean flag = false;
				JSONObject jsTestRun = (JSONObject) jsonTestrunsArr.get(k);
				String currRunName = jsTestRun.get("name").toString().trim();
				String testCaseUri = getLinkUri(jsTestRun.get("links"), "test-case"); 
						
				//Get Test Case Details
				String strTCInfo = sendGet(testCaseUri);
				JSONParser tcparser = new JSONParser();
				JSONObject jsTCInfo = (JSONObject) tcparser.parse(strTCInfo);
				
				//Verify & return test run corresponds to input test case id
				if(jsTCInfo.get("pid").toString().equalsIgnoreCase(TCID.trim()) && 
						currRunName.equalsIgnoreCase(inpRunName.trim()) ) {
					jsTargetTestRun = jsTestRun;
					break;
				}
			}
			
			if(jsTargetTestRun != null) {
				//testRunLogger.setTargetTestRun(jsTargetTestRun);
			}else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsTargetTestRun;
	}	*/
	/**
	 * Purpose: Check and return the details of a given test run (using its id & name), under given test cycle
	 * @param jsTestCycle
	 * @param TCID
	 * @param inpRunName
	 * @return
	 */
	public JSONObject getExistingTestRun(JSONObject jsTestCycle, String TCID, String inpRunName) {
		
		JSONObject jsTargetTestRun = null;		
		try {
			
			//Get Test Runs Info for given test cycle
			String tcRunsUri = getLinkUri(jsTestCycle.get("links"), "test-run");
			String testrunsParams ="";
			//Temp Workaround: for QTest API issue
			if(tcRunsUri.indexOf("?parentId=") >0)
			{
				testrunsParams = tcRunsUri.substring(tcRunsUri.indexOf("?parentId="), tcRunsUri.length());
			}
			tcRunsUri = testrunsUri.replace("{projectId}", String.valueOf(jsProjInfo.get("id"))) + testrunsParams;
			//Temp Workaround:
			
			String tcruns = sendGet(tcRunsUri.trim());
			JSONParser parsering = new JSONParser();
			JSONArray jsonTestrunsArr = (JSONArray) parsering.parse(tcruns);
			
			//Iterate through existing test runs
			for (int k = 0; k < jsonTestrunsArr.size(); k++) {
				
				boolean flag = false;
				JSONObject jsTestRun = (JSONObject) jsonTestrunsArr.get(k);
				//Verify & return test run corresponds to input test case id
				
				JSONObject jsTestCase = (JSONObject) jsTestRun.get("test_case");
				if(jsTestCase.get("id").toString().equalsIgnoreCase(TCID))
				{
					jsTargetTestRun = jsTestRun;
					break;
				}
			}
			
			if(jsTargetTestRun != null) {
				//testRunLogger.setTargetTestRun(jsTargetTestRun);
			}else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsTargetTestRun;
	}	
	
	/**
	 * Purpose: Check and return the details of a given test run (using its id & name), under given test cycle /OR  
	 * 			If not existing, post/create a new test run with given name & run id, under a given test cycle
	 * @param jsTestCycle
	 * @param tcPid
	 * @param runName
	 * @return
	 */
	public QTestRunLog startTestRunLog(JSONObject jsTestCycle, String tcPid, String runName) {		
		System.out.println("###############################" + tcPid + "###############################" + runName + "###############################");
		QTestRunLog testRunLogger=null;
		boolean blnRes = false;
		JSONObject jsTestRun = null;		
		try {			
			//Get Existing Run & Set the same to logger
			jsTestRun = getExistingTestRun(jsTestCycle, tcPid, runName);			
			if( jsTestRun  !=null) {
				blnRes = true;
				testRunLogger = new QTestRunLog();
				testRunLogger.setTargetTestRun(jsTestRun);
			}else {				
				//Create a new test run and set the same to logger
				jsTestRun = createTestRun(jsTestCycle, tcPid, runName);				
				if( jsTestRun  !=null) {
					blnRes = true;
					testRunLogger = new QTestRunLog();
					testRunLogger.setTargetTestRun(jsTestRun);
				}else {
					blnRes = false;
				}
			}			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return testRunLogger;
	}

	/**
	 * Purpose: Check and return the details of a given test run (using its Teset Case Number)  
	 * @param Universal Agent Test Runs log file path which will be created from Universal Agent shell script @ Run time 
	 * @param testCaseNumber - Test case number
	 * @param testCaseNumber - Test run name
	 * @return
	 */
	public QTestRunLog getUniversalAgentTestRunFromLog(String universalAgentRunJsonFilePath, String testCaseNumber, String runName) {		
		System.out.println("###############################" + testCaseNumber + "###############################" + runName + "###############################");
		QTestRunLog testRunLogger=null;
		boolean blnRes = false;
		JSONObject jsTestRun = null;		
		try {
			Object obj = new JSONParser().parse(new FileReader(universalAgentRunJsonFilePath)); 
			 JSONArray jArray = (JSONArray)obj;
			 for(int intCnt=0;intCnt<jArray.size();intCnt++)
			 {
				 
				 JSONObject jsObj = (JSONObject) jArray.get(intCnt);
				 JSONObject jsObjTC = (JSONObject) jsObj.get("test_case");
				 String TCNumber = jsObjTC.get("id").toString();
				 if(TCNumber.equalsIgnoreCase(testCaseNumber))
				 {
					 System.out.println("Found Run Log for : " + testCaseNumber);
					 
					 String TestRunNumber = jsObj.get("id").toString();
					 System.out.println("Test Run Number : "+ TestRunNumber);
					 
					 /*JSONArray jsObjLinksAray = (JSONArray) jsObj.get("links");
					 JSONObject jsObjSelf = (JSONObject) jsObjLinksAray.get(3);
					 String href = jsObjSelf.get("href").toString();
					 System.out.println(href);*/
					 
					 jsTestRun = jsObj;//getTestcyclefromTestRunID(TestRunNumber);
					 testRunLogger = new QTestRunLog();
					testRunLogger.setTargetTestRun(jsTestRun);
					blnRes=true;
					break;					 
				 }				 
			 }
			 if(blnRes==false)
			 {
				 throw new RuntimeException("Unable to find TC Number: "+ testCaseNumber + ", In Test Run Json File :" + universalAgentRunJsonFilePath + ", For Test Case:"+ runName);
			 }
		}catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Error in getUniversalAgentTestRunFromLog(): " + e.getMessage(), e);
			}
		return testRunLogger;
	}
	
	/**
	 * Purpose: To post given runLog to the given test run Id
	 * @param runLog
	 * @return
	 */
	public JSONObject postTestRunLog(QTestRunLog runLog) {
		JSONObject jsResult=null;
		try{
			if(runLog.getTargetTestRun() != null) {
				Long inpTestRunId = Long.valueOf(runLog.getTargetTestRun().get("id").toString());
	    		String projId = jsProjInfo.get("id").toString(); 		
	    		String reqUri = testlogsUri.replace("{projectId}", projId)
						   .replace("{testRunId}", String.valueOf(inpTestRunId));
				jsResult = (JSONObject) JSONValue.parse(sendPost(reqUri, "application/json", runLog.getTestRunLog())) ;
				System.out.println("Output of POST\n##########################" + jsResult + "\n##########################");
			}else {
				jsResult = null;
			}			
    	}catch(Exception e){
    		System.out.println("------------------------------------------------");
    		System.out.println(e.getMessage());
    		System.out.println("------------------------------------------------");
    		e.printStackTrace();
    	}finally {
    		testRunLogger.resetLogger();
    	}
    	return jsResult;			
	}	
	
	/**
	 * Purpose: Get Parent Test Cycle information using a test run id (can be fetched by QTE.testRuns[0].id 
	 * as shell agent command line parameter)
	 * @param projName
	 * @param testrunid
	 * @return
	 * @throws ParseException
	 */
	public JSONObject getTestcyclefromTestRunID(String testrunid) throws ParseException {
        JSONObject JsTC = null;
        try {
               //int projId = getProjectID(projName);
               Long projId = Long.valueOf(jsProjInfo.get("id").toString());
               String trunUri = testrunUri.replace("{projectId}", String.valueOf(projId)).replace("{testRunId}",
                            String.valueOf(testrunid));
               System.out.println(trunUri);
               
               String strtr = sendGet(trunUri);
               JSONParser parsering = new JSONParser();
               JSONObject jsonTestRun = (JSONObject) parsering.parse(strtr);
               
               String tcUri = getLinkUri(jsonTestRun.get("links"),"test-cycle");
               
               //Temp Workaround: Due to QTest API Issue - https://pureinsurance.qtestnet.com/api/v3/projects/40255/test-runs/49226931/api/v3/projects/40255/test-cycles/713438 is being fetched through in links
               String tcId = tcUri.substring(tcUri.lastIndexOf("/")+1, tcUri.length());
               tcUri = testcycleUri.replace("{projectId}", String.valueOf(projId)).replace("{testCycleId}",
                       String.valueOf(tcId));
               //Temp Workaround
            		   
               String strTestCycle = sendGet(tcUri);
               JSONParser tcparser = new JSONParser();
               JsTC = (JSONObject) tcparser.parse(strTestCycle);               
        } catch (IOException | QTestAPIException e) {
               e.printStackTrace();
               return null;
        }
        return JsTC;
	}
	/**
	 * createTestCycle() method to create new cycle under a release
	 * @param releaseName
	 * Example: createTestCycle("Release 2.0");
	 */
	synchronized public void createTestCycle(String releaseName){
		synchronized (lock) {
			try {
				setTimeStamp();
				releaseName = releaseName.replaceAll(" ", "_").toUpperCase();
				String releaseID = ConfigFileReadWrite.read("resources/framework.properties", releaseName);
				if(!ifCycleExists("release",releaseID, timestamp, true) && !isQTestCycleCreated()){
					String url = "/api/v3/projects/{projectId}/test-cycles?parentId={releaseId}&parentType=release";
					Long projId = Long.valueOf(jsProjInfo.get("id").toString());	
					url = url.replace("{projectId}", String.valueOf(projId));
					url = url.replace("{releaseId}", releaseID);
					JSONObject jsonObject = new JSONObject();
					jsonObject.put( "name", timestamp);
					jsonObject.put( "description", "Cycle create on :" + timestamp);
					String resp = sendPost(url, "application/json", jsonObject);
					qTestCycleCreated = true;
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (QTestAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	/**
	 * 
	 * @param parentType - test-cycle or release
	 * @param releaseOrcycleID - cycle or release ID which can be captured from QTest URL 
	 * eg: https://pureinsurance.qtestnet.com/p/40255/portal/project#tab=testexecution&object=8&id=186422
	 * id=186422, parameter should be 186422  
	 * @param createNewCycle, true or false , creates a new test cycle under release or test cycle with YYYY-MM-DD HH:MM
	 * @return - returns cycle Json object
	 * Below is an example to create a new test cycle under the parent node 713436, creates new test run if the test doesn't exist
	 * Eg: jsCycleInfo = qTest.createOrGetTestCycle("test-cycle", "713436", true);
	 * 
	 * Below is an example to update existing test cycle under the parent node 713436
	 * Eg: jsCycleInfo = qTest.createOrGetTestCycle("test-cycle", "713436", false);
	 */
	synchronized public JSONObject createOrGetTestCycle(String parentType, String releaseOrcycleID, boolean createNewCycle){
		synchronized (lock) {
			String url = "";
			String resp = "";
			JSONObject cycleInfo = null;
			Long projId;
			try {
				setTimeStamp();				
				
				if(!ifCycleExists(parentType, releaseOrcycleID, timestamp, createNewCycle) && !isQTestCycleCreated()){
					projId = Long.valueOf(jsProjInfo.get("id").toString());
					if(parentType.equalsIgnoreCase("release"))
					{
						url = "/api/v3/projects/{projectId}/test-cycles?parentId={releaseId}&parentType=release";
					}
					else
					{
						url = "/api/v3/projects/{projectId}/test-cycles?parentId={releaseId}&parentType=test-cycle";
						//url = "/api/v3/projects/"+ String.valueOf(projId) + "/test-cycles/" + releaseOrcycleID + "?expand=descendants" ;//?parentId=1820475&parentType=test-cycle";
					}
					url = url.replace("{releaseId}", releaseOrcycleID);
					url = url.replace("{projectId}", String.valueOf(projId));
					if(createNewCycle==true)
					{
						JSONObject jsonObject = new JSONObject();
						jsonObject.put( "name", timestamp);
						jsonObject.put( "description", "Cycle create on :" + timestamp);
						resp = sendPost(url, "application/json", jsonObject);
						JSONParser parsering = new JSONParser();
						cycleInfo = (JSONObject) parsering.parse(resp);
						
					}
					else
					{
						resp = sendGet(url);
						JSONParser parsering = new JSONParser();
						
						JSONArray cycleArr = (JSONArray) parsering.parse(resp);
						cycleInfo = (JSONObject) cycleArr.get(0);
					}
					curr_CycleInfo = cycleInfo;				
					qTestCycleCreated = true;
				}
				else
				{
					cycleInfo = curr_CycleInfo; //curr_CycleInfo is updated ifCycleExists();
				}
			} catch (Exception Ex) {
				// TODO Auto-generated catch block
				Ex.printStackTrace();				
				throw new RuntimeException(Ex.getMessage(), Ex);
/*			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (QTestAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();*/
			}
			return cycleInfo;
		}
		
	}
	
	public void setTimeStamp(){
			Date date = new Date();
			if(timestamp != null)
			{
					return;
			}
			String tempTimeStamp = "";
			/*getMonth() is deprecated need to implement below code*/
			
			LocalDateTime localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			
			
			String curr_Year = String.format("%04d", localDate.getYear());
			String curr_Month = String.format("%02d", localDate.getMonthValue());
			String curr_Day = String.format("%02d", localDate.getDayOfMonth());
			String curr_Hour = String.format("%02d", localDate.getHour());
			String curr_Minute = String.format("%02d", localDate.getMinute());
			
			//tempTimeStamp = curr_Year + "-" + curr_Month + "-" + curr_Day + "-" + curr_Hour + "-" + curr_Minute  + "-" + TestEngineWeb.region;
			tempTimeStamp =  curr_Month + "/" + curr_Day + "/" + curr_Year + " " + curr_Hour + ":" + curr_Minute + "-" + TestEngineWeb.region;
			
			
			timestamp = tempTimeStamp;
			
			//String tempTimeStamp = (1900 + date.getYear())+ "-" + (date.getMonth() + 1) +"-"+ date.getDate() + " " + date.getHours() +":"+ date.getMinutes() + "-" + TestEngineWeb.region ;
			
	}
	
	synchronized public boolean ifCycleExists(String parentType, String releaseOrCycleID, String cycleName, boolean createNewCycle) throws MalformedURLException, IOException, QTestAPIException, ParseException{
		
		String url = "";
		if(parentType.equalsIgnoreCase("release"))
		{
			url = "/api/v3/projects/{projectId}/test-cycles?parentId={releaseId}&parentType=" + parentType;
		}
		else
		{
			url = "/api/v3/projects/{projectId}/test-cycles/{releaseId}?expand=descendants";
		}
		Long projId = Long.valueOf(jsProjInfo.get("id").toString());	
		url = url.replace("{projectId}", String.valueOf(projId));
		url = url.replace("{releaseId}", releaseOrCycleID);
		String resp = sendGet(url);
		JSONParser parsering = new JSONParser();
		JSONArray jsonTestrunsArr = null;
		if(parentType.equalsIgnoreCase("release"))
		{
			jsonTestrunsArr = (JSONArray) parsering.parse(resp);
		}
		else
		{
			JSONObject jCycle  = (JSONObject) parsering.parse(resp);
			if(jCycle.size()==0)
			{
				throw new RuntimeException("Invalid " + parentType + ", ID:" + releaseOrCycleID);
			}
			else
			{
				if(createNewCycle==false)
				{
					curr_CycleInfo=jCycle;
					return true;
				}
				else
				{
					//Getting test-cycles under current cycle
					JSONObject jTestCycle  = (JSONObject) parsering.parse(resp);
					jsonTestrunsArr = (JSONArray) jTestCycle.get("test-cycles");					
				}
			}
		}
		
		//Iterate through existing cycles
		for (int k = 0; k < jsonTestrunsArr.size(); k++) {
			boolean flag = false;
			JSONObject jsonObject = (JSONObject) jsonTestrunsArr.get(k);
			String currCycle = jsonObject.get("name").toString().trim();
			if(cycleName.equalsIgnoreCase(currCycle)){
				curr_CycleInfo=jsonObject;
				return true;
			}
		}
		return false;
		
	}
	
	synchronized public JSONObject getCycleInfo(String cycleID) throws MalformedURLException, IOException, QTestAPIException, ParseException{
		String url = testcycleUri;
		Long projId = Long.valueOf(jsProjInfo.get("id").toString());	
		url = url.replace("{projectId}", String.valueOf(projId));
		url = url.replace("{testCycleId}", cycleID);
		String resp = sendGet(url);
		JSONParser parsering = new JSONParser();
		JSONObject cycleInfo = (JSONObject) parsering.parse(resp);
		if(cycleInfo==null)
		{
			throw new RuntimeException("Unable to find Cycle:" + cycleID);
		}
		
/*		//Iterate through existing cycles
		for (int k = 0; k < jsonTestrunsArr.size(); k++) {
			boolean flag = false;
			JSONObject jsonObject = (JSONObject) jsonTestrunsArr.get(k);
			String currCycle = jsonObject.get("name").toString().trim();
			if(cycleName.equalsIgnoreCase(currCycle)){
				return true;
			}
		}*/
		return cycleInfo;
		
	}
	
	synchronized public JSONObject getCycleInfoIfExistsOrCreateCycle(String parentType,String parentCycleOrReleaseID, String createNewCycleName) throws MalformedURLException, IOException, QTestAPIException, ParseException{
		String url = testcycleUri;
		JSONObject jCycleInfo = null;
		boolean newCycleNameFound = false;
		Long projId = Long.valueOf(jsProjInfo.get("id").toString());
		
		url = url.replace("{projectId}", String.valueOf(projId));
		url = url.replace("{testCycleId}", parentCycleOrReleaseID);
		url = url + "?parentId=" + parentCycleOrReleaseID + "&";
		if(parentType.equalsIgnoreCase("release"))
		{
			url = url + "parentType=release";
		}
		else if(parentType.equalsIgnoreCase("test-cycle"))
		{
			url = url + "parentType=test-cycle";
		}
		else 
		{
			url = url + "parentType=root";
		}
		String resp ="";
		JSONParser parsering = new JSONParser();
		url = "/api/v3/projects/"+ String.valueOf(projId) + "/test-cycles/" + parentCycleOrReleaseID + "?expand=descendants" ;//?parentId=1820475&parentType=test-cycle";
		resp = sendGet(url);
//		JSONArray test = (JSONArray) parsering.parse(resp);
//		url = "/api/v3/projects/40255/test-cycles?parentId=1820475&parentType=test-cycle&expand=descendants";
//		resp = sendGet(url);
		
		jCycleInfo = (JSONObject) parsering.parse(resp);
		
		/*
		//Iterate through existing cycles
		for (int currCycleIndex = 0; currCycleIndex < jCyclesArr.size(); currCycleIndex++) {
			boolean flag = false;
			jCycleInfo = (JSONObject) jCyclesArr.get(currCycleIndex);
			String currCycle = jCycleInfo.get("name").toString().trim();
			if(createNewCycleName.equalsIgnoreCase(currCycle)){
				newCycleNameFound=true;				
			}
		}
		*/
		//Verifies new cycle and creates the new cycle if not found
		if(newCycleNameFound==false)
		{
			String newUrl = "/api/v3/projects/{projectId}/test-cycles?parentId={releaseId}&parentType=release";				
			url = url.replace("{projectId}", String.valueOf(projId));
			url = url.replace("{releaseId}", parentCycleOrReleaseID);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put( "name", timestamp);
			jsonObject.put( "description", "Cycle create on :" + timestamp);
			resp = sendPost(url, "application/json", jsonObject);
			jCycleInfo = (JSONObject) parsering.parse(resp);			
			return jCycleInfo;
		}
		if(jCycleInfo==null)
		{
			throw new RuntimeException("Unable to Find and Create Test Cycle for " + parentCycleOrReleaseID + ", CycleName: " + createNewCycleName);
		}
		else
		{
			qTestCycleCreated = true;
		}
		return jCycleInfo;
		
	}
	public synchronized static boolean isQTestCycleCreated(){
			return qTestCycleCreated;
	}
	
	
	/**
	 * 
	 * @param pID
	 * @param testtype
	 * @return
	public JSONObject getTestObjectfromTestRelease(String testRunPID, String testtype) {
		 
        JSONObject jObject = null;
        
        try {
        	   Long projectID = Long.valueOf(jsProjInfo.get("id").toString());
               
               // JSON For Release
               String truri = releasesUri.replace("{projectId}", String.valueOf(projectID));
               String strtr = sendGet(truri);
               JSONParser parsering = new JSONParser();
               JSONArray jsonRarray = (JSONArray) parsering.parse(strtr); // JSOn Array For release
               
               for (int i = 0; i < jsonRarray.size(); i++) {
                     JSONObject jsonRelease = (JSONObject) jsonRarray.get(i);
                     if (jsonRelease.get("pid").toString().equalsIgnoreCase(testRunPID)) {
                            return jsonRelease; // Return Json release object
                     }
                     JSONArray testtypearr = (JSONArray) jsonRelease.get("links"); // JSOn Array For Test Releases
                     for (int j = 0; j < testtypearr.size(); j++) {
                            JSONObject jsonTr = (JSONObject) testtypearr.get(j);
                            // to Skip for Builds
                            if ((jsonTr.get("rel").toString().contains("builds")
                                          || jsonTr.get("rel").toString().contains("self"))) {
                                   continue;
                            }
                            String href = jsonTr.get("href").toString(); // href can be test cycles or test suites
                            String uri[] = href.split("/", 4);
                            String tstype = sendGet(uri[3]);
                            if (tstype.contentEquals("[]")) {
                                   continue;
                            }
                            JSONParser parsering2 = new JSONParser();
                            JSONArray jsonRarray2 = (JSONArray) parsering2.parse(tstype);
                            for (int k = 0; k < jsonRarray2.size(); k++) {
                                   JSONObject jsonTesttype = (JSONObject) jsonRarray2.get(k);
                                   if (jsonTesttype == null) {
                                          continue;
                                   }
                                   if (jsonTesttype.get("pid").toString().equalsIgnoreCase(testRunPID)) {
                                          System.out.println("Printing required object" + jsonTesttype);
                                          return jsonTesttype; // Could be Test Cycle , test Run,Test Suite
                                   }
                                   if ((testtype.contains("suite") || testtype.contains("run") || testtype.contains("case"))) {

                                          JSONArray testtypearr2 = (JSONArray) jsonTesttype.get("links");
                                          for (int l = 0; l < testtypearr2.size(); l++) {
                                                 JSONObject jsonType2 = (JSONObject) testtypearr2.get(l);
                                                 if ((jsonType2.get("rel").toString().contains("self")
                                                               || jsonType2.get("rel").toString().contains("release"))) {
                                                        continue;
                                                 }
                                                 String href1 = jsonType2.get("href").toString();
                                                 String uri1[] = href1.split("/", 4);
                                                 String tstype1 = sendGet(uri1[3]);
                                                 if (tstype1.contentEquals("[]")) {
                                                        continue;
                                                 }
                                                 JSONParser parsering3 = new JSONParser();
                                                 JSONArray jsonRarray3 = (JSONArray) parsering3.parse(tstype1);
                                                 for (int m = 0; m < jsonRarray3.size(); m++) {
                                                        JSONObject jsonType3 = (JSONObject) jsonRarray3.get(m);
                                                        if (jsonType3.get("pid").toString().equalsIgnoreCase(testRunPID)) {
                                                               System.out.println("Printing required object 2nd level" + jsonType3);
                                                               return jsonType3; // Could be Test Cycle , test Run,Test Suite
                                                        }
                                                        if (testtype.contains("case")) {
                                                               JSONArray jsonarray4 = (JSONArray) jsonType3.get("links");

                                                               for (int n = 0; n < jsonarray4.size(); n++) {
                                                                     JSONObject jsonType4 = (JSONObject) jsonarray4.get(n);
                                                                     if (jsonType4.get("rel").toString().equalsIgnoreCase("test-case")) {
                                                                            String hreftc = jsonType4.get("href").toString();
                                                                            System.out.println("printing href of test case " + hreftc);
                                                                            String uritc[] = hreftc.split("/", 4);
                                                                            System.out.println("Printing URI" + uritc[3]);
                                                                            String tc = sendGet(uritc[3]);
                                                                            JSONParser tcparser = new JSONParser();
                                                                            JSONObject jsTC = (JSONObject) tcparser.parse(tc);
                                                                            if (jsTC.get("pid").toString().equalsIgnoreCase(testRunPID)) {
                                                                                   System.out.println("Test Case matched");
                                                                                   return jsTC;
                                                                            }
                                                                     }
                                                               }
                                                        }
                                                 }
                                          }
                                   }
                            }
                     }
               }
        } catch (Exception e) {
               e.printStackTrace();
        }
        return jObject;
 	}
 	 */	
}
