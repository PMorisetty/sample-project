package com.pure.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zip {
	public static void zipFolder(String srcFolder, String destZipFile) throws Exception {
		ZipOutputStream zip = null;
		FileOutputStream fileWriter = null;

		fileWriter = new FileOutputStream(destZipFile);
		zip = new ZipOutputStream(fileWriter);

		addFolderToZip("", srcFolder, zip);
		zip.flush();
		zip.close();
		zip = null;
	}
	
	//Overloaded
	//This overloaded method has test case result zipped separately.
	public static void zipFolder(String srcFolder, String destZipFile, String testcaseName) throws Exception {
		ZipOutputStream zip = null;
		FileOutputStream fileWriter = null;

		fileWriter = new FileOutputStream(destZipFile);
		zip = new ZipOutputStream(fileWriter);

		addFolderToZip("", srcFolder, zip, testcaseName);
		zip.flush();
		zip.close();
	}


	static private void addFileToZip(String path, String srcFile,
			ZipOutputStream zip) throws Exception {

		File folder = new File(srcFile);
		if (folder.isDirectory()) {
			addFolderToZip(path, srcFile, zip);
		} else {
				byte[] buf = new byte[1024];
				int len;
				@SuppressWarnings("resource")
				FileInputStream in = new FileInputStream(srcFile);
				zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
				while ((len = in.read(buf)) > 0) {
					zip.write(buf, 0, len);
				}
		}
	}

	//Overloaded
	//This overloaded method has test case result zipped separately.
	static private void addFileToZip(String path, String srcFile,
			ZipOutputStream zip, String testcaseName) throws Exception {

		File folder = new File(srcFile);
		if (folder.isDirectory()) {
			if(folder.getAbsolutePath().contains("results")||folder.getAbsolutePath().contains("CHROME")) {
				addFolderToZip(path, srcFile, zip);
			}
		} else {
			if(folder.getAbsolutePath().contains(testcaseName)||folder.getAbsolutePath().contains("screenshots")) {
				byte[] buf = new byte[1024];
				int len;
				@SuppressWarnings("resource")
				FileInputStream in = new FileInputStream(srcFile);
				zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
				while ((len = in.read(buf)) > 0) {
					zip.write(buf, 0, len);
				}
			}
		}
	}
	
	static private void addFolderToZip(String path, String srcFolder,
			ZipOutputStream zip) throws Exception {
		File folder = new File(srcFolder);

		for (String fileName : folder.list()) {
			if (path.equals("")) {
				addFileToZip(folder.getName(), srcFolder + "/" + fileName, zip);
			} else {
				addFileToZip(path + "/" + folder.getName(), srcFolder + "/"
						+ fileName, zip);
			}
		}
	}

	//Overloaded
	//This overloaded method has test case result zipped separately.
	static private void addFolderToZip(String path, String srcFolder, ZipOutputStream zip, String testcaseName) throws Exception {
		File folder = new File(srcFolder);

		for (String fileName : folder.list()) {
			if (path.equals("")) {
				addFileToZip(folder.getName(), srcFolder + "/" + fileName, zip, testcaseName);
			} else {
				addFileToZip(path + "/" + folder.getName(), srcFolder + "/"
						+ fileName, zip, testcaseName);
			}
		}
	}

}
