/*
Author: Ravinder Kumar | Date: 10-10-2017
Libraries for qTest operations
*/

package com.pure.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.pure.report.ConfigFileReadWrite;

public class Qtest {
	@SuppressWarnings("finally")
	public static String POST(String endPointURL, String postBody, String authorizationHeader,String contentType)
	{
		String returnValue = "";
		try
		{
			URL url = new URL(endPointURL);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setDoOutput(true);    		
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty( "Authorization", authorizationHeader );
    		if(contentType.trim() != ""){
    			conn.setRequestProperty( "Content-Type", contentType );
    		}
    		OutputStream os = conn.getOutputStream();
    		os.write(postBody.getBytes());    		
    		os.flush();
    		os.close();
    		int responseCode = conn.getResponseCode();
    		if (responseCode == HttpURLConnection.HTTP_OK) { //success
    			StringBuilder sb = new StringBuilder();
    		    String line;
    		    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    		    while ((line = br.readLine()) != null) {
    		        sb.append(line);
    		    }
    		    returnValue = sb.toString();
    		}
		}
		catch (Exception ex)
		{
			
		}finally{
			return returnValue;
		}
	}
	
	@SuppressWarnings("finally")
	public static String GET(String endPointURL, String authorizationHeader)
	{
		String returnValue = "";
		try
		{
			URL url = new URL(endPointURL);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setDoOutput(true);    		
    		conn.setRequestMethod("GET");
    		conn.setRequestProperty( "Authorization", authorizationHeader );
    		int responseCode = conn.getResponseCode();
    		if (responseCode == HttpURLConnection.HTTP_OK) { //success
    			StringBuilder sb = new StringBuilder();
    		    String line;
    		    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    		    while ((line = br.readLine()) != null) {
    		        sb.append(line);
    		    }
    		    returnValue = sb.toString();
    		}
		}
		catch (Exception ex)
		{
			
		}finally{
			return returnValue;
		}
	}
	
	@SuppressWarnings("resource")
	public static void UpdateQTestResults(boolean scriptResult,String testCaseName, Date sTime, Date eTime)
	{
		String updateStatusOnFailure = ConfigFileReadWrite.read("resources/framework.properties", "UPDATE_STATUS_ON_FAILURE");;
		if(Boolean.parseBoolean(updateStatusOnFailure) || scriptResult) {
			try
			{
				String dataTCId="",
						dataTCName="" ;
				String excelFilePath = System.getProperty("user.dir") + "\\TestData\\TestMapping.xlsx";
		        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
		        Workbook workbook = new XSSFWorkbook(inputStream);
		        Sheet firstSheet = workbook.getSheetAt(0);
		        Iterator<Row> iterator = firstSheet.iterator();
		        iterator.next();
		        while (iterator.hasNext()) {
		        	Row nextRow = iterator.next();
		        	dataTCId = nextRow.getCell(0).toString();
		        	dataTCName = nextRow.getCell(1).toString();
		        	if(dataTCName.equalsIgnoreCase(testCaseName))
					{
		        		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		        		formatter.setTimeZone(TimeZone.getTimeZone("GMT-5:00"));
		        	    String startTime= formatter.format(sTime);  
						String endTime = formatter.format(eTime); 
						PublishQtestResults(dataTCId,(scriptResult)?"Passed":"Failed", startTime, endTime);
					}
		        }
			}
			catch (Exception ex)
			{
				System.out.println("Error while performing updating QTest Results\nException Details: "+ex.getMessage());
			}
		}
	}
	
	public static void PublishQtestResults(String testCaseName, String status, String startTime, String endTime)
	{
		JSONObject jsonObject = new JSONObject();
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonArray = new JSONArray();
		String qtestUrl = ConfigFileReadWrite.read("resources/framework.properties", "QTEST_URL");
		String userName = ConfigFileReadWrite.read("resources/framework.properties", "USERNAME");
		String password = ConfigFileReadWrite.read("resources/framework.properties", "PASSWORD");
		String projectName = ConfigFileReadWrite.read("resources/framework.properties", "PROJECT_NAME");
		String releaseName = ConfigFileReadWrite.read("resources/framework.properties", "RELEASE_NAME");
		String testSuiteName = ConfigFileReadWrite.read("resources/framework.properties", "TEST_SUITE_NAME");
		String projectID="",releaseID="",testSuiteID="",testRunID="";
		
		try {
			//To get token
			String url = qtestUrl + "/oauth/token?grant_type=password&username="+userName+"&password="+password;
			String authHeader = "Basic bGluaC1sb2dpbjo=";
			String contentType="application/x-www-form-urlencoded";
			String token = ((JSONObject)jsonParser.parse(POST(url, "", authHeader, contentType))).get("access_token").toString();
			//To get Project ID		
			url = qtestUrl + "/api/v3/projects/";
			authHeader = "bearer " + token;
			jsonArray = (JSONArray)jsonParser.parse(GET(url, authHeader));
			for(int i = 0; i < jsonArray.size(); i++){
				jsonObject = (JSONObject)jsonParser.parse(jsonArray.get(i).toString());
				if(jsonObject.get("name").toString().equalsIgnoreCase(projectName)){
					projectID = jsonObject.get("id").toString();
					break;
				}
			}
			//To get Release ID			
			url = qtestUrl + "/api/v3/projects/" + projectID + "/releases";
			authHeader = "bearer " + token;
			jsonArray = (JSONArray)jsonParser.parse(GET(url, authHeader));
			for(int i = 0; i < jsonArray.size(); i++){
				jsonObject = (JSONObject)jsonParser.parse(jsonArray.get(i).toString());
				if(jsonObject.get("name").toString().equalsIgnoreCase(releaseName)){
					releaseID = jsonObject.get("id").toString();
					break;
				}
			}
			//To get Suite ID
			url = qtestUrl + "/api/v3/projects/" + projectID + "/test-suites?parentId=" + releaseID + "&parentType=release";
			authHeader = "bearer " + token;
			jsonArray = (JSONArray)jsonParser.parse(GET(url, authHeader));
			for(int i = 0; i < jsonArray.size(); i++){
				jsonObject = (JSONObject)jsonParser.parse(jsonArray.get(i).toString());
				if(jsonObject.get("name").toString().equalsIgnoreCase(testSuiteName)){
					testSuiteID = jsonObject.get("id").toString();
					break;
				}
			}
			//To get Test Run ID
			url = qtestUrl + "/api/v3/projects/" + projectID + "/test-runs?parentId=" + testSuiteID + "&parentType=test-suite";
			authHeader = "bearer " + token;
			jsonArray = (JSONArray)jsonParser.parse(GET(url, authHeader));
			outerloop:
			for(int i = 0; i < jsonArray.size(); i++){
				jsonObject = (JSONObject)jsonParser.parse(jsonArray.get(i).toString());
				String pid = jsonObject.get("pid").toString();
				if(pid.equalsIgnoreCase(testCaseName.trim())) {
					testRunID = jsonObject.get("id").toString();
					break outerloop;
				}
				/*JSONArray tempArray = (JSONArray)jsonParser.parse(jsonObject.get("links").toString());
				for(int j = 0; j < tempArray.size(); j++){
					JSONObject tempJsonObject = (JSONObject)jsonParser.parse(tempArray.get(j).toString());
					if(tempJsonObject.get("rel").toString().equalsIgnoreCase("test-case")) {
						String tempURL = tempJsonObject.get("href").toString();
						JSONObject tempJsonObject2 = (JSONObject)jsonParser.parse(GET(tempURL, authHeader));
						if(tempJsonObject2.get("pid").toString().equalsIgnoreCase(testCaseName.trim())) {
							testRunID = jsonObject.get("id").toString();
							break outerloop;
						}
					}
				}*/
			}
			//To update status value
			switch(status)
			{
					case "Passed": status="601"; break;
					case "Failed": status="602"; break;
					case "Incomplete": status="603"; break;
					case "Blocked": status="604"; break;
					case "Unexecuted": status="605"; break;
			}
			//To update status for Test Run
			url = qtestUrl + "/api/v3/projects/" + projectID + "/test-runs/" + testRunID + "/test-logs";
			authHeader = "bearer " + token;
			contentType="application/json";
			String postBody = "{"
					+ "\"exe_start_date\":\"" + startTime + "\","
					+ "\"exe_end_date\":\"" + endTime + "\","
					+ "\"status\": {"
						+ "\"id\": \""+ status
						+ "\"}"
					+ "}";
			POST(url, postBody, authHeader, contentType);
		} catch (Exception exp) {
			System.out.println("Error while performing updating QTest Results\nException Details: "+exp.getMessage());
		}
	}
}
