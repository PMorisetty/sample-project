package com.pure.utilities;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
@SuppressWarnings({ "resource", "unused" })
public class PolicyDocumentPDFValidation {

	private PDFTextStripper pdfTextStripper;
	private String pdfFileLocation = null;

	public PolicyDocumentPDFValidation(String pdfFileLocation) throws Throwable {
		this.pdfTextStripper = new PDFLayoutTextStripper();
		this.pdfFileLocation = pdfFileLocation;
	}

	/*validateFormNumber-> checks the requested formNumFromInput available in pdf of not.*/
	/* formNumFromInput = "PHVH-END-GEN-029"; Get this i/p from forms mapping xls*/
	public boolean validateFormNumber(String formNumFromInput, int pageNumber) throws Throwable{
		return searchFormNumberInPage(getRequestedPDFPageContent(pageNumber), formNumFromInput);

	}

	public String[] getPageWiseTextFromPDF() throws Throwable, IOException{

		List<PDDocument> pages = (new Splitter()).split(PDDocument.load(new File(pdfFileLocation)));
		String[] arrayOfPageText = new String[pages.size()];
		try {
			for(int i = 0;i<=pages.size()-1;i++){
				arrayOfPageText[i] = pdfTextStripper.getText(pages.get(i));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		};
		return arrayOfPageText;

		/*  
		 *  To get content of all pages in single string
		 *  use string = pdfTextStripper.getText(pdDocument);
		 *  But the content would fit only as much as string's memory.
		 *  better don't try to get everything in one string.
		 *  
		 */
	}


	@SuppressWarnings("finally")
	private String getRequestedPDFPageContent(int pageNumber) throws Throwable{
		PDDocument pdDoc = PDDocument.load(new File(pdfFileLocation));
		List<PDDocument> pages = (new Splitter()).split(pdDoc);

		String content = null;
		String PageText = null;
		String searchKey = null;
		try {
			PageText = pdfTextStripper.getText(pages.get(pageNumber));
			searchKey ="Fraud and Cyber Defense Coverage";

			Scanner scanner = new Scanner(PageText);
			while(scanner.hasNextLine()){
				String lineRead = scanner.nextLine();
				String lineReadTrim = lineRead.trim().replace(" ", "");
				if(lineReadTrim.contains(searchKey.trim().replace(" ","")))
					content = PageText;						
			}
			scanner.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}finally{
			if( pdDoc != null ) {
				for (PDDocument pdDocument : pages) {
					pdDocument.close();
				}
				pdDoc.close();
			}
			return content;
		}
	}


	/*
	 * :::searchFormNumberInPage:::
	 * Form num in pdf diplayed either in full text as
	 * ----------------
	 * PHVH-END-GEN-029
	 * ----------------
	 * or in wrapped text as
	 * ----------------
	 * PHVH-END-
	 * GEN-029
	 * ----------------
	 * Logic to find the requested formnum in pdf is: 
	 * start finding formnum, if not found,
	 * then delete last character from the formnum
	 * then recursively find the formnum.
	 */
	private boolean searchFormNumberInPage(String contentLines, String formNumFromInput){			
		boolean foundFormNum = false;
		while(!foundFormNum){
			if(contentLines.contains(formNumFromInput.trim())){
				foundFormNum = true;
				break;				
			}else if(!foundFormNum){
				int i = formNumFromInput.length();
				StringBuilder stringBuilder = new StringBuilder(formNumFromInput);						
				StringBuilder newInput = stringBuilder.deleteCharAt(i-1);
				return searchFormNumberInPage(contentLines, newInput.toString());					
			}				
		}
		System.out.println("form number found in pdf: " + foundFormNum);
		return foundFormNum;		
	}	

	//*** Split String pageContent to String[] lines ***\\	

	private String[] convertPageContentToStringLines(String pageContent) {
		String[] substrings = null;
		Scanner scanner = new Scanner(pageContent);
		while(scanner.hasNextLine()){
			String lineRead = scanner.nextLine();			
			substrings = pageContent.split("\\s+");				
		}
		return substrings;
	}
	
	@SuppressWarnings("finally")
	private String getTextFromPDF(int pageNumber) throws Throwable{
		PDDocument pdDoc = PDDocument.load(new File(pdfFileLocation));
		List<PDDocument> pages = (new Splitter()).split(pdDoc);
		String PageText = null;
		try {
			PageText = pdfTextStripper.getText(pages.get(pageNumber));
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}finally{
			if( pdDoc != null ) {
				for (PDDocument pdDocument : pages) {
					pdDocument.close();
				}
				pdDoc.close();
			}
			return PageText;
		}
	}
	
	public boolean verifyPolicyNoInPDF(String policyNo, int pageIndex) throws Throwable{
		boolean flag = false;
		String sourceText = getTextFromPDF(pageIndex);
		if(sourceText.contains(policyNo)){
			flag = true;
		}
		return flag;
	}
}	