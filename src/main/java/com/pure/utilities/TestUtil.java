package com.pure.utilities;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;

import com.pure.accelerators.TestEngineWeb;
import com.pure.report.TestResult;;
@SuppressWarnings("all")
public class TestUtil extends TestEngineWeb {
	// true - Y
	// false - N

	/**
	 * Function to check whether test case can be executed or not.
	 */
	/*public boolean isTestCaseExecutable(String testCase, Xls_Reader xls) {
  
   * iterate through the rows of Test Cases sheet from 2nd row till
   * testCase name is equal to the value in TCID column.
   
		for (int rNum = 2; rNum <= xls.getRowCount("Test Cases"); rNum++) {
			// Checks whether testCase(passes value) name is equals to the value
			// in TCID
			if (testCase.equals(xls.getCellData("Test Cases", "TCID", rNum))) {
				// check runmode is equals to Y/N. Returns true if Y else return
				// false
				if (xls.getCellData("Test Cases", "Runmode", rNum)
						.equalsIgnoreCase("Y"))
					return true;
				else
					return false;
			}
		}
		return false;
	}*/
	
	public static synchronized Object[][] getData_NewFormat(Xls_Reader xls,
			 String sheetName, String... testCase) {
		globalsheetName = sheetName;
		int testCaseStartRowNum = 1;
		if(testCase.length>0){ // Previous condition if(testCase[0] != null)   as UAT doesnt have any testcase name to start from
			for (int rNum = 1; rNum <= xls.getRowCount(sheetName); rNum++) {
				// to identify testCase starting row number
				if (testCase[0].equals(xls.getCellData(sheetName, 0, rNum))) {
					testCaseStartRowNum = rNum;
					break;
				}
			}
		}
		gTestCaseStartRowNum = testCaseStartRowNum;
		int colStartRowNum = testCaseStartRowNum + 1;
		int cols = 0;
		while (!xls.getCellData(sheetName, cols, colStartRowNum).equals("")) {
			cols++;
		}
		int rowHeader = 2;
		int rowStartRowNum = testCaseStartRowNum + rowHeader;
		int rows = 0;
		while (!xls.getCellData(sheetName, 0, (rowStartRowNum + rows)).equals("")) {
			rows++;
		}
		Object[][] data = new Object[rows][1];
		Hashtable<String, String> table = null;
		for (int rNum = rowStartRowNum; rNum < (rows + rowStartRowNum); rNum++) {
			table = new Hashtable<String, String>();
			ArrayList<Integer> rowCounter = rowExists(data,"Policy_Number_Txt", xls.getCellData(sheetName, "Policy_Number_Txt", rowHeader, rNum-1), sheetName, rNum);
			int currentCounter=1;
			if(rowCounter.get(1)>0){
				currentCounter = rowCounter.get(1) + 1;
				table = (Hashtable<String, String>) data[rowCounter.get(0)][0];
			}
			table.put("Policy_Number_Txt", xls.getCellData(sheetName, "Policy_Number_Txt", rowHeader, rNum-1));
			table.put(sheetName + "_Count", String.valueOf(currentCounter));
			for (int cNum = 1; cNum < cols; cNum++) {//ignoring 0th column, since it will be Policy_Number_Txt
				table.put(xls.getCellData(sheetName, cNum, rowHeader) + "_" + currentCounter,
						xls.getCellData(sheetName, cNum, rNum));
			}
			if(currentCounter>1){
				data[rowCounter.get(0)][0] = table;
			}else{
				data[rNum - rowStartRowNum][0] = table;
			}
		}
		return removeEmptyObjects(data);
	}
	private static synchronized Object[][] removeEmptyObjects(Object[][] obj){
		int counter = 0;
		for(int objectCount = 0; objectCount < obj.length; objectCount++){
			if(obj[objectCount][0]!=null){counter++;}
		}
		Object[][] finalObject = new Object[counter][1];
		counter = 0;
		for(int objectCount = 0; objectCount < obj.length; objectCount++){
			if(obj[objectCount][0]!=null){
				finalObject[counter][0] = obj[objectCount][0];counter++;
			}
		}
		return finalObject;
	}
	private static synchronized ArrayList<Integer> rowExists(Object[][] obj, String searchKey, String searchValue, String sheetName, int startCounter){
		ArrayList<Integer> tempArrayList = new ArrayList<Integer>();
		for(int counter = 0; counter < obj.length; counter++){
			Hashtable<String, String> table = (Hashtable)obj[counter][0];
			if(table != null && table.get(searchKey)!=null && table.get(searchKey).equalsIgnoreCase(searchValue)){
				tempArrayList.add(counter);
				tempArrayList.add(Integer.valueOf(table.get(sheetName + "_Count")));
				return tempArrayList;
			}
			tempArrayList.clear();
		}
		tempArrayList.add(0);//location of found node 
		tempArrayList.add(0);//number of times value was found
		return tempArrayList;
	}
	/*to merge data wrt policy numbers in test data*/
	public static synchronized Object[][] mergeData(Object[][] obj1, Object[][] obj2){
		Object[][] finalObject = new Object[obj1.length][1];
		for(int counter = 0; counter<obj1.length; counter++){
			Hashtable<String, String> table1 = (Hashtable)obj1[counter][0];
			String policyNumber = table1.get("Policy_Number_Txt");
			Hashtable<String, String> table2 = (Hashtable)findCorrespondingTable(obj2, policyNumber);
			Set<String> keys = table2.keySet();
			for(String key: keys){
				String value = table2.get(key);
				table1.put(key, value);            
			}
			finalObject[counter][0] = table1;
		}
		return finalObject;
	}
	
	public static synchronized Hashtable<String, String> findCorrespondingTable(Object[][] obj, String policyNumber){
		for(int counter = 0; counter<obj.length; counter++){
			Hashtable<String, String> table1 = (Hashtable)obj[counter][0];
			if(policyNumber.equalsIgnoreCase(table1.get("Policy_Number_Txt"))){
				return table1;
			}
		}
		return new Hashtable<String, String>();
	}

	/**
	 * Function to get data from xls sheet in 2 dimensional array
	 *
	 * @param testCase - testCase name
	 * @param xls      - Xls_Reader Object
	 * @return 2 dimensional array
	 */
	public static synchronized Object[][] getData(String testCase, Xls_Reader xls,
									 String sheetName) {
		i = 0;
		if(xls==null)
		{
			LOG.info("Error in getData(): xls of type Xls_Reader should not be null - Test Case :" + testCase);
			throw new RuntimeException("Error in getData(): xls of type Xls_Reader should not be null - Test Case :" + testCase);
		}
//		listofTestCaseDescription.clear();
		globalsheetName = sheetName;
		gTestCaseDesc = null;

//		LOG.info("******getData*******: " + testCase);
		// find the test in xls
		// find number of cols in test
		// number of rows in test
		// put the data in hashtable and put hashtable in object array
		// return object array
		int testCaseStartRowNum = 0;
		// iterate through all rows from the sheet Test Data
		for (int rNum = 1; rNum <= xls.getRowCount(sheetName); rNum++) {
			// to identify testCase starting row number
			if (testCase.equals(xls.getCellData(sheetName, 0, rNum))) {
				testCaseStartRowNum = rNum;
				break;
			}
		}
		gTestCaseStartRowNum = testCaseStartRowNum;

//		LOG.info("Test Starts from row -> " + testCaseStartRowNum);
		// total cols
		int colStartRowNum = testCaseStartRowNum + 1;
		int cols = 0;
		// Get the total number of columns for which test data is present
		while (!xls.getCellData(sheetName, cols, colStartRowNum).equals("")) {
			cols++;
		}

		//LOG.info("Total cols in test -> " + cols);
		// rows
		int rowStartRowNum = testCaseStartRowNum + 2;
		int rows = 0;
		// Incorrect logic, rewritten after comment
		// Get the total number of rows for which test data is present
		while (!xls.getCellData(sheetName, 0, (rowStartRowNum + rows)).equals("")) {
			rows++;
		}
		/*boolean dataFound;
		OuterLoop:
			while(true){
				dataFound = false;
				for(int colCount = 0; colCount < 50; colCount++){
					if(!xls.getCellData(sheetName, colCount, (rowStartRowNum + rows)).equals("")){
						dataFound = true;
					}
				}
				if(!dataFound){break OuterLoop;}else{rows++;}
			}*/

		//LOG.info("Total rows in test -> " + rows);
		Object[][] data = new Object[rows][1];
		Hashtable<String, String> table = null;
		String value = Thread.currentThread().getStackTrace()[2].getClassName();
		value = value.substring(value.lastIndexOf(".") + 1);
		//listofTestCaseDescription = new ArrayList<String>();
		// print the test data
		for (int rNum = rowStartRowNum; rNum < (rows + rowStartRowNum); rNum++) {
			table = new Hashtable<String, String>();
			for (int cNum = 0; cNum < cols; cNum++) {
				table.put(xls.getCellData(sheetName, cNum, colStartRowNum),
						xls.getCellData(sheetName, cNum, rNum));

				//To read testcase description
				if (TestResult.testCaseDescription.get(value)== null){
					if(xls.getCellData(sheetName, cNum, colStartRowNum).equalsIgnoreCase("TestCase_Description")) {
						if(xls.getCellData(sheetName, cNum, rNum) != null){
							gTestCaseDesc = xls.getCellData(sheetName, cNum, rNum);
							//listofTestCaseDescription.add(gTestCaseDesc);
							TestResult.testCaseDescription.put(value, gTestCaseDesc);
						}
					}
				}
			}
			data[rNum - rowStartRowNum][0] = table;
			// LOG.info();
		}
		Global_DataTable = table;
		return data;// dummy
	}

	/**
	 * Function to get start row number
	 *
	 * @param xls - Xls_Reader Object
	 * @param xls - row number
	 */
	public int startRow(Xls_Reader xls, int rowNum) {
		String testCaseStartRow = null;
		testCaseStartRow = xls.getCellData("Test Cases", 3, rowNum);
		int testCaseStartRowNum = Integer.parseInt(testCaseStartRow);
		return testCaseStartRowNum;
	}

	/**
	 * Function to get End row number
	 *
	 * @param xls - Xls_Reader Object
	 * @param xls - row number
	 */
	public int endRow(Xls_Reader xls, int rowNum) {
		String testCaseEndRow = null;
		testCaseEndRow = xls.getCellData("Test Cases", 4, rowNum);
		int testCaseEndRowNum = Integer.parseInt(testCaseEndRow);
		return testCaseEndRowNum;
	}
	
	public static Object[][] getAllData(Object... objects){
		int rows = ((Object[][]) objects[0]).length;
		Object[][] finalObject= new Object[rows][1];
		for(int j = 0; j < rows; j++){
			Hashtable<String, String> finalTable = new Hashtable<String, String>();
			for(int i = 0; i < objects.length; i++){
				Object[][] temp = (Object[][]) objects[i];
				if(j != temp.length){
					try{
						Hashtable<String, String> table = (Hashtable)temp[j][0];
						Set<String> keys = table.keySet();
						for(String key: keys){
							String value = table.get(key);
							finalTable.put(key, value);            
						}
					}catch(Exception ex){
						//Not mandatory for all objects to contain more than one row
					}
				}
			}
			finalObject[j][0] = finalTable;
		}
		return finalObject;
	}
}