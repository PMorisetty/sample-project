package com.pure.accelerators;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.google.common.base.Strings;
import com.pure.commonutilities.CustomTestListenerCT;
import com.pure.report.CReporter;
import com.pure.report.ConfigFileReadWrite;
import com.pure.report.ReporterConstants;
import com.pure.support.MyListener;
import com.pure.utilities.Xls_Reader;
import com.pure.utilities.Zip;
import com.pure.testmanagement.QTestAPI_Bridge;
import com.pure.testmanagement.QTestRunLog;
import com.pure.testmanagement.QTestAPI_Bridge.tcResult;

import java.lang.reflect.Method; 


@SuppressWarnings({"unused"})
@Listeners(CustomTestListenerCT.class)
public class TestEngineWeb {
	Date QTeststartTime, QTestendTime;
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	QTestAPI_Bridge qTest = new QTestAPI_Bridge("pureinsurance");
	JSONObject jsCycleInfo = null;
	List<QTestRunLog> tcLog = new ArrayList<QTestRunLog>();
	tcResult scriptResult = tcResult.UNEXECUTED;
	int QTestTCCount=0;

	public static final Logger LOG = Logger.getLogger(TestEngineWeb.class);
	protected WebDriver WebDriver = null;
	public EventFiringWebDriver driver = null;
	public CReporter reporter = null;
	public ITestContext ctx = null;
	public static String gTestCaseDesc = null;

	/* cloud platform */
	public static String browser = null;
	public static String version = null;
	public static String platform = null;
	public String seleniumgridurl = null;
	public String environment = null;
	public static String Environment = null;
	public String localBaseUrl = null;
	public String userName = null;
	public String accessKey = null;
	public String cloudImplicitWait = null;
	public String cloudPageLoadTimeOut = null;
	public String updateJira = null;
	public String buildNumber = "";
	public String jobName = "";
	public String executedFrom = null;
	public String executionType = null;
	public String suiteExecution = null;
	public String suiteStartTime = null;
	public String APP_BASE_URL = null;
	public String SUMMARY_REPORTER_BASEURL = null;
	public static String LOCATION_CLIENT_LOGO = null;

	public static long startTime;
	public static String newCycleID = null;

	public HashMap<String, String> mapObj = new HashMap<>();

	// Below parameters are for update the test data into Excel at run time
	public static String globalsheetName = null;
	public static int gTestCaseStartRowNum = 0;

	public static String gTestData = "./TestData" + File.separator
			+ "TestData.xlsx";
	//	public static ArrayList<String> listofTestCaseDescription = new ArrayList<>();
	public static int i = 0;

	public static Hashtable<String, String> Global_DataTable = null;
	public static String region;
	public static String executionFrom;
	public static String updateQTestResults;
	public static String universalAgentRunsJsonPath;
	public static String qTestProjectName;
	public static boolean qTestCreateNewCycle;
	public static String qTestParentCycleOrReleaseID;
	public static String qTestParentNodeType;	
	public boolean isClaim = false;

	public static int universalID = 0;
	
	static{
		//USE THIS BLOCK TO SET CLAIMS ENV to run LOCAL.
		// This value is used for claims input data read.
		region="PROD";
		//region="CLAIMMAINT";
		//region="CLAIMDEV";
	}

	
	@Parameters({ "executionType", "suiteExecuted", "browser", "threadCount", "executionFrom", "updateQTestResults", "universalAgentRunsJsonPath", "region"})
	@BeforeSuite(alwaysRun = true)
	public void beforeSuite(ITestContext ctx, String type, String suite,
			String browserName, String threadCount, @Optional("local") String curr_ExecutionFrom, @Optional("false") String curr_updateQTestResults, @Optional("") String curr_universalAgentRunsJsonPath, @Optional("") String curr_Region) throws Throwable {
		String enteredTry = "Not Entered Try in Before Suite";
		LOG.info("---------------------------Entering Before Suite---------------------------");
		try
		{
			enteredTry = "Entered Try in Before Suite";
			LOG.info("---------------------");
			LOG.info("-----Before Suite----");
			LOG.info("---------------------");
			
			LOG.info("Entered @BeforeSuite in TestEngineWeb");
			if(!curr_ExecutionFrom.equalsIgnoreCase("QTest"))
			{
				try
				{
					region = System.getProperty("region");
				}
				catch(Exception ex)
				{
					region = curr_Region;
				}
			}			
			if(curr_ExecutionFrom.equalsIgnoreCase("QTest") || region==null)
			{
				region = curr_Region;			
			}
			
			LOG.info("region set in TestEngineWeb @BeforeSuite: " + region);
			TestEngineWeb.executionFrom = curr_ExecutionFrom;
			TestEngineWeb.updateQTestResults = curr_updateQTestResults;
			TestEngineWeb.universalAgentRunsJsonPath = curr_universalAgentRunsJsonPath;
			// 		Below should be used only for local runs		
			//		region = "CLAIMMAINT";
			//		region = "PROD";		
			startTime = System.currentTimeMillis();
			ctx.setAttribute("browser", browserName);//System.getenv("Browsers"));
			CReporter.SUITE_NAME = ctx.getCurrentXmlTest().getSuite().getName();
			LOG.info(browserName);//System.getenv("Browsers"));
			LOG.info("--------------------------------------------------------------------------");
			LOG.info("------------------Suite :: " + suite + "------------------------------");
			LOG.info("Execution Start Time :: " + startTime);
			LOG.info("Executing on :: " + browserName.toUpperCase());
			LOG.info("---------------------------End Suite Details-----------------------------------");
			executionType = type;
			suiteExecution = suite;
			PropertyConfigurator.configure("./Log.properties"); //System.getProperty("user.dir")
	
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy hh mm ss SSS");
			String formattedDate = sdf.format(date);
			suiteStartTime = formattedDate.replace(":", "_").replace(" ", "_");
			LOG.info("Before Suite time ==============>" + suiteStartTime);
			if(seleniumgridurl!=null && !seleniumgridurl.equalsIgnoreCase("local")){
				if(universalID == 0){
					Random rand = new Random();
					int n = rand.nextInt(32768) + 1;
					universalID = n;
				}
				LOG.info("---------------------------Setting up nodes---------------------------");
				setupNodes(browserName.toLowerCase(), Integer.toString(universalID),	Integer.valueOf(threadCount),seleniumgridurl);
				LOG.info("---------------------------Node setup complete------------------------");
			}
		}
		catch(Exception ex)
		{
			LOG.error("Error in beforesuite() Error: " + ex.getMessage());
			throw new RuntimeException("Error in beforesuite() Error: " + ex.getMessage(), ex);

		}
		initClaimsXLSObject();
		LOG.info("---------------------------Exiting from Before Suite Gracefully:"+ enteredTry + "---------------------------");
	}

	@BeforeClass(alwaysRun = true)
	@Parameters({ "automationName", "browser", "browserVersion", "environment",
		"platformName", "seleniumgridurl", "qTestProjectName", "qTestCreateNewCycle","qTestParentCycleOrReleaseID","qTestParentNodeType" })
	public void beforeClass(String automationName, String browser,
			String browserVersion, String environment, String platformName,
			String seleniumgridurl, @Optional("") String curr_qTestProjectName, @Optional("false") String curr_qTestCreateNewCycle, 
			@Optional("") String curr_qTestParentCycleOrReleaseID, @Optional("") String curr_qTestParentNodeType, ITestContext iTestContext)
					throws IOException, InterruptedException {
		// set and get system property at before class
		
		String testName = iTestContext.getAllTestMethods()[0].getInstance().getClass().toString();
		
		initClaimsXLSObject();

		APP_BASE_URL = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "emBaseUrl");
		SUMMARY_REPORTER_BASEURL = APP_BASE_URL;

		LOG.info("---------------------");
		LOG.info("-----Before Class----");
		LOG.info("---------------------");
		LOG.info("Execution Start Time :: " + startTime);
		LOG.info("browser name :: " + browser + " Browser version :: "
				+ browserVersion + " platform ::" + platformName);

		TestEngineWeb.browser = browser;
		TestEngineWeb.version = browserVersion;
		TestEngineWeb.platform = platformName;
		TestEngineWeb.Environment= environment;
		Environment = environment;
		this.seleniumgridurl = seleniumgridurl;
		//		this.localBaseUrl = ReporterConstants.APP_BASE_URL;
		this.executedFrom = System.getenv("COMPUTERNAME");
		TestEngineWeb.qTestProjectName = curr_qTestProjectName;
		TestEngineWeb.qTestCreateNewCycle = false;
		
		if(curr_qTestCreateNewCycle.equalsIgnoreCase("true")){
			TestEngineWeb.qTestCreateNewCycle = true;			
		}
		TestEngineWeb.qTestParentCycleOrReleaseID = curr_qTestParentCycleOrReleaseID;
		String mappingCycleName = "REPORTFOLDER_";
		if(TestEngineWeb.executionFrom.equalsIgnoreCase("jenkins")){
			if(testName.contains("smoke")){
				mappingCycleName = mappingCycleName+ "SMOKE_";
			}else if(testName.contains("regression")){
				mappingCycleName = mappingCycleName+ "REGRESSION_";					
			}
			mappingCycleName = mappingCycleName+ TestEngineWeb.region.toUpperCase();
			TestEngineWeb.qTestParentCycleOrReleaseID = ConfigFileReadWrite.read(ReporterConstants.qtestConfigFile, mappingCycleName);
			System.out.println("Mapping Cycle Name: " + mappingCycleName + ", Test Cycle ID: " + TestEngineWeb.qTestParentCycleOrReleaseID);
		}
		
		TestEngineWeb.qTestParentNodeType = curr_qTestParentNodeType;		
		this.updateJira = "";

		LOCATION_CLIENT_LOGO = ReporterConstants.PURE_LOCATION_CLIENT_LOGO;
		LOG.info(environment);
		if (environment.equalsIgnoreCase("local")) {
			LOG.info("Selenium Grid URL :: " + seleniumgridurl);
			this.setWebDriverForLocal(browser, seleniumgridurl);
		}
		if (environment.equalsIgnoreCase("cloudBrowserStackJenkins")) {
			/* TBD: Not Implemented For Running Using Jenkins */
			this.updateConfigurationForCloudBrowserStackJenkins();
		}
		LOG.info("Assigning event firing webdriver instance to driver");
		LOG.info("WebDriver ::: " + this.WebDriver.toString());
		driver = new EventFiringWebDriver(this.WebDriver);
		LOG.info("Assigned event firing webdriver instance to driver");
		LOG.info("eventfiring driver ::: " + driver.toString());

		if (!environment.equalsIgnoreCase("cloudSaucelabs")) {
			MyListener myListener = new MyListener();
			driver.register(myListener);
		}
		LOG.info("Creating reporter instance");
		reporter = CReporter.getCReporter(TestEngineWeb.browser, TestEngineWeb.platform, TestEngineWeb.Environment,
				driver.toString());
		reporter_StartNewTestCase(null,null);

		LOG.info("---------------------");
		LOG.info("---End Before Class--");
		LOG.info("---------------------");		
	}
	/**
	 * Starts New Detailed Test Case in reporter 
	 * @param testName
	 * @param testCaseDescription
	 * 
	 * Example 1: Below example will start a new test case report with the existing method name as test case name
	 * startNewTestCase_InReporter(null, null);
	 *
	 * Example 2: Below example start new test report with the name "Test_To_Login_001" 
	 * startNewTestCase_InReporter("Test_To_Login_0001", "Login functionality is verified");
	 */
	public void reporter_StartNewTestCase(String testName, String testCaseDescription)
	{
		if(testName == null)
		{
			testName = this.getClass()
					.getName()
					.substring(
							this.getClass().getName().lastIndexOf(".") + 1);

		}
		reporter.initTestCase(
				this.getClass()
				.getName()
				.substring(0,
						this.getClass().getName().lastIndexOf(".")),
						testName,
						testCaseDescription, true);
		LOG.info("Started New Reporter Test instance : " + testName);				
		reporter.calculateSuiteStartTime();
		QTest_StartTestRun(testName);
	}

	@Parameters({ "browser" })
	@AfterClass(alwaysRun = true)
	public void afterClass(String browser) throws Throwable {
		
		reporter.calculateTestCaseExecutionTime();
		reporter.closeDetailedReport();
		SessionId sessionid = ((RemoteWebDriver) WebDriver).getSessionId();
		LOG.info("******sessionid*******"+sessionid);
		reporter.updateTestCaseStatus(sessionid);	
		try {
			this.driver.quit();
			LOG.info("Driver quit :: " + browser);
		} catch (Exception e) {
			LOG.warn("Driver exception :: " + e.getMessage());
		}
	}

	public void reporter_CloseTestCase(ITestResult result, tcResult currTestRunResult) throws Exception
	{
		reporter.calculateTestCaseExecutionTime();
		reporter.closeDetailedReport();
		SessionId sessionid = ((RemoteWebDriver) WebDriver).getSessionId();
		LOG.info("******sessionid*******"+sessionid);
		reporter.updateTestCaseStatus(sessionid);
		QTest_CloseTestRun(result, currTestRunResult);
	}

	/**
	 * 
	 * @param method
	 */
	@BeforeMethod
	public void beforeMethod(Method method) {
		//QTest_StartTestRun(method, null);
	}
	/**
	 * Method to set the QTest to create/initiate test run
	 * @param method
	 * @param testName - can be null or a string with the test name
	 */
	private void QTest_StartTestRun(String testName)
	{
		if(!TestEngineWeb.updateQTestResults.equalsIgnoreCase("true"))
		{
			return;
		}
		String QTestStartTime = dateFormatter.format(new Date());
		System.out.println("Test Name: "+ testName);
		LOG.info("Entered QTestBeforeTest"); 
		try {
			if(qTest != null){
				String scriptName = ConfigFileReadWrite.read(ReporterConstants.qtestConfigFile, testName);
				LOG.info("Test Mapping Details for Test Name: " + testName + ", " + scriptName);
				System.out.println("Test Mapping Details for Test Name: " + testName + ", " + scriptName);
				if(scriptName==null)
				{
					throw new RuntimeException("QTest Mapping is not found for Test Name:" + testName);
				}
				String[] tcArr = scriptName.split(",");
				//Get QTestAuthorization
				qTest.authorize();
				//Load Project
				qTest.loadProject(tcArr[1]);//TestEngineWeb.qTestProjectName);// "z_QConnect - Sample Project");

				boolean qTestCreateNewCycle=TestEngineWeb.qTestCreateNewCycle;
				String qTestParentCycleOrReleaseID = TestEngineWeb.qTestParentCycleOrReleaseID;
				String qTestParentCycleOrRelease = TestEngineWeb.qTestParentNodeType;
				//System.out.println("QTest Project :" + TestEngineWeb.qTestProjectName);

				if(!TestEngineWeb.executionFrom.equalsIgnoreCase("QTest"))
				{				
					jsCycleInfo = qTest.createOrGetTestCycle(qTestParentCycleOrRelease, qTestParentCycleOrReleaseID, qTestCreateNewCycle);
					System.out.println("Test Case Number:" + tcArr[0]);
				}

				String[] tcNumberArr = tcArr[0].split("\\s*\\|\\s*");
				for (int tcCounter = 0; tcCounter < tcNumberArr.length; tcCounter++)
				{
					QTestRunLog currTCLog = null;
					if((!TestEngineWeb.executionFrom.equalsIgnoreCase("qtest")))
					{
						currTCLog = qTest.startTestRunLog(jsCycleInfo, tcNumberArr[tcCounter], testName);
					}
					else
					{
						if(tcCounter==0)
						{							
							currTCLog = qTest.getUniversalAgentTestRunFromLog(TestEngineWeb.universalAgentRunsJsonPath, tcNumberArr[tcCounter], testName);
						}
					}
					currTCLog.setStartTime(QTestStartTime);
					tcLog.add(currTCLog);
					testName="";
				}
			}		

		} catch (Exception e) {
			qTest = null;
			e.printStackTrace();
			LOG.error("Unable to create Test Run Log for test case:" + testName + ", " + e.getMessage());
			throw new RuntimeException("Unable Create Test Run Log :" + e.getMessage(), e);
		}
	}
	/*
	 * @BeforeMethod public void beforeMethod(Method method) { int k; // get
	 * browser info // list object is the value of test case description
	 * HashMap<String, Integer> tcDescriptionMapObject = new HashMap<>(); for
	 * (int j = 0; j < listofTestCaseDescription.size(); j++) { if
	 * (tcDescriptionMapObject.get(listofTestCaseDescription.get(j).replace(" ",
	 * "")) == null)
	 * tcDescriptionMapObject.put(listofTestCaseDescription.get(j).replace(" ",
	 * ""), j); } if (listofTestCaseDescription.size() == 0) { k = 0; } else { k
	 * =
	 * tcDescriptionMapObject.get(listofTestCaseDescription.get(i).replace(" ",
	 * "")); i = i + 1; } //
	 * reporter.initTestCase(this.getClass().getName().substring(0,
	 * this.getClass().getName().lastIndexOf(".")), // method.getName() + "-" +
	 * k, null, true); // i=i+1; }
	 */

	@AfterMethod
	public void afterMethod(ITestResult result) throws Exception{
		try{
			LOG.info("After method executing...");
			if (result.getStatus() == ITestResult.FAILURE) {
				String methodName = result.getMethod().getMethodName();
				reporter.addFailureIfSkipped("Execution failed",
						"Execution failed due to :: "
								+ result.getThrowable().toString(), driver);
			}  
			QTest_CloseTestRun(result, null);
		}catch(Exception e){
			e.printStackTrace();
			LOG.error("Error in afterMethod():" + e.getMessage());
			throw new RuntimeException("Error in afterMethod():" + e.getMessage());			
			//if "adding failure if skipped" raises exception
		}

		//QTest Implementation
		System.out.println("Inside AfterMethod------------------------------------------\n");

		//QTest_CloseTestRun(result, null);			
	}

	private void QTest_CloseTestRun(ITestResult result, tcResult testRunResult) throws Exception
	{

		if(!TestEngineWeb.updateQTestResults.equalsIgnoreCase("true"))
		{
			return;
		}
		tcResult currRunResult = testRunResult;
		String QTestEndTime = dateFormatter.format(new Date());
		if(result!=null)
		{
			if (result.getStatus() == ITestResult.FAILURE)
			{
				scriptResult = tcResult.FAILED;
			}
			else
			{
				scriptResult = tcResult.PASSED;
			}
		}
		else{
			if(testRunResult == tcResult.PASSED)
			{
				scriptResult = tcResult.PASSED;
			}
			else
			{
				scriptResult = tcResult.FAILED;
			}
		}
		for (QTestRunLog currRunLog : tcLog) {
			if(currRunLog != null){
				currRunLog.setEndTime(QTestEndTime);
				currRunLog.addAttachment(reporter.getDetailedFilePath(), "application/octet-stream");
				//if (result.getStatus() == ITestResult.FAILURE){
				if(scriptResult == tcResult.FAILED)
				{
					currRunLog.addAttachment(reporter.getFailureScreenshot(), "application/octet-stream");
				}
				else{
					scriptResult = tcResult.PASSED;
				}
				currRunLog.setExecutionStatus(scriptResult);
				JSONObject jsRes = qTest.postTestRunLog(currRunLog);
				currRunLog = null;				
			}
		}
		scriptResult = tcResult.UNEXECUTED;
		jsCycleInfo = null;
		tcLog = new ArrayList<QTestRunLog>();
		/*
		if(tcLog != null){
			tcLog.setEndTime(dateFormatter.format(new Date()));
			tcLog.addAttachment(reporter.getDetailedFilePath(), "application/octet-stream");
			if (result.getStatus() == ITestResult.FAILURE){
				scriptResult = tcResult.FAILED;
				tcLog.addAttachment(reporter.getFailureScreenshot(), "application/octet-stream");
			}
			else{
				scriptResult = tcResult.PASSED;
			}
			tcLog.setExecutionStatus(scriptResult);
			qTest.postTestRunLog(tcLog);
			scriptResult = tcResult.UNEXECUTED;
			jsCycleInfo = null;
			tcLog = null;
		}
		 */

	}

	public void setWebDriverForLocal(String browser, String seleniumgridurl)
			throws IOException, InterruptedException {
		DesiredCapabilities capab = null;
		String downloadFilepath = "." + File.separator + "Downloads";
		String driversPath = "." + File.separator + "Drivers" + File.separator;
		switch (browser.toLowerCase()) {
		case "firefox":
			capab = DesiredCapabilities.firefox();
			System.setProperty("webdriver.gecko.driver", driversPath
					+ "geckodriver.exe");
			final FirefoxProfile firefoxProfile = new FirefoxProfile();
			firefoxProfile.setPreference("browser.download.dir",
					downloadFilepath);
			firefoxProfile.setPreference("browser.download.folderList", 2);
			firefoxProfile.setPreference(
					"browser.helperApps.neverAsk.openFile",
					"application/octet-stream");
			firefoxProfile
			.setPreference(
					"browser.helperApps.neverAsk.saveToDisk",
					"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;"
							+ "application/pdf;"
							+ "application/vnd.openxmlformats-officedocument.wordprocessingml.document;"
							+ "text/plain;" + "text/csv;"
							+ "application/octet-stream");
			firefoxProfile.setPreference(
					"browser.download.manager.showWhenStarting", false);
			firefoxProfile.setPreference("pdfjs.disabled", true);

			firefoxProfile
			.setPreference("xpinstall.signatures.required", false);

			capab.setCapability(FirefoxDriver.PROFILE, firefoxProfile);

			if (seleniumgridurl.equalsIgnoreCase("local")) {
				this.WebDriver = new FirefoxDriver();
			}
			LOG.info("Driver launch :: " + browser);
			break;

		case "ie":
			capab = DesiredCapabilities.internetExplorer();
			File file = new File(driversPath + "IEDriverServer.exe");
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			// To disable popup blocker.
			capab.setCapability(
					InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, true);
			// to enable protected mode settings
			capab.setCapability(
					InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
					true);
			capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

			// to get window focus
			capab.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS,
					true);
			// to set zoom level is set to 100% so that the native mouse events
			// can be set to the correct coordinates.
			capab.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING,
					true);
			capab.setCapability(
					InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			capab.setCapability(InternetExplorerDriver.NATIVE_EVENTS, true);
			Process p = Runtime.getRuntime().exec(
					"RunDll32.exe InetCpl.cpl,ClearMyTracksByProcess 255");
			p.waitFor();
			if (seleniumgridurl.equalsIgnoreCase("local")) {
				this.WebDriver = new InternetExplorerDriver(capab);
			}
			break;
		case "chrome":
			HashMap<String, Object> chromePrefs = new HashMap<>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.default_directory", downloadFilepath);
			chromePrefs.put("plugins.always_open_pdf_externally", true);
			chromePrefs.put("plugins.plugins_disabled", new String[] {
					"Adobe Flash Player", "Chrome PDF Viewer" });
			chromePrefs.put("download.prompt_for_download", false);
			chromePrefs.put("download.directory_upgrade", true);

			capab = DesiredCapabilities.chrome();
			if (seleniumgridurl.equalsIgnoreCase("local")) {
				System.setProperty("webdriver.chrome.driver", driversPath
						+ "chromedriver_32Bit.exe");
			}
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);
			capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			options.addArguments("--start-maximized");
			//options.addArguments("--incognito");
			options.addArguments("test-type");
			options.addArguments("chrome.switches", "--disable-extensions");
			capab.setCapability(ChromeOptions.CAPABILITY, options);
			if (seleniumgridurl.equalsIgnoreCase("local")) {
				this.WebDriver = new ChromeDriver(capab);
			}
			LOG.info("Driver launch :: " + browser);
			break;
		case "edge":
			LOG.info("In the case Edge");
			System.setProperty("webdriver.edge.driver", driversPath
					+ "MicrosoftWebDriver.exe");
			capab = DesiredCapabilities.edge();
			if (seleniumgridurl.equalsIgnoreCase("local")) {
				this.WebDriver = new EdgeDriver(capab);
			}
			LOG.info("Driver launch :: " + browser);
			break;

		case "Safari":
			for (int i = 1; i <= 10; i++) {
				try {
					if (seleniumgridurl.equalsIgnoreCase("local")) {
						this.WebDriver = new SafariDriver();
					}
					break;
				} catch (Exception e1) {
				}
			}
		}
		// ReporterConstants.SELENIUM_GRID_HUB_URL
		LOG.info("");
		if (!seleniumgridurl.equalsIgnoreCase("local")) {
			capab.setCapability("uuid", universalID);
			LOG.info("Assigning remote webdriver instance to this.WebDriver");
			this.WebDriver = new Augmenter().augment(new RemoteWebDriver(
					new URL(seleniumgridurl), capab));
			LOG.info("this.WebDriver :::" + this.WebDriver.toString());
			((RemoteWebDriver) this.WebDriver)
			.setFileDetector(new LocalFileDetector());
			LOG.info("Assigned remote webdriver instance to this.WebDriver");
		}
	}

	/* TBD: Not Implemented For Running Using Jenkins */
	private void updateConfigurationForCloudBrowserStackJenkins() {
	}

	public String getBrowser() {
		return this.browser;
	}

	@Parameters({ "executionType", "suiteExecuted" })
	@AfterSuite(alwaysRun = true)
	public void afterSuite(ITestContext ctx, String type, String suite)
			throws Throwable {
		reporter.calculateSuiteExecutionTime();
		reporter.createHtmlSummaryReport(SUMMARY_REPORTER_BASEURL, true);
		reporter.closeSummaryReport();
		startTime = System.currentTimeMillis();
		ctx.setAttribute("browser", System.getenv("Browsers"));
		LOG.info("--------------------------------------------------------------------------");
		LOG.info("------------------Suite :: " + suite
				+ "------------------------------");
		LOG.info("AfterSuite Start Time :: " + startTime);
		LOG.info("---------------------------End After Suite Details-----------------------------------");

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy hh mm ss SSS");
		String formattedDate = sdf.format(date);
		suiteStartTime = formattedDate.replace(":", "_").replace(" ", "_");
		LOG.info("After Suite end time ==============>" + suiteStartTime);
		LOG.info("Before killing " + browser + " browser");
		//Runtime.getRuntime().exec("taskkill /F /IM chromedriver_32Bit.exe");
		//Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer64bit.exe");
		//Runtime.getRuntime().exec("taskkill /F /IM MicrosoftWebDriver.exe");
		LOG.info("After killing " + browser + " browser");
		LOG.info("Before Zipping the results folder");
		String sourceFolder = "results";
		String destFolder = "results.zip";
		Properties prop = System.getProperties();
		//destFolder = prop.getProperty("java.io.tmpdir")+destFolder;
		Zip.zipFolder(sourceFolder, destFolder);
		LOG.info("After Zipping the results folder");
	}

	/*Function to create nodes*/
	private void setupNodes(String browser, String uuid, int count, String seleniumgridurl)
			throws Throwable {
		URL url = null;
		String seleniumgridurlTestNG = seleniumgridurl;
		seleniumgridurlTestNG = seleniumgridurlTestNG.replace("/wd/hub", "/grid/admin");
		if (browser == "internetexplorer") {
			url = new URL(
					seleniumgridurlTestNG+ "/AutomationTestRunServlet?uuid="
							+ uuid + "&threadCount=" + count + "&browser="
							+ browser + "&os=windows");
		} else {
			String tempURL = seleniumgridurlTestNG+ "/AutomationTestRunServlet?uuid="
					+ uuid + "&threadCount=" + count + "&browser=" + browser;
			System.out.println("Temp URL is::: " + tempURL);
			url = new URL(tempURL);
			/*
			 * url = new URL(
			 * "http://10.40.32.16:4444/grid/admin/AutomationTestRunServlet?uuid="
			 * + uuid + "&threadCount=" + count + "&browser=" + browser);
			 */
		}
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		if (conn.getResponseCode() != 201 && conn.getResponseCode() != 202) {
			throw new Throwable("Failed : HTTP error code : "
					+ conn.getResponseCode());
		} else if (conn.getResponseCode() == 201) {
			/*Wait until session is ready*/
			Date d1 = new Date();
			while(!waitForNode(browser, count, seleniumgridurl)){
				Thread.sleep(500);
				Date d2 = new Date();
				if((d2.getTime() - d1.getTime()) > 600000)
					throw new Throwable("Unable to create nodes with " + count
							+ " number of instances within 10 minutes.");
			}
			System.out.println("wait is over");
		}
		conn.disconnect();
	}

	private boolean waitForNode(String browserName, int numberOfInstances, String seleniumgridurl){
		boolean status = false;
		try {
			String url2 = "http://seleniumgrid.purehnw.com:4444/grid/console";
			String outputResponse;
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet(url2);
			HttpResponse response = client.execute(getRequest);                    
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			String responseData = "";
			while ((outputResponse = br.readLine()) != null) {
				responseData+=outputResponse;
			}
			int count = StringUtils.countMatches(responseData,
					browserName.toLowerCase() + ".png");
			LOG.info("Available number of instances:::" + count);
			if (count >= numberOfInstances)
				status = true;
		} catch (Exception e) {
			LOG.info("Failed to verify node creation due to :: "
					+ e.getMessage());
		}
		return status;
	}
	
	public void initClaimsXLSObject()
	{
		if(ActionEngine.TestDataClaim!=null) return;
		LOG.info("---------------------------------------Initiated ClaimsXLSObject---------------------------------------");
		ActionEngine.TestDataClaim = new Xls_Reader("./TestData/TestDataClaim.xlsx");
		if(ActionEngine.TestDataClaim!=null){
			switch(region.toUpperCase()){
			case "CLAIMQA": 
				ActionEngine.TestDataClaim = new Xls_Reader("./TestData/TestDataClaimQA.xlsx");
				break;
			case "CLAIMMAINT":
				ActionEngine.TestDataClaim = new Xls_Reader("./TestData/TestDataClaimMAINT.xlsx");
				break;
			case "CLAIMDEV":
				ActionEngine.TestDataClaim = new Xls_Reader("./TestData/TestDataClaimDEV.xlsx");
				break;
			}
		}
	}
}
